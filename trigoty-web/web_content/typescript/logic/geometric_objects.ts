/**
 * Holds the geometrical object prototypes.
 */


import { min, max, StandardObject, removeFromList } from "../util/shared";
import { debug, canvasContext } from "../run/main"; //Both information shall only be used for debugging
import { IngameTriangle } from "../board_handling/game_objects";

export interface GeometricObject extends StandardObject { }

/**
 * Represents the orientation of three vertices that constitute a
 * {@code Triangle} in 2D, based on a point of origin in the bottom left
 * 
 * @author J
 *
 */
enum Orientation {
    CLOCKWISE, COUNTER_CLOCKWISE, COLINEAR,
}

/**All geometry is adjusted on this orientation*/
let targetOrientation: Orientation = Orientation.CLOCKWISE;

export class Vertex implements GeometricObject {
    private _x: number;
    private _y: number;

    get x() { return this._x; }
    get y() { return this._y; }

    constructor( x: number, y: number ) {
        this._x = x;
        this._y = y;
    }

    subtractVertex( vertex: Vertex ): Vertex {
        return new Vertex( this.x - vertex.x, this.y - vertex.y );
    };

    skalarProduct( vertex: Vertex ): number {
        return ( this.x * vertex.x ) + ( this.y * vertex.y );
    };

    inverseVertex(): Vertex {
        return new Vertex( -this.x, -this.y );
    };

    sqrDistanceToPoint( vertex: Vertex ): number {
        return Math.pow( this.x - vertex.x, 2 ) + Math.pow( this.y - vertex.y, 2 );
    };

    toString = (): string => {
        return this.x + "," + this.y;
    }

    equals( vertex: object ): boolean {
        if ( !vertex ) return false;
        if ( !( vertex instanceof Vertex ) ) return false;
        else return this.x == vertex.x && this.y == vertex.y;
    }

    compareTo( toCompareWith: Vertex ): number {
        let xComp = this.x - toCompareWith.x;
        if ( xComp != 0 ) return xComp;
        else return this.y - toCompareWith.y;
    }
}

export class Edge implements GeometricObject {
    private _startVertex: Vertex;
    private _prevEdge: Edge;
    private _nextEdge: Edge;
    private _twinEdge: Edge;
    private _relatedTriangle: Triangle;

    get startVertex(): Vertex { return this._startVertex; }
    set startVertex( startVertex: Vertex ) { this._startVertex = startVertex; }
    get prevEdge(): Edge { return this._prevEdge; }
    set prevEdge( prevEdge: Edge ) { this._prevEdge = prevEdge; }
    get nextEdge(): Edge { return this._nextEdge; }
    set nextEdge( nextEdge: Edge ) { this._nextEdge = nextEdge; }
    //SINCE public get and private set are not allowed yet, but set is complex and may fail (see linkAsTwins), we skip the whole approach here
    //    get twinEdge(): Edge { return this._twinEdge; }
    //    private set twinEdge( twinEdge: Edge ) {
    //        if ( !twinEdge ) return;
    //        this._twinEdge = twinEdge;
    //        twinEdge._twinEdge = this;
    //    }
    get relatedTriangle(): Triangle { return this._relatedTriangle; }
    set relatedTriangle( relatedTriangle: Triangle ) {
        this._relatedTriangle = relatedTriangle;
        if ( !this._prevEdge.relatedTriangle ) this._prevEdge.relatedTriangle = relatedTriangle;
        if ( !this._nextEdge.relatedTriangle ) this._nextEdge.relatedTriangle = relatedTriangle;
    }

    constructor( startVertex: Vertex, prevEdge: Edge, nextEdge: Edge, twinEdge?: Edge ) {
        this._startVertex = startVertex;
        this._prevEdge = prevEdge
        this._nextEdge = nextEdge;
        this._twinEdge = twinEdge;
        this._relatedTriangle
    }


    /**
     * Registers a given {@code Edge} as twin for this {@code Edge}, if possible. Fails if there is already a twin {@code Edge} or it's no proper twin.
     * @param toRegister the proposed twin {@code Edge}
     * @return {@code true} if the twin was successfully registered, otherwise {@code false}
     */
    linkAsTwins( toLink: Edge ): boolean {
        if ( !toLink || toLink._twinEdge ) return false;
        if ( !this._twinEdge && this.isProperTwinFor( toLink ) ) {
            this._twinEdge = toLink;
            toLink._twinEdge = this;
            return true;
        } else return false;
    }

    getTwinEdge(): Edge {
        return this._twinEdge;
    }

    /**
     * Creates a twin for a given {@code Edge}. The given {@code Edge} won't be linked to the created twin model.
     * For that, use {@link Edge#linkAsTwins(Edge)}.
     * @param toConvert a twin will be created for
     * @return an {@code Edge} working as proper twin 
     */
    static createTwinModel( toConvert: Edge ): Edge {
        let twinNextEdge: Edge = toConvert.prevEdge._twinEdge;
        if ( twinNextEdge == null )
            twinNextEdge = new Edge( toConvert.startVertex, null, null, null );
        return new Edge( toConvert.nextEdge.startVertex, toConvert.nextEdge._twinEdge, twinNextEdge, toConvert );
    }

    isProperTwinFor( edge: Edge ): boolean {
        if ( !edge ) return false;
        else return this.startVertex.equals( edge.nextEdge.startVertex ) && this.nextEdge.startVertex.equals( edge.startVertex );
    }

    drop() {
        if ( this._twinEdge ) this._twinEdge._twinEdge = null;
        if ( this.nextEdge )
            this.nextEdge.prevEdge = null;
        if ( this.prevEdge )
            this.prevEdge.nextEdge = null;
    }

    sqrDistanceToPoint( vertex: Vertex ): number {
        // stolen from http://stackoverflow.com/a/6853926/5767484
        var y1 = this.startVertex.y;
        var x1 = this.startVertex.x;
        var x2 = this.nextEdge.startVertex.x;
        var y2 = this.nextEdge.startVertex.y;
        var A = vertex.x - x1;
        var B = vertex.y - y1;
        var C = x2 - x1;
        var D = y2 - y1;

        var dot = A * C + B * D;
        var len_sq = C * C + D * D;
        var param = -1;
        if ( len_sq != 0 ) // in case of 0 length line
            param = dot / len_sq;

        var xx, yy;

        if ( param < 0 ) {
            xx = x1;
            yy = y1;
        }
        else if ( param > 1 ) {
            xx = x2;
            yy = y2;
        }
        else {
            xx = x1 + param * C;
            yy = y1 + param * D;
        }

        var dx = vertex.x - xx;
        var dy = vertex.y - yy;
        return dx * dx + dy * dy;
    };

    sqrDistanceToEdge( edge: Edge ): number {
        // TODO remove because can't happen in current implementation?
        if ( this.isIntersecting( edge ) ) return 0;
        var d1 = this.sqrDistanceToPoint( edge.startVertex );
        var d2 = this.sqrDistanceToPoint( edge.nextEdge.startVertex );
        var d3 = edge.sqrDistanceToPoint( this.startVertex );
        var d4 = edge.sqrDistanceToPoint( this.nextEdge.startVertex );
        return min( d1, d2, d3, d4 );
    };

    /**
     * Returns the {@code Vertex} both {@code Edges} share, if any.
     *
     * @param toCheck
     *            the {@code Edge} to be checked for a common {@code Vertex}
     * @return the shared {@code Vertex}, if any, otherwise {@code null}
     */
    getTouch( toCheck: Edge ): Vertex {
        if ( !toCheck ) return null;
        else if ( this.startVertex.equals( toCheck.startVertex ) ) return this.startVertex;
        else if ( this.startVertex.equals( toCheck.nextEdge.startVertex ) ) return this.startVertex;
        else if ( this.nextEdge.startVertex.equals( toCheck.startVertex ) ) return this.nextEdge.startVertex;
        else if ( this.nextEdge.startVertex.equals( toCheck.nextEdge.startVertex ) ) return this.nextEdge.startVertex;
        else return null;
    }

    isIntersecting( edge: Edge, proximity?: number ): boolean {
        if ( !proximity ) {
            if ( this.getTouch( edge ) ) return false;
            let o1: Orientation = Triangle.getOrientation( this.startVertex, this.nextEdge.startVertex, edge.startVertex );
            let o2: Orientation = Triangle.getOrientation( this.startVertex, this.nextEdge.startVertex, edge.nextEdge.startVertex );
            let o3: Orientation = Triangle.getOrientation( edge.startVertex, edge.nextEdge.startVertex, this.startVertex );
            let o4: Orientation = Triangle.getOrientation( edge.startVertex, edge.nextEdge.startVertex, this.nextEdge.startVertex );
            // General case
            if ( o1 != o2 && o3 != o4 ) return true;
            else return false;
            // colinear case skipped
        }
        else return this.sqrDistanceToEdge( edge ) < proximity;
    }

    /**
     * Slower method which returns the exact point of collision though. Should
     * be only used if needed ... stolen from
     * http://stackoverflow.com/a/1968345/5767484
     */
    calculateIntersectionPoint( edge1: Edge, edge2: Edge ) {
        let p0_x: number = edge1.startVertex.x;
        let p0_y: number = edge1.startVertex.y;
        let p1_x: number = edge1.nextEdge.startVertex.x;
        let p1_y: number = edge1.nextEdge.startVertex.y;
        let p2_x: number = edge2.startVertex.x;
        let p2_y: number = edge2.startVertex.y;
        let p3_x: number = edge2.nextEdge.startVertex.x;
        let p3_y: number = edge2.nextEdge.startVertex.y;

        let s1_x: number = p1_x - p0_x;
        let s1_y: number = p1_y - p0_y;
        let s2_x: number = p3_x - p2_x;
        let s2_y: number = p3_y - p2_y;
        // TODO: divisor = 0
        let s: number = ( -s1_y * ( p0_x - p2_x ) + s1_x * ( p0_y - p2_y ) ) / ( -s2_x * s1_y + s1_x * s2_y );
        let t: number = ( s2_x * ( p0_y - p2_y ) - s2_y * ( p0_x - p2_x ) ) / ( -s2_x * s1_y + s1_x * s2_y );

        if ( s >= 0 && s <= 1 && t >= 0 && t <= 1 ) {
            // Collision detected
            let i_x: number = p0_x + ( t * s1_x );
            let i_y: number = p0_y + ( t * s1_y );

            return new Vertex( i_x, i_y );
        }
        return null;
    }

    /**
     * Returns the squared length of this {@code Edge}.
     * 
     * @return the squared length of this {@code Edge}
     */
    public getSqrLength(): number {
        return this.startVertex.sqrDistanceToPoint( this.nextEdge.startVertex );
    }


    toString = (): string => {
        var prev = !this.prevEdge ? "null" : this.prevEdge.startVertex;
        var next = !this.nextEdge ? "null" : this.nextEdge.startVertex;
        var twin = !this._twinEdge ? "null" : this._twinEdge.prevEdge.startVertex + " <-- " + this.startVertex + " --> " + this._twinEdge.nextEdge.startVertex;
        if ( debug > 0 ) return prev + " <-- " + this.startVertex + " --> " + next + "\nTwin: " + twin;
        else return prev + " <-- " + this.startVertex + " --> " + next;
    }

    compareTo( toCompareWith: Edge ): number {
        let xComp = this.startVertex.compareTo( toCompareWith.startVertex );
        if ( xComp != 0 ) return xComp;
        else return this.nextEdge.startVertex.compareTo( toCompareWith._nextEdge.startVertex );
    }

    equals( edge: object ): boolean {
        if ( !edge ) return false;
        if ( !( edge instanceof Edge ) ) return false;
        return this.startVertex.equals( edge.startVertex ) && this.nextEdge.startVertex.equals( edge.nextEdge.startVertex );
    }
}


export class Triangle implements GeometricObject {
    private _relatedEdge: Edge;
    private _minX: number;
    private _maxX: number;
    private _minY: number;
    private _maxY: number;
    /**Reference to the containing {@code IngameTriangle]*/
    private container: IngameTriangle;
    //TODO make these immutable somehow
    /**Stored for faster access in {@code equals()} checks etc.*/
    private _vertices: [Vertex, Vertex, Vertex];
    private _edges: [Edge, Edge, Edge];

    get relatedEdge(): Edge { return this._relatedEdge; }
    set relatedEdge( edge: Edge ) { this._relatedEdge = edge; }
    get ingameTriangle(): IngameTriangle { return this.container; }
    set ingameTriangle( ingameTriangle: IngameTriangle ) { this.container = ingameTriangle; }
    get vertices(): [Vertex, Vertex, Vertex] {
        return this._vertices;
    }
    get edges(): [Edge, Edge, Edge] {
        return this._edges;
    }

    constructor( edge1: Edge ) {
        this._relatedEdge = edge1;
        this._relatedEdge.relatedTriangle = this;
        this._minX = min( this.relatedEdge.startVertex.x, this.relatedEdge.nextEdge.startVertex.x, this.relatedEdge.prevEdge.startVertex.x );
        this._maxX = max( this.relatedEdge.startVertex.x, this.relatedEdge.nextEdge.startVertex.x, this.relatedEdge.prevEdge.startVertex.x );
        this._minY = min( this.relatedEdge.startVertex.y, this.relatedEdge.nextEdge.startVertex.y, this.relatedEdge.prevEdge.startVertex.y );
        this._maxY = max( this.relatedEdge.startVertex.y, this.relatedEdge.nextEdge.startVertex.y, this.relatedEdge.prevEdge.startVertex.y );
        //sort for easier comparison
        this._vertices = [this.relatedEdge.startVertex, this.relatedEdge.nextEdge.startVertex, this.relatedEdge.prevEdge.startVertex].
            sort(( v1, v2 ) => v1.compareTo( v2 ) ) as [Vertex, Vertex, Vertex];
        this._edges = [this.relatedEdge, this.relatedEdge.nextEdge, this.relatedEdge.prevEdge].
            sort(( e1, e2 ) => e1.compareTo( e2 ) ) as [Edge, Edge, Edge];
    }

    static of( vertex1: Vertex, vertex2: Vertex, vertex3: Vertex ): Triangle {
        let actualOrientation: Orientation = Triangle.getOrientation( vertex1, vertex2, vertex3 );
        if ( actualOrientation == Orientation.COLINEAR ) return null;
        let v1: Vertex = vertex1;
        let v2: Vertex;
        let v3: Vertex;
        if ( actualOrientation == targetOrientation ) {
            v2 = vertex2;
            v3 = vertex3;
        } else {
            v2 = vertex3;
            v3 = vertex2;
        }
        let v1v2: Edge = new Edge( v1, null, null, null );
        let v2v3: Edge = new Edge( v2, v1v2, null, null );
        let v3v1: Edge = new Edge( v3, v2v3, v1v2, null );
        v1v2.nextEdge = v2v3;
        v1v2.prevEdge = v3v1;
        v2v3.nextEdge = v3v1;
        return new Triangle( v1v2 );
    }

    // idea stolen from
    // http://www.geeksforgeeks.org/check-if-two-given-line-segments-intersect
    static getOrientation( vertex1: Vertex, vertex2: Vertex, vertex3: Vertex ): Orientation {
        // See http://www.geeksforgeeks.org/orientation-3-ordered-points/
        // for details of below formula.
        let val: number = ( vertex2.y - vertex1.y ) * ( vertex3.x - vertex2.x ) -
            ( vertex2.x - vertex1.x ) * ( vertex3.y - vertex2.y );

        if ( val == 0 ) return Orientation.COLINEAR;  // colinear

        /*
         * Canvas (0,0) lies in the upper left corner, while the result is based
         * on the classical point of origin. Won"t make a difference in the
         * checkIntersecting-evaluation though
         */
        return ( val > 0 ) ? Orientation.CLOCKWISE : Orientation.COUNTER_CLOCKWISE;
    }

    drop( cleanLists?: Edge[] ) {
        for ( let edge of this.edges ) {
            edge.drop();
        }
    }

    /**
     * Returns the area enclosed by this {@code Triangle}.
     * @return the area of this {@code Triangle}
     */
    public getArea(): number {
        let v1 = this.vertices[0];
        let v2 = this.vertices[1];
        let v3 = this.vertices[2];
        let part1 = v1.x * ( v2.y - v3.y );
        let part2 = v2.x * ( v3.y - v1.y );
        let part3 = v3.x * ( v1.y - v2.y );
        return Math.abs( part1 + part2 + part3 ) >> 1;
    }


    contains( vertex: Vertex ): boolean {
        if ( !vertex ) return false;
        if ( this._maxX < vertex.x || this._minX > vertex.x
            || this._maxY < vertex.y || this._minY > vertex.y ) return false;


        // Stolen from http://blackpawn.com/texts/pointinpoly

        // Compute vectors
        let v0: Vertex = this.relatedEdge.prevEdge.startVertex.subtractVertex( this.relatedEdge.startVertex );
        let v1: Vertex = this.relatedEdge.nextEdge.startVertex.subtractVertex( this.relatedEdge.startVertex );
        let v2: Vertex = vertex.subtractVertex( this.relatedEdge.startVertex );

        // Compute dot products
        let dot00: number = v0.skalarProduct( v0 );
        let dot01: number = v0.skalarProduct( v1 );
        let dot02: number = v0.skalarProduct( v2 );
        let dot11: number = v1.skalarProduct( v1 );
        let dot12: number = v1.skalarProduct( v2 );

        // Compute barycentric vertexinates
        let invDenom: number = 1 / ( dot00 * dot11 - dot01 * dot01 );
        let u: number = ( dot11 * dot02 - dot01 * dot12 ) * invDenom;
        let v: number = ( dot00 * dot12 - dot01 * dot02 ) * invDenom;

        // Check if point is in triangle
        return ( u >= 0 ) && ( v >= 0 ) && ( u + v < 1 );
    }

    /**
     * Checks whether a {@code Triangle} intersects with this one. A common
     * {@code Vertex} does not count as intersection. Note this only a fast check will be done here,
     * relying on the fact that some situations might not happen due to the way {@code Trianges} are
     * constructed here (via the evaluation of a drawn line). The in-depth check will only happen
     * on backend side.
     *
     * @param toCheck
     *            the {@code Triangle} to be checked for intersection
     * @return {@code true}, if the {@code Triangle} intersects with on this one
     */
    intersectsWithTriangle( triangle: Triangle ): boolean {
        if ( !triangle ) return false;
        if ( this._maxX < triangle._minX || this._minX > triangle._maxX
            || this._maxY < triangle._minY || this._minY > triangle._maxY ) {
            return false;
        }
        if ( this.isNeighbour( triangle ) ) return false;


        for ( let vertex of triangle.vertices ) {
            // Check all vertices of the new triangle: if there a vertex
            // contained in this triangle and this vertex is not an vertex of
            // this triangle, too, then there's an intersection

            for ( let triangleEdge of this.edges ) {
                for ( let toCheckEdge of triangle.edges ) {
                    if ( triangleEdge.isIntersecting( toCheckEdge ) ) return true;
                }
            }
        }
        return false;
    }

    isNeighbour( triangle: Triangle ): boolean {
        if ( !triangle ) return false;
        for ( let edge of triangle.edges ) {
            for ( let ownEdge of this.edges ) {
                if ( ownEdge.getTwinEdge() && ownEdge.getTwinEdge().equals( edge ) ) return true;
            }
        }
        return false;
    }

    /**
     * Returns the {@code Triangle's} centroid.
     * @return the centroid
     */
    getCentroid(): Vertex {
        let x = ( this.vertices[0].x + this.vertices[1].x + this.vertices[2].x ) * 0.333;
        let y = ( this.vertices[0].y + this.vertices[1].y + this.vertices[2].y ) * 0.333;
        return new Vertex( x, y );
    }

    toString = (): string => {
        if ( debug > 0 ) {
            return "Start edge: " + this.relatedEdge + "\nSecond edge: " + this.relatedEdge.nextEdge + "\nThird edge: " + this.relatedEdge.prevEdge;
        } else return this.relatedEdge.startVertex + ":" + this.relatedEdge.nextEdge.startVertex + ":" + this.relatedEdge.prevEdge.startVertex;
    }

    compareTo( toCompareWith: Triangle ): number {
        let ownEdges = this.edges;
        let otherEdges = toCompareWith.edges;
        let xComp1 = ownEdges[0].compareTo( otherEdges[0] );
        if ( xComp1 != 0 ) return xComp1;
        let xComp2 = ownEdges[1].compareTo( otherEdges[1] );
        if ( xComp2 != 0 ) return xComp2;
        else return ownEdges[2].compareTo( otherEdges[2] );
    }

    equals( triangle: object ): boolean {
        if ( !( triangle instanceof Triangle ) ) return false;
        let ownVertices = this.vertices;
        let otherVertices = triangle.vertices;
        return ( ownVertices[0].equals( otherVertices[0] ) && ownVertices[1].equals( otherVertices[1] ) && ownVertices[2].equals( otherVertices[2] ) );
    }
}

/**
 * Takes vertices and builds edges from them. All edges will be linked in their
 * order and the first with the last one.
 * 
 * @returns array of created edges
 */
export function verticesToEdges( ...vertices: Vertex[] ) {
    if ( vertices.length < 3 ) return;
    let edges: Edge[] = [];
    for ( var i = 0; i < vertices.length; i++ ) {
        edges = edges.concat( new Edge( vertices[i], null, null ) );
    }
    for ( let i = 1; i < edges.length - 1; i++ ) {
        edges[i].prevEdge = edges[i - 1];
        edges[i].nextEdge = edges[i + 1];
    }
    edges[0].nextEdge = edges[1];
    edges[0].prevEdge = edges[edges.length - 1];
    edges[edges.length - 1].nextEdge = edges[0];
    edges[edges.length - 1].prevEdge = edges[edges.length - 2];
    return edges;
}