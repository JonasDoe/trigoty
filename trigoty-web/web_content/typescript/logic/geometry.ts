import { Vertex, Edge, Triangle } from "../logic/geometric_objects";
import { debug, epsilon, minRangeMultiplier } from "../run/main";
import { Canvas } from "../board_handling/canvas";
import { BoardState } from "../board_handling/board_state";

/**
 * Holds functionality to create {@code Triangles} based on the user input and the current {@code BoardState}.
 */
export class Geometry {
    private canvas: Canvas;

    /**
     * Creates a new {@Geometry} instance.
     * @param canvas mostly for debug, allows highlighting some elements
     */
    constructor( canvas?: Canvas ) {
        this.canvas = canvas;
    }

    /**
     * Creates a preview {@code Triangle} based on the line the player is drawing.
     * Note that no in-depth validation for costs or collisions with {@code Triangles} will be taken
     * into account here and there must be done separatly, if required. 
     * This {@code Triangle} will use the first intersected edge as well as any second one
     * (if both are connected and therefore form two sides of the {@code Triangle}. 
     */
    createPreviewTriangle( mouseStart: Vertex, mouseEnd: Vertex, state: BoardState ): Triangle {
        if ( !mouseStart ) {
            return null;
        }
        let involvedEdges: EdgeIntersections = this.getEdgeIntersectionInfo(
            new Edge( mouseStart, null, new Edge( mouseEnd, null, null ) ),
            state );
        let ownEdges = involvedEdges.getOwnEnclosingEdges();
        //find first outline edge
        let startEdge: Edge = ownEdges[0];
        // no edges involved or line starts in a foreign triangle
        if ( !startEdge ) {
            return null;
        }
        let closestEdge: Edge = ownEdges[1];

        // if there's only a start edge an the distance between start edge and start edge is to small -> return null to avoid micro triangles
        if ( !closestEdge && startEdge.sqrDistanceToPoint( mouseEnd ) <= epsilon * minRangeMultiplier ) return null;

        if ( startEdge.sqrDistanceToPoint( mouseEnd ) < epsilon ) {
            return null;
        }
        // TODO use verticesToEdges
        let previewTriangle: Triangle;
        let firstEdge: Edge;
        let secondEdge: Edge;
        let thirdEdge: Edge;
        /** the first edge of two: the startEdge and another one, if any */
        let firstConnectedEdge: Edge;

        // If there's no other intersected edge to connect with, search in proxmity
        if ( !closestEdge && epsilon > 0 ) { // TODO apply proximity logic before?
            let inProximity: Edge[] = this.findNearEdges( mouseEnd, state );
            for ( let edge of inProximity ) {
                firstConnectedEdge = this.checkConnection( startEdge, edge );
                if ( firstConnectedEdge ) {
                    closestEdge = edge;
                    // involvedEdges[1] = closestEdge;
                    break;
                }
            }
            // if there is another edge met with does not share a vertex:
            // check if it's vertix is near. if that's the case, connect to
            // this vertex, else don't allow the drawn triangle
            if ( inProximity.length > 0 && !closestEdge ) {
                previewTriangle = this.tryToConnectWithVertex( startEdge, inProximity, state, mouseEnd );
                if ( !previewTriangle ) {
                    return null;
                }
            }
        }
        if ( !previewTriangle ) { // if there's no triangle created yet ...
            // if two outlines involved -> if they are connected, create a triangle
            // from there
            if ( closestEdge ) {
                previewTriangle = this.tryToConnectIntersections( startEdge, closestEdge, state );
            } else {
                firstEdge = Edge.createTwinModel( startEdge );
                secondEdge = new Edge( startEdge.startVertex, firstEdge, null );
                thirdEdge = new Edge( mouseEnd, secondEdge, firstEdge );
                firstEdge.prevEdge = thirdEdge;
                firstEdge.nextEdge = secondEdge;
                secondEdge.nextEdge = thirdEdge;
                previewTriangle = new Triangle( firstEdge );
            }
        }

        // Check whether the triangle intersects with another one. If
        // that's the case deregister all set twins and cancel
        // Create new, registered triangle
        if ( !previewTriangle ) {
            return null;
        }
        return previewTriangle;
    }

    /**
     * Returns all {@code Edges} which are intersected, in the order or their intersection. This intersections might represent the edges of a new {@code Triangle}. 
     * @param drawnEdge a line which will be checked whether it intersects existing {@code Edges}
     * @return the intersecting {@code Edges}, in the order of their intersection
     */
    //FIXME this line is not enough to tell about all triangles which must be deleted
    getEdgeIntersectionInfo( drawnEdge: Edge, state: BoardState ): EdgeIntersections {
        // TODO use player? Maybe: use playerOutline + all edges of other player's triangles 
        // since they're destroyable. Then: return all outlines, orderd, to allow a possible destruction
        // of foreign triangles. More performant: check all enemies' edges as soon as an enemy's outline
        // has been crossed, but not before (=>concat list with all foreing outline edges with those which have a twin)
        let allEdges: Edge[] = state.playerOutline.concat( state.getAllOtherEdges() );
        let startVertex = drawnEdge.startVertex;
        //        if ( debug ) for ( let edge of outline ) edge.draw( this.canvasContext, Color.RED );
        let result: EdgeIntersections = new EdgeIntersections( state.playerId );
        //        if ( debug ) for ( let edge of outline ) edge.draw( this.canvasContext, Color.BLACK );
        for ( let line of allEdges ) {
            //            if ( debug ) line.draw( this.canvasContext, Color.RED );
            if ( drawnEdge.isIntersecting( line ) ) {
                let distance: number = drawnEdge.sqrDistanceToPoint( startVertex );
                result.processEdge( new EdgeAndDistance( line, distance ) );
            }
            //            if ( debug ) line.draw( this.canvasContext, Color.BLACK );
        }
        return result.finalize();
    }

    /**
     * Searches for {@code Edges} close to a given {@code Vertex}. Used to achieve a "magnetism" effect when drawing {@code Triangles} close to existing ones.
     * @param mouseEnd the {@code Vertex} in whose proximity {@code Edges} will be searched
     * @return all {@code Edges} owned by the given player in proximity of the given {@code Vertex}
     */
    findNearEdges( mouseEnd: Vertex, state: BoardState ): Edge[] {
        let inProximity: Edge[] = [];
        for ( let line of state.playerOutline ) {
            //            if ( debug > 0 ) line.draw( this.canvasContext, Color.RED );
            let proximity = line.sqrDistanceToPoint( mouseEnd );
            if ( proximity < epsilon ) inProximity.push( line );
            //            if ( debug > 0 ) line.draw( this.canvasContext, Color.BLACK );
        }
        if ( debug > 0 && this.canvas ) for ( let edge of inProximity ) this.canvas.highlight( edge );
        return inProximity;
    }

    /**
     * Checks whether two edges share a vertex.
     * @param edge1 the first {@code Edge}, checked whether it ist connected with the second one
     * @param edge2 the second {@code Edge}, checked whether it ist connected with the first one
     * @return The first {@code Edge} of the chain, if both {@code Edges} are connected, therwise {@code null}
     */
    checkConnection( edge1: Edge, edge2: Edge ) {
        if ( edge1.startVertex.equals( edge2.nextEdge.startVertex ) ) return edge1;
        else if ( edge1.nextEdge.startVertex.equals( edge2.startVertex ) ) return edge2;
        else return null;
    }

    /**
     * Checks all {@code Triangle} for intersection and returns those.
     * @param triangle to be checked for intersection with any {@code Triangles}
     * @return all {@code Triangles} which intersects with the given one
     */
    intersectingTriangles( triangle: Triangle, state: BoardState ): Triangle[] {
        let collision: Triangle[] = [];
        for ( let edge of state.getAllEdges() ) {
            for ( let triangleEdge of triangle.edges ) {
                if ( triangleEdge.isIntersecting( edge ) ) {//TODO sets would be nice here
                    if ( collision.indexOf( edge.relatedTriangle ) < 0 ) collision.push( edge.relatedTriangle );
                    break;
                }
            }
        }
        return collision;
    }

    /**
     * Tries to form a {@code Triangle} by "magnetism" towards a {@code Vertex}. That means, if possible, 
     * a {@code Triangle} will be created by a given {@code Edge} and two other ones connecting to a {@code Vertex} in proximity.
     * @param startEdge the basis. Might form a {@code Triangle} together with a {@code Vertex} which does not lie on this {@code Edge}. 
     * @param inProximity {@code Edges} whose {@code Vertices} will be checked whether they form a {@code Triangle} together with the given startEdge
     * @return a preview triangle formed by the given basis {@code Edge} and the closest {@code Vertex} in proximity, if any, otherwise {@code null}
     */
    tryToConnectWithVertex( startEdge: Edge, inProximity: Edge[], state: BoardState, mouseEnd: Vertex ): Triangle {
        /** first edge of the the newly created triangle, if any */
        let firstEdge: Edge;
        /** second edge of the the newly created triangle, if any */
        let secondEdge: Edge;
        /** second edge of the the newly created triangle, if any */
        let thirdEdge: Edge;

        let closestCommonVertex: VertexAndDistance;
        /*
         * if (debug) drawCanvas(); if (debug) inProximity[0].draw(Color.RED); if
         * (debug) inProximity[0].draw(Color.BLACK);
         */
        for ( let edge of inProximity ) {
            /*
             * if (debug) edge.draw(Color.RED); if (debug) console.log("edge: " +
             * edge.toString());
             */
            let distance: number = edge.startVertex.sqrDistanceToPoint( mouseEnd );
            if ( distance < epsilon * 2 && ( !closestCommonVertex || closestCommonVertex.distance < distance ) )
                closestCommonVertex = new VertexAndDistance( edge.startVertex, distance );
            else {
                distance = edge.nextEdge.startVertex.sqrDistanceToPoint( mouseEnd );
                if ( distance < epsilon * 2 && ( !closestCommonVertex || closestCommonVertex.distance < distance ) )
                    closestCommonVertex = new VertexAndDistance( edge.nextEdge.startVertex, distance );
            }
            // if (debug) edge.draw(Color.BLACK);
        }
        if ( closestCommonVertex ) {
            // if (debug) console.log("commonVertex: " + commonVertex.toString());
            firstEdge = Edge.createTwinModel( startEdge );
            secondEdge = new Edge( startEdge.startVertex, firstEdge, null );
            thirdEdge = new Edge( closestCommonVertex.vertex, secondEdge, firstEdge );
            firstEdge.prevEdge = thirdEdge;
            firstEdge.nextEdge = secondEdge;
            secondEdge.nextEdge = thirdEdge;
            return new Triangle( firstEdge );
        }
    }

    /**
     * Forms an {@code Triangle} by the two given {@code Edges} which intersected the drawn line.
     * If these two {@code Edges} aren't connected no {@code Triangle} will be formed by this method.
     * Otherwise a {@code Triangle} will be formed. If there is a first edge available which works as third {@code Edge},
     * it will be used as well. 
     * @param involvedEdges the two {@code Edges} who will takes as two sides of a {@code Triangle} if they're are connected
     * @return a preview triangle formed by the two {@code Edges}, if any, otherwise {@code null}
     */
    tryToConnectIntersections( startEdge: Edge, closestEdge: Edge, state: BoardState ): Triangle {
        if ( !( startEdge && closestEdge ) ) return null;
        let firstEdge: Edge;
        let secondEdge: Edge;
        let thirdEdge: Edge;
        let firstConnectedEdge: Edge = this.checkConnection( startEdge, closestEdge )
        // if the are connected, close the gap between them
        if ( debug > 0 && this.canvas ) { this.canvas.highlight( startEdge ); this.canvas.highlight( closestEdge ); };
        if ( !firstConnectedEdge ) return null;
        else if ( startEdge.equals( firstConnectedEdge ) ) {
            firstEdge = Edge.createTwinModel( startEdge );
            secondEdge = Edge.createTwinModel( closestEdge );
            thirdEdge = new Edge( closestEdge.startVertex, secondEdge, firstEdge );
        } else if ( closestEdge.equals( firstConnectedEdge ) ) {
            firstEdge = Edge.createTwinModel( closestEdge );
            secondEdge = Edge.createTwinModel( startEdge );
            thirdEdge = new Edge( startEdge.startVertex, secondEdge, firstEdge );
        } else return null;
        firstEdge.prevEdge = thirdEdge;
        firstEdge.nextEdge = secondEdge;
        secondEdge.prevEdge = firstEdge;
        secondEdge.nextEdge = thirdEdge;
        // check if a hole as been closed, i.e. there is a (yet unlinked) twin edge for the thirdEdge
        // TODO: Alternative: just walk the outline and check for matching twin edge?
        //        let possibleTwinEdge:Edge = this.findExistingTwin( thirdEdge, e => e.nextEdge );
        //        if (possibleTwinEdge) thirdEdge.twinEdge = this.findExistingTwin( thirdEdge, e => e.prevEdge );
        return new Triangle( firstEdge );
    }

    /**
     * Not used atm. Starting from a outline edge, this method collects all edges
     * from the outline.
     * 
     * @param relatedEdge
     *            from the outline
     * @returns array with all {@code Edges} establishing the outline
     */
    calculateOutline( startEdge: Edge, state: BoardState ): Edge[] {
        let outline: Edge[] = [startEdge];
        let currentEdge: Edge = !startEdge.nextEdge.getTwinEdge() ? startEdge.nextEdge : startEdge.nextEdge.getTwinEdge().nextEdge;
        while ( !currentEdge.equals( startEdge ) ) {
            if ( debug && this.canvas ) this.canvas.highlight( currentEdge );
            outline = outline.concat( currentEdge );
            if ( debug && this.canvas ) this.canvas.unhighlight( currentEdge );
            currentEdge = !currentEdge.nextEdge.getTwinEdge() ? currentEdge.nextEdge : currentEdge.nextEdge.getTwinEdge().nextEdge;
        }
        return outline;
    }

    /**
     * Returns the {@code Triangle} containing a given {@code Vertex}.
     * @param vertex a containing {@code Triangle} is requested for
     * @param currentPlayer whose {@code Triangles} shall be checked
     * @param the containing {@code Triangle}, if any, otherwise {@code null}
     */
    //TODO handle collisions with triangles an other way?
    getContainingTriangle( vertex: Vertex, state: BoardState ): Triangle {
        for ( let triangle of state.playerTriangles ) {
            let geometricTriangle = triangle.triangle;
            if (/* triangle.owner == currentPlayer &&*/ geometricTriangle.contains( vertex ) ) return geometricTriangle;
        }
        for ( let triangle of state.otherTriangles ) {
            let geometricTriangle = triangle.triangle;
            if ( geometricTriangle.contains( vertex ) ) return geometricTriangle;
        }
        return null;
    }

    /**
     * Takes vertices and builds edges from them. All edges will be linked in their
     * order and the first with the last one.
     * @param vertices forming {@code Edges} in the order they're passed
     * @returns array of created edges
     */
    verticesToEdges( ...vertices: Vertex[] ): Edge[] {
        if ( vertices.length < 3 ) return;
        let edges: Edge[] = [];
        for ( let i = 0; i < vertices.length; i++ ) {
            edges = edges.concat( new Edge( vertices[i], null, null ) );
        }
        for ( let i = 1; i < edges.length - 1; i++ ) {
            edges[i].prevEdge = edges[i - 1];
            edges[i].nextEdge = edges[i + 1];
        }
        edges[0].nextEdge = edges[1];
        edges[0].prevEdge = edges[edges.length - 1];
        edges[edges.length - 1].nextEdge = edges[0];
        edges[edges.length - 1].prevEdge = edges[edges.length - 2];
        return edges;
    }
}


/**
 * Pure data class. Holds the an {@code Edge} an the distance towards it. Used for calculations of new {@code Triangles}.
 */
class EdgeAndDistance {
    private _edge: Edge;
    private _distance: number;

    get edge(): Edge { return this._edge; }
    get distance(): number { return this._distance; }

    constructor( edge: Edge, distance: number ) {
        this._edge = edge;
        this._distance = distance;
    }
}
/**
 * Pure data class. Holds an {@code Vertex} and the distance towards it. Used for calculations of new {@code Triangles}.
 */
class VertexAndDistance {
    private _vertex: Vertex;

    get vertex(): Vertex { return this._vertex; }
    get distance(): number { return this._distance; }

    private _distance: number;
    constructor( vertex, distance ) {
        this._vertex = vertex;
        this._distance = distance;
    }
}
/**
 * Helper class which tells about the one or two own {@code Edges} which are relevant 
 * to constitute a {@code Triangle} as well as about affected foreign {@code Edges} which might get destroyed. 
 */
class EdgeIntersections {
    //TODO use ImmutableJS - SortedSet for more performance?
    /**All own {@code Edges}, used to determine the one or two relevant ones*/
    private ownInvolvedEdges: EdgeAndDistance[] = [];
    /*The one or two relevant own {@code Edges} to constitute a {@code Triangle}*/
    private ownEnclosingEdges: [EdgeAndDistance, EdgeAndDistance];
    /**All foreing{@code Edges}.*/
    private foreignInvolvedEdges: EdgeAndDistance[] = [];
    /**All foreing {@code Edges} which might be affected if a {@code Triangle} will be created.*/
    private foreignEnclosedEdges: EdgeAndDistance[];
    /**Determines which {@code Edges} count as own ones*/
    private playerId: string;
    /**Tells whether more {@code EdgesAndDistances} can be prcoessed or whether a result has already been calculated*/
    private finalized: boolean = false;
    /**
     * Creates a new {@code EdgeIntersecions}
     * @param process determines which {@code Edges} count as own ones
     */
    constructor( process: string ) {
        this.playerId = process;
    }

    /**
     * Puts another {@code EdgeAndDistance} to the internal data model.
     * @param toProcess to be processed an put to the data model
     * @return {@code this}, for chaining
     * @throws Exception if this instance has already been finalized and therefore can't process any new data.
     */
    public processEdge( toProcess: EdgeAndDistance ): void {
        if ( this.finalized ) throw "Illegal State: already finalized."
        //      if (this.ownInvolvedEdges[0]==null&&this.ownInvolvedEdges[1]==null&&this.playerId!=toRegister.edge.relatedTriangle.owner){
        if ( this.playerId != toProcess.edge.relatedTriangle.ingameTriangle.owner ) this.foreignInvolvedEdges.push( toProcess );
        else this.ownInvolvedEdges.push( toProcess );
    }

    /**
     * Disallows further items to be processed and determines the results.
     * @throws Exception if this instance has already been finalized
     * @return {@code this}, for chaining
     */
    public finalize(): EdgeIntersections {
        if ( this.finalized ) throw "Illegal State: already finalized."
        this.finalized = true;
        if ( !this.getOwnEnclosingEdges() )
            this.ownInvolvedEdges = this.ownInvolvedEdges.sort(( i1: EdgeAndDistance, i2: EdgeAndDistance ) => i1.distance - i2.distance );
        let ownInvolved1: EdgeAndDistance = this.ownInvolvedEdges[0];
        let ownInvolved2: EdgeAndDistance = this.ownInvolvedEdges[1];
        let ownEnclosingEdges: [EdgeAndDistance, EdgeAndDistance] = [ownInvolved1, ownInvolved2];

        if ( !ownInvolved1 ) return this;
        let min = ownInvolved1.distance;
        let max = ownInvolved2 ? ownInvolved2.distance : null;
        let foreignEnclosedEdges;
        try {
            foreignEnclosedEdges = this.foreignInvolvedEdges.map( e => { if ( e.distance < min ) throw ""; else return e } ).
                filter(( e => e.distance >= min && ( !max || e.distance <= max ) ) );
        } catch ( e ) {
            return this; //nasty workaround to stop as soon as its clear the line started not in an own triangle
        }
        this.ownEnclosingEdges = ownEnclosingEdges;
        this.foreignEnclosedEdges = foreignEnclosedEdges;
        return this;
    }
    /**
     * Returns the one or two own {@code Edges} which might constitute a {@code Triangle}.
     * @return the {@code Edge(s)} (or better: whose not-yet-existing-twins) which might constitute a {@code Triangle}.  
     * @throws Exception if this instance has not been finalized yet
     */
    public getOwnEnclosingEdges(): [Edge, Edge] {
        if ( !this.finalized ) throw "Illegal State: not finalized.";
        let e1: EdgeAndDistance = this.ownInvolvedEdges[0];
        let e2: EdgeAndDistance = this.ownInvolvedEdges[1];
        this.ownEnclosingEdges = [e1, e2];
        return [e1 ? e1.edge : null, e2 ? e2.edge : null];
    }
    /**
     * Returns all foreign {@code Edges} which might be destroyed if the {@code Triangle} is created.
     * @return all foreign {@code Edges} which might be destroyed
     * @throws Exception if this instance has not been finalized yet
     */
    public getForeignEnclosedEdge(): Edge[] {
        if ( !this.finalized ) throw "Illegal State: not finalized.";
        return this.foreignEnclosedEdges.map( e => e.edge );
    }
}
