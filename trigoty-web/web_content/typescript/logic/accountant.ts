/**
 * Holds all the logic relating players' resources and costs of actions.
 * Balancing adjustments should be done here (in its backend counterpart).
 */


/** Used to scale down all values */
import { IngameTriangle, PreviewTriangle } from "../board_handling/game_objects";
import { Triangle } from "./geometric_objects";

const AREA_FACTOR = .001;

/**
 * Returns the value of an triangle.
 *
 * @param triangle
 *            to be evaluated.
 * @return the {@code Triangle} value
 */
export function getValueOf( triangle: Triangle | IngameTriangle | PreviewTriangle ): number {
    let t = triangle instanceof Triangle ? triangle : triangle.triangle;
    let trianglePrice = Math.trunc( Math.max( 1, t.getArea() * AREA_FACTOR ) );
    return ( triangle instanceof PreviewTriangle ) ? triangle.collisionCosts + trianglePrice : trianglePrice;
}

/**
 * Returns the income a given triangle supplies.
 *
 * @param triangle
 *            whose supplied income is requested
 * @return the supplied income
 */
export function getTurnIncomeFor( triangle: IngameTriangle | Triangle ): number {
    let t = triangle instanceof IngameTriangle ? triangle : triangle.ingameTriangle;
    // TODO only 32 bit supports bit shift?
    // will become more complex with more factors later on
    return Math.max( 1, t.baseValue >> 2 ); // atm: 25% of base value
}

/**
 * Returns the creation costs of the given triangle.
 *
 * @param triangle
 *            whose creation costs shall be determined
 * @return the creation costs of the given triangle
 */
export function getCostsFor( triangle: Triangle | IngameTriangle | PreviewTriangle ): number {
    return getValueOf( triangle );
}

/**
 * Returns the costs to destroy an triangle.
 * @param triangle whose destruction costs shall be determined
 * @return the costs to destruct the given triangle
 */
export function getDestructionCostsFor( triangle: IngameTriangle ): number {
    return getValueOf( triangle ) + getEffectiveDefense( triangle );
}

/**
 * Returns the costs to upgrade the defense of a given {@code IngameTriangle} by
 * {@code 1}.
 * 
 * @param toUpgrade
 *            whose upgrade costs shall be determined
 * @return the costs to upgrade the defense of the given {@code IngameTriangle}
 *         by {@code 1}
 */
export function getDefenseUpgradeCosts( toUpgrade: IngameTriangle | PreviewTriangle ): number {
    return toUpgrade.baseValue * ( toUpgrade.defense + 1 ) * 4;
}

/**
 * Returns the effective defense value based on a given {@code IngameTriangle's}
 * defense.
 * 
 * @param triangle
 *            howse effective defense value shall be determined
 * @return the effective defense value based on the given
 *         {@code IngameTriangle's} defense
 */
export function getEffectiveDefense( triangle: IngameTriangle ): number {
    return triangle.defense * 10;
}