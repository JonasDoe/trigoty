import { removeFromList } from "../util/shared";
import { NetworkConnector } from "../network/network";
import { Edge, Vertex, Triangle } from "../logic/geometric_objects";
import { debug, epsilon } from "../run/main";
import { BoardState } from "./board_state";
import { Canvas } from "./canvas";
import { Geometry } from "../logic/geometry";
import { IngameTriangle, PreviewTriangle } from "./game_objects";
import { getCostsFor, getDestructionCostsFor, getDefenseUpgradeCosts } from "../logic/accountant";
import { BLACK, RED } from "../util/colors";

/**
 * Displays the {@code Triangles} and allows new ones to be drawn.
 */
//TODO allow more async action: 
//- don't set waitingforrespons except on reset due to failure
//- estimate costs
//- estimate income?
export class CanvasHandler {
    private geoCalc: Geometry;
    //TODO support touch as well
    private mouseStart: Vertex = null;
    private mouseEnd: Vertex = null;
    private canvas: Canvas;
    private board: HTMLCanvasElement;
    private server: NetworkConnector;
    private _state: BoardState = new BoardState();
    private liveInfo: LiveInfo;
    private hoveredTriangle: IngameTriangle;
    public get state(): BoardState {
        return this._state;
    }

    /** Holds the currently drawn {@code Triangle} which is proposed to the player and might be requested from the server*/
    private previewTriangle: PreviewTriangle | IngameTriangle;
    /* Tells whether a triangle was proposed to the server and the response is still pending */
    private waitingForResponse: boolean = true;

    /**
     * Creates a new {@code CanvasHandler}.
     * @param server the connection to the server all communication will be realized with.
     */
    constructor( server: NetworkConnector ) {
        this.server = server;
        //workaround to prevent us from referring to the callback 'this' there
        this.applyServerUpdate = this.applyServerUpdate.bind( this );
        this.mouseDownEvent = this.mouseDownEvent.bind( this );
        this.mouseMoveEvent = this.mouseMoveEvent.bind( this );
        this.mouseUpEvent = this.mouseUpEvent.bind( this );
    }


    /**
     * Initializes this {@code CanvasHandler}.
     * @param board the canvas this handler will work on
     */
    init( board: HTMLCanvasElement ) {
        this.board = board;
        this.canvas = new Canvas( board );
        this.geoCalc = new Geometry( this.canvas );
        this._state.canvas = this.board.getContext( "2d" );
        if ( !this.canvas ) { alert( "Could not get the canvas element!" ); return; }
        // this.canvasContext.translate( 0.5, 0.5 );
        this.board.addEventListener( "mousedown", this.mouseDownEvent );
        this.board.addEventListener( "mouseup", this.mouseUpEvent );
        this.board.addEventListener( "mousemove", this.mouseMoveEvent )
        this.board.addEventListener( "mouseover", () => event.preventDefault() )
        this.liveInfo = new LiveInfo( this.board );
        this.server.registerPlayer();
    }

    /**
     * Finalizies the registration of this board an fetches the initial information. This method is supposed to be invoked by a server's command.
     * @param name the player is registered with at the server
     */
    register( name: string, toCreate: IngameTriangle[] ) {
        this._state.playerId = name;
        this.applyServerReset( toCreate );
    }

    /**
     * Creates a new {@code Triangle} and registers it for the owner. Note that the outline won't be updated here.
     * @param relatedEdge which forms the {@code Triangle} together with its predecessor and successor
     * @param owner the {@code Triangle} belongs to and will be registered for
     */
    private registerTriangle( toRegister: IngameTriangle ): void {
        this.state.getTrianglesByOwner( toRegister.owner ).push( toRegister );
    }

    /**
     * Unregisters a given {@code Triangle} from neighbors and other fields. Note that the outline won't be updated here.
     * @param toRemove the {@code Triangle} to be dropped and unregistered
     */
    private dropAndUnregisterTriangle( toRemove: IngameTriangle ): void {
        for ( let edge of toRemove.triangle.edges ) {
            let twin = edge.getTwinEdge();
            if ( twin ) this.state.playerOutline.push( twin );
        }
        removeFromList( toRemove, this.state.getTrianglesByOwner( toRemove.owner ) );
        toRemove.drop();
    }

    /**
     * Starts the process which creates/drawes a new {@code Triangle}.
     * @param e the mouse event triggering this method
     */
    private mouseDownEvent( e: MouseEvent ): void {
        event.preventDefault();
        if ( this.waitingForResponse ) return;
        this.mouseStart = new Vertex( e.pageX - this.board.offsetLeft, e.pageY - this.board.offsetTop );
        if ( !this.geoCalc.getContainingTriangle( this.mouseStart, this._state ) ) {
            this.mouseStart = null;
            this.mouseEnd = null;
            return;
        }
        this.mouseEnd = null;
    }

    /**
     * Processes the current drawn line to calculate a new {@code Triangle}.
     * @param e the mouse event triggering this method
     */
    private mouseMoveEvent( e: MouseEvent ): void {
        event.preventDefault();
        if ( this.waitingForResponse ) return;
        if ( !this.mouseStart ) {
            this.hoveredTriangle = this.getContainingTriangle( new Vertex( e.pageX - this.board.offsetLeft, e.pageY - this.board.offsetTop ) )
            if ( this.hoveredTriangle && this.hoveredTriangle.owner === this.state.playerId ) this.updateUpgradeTriangleLiveInfos();
            return;
        } else this.hoveredTriangle = null; //TODO count as click even when mouseStart exists, as long as it is close by an in the same triangle?
        this.canvas.drawCanvas( this.state );
        this.mouseEnd = new Vertex( e.pageX - this.board.offsetLeft, e.pageY - this.board.offsetTop );
        if ( debug == 2 ) {
            this.canvas.drawLine( this.mouseStart, this.mouseEnd );
        } else {
            this.previewTriangle = IngameTriangle.of( this.geoCalc.createPreviewTriangle( this.mouseStart, this.mouseEnd, this._state ), this._state.playerId );
            if ( this.previewTriangle ) {
                if ( this.validatePreviewTriangle() )
                    this.canvas.draw( this.previewTriangle );
            }
        }
        this.updatePreviewTriangleLiveInfos();
    }

    /**
     * Validates a given preview triangle and update information about the costs etc.
     */
    private validatePreviewTriangle(): boolean {
        //TODO is all here the canvas handler's job to determine?
        let collisions: Triangle[] = this.geoCalc.intersectingTriangles( this.previewTriangle.triangle, this.state );
        let destructionCosts = 0;
        for ( let collision of collisions ) {
            if ( collision.ingameTriangle.owner === this.state.playerId ) return false;
            destructionCosts += getDestructionCostsFor( collision.ingameTriangle );
        }
        if ( this.previewTriangle instanceof IngameTriangle )
            this.previewTriangle = new PreviewTriangle( this.previewTriangle );
        this.previewTriangle.collisionCosts = destructionCosts;
        this.updatePreviewTriangleLiveInfos();
        return true;
    }

    /**
     * Checks whether a valid {@code Triangle} can be created from the drawn line. If that's the case, the new {@code Triangle} will be created.
     * @param e the mouse event triggering this method
     */
    private mouseUpEvent( e: MouseEvent ): void {
        event.preventDefault();
        this.liveInfo.setHidden( true );
        if ( this.waitingForResponse ) return;
        if ( !this.mouseEnd ) this.mouseEnd = new Vertex( e.pageX - this.board.offsetLeft, e.pageY - this.board.offsetTop );
        if ( this.hoveredTriangle && this.hoveredTriangle.owner === this.state.playerId ) {
            //check whether start was in the same triangle?
            if ( this.hoveredTriangle.defense < IngameTriangle.MAX_DEFENSE ) {
                let upgradeProposal = new PreviewTriangle( this.hoveredTriangle );
                let upgradeCosts = getDefenseUpgradeCosts( this.hoveredTriangle );
                if ( upgradeCosts <= this.state.balance ) {
                    upgradeProposal.defense++;
                    this.server.proposeTriangle( upgradeProposal.ingameTriangle );
                    this.state.balance -= upgradeCosts; //rejected upgrade costs reset automatically
                }
            }
            this.mouseStart = null;
            this.mouseEnd = null;
            return;
        }

        if ( debug == 2 ) {
            this.previewTriangle = IngameTriangle.of( this.geoCalc.createPreviewTriangle( this.mouseStart, this.mouseEnd, this._state ), this._state.playerId );
            if ( this.previewTriangle ) this.canvas.draw( this.previewTriangle );
        }
        if ( debug ) this.previewTriangle = IngameTriangle.of( this.geoCalc.createPreviewTriangle( this.mouseStart, this.mouseEnd, this._state ), this._state.playerId );
        if ( this.previewTriangle ) {
            let creationCosts = getCostsFor( this.previewTriangle );
            if ( creationCosts <= this.state.balance ) {
                this.server.proposeTriangle( this.previewTriangle instanceof PreviewTriangle ? this.previewTriangle.ingameTriangle : this.previewTriangle );
                this.state.balance -= creationCosts; //rejected createion costs reset automatically
            }
            this.canvas.drawCanvas( this.state );
            this.previewTriangle = null;
        }
        this.mouseStart = null;
        this.mouseEnd = null;
    }

    /**
     * Removes and adds the specified {@code Triangles}. This method is supposed to be invoked by a server's command.
     * @param toRemove {@code Triangles} which shall be removed. Will be processed first.
     * @param toAdd {@code Triangles} which shall be added. Will be processed second and therefore should not rely {@code Triangles} ought to be removed.
     */
    applyServerUpdate( toRemove: IngameTriangle[], toAdd: IngameTriangle[] ): void {
        for ( let remove of toRemove ) {
            let success: boolean = this.removeServerTriangle( remove );
            if ( !success ) {
                this.server.requestCompleteState();
                this.waitingForResponse = true;
                return;
            }
        }
        for ( let add of toAdd ) {
            let success: boolean = this.addServerTriangle( add );
            if ( !success ) {
                this.server.requestCompleteState();
                this.waitingForResponse = true;
                return;
            }
        }
        this.finalizeResponseHandling()
    }

    /**
     * Resets the state and replaces it with the given {@code Triangles}. This method is supposed to be invoked by a server's command.
     * @param toCreate {@code Triangles} constituting the new state. They will be constructed in the given order. 
     */
    applyServerReset( toCreate: IngameTriangle[] ): void {
        this._state.playerTriangles = [];
        this._state.otherTriangles = [];
        this._state.playerOutline = [];
        this._state.otherOutline = [];
        for ( let nextToCreate of toCreate ) {
            let success: boolean = this.addServerTriangle( nextToCreate );
            if ( !success ) {
                alert( "Reset failed, stopping game." );
                return;
            }
        }
        this.finalizeResponseHandling();
    }

    /**Cancels any blockings of user actions*/
    resume(): void {
        this.finalizeResponseHandling();
    }

    /**
     * Sets the players account to the given balance.
     * @param balance the account shall be set to
     */
    setAccount( balance: number ) {
        this.state.balance = balance;
        if ( this.mouseStart ) this.updatePreviewTriangleLiveInfos();
        else this.updateUpgradeTriangleLiveInfos();
    }

    /**
     * Returns {@code IngameTriangle} containing the {@code Vertex} (most likely the mouse position)
     * @param vertex the mouse position a matching {@code IngameTriangle} will be searched for
     * @return the {@code IngameTriangle} containing the given {@code Vertex}
     */
    private getContainingTriangle( vertex: Vertex ): IngameTriangle {
        for ( let triangle of this.state.playerTriangles )
            if ( triangle.triangle.contains( vertex ) ) return triangle;
        for ( let triangle of this.state.otherTriangles )
            if ( triangle.triangle.contains( vertex ) ) return triangle;
        return null;
    }

    /**
     * Updates teh live infos based on the triangle the mouse if hovering over
     */
    //TODO refactor with the one below ... put a method in LiveInfo?
    private updateUpgradeTriangleLiveInfos(): void {
        if ( this.hoveredTriangle && this.hoveredTriangle.defense < IngameTriangle.MAX_DEFENSE ) {
            let center = this.hoveredTriangle.triangle.getCentroid();
            let costs = getDefenseUpgradeCosts( this.hoveredTriangle );
            this.liveInfo.setColor( costs <= this.state.balance ? BLACK.val() : RED.val() );
            this.liveInfo.setHidden( false );
            this.liveInfo.setActionInfo( "Upgrade defense", costs );
            this.liveInfo.moveTo( center );
        } else this.liveInfo.setHidden( true );
    }

    /**
     * Updates the live infos based on the preview triangle.
     */
    private updatePreviewTriangleLiveInfos(): void {
        if ( this.previewTriangle ) {
            let center = this.previewTriangle.triangle.getCentroid();
            let costs = getCostsFor( this.previewTriangle );
            this.liveInfo.setColor( costs <= this.state.balance ? BLACK.val() : RED.val() );
            this.liveInfo.setHidden( false );
            this.liveInfo.setActionInfo( "Create triangle", costs );
            this.liveInfo.moveTo( center );
        } else this.liveInfo.setHidden( true );
    }

    /**
     * Removes a given {@code Triangle} from the board.
     * @param toRemove {@code Triangle} to be removed
     * @return {@code true} if the {@code Triangle} was removed successfully, otherwise {@code false}
     */
    private removeServerTriangle( toRemove: IngameTriangle ): boolean {
        let existing: IngameTriangle = this.findExistingTriangle( toRemove )
        if ( !existing ) {
            return false;
        }
        let newOutline: Edge[] = toRemove.triangle.edges.filter( t => t.getTwinEdge() ).map( t => t.getTwinEdge() );
        this.updateOutline( toRemove.owner, toRemove.triangle.edges, newOutline );
        this.dropAndUnregisterTriangle( existing );
        return true;
    }

    /**
     * Creates a {@code Triangle} received from the server or updates a existing one. 
     * @param assignedTriangle the {@code Triangle} received from the server
     * @return {@code true} if the {@code Triangle} was successfully added or was used for an update,
     * otherwise {@code false}  
     */
    private addServerTriangle( assignedTriangle: IngameTriangle ): boolean {
        if ( !assignedTriangle || !assignedTriangle.owner || !assignedTriangle.triangle.relatedEdge
            || assignedTriangle.triangle.edges.find( e => !e || !e.prevEdge || !e.nextEdge ) ) {
            this.finalizeResponseHandling();
            return false;
        }

        let existing = this.findExistingTriangle( assignedTriangle )
        if ( existing ) {
            existing.updateAttributesWith( assignedTriangle );
            return true;
        }


        let toRemove: Edge[] = new Array();
        //find proper twin edges in old outline
        let outlines = this.state.getMergedOutlines();
        for ( let outlineEdge of outlines ) {
            for ( let newEdge of assignedTriangle.triangle.edges ) {
                if ( newEdge.isProperTwinFor( outlineEdge ) ) {
                    if ( assignedTriangle.owner != outlineEdge.relatedTriangle.ingameTriangle.owner ) {
                        this.finalizeResponseHandling();
                        return false;
                    } else {
                        newEdge.linkAsTwins( outlineEdge );
                        toRemove.push( outlineEdge );
                    }
                }
            }
        }


        let toAdd: Edge[] = new Array();
        for ( let edge of assignedTriangle.triangle.edges ) {
            if ( !edge.getTwinEdge() ) toAdd.push( edge );
        }

        //do not check for floating triangles anymore since this can happen by deletions
        // let trianglesOfPlayer: Triangle[] = this.state.getTrianglesByOwner( assignedTriangle.owner ).filter( t => t.owner === assignedTriangle.owner );
        // if ( trianglesOfPlayer.length > 0 && toAdd.length > 2 ) { // illegal: triangle is not connected
        //     this.finalizeResponseHandling();
        //     return false;
        // }
        this.registerTriangle( assignedTriangle );
        this.updateOutline( assignedTriangle.owner, toRemove, toAdd );
        this.finalizeResponseHandling();
        return true;
    }

    /**
     * Refreshes the canvas and allows further player actions.
     */
    private finalizeResponseHandling() {
        this.canvas.drawCanvas( this.state );
        this.waitingForResponse = false;
    }


    //TODO drop this.previewTriangle on failure to create a new one ... still required?


    /**
     * Highlights the polygon's outline of a given player
     * @param player whose polygon outline shall be highlighted
     */
    highlightOutline( player: string ) {
        for ( let edge of this._state.playerOutline ) {
            this.canvas.highlight( edge );
        }
    }

    /**
     * Reverts the higlighing of a player's polygon outline
     * @param player whose polygon outline shall be unhighlighted
     */
    unhighlightOutline( player: string ) {
        for ( let edge of this._state.playerOutline ) {
            this.canvas.unhighlight( edge );
        }
    }

    /**
     * Updates the outline of a player's polygon by {@code Edges} which shall be removed or added
     * @param player whose polygon outline shall be updated
     * @param toRemove {@code Edges} which will be removed from the player's polygon outline
     * @param toAdd {@code Edges} which will be added to the player's polygon outline
     */
    private updateOutline( player: string, toRemove: Edge[], toAdd: Edge[] ) {
        // could be optimized by utilizing the splicing option ...
        let outline: Edge[] = player === this._state.playerId ? this._state.playerOutline : this._state.otherOutline;
        for ( let edge of toRemove ) removeFromList( edge, outline );
        if ( player === this._state.playerId ) this._state.playerOutline = outline.concat( toAdd );
        else this._state.otherOutline = outline.concat( toAdd );
    }

    /**
     * Starting from a given {@code Edge} an existing twin {@code Edge} candidate will be searched.
     * @param edge a twin {@code Edge} shall be searched for
     * @param nextEdgeSelectionFunction describes how the mesh of {@code Edges} will be travered to find a twin.
     * Expected to either return the previous or the next connected {@code Edge} to work properly.
     * @return the matching twin {@code Edge} candidate, if any, otherwise {@code null}
     */
    private findExistingTwin( edge: Edge, nextEdgeSelectionFunction: ( Edge ) => Edge ): Edge {
        // if (debug) for (triangle of triangles) console.log(triangle.toString());
        this.unhighlightOutline( this._state.playerId );
        /*
         * if (debug){ edge.draw(Color.RED); edge.draw(Color.BLACK); }
         */
        let currentEdge: Edge = nextEdgeSelectionFunction.call( this, edge );
        if ( !currentEdge ) return null;
        /*
         * if (debug && currentEdge) currentEdge.draw(Color.RED); if (debug &&
         * currentEdge) currentEdge.draw(Color.BLACK);
         */
        while ( !edge.isProperTwinFor( currentEdge ) ) {
            if ( !currentEdge.getTwinEdge() ) return null;
            currentEdge = nextEdgeSelectionFunction.call( this, currentEdge.getTwinEdge() );
            //            if ( debug && currentEdge ) {
            //                this.canvas.highlight(currentEdge);
            //                 console.log(currentEdge.toString());
            //                this.canvas.unhighlight(currentEdge);
            //            }
        }
        return currentEdge;
    }


    /**
     * Searches for an existing {@code Triangle} which matches the given one's geometry and owner.
     * @param findExistingFor {@code Triangle} a existing one shall be searched for
     * @return an matching existing {@code Triangle}, if any, otherwse {@code null}
     */
    private findExistingTriangle( findExistingFor: IngameTriangle ): IngameTriangle {
        for ( let otherTriangle of this.state.getTrianglesByOwner( findExistingFor.owner ) ) {
            if ( findExistingFor.equals( otherTriangle ) ) return otherTriangle;
        }
        return null;
    }
}

/**
 * The field displaying live information.
 */
class LiveInfo {
    private liveField: HTMLDivElement;
    private verboseField: HTMLDivElement;
    offsetX: number;
    offsetY: number;
    board: HTMLCanvasElement;
    constructor( board: HTMLCanvasElement ) {
        let anchor = document.getElementById( "offset_anchor" ) as HTMLDivElement;
        this.verboseField = document.getElementById( "liveinfo" ) as HTMLDivElement;
        let liveField = document.createElement( "div" ) as HTMLDivElement;
        liveField.style.background = "transparent";
        liveField.style.color = "black";
        liveField.style.position = "absolute";
        liveField.style.userSelect = "none";
        liveField.style.pointerEvents = "none";
        liveField.addEventListener( "mouseup", () => event.preventDefault() );
        liveField.addEventListener( "mousedown", () => event.preventDefault() );
        liveField.addEventListener( "mousemove", () => event.preventDefault() );
        liveField.addEventListener( "click", () => event.preventDefault() );
        liveField.addEventListener( "mouseover", () => event.preventDefault() )
        document.body.insertBefore( liveField, board );
        this.liveField = liveField;
        this.board = board;
        this.offsetY = anchor.offsetTop;
        this.offsetX = anchor.offsetLeft;
        liveField.hidden = true;
    }
    setHidden( hide: boolean ): void {
        this.liveField.hidden = hide;
    }
    isHidden(): boolean {
        return this.liveField.hidden;
    }
    setText( text: any ): void {
        this.liveField.textContent = String( text );
        this.verboseField.textContent = String( text );
    }

    setActionInfo( action: string, costs: number ): void {
        this.liveField.textContent = String( costs );
        this.verboseField.textContent = action + ": " + String( costs );
    }
    getText(): string {
        return this.liveField.textContent;
    }
    setColor( color: string ): void {
        this.liveField.style.color = color;
    }
    //TODO still not perfect positioning
    moveTo( center: Vertex ): void {
        let posX = center.x - this.offsetX + "px";
        let poxY = center.y - this.offsetY + "px";
        let trans = "translate(" + posX + "," + poxY + ")";
        this.liveField.style.transform = trans;
    }
}