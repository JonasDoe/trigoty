/**
 * Represents a geometric {@code Triangle} wrapped with some game relevant information.  
 */
import { Triangle } from "../logic/geometric_objects";
import { BoardState } from "./board_state";
import { StandardObject } from "../util/shared";

export class IngameTriangle implements StandardObject {
    /**The maximum defense for a {@code IngameTriangle}*/
    static readonly MAX_DEFENSE = 10;
    /** The base value to calculate effect impacts with */
    private _baseValue: number;
    /**Geometric {@code Triangle} held by this instance*/
    private _triangle: Triangle;
    /**Player who owns this {@code IngameTriangle}*/
    private _owner: string;
    /**Influences the enemies costs do destroy this {@code IngameTriangle}*/
    private _defense: number = 0;
    get baseValue(): number { return this._baseValue; }
    set baseValue( baseValue: number ) { this._baseValue = baseValue; }
    get triangle(): Triangle { return this._triangle; }
    set triangle( triangle: Triangle ) { this._triangle = triangle; }
    get owner(): string { return this._owner; }
    set owner( owner: string ) { this._owner = owner; }
    get defense(): number { return this._defense; }
    set defense( defenseValue: number ) { if ( defenseValue <= IngameTriangle.MAX_DEFENSE ) this._defense = defenseValue; }

    /**
     * Creates a new {@code IngameTriangle}. Private to make sure it can be only called from the factory method
     * which ensures the requrired side effects.
     * @param innerTriangle to be held by this instance
     * @param owner the player who onws this {@code IngameTriangle}
     */
    private constructor( innerTriangle: Triangle, owner?: string ) {
        this._triangle = innerTriangle;
        this._owner = owner;
    }

    /**
     * Creates an returns a new {@code IngameTriangle}. Links the inner {@code Triangle} to this instance.
     * @param innerTriangle to be held by this instance
     * @param owner the player who onws this {@code IngameTriangle}
     */
    static of( innerTriangle: Triangle, owner: string ) {
        if ( !innerTriangle ) return null;
        let toReturn = new IngameTriangle( innerTriangle, owner );
        toReturn.triangle.ingameTriangle = toReturn;
        return toReturn;
    }

    /**
     * Removes all references of the held inner {@code Triangle}.
     */
    drop(): void {
        this.triangle.drop();
    }

    /**
     * Replaces the values of the {@IngameTriangle's} attributes with the values from the given one.
     * Note that the underlying geometric {@code Triangle} won't be replaced.
     * @param replacement whose attributes shall replace the current ones
     * @return {@code true} if anything was updated, otherise {@code false}
     */
    updateAttributesWith( replacement: IngameTriangle ): boolean {
        let changed: boolean = false;
        if ( this.owner != replacement.owner ) { changed = true; this.owner = replacement.owner; }
        if ( this.baseValue != replacement.baseValue ) { changed = true; this.baseValue = replacement.baseValue; }
        if ( this.defense != replacement.defense ) { changed = true; this.defense = replacement.defense; }
        return changed;
    }

    equals( other: any ): boolean {
        if ( other instanceof IngameTriangle ) {
            return ( this.triangle.equals( other.triangle ) && this.owner === other.owner );
        } else return false;
    }

    toString(): string {
        return this.owner + ": " + this.triangle;
    }
}

/**
 * Holds an {@code IngameTriangle} as well as additional infos required to display live feedback.
 */
export class PreviewTriangle implements StandardObject {
    private _ingameTriangle: IngameTriangle;
    private _collisionCosts: number = 0;
    /**Not the real defense of the {@code IngameTriangle} but the defense of the triangle which might proposed to the server*/
    private _defense: number;
    get ingameTriangle(): IngameTriangle { return this._ingameTriangle; }
    //    set ingameTriangle( ingameTriangle: IngameTriangle ) { this._ingameTriangle = ingameTriangle; }
    get collisionCosts(): number { return this._collisionCosts; }
    set collisionCosts( collisionCosts: number ) { this._collisionCosts = collisionCosts; }
    get triangle(): Triangle { return this._ingameTriangle.triangle; }
    get defense(): number { return this._defense; }
    set defense( defenseValue: number ) { this._defense = defenseValue; }
    get baseValue(): number { return this._ingameTriangle.baseValue; }
    get owner(): string { return this._ingameTriangle.owner };
    //    set baseValue( baseValue: number ) { this._ingameTriangle.baseValue = baseValue; }

    constructor( ingameTriangle: IngameTriangle ) {
        this._ingameTriangle = ingameTriangle;
        this._defense = ingameTriangle.defense;
    }

    equals( obj: object ): boolean {
        if ( !( obj instanceof PreviewTriangle ) ) return false;
        if ( this == obj ) return true;
        return this._ingameTriangle.equals( obj._ingameTriangle );
    }

    toString(): string {
        return this._ingameTriangle.toString();
    }
}

