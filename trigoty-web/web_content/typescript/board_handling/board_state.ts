import { Edge } from "../logic/geometric_objects";
import { CanvasHandler } from "./canvas_handler";
import { IngameTriangle } from "./game_objects";
import { Color, GREEN, RED } from "../util/colors";

/**
 * Data class which holds the state of the board.
 */
export class BoardState {
    /**The player's id, received from the server*/
    playerId: string;
    /**The color representing the player*/
    playerColor: Color = GREEN;
    /**The color representing the other players*/
    otherColor: Color = RED;
    /**{@code Triangles} belong to the player */
    playerTriangles: IngameTriangle[] = [];
    /**{@code Triangles} belong to the other players */
    otherTriangles: IngameTriangle[] = [];
    /** The the outlines of the player's {@code Triangles} */
    playerOutline: Edge[] = [];
    /** The the outlines of all other players' {@code Triangles} */
    otherOutline: Edge[] = [];
    /** Should be only accessed for debugging, i.e. highlighting*/
    canvas: CanvasRenderingContext2D;
    /** Money of the player.*/
    private _balance: number = 0;
    get balance(): number { return this._balance }
    set balance( balance: number ) { this._balance = balance; }


    /**
     * Returns all {@code Edges} from all players that represent an outline.
     * @return List of all {@code Edges} belonging to an outline
     */
    getMergedOutlines(): Edge[] {
        return this.playerOutline.concat( this.otherOutline );
    }

    /**
     * Returns all {@code Triangles} belonging to the player or all {@code Triangles} belonging to the other players,
     * depending on the requested owner.
     * @param owner used to determine whethere the player's {@code Triangles} or the others' {@code Triangles} shall be returned.
     * @return the {@code Triangles}
     */
    getTrianglesByOwner( owner: string ): IngameTriangle[] {
        return owner === this.playerId ? this.playerTriangles : this.otherTriangles;
    }

    /**
     * Returns all existing {@code Edges}.
     * @return all existing {@code Edges}.
     */
    getAllEdges( owner?: string ): Edge[] {
        let list1 = ( !owner || owner === this.playerId ) ? this.playerTriangles : [];
        let list2 = !owner ? this.otherTriangles : [];
        return list1.concat( list2 ).map( t => t.triangle.edges ).reduce(( x, y ) => x.concat( y ), [] );
    }

    getAllOtherEdges(): Edge[] {
        return this.otherTriangles.map( t => t.triangle.edges ).reduce(( x, y ) => x.concat( y ), [] );
    }

}