import { Vertex, Edge, GeometricObject, Triangle } from "../logic/geometric_objects";
import { Color } from "../util/colors";
import { RED } from "../util/colors";
import { debug } from "../run/main";
import { BoardState } from "./board_state";
import { IngameTriangle, PreviewTriangle } from "./game_objects";
import { StandardObject } from "../util/shared";

/**
 * Draws the elements.
 */
export class Canvas {
    private canvasContext: CanvasRenderingContext2D;
    private canvas: HTMLCanvasElement;
    private backgroundColor: string | CanvasGradient | CanvasPattern;
    private lineColor: string | CanvasGradient | CanvasPattern;
    private highlightColor: Color;

    constructor( canvas: HTMLCanvasElement, highlightColor?: Color ) {
        this.canvasContext = canvas.getContext( "2d" );
        this.canvas = canvas;
        this.backgroundColor = this.canvasContext.fillStyle;
        this.lineColor = this.canvasContext.strokeStyle;
        this.highlightColor = highlightColor ? highlightColor : RED;
    }

    /**
     * Cleans and redraws the whole canvas.
     */
    drawCanvas( state: BoardState ): void {
        this.canvasContext.clearRect( 0, 0, this.canvas.width, this.canvas.height ); //use backgroundColor?
        for ( let triangle of state.playerTriangles ) {
            this.fill( triangle, state.playerColor );
        }
        for ( let triangle of state.otherTriangles ) {
            this.fill( triangle, state.otherColor );
        }
    }

    draw( toDraw: any, color?: Color ): void {
        if ( toDraw instanceof Vertex ) {
            this.canvasContext.fillRect( toDraw.x, toDraw.y, 1, 1 );
        } else if ( toDraw instanceof Edge ) {
            let oldColor = this.canvasContext.strokeStyle;
            if ( color ) this.canvasContext.strokeStyle = color.val();
            //            this.canvasContext.lineWidth = this.highlightColor == color && debug > 0 ? 5 : 1;
            this.canvasContext.beginPath();
            this.canvasContext.moveTo( toDraw.startVertex.x, toDraw.startVertex.y );
            this.canvasContext.lineTo( toDraw.nextEdge.startVertex.x, toDraw.nextEdge.startVertex.y );
            this.canvasContext.stroke();
            this.canvasContext.strokeStyle = oldColor;
        } else if ( toDraw instanceof Triangle ) {
            for ( let edge of toDraw.edges ) this.draw( edge, color );
        } else if ( toDraw instanceof IngameTriangle || toDraw instanceof PreviewTriangle ) {
            this.draw( toDraw.triangle, color );
        }
    }

    fill( toFill: Triangle | IngameTriangle | PreviewTriangle, fillColor: Color ): void {
        let strength = toFill instanceof Triangle ? 0 : toFill.defense;
        this.canvasContext.fillStyle = fillColor.val( strength );
        let vertices = toFill instanceof Triangle ? toFill.vertices : toFill.triangle.vertices;
        this.canvasContext.beginPath();
        this.canvasContext.moveTo( vertices[0].x, vertices[0].y );
        this.canvasContext.lineTo( vertices[1].x, vertices[1].y );
        this.canvasContext.lineTo( vertices[2].x, vertices[2].y );
        this.canvasContext.closePath();
        this.canvasContext.stroke();
        this.canvasContext.fill();
    }

    drawLine( from: Vertex, to: Vertex ): void { //TODO similar to draw edge
        this.canvasContext.moveTo( from.x, from.y );
        this.canvasContext.lineTo( to.x, to.y );
        this.canvasContext.stroke();
    }

    highlight( toDraw: GeometricObject ): void {
        if ( toDraw instanceof Vertex ) {
            var oldColor = this.canvasContext.fillStyle;
            this.canvasContext.fillStyle = this.highlightColor.val();
            this.canvasContext.beginPath();
            this.canvasContext.arc( toDraw.x, toDraw.y, 5, 0, 2 * Math.PI );
            this.canvasContext.fill();
            this.canvasContext.fillStyle = oldColor;
        } else if ( toDraw instanceof Edge ) {
            this.draw( toDraw, this.highlightColor );
        } else if ( toDraw instanceof IngameTriangle ) {
            this.draw( toDraw, this.highlightColor );
        }
    }

    unhighlight( toUnhighlight: GeometricObject ): void {
        if ( toUnhighlight instanceof Vertex ) {
            throw "Unhighlighing vertices is not supported."
        } else this.draw( toUnhighlight );
    }

}