/*
 * Handles the (de-)serialization of objects.
 */

/*
 * OBJECT SERIALIZATION
 */

import { Vertex, Triangle } from "../logic/geometric_objects";
import { IngameTriangle } from "../board_handling/game_objects";

export class VertexBean {
    public x: number;
    public y: number;
    public player: string;
    constructor( x: number, y: number, player: string ) {
        this.x = x;
        this.y = y;
        this.player = player;
    }

    public static of( toConvert: Vertex, player: string ): VertexBean {
        return new VertexBean( toConvert.x, toConvert.y, player );
    }

    public static asVertex( toConvert: VertexBean | any ): Vertex {
        let x, y: number;
        if ( toConvert instanceof VertexBean ) {
            x = toConvert.x;
            y = toConvert.y;
        } else { // well ...
            x = toConvert.x;
            y = toConvert.y;
        }
        return new Vertex( x, y );
    }
}

export class TriangleBean {
    public point1: VertexBean;
    public point2: VertexBean;
    public point3: VertexBean;
    public owner: string;
    /** The base value to calculate effect impacts with */
    public baseValue: number;
    /**The defense value which increases the costs of destruction*/
    public defense;

    constructor( point1: VertexBean, point2: VertexBean, point3: VertexBean, baseValue: number, defense: number, owner: string ) {
        this.point1 = point1;
        this.point2 = point2;
        this.point3 = point3;
        this.owner = owner;
        this.defense = defense;
        this.baseValue = baseValue;
    }

    public static of( toConvert: IngameTriangle | any ): TriangleBean {
        let owner: string = toConvert.owner;
        let vertices: [Vertex, Vertex, Vertex] = toConvert instanceof IngameTriangle ?
            toConvert.triangle.vertices : [toConvert.point1, toConvert.point2, toConvert.point3];
        return new TriangleBean(
            VertexBean.of( vertices[0], owner ),
            VertexBean.of( vertices[1], owner ),
            VertexBean.of( vertices[2], owner ), toConvert.baseValue, toConvert.defense, owner );
    }

    public static asIngameTriangle( toConvert: TriangleBean | any ): IngameTriangle {
        let v1, v2, v3: Vertex;
        let player: string;
        if ( toConvert instanceof TriangleBean ) {
            v1 = VertexBean.asVertex( toConvert.point1 );
            v2 = VertexBean.asVertex( toConvert.point2 );
            v3 = VertexBean.asVertex( toConvert.point3 );
            player = toConvert.owner;
        } else { //TODO well ...
            v1 = VertexBean.asVertex( toConvert.point1 );
            v2 = VertexBean.asVertex( toConvert.point2 );
            v3 = VertexBean.asVertex( toConvert.point3 );
            player = toConvert.player;
        }
        let triangle = Triangle.of( v1, v2, v3 );
        let ingTriangle = IngameTriangle.of( triangle, player );
        ingTriangle.baseValue = toConvert.baseValue;
        ingTriangle.defense = toConvert.defense;
        return ingTriangle;
    }
}


/*
 * SERVER COMMANDS 
 */

export interface ServerCommand { }

/**
 * Represents a list of actions issued by the server.
 */
export class ServerUpdate implements ServerCommand {
    private _trianglesToRemove: TriangleBean[];
    private _trianglesToAdd: TriangleBean[];

    get trianglesToAdd() { return this._trianglesToAdd; }
    set trianglesToAdd( trianglesToAdd: TriangleBean[] ) { this._trianglesToAdd = trianglesToAdd; }
    get trianglesToRemove() { return this._trianglesToRemove; }
    set trianglesToRemove( trianglesToRemove: TriangleBean[] ) { this._trianglesToRemove = trianglesToRemove; }

    /**
     * Returns a JSON view of this object.
     * Due to issues with the private name fields this method must be used to serialize a {@code ServerUpdate} as JSON.
     * @return a JSON view of this object. Note that modifications on this view won't affect the underlying {@code ServerUpdate} object.
     */
    //TODO is this method even required?
    public toJson() {
        return {
            trianglesToRemove: this._trianglesToRemove,
            trianglesToAdd: this._trianglesToAdd
        };
    }

    /**
     * Creates a new {@code ServerUpdate} based on a JSON object.
     * @param toConvert holds the information of the {@code ServerUpdate} to be created
     */
    public static fromJson( toConvert: any ): ServerUpdate {
        let toReturn: ServerUpdate = new ServerUpdate();
        let toRemove: TriangleBean[] = toConvert.trianglesToRemove.map( t => TriangleBean.of( t ) );
        let toAdd: TriangleBean[] = toConvert.trianglesToAdd.map( t => TriangleBean.of( t ) );
        toReturn.trianglesToRemove = toRemove;
        toReturn.trianglesToAdd = toAdd;
        return toReturn;
    }
}

/**
 * Holds the complete state of the server and allows to re-build the client's state.
 */
export class ResetState implements ServerCommand {

    private _trianglesToCreate: TriangleBean[];
    get trianglesToCreate(): TriangleBean[] { return this._trianglesToCreate; }

    set trianglesToCreate( trianglesToCreate: TriangleBean[] ) {
        this._trianglesToCreate = trianglesToCreate;
    }

    public static fromJson( toConvert: any ): ResetState {
        let toReturn: ResetState = new ResetState();
        toReturn.trianglesToCreate = toConvert.trianglesToCreate.map( t => TriangleBean.of( t ) );
        return toReturn;
    }

    public toJson() {
        return {
            trianglesToCreate: this._trianglesToCreate,
            incrementalUpdate: false
        };
    }
}

/**
 * Holds the acknowledged player's name.
 */
export class InitInfo implements ServerCommand {

    private _playerId: string;
    private _initialState: ResetState;
    get playerId(): string { return this._playerId; }
    set playerId( playerId: string ) { this._playerId = playerId; }
    get initialState(): ResetState { return this._initialState; }
    set initialState( initialState: ResetState ) { this._initialState = initialState; }

    public static fromJson( toConvert: any ): InitInfo {
        let toReturn: InitInfo = new InitInfo();
        toReturn.playerId = toConvert.playerId;
        toReturn.initialState = ResetState.fromJson( toConvert.initialState );
        return toReturn;
    }

    public toJson() {
        return {
            name: this.playerId
        };
    }
}

/**
 * Tells a player's account balance.
 */
export class AccountInfo implements ServerCommand {

    private _balance: number;
    get balance(): number { return this._balance; }
    set balance( balance: number ) { this._balance = balance; }

    public static fromJson( toConvert: any ): AccountInfo {
        let toReturn: AccountInfo = new AccountInfo();
        toReturn.balance = toConvert.balance;
        return toReturn;
    }

    public toJson() {
        return {
            balance: this.balance
        };
    }
}