/*
 * RESPONSE HANDLING
 */

/**
 * Processes a server command for a given command type. 
 */
import { resume, applyServerRegister, applyServerReset, applyServerUpdate, setAccount } from "../run/main";
import { InitInfo, TriangleBean, ResetState, ServerUpdate, AccountInfo } from "./network_beans";
import { IngameTriangle } from "../board_handling/game_objects";

export interface ServerCommandHandler {
    /**Returns the type of the server command.*/
    getType(): CommandType;
    /**Processes the command's content.*/
    process( command: any ): void;
}

/**
 * Possible types of response from the server. Tells how to parse and process the server's response. 
 */
export enum CommandType {
    UPDATE = "UPDATE", RESET = "RESET", INIT = "INIT", ACCOUNT_INFO = "ACCOUNT_INFO"
}

/**
 * Processes a account inforation from the server.
 */
export class AccountHandler implements ServerCommandHandler {
    static readonly TYPE = CommandType.ACCOUNT_INFO;
    readonly accountInfo = setAccount;
    getType() { return AccountHandler.TYPE };
    process( command: any ) {
        let accInfo: AccountInfo = AccountInfo.fromJson( command )
        this.accountInfo.call( this, accInfo.balance );
    }
}

/**
 * Processes a update command from the server.
 */
export class UpdateHandler implements ServerCommandHandler {
    static readonly TYPE = CommandType.UPDATE;
    readonly serverUpdate = applyServerUpdate;
    getType() { return UpdateHandler.TYPE };
    process( command: any ) {
        let update: ServerUpdate = ServerUpdate.fromJson( command )
        let toAdd: IngameTriangle[] = update.trianglesToAdd.map( t => TriangleBean.asIngameTriangle( t ) );
        let toRemove: IngameTriangle[] = update.trianglesToRemove.map( t => TriangleBean.asIngameTriangle( t ) );
        this.serverUpdate.call( this, toRemove, toAdd );
    }
}

/**
 * Processes a reset command from the server. 
 */
export class ResetHandler implements ServerCommandHandler {
    static readonly TYPE = CommandType.RESET;
    getType() { return ResetHandler.TYPE };
    readonly serverReset = applyServerReset;
    process( command: any ) {
        let reset: ResetState = ResetState.fromJson( command );
        this.serverReset.call( this, reset.trianglesToCreate.map( t => TriangleBean.asIngameTriangle( t ) ) );
    }
}

/**
 * Processes a registering acknowledgement from the server. 
 */
export class InitHandler implements ServerCommandHandler {
    static readonly TYPE = CommandType.INIT;
    getType() { return InitHandler.TYPE };
    readonly serverRegister = applyServerRegister;
    process( command: any ) {
        let registration: InitInfo = InitInfo.fromJson( command );
        this.serverRegister.call( this, registration.playerId, registration.initialState.trianglesToCreate.map( t => TriangleBean.asIngameTriangle( t ) ) );
    }
}

/**
* Processes an invalid command from the server by allowing the front end to proceed without waiting for proper server command. 
*/
export class InvalidCommandHandler implements ServerCommandHandler {
    static readonly TYPE = undefined;
    getType() { return InvalidCommandHandler.TYPE };
    readonly serverReset = resume;
    process( command?: any ) {
        resume();
    }
}

