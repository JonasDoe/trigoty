
/**
 * Network communication
 */

import { Vertex, Edge } from "../logic/geometric_objects";
import { CanvasHandler } from "../board_handling/canvas_handler";
import { applyServerUpdate, applyServerReset, applyServerRegister, resume } from "../run/main";
import * as SockJS from '../../resources/sockjs.js';
import * as Stomp from '../../resources/stomp.js';
import { UpdateHandler, ResetHandler, InitHandler, InvalidCommandHandler, AccountHandler } from "./command_handlers";
import { TriangleBean } from "./network_beans";
import { IngameTriangle } from "../board_handling/game_objects";

/*
 *  NETWORK COMMUNICATION 
 */


/** Holds the available server command handlers. */
const CommandHandlers = [new UpdateHandler(), new ResetHandler(), new InitHandler(), new AccountHandler()];

/**Base path for requests*/
const webSocketPath = "/trigoty-websocket";
/** Request path to register as a new player*/
const registerPlayerPath = "/register";
/** Request path to request the initial information*/
const requestInitInfosPath = "/init";
/** Request path to propose {@code Triangle}*/
const proposeTrianglePath = "/propose/triangle";
/** Request path for the complete game state*/
const requestCompleteStatePath = "/fullstate";
/**Path to request a game reset*/
const newGamePath = "/debug/new_game";
/** Channel for private messages*/
const whisperChannel = "/user/queue/reply";
/** Channel for public, i.e. broadcasted messages*/
const broadcastChannel = "/update";

export interface Network { }

/**
 * Handles requests for the server and delivers server messages via callbacks.
 */
export class NetworkConnector implements Network {

    /**Holds the connection to the server*/
    private socket = new SockJS( webSocketPath );
    /**Handles the communication via the Stomp protocol*/
    private stompClient = Stomp.Stomp.over( this.socket );
    /**Handles server commands which have caused an error*/
    private defaultHandler = new InvalidCommandHandler();
    /**Additional header information*/
    private header: any = {};

    /**
     * Initialises this connector and calls the given method once the connecion is completed.
     * @param readyHandler callback once the connection is established
     */
    public init( readyHandler: ( boolean ) => void ): void {
        this.stompClient.connect( /*'guest', 'guest'*/this.header, ( frame ) => {
            this.subscribe( this, whisperChannel ).call( frame );
            this.subscribe( this, broadcastChannel ).call( frame );
            readyHandler.call( this, true );
        } );
    }
    /**
     * Subscribes to the websocket's channel all the communication will be realized with.
     *@param connector holding the websocket. Required because {@code this} will be overwritten when this method is invoked via callback.
     */
    private subscribe( connector: NetworkConnector, channel: string ) {
        return function( frame ) {
            console.log( 'Connected: ' + frame );
            //TODO use subscription?
            let subcription = connector.stompClient.subscribe( channel, function( updateObject: any ) {
                try {
                    let response: any = JSON.parse( updateObject.body );
                    let handler = CommandHandlers.find( h => h.getType() === response.type );
                    if ( !handler ) throw "No handler for response type " + response.type;
                    else handler.process( response );
                } catch ( e ) {
                    console.log( "Parsing the response failed: " + e );
                    connector.defaultHandler.process();
                }
            } );
        }
    }

    /**
     * Registers a new player for the game.
     */
    public registerPlayer() {
        try {
            this.stompClient.send( registerPlayerPath, this.header, null );
        } catch ( e ) {
            console.log( "Registering failed: " + e );
            resume();
        }
    }

    /**Proposes a {@code Triangle} to the server which might appear in the next {@code UpdateCommand} from the server.
     *@toRequest the {@code Triangle} which shall be proposed
     */
    public proposeTriangle( toRequest: IngameTriangle ) {
        if ( !toRequest ) resume();
        try {
            let requestObj: TriangleBean = TriangleBean.of( toRequest );
            this.stompClient.send( proposeTrianglePath, this.header, JSON.stringify( requestObj ) );
        } catch ( e ) {
            console.log( "Requesting the the triangle " + toRequest.toString() + " failed: " + e );
            resume();
        }
    }

    /**
     * Requests the complete server state. Useful if the client's state seems to be inconsistent server's.
     */
    public requestCompleteState() {
        try {

            this.stompClient.send( requestCompleteStatePath, this.header, null );
        } catch ( e ) {
            console.log( "Resetting failed: " + e );
            resume();
        }
    }

    public newGame() {
        this.stompClient.send( newGamePath, this.header, null );
    }
}

