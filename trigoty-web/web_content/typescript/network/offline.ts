
/**
 * Simulates network communication in an offline situation - for demo issues only
 */

import { Vertex, Edge, verticesToEdges, Triangle } from "../logic/geometric_objects";
import { CanvasHandler } from "../board_handling/canvas_handler";
import { applyServerUpdate, applyServerReset, applyServerRegister, resume, setAccount } from "../run/main";
import * as SockJS from '../../resources/sockjs.js';
import * as Stomp from '../../resources/stomp.js';
import { UpdateHandler, ResetHandler, InitHandler, InvalidCommandHandler, AccountHandler } from "./command_handlers";
import { TriangleBean, ResetState, VertexBean } from "./network_beans";
import { IngameTriangle } from "../board_handling/game_objects";
import { getCostsFor, getValueOf, getTurnIncomeFor, getDefenseUpgradeCosts } from "../logic/accountant";
import { Network } from "./network";
import { removeFromList } from "../util/shared";

/** Holds the available server command handlers. */
const initHandler = new InitHandler();
const updateHandler = new UpdateHandler();
const resetHandler = new ResetHandler();
const accountHandler = new AccountHandler();

/**
 * Handles requests for the server and delivers server messages via callbacks.
 */
export class DummyConnector implements Network {
    /**The {@code IngameTriangles} of the player*/
    private triangles: IngameTriangle[] = [];
    /**The account balance of the player*/
    private money = 0;
    /**
     * Initialises this connector and calls the given method once the connecion is completed.
     * @param readyHandler callback once the connection is established
     */
    public init( readyHandler: ( boolean ) => void ): void {
        setInterval(() => {
            for ( let triangle of this.triangles ) this.money += getTurnIncomeFor( triangle );
            setAccount( this.money );
        }, 1000 );
        readyHandler.call( this, true );
    }


    /**
     * Registers a new player for the game.
     */
    public registerPlayer() {
        let player = "[not connected]";
        let v1 = new Vertex( 0, 0 ); let v2 = new Vertex( 50, 0 ); let v3 = new Vertex( 0, 50 );
        let edges = verticesToEdges( v1, v2, v3 );
        let triangle = new Triangle( edges[0] );
        this.triangles.push( IngameTriangle.of( triangle, player ) );
        let vb1 = VertexBean.of( v1, player ); let vb2 = VertexBean.of( v2, player ); let vb3 = VertexBean.of( v3, player );
        let triangleBean = new TriangleBean( vb1, vb2, vb3, getCostsFor( triangle ), 0, player );
        initHandler.process( {
            playerId: player,
            initialState: ResetState.fromJson( { trianglesToCreate: [triangleBean] } )
        } );
    }

    /**Proposes a {@code Triangle} to the server which might appear in the next {@code UpdateCommand} from the server.
     *@toRequest the {@code Triangle} which shall be proposed
     */
    public proposeTriangle( toRequest: IngameTriangle ) {
        if ( !toRequest ) resume();
        let upgrade = false;
        for ( let triangle of this.triangles ) {
            if ( triangle.equals( toRequest ) ) {
                if ( !triangle.baseValue ) triangle.baseValue = getValueOf( triangle );
                this.money -= getDefenseUpgradeCosts( triangle );
                toRequest.defense += 1;
                upgrade = true;
                break;
            }
        }
        if ( !upgrade ) {
            toRequest.baseValue = getValueOf( toRequest );
            toRequest.defense = 0;
            this.money -= getCostsFor( toRequest );
            this.triangles.push( toRequest );
        }
        updateHandler.process( {
            trianglesToAdd: [toRequest],
            trianglesToRemove: []
        } );
    }

    /**
     * Requests the complete server state. Useful if the client's state seems to be inconsistent server's.
     */
    public requestCompleteState() {
        resetHandler.process( {
            trianglesToCreate: []
        } );
    }

    public newGame() {
        resetHandler.process( {
            trianglesToCreate: []
        } );
    }
}

