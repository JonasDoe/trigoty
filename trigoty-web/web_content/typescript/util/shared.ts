export interface StandardObject {
    toString(): string;
    equals( obj: object ): boolean;
}

export function removeFromList( object: StandardObject, list: StandardObject[] ) {
    for ( let i = 0; i < list.length; i++ ) {
        let equal = list[i].equals( object );
        if ( equal ) {
            list.splice( i, 1 );
        }
    }
}

export function min( ...values: number[] ): number {
    let min: number = null;
    for ( let value of values ) {
        min = min == null ? value : Math.min( min, value );
    }
    return min;
}

export function max( ...values: number[] ): number {
    let max: number = null;
    for ( var value of values ) {
        max = max = null ? value : Math.max( max, value );
    }
    return max;
}
