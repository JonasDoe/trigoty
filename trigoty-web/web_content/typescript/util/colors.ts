
//export enum Color {
//    RED = "#ff0000", BLACK = "#000000"
//}



function createOpacitySteps( rgbColor: string ): string[] {
    let toReturn: string[] = new Array();
    let rgba = rgbColor.replace( "rgb", "rgba" );
    for ( let alpha = 0; alpha <= 10; alpha += 0.10 ) {
        toReturn.push( rgba.replace( ")", "," + String( alpha ) + ")" ) );
    }
    return toReturn;
}

let greenRGB = "rgb(0,128,0)";
let redRGB = "rgb(220,20,60)";
let blackRGB = "rgb(0,0,0)";

const greenSteps: string[] = createOpacitySteps( greenRGB );
const redSteps: string[] = createOpacitySteps( redRGB );
const blackSteps: string[] = createOpacitySteps( blackRGB );


export class Color {
    private _colors: string[];
    val( strength?: number ): string { return strength != null && strength != undefined && strength < this._colors.length ? this._colors[strength] : this._colors[this._colors.length - 1]; }
    constructor( colors: string[] ) {
        this._colors = colors;
    }
}
export const GREEN: Color = new Color( greenSteps );
export const RED: Color = new Color( redSteps );
export const BLACK: Color = new Color( blackSteps );
