/*
 * Works as a controller. Manages the match, especially its proper initalization. Works with the html file.
 */


/**Only for debugging issues, can be shared to other modules to draw information relevant for debugging*/
import { CanvasHandler } from "../board_handling/canvas_handler";
import { NetworkConnector, Network } from "../network/network";
import { TriangleBean } from "../network/network_beans";
import { IngameTriangle } from "../board_handling/game_objects";
import { Vertex, Triangle, verticesToEdges } from "../logic/geometric_objects";
import { DummyConnector } from "../network/offline";

/**0: no debugging, 1: normal debug mode, 2: don't draw preview triangle*/
export let debug: number = 1; //__WEBPACK_IMPORTED_MODULE_2__run_main__["debug"]
/**Represents the (squared) proximity where {@code Vertices} will be matched*/
export const epsilon = 100;
/**Determines the min size of a {@code Triangle}*/
export const minRangeMultiplier = 10;
/**The canvas everything will be drawn in*/
export let canvasContext: CanvasRenderingContext2D;
/**Connection to the server*/
let connector;
/**Holds the game board and manages the player's input*/
let canvasHandler: CanvasHandler;
/**The player's id*/
let id: string;
/**Displays the player's name*/
let player: HTMLDivElement;
/**Displays the player's money*/
let money: HTMLDivElement;
/**Initializes the game as soon as the website is fully loaded.*/
document.addEventListener( 'DOMContentLoaded', init, false );

/**
 * Initializes the game.
 */
function init() {
    //    liveInfo = createLiveInfoField();
    player = document.getElementById( "player_name" ) as HTMLDivElement;
    money = document.getElementById( "money" ) as HTMLDivElement;
    try {
        connector = new NetworkConnector();
    } catch ( e ) { // Uncaught SyntaxError: The URL '/trigoty-websocket' is invalid
        connector = new DummyConnector();
    }
    connector.init( success => { if ( success ) createBoard(); } );
}

/**Initializes the board*/
function createBoard() {
    //TODO receive canvas size
    canvasHandler = new CanvasHandler( connector );
    let board = document.getElementById( "board" ) as HTMLCanvasElement;
    canvasHandler.init( board );
    if ( debug ) {
        enableStoreButton();
        enableResetButton();
    }
}

/**
 * Enables button to store the state in a json file.
 */
function enableStoreButton() {
    let storeButton = document.getElementById( "store" );
    storeButton.addEventListener( "click", ( e ) => {
        let triangleBeans: TriangleBean[] = new Array();
        for ( let triangle of canvasHandler.state.playerTriangles.concat( canvasHandler.state.otherTriangles ) ) {
            triangleBeans.push( TriangleBean.of( triangle ) );
        }
        let now: Date = new Date();
        storeButton.setAttribute( "download", "trigoty_snapshot_" + now.getFullYear() + now.getMonth() + now.getDate() + now.getHours() + now.getMinutes() + now.getSeconds() + ".json" );
        storeButton.setAttribute( "href", 'data:application/xml;charset=utf-8,{"triangles":' + JSON.stringify( triangleBeans ) + '}' );
    } );
    storeButton.style.display = "block";
}

/**
 * Enables button to reset the game's state. Since this back-end allows only limited players atm,
 * this button allows to restart so new players can be registered. 
 */
function enableResetButton() {
    let resetButton = document.getElementById( "new_game" );
    resetButton.addEventListener( "click", ( e ) => connector.newGame() );
    resetButton.style.display = "block";
}

/**
 * Redirects updates from the server to the board.
 * @param toAdd {@code Triangles} to be added
 * @param toRemove {@code Triangles} to be removed
 */
export function applyServerUpdate( toRemove: IngameTriangle[], toAdd: IngameTriangle[] ) {
    if ( money ) toAdd.forEach( t => money.innerText = String( +money.innerText - t.baseValue ) );
    canvasHandler.applyServerUpdate( toRemove, toAdd );
}

/**
 * Redirects a reset to the server's state to the board.
 * @param toCreate {@code Triangles} from the server's state
 */
export function applyServerReset( toCreate: IngameTriangle[] ) {
    canvasHandler.applyServerReset( toCreate );
}

/**
 * Redirects successful registration at the server to the board.
 * @param playerId of the player, used to identify him on the server
 * @param toCreate {@code Triangles} from the server's state
 */
export function applyServerRegister( playerId: string, toCreate: IngameTriangle[] ) {
    if ( player ) player.innerText = playerId;
    canvasHandler.register( playerId, toCreate );
}

/**
 * Resumes the board which is weighting for a server response.
 */
export function resume() {
    canvasHandler.resume();
}

/**
 * Resumes the board which is weighting for a server response.
 */
export function setAccount( balance: number ) {
    if ( money ) money.innerText = String( balance );
    canvasHandler.setAccount( balance );
}

