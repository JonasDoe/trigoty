/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, {
/******/ 				configurable: false,
/******/ 				enumerable: true,
/******/ 				get: getter
/******/ 			});
/******/ 		}
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "";
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = 0);
/******/ })
/************************************************************************/
/******/ ([
/* 0 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "debug", function() { return debug; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "canvasContext", function() { return canvasContext; });
/* harmony export (immutable) */ __webpack_exports__["applyServerUpdate"] = applyServerUpdate;
/* harmony export (immutable) */ __webpack_exports__["applyServerReset"] = applyServerReset;
/* harmony export (immutable) */ __webpack_exports__["applyServerRegister"] = applyServerRegister;
/* harmony export (immutable) */ __webpack_exports__["resume"] = resume;
/* harmony export (immutable) */ __webpack_exports__["setAccount"] = setAccount;
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__board_handling_canvas_handler__ = __webpack_require__(8);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__network_network__ = __webpack_require__(12);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__network_network_beans__ = __webpack_require__(3);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__network_offline__ = __webpack_require__(16);
/*
 * Works as a controller. Manages the match, especially its proper initalization. Works with the html file.
 */
/**Only for debugging issues, can be shared to other modules to draw information relevant for debugging*/




/**0: no debugging, 1: normal debug mode, 2: don't draw preview triangle*/
let debug = 1; //__WEBPACK_IMPORTED_MODULE_2__run_main__["debug"]
/**Represents the (squared) proximity where {@code Vertices} will be matched*/
const epsilon = 100;
/* harmony export (immutable) */ __webpack_exports__["epsilon"] = epsilon;

/**Determines the min size of a {@code Triangle}*/
const minRangeMultiplier = 10;
/* harmony export (immutable) */ __webpack_exports__["minRangeMultiplier"] = minRangeMultiplier;

/**The canvas everything will be drawn in*/
let canvasContext;
/**Connection to the server*/
let connector;
/**Holds the game board and manages the player's input*/
let canvasHandler;
/**The player's id*/
let id;
/**Displays the player's name*/
let player;
/**Displays the player's money*/
let money;
/**Initializes the game as soon as the website is fully loaded.*/
document.addEventListener('DOMContentLoaded', init, false);
/**
 * Initializes the game.
 */
function init() {
    //    liveInfo = createLiveInfoField();
    player = document.getElementById("player_name");
    money = document.getElementById("money");
    try {
        connector = new __WEBPACK_IMPORTED_MODULE_1__network_network__["a" /* NetworkConnector */]();
    }
    catch (e) {
        connector = new __WEBPACK_IMPORTED_MODULE_3__network_offline__["a" /* DummyConnector */]();
    }
    connector.init(success => { if (success)
        createBoard(); });
}
/**Initializes the board*/
function createBoard() {
    //TODO receive canvas size
    canvasHandler = new __WEBPACK_IMPORTED_MODULE_0__board_handling_canvas_handler__["a" /* CanvasHandler */](connector);
    let board = document.getElementById("board");
    canvasHandler.init(board);
    if (debug) {
        enableStoreButton();
        enableResetButton();
    }
}
/**
 * Enables button to store the state in a json file.
 */
function enableStoreButton() {
    let storeButton = document.getElementById("store");
    storeButton.addEventListener("click", (e) => {
        let triangleBeans = new Array();
        for (let triangle of canvasHandler.state.playerTriangles.concat(canvasHandler.state.otherTriangles)) {
            triangleBeans.push(__WEBPACK_IMPORTED_MODULE_2__network_network_beans__["e" /* TriangleBean */].of(triangle));
        }
        let now = new Date();
        storeButton.setAttribute("download", "trigoty_snapshot_" + now.getFullYear() + now.getMonth() + now.getDate() + now.getHours() + now.getMinutes() + now.getSeconds() + ".json");
        storeButton.setAttribute("href", 'data:application/xml;charset=utf-8,{"triangles":' + JSON.stringify(triangleBeans) + '}');
    });
    storeButton.style.display = "block";
}
/**
 * Enables button to reset the game's state. Since this back-end allows only limited players atm,
 * this button allows to restart so new players can be registered.
 */
function enableResetButton() {
    let resetButton = document.getElementById("new_game");
    resetButton.addEventListener("click", (e) => connector.newGame());
    resetButton.style.display = "block";
}
/**
 * Redirects updates from the server to the board.
 * @param toAdd {@code Triangles} to be added
 * @param toRemove {@code Triangles} to be removed
 */
function applyServerUpdate(toRemove, toAdd) {
    if (money)
        toAdd.forEach(t => money.innerText = String(+money.innerText - t.baseValue));
    canvasHandler.applyServerUpdate(toRemove, toAdd);
}
/**
 * Redirects a reset to the server's state to the board.
 * @param toCreate {@code Triangles} from the server's state
 */
function applyServerReset(toCreate) {
    canvasHandler.applyServerReset(toCreate);
}
/**
 * Redirects successful registration at the server to the board.
 * @param playerId of the player, used to identify him on the server
 * @param toCreate {@code Triangles} from the server's state
 */
function applyServerRegister(playerId, toCreate) {
    if (player)
        player.innerText = playerId;
    canvasHandler.register(playerId, toCreate);
}
/**
 * Resumes the board which is weighting for a server response.
 */
function resume() {
    canvasHandler.resume();
}
/**
 * Resumes the board which is weighting for a server response.
 */
function setAccount(balance) {
    if (money)
        money.innerText = String(balance);
    canvasHandler.setAccount(balance);
}


/***/ }),
/* 1 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (immutable) */ __webpack_exports__["d"] = verticesToEdges;
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__util_shared__ = __webpack_require__(5);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__run_main__ = __webpack_require__(0);
/**
 * Holds the geometrical object prototypes.
 */

 //Both information shall only be used for debugging
/**
 * Represents the orientation of three vertices that constitute a
 * {@code Triangle} in 2D, based on a point of origin in the bottom left
 *
 * @author J
 *
 */
var Orientation;
(function (Orientation) {
    Orientation[Orientation["CLOCKWISE"] = 0] = "CLOCKWISE";
    Orientation[Orientation["COUNTER_CLOCKWISE"] = 1] = "COUNTER_CLOCKWISE";
    Orientation[Orientation["COLINEAR"] = 2] = "COLINEAR";
})(Orientation || (Orientation = {}));
/**All geometry is adjusted on this orientation*/
let targetOrientation = Orientation.CLOCKWISE;
class Vertex {
    constructor(x, y) {
        this.toString = () => {
            return this.x + "," + this.y;
        };
        this._x = x;
        this._y = y;
    }
    get x() { return this._x; }
    get y() { return this._y; }
    subtractVertex(vertex) {
        return new Vertex(this.x - vertex.x, this.y - vertex.y);
    }
    ;
    skalarProduct(vertex) {
        return (this.x * vertex.x) + (this.y * vertex.y);
    }
    ;
    inverseVertex() {
        return new Vertex(-this.x, -this.y);
    }
    ;
    sqrDistanceToPoint(vertex) {
        return Math.pow(this.x - vertex.x, 2) + Math.pow(this.y - vertex.y, 2);
    }
    ;
    equals(vertex) {
        if (!vertex)
            return false;
        if (!(vertex instanceof Vertex))
            return false;
        else
            return this.x == vertex.x && this.y == vertex.y;
    }
    compareTo(toCompareWith) {
        let xComp = this.x - toCompareWith.x;
        if (xComp != 0)
            return xComp;
        else
            return this.y - toCompareWith.y;
    }
}
/* harmony export (immutable) */ __webpack_exports__["c"] = Vertex;

class Edge {
    constructor(startVertex, prevEdge, nextEdge, twinEdge) {
        this.toString = () => {
            var prev = !this.prevEdge ? "null" : this.prevEdge.startVertex;
            var next = !this.nextEdge ? "null" : this.nextEdge.startVertex;
            var twin = !this._twinEdge ? "null" : this._twinEdge.prevEdge.startVertex + " <-- " + this.startVertex + " --> " + this._twinEdge.nextEdge.startVertex;
            if (__WEBPACK_IMPORTED_MODULE_1__run_main__["debug"] > 0)
                return prev + " <-- " + this.startVertex + " --> " + next + "\nTwin: " + twin;
            else
                return prev + " <-- " + this.startVertex + " --> " + next;
        };
        this._startVertex = startVertex;
        this._prevEdge = prevEdge;
        this._nextEdge = nextEdge;
        this._twinEdge = twinEdge;
        this._relatedTriangle;
    }
    get startVertex() { return this._startVertex; }
    set startVertex(startVertex) { this._startVertex = startVertex; }
    get prevEdge() { return this._prevEdge; }
    set prevEdge(prevEdge) { this._prevEdge = prevEdge; }
    get nextEdge() { return this._nextEdge; }
    set nextEdge(nextEdge) { this._nextEdge = nextEdge; }
    //SINCE public get and private set are not allowed yet, but set is complex and may fail (see linkAsTwins), we skip the whole approach here
    //    get twinEdge(): Edge { return this._twinEdge; }
    //    private set twinEdge( twinEdge: Edge ) {
    //        if ( !twinEdge ) return;
    //        this._twinEdge = twinEdge;
    //        twinEdge._twinEdge = this;
    //    }
    get relatedTriangle() { return this._relatedTriangle; }
    set relatedTriangle(relatedTriangle) {
        this._relatedTriangle = relatedTriangle;
        if (!this._prevEdge.relatedTriangle)
            this._prevEdge.relatedTriangle = relatedTriangle;
        if (!this._nextEdge.relatedTriangle)
            this._nextEdge.relatedTriangle = relatedTriangle;
    }
    /**
     * Registers a given {@code Edge} as twin for this {@code Edge}, if possible. Fails if there is already a twin {@code Edge} or it's no proper twin.
     * @param toRegister the proposed twin {@code Edge}
     * @return {@code true} if the twin was successfully registered, otherwise {@code false}
     */
    linkAsTwins(toLink) {
        if (!toLink || toLink._twinEdge)
            return false;
        if (!this._twinEdge && this.isProperTwinFor(toLink)) {
            this._twinEdge = toLink;
            toLink._twinEdge = this;
            return true;
        }
        else
            return false;
    }
    getTwinEdge() {
        return this._twinEdge;
    }
    /**
     * Creates a twin for a given {@code Edge}. The given {@code Edge} won't be linked to the created twin model.
     * For that, use {@link Edge#linkAsTwins(Edge)}.
     * @param toConvert a twin will be created for
     * @return an {@code Edge} working as proper twin
     */
    static createTwinModel(toConvert) {
        let twinNextEdge = toConvert.prevEdge._twinEdge;
        if (twinNextEdge == null)
            twinNextEdge = new Edge(toConvert.startVertex, null, null, null);
        return new Edge(toConvert.nextEdge.startVertex, toConvert.nextEdge._twinEdge, twinNextEdge, toConvert);
    }
    isProperTwinFor(edge) {
        if (!edge)
            return false;
        else
            return this.startVertex.equals(edge.nextEdge.startVertex) && this.nextEdge.startVertex.equals(edge.startVertex);
    }
    drop() {
        if (this._twinEdge)
            this._twinEdge._twinEdge = null;
        if (this.nextEdge)
            this.nextEdge.prevEdge = null;
        if (this.prevEdge)
            this.prevEdge.nextEdge = null;
    }
    sqrDistanceToPoint(vertex) {
        // stolen from http://stackoverflow.com/a/6853926/5767484
        var y1 = this.startVertex.y;
        var x1 = this.startVertex.x;
        var x2 = this.nextEdge.startVertex.x;
        var y2 = this.nextEdge.startVertex.y;
        var A = vertex.x - x1;
        var B = vertex.y - y1;
        var C = x2 - x1;
        var D = y2 - y1;
        var dot = A * C + B * D;
        var len_sq = C * C + D * D;
        var param = -1;
        if (len_sq != 0)
            param = dot / len_sq;
        var xx, yy;
        if (param < 0) {
            xx = x1;
            yy = y1;
        }
        else if (param > 1) {
            xx = x2;
            yy = y2;
        }
        else {
            xx = x1 + param * C;
            yy = y1 + param * D;
        }
        var dx = vertex.x - xx;
        var dy = vertex.y - yy;
        return dx * dx + dy * dy;
    }
    ;
    sqrDistanceToEdge(edge) {
        // TODO remove because can't happen in current implementation?
        if (this.isIntersecting(edge))
            return 0;
        var d1 = this.sqrDistanceToPoint(edge.startVertex);
        var d2 = this.sqrDistanceToPoint(edge.nextEdge.startVertex);
        var d3 = edge.sqrDistanceToPoint(this.startVertex);
        var d4 = edge.sqrDistanceToPoint(this.nextEdge.startVertex);
        return Object(__WEBPACK_IMPORTED_MODULE_0__util_shared__["b" /* min */])(d1, d2, d3, d4);
    }
    ;
    /**
     * Returns the {@code Vertex} both {@code Edges} share, if any.
     *
     * @param toCheck
     *            the {@code Edge} to be checked for a common {@code Vertex}
     * @return the shared {@code Vertex}, if any, otherwise {@code null}
     */
    getTouch(toCheck) {
        if (!toCheck)
            return null;
        else if (this.startVertex.equals(toCheck.startVertex))
            return this.startVertex;
        else if (this.startVertex.equals(toCheck.nextEdge.startVertex))
            return this.startVertex;
        else if (this.nextEdge.startVertex.equals(toCheck.startVertex))
            return this.nextEdge.startVertex;
        else if (this.nextEdge.startVertex.equals(toCheck.nextEdge.startVertex))
            return this.nextEdge.startVertex;
        else
            return null;
    }
    isIntersecting(edge, proximity) {
        if (!proximity) {
            if (this.getTouch(edge))
                return false;
            let o1 = Triangle.getOrientation(this.startVertex, this.nextEdge.startVertex, edge.startVertex);
            let o2 = Triangle.getOrientation(this.startVertex, this.nextEdge.startVertex, edge.nextEdge.startVertex);
            let o3 = Triangle.getOrientation(edge.startVertex, edge.nextEdge.startVertex, this.startVertex);
            let o4 = Triangle.getOrientation(edge.startVertex, edge.nextEdge.startVertex, this.nextEdge.startVertex);
            // General case
            if (o1 != o2 && o3 != o4)
                return true;
            else
                return false;
            // colinear case skipped
        }
        else
            return this.sqrDistanceToEdge(edge) < proximity;
    }
    /**
     * Slower method which returns the exact point of collision though. Should
     * be only used if needed ... stolen from
     * http://stackoverflow.com/a/1968345/5767484
     */
    calculateIntersectionPoint(edge1, edge2) {
        let p0_x = edge1.startVertex.x;
        let p0_y = edge1.startVertex.y;
        let p1_x = edge1.nextEdge.startVertex.x;
        let p1_y = edge1.nextEdge.startVertex.y;
        let p2_x = edge2.startVertex.x;
        let p2_y = edge2.startVertex.y;
        let p3_x = edge2.nextEdge.startVertex.x;
        let p3_y = edge2.nextEdge.startVertex.y;
        let s1_x = p1_x - p0_x;
        let s1_y = p1_y - p0_y;
        let s2_x = p3_x - p2_x;
        let s2_y = p3_y - p2_y;
        // TODO: divisor = 0
        let s = (-s1_y * (p0_x - p2_x) + s1_x * (p0_y - p2_y)) / (-s2_x * s1_y + s1_x * s2_y);
        let t = (s2_x * (p0_y - p2_y) - s2_y * (p0_x - p2_x)) / (-s2_x * s1_y + s1_x * s2_y);
        if (s >= 0 && s <= 1 && t >= 0 && t <= 1) {
            // Collision detected
            let i_x = p0_x + (t * s1_x);
            let i_y = p0_y + (t * s1_y);
            return new Vertex(i_x, i_y);
        }
        return null;
    }
    /**
     * Returns the squared length of this {@code Edge}.
     *
     * @return the squared length of this {@code Edge}
     */
    getSqrLength() {
        return this.startVertex.sqrDistanceToPoint(this.nextEdge.startVertex);
    }
    compareTo(toCompareWith) {
        let xComp = this.startVertex.compareTo(toCompareWith.startVertex);
        if (xComp != 0)
            return xComp;
        else
            return this.nextEdge.startVertex.compareTo(toCompareWith._nextEdge.startVertex);
    }
    equals(edge) {
        if (!edge)
            return false;
        if (!(edge instanceof Edge))
            return false;
        return this.startVertex.equals(edge.startVertex) && this.nextEdge.startVertex.equals(edge.nextEdge.startVertex);
    }
}
/* harmony export (immutable) */ __webpack_exports__["a"] = Edge;

class Triangle {
    constructor(edge1) {
        this.toString = () => {
            if (__WEBPACK_IMPORTED_MODULE_1__run_main__["debug"] > 0) {
                return "Start edge: " + this.relatedEdge + "\nSecond edge: " + this.relatedEdge.nextEdge + "\nThird edge: " + this.relatedEdge.prevEdge;
            }
            else
                return this.relatedEdge.startVertex + ":" + this.relatedEdge.nextEdge.startVertex + ":" + this.relatedEdge.prevEdge.startVertex;
        };
        this._relatedEdge = edge1;
        this._relatedEdge.relatedTriangle = this;
        this._minX = Object(__WEBPACK_IMPORTED_MODULE_0__util_shared__["b" /* min */])(this.relatedEdge.startVertex.x, this.relatedEdge.nextEdge.startVertex.x, this.relatedEdge.prevEdge.startVertex.x);
        this._maxX = Object(__WEBPACK_IMPORTED_MODULE_0__util_shared__["a" /* max */])(this.relatedEdge.startVertex.x, this.relatedEdge.nextEdge.startVertex.x, this.relatedEdge.prevEdge.startVertex.x);
        this._minY = Object(__WEBPACK_IMPORTED_MODULE_0__util_shared__["b" /* min */])(this.relatedEdge.startVertex.y, this.relatedEdge.nextEdge.startVertex.y, this.relatedEdge.prevEdge.startVertex.y);
        this._maxY = Object(__WEBPACK_IMPORTED_MODULE_0__util_shared__["a" /* max */])(this.relatedEdge.startVertex.y, this.relatedEdge.nextEdge.startVertex.y, this.relatedEdge.prevEdge.startVertex.y);
        //sort for easier comparison
        this._vertices = [this.relatedEdge.startVertex, this.relatedEdge.nextEdge.startVertex, this.relatedEdge.prevEdge.startVertex].
            sort((v1, v2) => v1.compareTo(v2));
        this._edges = [this.relatedEdge, this.relatedEdge.nextEdge, this.relatedEdge.prevEdge].
            sort((e1, e2) => e1.compareTo(e2));
    }
    get relatedEdge() { return this._relatedEdge; }
    set relatedEdge(edge) { this._relatedEdge = edge; }
    get ingameTriangle() { return this.container; }
    set ingameTriangle(ingameTriangle) { this.container = ingameTriangle; }
    get vertices() {
        return this._vertices;
    }
    get edges() {
        return this._edges;
    }
    static of(vertex1, vertex2, vertex3) {
        let actualOrientation = Triangle.getOrientation(vertex1, vertex2, vertex3);
        if (actualOrientation == Orientation.COLINEAR)
            return null;
        let v1 = vertex1;
        let v2;
        let v3;
        if (actualOrientation == targetOrientation) {
            v2 = vertex2;
            v3 = vertex3;
        }
        else {
            v2 = vertex3;
            v3 = vertex2;
        }
        let v1v2 = new Edge(v1, null, null, null);
        let v2v3 = new Edge(v2, v1v2, null, null);
        let v3v1 = new Edge(v3, v2v3, v1v2, null);
        v1v2.nextEdge = v2v3;
        v1v2.prevEdge = v3v1;
        v2v3.nextEdge = v3v1;
        return new Triangle(v1v2);
    }
    // idea stolen from
    // http://www.geeksforgeeks.org/check-if-two-given-line-segments-intersect
    static getOrientation(vertex1, vertex2, vertex3) {
        // See http://www.geeksforgeeks.org/orientation-3-ordered-points/
        // for details of below formula.
        let val = (vertex2.y - vertex1.y) * (vertex3.x - vertex2.x) -
            (vertex2.x - vertex1.x) * (vertex3.y - vertex2.y);
        if (val == 0)
            return Orientation.COLINEAR; // colinear
        /*
         * Canvas (0,0) lies in the upper left corner, while the result is based
         * on the classical point of origin. Won"t make a difference in the
         * checkIntersecting-evaluation though
         */
        return (val > 0) ? Orientation.CLOCKWISE : Orientation.COUNTER_CLOCKWISE;
    }
    drop(cleanLists) {
        for (let edge of this.edges) {
            edge.drop();
        }
    }
    /**
     * Returns the area enclosed by this {@code Triangle}.
     * @return the area of this {@code Triangle}
     */
    getArea() {
        let v1 = this.vertices[0];
        let v2 = this.vertices[1];
        let v3 = this.vertices[2];
        let part1 = v1.x * (v2.y - v3.y);
        let part2 = v2.x * (v3.y - v1.y);
        let part3 = v3.x * (v1.y - v2.y);
        return Math.abs(part1 + part2 + part3) >> 1;
    }
    contains(vertex) {
        if (!vertex)
            return false;
        if (this._maxX < vertex.x || this._minX > vertex.x
            || this._maxY < vertex.y || this._minY > vertex.y)
            return false;
        // Stolen from http://blackpawn.com/texts/pointinpoly
        // Compute vectors
        let v0 = this.relatedEdge.prevEdge.startVertex.subtractVertex(this.relatedEdge.startVertex);
        let v1 = this.relatedEdge.nextEdge.startVertex.subtractVertex(this.relatedEdge.startVertex);
        let v2 = vertex.subtractVertex(this.relatedEdge.startVertex);
        // Compute dot products
        let dot00 = v0.skalarProduct(v0);
        let dot01 = v0.skalarProduct(v1);
        let dot02 = v0.skalarProduct(v2);
        let dot11 = v1.skalarProduct(v1);
        let dot12 = v1.skalarProduct(v2);
        // Compute barycentric vertexinates
        let invDenom = 1 / (dot00 * dot11 - dot01 * dot01);
        let u = (dot11 * dot02 - dot01 * dot12) * invDenom;
        let v = (dot00 * dot12 - dot01 * dot02) * invDenom;
        // Check if point is in triangle
        return (u >= 0) && (v >= 0) && (u + v < 1);
    }
    /**
     * Checks whether a {@code Triangle} intersects with this one. A common
     * {@code Vertex} does not count as intersection. Note this only a fast check will be done here,
     * relying on the fact that some situations might not happen due to the way {@code Trianges} are
     * constructed here (via the evaluation of a drawn line). The in-depth check will only happen
     * on backend side.
     *
     * @param toCheck
     *            the {@code Triangle} to be checked for intersection
     * @return {@code true}, if the {@code Triangle} intersects with on this one
     */
    intersectsWithTriangle(triangle) {
        if (!triangle)
            return false;
        if (this._maxX < triangle._minX || this._minX > triangle._maxX
            || this._maxY < triangle._minY || this._minY > triangle._maxY) {
            return false;
        }
        if (this.isNeighbour(triangle))
            return false;
        for (let vertex of triangle.vertices) {
            // Check all vertices of the new triangle: if there a vertex
            // contained in this triangle and this vertex is not an vertex of
            // this triangle, too, then there's an intersection
            for (let triangleEdge of this.edges) {
                for (let toCheckEdge of triangle.edges) {
                    if (triangleEdge.isIntersecting(toCheckEdge))
                        return true;
                }
            }
        }
        return false;
    }
    isNeighbour(triangle) {
        if (!triangle)
            return false;
        for (let edge of triangle.edges) {
            for (let ownEdge of this.edges) {
                if (ownEdge.getTwinEdge() && ownEdge.getTwinEdge().equals(edge))
                    return true;
            }
        }
        return false;
    }
    /**
     * Returns the {@code Triangle's} centroid.
     * @return the centroid
     */
    getCentroid() {
        let x = (this.vertices[0].x + this.vertices[1].x + this.vertices[2].x) * 0.333;
        let y = (this.vertices[0].y + this.vertices[1].y + this.vertices[2].y) * 0.333;
        return new Vertex(x, y);
    }
    compareTo(toCompareWith) {
        let ownEdges = this.edges;
        let otherEdges = toCompareWith.edges;
        let xComp1 = ownEdges[0].compareTo(otherEdges[0]);
        if (xComp1 != 0)
            return xComp1;
        let xComp2 = ownEdges[1].compareTo(otherEdges[1]);
        if (xComp2 != 0)
            return xComp2;
        else
            return ownEdges[2].compareTo(otherEdges[2]);
    }
    equals(triangle) {
        if (!(triangle instanceof Triangle))
            return false;
        let ownVertices = this.vertices;
        let otherVertices = triangle.vertices;
        return (ownVertices[0].equals(otherVertices[0]) && ownVertices[1].equals(otherVertices[1]) && ownVertices[2].equals(otherVertices[2]));
    }
}
/* harmony export (immutable) */ __webpack_exports__["b"] = Triangle;

/**
 * Takes vertices and builds edges from them. All edges will be linked in their
 * order and the first with the last one.
 *
 * @returns array of created edges
 */
function verticesToEdges(...vertices) {
    if (vertices.length < 3)
        return;
    let edges = [];
    for (var i = 0; i < vertices.length; i++) {
        edges = edges.concat(new Edge(vertices[i], null, null));
    }
    for (let i = 1; i < edges.length - 1; i++) {
        edges[i].prevEdge = edges[i - 1];
        edges[i].nextEdge = edges[i + 1];
    }
    edges[0].nextEdge = edges[1];
    edges[0].prevEdge = edges[edges.length - 1];
    edges[edges.length - 1].nextEdge = edges[0];
    edges[edges.length - 1].prevEdge = edges[edges.length - 2];
    return edges;
}


/***/ }),
/* 2 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
class IngameTriangle {
    /**
     * Creates a new {@code IngameTriangle}. Private to make sure it can be only called from the factory method
     * which ensures the requrired side effects.
     * @param innerTriangle to be held by this instance
     * @param owner the player who onws this {@code IngameTriangle}
     */
    constructor(innerTriangle, owner) {
        /**Influences the enemies costs do destroy this {@code IngameTriangle}*/
        this._defense = 0;
        this._triangle = innerTriangle;
        this._owner = owner;
    }
    get baseValue() { return this._baseValue; }
    set baseValue(baseValue) { this._baseValue = baseValue; }
    get triangle() { return this._triangle; }
    set triangle(triangle) { this._triangle = triangle; }
    get owner() { return this._owner; }
    set owner(owner) { this._owner = owner; }
    get defense() { return this._defense; }
    set defense(defenseValue) { if (defenseValue <= IngameTriangle.MAX_DEFENSE)
        this._defense = defenseValue; }
    /**
     * Creates an returns a new {@code IngameTriangle}. Links the inner {@code Triangle} to this instance.
     * @param innerTriangle to be held by this instance
     * @param owner the player who onws this {@code IngameTriangle}
     */
    static of(innerTriangle, owner) {
        if (!innerTriangle)
            return null;
        let toReturn = new IngameTriangle(innerTriangle, owner);
        toReturn.triangle.ingameTriangle = toReturn;
        return toReturn;
    }
    /**
     * Removes all references of the held inner {@code Triangle}.
     */
    drop() {
        this.triangle.drop();
    }
    /**
     * Replaces the values of the {@IngameTriangle's} attributes with the values from the given one.
     * Note that the underlying geometric {@code Triangle} won't be replaced.
     * @param replacement whose attributes shall replace the current ones
     * @return {@code true} if anything was updated, otherise {@code false}
     */
    updateAttributesWith(replacement) {
        let changed = false;
        if (this.owner != replacement.owner) {
            changed = true;
            this.owner = replacement.owner;
        }
        if (this.baseValue != replacement.baseValue) {
            changed = true;
            this.baseValue = replacement.baseValue;
        }
        if (this.defense != replacement.defense) {
            changed = true;
            this.defense = replacement.defense;
        }
        return changed;
    }
    equals(other) {
        if (other instanceof IngameTriangle) {
            return (this.triangle.equals(other.triangle) && this.owner === other.owner);
        }
        else
            return false;
    }
    toString() {
        return this.owner + ": " + this.triangle;
    }
}
/* harmony export (immutable) */ __webpack_exports__["a"] = IngameTriangle;

/**The maximum defense for a {@code IngameTriangle}*/
IngameTriangle.MAX_DEFENSE = 10;
/**
 * Holds an {@code IngameTriangle} as well as additional infos required to display live feedback.
 */
class PreviewTriangle {
    //    set baseValue( baseValue: number ) { this._ingameTriangle.baseValue = baseValue; }
    constructor(ingameTriangle) {
        this._collisionCosts = 0;
        this._ingameTriangle = ingameTriangle;
        this._defense = ingameTriangle.defense;
    }
    get ingameTriangle() { return this._ingameTriangle; }
    //    set ingameTriangle( ingameTriangle: IngameTriangle ) { this._ingameTriangle = ingameTriangle; }
    get collisionCosts() { return this._collisionCosts; }
    set collisionCosts(collisionCosts) { this._collisionCosts = collisionCosts; }
    get triangle() { return this._ingameTriangle.triangle; }
    get defense() { return this._defense; }
    set defense(defenseValue) { this._defense = defenseValue; }
    get baseValue() { return this._ingameTriangle.baseValue; }
    get owner() { return this._ingameTriangle.owner; }
    ;
    equals(obj) {
        if (!(obj instanceof PreviewTriangle))
            return false;
        if (this == obj)
            return true;
        return this._ingameTriangle.equals(obj._ingameTriangle);
    }
    toString() {
        return this._ingameTriangle.toString();
    }
}
/* harmony export (immutable) */ __webpack_exports__["b"] = PreviewTriangle;



/***/ }),
/* 3 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__logic_geometric_objects__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__board_handling_game_objects__ = __webpack_require__(2);
/*
 * Handles the (de-)serialization of objects.
 */
/*
 * OBJECT SERIALIZATION
 */


class VertexBean {
    constructor(x, y, player) {
        this.x = x;
        this.y = y;
        this.player = player;
    }
    static of(toConvert, player) {
        return new VertexBean(toConvert.x, toConvert.y, player);
    }
    static asVertex(toConvert) {
        let x, y;
        if (toConvert instanceof VertexBean) {
            x = toConvert.x;
            y = toConvert.y;
        }
        else {
            x = toConvert.x;
            y = toConvert.y;
        }
        return new __WEBPACK_IMPORTED_MODULE_0__logic_geometric_objects__["c" /* Vertex */](x, y);
    }
}
/* harmony export (immutable) */ __webpack_exports__["f"] = VertexBean;

class TriangleBean {
    constructor(point1, point2, point3, baseValue, defense, owner) {
        this.point1 = point1;
        this.point2 = point2;
        this.point3 = point3;
        this.owner = owner;
        this.defense = defense;
        this.baseValue = baseValue;
    }
    static of(toConvert) {
        let owner = toConvert.owner;
        let vertices = toConvert instanceof __WEBPACK_IMPORTED_MODULE_1__board_handling_game_objects__["a" /* IngameTriangle */] ?
            toConvert.triangle.vertices : [toConvert.point1, toConvert.point2, toConvert.point3];
        return new TriangleBean(VertexBean.of(vertices[0], owner), VertexBean.of(vertices[1], owner), VertexBean.of(vertices[2], owner), toConvert.baseValue, toConvert.defense, owner);
    }
    static asIngameTriangle(toConvert) {
        let v1, v2, v3;
        let player;
        if (toConvert instanceof TriangleBean) {
            v1 = VertexBean.asVertex(toConvert.point1);
            v2 = VertexBean.asVertex(toConvert.point2);
            v3 = VertexBean.asVertex(toConvert.point3);
            player = toConvert.owner;
        }
        else {
            v1 = VertexBean.asVertex(toConvert.point1);
            v2 = VertexBean.asVertex(toConvert.point2);
            v3 = VertexBean.asVertex(toConvert.point3);
            player = toConvert.player;
        }
        let triangle = __WEBPACK_IMPORTED_MODULE_0__logic_geometric_objects__["b" /* Triangle */].of(v1, v2, v3);
        let ingTriangle = __WEBPACK_IMPORTED_MODULE_1__board_handling_game_objects__["a" /* IngameTriangle */].of(triangle, player);
        ingTriangle.baseValue = toConvert.baseValue;
        ingTriangle.defense = toConvert.defense;
        return ingTriangle;
    }
}
/* harmony export (immutable) */ __webpack_exports__["e"] = TriangleBean;

/**
 * Represents a list of actions issued by the server.
 */
class ServerUpdate {
    get trianglesToAdd() { return this._trianglesToAdd; }
    set trianglesToAdd(trianglesToAdd) { this._trianglesToAdd = trianglesToAdd; }
    get trianglesToRemove() { return this._trianglesToRemove; }
    set trianglesToRemove(trianglesToRemove) { this._trianglesToRemove = trianglesToRemove; }
    /**
     * Returns a JSON view of this object.
     * Due to issues with the private name fields this method must be used to serialize a {@code ServerUpdate} as JSON.
     * @return a JSON view of this object. Note that modifications on this view won't affect the underlying {@code ServerUpdate} object.
     */
    //TODO is this method even required?
    toJson() {
        return {
            trianglesToRemove: this._trianglesToRemove,
            trianglesToAdd: this._trianglesToAdd
        };
    }
    /**
     * Creates a new {@code ServerUpdate} based on a JSON object.
     * @param toConvert holds the information of the {@code ServerUpdate} to be created
     */
    static fromJson(toConvert) {
        let toReturn = new ServerUpdate();
        let toRemove = toConvert.trianglesToRemove.map(t => TriangleBean.of(t));
        let toAdd = toConvert.trianglesToAdd.map(t => TriangleBean.of(t));
        toReturn.trianglesToRemove = toRemove;
        toReturn.trianglesToAdd = toAdd;
        return toReturn;
    }
}
/* harmony export (immutable) */ __webpack_exports__["d"] = ServerUpdate;

/**
 * Holds the complete state of the server and allows to re-build the client's state.
 */
class ResetState {
    get trianglesToCreate() { return this._trianglesToCreate; }
    set trianglesToCreate(trianglesToCreate) {
        this._trianglesToCreate = trianglesToCreate;
    }
    static fromJson(toConvert) {
        let toReturn = new ResetState();
        toReturn.trianglesToCreate = toConvert.trianglesToCreate.map(t => TriangleBean.of(t));
        return toReturn;
    }
    toJson() {
        return {
            trianglesToCreate: this._trianglesToCreate,
            incrementalUpdate: false
        };
    }
}
/* harmony export (immutable) */ __webpack_exports__["c"] = ResetState;

/**
 * Holds the acknowledged player's name.
 */
class InitInfo {
    get playerId() { return this._playerId; }
    set playerId(playerId) { this._playerId = playerId; }
    get initialState() { return this._initialState; }
    set initialState(initialState) { this._initialState = initialState; }
    static fromJson(toConvert) {
        let toReturn = new InitInfo();
        toReturn.playerId = toConvert.playerId;
        toReturn.initialState = ResetState.fromJson(toConvert.initialState);
        return toReturn;
    }
    toJson() {
        return {
            name: this.playerId
        };
    }
}
/* harmony export (immutable) */ __webpack_exports__["b"] = InitInfo;

/**
 * Tells a player's account balance.
 */
class AccountInfo {
    get balance() { return this._balance; }
    set balance(balance) { this._balance = balance; }
    static fromJson(toConvert) {
        let toReturn = new AccountInfo();
        toReturn.balance = toConvert.balance;
        return toReturn;
    }
    toJson() {
        return {
            balance: this.balance
        };
    }
}
/* harmony export (immutable) */ __webpack_exports__["a"] = AccountInfo;



/***/ }),
/* 4 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
//export enum Color {
//    RED = "#ff0000", BLACK = "#000000"
//}
function createOpacitySteps(rgbColor) {
    let toReturn = new Array();
    let rgba = rgbColor.replace("rgb", "rgba");
    for (let alpha = 0; alpha <= 10; alpha += 0.10) {
        toReturn.push(rgba.replace(")", "," + String(alpha) + ")"));
    }
    return toReturn;
}
let greenRGB = "rgb(0,128,0)";
let redRGB = "rgb(220,20,60)";
let blackRGB = "rgb(0,0,0)";
const greenSteps = createOpacitySteps(greenRGB);
const redSteps = createOpacitySteps(redRGB);
const blackSteps = createOpacitySteps(blackRGB);
class Color {
    val(strength) { return strength != null && strength != undefined && strength < this._colors.length ? this._colors[strength] : this._colors[this._colors.length - 1]; }
    constructor(colors) {
        this._colors = colors;
    }
}
/* unused harmony export Color */

const GREEN = new Color(greenSteps);
/* harmony export (immutable) */ __webpack_exports__["b"] = GREEN;

const RED = new Color(redSteps);
/* harmony export (immutable) */ __webpack_exports__["c"] = RED;

const BLACK = new Color(blackSteps);
/* harmony export (immutable) */ __webpack_exports__["a"] = BLACK;



/***/ }),
/* 5 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (immutable) */ __webpack_exports__["c"] = removeFromList;
/* harmony export (immutable) */ __webpack_exports__["b"] = min;
/* harmony export (immutable) */ __webpack_exports__["a"] = max;
function removeFromList(object, list) {
    for (let i = 0; i < list.length; i++) {
        let equal = list[i].equals(object);
        if (equal) {
            list.splice(i, 1);
        }
    }
}
function min(...values) {
    let min = null;
    for (let value of values) {
        min = min == null ? value : Math.min(min, value);
    }
    return min;
}
function max(...values) {
    let max = null;
    for (var value of values) {
        max = max = null ? value : Math.max(max, value);
    }
    return max;
}


/***/ }),
/* 6 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (immutable) */ __webpack_exports__["e"] = getValueOf;
/* harmony export (immutable) */ __webpack_exports__["d"] = getTurnIncomeFor;
/* harmony export (immutable) */ __webpack_exports__["a"] = getCostsFor;
/* harmony export (immutable) */ __webpack_exports__["c"] = getDestructionCostsFor;
/* harmony export (immutable) */ __webpack_exports__["b"] = getDefenseUpgradeCosts;
/* unused harmony export getEffectiveDefense */
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__board_handling_game_objects__ = __webpack_require__(2);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__geometric_objects__ = __webpack_require__(1);
/**
 * Holds all the logic relating players' resources and costs of actions.
 * Balancing adjustments should be done here (in its backend counterpart).
 */
/** Used to scale down all values */


const AREA_FACTOR = .001;
/**
 * Returns the value of an triangle.
 *
 * @param triangle
 *            to be evaluated.
 * @return the {@code Triangle} value
 */
function getValueOf(triangle) {
    let t = triangle instanceof __WEBPACK_IMPORTED_MODULE_1__geometric_objects__["b" /* Triangle */] ? triangle : triangle.triangle;
    let trianglePrice = Math.trunc(Math.max(1, t.getArea() * AREA_FACTOR));
    return (triangle instanceof __WEBPACK_IMPORTED_MODULE_0__board_handling_game_objects__["b" /* PreviewTriangle */]) ? triangle.collisionCosts + trianglePrice : trianglePrice;
}
/**
 * Returns the income a given triangle supplies.
 *
 * @param triangle
 *            whose supplied income is requested
 * @return the supplied income
 */
function getTurnIncomeFor(triangle) {
    let t = triangle instanceof __WEBPACK_IMPORTED_MODULE_0__board_handling_game_objects__["a" /* IngameTriangle */] ? triangle : triangle.ingameTriangle;
    // TODO only 32 bit supports bit shift?
    // will become more complex with more factors later on
    return Math.max(1, t.baseValue >> 2); // atm: 25% of base value
}
/**
 * Returns the creation costs of the given triangle.
 *
 * @param triangle
 *            whose creation costs shall be determined
 * @return the creation costs of the given triangle
 */
function getCostsFor(triangle) {
    return getValueOf(triangle);
}
/**
 * Returns the costs to destroy an triangle.
 * @param triangle whose destruction costs shall be determined
 * @return the costs to destruct the given triangle
 */
function getDestructionCostsFor(triangle) {
    return getValueOf(triangle) + getEffectiveDefense(triangle);
}
/**
 * Returns the costs to upgrade the defense of a given {@code IngameTriangle} by
 * {@code 1}.
 *
 * @param toUpgrade
 *            whose upgrade costs shall be determined
 * @return the costs to upgrade the defense of the given {@code IngameTriangle}
 *         by {@code 1}
 */
function getDefenseUpgradeCosts(toUpgrade) {
    return toUpgrade.baseValue * (toUpgrade.defense + 1) * 4;
}
/**
 * Returns the effective defense value based on a given {@code IngameTriangle's}
 * defense.
 *
 * @param triangle
 *            howse effective defense value shall be determined
 * @return the effective defense value based on the given
 *         {@code IngameTriangle's} defense
 */
function getEffectiveDefense(triangle) {
    return triangle.defense * 10;
}


/***/ }),
/* 7 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* unused harmony export CommandType */
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__run_main__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__network_beans__ = __webpack_require__(3);
/*
 * RESPONSE HANDLING
 */
/**
 * Processes a server command for a given command type.
 */


/**
 * Possible types of response from the server. Tells how to parse and process the server's response.
 */
var CommandType;
(function (CommandType) {
    CommandType["UPDATE"] = "UPDATE";
    CommandType["RESET"] = "RESET";
    CommandType["INIT"] = "INIT";
    CommandType["ACCOUNT_INFO"] = "ACCOUNT_INFO";
})(CommandType || (CommandType = {}));
/**
 * Processes a account inforation from the server.
 */
class AccountHandler {
    constructor() {
        this.accountInfo = __WEBPACK_IMPORTED_MODULE_0__run_main__["setAccount"];
    }
    getType() { return AccountHandler.TYPE; }
    ;
    process(command) {
        let accInfo = __WEBPACK_IMPORTED_MODULE_1__network_beans__["a" /* AccountInfo */].fromJson(command);
        this.accountInfo.call(this, accInfo.balance);
    }
}
/* harmony export (immutable) */ __webpack_exports__["a"] = AccountHandler;

AccountHandler.TYPE = CommandType.ACCOUNT_INFO;
/**
 * Processes a update command from the server.
 */
class UpdateHandler {
    constructor() {
        this.serverUpdate = __WEBPACK_IMPORTED_MODULE_0__run_main__["applyServerUpdate"];
    }
    getType() { return UpdateHandler.TYPE; }
    ;
    process(command) {
        let update = __WEBPACK_IMPORTED_MODULE_1__network_beans__["d" /* ServerUpdate */].fromJson(command);
        let toAdd = update.trianglesToAdd.map(t => __WEBPACK_IMPORTED_MODULE_1__network_beans__["e" /* TriangleBean */].asIngameTriangle(t));
        let toRemove = update.trianglesToRemove.map(t => __WEBPACK_IMPORTED_MODULE_1__network_beans__["e" /* TriangleBean */].asIngameTriangle(t));
        this.serverUpdate.call(this, toRemove, toAdd);
    }
}
/* harmony export (immutable) */ __webpack_exports__["e"] = UpdateHandler;

UpdateHandler.TYPE = CommandType.UPDATE;
/**
 * Processes a reset command from the server.
 */
class ResetHandler {
    constructor() {
        this.serverReset = __WEBPACK_IMPORTED_MODULE_0__run_main__["applyServerReset"];
    }
    getType() { return ResetHandler.TYPE; }
    ;
    process(command) {
        let reset = __WEBPACK_IMPORTED_MODULE_1__network_beans__["c" /* ResetState */].fromJson(command);
        this.serverReset.call(this, reset.trianglesToCreate.map(t => __WEBPACK_IMPORTED_MODULE_1__network_beans__["e" /* TriangleBean */].asIngameTriangle(t)));
    }
}
/* harmony export (immutable) */ __webpack_exports__["d"] = ResetHandler;

ResetHandler.TYPE = CommandType.RESET;
/**
 * Processes a registering acknowledgement from the server.
 */
class InitHandler {
    constructor() {
        this.serverRegister = __WEBPACK_IMPORTED_MODULE_0__run_main__["applyServerRegister"];
    }
    getType() { return InitHandler.TYPE; }
    ;
    process(command) {
        let registration = __WEBPACK_IMPORTED_MODULE_1__network_beans__["b" /* InitInfo */].fromJson(command);
        this.serverRegister.call(this, registration.playerId, registration.initialState.trianglesToCreate.map(t => __WEBPACK_IMPORTED_MODULE_1__network_beans__["e" /* TriangleBean */].asIngameTriangle(t)));
    }
}
/* harmony export (immutable) */ __webpack_exports__["b"] = InitHandler;

InitHandler.TYPE = CommandType.INIT;
/**
* Processes an invalid command from the server by allowing the front end to proceed without waiting for proper server command.
*/
class InvalidCommandHandler {
    constructor() {
        this.serverReset = __WEBPACK_IMPORTED_MODULE_0__run_main__["resume"];
    }
    getType() { return InvalidCommandHandler.TYPE; }
    ;
    process(command) {
        Object(__WEBPACK_IMPORTED_MODULE_0__run_main__["resume"])();
    }
}
/* harmony export (immutable) */ __webpack_exports__["c"] = InvalidCommandHandler;

InvalidCommandHandler.TYPE = undefined;


/***/ }),
/* 8 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__util_shared__ = __webpack_require__(5);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__logic_geometric_objects__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__run_main__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__board_state__ = __webpack_require__(9);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__canvas__ = __webpack_require__(10);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__logic_geometry__ = __webpack_require__(11);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__game_objects__ = __webpack_require__(2);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__logic_accountant__ = __webpack_require__(6);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8__util_colors__ = __webpack_require__(4);









/**
 * Displays the {@code Triangles} and allows new ones to be drawn.
 */
//TODO allow more async action: 
//- don't set waitingforrespons except on reset due to failure
//- estimate costs
//- estimate income?
class CanvasHandler {
    /**
     * Creates a new {@code CanvasHandler}.
     * @param server the connection to the server all communication will be realized with.
     */
    constructor(server) {
        //TODO support touch as well
        this.mouseStart = null;
        this.mouseEnd = null;
        this._state = new __WEBPACK_IMPORTED_MODULE_3__board_state__["a" /* BoardState */]();
        /* Tells whether a triangle was proposed to the server and the response is still pending */
        this.waitingForResponse = true;
        this.server = server;
        //workaround to prevent us from referring to the callback 'this' there
        this.applyServerUpdate = this.applyServerUpdate.bind(this);
        this.mouseDownEvent = this.mouseDownEvent.bind(this);
        this.mouseMoveEvent = this.mouseMoveEvent.bind(this);
        this.mouseUpEvent = this.mouseUpEvent.bind(this);
    }
    get state() {
        return this._state;
    }
    /**
     * Initializes this {@code CanvasHandler}.
     * @param board the canvas this handler will work on
     */
    init(board) {
        this.board = board;
        this.canvas = new __WEBPACK_IMPORTED_MODULE_4__canvas__["a" /* Canvas */](board);
        this.geoCalc = new __WEBPACK_IMPORTED_MODULE_5__logic_geometry__["a" /* Geometry */](this.canvas);
        this._state.canvas = this.board.getContext("2d");
        if (!this.canvas) {
            alert("Could not get the canvas element!");
            return;
        }
        // this.canvasContext.translate( 0.5, 0.5 );
        this.board.addEventListener("mousedown", this.mouseDownEvent);
        this.board.addEventListener("mouseup", this.mouseUpEvent);
        this.board.addEventListener("mousemove", this.mouseMoveEvent);
        this.board.addEventListener("mouseover", () => event.preventDefault());
        this.liveInfo = new LiveInfo(this.board);
        this.server.registerPlayer();
    }
    /**
     * Finalizies the registration of this board an fetches the initial information. This method is supposed to be invoked by a server's command.
     * @param name the player is registered with at the server
     */
    register(name, toCreate) {
        this._state.playerId = name;
        this.applyServerReset(toCreate);
    }
    /**
     * Creates a new {@code Triangle} and registers it for the owner. Note that the outline won't be updated here.
     * @param relatedEdge which forms the {@code Triangle} together with its predecessor and successor
     * @param owner the {@code Triangle} belongs to and will be registered for
     */
    registerTriangle(toRegister) {
        this.state.getTrianglesByOwner(toRegister.owner).push(toRegister);
    }
    /**
     * Unregisters a given {@code Triangle} from neighbors and other fields. Note that the outline won't be updated here.
     * @param toRemove the {@code Triangle} to be dropped and unregistered
     */
    dropAndUnregisterTriangle(toRemove) {
        for (let edge of toRemove.triangle.edges) {
            let twin = edge.getTwinEdge();
            if (twin)
                this.state.playerOutline.push(twin);
        }
        Object(__WEBPACK_IMPORTED_MODULE_0__util_shared__["c" /* removeFromList */])(toRemove, this.state.getTrianglesByOwner(toRemove.owner));
        toRemove.drop();
    }
    /**
     * Starts the process which creates/drawes a new {@code Triangle}.
     * @param e the mouse event triggering this method
     */
    mouseDownEvent(e) {
        event.preventDefault();
        if (this.waitingForResponse)
            return;
        this.mouseStart = new __WEBPACK_IMPORTED_MODULE_1__logic_geometric_objects__["c" /* Vertex */](e.pageX - this.board.offsetLeft, e.pageY - this.board.offsetTop);
        if (!this.geoCalc.getContainingTriangle(this.mouseStart, this._state)) {
            this.mouseStart = null;
            this.mouseEnd = null;
            return;
        }
        this.mouseEnd = null;
    }
    /**
     * Processes the current drawn line to calculate a new {@code Triangle}.
     * @param e the mouse event triggering this method
     */
    mouseMoveEvent(e) {
        event.preventDefault();
        if (this.waitingForResponse)
            return;
        if (!this.mouseStart) {
            this.hoveredTriangle = this.getContainingTriangle(new __WEBPACK_IMPORTED_MODULE_1__logic_geometric_objects__["c" /* Vertex */](e.pageX - this.board.offsetLeft, e.pageY - this.board.offsetTop));
            if (this.hoveredTriangle && this.hoveredTriangle.owner === this.state.playerId)
                this.updateUpgradeTriangleLiveInfos();
            return;
        }
        else
            this.hoveredTriangle = null; //TODO count as click even when mouseStart exists, as long as it is close by an in the same triangle?
        this.canvas.drawCanvas(this.state);
        this.mouseEnd = new __WEBPACK_IMPORTED_MODULE_1__logic_geometric_objects__["c" /* Vertex */](e.pageX - this.board.offsetLeft, e.pageY - this.board.offsetTop);
        if (__WEBPACK_IMPORTED_MODULE_2__run_main__["debug"] == 2) {
            this.canvas.drawLine(this.mouseStart, this.mouseEnd);
        }
        else {
            this.previewTriangle = __WEBPACK_IMPORTED_MODULE_6__game_objects__["a" /* IngameTriangle */].of(this.geoCalc.createPreviewTriangle(this.mouseStart, this.mouseEnd, this._state), this._state.playerId);
            if (this.previewTriangle) {
                if (this.validatePreviewTriangle())
                    this.canvas.draw(this.previewTriangle);
            }
        }
        this.updatePreviewTriangleLiveInfos();
    }
    /**
     * Validates a given preview triangle and update information about the costs etc.
     */
    validatePreviewTriangle() {
        //TODO is all here the canvas handler's job to determine?
        let collisions = this.geoCalc.intersectingTriangles(this.previewTriangle.triangle, this.state);
        let destructionCosts = 0;
        for (let collision of collisions) {
            if (collision.ingameTriangle.owner === this.state.playerId)
                return false;
            destructionCosts += Object(__WEBPACK_IMPORTED_MODULE_7__logic_accountant__["c" /* getDestructionCostsFor */])(collision.ingameTriangle);
        }
        if (this.previewTriangle instanceof __WEBPACK_IMPORTED_MODULE_6__game_objects__["a" /* IngameTriangle */])
            this.previewTriangle = new __WEBPACK_IMPORTED_MODULE_6__game_objects__["b" /* PreviewTriangle */](this.previewTriangle);
        this.previewTriangle.collisionCosts = destructionCosts;
        this.updatePreviewTriangleLiveInfos();
        return true;
    }
    /**
     * Checks whether a valid {@code Triangle} can be created from the drawn line. If that's the case, the new {@code Triangle} will be created.
     * @param e the mouse event triggering this method
     */
    mouseUpEvent(e) {
        event.preventDefault();
        this.liveInfo.setHidden(true);
        if (this.waitingForResponse)
            return;
        if (!this.mouseEnd)
            this.mouseEnd = new __WEBPACK_IMPORTED_MODULE_1__logic_geometric_objects__["c" /* Vertex */](e.pageX - this.board.offsetLeft, e.pageY - this.board.offsetTop);
        if (this.hoveredTriangle && this.hoveredTriangle.owner === this.state.playerId) {
            //check whether start was in the same triangle?
            if (this.hoveredTriangle.defense < __WEBPACK_IMPORTED_MODULE_6__game_objects__["a" /* IngameTriangle */].MAX_DEFENSE) {
                let upgradeProposal = new __WEBPACK_IMPORTED_MODULE_6__game_objects__["b" /* PreviewTriangle */](this.hoveredTriangle);
                let upgradeCosts = Object(__WEBPACK_IMPORTED_MODULE_7__logic_accountant__["b" /* getDefenseUpgradeCosts */])(this.hoveredTriangle);
                if (upgradeCosts <= this.state.balance) {
                    upgradeProposal.defense++;
                    this.server.proposeTriangle(upgradeProposal.ingameTriangle);
                    this.state.balance -= upgradeCosts; //rejected upgrade costs reset automatically
                }
            }
            this.mouseStart = null;
            this.mouseEnd = null;
            return;
        }
        if (__WEBPACK_IMPORTED_MODULE_2__run_main__["debug"] == 2) {
            this.previewTriangle = __WEBPACK_IMPORTED_MODULE_6__game_objects__["a" /* IngameTriangle */].of(this.geoCalc.createPreviewTriangle(this.mouseStart, this.mouseEnd, this._state), this._state.playerId);
            if (this.previewTriangle)
                this.canvas.draw(this.previewTriangle);
        }
        if (__WEBPACK_IMPORTED_MODULE_2__run_main__["debug"])
            this.previewTriangle = __WEBPACK_IMPORTED_MODULE_6__game_objects__["a" /* IngameTriangle */].of(this.geoCalc.createPreviewTriangle(this.mouseStart, this.mouseEnd, this._state), this._state.playerId);
        if (this.previewTriangle) {
            let creationCosts = Object(__WEBPACK_IMPORTED_MODULE_7__logic_accountant__["a" /* getCostsFor */])(this.previewTriangle);
            if (creationCosts <= this.state.balance) {
                this.server.proposeTriangle(this.previewTriangle instanceof __WEBPACK_IMPORTED_MODULE_6__game_objects__["b" /* PreviewTriangle */] ? this.previewTriangle.ingameTriangle : this.previewTriangle);
                this.state.balance -= creationCosts; //rejected createion costs reset automatically
            }
            this.canvas.drawCanvas(this.state);
            this.previewTriangle = null;
        }
        this.mouseStart = null;
        this.mouseEnd = null;
    }
    /**
     * Removes and adds the specified {@code Triangles}. This method is supposed to be invoked by a server's command.
     * @param toRemove {@code Triangles} which shall be removed. Will be processed first.
     * @param toAdd {@code Triangles} which shall be added. Will be processed second and therefore should not rely {@code Triangles} ought to be removed.
     */
    applyServerUpdate(toRemove, toAdd) {
        for (let remove of toRemove) {
            let success = this.removeServerTriangle(remove);
            if (!success) {
                this.server.requestCompleteState();
                this.waitingForResponse = true;
                return;
            }
        }
        for (let add of toAdd) {
            let success = this.addServerTriangle(add);
            if (!success) {
                this.server.requestCompleteState();
                this.waitingForResponse = true;
                return;
            }
        }
        this.finalizeResponseHandling();
    }
    /**
     * Resets the state and replaces it with the given {@code Triangles}. This method is supposed to be invoked by a server's command.
     * @param toCreate {@code Triangles} constituting the new state. They will be constructed in the given order.
     */
    applyServerReset(toCreate) {
        this._state.playerTriangles = [];
        this._state.otherTriangles = [];
        this._state.playerOutline = [];
        this._state.otherOutline = [];
        for (let nextToCreate of toCreate) {
            let success = this.addServerTriangle(nextToCreate);
            if (!success) {
                alert("Reset failed, stopping game.");
                return;
            }
        }
        this.finalizeResponseHandling();
    }
    /**Cancels any blockings of user actions*/
    resume() {
        this.finalizeResponseHandling();
    }
    /**
     * Sets the players account to the given balance.
     * @param balance the account shall be set to
     */
    setAccount(balance) {
        this.state.balance = balance;
        if (this.mouseStart)
            this.updatePreviewTriangleLiveInfos();
        else
            this.updateUpgradeTriangleLiveInfos();
    }
    /**
     * Returns {@code IngameTriangle} containing the {@code Vertex} (most likely the mouse position)
     * @param vertex the mouse position a matching {@code IngameTriangle} will be searched for
     * @return the {@code IngameTriangle} containing the given {@code Vertex}
     */
    getContainingTriangle(vertex) {
        for (let triangle of this.state.playerTriangles)
            if (triangle.triangle.contains(vertex))
                return triangle;
        for (let triangle of this.state.otherTriangles)
            if (triangle.triangle.contains(vertex))
                return triangle;
        return null;
    }
    /**
     * Updates teh live infos based on the triangle the mouse if hovering over
     */
    //TODO refactor with the one below ... put a method in LiveInfo?
    updateUpgradeTriangleLiveInfos() {
        if (this.hoveredTriangle && this.hoveredTriangle.defense < __WEBPACK_IMPORTED_MODULE_6__game_objects__["a" /* IngameTriangle */].MAX_DEFENSE) {
            let center = this.hoveredTriangle.triangle.getCentroid();
            let costs = Object(__WEBPACK_IMPORTED_MODULE_7__logic_accountant__["b" /* getDefenseUpgradeCosts */])(this.hoveredTriangle);
            this.liveInfo.setColor(costs <= this.state.balance ? __WEBPACK_IMPORTED_MODULE_8__util_colors__["a" /* BLACK */].val() : __WEBPACK_IMPORTED_MODULE_8__util_colors__["c" /* RED */].val());
            this.liveInfo.setHidden(false);
            this.liveInfo.setActionInfo("Upgrade defense", costs);
            this.liveInfo.moveTo(center);
        }
        else
            this.liveInfo.setHidden(true);
    }
    /**
     * Updates the live infos based on the preview triangle.
     */
    updatePreviewTriangleLiveInfos() {
        if (this.previewTriangle) {
            let center = this.previewTriangle.triangle.getCentroid();
            let costs = Object(__WEBPACK_IMPORTED_MODULE_7__logic_accountant__["a" /* getCostsFor */])(this.previewTriangle);
            this.liveInfo.setColor(costs <= this.state.balance ? __WEBPACK_IMPORTED_MODULE_8__util_colors__["a" /* BLACK */].val() : __WEBPACK_IMPORTED_MODULE_8__util_colors__["c" /* RED */].val());
            this.liveInfo.setHidden(false);
            this.liveInfo.setActionInfo("Create triangle", costs);
            this.liveInfo.moveTo(center);
        }
        else
            this.liveInfo.setHidden(true);
    }
    /**
     * Removes a given {@code Triangle} from the board.
     * @param toRemove {@code Triangle} to be removed
     * @return {@code true} if the {@code Triangle} was removed successfully, otherwise {@code false}
     */
    removeServerTriangle(toRemove) {
        let existing = this.findExistingTriangle(toRemove);
        if (!existing) {
            return false;
        }
        let newOutline = toRemove.triangle.edges.filter(t => t.getTwinEdge()).map(t => t.getTwinEdge());
        this.updateOutline(toRemove.owner, toRemove.triangle.edges, newOutline);
        this.dropAndUnregisterTriangle(existing);
        return true;
    }
    /**
     * Creates a {@code Triangle} received from the server or updates a existing one.
     * @param assignedTriangle the {@code Triangle} received from the server
     * @return {@code true} if the {@code Triangle} was successfully added or was used for an update,
     * otherwise {@code false}
     */
    addServerTriangle(assignedTriangle) {
        if (!assignedTriangle || !assignedTriangle.owner || !assignedTriangle.triangle.relatedEdge
            || assignedTriangle.triangle.edges.find(e => !e || !e.prevEdge || !e.nextEdge)) {
            this.finalizeResponseHandling();
            return false;
        }
        let existing = this.findExistingTriangle(assignedTriangle);
        if (existing) {
            existing.updateAttributesWith(assignedTriangle);
            return true;
        }
        let toRemove = new Array();
        //find proper twin edges in old outline
        let outlines = this.state.getMergedOutlines();
        for (let outlineEdge of outlines) {
            for (let newEdge of assignedTriangle.triangle.edges) {
                if (newEdge.isProperTwinFor(outlineEdge)) {
                    if (assignedTriangle.owner != outlineEdge.relatedTriangle.ingameTriangle.owner) {
                        this.finalizeResponseHandling();
                        return false;
                    }
                    else {
                        newEdge.linkAsTwins(outlineEdge);
                        toRemove.push(outlineEdge);
                    }
                }
            }
        }
        let toAdd = new Array();
        for (let edge of assignedTriangle.triangle.edges) {
            if (!edge.getTwinEdge())
                toAdd.push(edge);
        }
        //do not check for floating triangles anymore since this can happen by deletions
        // let trianglesOfPlayer: Triangle[] = this.state.getTrianglesByOwner( assignedTriangle.owner ).filter( t => t.owner === assignedTriangle.owner );
        // if ( trianglesOfPlayer.length > 0 && toAdd.length > 2 ) { // illegal: triangle is not connected
        //     this.finalizeResponseHandling();
        //     return false;
        // }
        this.registerTriangle(assignedTriangle);
        this.updateOutline(assignedTriangle.owner, toRemove, toAdd);
        this.finalizeResponseHandling();
        return true;
    }
    /**
     * Refreshes the canvas and allows further player actions.
     */
    finalizeResponseHandling() {
        this.canvas.drawCanvas(this.state);
        this.waitingForResponse = false;
    }
    //TODO drop this.previewTriangle on failure to create a new one ... still required?
    /**
     * Highlights the polygon's outline of a given player
     * @param player whose polygon outline shall be highlighted
     */
    highlightOutline(player) {
        for (let edge of this._state.playerOutline) {
            this.canvas.highlight(edge);
        }
    }
    /**
     * Reverts the higlighing of a player's polygon outline
     * @param player whose polygon outline shall be unhighlighted
     */
    unhighlightOutline(player) {
        for (let edge of this._state.playerOutline) {
            this.canvas.unhighlight(edge);
        }
    }
    /**
     * Updates the outline of a player's polygon by {@code Edges} which shall be removed or added
     * @param player whose polygon outline shall be updated
     * @param toRemove {@code Edges} which will be removed from the player's polygon outline
     * @param toAdd {@code Edges} which will be added to the player's polygon outline
     */
    updateOutline(player, toRemove, toAdd) {
        // could be optimized by utilizing the splicing option ...
        let outline = player === this._state.playerId ? this._state.playerOutline : this._state.otherOutline;
        for (let edge of toRemove)
            Object(__WEBPACK_IMPORTED_MODULE_0__util_shared__["c" /* removeFromList */])(edge, outline);
        if (player === this._state.playerId)
            this._state.playerOutline = outline.concat(toAdd);
        else
            this._state.otherOutline = outline.concat(toAdd);
    }
    /**
     * Starting from a given {@code Edge} an existing twin {@code Edge} candidate will be searched.
     * @param edge a twin {@code Edge} shall be searched for
     * @param nextEdgeSelectionFunction describes how the mesh of {@code Edges} will be travered to find a twin.
     * Expected to either return the previous or the next connected {@code Edge} to work properly.
     * @return the matching twin {@code Edge} candidate, if any, otherwise {@code null}
     */
    findExistingTwin(edge, nextEdgeSelectionFunction) {
        // if (debug) for (triangle of triangles) console.log(triangle.toString());
        this.unhighlightOutline(this._state.playerId);
        /*
         * if (debug){ edge.draw(Color.RED); edge.draw(Color.BLACK); }
         */
        let currentEdge = nextEdgeSelectionFunction.call(this, edge);
        if (!currentEdge)
            return null;
        /*
         * if (debug && currentEdge) currentEdge.draw(Color.RED); if (debug &&
         * currentEdge) currentEdge.draw(Color.BLACK);
         */
        while (!edge.isProperTwinFor(currentEdge)) {
            if (!currentEdge.getTwinEdge())
                return null;
            currentEdge = nextEdgeSelectionFunction.call(this, currentEdge.getTwinEdge());
            //            if ( debug && currentEdge ) {
            //                this.canvas.highlight(currentEdge);
            //                 console.log(currentEdge.toString());
            //                this.canvas.unhighlight(currentEdge);
            //            }
        }
        return currentEdge;
    }
    /**
     * Searches for an existing {@code Triangle} which matches the given one's geometry and owner.
     * @param findExistingFor {@code Triangle} a existing one shall be searched for
     * @return an matching existing {@code Triangle}, if any, otherwse {@code null}
     */
    findExistingTriangle(findExistingFor) {
        for (let otherTriangle of this.state.getTrianglesByOwner(findExistingFor.owner)) {
            if (findExistingFor.equals(otherTriangle))
                return otherTriangle;
        }
        return null;
    }
}
/* harmony export (immutable) */ __webpack_exports__["a"] = CanvasHandler;

/**
 * The field displaying live information.
 */
class LiveInfo {
    constructor(board) {
        let anchor = document.getElementById("offset_anchor");
        this.verboseField = document.getElementById("liveinfo");
        let liveField = document.createElement("div");
        liveField.style.background = "transparent";
        liveField.style.color = "black";
        liveField.style.position = "absolute";
        liveField.style.userSelect = "none";
        liveField.style.pointerEvents = "none";
        liveField.addEventListener("mouseup", () => event.preventDefault());
        liveField.addEventListener("mousedown", () => event.preventDefault());
        liveField.addEventListener("mousemove", () => event.preventDefault());
        liveField.addEventListener("click", () => event.preventDefault());
        liveField.addEventListener("mouseover", () => event.preventDefault());
        document.body.insertBefore(liveField, board);
        this.liveField = liveField;
        this.board = board;
        this.offsetY = anchor.offsetTop;
        this.offsetX = anchor.offsetLeft;
        liveField.hidden = true;
    }
    setHidden(hide) {
        this.liveField.hidden = hide;
    }
    isHidden() {
        return this.liveField.hidden;
    }
    setText(text) {
        this.liveField.textContent = String(text);
        this.verboseField.textContent = String(text);
    }
    setActionInfo(action, costs) {
        this.liveField.textContent = String(costs);
        this.verboseField.textContent = action + ": " + String(costs);
    }
    getText() {
        return this.liveField.textContent;
    }
    setColor(color) {
        this.liveField.style.color = color;
    }
    //TODO still not perfect positioning
    moveTo(center) {
        let posX = center.x - this.offsetX + "px";
        let poxY = center.y - this.offsetY + "px";
        let trans = "translate(" + posX + "," + poxY + ")";
        this.liveField.style.transform = trans;
    }
}


/***/ }),
/* 9 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__util_colors__ = __webpack_require__(4);

/**
 * Data class which holds the state of the board.
 */
class BoardState {
    constructor() {
        /**The color representing the player*/
        this.playerColor = __WEBPACK_IMPORTED_MODULE_0__util_colors__["b" /* GREEN */];
        /**The color representing the other players*/
        this.otherColor = __WEBPACK_IMPORTED_MODULE_0__util_colors__["c" /* RED */];
        /**{@code Triangles} belong to the player */
        this.playerTriangles = [];
        /**{@code Triangles} belong to the other players */
        this.otherTriangles = [];
        /** The the outlines of the player's {@code Triangles} */
        this.playerOutline = [];
        /** The the outlines of all other players' {@code Triangles} */
        this.otherOutline = [];
        /** Money of the player.*/
        this._balance = 0;
    }
    get balance() { return this._balance; }
    set balance(balance) { this._balance = balance; }
    /**
     * Returns all {@code Edges} from all players that represent an outline.
     * @return List of all {@code Edges} belonging to an outline
     */
    getMergedOutlines() {
        return this.playerOutline.concat(this.otherOutline);
    }
    /**
     * Returns all {@code Triangles} belonging to the player or all {@code Triangles} belonging to the other players,
     * depending on the requested owner.
     * @param owner used to determine whethere the player's {@code Triangles} or the others' {@code Triangles} shall be returned.
     * @return the {@code Triangles}
     */
    getTrianglesByOwner(owner) {
        return owner === this.playerId ? this.playerTriangles : this.otherTriangles;
    }
    /**
     * Returns all existing {@code Edges}.
     * @return all existing {@code Edges}.
     */
    getAllEdges(owner) {
        let list1 = (!owner || owner === this.playerId) ? this.playerTriangles : [];
        let list2 = !owner ? this.otherTriangles : [];
        return list1.concat(list2).map(t => t.triangle.edges).reduce((x, y) => x.concat(y), []);
    }
    getAllOtherEdges() {
        return this.otherTriangles.map(t => t.triangle.edges).reduce((x, y) => x.concat(y), []);
    }
}
/* harmony export (immutable) */ __webpack_exports__["a"] = BoardState;



/***/ }),
/* 10 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__logic_geometric_objects__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__util_colors__ = __webpack_require__(4);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__game_objects__ = __webpack_require__(2);



/**
 * Draws the elements.
 */
class Canvas {
    constructor(canvas, highlightColor) {
        this.canvasContext = canvas.getContext("2d");
        this.canvas = canvas;
        this.backgroundColor = this.canvasContext.fillStyle;
        this.lineColor = this.canvasContext.strokeStyle;
        this.highlightColor = highlightColor ? highlightColor : __WEBPACK_IMPORTED_MODULE_1__util_colors__["c" /* RED */];
    }
    /**
     * Cleans and redraws the whole canvas.
     */
    drawCanvas(state) {
        this.canvasContext.clearRect(0, 0, this.canvas.width, this.canvas.height); //use backgroundColor?
        for (let triangle of state.playerTriangles) {
            this.fill(triangle, state.playerColor);
        }
        for (let triangle of state.otherTriangles) {
            this.fill(triangle, state.otherColor);
        }
    }
    draw(toDraw, color) {
        if (toDraw instanceof __WEBPACK_IMPORTED_MODULE_0__logic_geometric_objects__["c" /* Vertex */]) {
            this.canvasContext.fillRect(toDraw.x, toDraw.y, 1, 1);
        }
        else if (toDraw instanceof __WEBPACK_IMPORTED_MODULE_0__logic_geometric_objects__["a" /* Edge */]) {
            let oldColor = this.canvasContext.strokeStyle;
            if (color)
                this.canvasContext.strokeStyle = color.val();
            //            this.canvasContext.lineWidth = this.highlightColor == color && debug > 0 ? 5 : 1;
            this.canvasContext.beginPath();
            this.canvasContext.moveTo(toDraw.startVertex.x, toDraw.startVertex.y);
            this.canvasContext.lineTo(toDraw.nextEdge.startVertex.x, toDraw.nextEdge.startVertex.y);
            this.canvasContext.stroke();
            this.canvasContext.strokeStyle = oldColor;
        }
        else if (toDraw instanceof __WEBPACK_IMPORTED_MODULE_0__logic_geometric_objects__["b" /* Triangle */]) {
            for (let edge of toDraw.edges)
                this.draw(edge, color);
        }
        else if (toDraw instanceof __WEBPACK_IMPORTED_MODULE_2__game_objects__["a" /* IngameTriangle */] || toDraw instanceof __WEBPACK_IMPORTED_MODULE_2__game_objects__["b" /* PreviewTriangle */]) {
            this.draw(toDraw.triangle, color);
        }
    }
    fill(toFill, fillColor) {
        let strength = toFill instanceof __WEBPACK_IMPORTED_MODULE_0__logic_geometric_objects__["b" /* Triangle */] ? 0 : toFill.defense;
        this.canvasContext.fillStyle = fillColor.val(strength);
        let vertices = toFill instanceof __WEBPACK_IMPORTED_MODULE_0__logic_geometric_objects__["b" /* Triangle */] ? toFill.vertices : toFill.triangle.vertices;
        this.canvasContext.beginPath();
        this.canvasContext.moveTo(vertices[0].x, vertices[0].y);
        this.canvasContext.lineTo(vertices[1].x, vertices[1].y);
        this.canvasContext.lineTo(vertices[2].x, vertices[2].y);
        this.canvasContext.closePath();
        this.canvasContext.stroke();
        this.canvasContext.fill();
    }
    drawLine(from, to) {
        this.canvasContext.moveTo(from.x, from.y);
        this.canvasContext.lineTo(to.x, to.y);
        this.canvasContext.stroke();
    }
    highlight(toDraw) {
        if (toDraw instanceof __WEBPACK_IMPORTED_MODULE_0__logic_geometric_objects__["c" /* Vertex */]) {
            var oldColor = this.canvasContext.fillStyle;
            this.canvasContext.fillStyle = this.highlightColor.val();
            this.canvasContext.beginPath();
            this.canvasContext.arc(toDraw.x, toDraw.y, 5, 0, 2 * Math.PI);
            this.canvasContext.fill();
            this.canvasContext.fillStyle = oldColor;
        }
        else if (toDraw instanceof __WEBPACK_IMPORTED_MODULE_0__logic_geometric_objects__["a" /* Edge */]) {
            this.draw(toDraw, this.highlightColor);
        }
        else if (toDraw instanceof __WEBPACK_IMPORTED_MODULE_2__game_objects__["a" /* IngameTriangle */]) {
            this.draw(toDraw, this.highlightColor);
        }
    }
    unhighlight(toUnhighlight) {
        if (toUnhighlight instanceof __WEBPACK_IMPORTED_MODULE_0__logic_geometric_objects__["c" /* Vertex */]) {
            throw "Unhighlighing vertices is not supported.";
        }
        else
            this.draw(toUnhighlight);
    }
}
/* harmony export (immutable) */ __webpack_exports__["a"] = Canvas;



/***/ }),
/* 11 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__logic_geometric_objects__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__run_main__ = __webpack_require__(0);


/**
 * Holds functionality to create {@code Triangles} based on the user input and the current {@code BoardState}.
 */
class Geometry {
    /**
     * Creates a new {@Geometry} instance.
     * @param canvas mostly for debug, allows highlighting some elements
     */
    constructor(canvas) {
        this.canvas = canvas;
    }
    /**
     * Creates a preview {@code Triangle} based on the line the player is drawing.
     * Note that no in-depth validation for costs or collisions with {@code Triangles} will be taken
     * into account here and there must be done separatly, if required.
     * This {@code Triangle} will use the first intersected edge as well as any second one
     * (if both are connected and therefore form two sides of the {@code Triangle}.
     */
    createPreviewTriangle(mouseStart, mouseEnd, state) {
        if (!mouseStart) {
            return null;
        }
        let involvedEdges = this.getEdgeIntersectionInfo(new __WEBPACK_IMPORTED_MODULE_0__logic_geometric_objects__["a" /* Edge */](mouseStart, null, new __WEBPACK_IMPORTED_MODULE_0__logic_geometric_objects__["a" /* Edge */](mouseEnd, null, null)), state);
        let ownEdges = involvedEdges.getOwnEnclosingEdges();
        //find first outline edge
        let startEdge = ownEdges[0];
        // no edges involved or line starts in a foreign triangle
        if (!startEdge) {
            return null;
        }
        let closestEdge = ownEdges[1];
        // if there's only a start edge an the distance between start edge and start edge is to small -> return null to avoid micro triangles
        if (!closestEdge && startEdge.sqrDistanceToPoint(mouseEnd) <= __WEBPACK_IMPORTED_MODULE_1__run_main__["epsilon"] * __WEBPACK_IMPORTED_MODULE_1__run_main__["minRangeMultiplier"])
            return null;
        if (startEdge.sqrDistanceToPoint(mouseEnd) < __WEBPACK_IMPORTED_MODULE_1__run_main__["epsilon"]) {
            return null;
        }
        // TODO use verticesToEdges
        let previewTriangle;
        let firstEdge;
        let secondEdge;
        let thirdEdge;
        /** the first edge of two: the startEdge and another one, if any */
        let firstConnectedEdge;
        // If there's no other intersected edge to connect with, search in proxmity
        if (!closestEdge && __WEBPACK_IMPORTED_MODULE_1__run_main__["epsilon"] > 0) {
            let inProximity = this.findNearEdges(mouseEnd, state);
            for (let edge of inProximity) {
                firstConnectedEdge = this.checkConnection(startEdge, edge);
                if (firstConnectedEdge) {
                    closestEdge = edge;
                    // involvedEdges[1] = closestEdge;
                    break;
                }
            }
            // if there is another edge met with does not share a vertex:
            // check if it's vertix is near. if that's the case, connect to
            // this vertex, else don't allow the drawn triangle
            if (inProximity.length > 0 && !closestEdge) {
                previewTriangle = this.tryToConnectWithVertex(startEdge, inProximity, state, mouseEnd);
                if (!previewTriangle) {
                    return null;
                }
            }
        }
        if (!previewTriangle) {
            // if two outlines involved -> if they are connected, create a triangle
            // from there
            if (closestEdge) {
                previewTriangle = this.tryToConnectIntersections(startEdge, closestEdge, state);
            }
            else {
                firstEdge = __WEBPACK_IMPORTED_MODULE_0__logic_geometric_objects__["a" /* Edge */].createTwinModel(startEdge);
                secondEdge = new __WEBPACK_IMPORTED_MODULE_0__logic_geometric_objects__["a" /* Edge */](startEdge.startVertex, firstEdge, null);
                thirdEdge = new __WEBPACK_IMPORTED_MODULE_0__logic_geometric_objects__["a" /* Edge */](mouseEnd, secondEdge, firstEdge);
                firstEdge.prevEdge = thirdEdge;
                firstEdge.nextEdge = secondEdge;
                secondEdge.nextEdge = thirdEdge;
                previewTriangle = new __WEBPACK_IMPORTED_MODULE_0__logic_geometric_objects__["b" /* Triangle */](firstEdge);
            }
        }
        // Check whether the triangle intersects with another one. If
        // that's the case deregister all set twins and cancel
        // Create new, registered triangle
        if (!previewTriangle) {
            return null;
        }
        return previewTriangle;
    }
    /**
     * Returns all {@code Edges} which are intersected, in the order or their intersection. This intersections might represent the edges of a new {@code Triangle}.
     * @param drawnEdge a line which will be checked whether it intersects existing {@code Edges}
     * @return the intersecting {@code Edges}, in the order of their intersection
     */
    //FIXME this line is not enough to tell about all triangles which must be deleted
    getEdgeIntersectionInfo(drawnEdge, state) {
        // TODO use player? Maybe: use playerOutline + all edges of other player's triangles 
        // since they're destroyable. Then: return all outlines, orderd, to allow a possible destruction
        // of foreign triangles. More performant: check all enemies' edges as soon as an enemy's outline
        // has been crossed, but not before (=>concat list with all foreing outline edges with those which have a twin)
        let allEdges = state.playerOutline.concat(state.getAllOtherEdges());
        let startVertex = drawnEdge.startVertex;
        //        if ( debug ) for ( let edge of outline ) edge.draw( this.canvasContext, Color.RED );
        let result = new EdgeIntersections(state.playerId);
        //        if ( debug ) for ( let edge of outline ) edge.draw( this.canvasContext, Color.BLACK );
        for (let line of allEdges) {
            //            if ( debug ) line.draw( this.canvasContext, Color.RED );
            if (drawnEdge.isIntersecting(line)) {
                let distance = drawnEdge.sqrDistanceToPoint(startVertex);
                result.processEdge(new EdgeAndDistance(line, distance));
            }
            //            if ( debug ) line.draw( this.canvasContext, Color.BLACK );
        }
        return result.finalize();
    }
    /**
     * Searches for {@code Edges} close to a given {@code Vertex}. Used to achieve a "magnetism" effect when drawing {@code Triangles} close to existing ones.
     * @param mouseEnd the {@code Vertex} in whose proximity {@code Edges} will be searched
     * @return all {@code Edges} owned by the given player in proximity of the given {@code Vertex}
     */
    findNearEdges(mouseEnd, state) {
        let inProximity = [];
        for (let line of state.playerOutline) {
            //            if ( debug > 0 ) line.draw( this.canvasContext, Color.RED );
            let proximity = line.sqrDistanceToPoint(mouseEnd);
            if (proximity < __WEBPACK_IMPORTED_MODULE_1__run_main__["epsilon"])
                inProximity.push(line);
            //            if ( debug > 0 ) line.draw( this.canvasContext, Color.BLACK );
        }
        if (__WEBPACK_IMPORTED_MODULE_1__run_main__["debug"] > 0 && this.canvas)
            for (let edge of inProximity)
                this.canvas.highlight(edge);
        return inProximity;
    }
    /**
     * Checks whether two edges share a vertex.
     * @param edge1 the first {@code Edge}, checked whether it ist connected with the second one
     * @param edge2 the second {@code Edge}, checked whether it ist connected with the first one
     * @return The first {@code Edge} of the chain, if both {@code Edges} are connected, therwise {@code null}
     */
    checkConnection(edge1, edge2) {
        if (edge1.startVertex.equals(edge2.nextEdge.startVertex))
            return edge1;
        else if (edge1.nextEdge.startVertex.equals(edge2.startVertex))
            return edge2;
        else
            return null;
    }
    /**
     * Checks all {@code Triangle} for intersection and returns those.
     * @param triangle to be checked for intersection with any {@code Triangles}
     * @return all {@code Triangles} which intersects with the given one
     */
    intersectingTriangles(triangle, state) {
        let collision = [];
        for (let edge of state.getAllEdges()) {
            for (let triangleEdge of triangle.edges) {
                if (triangleEdge.isIntersecting(edge)) {
                    if (collision.indexOf(edge.relatedTriangle) < 0)
                        collision.push(edge.relatedTriangle);
                    break;
                }
            }
        }
        return collision;
    }
    /**
     * Tries to form a {@code Triangle} by "magnetism" towards a {@code Vertex}. That means, if possible,
     * a {@code Triangle} will be created by a given {@code Edge} and two other ones connecting to a {@code Vertex} in proximity.
     * @param startEdge the basis. Might form a {@code Triangle} together with a {@code Vertex} which does not lie on this {@code Edge}.
     * @param inProximity {@code Edges} whose {@code Vertices} will be checked whether they form a {@code Triangle} together with the given startEdge
     * @return a preview triangle formed by the given basis {@code Edge} and the closest {@code Vertex} in proximity, if any, otherwise {@code null}
     */
    tryToConnectWithVertex(startEdge, inProximity, state, mouseEnd) {
        /** first edge of the the newly created triangle, if any */
        let firstEdge;
        /** second edge of the the newly created triangle, if any */
        let secondEdge;
        /** second edge of the the newly created triangle, if any */
        let thirdEdge;
        let closestCommonVertex;
        /*
         * if (debug) drawCanvas(); if (debug) inProximity[0].draw(Color.RED); if
         * (debug) inProximity[0].draw(Color.BLACK);
         */
        for (let edge of inProximity) {
            /*
             * if (debug) edge.draw(Color.RED); if (debug) console.log("edge: " +
             * edge.toString());
             */
            let distance = edge.startVertex.sqrDistanceToPoint(mouseEnd);
            if (distance < __WEBPACK_IMPORTED_MODULE_1__run_main__["epsilon"] * 2 && (!closestCommonVertex || closestCommonVertex.distance < distance))
                closestCommonVertex = new VertexAndDistance(edge.startVertex, distance);
            else {
                distance = edge.nextEdge.startVertex.sqrDistanceToPoint(mouseEnd);
                if (distance < __WEBPACK_IMPORTED_MODULE_1__run_main__["epsilon"] * 2 && (!closestCommonVertex || closestCommonVertex.distance < distance))
                    closestCommonVertex = new VertexAndDistance(edge.nextEdge.startVertex, distance);
            }
            // if (debug) edge.draw(Color.BLACK);
        }
        if (closestCommonVertex) {
            // if (debug) console.log("commonVertex: " + commonVertex.toString());
            firstEdge = __WEBPACK_IMPORTED_MODULE_0__logic_geometric_objects__["a" /* Edge */].createTwinModel(startEdge);
            secondEdge = new __WEBPACK_IMPORTED_MODULE_0__logic_geometric_objects__["a" /* Edge */](startEdge.startVertex, firstEdge, null);
            thirdEdge = new __WEBPACK_IMPORTED_MODULE_0__logic_geometric_objects__["a" /* Edge */](closestCommonVertex.vertex, secondEdge, firstEdge);
            firstEdge.prevEdge = thirdEdge;
            firstEdge.nextEdge = secondEdge;
            secondEdge.nextEdge = thirdEdge;
            return new __WEBPACK_IMPORTED_MODULE_0__logic_geometric_objects__["b" /* Triangle */](firstEdge);
        }
    }
    /**
     * Forms an {@code Triangle} by the two given {@code Edges} which intersected the drawn line.
     * If these two {@code Edges} aren't connected no {@code Triangle} will be formed by this method.
     * Otherwise a {@code Triangle} will be formed. If there is a first edge available which works as third {@code Edge},
     * it will be used as well.
     * @param involvedEdges the two {@code Edges} who will takes as two sides of a {@code Triangle} if they're are connected
     * @return a preview triangle formed by the two {@code Edges}, if any, otherwise {@code null}
     */
    tryToConnectIntersections(startEdge, closestEdge, state) {
        if (!(startEdge && closestEdge))
            return null;
        let firstEdge;
        let secondEdge;
        let thirdEdge;
        let firstConnectedEdge = this.checkConnection(startEdge, closestEdge);
        // if the are connected, close the gap between them
        if (__WEBPACK_IMPORTED_MODULE_1__run_main__["debug"] > 0 && this.canvas) {
            this.canvas.highlight(startEdge);
            this.canvas.highlight(closestEdge);
        }
        ;
        if (!firstConnectedEdge)
            return null;
        else if (startEdge.equals(firstConnectedEdge)) {
            firstEdge = __WEBPACK_IMPORTED_MODULE_0__logic_geometric_objects__["a" /* Edge */].createTwinModel(startEdge);
            secondEdge = __WEBPACK_IMPORTED_MODULE_0__logic_geometric_objects__["a" /* Edge */].createTwinModel(closestEdge);
            thirdEdge = new __WEBPACK_IMPORTED_MODULE_0__logic_geometric_objects__["a" /* Edge */](closestEdge.startVertex, secondEdge, firstEdge);
        }
        else if (closestEdge.equals(firstConnectedEdge)) {
            firstEdge = __WEBPACK_IMPORTED_MODULE_0__logic_geometric_objects__["a" /* Edge */].createTwinModel(closestEdge);
            secondEdge = __WEBPACK_IMPORTED_MODULE_0__logic_geometric_objects__["a" /* Edge */].createTwinModel(startEdge);
            thirdEdge = new __WEBPACK_IMPORTED_MODULE_0__logic_geometric_objects__["a" /* Edge */](startEdge.startVertex, secondEdge, firstEdge);
        }
        else
            return null;
        firstEdge.prevEdge = thirdEdge;
        firstEdge.nextEdge = secondEdge;
        secondEdge.prevEdge = firstEdge;
        secondEdge.nextEdge = thirdEdge;
        // check if a hole as been closed, i.e. there is a (yet unlinked) twin edge for the thirdEdge
        // TODO: Alternative: just walk the outline and check for matching twin edge?
        //        let possibleTwinEdge:Edge = this.findExistingTwin( thirdEdge, e => e.nextEdge );
        //        if (possibleTwinEdge) thirdEdge.twinEdge = this.findExistingTwin( thirdEdge, e => e.prevEdge );
        return new __WEBPACK_IMPORTED_MODULE_0__logic_geometric_objects__["b" /* Triangle */](firstEdge);
    }
    /**
     * Not used atm. Starting from a outline edge, this method collects all edges
     * from the outline.
     *
     * @param relatedEdge
     *            from the outline
     * @returns array with all {@code Edges} establishing the outline
     */
    calculateOutline(startEdge, state) {
        let outline = [startEdge];
        let currentEdge = !startEdge.nextEdge.getTwinEdge() ? startEdge.nextEdge : startEdge.nextEdge.getTwinEdge().nextEdge;
        while (!currentEdge.equals(startEdge)) {
            if (__WEBPACK_IMPORTED_MODULE_1__run_main__["debug"] && this.canvas)
                this.canvas.highlight(currentEdge);
            outline = outline.concat(currentEdge);
            if (__WEBPACK_IMPORTED_MODULE_1__run_main__["debug"] && this.canvas)
                this.canvas.unhighlight(currentEdge);
            currentEdge = !currentEdge.nextEdge.getTwinEdge() ? currentEdge.nextEdge : currentEdge.nextEdge.getTwinEdge().nextEdge;
        }
        return outline;
    }
    /**
     * Returns the {@code Triangle} containing a given {@code Vertex}.
     * @param vertex a containing {@code Triangle} is requested for
     * @param currentPlayer whose {@code Triangles} shall be checked
     * @param the containing {@code Triangle}, if any, otherwise {@code null}
     */
    //TODO handle collisions with triangles an other way?
    getContainingTriangle(vertex, state) {
        for (let triangle of state.playerTriangles) {
            let geometricTriangle = triangle.triangle;
            if (geometricTriangle.contains(vertex))
                return geometricTriangle;
        }
        for (let triangle of state.otherTriangles) {
            let geometricTriangle = triangle.triangle;
            if (geometricTriangle.contains(vertex))
                return geometricTriangle;
        }
        return null;
    }
    /**
     * Takes vertices and builds edges from them. All edges will be linked in their
     * order and the first with the last one.
     * @param vertices forming {@code Edges} in the order they're passed
     * @returns array of created edges
     */
    verticesToEdges(...vertices) {
        if (vertices.length < 3)
            return;
        let edges = [];
        for (let i = 0; i < vertices.length; i++) {
            edges = edges.concat(new __WEBPACK_IMPORTED_MODULE_0__logic_geometric_objects__["a" /* Edge */](vertices[i], null, null));
        }
        for (let i = 1; i < edges.length - 1; i++) {
            edges[i].prevEdge = edges[i - 1];
            edges[i].nextEdge = edges[i + 1];
        }
        edges[0].nextEdge = edges[1];
        edges[0].prevEdge = edges[edges.length - 1];
        edges[edges.length - 1].nextEdge = edges[0];
        edges[edges.length - 1].prevEdge = edges[edges.length - 2];
        return edges;
    }
}
/* harmony export (immutable) */ __webpack_exports__["a"] = Geometry;

/**
 * Pure data class. Holds the an {@code Edge} an the distance towards it. Used for calculations of new {@code Triangles}.
 */
class EdgeAndDistance {
    get edge() { return this._edge; }
    get distance() { return this._distance; }
    constructor(edge, distance) {
        this._edge = edge;
        this._distance = distance;
    }
}
/**
 * Pure data class. Holds an {@code Vertex} and the distance towards it. Used for calculations of new {@code Triangles}.
 */
class VertexAndDistance {
    get vertex() { return this._vertex; }
    get distance() { return this._distance; }
    constructor(vertex, distance) {
        this._vertex = vertex;
        this._distance = distance;
    }
}
/**
 * Helper class which tells about the one or two own {@code Edges} which are relevant
 * to constitute a {@code Triangle} as well as about affected foreign {@code Edges} which might get destroyed.
 */
class EdgeIntersections {
    /**
     * Creates a new {@code EdgeIntersecions}
     * @param process determines which {@code Edges} count as own ones
     */
    constructor(process) {
        //TODO use ImmutableJS - SortedSet for more performance?
        /**All own {@code Edges}, used to determine the one or two relevant ones*/
        this.ownInvolvedEdges = [];
        /**All foreing{@code Edges}.*/
        this.foreignInvolvedEdges = [];
        /**Tells whether more {@code EdgesAndDistances} can be prcoessed or whether a result has already been calculated*/
        this.finalized = false;
        this.playerId = process;
    }
    /**
     * Puts another {@code EdgeAndDistance} to the internal data model.
     * @param toProcess to be processed an put to the data model
     * @return {@code this}, for chaining
     * @throws Exception if this instance has already been finalized and therefore can't process any new data.
     */
    processEdge(toProcess) {
        if (this.finalized)
            throw "Illegal State: already finalized.";
        //      if (this.ownInvolvedEdges[0]==null&&this.ownInvolvedEdges[1]==null&&this.playerId!=toRegister.edge.relatedTriangle.owner){
        if (this.playerId != toProcess.edge.relatedTriangle.ingameTriangle.owner)
            this.foreignInvolvedEdges.push(toProcess);
        else
            this.ownInvolvedEdges.push(toProcess);
    }
    /**
     * Disallows further items to be processed and determines the results.
     * @throws Exception if this instance has already been finalized
     * @return {@code this}, for chaining
     */
    finalize() {
        if (this.finalized)
            throw "Illegal State: already finalized.";
        this.finalized = true;
        if (!this.getOwnEnclosingEdges())
            this.ownInvolvedEdges = this.ownInvolvedEdges.sort((i1, i2) => i1.distance - i2.distance);
        let ownInvolved1 = this.ownInvolvedEdges[0];
        let ownInvolved2 = this.ownInvolvedEdges[1];
        let ownEnclosingEdges = [ownInvolved1, ownInvolved2];
        if (!ownInvolved1)
            return this;
        let min = ownInvolved1.distance;
        let max = ownInvolved2 ? ownInvolved2.distance : null;
        let foreignEnclosedEdges;
        try {
            foreignEnclosedEdges = this.foreignInvolvedEdges.map(e => { if (e.distance < min)
                throw "";
            else
                return e; }).
                filter((e => e.distance >= min && (!max || e.distance <= max)));
        }
        catch (e) {
            return this; //nasty workaround to stop as soon as its clear the line started not in an own triangle
        }
        this.ownEnclosingEdges = ownEnclosingEdges;
        this.foreignEnclosedEdges = foreignEnclosedEdges;
        return this;
    }
    /**
     * Returns the one or two own {@code Edges} which might constitute a {@code Triangle}.
     * @return the {@code Edge(s)} (or better: whose not-yet-existing-twins) which might constitute a {@code Triangle}.
     * @throws Exception if this instance has not been finalized yet
     */
    getOwnEnclosingEdges() {
        if (!this.finalized)
            throw "Illegal State: not finalized.";
        let e1 = this.ownInvolvedEdges[0];
        let e2 = this.ownInvolvedEdges[1];
        this.ownEnclosingEdges = [e1, e2];
        return [e1 ? e1.edge : null, e2 ? e2.edge : null];
    }
    /**
     * Returns all foreign {@code Edges} which might be destroyed if the {@code Triangle} is created.
     * @return all foreign {@code Edges} which might be destroyed
     * @throws Exception if this instance has not been finalized yet
     */
    getForeignEnclosedEdge() {
        if (!this.finalized)
            throw "Illegal State: not finalized.";
        return this.foreignEnclosedEdges.map(e => e.edge);
    }
}


/***/ }),
/* 12 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__run_main__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__resources_sockjs_js__ = __webpack_require__(13);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__resources_sockjs_js___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_1__resources_sockjs_js__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__resources_stomp_js__ = __webpack_require__(15);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__resources_stomp_js___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_2__resources_stomp_js__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__command_handlers__ = __webpack_require__(7);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__network_beans__ = __webpack_require__(3);
/**
 * Network communication
 */





/*
 *  NETWORK COMMUNICATION
 */
/** Holds the available server command handlers. */
const CommandHandlers = [new __WEBPACK_IMPORTED_MODULE_3__command_handlers__["e" /* UpdateHandler */](), new __WEBPACK_IMPORTED_MODULE_3__command_handlers__["d" /* ResetHandler */](), new __WEBPACK_IMPORTED_MODULE_3__command_handlers__["b" /* InitHandler */](), new __WEBPACK_IMPORTED_MODULE_3__command_handlers__["a" /* AccountHandler */]()];
/**Base path for requests*/
const webSocketPath = "/trigoty-websocket";
/** Request path to register as a new player*/
const registerPlayerPath = "/register";
/** Request path to request the initial information*/
const requestInitInfosPath = "/init";
/** Request path to propose {@code Triangle}*/
const proposeTrianglePath = "/propose/triangle";
/** Request path for the complete game state*/
const requestCompleteStatePath = "/fullstate";
/**Path to request a game reset*/
const newGamePath = "/debug/new_game";
/** Channel for private messages*/
const whisperChannel = "/user/queue/reply";
/** Channel for public, i.e. broadcasted messages*/
const broadcastChannel = "/update";
/**
 * Handles requests for the server and delivers server messages via callbacks.
 */
class NetworkConnector {
    constructor() {
        /**Holds the connection to the server*/
        this.socket = new __WEBPACK_IMPORTED_MODULE_1__resources_sockjs_js__(webSocketPath);
        /**Handles the communication via the Stomp protocol*/
        this.stompClient = __WEBPACK_IMPORTED_MODULE_2__resources_stomp_js__["Stomp"].over(this.socket);
        /**Handles server commands which have caused an error*/
        this.defaultHandler = new __WEBPACK_IMPORTED_MODULE_3__command_handlers__["c" /* InvalidCommandHandler */]();
        /**Additional header information*/
        this.header = {};
    }
    /**
     * Initialises this connector and calls the given method once the connecion is completed.
     * @param readyHandler callback once the connection is established
     */
    init(readyHandler) {
        this.stompClient.connect(/*'guest', 'guest'*/ this.header, (frame) => {
            this.subscribe(this, whisperChannel).call(frame);
            this.subscribe(this, broadcastChannel).call(frame);
            readyHandler.call(this, true);
        });
    }
    /**
     * Subscribes to the websocket's channel all the communication will be realized with.
     *@param connector holding the websocket. Required because {@code this} will be overwritten when this method is invoked via callback.
     */
    subscribe(connector, channel) {
        return function (frame) {
            console.log('Connected: ' + frame);
            //TODO use subscription?
            let subcription = connector.stompClient.subscribe(channel, function (updateObject) {
                try {
                    let response = JSON.parse(updateObject.body);
                    let handler = CommandHandlers.find(h => h.getType() === response.type);
                    if (!handler)
                        throw "No handler for response type " + response.type;
                    else
                        handler.process(response);
                }
                catch (e) {
                    console.log("Parsing the response failed: " + e);
                    connector.defaultHandler.process();
                }
            });
        };
    }
    /**
     * Registers a new player for the game.
     */
    registerPlayer() {
        try {
            this.stompClient.send(registerPlayerPath, this.header, null);
        }
        catch (e) {
            console.log("Registering failed: " + e);
            Object(__WEBPACK_IMPORTED_MODULE_0__run_main__["resume"])();
        }
    }
    /**Proposes a {@code Triangle} to the server which might appear in the next {@code UpdateCommand} from the server.
     *@toRequest the {@code Triangle} which shall be proposed
     */
    proposeTriangle(toRequest) {
        if (!toRequest)
            Object(__WEBPACK_IMPORTED_MODULE_0__run_main__["resume"])();
        try {
            let requestObj = __WEBPACK_IMPORTED_MODULE_4__network_beans__["e" /* TriangleBean */].of(toRequest);
            this.stompClient.send(proposeTrianglePath, this.header, JSON.stringify(requestObj));
        }
        catch (e) {
            console.log("Requesting the the triangle " + toRequest.toString() + " failed: " + e);
            Object(__WEBPACK_IMPORTED_MODULE_0__run_main__["resume"])();
        }
    }
    /**
     * Requests the complete server state. Useful if the client's state seems to be inconsistent server's.
     */
    requestCompleteState() {
        try {
            this.stompClient.send(requestCompleteStatePath, this.header, null);
        }
        catch (e) {
            console.log("Resetting failed: " + e);
            Object(__WEBPACK_IMPORTED_MODULE_0__run_main__["resume"])();
        }
    }
    newGame() {
        this.stompClient.send(newGamePath, this.header, null);
    }
}
/* harmony export (immutable) */ __webpack_exports__["a"] = NetworkConnector;



/***/ }),
/* 13 */
/***/ (function(module, exports, __webpack_require__) {

/* WEBPACK VAR INJECTION */(function(global) {var require;var require;/* sockjs-client v1.1.4 | http://sockjs.org | MIT license */
(function(f){if(true){module.exports=f()}else if(typeof define==="function"&&define.amd){define([],f)}else{var g;if(typeof window!=="undefined"){g=window}else if(typeof global!=="undefined"){g=global}else if(typeof self!=="undefined"){g=self}else{g=this}g.SockJS = f()}})(function(){var define,module,exports;return (function e(t,n,r){function s(o,u){if(!n[o]){if(!t[o]){var a=typeof require=="function"&&require;if(!u&&a)return require(o,!0);if(i)return i(o,!0);var f=new Error("Cannot find module '"+o+"'");throw f.code="MODULE_NOT_FOUND",f}var l=n[o]={exports:{}};t[o][0].call(l.exports,function(e){var n=t[o][1][e];return s(n?n:e)},l,l.exports,e,t,n,r)}return n[o].exports}var i=typeof require=="function"&&require;for(var o=0;o<r.length;o++)s(r[o]);return s})({1:[function(require,module,exports){
(function (global){
'use strict';

var transportList = require('./transport-list');

module.exports = require('./main')(transportList);

// TODO can't get rid of this until all servers do
if ('_sockjs_onload' in global) {
  setTimeout(global._sockjs_onload, 1);
}

}).call(this,typeof global !== "undefined" ? global : typeof self !== "undefined" ? self : typeof window !== "undefined" ? window : {})

},{"./main":14,"./transport-list":16}],2:[function(require,module,exports){
'use strict';

var inherits = require('inherits')
  , Event = require('./event')
  ;

function CloseEvent() {
  Event.call(this);
  this.initEvent('close', false, false);
  this.wasClean = false;
  this.code = 0;
  this.reason = '';
}

inherits(CloseEvent, Event);

module.exports = CloseEvent;

},{"./event":4,"inherits":57}],3:[function(require,module,exports){
'use strict';

var inherits = require('inherits')
  , EventTarget = require('./eventtarget')
  ;

function EventEmitter() {
  EventTarget.call(this);
}

inherits(EventEmitter, EventTarget);

EventEmitter.prototype.removeAllListeners = function(type) {
  if (type) {
    delete this._listeners[type];
  } else {
    this._listeners = {};
  }
};

EventEmitter.prototype.once = function(type, listener) {
  var self = this
    , fired = false;

  function g() {
    self.removeListener(type, g);

    if (!fired) {
      fired = true;
      listener.apply(this, arguments);
    }
  }

  this.on(type, g);
};

EventEmitter.prototype.emit = function() {
  var type = arguments[0];
  var listeners = this._listeners[type];
  if (!listeners) {
    return;
  }
  // equivalent of Array.prototype.slice.call(arguments, 1);
  var l = arguments.length;
  var args = new Array(l - 1);
  for (var ai = 1; ai < l; ai++) {
    args[ai - 1] = arguments[ai];
  }
  for (var i = 0; i < listeners.length; i++) {
    listeners[i].apply(this, args);
  }
};

EventEmitter.prototype.on = EventEmitter.prototype.addListener = EventTarget.prototype.addEventListener;
EventEmitter.prototype.removeListener = EventTarget.prototype.removeEventListener;

module.exports.EventEmitter = EventEmitter;

},{"./eventtarget":5,"inherits":57}],4:[function(require,module,exports){
'use strict';

function Event(eventType) {
  this.type = eventType;
}

Event.prototype.initEvent = function(eventType, canBubble, cancelable) {
  this.type = eventType;
  this.bubbles = canBubble;
  this.cancelable = cancelable;
  this.timeStamp = +new Date();
  return this;
};

Event.prototype.stopPropagation = function() {};
Event.prototype.preventDefault = function() {};

Event.CAPTURING_PHASE = 1;
Event.AT_TARGET = 2;
Event.BUBBLING_PHASE = 3;

module.exports = Event;

},{}],5:[function(require,module,exports){
'use strict';

/* Simplified implementation of DOM2 EventTarget.
 *   http://www.w3.org/TR/DOM-Level-2-Events/events.html#Events-EventTarget
 */

function EventTarget() {
  this._listeners = {};
}

EventTarget.prototype.addEventListener = function(eventType, listener) {
  if (!(eventType in this._listeners)) {
    this._listeners[eventType] = [];
  }
  var arr = this._listeners[eventType];
  // #4
  if (arr.indexOf(listener) === -1) {
    // Make a copy so as not to interfere with a current dispatchEvent.
    arr = arr.concat([listener]);
  }
  this._listeners[eventType] = arr;
};

EventTarget.prototype.removeEventListener = function(eventType, listener) {
  var arr = this._listeners[eventType];
  if (!arr) {
    return;
  }
  var idx = arr.indexOf(listener);
  if (idx !== -1) {
    if (arr.length > 1) {
      // Make a copy so as not to interfere with a current dispatchEvent.
      this._listeners[eventType] = arr.slice(0, idx).concat(arr.slice(idx + 1));
    } else {
      delete this._listeners[eventType];
    }
    return;
  }
};

EventTarget.prototype.dispatchEvent = function() {
  var event = arguments[0];
  var t = event.type;
  // equivalent of Array.prototype.slice.call(arguments, 0);
  var args = arguments.length === 1 ? [event] : Array.apply(null, arguments);
  // TODO: This doesn't match the real behavior; per spec, onfoo get
  // their place in line from the /first/ time they're set from
  // non-null. Although WebKit bumps it to the end every time it's
  // set.
  if (this['on' + t]) {
    this['on' + t].apply(this, args);
  }
  if (t in this._listeners) {
    // Grab a reference to the listeners list. removeEventListener may alter the list.
    var listeners = this._listeners[t];
    for (var i = 0; i < listeners.length; i++) {
      listeners[i].apply(this, args);
    }
  }
};

module.exports = EventTarget;

},{}],6:[function(require,module,exports){
'use strict';

var inherits = require('inherits')
  , Event = require('./event')
  ;

function TransportMessageEvent(data) {
  Event.call(this);
  this.initEvent('message', false, false);
  this.data = data;
}

inherits(TransportMessageEvent, Event);

module.exports = TransportMessageEvent;

},{"./event":4,"inherits":57}],7:[function(require,module,exports){
'use strict';

var JSON3 = require('json3')
  , iframeUtils = require('./utils/iframe')
  ;

function FacadeJS(transport) {
  this._transport = transport;
  transport.on('message', this._transportMessage.bind(this));
  transport.on('close', this._transportClose.bind(this));
}

FacadeJS.prototype._transportClose = function(code, reason) {
  iframeUtils.postMessage('c', JSON3.stringify([code, reason]));
};
FacadeJS.prototype._transportMessage = function(frame) {
  iframeUtils.postMessage('t', frame);
};
FacadeJS.prototype._send = function(data) {
  this._transport.send(data);
};
FacadeJS.prototype._close = function() {
  this._transport.close();
  this._transport.removeAllListeners();
};

module.exports = FacadeJS;

},{"./utils/iframe":47,"json3":58}],8:[function(require,module,exports){
(function (process){
'use strict';

var urlUtils = require('./utils/url')
  , eventUtils = require('./utils/event')
  , JSON3 = require('json3')
  , FacadeJS = require('./facade')
  , InfoIframeReceiver = require('./info-iframe-receiver')
  , iframeUtils = require('./utils/iframe')
  , loc = require('./location')
  ;

var debug = function() {};
if (process.env.NODE_ENV !== 'production') {
  debug = require('debug')('sockjs-client:iframe-bootstrap');
}

module.exports = function(SockJS, availableTransports) {
  var transportMap = {};
  availableTransports.forEach(function(at) {
    if (at.facadeTransport) {
      transportMap[at.facadeTransport.transportName] = at.facadeTransport;
    }
  });

  // hard-coded for the info iframe
  // TODO see if we can make this more dynamic
  transportMap[InfoIframeReceiver.transportName] = InfoIframeReceiver;
  var parentOrigin;

  /* eslint-disable camelcase */
  SockJS.bootstrap_iframe = function() {
    /* eslint-enable camelcase */
    var facade;
    iframeUtils.currentWindowId = loc.hash.slice(1);
    var onMessage = function(e) {
      if (e.source !== parent) {
        return;
      }
      if (typeof parentOrigin === 'undefined') {
        parentOrigin = e.origin;
      }
      if (e.origin !== parentOrigin) {
        return;
      }

      var iframeMessage;
      try {
        iframeMessage = JSON3.parse(e.data);
      } catch (ignored) {
        debug('bad json', e.data);
        return;
      }

      if (iframeMessage.windowId !== iframeUtils.currentWindowId) {
        return;
      }
      switch (iframeMessage.type) {
      case 's':
        var p;
        try {
          p = JSON3.parse(iframeMessage.data);
        } catch (ignored) {
          debug('bad json', iframeMessage.data);
          break;
        }
        var version = p[0];
        var transport = p[1];
        var transUrl = p[2];
        var baseUrl = p[3];
        debug(version, transport, transUrl, baseUrl);
        // change this to semver logic
        if (version !== SockJS.version) {
          throw new Error('Incompatible SockJS! Main site uses:' +
                    ' "' + version + '", the iframe:' +
                    ' "' + SockJS.version + '".');
        }

        if (!urlUtils.isOriginEqual(transUrl, loc.href) ||
            !urlUtils.isOriginEqual(baseUrl, loc.href)) {
          throw new Error('Can\'t connect to different domain from within an ' +
                    'iframe. (' + loc.href + ', ' + transUrl + ', ' + baseUrl + ')');
        }
        facade = new FacadeJS(new transportMap[transport](transUrl, baseUrl));
        break;
      case 'm':
        facade._send(iframeMessage.data);
        break;
      case 'c':
        if (facade) {
          facade._close();
        }
        facade = null;
        break;
      }
    };

    eventUtils.attachEvent('message', onMessage);

    // Start
    iframeUtils.postMessage('s');
  };
};

}).call(this,{ env: {} })

},{"./facade":7,"./info-iframe-receiver":10,"./location":13,"./utils/event":46,"./utils/iframe":47,"./utils/url":52,"debug":55,"json3":58}],9:[function(require,module,exports){
(function (process){
'use strict';

var EventEmitter = require('events').EventEmitter
  , inherits = require('inherits')
  , JSON3 = require('json3')
  , objectUtils = require('./utils/object')
  ;

var debug = function() {};
if (process.env.NODE_ENV !== 'production') {
  debug = require('debug')('sockjs-client:info-ajax');
}

function InfoAjax(url, AjaxObject) {
  EventEmitter.call(this);

  var self = this;
  var t0 = +new Date();
  this.xo = new AjaxObject('GET', url);

  this.xo.once('finish', function(status, text) {
    var info, rtt;
    if (status === 200) {
      rtt = (+new Date()) - t0;
      if (text) {
        try {
          info = JSON3.parse(text);
        } catch (e) {
          debug('bad json', text);
        }
      }

      if (!objectUtils.isObject(info)) {
        info = {};
      }
    }
    self.emit('finish', info, rtt);
    self.removeAllListeners();
  });
}

inherits(InfoAjax, EventEmitter);

InfoAjax.prototype.close = function() {
  this.removeAllListeners();
  this.xo.close();
};

module.exports = InfoAjax;

}).call(this,{ env: {} })

},{"./utils/object":49,"debug":55,"events":3,"inherits":57,"json3":58}],10:[function(require,module,exports){
'use strict';

var inherits = require('inherits')
  , EventEmitter = require('events').EventEmitter
  , JSON3 = require('json3')
  , XHRLocalObject = require('./transport/sender/xhr-local')
  , InfoAjax = require('./info-ajax')
  ;

function InfoReceiverIframe(transUrl) {
  var self = this;
  EventEmitter.call(this);

  this.ir = new InfoAjax(transUrl, XHRLocalObject);
  this.ir.once('finish', function(info, rtt) {
    self.ir = null;
    self.emit('message', JSON3.stringify([info, rtt]));
  });
}

inherits(InfoReceiverIframe, EventEmitter);

InfoReceiverIframe.transportName = 'iframe-info-receiver';

InfoReceiverIframe.prototype.close = function() {
  if (this.ir) {
    this.ir.close();
    this.ir = null;
  }
  this.removeAllListeners();
};

module.exports = InfoReceiverIframe;

},{"./info-ajax":9,"./transport/sender/xhr-local":37,"events":3,"inherits":57,"json3":58}],11:[function(require,module,exports){
(function (process,global){
'use strict';

var EventEmitter = require('events').EventEmitter
  , inherits = require('inherits')
  , JSON3 = require('json3')
  , utils = require('./utils/event')
  , IframeTransport = require('./transport/iframe')
  , InfoReceiverIframe = require('./info-iframe-receiver')
  ;

var debug = function() {};
if (process.env.NODE_ENV !== 'production') {
  debug = require('debug')('sockjs-client:info-iframe');
}

function InfoIframe(baseUrl, url) {
  var self = this;
  EventEmitter.call(this);

  var go = function() {
    var ifr = self.ifr = new IframeTransport(InfoReceiverIframe.transportName, url, baseUrl);

    ifr.once('message', function(msg) {
      if (msg) {
        var d;
        try {
          d = JSON3.parse(msg);
        } catch (e) {
          debug('bad json', msg);
          self.emit('finish');
          self.close();
          return;
        }

        var info = d[0], rtt = d[1];
        self.emit('finish', info, rtt);
      }
      self.close();
    });

    ifr.once('close', function() {
      self.emit('finish');
      self.close();
    });
  };

  // TODO this seems the same as the 'needBody' from transports
  if (!global.document.body) {
    utils.attachEvent('load', go);
  } else {
    go();
  }
}

inherits(InfoIframe, EventEmitter);

InfoIframe.enabled = function() {
  return IframeTransport.enabled();
};

InfoIframe.prototype.close = function() {
  if (this.ifr) {
    this.ifr.close();
  }
  this.removeAllListeners();
  this.ifr = null;
};

module.exports = InfoIframe;

}).call(this,{ env: {} },typeof global !== "undefined" ? global : typeof self !== "undefined" ? self : typeof window !== "undefined" ? window : {})

},{"./info-iframe-receiver":10,"./transport/iframe":22,"./utils/event":46,"debug":55,"events":3,"inherits":57,"json3":58}],12:[function(require,module,exports){
(function (process){
'use strict';

var EventEmitter = require('events').EventEmitter
  , inherits = require('inherits')
  , urlUtils = require('./utils/url')
  , XDR = require('./transport/sender/xdr')
  , XHRCors = require('./transport/sender/xhr-cors')
  , XHRLocal = require('./transport/sender/xhr-local')
  , XHRFake = require('./transport/sender/xhr-fake')
  , InfoIframe = require('./info-iframe')
  , InfoAjax = require('./info-ajax')
  ;

var debug = function() {};
if (process.env.NODE_ENV !== 'production') {
  debug = require('debug')('sockjs-client:info-receiver');
}

function InfoReceiver(baseUrl, urlInfo) {
  debug(baseUrl);
  var self = this;
  EventEmitter.call(this);

  setTimeout(function() {
    self.doXhr(baseUrl, urlInfo);
  }, 0);
}

inherits(InfoReceiver, EventEmitter);

// TODO this is currently ignoring the list of available transports and the whitelist

InfoReceiver._getReceiver = function(baseUrl, url, urlInfo) {
  // determine method of CORS support (if needed)
  if (urlInfo.sameOrigin) {
    return new InfoAjax(url, XHRLocal);
  }
  if (XHRCors.enabled) {
    return new InfoAjax(url, XHRCors);
  }
  if (XDR.enabled && urlInfo.sameScheme) {
    return new InfoAjax(url, XDR);
  }
  if (InfoIframe.enabled()) {
    return new InfoIframe(baseUrl, url);
  }
  return new InfoAjax(url, XHRFake);
};

InfoReceiver.prototype.doXhr = function(baseUrl, urlInfo) {
  var self = this
    , url = urlUtils.addPath(baseUrl, '/info')
    ;
  debug('doXhr', url);

  this.xo = InfoReceiver._getReceiver(baseUrl, url, urlInfo);

  this.timeoutRef = setTimeout(function() {
    debug('timeout');
    self._cleanup(false);
    self.emit('finish');
  }, InfoReceiver.timeout);

  this.xo.once('finish', function(info, rtt) {
    debug('finish', info, rtt);
    self._cleanup(true);
    self.emit('finish', info, rtt);
  });
};

InfoReceiver.prototype._cleanup = function(wasClean) {
  debug('_cleanup');
  clearTimeout(this.timeoutRef);
  this.timeoutRef = null;
  if (!wasClean && this.xo) {
    this.xo.close();
  }
  this.xo = null;
};

InfoReceiver.prototype.close = function() {
  debug('close');
  this.removeAllListeners();
  this._cleanup(false);
};

InfoReceiver.timeout = 8000;

module.exports = InfoReceiver;

}).call(this,{ env: {} })

},{"./info-ajax":9,"./info-iframe":11,"./transport/sender/xdr":34,"./transport/sender/xhr-cors":35,"./transport/sender/xhr-fake":36,"./transport/sender/xhr-local":37,"./utils/url":52,"debug":55,"events":3,"inherits":57}],13:[function(require,module,exports){
(function (global){
'use strict';

module.exports = global.location || {
  origin: 'http://localhost:80'
, protocol: 'http'
, host: 'localhost'
, port: 80
, href: 'http://localhost/'
, hash: ''
};

}).call(this,typeof global !== "undefined" ? global : typeof self !== "undefined" ? self : typeof window !== "undefined" ? window : {})

},{}],14:[function(require,module,exports){
(function (process,global){
'use strict';

require('./shims');

var URL = require('url-parse')
  , inherits = require('inherits')
  , JSON3 = require('json3')
  , random = require('./utils/random')
  , escape = require('./utils/escape')
  , urlUtils = require('./utils/url')
  , eventUtils = require('./utils/event')
  , transport = require('./utils/transport')
  , objectUtils = require('./utils/object')
  , browser = require('./utils/browser')
  , log = require('./utils/log')
  , Event = require('./event/event')
  , EventTarget = require('./event/eventtarget')
  , loc = require('./location')
  , CloseEvent = require('./event/close')
  , TransportMessageEvent = require('./event/trans-message')
  , InfoReceiver = require('./info-receiver')
  ;

var debug = function() {};
if (process.env.NODE_ENV !== 'production') {
  debug = require('debug')('sockjs-client:main');
}

var transports;

// follow constructor steps defined at http://dev.w3.org/html5/websockets/#the-websocket-interface
function SockJS(url, protocols, options) {
  if (!(this instanceof SockJS)) {
    return new SockJS(url, protocols, options);
  }
  if (arguments.length < 1) {
    throw new TypeError("Failed to construct 'SockJS: 1 argument required, but only 0 present");
  }
  EventTarget.call(this);

  this.readyState = SockJS.CONNECTING;
  this.extensions = '';
  this.protocol = '';

  // non-standard extension
  options = options || {};
  if (options.protocols_whitelist) {
    log.warn("'protocols_whitelist' is DEPRECATED. Use 'transports' instead.");
  }
  this._transportsWhitelist = options.transports;
  this._transportOptions = options.transportOptions || {};

  var sessionId = options.sessionId || 8;
  if (typeof sessionId === 'function') {
    this._generateSessionId = sessionId;
  } else if (typeof sessionId === 'number') {
    this._generateSessionId = function() {
      return random.string(sessionId);
    };
  } else {
    throw new TypeError('If sessionId is used in the options, it needs to be a number or a function.');
  }

  this._server = options.server || random.numberString(1000);

  // Step 1 of WS spec - parse and validate the url. Issue #8
  var parsedUrl = new URL(url);
  if (!parsedUrl.host || !parsedUrl.protocol) {
    throw new SyntaxError("The URL '" + url + "' is invalid");
  } else if (parsedUrl.hash) {
    throw new SyntaxError('The URL must not contain a fragment');
  } else if (parsedUrl.protocol !== 'http:' && parsedUrl.protocol !== 'https:') {
    throw new SyntaxError("The URL's scheme must be either 'http:' or 'https:'. '" + parsedUrl.protocol + "' is not allowed.");
  }

  var secure = parsedUrl.protocol === 'https:';
  // Step 2 - don't allow secure origin with an insecure protocol
  if (loc.protocol === 'https' && !secure) {
    throw new Error('SecurityError: An insecure SockJS connection may not be initiated from a page loaded over HTTPS');
  }

  // Step 3 - check port access - no need here
  // Step 4 - parse protocols argument
  if (!protocols) {
    protocols = [];
  } else if (!Array.isArray(protocols)) {
    protocols = [protocols];
  }

  // Step 5 - check protocols argument
  var sortedProtocols = protocols.sort();
  sortedProtocols.forEach(function(proto, i) {
    if (!proto) {
      throw new SyntaxError("The protocols entry '" + proto + "' is invalid.");
    }
    if (i < (sortedProtocols.length - 1) && proto === sortedProtocols[i + 1]) {
      throw new SyntaxError("The protocols entry '" + proto + "' is duplicated.");
    }
  });

  // Step 6 - convert origin
  var o = urlUtils.getOrigin(loc.href);
  this._origin = o ? o.toLowerCase() : null;

  // remove the trailing slash
  parsedUrl.set('pathname', parsedUrl.pathname.replace(/\/+$/, ''));

  // store the sanitized url
  this.url = parsedUrl.href;
  debug('using url', this.url);

  // Step 7 - start connection in background
  // obtain server info
  // http://sockjs.github.io/sockjs-protocol/sockjs-protocol-0.3.3.html#section-26
  this._urlInfo = {
    nullOrigin: !browser.hasDomain()
  , sameOrigin: urlUtils.isOriginEqual(this.url, loc.href)
  , sameScheme: urlUtils.isSchemeEqual(this.url, loc.href)
  };

  this._ir = new InfoReceiver(this.url, this._urlInfo);
  this._ir.once('finish', this._receiveInfo.bind(this));
}

inherits(SockJS, EventTarget);

function userSetCode(code) {
  return code === 1000 || (code >= 3000 && code <= 4999);
}

SockJS.prototype.close = function(code, reason) {
  // Step 1
  if (code && !userSetCode(code)) {
    throw new Error('InvalidAccessError: Invalid code');
  }
  // Step 2.4 states the max is 123 bytes, but we are just checking length
  if (reason && reason.length > 123) {
    throw new SyntaxError('reason argument has an invalid length');
  }

  // Step 3.1
  if (this.readyState === SockJS.CLOSING || this.readyState === SockJS.CLOSED) {
    return;
  }

  // TODO look at docs to determine how to set this
  var wasClean = true;
  this._close(code || 1000, reason || 'Normal closure', wasClean);
};

SockJS.prototype.send = function(data) {
  // #13 - convert anything non-string to string
  // TODO this currently turns objects into [object Object]
  if (typeof data !== 'string') {
    data = '' + data;
  }
  if (this.readyState === SockJS.CONNECTING) {
    throw new Error('InvalidStateError: The connection has not been established yet');
  }
  if (this.readyState !== SockJS.OPEN) {
    return;
  }
  this._transport.send(escape.quote(data));
};

SockJS.version = require('./version');

SockJS.CONNECTING = 0;
SockJS.OPEN = 1;
SockJS.CLOSING = 2;
SockJS.CLOSED = 3;

SockJS.prototype._receiveInfo = function(info, rtt) {
  debug('_receiveInfo', rtt);
  this._ir = null;
  if (!info) {
    this._close(1002, 'Cannot connect to server');
    return;
  }

  // establish a round-trip timeout (RTO) based on the
  // round-trip time (RTT)
  this._rto = this.countRTO(rtt);
  // allow server to override url used for the actual transport
  this._transUrl = info.base_url ? info.base_url : this.url;
  info = objectUtils.extend(info, this._urlInfo);
  debug('info', info);
  // determine list of desired and supported transports
  var enabledTransports = transports.filterToEnabled(this._transportsWhitelist, info);
  this._transports = enabledTransports.main;
  debug(this._transports.length + ' enabled transports');

  this._connect();
};

SockJS.prototype._connect = function() {
  for (var Transport = this._transports.shift(); Transport; Transport = this._transports.shift()) {
    debug('attempt', Transport.transportName);
    if (Transport.needBody) {
      if (!global.document.body ||
          (typeof global.document.readyState !== 'undefined' &&
            global.document.readyState !== 'complete' &&
            global.document.readyState !== 'interactive')) {
        debug('waiting for body');
        this._transports.unshift(Transport);
        eventUtils.attachEvent('load', this._connect.bind(this));
        return;
      }
    }

    // calculate timeout based on RTO and round trips. Default to 5s
    var timeoutMs = (this._rto * Transport.roundTrips) || 5000;
    this._transportTimeoutId = setTimeout(this._transportTimeout.bind(this), timeoutMs);
    debug('using timeout', timeoutMs);

    var transportUrl = urlUtils.addPath(this._transUrl, '/' + this._server + '/' + this._generateSessionId());
    var options = this._transportOptions[Transport.transportName];
    debug('transport url', transportUrl);
    var transportObj = new Transport(transportUrl, this._transUrl, options);
    transportObj.on('message', this._transportMessage.bind(this));
    transportObj.once('close', this._transportClose.bind(this));
    transportObj.transportName = Transport.transportName;
    this._transport = transportObj;

    return;
  }
  this._close(2000, 'All transports failed', false);
};

SockJS.prototype._transportTimeout = function() {
  debug('_transportTimeout');
  if (this.readyState === SockJS.CONNECTING) {
    this._transportClose(2007, 'Transport timed out');
  }
};

SockJS.prototype._transportMessage = function(msg) {
  debug('_transportMessage', msg);
  var self = this
    , type = msg.slice(0, 1)
    , content = msg.slice(1)
    , payload
    ;

  // first check for messages that don't need a payload
  switch (type) {
    case 'o':
      this._open();
      return;
    case 'h':
      this.dispatchEvent(new Event('heartbeat'));
      debug('heartbeat', this.transport);
      return;
  }

  if (content) {
    try {
      payload = JSON3.parse(content);
    } catch (e) {
      debug('bad json', content);
    }
  }

  if (typeof payload === 'undefined') {
    debug('empty payload', content);
    return;
  }

  switch (type) {
    case 'a':
      if (Array.isArray(payload)) {
        payload.forEach(function(p) {
          debug('message', self.transport, p);
          self.dispatchEvent(new TransportMessageEvent(p));
        });
      }
      break;
    case 'm':
      debug('message', this.transport, payload);
      this.dispatchEvent(new TransportMessageEvent(payload));
      break;
    case 'c':
      if (Array.isArray(payload) && payload.length === 2) {
        this._close(payload[0], payload[1], true);
      }
      break;
  }
};

SockJS.prototype._transportClose = function(code, reason) {
  debug('_transportClose', this.transport, code, reason);
  if (this._transport) {
    this._transport.removeAllListeners();
    this._transport = null;
    this.transport = null;
  }

  if (!userSetCode(code) && code !== 2000 && this.readyState === SockJS.CONNECTING) {
    this._connect();
    return;
  }

  this._close(code, reason);
};

SockJS.prototype._open = function() {
  debug('_open', this._transport.transportName, this.readyState);
  if (this.readyState === SockJS.CONNECTING) {
    if (this._transportTimeoutId) {
      clearTimeout(this._transportTimeoutId);
      this._transportTimeoutId = null;
    }
    this.readyState = SockJS.OPEN;
    this.transport = this._transport.transportName;
    this.dispatchEvent(new Event('open'));
    debug('connected', this.transport);
  } else {
    // The server might have been restarted, and lost track of our
    // connection.
    this._close(1006, 'Server lost session');
  }
};

SockJS.prototype._close = function(code, reason, wasClean) {
  debug('_close', this.transport, code, reason, wasClean, this.readyState);
  var forceFail = false;

  if (this._ir) {
    forceFail = true;
    this._ir.close();
    this._ir = null;
  }
  if (this._transport) {
    this._transport.close();
    this._transport = null;
    this.transport = null;
  }

  if (this.readyState === SockJS.CLOSED) {
    throw new Error('InvalidStateError: SockJS has already been closed');
  }

  this.readyState = SockJS.CLOSING;
  setTimeout(function() {
    this.readyState = SockJS.CLOSED;

    if (forceFail) {
      this.dispatchEvent(new Event('error'));
    }

    var e = new CloseEvent('close');
    e.wasClean = wasClean || false;
    e.code = code || 1000;
    e.reason = reason;

    this.dispatchEvent(e);
    this.onmessage = this.onclose = this.onerror = null;
    debug('disconnected');
  }.bind(this), 0);
};

// See: http://www.erg.abdn.ac.uk/~gerrit/dccp/notes/ccid2/rto_estimator/
// and RFC 2988.
SockJS.prototype.countRTO = function(rtt) {
  // In a local environment, when using IE8/9 and the `jsonp-polling`
  // transport the time needed to establish a connection (the time that pass
  // from the opening of the transport to the call of `_dispatchOpen`) is
  // around 200msec (the lower bound used in the article above) and this
  // causes spurious timeouts. For this reason we calculate a value slightly
  // larger than that used in the article.
  if (rtt > 100) {
    return 4 * rtt; // rto > 400msec
  }
  return 300 + rtt; // 300msec < rto <= 400msec
};

module.exports = function(availableTransports) {
  transports = transport(availableTransports);
  require('./iframe-bootstrap')(SockJS, availableTransports);
  return SockJS;
};

}).call(this,{ env: {} },typeof global !== "undefined" ? global : typeof self !== "undefined" ? self : typeof window !== "undefined" ? window : {})

},{"./event/close":2,"./event/event":4,"./event/eventtarget":5,"./event/trans-message":6,"./iframe-bootstrap":8,"./info-receiver":12,"./location":13,"./shims":15,"./utils/browser":44,"./utils/escape":45,"./utils/event":46,"./utils/log":48,"./utils/object":49,"./utils/random":50,"./utils/transport":51,"./utils/url":52,"./version":53,"debug":55,"inherits":57,"json3":58,"url-parse":61}],15:[function(require,module,exports){
/* eslint-disable */
/* jscs: disable */
'use strict';

// pulled specific shims from https://github.com/es-shims/es5-shim

var ArrayPrototype = Array.prototype;
var ObjectPrototype = Object.prototype;
var FunctionPrototype = Function.prototype;
var StringPrototype = String.prototype;
var array_slice = ArrayPrototype.slice;

var _toString = ObjectPrototype.toString;
var isFunction = function (val) {
    return ObjectPrototype.toString.call(val) === '[object Function]';
};
var isArray = function isArray(obj) {
    return _toString.call(obj) === '[object Array]';
};
var isString = function isString(obj) {
    return _toString.call(obj) === '[object String]';
};

var supportsDescriptors = Object.defineProperty && (function () {
    try {
        Object.defineProperty({}, 'x', {});
        return true;
    } catch (e) { /* this is ES3 */
        return false;
    }
}());

// Define configurable, writable and non-enumerable props
// if they don't exist.
var defineProperty;
if (supportsDescriptors) {
    defineProperty = function (object, name, method, forceAssign) {
        if (!forceAssign && (name in object)) { return; }
        Object.defineProperty(object, name, {
            configurable: true,
            enumerable: false,
            writable: true,
            value: method
        });
    };
} else {
    defineProperty = function (object, name, method, forceAssign) {
        if (!forceAssign && (name in object)) { return; }
        object[name] = method;
    };
}
var defineProperties = function (object, map, forceAssign) {
    for (var name in map) {
        if (ObjectPrototype.hasOwnProperty.call(map, name)) {
          defineProperty(object, name, map[name], forceAssign);
        }
    }
};

var toObject = function (o) {
    if (o == null) { // this matches both null and undefined
        throw new TypeError("can't convert " + o + ' to object');
    }
    return Object(o);
};

//
// Util
// ======
//

// ES5 9.4
// http://es5.github.com/#x9.4
// http://jsperf.com/to-integer

function toInteger(num) {
    var n = +num;
    if (n !== n) { // isNaN
        n = 0;
    } else if (n !== 0 && n !== (1 / 0) && n !== -(1 / 0)) {
        n = (n > 0 || -1) * Math.floor(Math.abs(n));
    }
    return n;
}

function ToUint32(x) {
    return x >>> 0;
}

//
// Function
// ========
//

// ES-5 15.3.4.5
// http://es5.github.com/#x15.3.4.5

function Empty() {}

defineProperties(FunctionPrototype, {
    bind: function bind(that) { // .length is 1
        // 1. Let Target be the this value.
        var target = this;
        // 2. If IsCallable(Target) is false, throw a TypeError exception.
        if (!isFunction(target)) {
            throw new TypeError('Function.prototype.bind called on incompatible ' + target);
        }
        // 3. Let A be a new (possibly empty) internal list of all of the
        //   argument values provided after thisArg (arg1, arg2 etc), in order.
        // XXX slicedArgs will stand in for "A" if used
        var args = array_slice.call(arguments, 1); // for normal call
        // 4. Let F be a new native ECMAScript object.
        // 11. Set the [[Prototype]] internal property of F to the standard
        //   built-in Function prototype object as specified in 15.3.3.1.
        // 12. Set the [[Call]] internal property of F as described in
        //   15.3.4.5.1.
        // 13. Set the [[Construct]] internal property of F as described in
        //   15.3.4.5.2.
        // 14. Set the [[HasInstance]] internal property of F as described in
        //   15.3.4.5.3.
        var binder = function () {

            if (this instanceof bound) {
                // 15.3.4.5.2 [[Construct]]
                // When the [[Construct]] internal method of a function object,
                // F that was created using the bind function is called with a
                // list of arguments ExtraArgs, the following steps are taken:
                // 1. Let target be the value of F's [[TargetFunction]]
                //   internal property.
                // 2. If target has no [[Construct]] internal method, a
                //   TypeError exception is thrown.
                // 3. Let boundArgs be the value of F's [[BoundArgs]] internal
                //   property.
                // 4. Let args be a new list containing the same values as the
                //   list boundArgs in the same order followed by the same
                //   values as the list ExtraArgs in the same order.
                // 5. Return the result of calling the [[Construct]] internal
                //   method of target providing args as the arguments.

                var result = target.apply(
                    this,
                    args.concat(array_slice.call(arguments))
                );
                if (Object(result) === result) {
                    return result;
                }
                return this;

            } else {
                // 15.3.4.5.1 [[Call]]
                // When the [[Call]] internal method of a function object, F,
                // which was created using the bind function is called with a
                // this value and a list of arguments ExtraArgs, the following
                // steps are taken:
                // 1. Let boundArgs be the value of F's [[BoundArgs]] internal
                //   property.
                // 2. Let boundThis be the value of F's [[BoundThis]] internal
                //   property.
                // 3. Let target be the value of F's [[TargetFunction]] internal
                //   property.
                // 4. Let args be a new list containing the same values as the
                //   list boundArgs in the same order followed by the same
                //   values as the list ExtraArgs in the same order.
                // 5. Return the result of calling the [[Call]] internal method
                //   of target providing boundThis as the this value and
                //   providing args as the arguments.

                // equiv: target.call(this, ...boundArgs, ...args)
                return target.apply(
                    that,
                    args.concat(array_slice.call(arguments))
                );

            }

        };

        // 15. If the [[Class]] internal property of Target is "Function", then
        //     a. Let L be the length property of Target minus the length of A.
        //     b. Set the length own property of F to either 0 or L, whichever is
        //       larger.
        // 16. Else set the length own property of F to 0.

        var boundLength = Math.max(0, target.length - args.length);

        // 17. Set the attributes of the length own property of F to the values
        //   specified in 15.3.5.1.
        var boundArgs = [];
        for (var i = 0; i < boundLength; i++) {
            boundArgs.push('$' + i);
        }

        // XXX Build a dynamic function with desired amount of arguments is the only
        // way to set the length property of a function.
        // In environments where Content Security Policies enabled (Chrome extensions,
        // for ex.) all use of eval or Function costructor throws an exception.
        // However in all of these environments Function.prototype.bind exists
        // and so this code will never be executed.
        var bound = Function('binder', 'return function (' + boundArgs.join(',') + '){ return binder.apply(this, arguments); }')(binder);

        if (target.prototype) {
            Empty.prototype = target.prototype;
            bound.prototype = new Empty();
            // Clean up dangling references.
            Empty.prototype = null;
        }

        // TODO
        // 18. Set the [[Extensible]] internal property of F to true.

        // TODO
        // 19. Let thrower be the [[ThrowTypeError]] function Object (13.2.3).
        // 20. Call the [[DefineOwnProperty]] internal method of F with
        //   arguments "caller", PropertyDescriptor {[[Get]]: thrower, [[Set]]:
        //   thrower, [[Enumerable]]: false, [[Configurable]]: false}, and
        //   false.
        // 21. Call the [[DefineOwnProperty]] internal method of F with
        //   arguments "arguments", PropertyDescriptor {[[Get]]: thrower,
        //   [[Set]]: thrower, [[Enumerable]]: false, [[Configurable]]: false},
        //   and false.

        // TODO
        // NOTE Function objects created using Function.prototype.bind do not
        // have a prototype property or the [[Code]], [[FormalParameters]], and
        // [[Scope]] internal properties.
        // XXX can't delete prototype in pure-js.

        // 22. Return F.
        return bound;
    }
});

//
// Array
// =====
//

// ES5 15.4.3.2
// http://es5.github.com/#x15.4.3.2
// https://developer.mozilla.org/en/JavaScript/Reference/Global_Objects/Array/isArray
defineProperties(Array, { isArray: isArray });


var boxedString = Object('a');
var splitString = boxedString[0] !== 'a' || !(0 in boxedString);

var properlyBoxesContext = function properlyBoxed(method) {
    // Check node 0.6.21 bug where third parameter is not boxed
    var properlyBoxesNonStrict = true;
    var properlyBoxesStrict = true;
    if (method) {
        method.call('foo', function (_, __, context) {
            if (typeof context !== 'object') { properlyBoxesNonStrict = false; }
        });

        method.call([1], function () {
            'use strict';
            properlyBoxesStrict = typeof this === 'string';
        }, 'x');
    }
    return !!method && properlyBoxesNonStrict && properlyBoxesStrict;
};

defineProperties(ArrayPrototype, {
    forEach: function forEach(fun /*, thisp*/) {
        var object = toObject(this),
            self = splitString && isString(this) ? this.split('') : object,
            thisp = arguments[1],
            i = -1,
            length = self.length >>> 0;

        // If no callback function or if callback is not a callable function
        if (!isFunction(fun)) {
            throw new TypeError(); // TODO message
        }

        while (++i < length) {
            if (i in self) {
                // Invoke the callback function with call, passing arguments:
                // context, property value, property key, thisArg object
                // context
                fun.call(thisp, self[i], i, object);
            }
        }
    }
}, !properlyBoxesContext(ArrayPrototype.forEach));

// ES5 15.4.4.14
// http://es5.github.com/#x15.4.4.14
// https://developer.mozilla.org/en/JavaScript/Reference/Global_Objects/Array/indexOf
var hasFirefox2IndexOfBug = Array.prototype.indexOf && [0, 1].indexOf(1, 2) !== -1;
defineProperties(ArrayPrototype, {
    indexOf: function indexOf(sought /*, fromIndex */ ) {
        var self = splitString && isString(this) ? this.split('') : toObject(this),
            length = self.length >>> 0;

        if (!length) {
            return -1;
        }

        var i = 0;
        if (arguments.length > 1) {
            i = toInteger(arguments[1]);
        }

        // handle negative indices
        i = i >= 0 ? i : Math.max(0, length + i);
        for (; i < length; i++) {
            if (i in self && self[i] === sought) {
                return i;
            }
        }
        return -1;
    }
}, hasFirefox2IndexOfBug);

//
// String
// ======
//

// ES5 15.5.4.14
// http://es5.github.com/#x15.5.4.14

// [bugfix, IE lt 9, firefox 4, Konqueror, Opera, obscure browsers]
// Many browsers do not split properly with regular expressions or they
// do not perform the split correctly under obscure conditions.
// See http://blog.stevenlevithan.com/archives/cross-browser-split
// I've tested in many browsers and this seems to cover the deviant ones:
//    'ab'.split(/(?:ab)*/) should be ["", ""], not [""]
//    '.'.split(/(.?)(.?)/) should be ["", ".", "", ""], not ["", ""]
//    'tesst'.split(/(s)*/) should be ["t", undefined, "e", "s", "t"], not
//       [undefined, "t", undefined, "e", ...]
//    ''.split(/.?/) should be [], not [""]
//    '.'.split(/()()/) should be ["."], not ["", "", "."]

var string_split = StringPrototype.split;
if (
    'ab'.split(/(?:ab)*/).length !== 2 ||
    '.'.split(/(.?)(.?)/).length !== 4 ||
    'tesst'.split(/(s)*/)[1] === 't' ||
    'test'.split(/(?:)/, -1).length !== 4 ||
    ''.split(/.?/).length ||
    '.'.split(/()()/).length > 1
) {
    (function () {
        var compliantExecNpcg = /()??/.exec('')[1] === void 0; // NPCG: nonparticipating capturing group

        StringPrototype.split = function (separator, limit) {
            var string = this;
            if (separator === void 0 && limit === 0) {
                return [];
            }

            // If `separator` is not a regex, use native split
            if (_toString.call(separator) !== '[object RegExp]') {
                return string_split.call(this, separator, limit);
            }

            var output = [],
                flags = (separator.ignoreCase ? 'i' : '') +
                        (separator.multiline  ? 'm' : '') +
                        (separator.extended   ? 'x' : '') + // Proposed for ES6
                        (separator.sticky     ? 'y' : ''), // Firefox 3+
                lastLastIndex = 0,
                // Make `global` and avoid `lastIndex` issues by working with a copy
                separator2, match, lastIndex, lastLength;
            separator = new RegExp(separator.source, flags + 'g');
            string += ''; // Type-convert
            if (!compliantExecNpcg) {
                // Doesn't need flags gy, but they don't hurt
                separator2 = new RegExp('^' + separator.source + '$(?!\\s)', flags);
            }
            /* Values for `limit`, per the spec:
             * If undefined: 4294967295 // Math.pow(2, 32) - 1
             * If 0, Infinity, or NaN: 0
             * If positive number: limit = Math.floor(limit); if (limit > 4294967295) limit -= 4294967296;
             * If negative number: 4294967296 - Math.floor(Math.abs(limit))
             * If other: Type-convert, then use the above rules
             */
            limit = limit === void 0 ?
                -1 >>> 0 : // Math.pow(2, 32) - 1
                ToUint32(limit);
            while (match = separator.exec(string)) {
                // `separator.lastIndex` is not reliable cross-browser
                lastIndex = match.index + match[0].length;
                if (lastIndex > lastLastIndex) {
                    output.push(string.slice(lastLastIndex, match.index));
                    // Fix browsers whose `exec` methods don't consistently return `undefined` for
                    // nonparticipating capturing groups
                    if (!compliantExecNpcg && match.length > 1) {
                        match[0].replace(separator2, function () {
                            for (var i = 1; i < arguments.length - 2; i++) {
                                if (arguments[i] === void 0) {
                                    match[i] = void 0;
                                }
                            }
                        });
                    }
                    if (match.length > 1 && match.index < string.length) {
                        ArrayPrototype.push.apply(output, match.slice(1));
                    }
                    lastLength = match[0].length;
                    lastLastIndex = lastIndex;
                    if (output.length >= limit) {
                        break;
                    }
                }
                if (separator.lastIndex === match.index) {
                    separator.lastIndex++; // Avoid an infinite loop
                }
            }
            if (lastLastIndex === string.length) {
                if (lastLength || !separator.test('')) {
                    output.push('');
                }
            } else {
                output.push(string.slice(lastLastIndex));
            }
            return output.length > limit ? output.slice(0, limit) : output;
        };
    }());

// [bugfix, chrome]
// If separator is undefined, then the result array contains just one String,
// which is the this value (converted to a String). If limit is not undefined,
// then the output array is truncated so that it contains no more than limit
// elements.
// "0".split(undefined, 0) -> []
} else if ('0'.split(void 0, 0).length) {
    StringPrototype.split = function split(separator, limit) {
        if (separator === void 0 && limit === 0) { return []; }
        return string_split.call(this, separator, limit);
    };
}

// ECMA-262, 3rd B.2.3
// Not an ECMAScript standard, although ECMAScript 3rd Edition has a
// non-normative section suggesting uniform semantics and it should be
// normalized across all browsers
// [bugfix, IE lt 9] IE < 9 substr() with negative value not working in IE
var string_substr = StringPrototype.substr;
var hasNegativeSubstrBug = ''.substr && '0b'.substr(-1) !== 'b';
defineProperties(StringPrototype, {
    substr: function substr(start, length) {
        return string_substr.call(
            this,
            start < 0 ? ((start = this.length + start) < 0 ? 0 : start) : start,
            length
        );
    }
}, hasNegativeSubstrBug);

},{}],16:[function(require,module,exports){
'use strict';

module.exports = [
  // streaming transports
  require('./transport/websocket')
, require('./transport/xhr-streaming')
, require('./transport/xdr-streaming')
, require('./transport/eventsource')
, require('./transport/lib/iframe-wrap')(require('./transport/eventsource'))

  // polling transports
, require('./transport/htmlfile')
, require('./transport/lib/iframe-wrap')(require('./transport/htmlfile'))
, require('./transport/xhr-polling')
, require('./transport/xdr-polling')
, require('./transport/lib/iframe-wrap')(require('./transport/xhr-polling'))
, require('./transport/jsonp-polling')
];

},{"./transport/eventsource":20,"./transport/htmlfile":21,"./transport/jsonp-polling":23,"./transport/lib/iframe-wrap":26,"./transport/websocket":38,"./transport/xdr-polling":39,"./transport/xdr-streaming":40,"./transport/xhr-polling":41,"./transport/xhr-streaming":42}],17:[function(require,module,exports){
(function (process,global){
'use strict';

var EventEmitter = require('events').EventEmitter
  , inherits = require('inherits')
  , utils = require('../../utils/event')
  , urlUtils = require('../../utils/url')
  , XHR = global.XMLHttpRequest
  ;

var debug = function() {};
if (process.env.NODE_ENV !== 'production') {
  debug = require('debug')('sockjs-client:browser:xhr');
}

function AbstractXHRObject(method, url, payload, opts) {
  debug(method, url);
  var self = this;
  EventEmitter.call(this);

  setTimeout(function () {
    self._start(method, url, payload, opts);
  }, 0);
}

inherits(AbstractXHRObject, EventEmitter);

AbstractXHRObject.prototype._start = function(method, url, payload, opts) {
  var self = this;

  try {
    this.xhr = new XHR();
  } catch (x) {
    // intentionally empty
  }

  if (!this.xhr) {
    debug('no xhr');
    this.emit('finish', 0, 'no xhr support');
    this._cleanup();
    return;
  }

  // several browsers cache POSTs
  url = urlUtils.addQuery(url, 't=' + (+new Date()));

  // Explorer tends to keep connection open, even after the
  // tab gets closed: http://bugs.jquery.com/ticket/5280
  this.unloadRef = utils.unloadAdd(function() {
    debug('unload cleanup');
    self._cleanup(true);
  });
  try {
    this.xhr.open(method, url, true);
    if (this.timeout && 'timeout' in this.xhr) {
      this.xhr.timeout = this.timeout;
      this.xhr.ontimeout = function() {
        debug('xhr timeout');
        self.emit('finish', 0, '');
        self._cleanup(false);
      };
    }
  } catch (e) {
    debug('exception', e);
    // IE raises an exception on wrong port.
    this.emit('finish', 0, '');
    this._cleanup(false);
    return;
  }

  if ((!opts || !opts.noCredentials) && AbstractXHRObject.supportsCORS) {
    debug('withCredentials');
    // Mozilla docs says https://developer.mozilla.org/en/XMLHttpRequest :
    // "This never affects same-site requests."

    this.xhr.withCredentials = 'true';
  }
  if (opts && opts.headers) {
    for (var key in opts.headers) {
      this.xhr.setRequestHeader(key, opts.headers[key]);
    }
  }

  this.xhr.onreadystatechange = function() {
    if (self.xhr) {
      var x = self.xhr;
      var text, status;
      debug('readyState', x.readyState);
      switch (x.readyState) {
      case 3:
        // IE doesn't like peeking into responseText or status
        // on Microsoft.XMLHTTP and readystate=3
        try {
          status = x.status;
          text = x.responseText;
        } catch (e) {
          // intentionally empty
        }
        debug('status', status);
        // IE returns 1223 for 204: http://bugs.jquery.com/ticket/1450
        if (status === 1223) {
          status = 204;
        }

        // IE does return readystate == 3 for 404 answers.
        if (status === 200 && text && text.length > 0) {
          debug('chunk');
          self.emit('chunk', status, text);
        }
        break;
      case 4:
        status = x.status;
        debug('status', status);
        // IE returns 1223 for 204: http://bugs.jquery.com/ticket/1450
        if (status === 1223) {
          status = 204;
        }
        // IE returns this for a bad port
        // http://msdn.microsoft.com/en-us/library/windows/desktop/aa383770(v=vs.85).aspx
        if (status === 12005 || status === 12029) {
          status = 0;
        }

        debug('finish', status, x.responseText);
        self.emit('finish', status, x.responseText);
        self._cleanup(false);
        break;
      }
    }
  };

  try {
    self.xhr.send(payload);
  } catch (e) {
    self.emit('finish', 0, '');
    self._cleanup(false);
  }
};

AbstractXHRObject.prototype._cleanup = function(abort) {
  debug('cleanup');
  if (!this.xhr) {
    return;
  }
  this.removeAllListeners();
  utils.unloadDel(this.unloadRef);

  // IE needs this field to be a function
  this.xhr.onreadystatechange = function() {};
  if (this.xhr.ontimeout) {
    this.xhr.ontimeout = null;
  }

  if (abort) {
    try {
      this.xhr.abort();
    } catch (x) {
      // intentionally empty
    }
  }
  this.unloadRef = this.xhr = null;
};

AbstractXHRObject.prototype.close = function() {
  debug('close');
  this._cleanup(true);
};

AbstractXHRObject.enabled = !!XHR;
// override XMLHttpRequest for IE6/7
// obfuscate to avoid firewalls
var axo = ['Active'].concat('Object').join('X');
if (!AbstractXHRObject.enabled && (axo in global)) {
  debug('overriding xmlhttprequest');
  XHR = function() {
    try {
      return new global[axo]('Microsoft.XMLHTTP');
    } catch (e) {
      return null;
    }
  };
  AbstractXHRObject.enabled = !!new XHR();
}

var cors = false;
try {
  cors = 'withCredentials' in new XHR();
} catch (ignored) {
  // intentionally empty
}

AbstractXHRObject.supportsCORS = cors;

module.exports = AbstractXHRObject;

}).call(this,{ env: {} },typeof global !== "undefined" ? global : typeof self !== "undefined" ? self : typeof window !== "undefined" ? window : {})

},{"../../utils/event":46,"../../utils/url":52,"debug":55,"events":3,"inherits":57}],18:[function(require,module,exports){
(function (global){
module.exports = global.EventSource;

}).call(this,typeof global !== "undefined" ? global : typeof self !== "undefined" ? self : typeof window !== "undefined" ? window : {})

},{}],19:[function(require,module,exports){
(function (global){
'use strict';

var Driver = global.WebSocket || global.MozWebSocket;
if (Driver) {
	module.exports = function WebSocketBrowserDriver(url) {
		return new Driver(url);
	};
} else {
	module.exports = undefined;
}

}).call(this,typeof global !== "undefined" ? global : typeof self !== "undefined" ? self : typeof window !== "undefined" ? window : {})

},{}],20:[function(require,module,exports){
'use strict';

var inherits = require('inherits')
  , AjaxBasedTransport = require('./lib/ajax-based')
  , EventSourceReceiver = require('./receiver/eventsource')
  , XHRCorsObject = require('./sender/xhr-cors')
  , EventSourceDriver = require('eventsource')
  ;

function EventSourceTransport(transUrl) {
  if (!EventSourceTransport.enabled()) {
    throw new Error('Transport created when disabled');
  }

  AjaxBasedTransport.call(this, transUrl, '/eventsource', EventSourceReceiver, XHRCorsObject);
}

inherits(EventSourceTransport, AjaxBasedTransport);

EventSourceTransport.enabled = function() {
  return !!EventSourceDriver;
};

EventSourceTransport.transportName = 'eventsource';
EventSourceTransport.roundTrips = 2;

module.exports = EventSourceTransport;

},{"./lib/ajax-based":24,"./receiver/eventsource":29,"./sender/xhr-cors":35,"eventsource":18,"inherits":57}],21:[function(require,module,exports){
'use strict';

var inherits = require('inherits')
  , HtmlfileReceiver = require('./receiver/htmlfile')
  , XHRLocalObject = require('./sender/xhr-local')
  , AjaxBasedTransport = require('./lib/ajax-based')
  ;

function HtmlFileTransport(transUrl) {
  if (!HtmlfileReceiver.enabled) {
    throw new Error('Transport created when disabled');
  }
  AjaxBasedTransport.call(this, transUrl, '/htmlfile', HtmlfileReceiver, XHRLocalObject);
}

inherits(HtmlFileTransport, AjaxBasedTransport);

HtmlFileTransport.enabled = function(info) {
  return HtmlfileReceiver.enabled && info.sameOrigin;
};

HtmlFileTransport.transportName = 'htmlfile';
HtmlFileTransport.roundTrips = 2;

module.exports = HtmlFileTransport;

},{"./lib/ajax-based":24,"./receiver/htmlfile":30,"./sender/xhr-local":37,"inherits":57}],22:[function(require,module,exports){
(function (process){
'use strict';

// Few cool transports do work only for same-origin. In order to make
// them work cross-domain we shall use iframe, served from the
// remote domain. New browsers have capabilities to communicate with
// cross domain iframe using postMessage(). In IE it was implemented
// from IE 8+, but of course, IE got some details wrong:
//    http://msdn.microsoft.com/en-us/library/cc197015(v=VS.85).aspx
//    http://stevesouders.com/misc/test-postmessage.php

var inherits = require('inherits')
  , JSON3 = require('json3')
  , EventEmitter = require('events').EventEmitter
  , version = require('../version')
  , urlUtils = require('../utils/url')
  , iframeUtils = require('../utils/iframe')
  , eventUtils = require('../utils/event')
  , random = require('../utils/random')
  ;

var debug = function() {};
if (process.env.NODE_ENV !== 'production') {
  debug = require('debug')('sockjs-client:transport:iframe');
}

function IframeTransport(transport, transUrl, baseUrl) {
  if (!IframeTransport.enabled()) {
    throw new Error('Transport created when disabled');
  }
  EventEmitter.call(this);

  var self = this;
  this.origin = urlUtils.getOrigin(baseUrl);
  this.baseUrl = baseUrl;
  this.transUrl = transUrl;
  this.transport = transport;
  this.windowId = random.string(8);

  var iframeUrl = urlUtils.addPath(baseUrl, '/iframe.html') + '#' + this.windowId;
  debug(transport, transUrl, iframeUrl);

  this.iframeObj = iframeUtils.createIframe(iframeUrl, function(r) {
    debug('err callback');
    self.emit('close', 1006, 'Unable to load an iframe (' + r + ')');
    self.close();
  });

  this.onmessageCallback = this._message.bind(this);
  eventUtils.attachEvent('message', this.onmessageCallback);
}

inherits(IframeTransport, EventEmitter);

IframeTransport.prototype.close = function() {
  debug('close');
  this.removeAllListeners();
  if (this.iframeObj) {
    eventUtils.detachEvent('message', this.onmessageCallback);
    try {
      // When the iframe is not loaded, IE raises an exception
      // on 'contentWindow'.
      this.postMessage('c');
    } catch (x) {
      // intentionally empty
    }
    this.iframeObj.cleanup();
    this.iframeObj = null;
    this.onmessageCallback = this.iframeObj = null;
  }
};

IframeTransport.prototype._message = function(e) {
  debug('message', e.data);
  if (!urlUtils.isOriginEqual(e.origin, this.origin)) {
    debug('not same origin', e.origin, this.origin);
    return;
  }

  var iframeMessage;
  try {
    iframeMessage = JSON3.parse(e.data);
  } catch (ignored) {
    debug('bad json', e.data);
    return;
  }

  if (iframeMessage.windowId !== this.windowId) {
    debug('mismatched window id', iframeMessage.windowId, this.windowId);
    return;
  }

  switch (iframeMessage.type) {
  case 's':
    this.iframeObj.loaded();
    // window global dependency
    this.postMessage('s', JSON3.stringify([
      version
    , this.transport
    , this.transUrl
    , this.baseUrl
    ]));
    break;
  case 't':
    this.emit('message', iframeMessage.data);
    break;
  case 'c':
    var cdata;
    try {
      cdata = JSON3.parse(iframeMessage.data);
    } catch (ignored) {
      debug('bad json', iframeMessage.data);
      return;
    }
    this.emit('close', cdata[0], cdata[1]);
    this.close();
    break;
  }
};

IframeTransport.prototype.postMessage = function(type, data) {
  debug('postMessage', type, data);
  this.iframeObj.post(JSON3.stringify({
    windowId: this.windowId
  , type: type
  , data: data || ''
  }), this.origin);
};

IframeTransport.prototype.send = function(message) {
  debug('send', message);
  this.postMessage('m', message);
};

IframeTransport.enabled = function() {
  return iframeUtils.iframeEnabled;
};

IframeTransport.transportName = 'iframe';
IframeTransport.roundTrips = 2;

module.exports = IframeTransport;

}).call(this,{ env: {} })

},{"../utils/event":46,"../utils/iframe":47,"../utils/random":50,"../utils/url":52,"../version":53,"debug":55,"events":3,"inherits":57,"json3":58}],23:[function(require,module,exports){
(function (global){
'use strict';

// The simplest and most robust transport, using the well-know cross
// domain hack - JSONP. This transport is quite inefficient - one
// message could use up to one http request. But at least it works almost
// everywhere.
// Known limitations:
//   o you will get a spinning cursor
//   o for Konqueror a dumb timer is needed to detect errors

var inherits = require('inherits')
  , SenderReceiver = require('./lib/sender-receiver')
  , JsonpReceiver = require('./receiver/jsonp')
  , jsonpSender = require('./sender/jsonp')
  ;

function JsonPTransport(transUrl) {
  if (!JsonPTransport.enabled()) {
    throw new Error('Transport created when disabled');
  }
  SenderReceiver.call(this, transUrl, '/jsonp', jsonpSender, JsonpReceiver);
}

inherits(JsonPTransport, SenderReceiver);

JsonPTransport.enabled = function() {
  return !!global.document;
};

JsonPTransport.transportName = 'jsonp-polling';
JsonPTransport.roundTrips = 1;
JsonPTransport.needBody = true;

module.exports = JsonPTransport;

}).call(this,typeof global !== "undefined" ? global : typeof self !== "undefined" ? self : typeof window !== "undefined" ? window : {})

},{"./lib/sender-receiver":28,"./receiver/jsonp":31,"./sender/jsonp":33,"inherits":57}],24:[function(require,module,exports){
(function (process){
'use strict';

var inherits = require('inherits')
  , urlUtils = require('../../utils/url')
  , SenderReceiver = require('./sender-receiver')
  ;

var debug = function() {};
if (process.env.NODE_ENV !== 'production') {
  debug = require('debug')('sockjs-client:ajax-based');
}

function createAjaxSender(AjaxObject) {
  return function(url, payload, callback) {
    debug('create ajax sender', url, payload);
    var opt = {};
    if (typeof payload === 'string') {
      opt.headers = {'Content-type': 'text/plain'};
    }
    var ajaxUrl = urlUtils.addPath(url, '/xhr_send');
    var xo = new AjaxObject('POST', ajaxUrl, payload, opt);
    xo.once('finish', function(status) {
      debug('finish', status);
      xo = null;

      if (status !== 200 && status !== 204) {
        return callback(new Error('http status ' + status));
      }
      callback();
    });
    return function() {
      debug('abort');
      xo.close();
      xo = null;

      var err = new Error('Aborted');
      err.code = 1000;
      callback(err);
    };
  };
}

function AjaxBasedTransport(transUrl, urlSuffix, Receiver, AjaxObject) {
  SenderReceiver.call(this, transUrl, urlSuffix, createAjaxSender(AjaxObject), Receiver, AjaxObject);
}

inherits(AjaxBasedTransport, SenderReceiver);

module.exports = AjaxBasedTransport;

}).call(this,{ env: {} })

},{"../../utils/url":52,"./sender-receiver":28,"debug":55,"inherits":57}],25:[function(require,module,exports){
(function (process){
'use strict';

var inherits = require('inherits')
  , EventEmitter = require('events').EventEmitter
  ;

var debug = function() {};
if (process.env.NODE_ENV !== 'production') {
  debug = require('debug')('sockjs-client:buffered-sender');
}

function BufferedSender(url, sender) {
  debug(url);
  EventEmitter.call(this);
  this.sendBuffer = [];
  this.sender = sender;
  this.url = url;
}

inherits(BufferedSender, EventEmitter);

BufferedSender.prototype.send = function(message) {
  debug('send', message);
  this.sendBuffer.push(message);
  if (!this.sendStop) {
    this.sendSchedule();
  }
};

// For polling transports in a situation when in the message callback,
// new message is being send. If the sending connection was started
// before receiving one, it is possible to saturate the network and
// timeout due to the lack of receiving socket. To avoid that we delay
// sending messages by some small time, in order to let receiving
// connection be started beforehand. This is only a halfmeasure and
// does not fix the big problem, but it does make the tests go more
// stable on slow networks.
BufferedSender.prototype.sendScheduleWait = function() {
  debug('sendScheduleWait');
  var self = this;
  var tref;
  this.sendStop = function() {
    debug('sendStop');
    self.sendStop = null;
    clearTimeout(tref);
  };
  tref = setTimeout(function() {
    debug('timeout');
    self.sendStop = null;
    self.sendSchedule();
  }, 25);
};

BufferedSender.prototype.sendSchedule = function() {
  debug('sendSchedule', this.sendBuffer.length);
  var self = this;
  if (this.sendBuffer.length > 0) {
    var payload = '[' + this.sendBuffer.join(',') + ']';
    this.sendStop = this.sender(this.url, payload, function(err) {
      self.sendStop = null;
      if (err) {
        debug('error', err);
        self.emit('close', err.code || 1006, 'Sending error: ' + err);
        self.close();
      } else {
        self.sendScheduleWait();
      }
    });
    this.sendBuffer = [];
  }
};

BufferedSender.prototype._cleanup = function() {
  debug('_cleanup');
  this.removeAllListeners();
};

BufferedSender.prototype.close = function() {
  debug('close');
  this._cleanup();
  if (this.sendStop) {
    this.sendStop();
    this.sendStop = null;
  }
};

module.exports = BufferedSender;

}).call(this,{ env: {} })

},{"debug":55,"events":3,"inherits":57}],26:[function(require,module,exports){
(function (global){
'use strict';

var inherits = require('inherits')
  , IframeTransport = require('../iframe')
  , objectUtils = require('../../utils/object')
  ;

module.exports = function(transport) {

  function IframeWrapTransport(transUrl, baseUrl) {
    IframeTransport.call(this, transport.transportName, transUrl, baseUrl);
  }

  inherits(IframeWrapTransport, IframeTransport);

  IframeWrapTransport.enabled = function(url, info) {
    if (!global.document) {
      return false;
    }

    var iframeInfo = objectUtils.extend({}, info);
    iframeInfo.sameOrigin = true;
    return transport.enabled(iframeInfo) && IframeTransport.enabled();
  };

  IframeWrapTransport.transportName = 'iframe-' + transport.transportName;
  IframeWrapTransport.needBody = true;
  IframeWrapTransport.roundTrips = IframeTransport.roundTrips + transport.roundTrips - 1; // html, javascript (2) + transport - no CORS (1)

  IframeWrapTransport.facadeTransport = transport;

  return IframeWrapTransport;
};

}).call(this,typeof global !== "undefined" ? global : typeof self !== "undefined" ? self : typeof window !== "undefined" ? window : {})

},{"../../utils/object":49,"../iframe":22,"inherits":57}],27:[function(require,module,exports){
(function (process){
'use strict';

var inherits = require('inherits')
  , EventEmitter = require('events').EventEmitter
  ;

var debug = function() {};
if (process.env.NODE_ENV !== 'production') {
  debug = require('debug')('sockjs-client:polling');
}

function Polling(Receiver, receiveUrl, AjaxObject) {
  debug(receiveUrl);
  EventEmitter.call(this);
  this.Receiver = Receiver;
  this.receiveUrl = receiveUrl;
  this.AjaxObject = AjaxObject;
  this._scheduleReceiver();
}

inherits(Polling, EventEmitter);

Polling.prototype._scheduleReceiver = function() {
  debug('_scheduleReceiver');
  var self = this;
  var poll = this.poll = new this.Receiver(this.receiveUrl, this.AjaxObject);

  poll.on('message', function(msg) {
    debug('message', msg);
    self.emit('message', msg);
  });

  poll.once('close', function(code, reason) {
    debug('close', code, reason, self.pollIsClosing);
    self.poll = poll = null;

    if (!self.pollIsClosing) {
      if (reason === 'network') {
        self._scheduleReceiver();
      } else {
        self.emit('close', code || 1006, reason);
        self.removeAllListeners();
      }
    }
  });
};

Polling.prototype.abort = function() {
  debug('abort');
  this.removeAllListeners();
  this.pollIsClosing = true;
  if (this.poll) {
    this.poll.abort();
  }
};

module.exports = Polling;

}).call(this,{ env: {} })

},{"debug":55,"events":3,"inherits":57}],28:[function(require,module,exports){
(function (process){
'use strict';

var inherits = require('inherits')
  , urlUtils = require('../../utils/url')
  , BufferedSender = require('./buffered-sender')
  , Polling = require('./polling')
  ;

var debug = function() {};
if (process.env.NODE_ENV !== 'production') {
  debug = require('debug')('sockjs-client:sender-receiver');
}

function SenderReceiver(transUrl, urlSuffix, senderFunc, Receiver, AjaxObject) {
  var pollUrl = urlUtils.addPath(transUrl, urlSuffix);
  debug(pollUrl);
  var self = this;
  BufferedSender.call(this, transUrl, senderFunc);

  this.poll = new Polling(Receiver, pollUrl, AjaxObject);
  this.poll.on('message', function(msg) {
    debug('poll message', msg);
    self.emit('message', msg);
  });
  this.poll.once('close', function(code, reason) {
    debug('poll close', code, reason);
    self.poll = null;
    self.emit('close', code, reason);
    self.close();
  });
}

inherits(SenderReceiver, BufferedSender);

SenderReceiver.prototype.close = function() {
  BufferedSender.prototype.close.call(this);
  debug('close');
  this.removeAllListeners();
  if (this.poll) {
    this.poll.abort();
    this.poll = null;
  }
};

module.exports = SenderReceiver;

}).call(this,{ env: {} })

},{"../../utils/url":52,"./buffered-sender":25,"./polling":27,"debug":55,"inherits":57}],29:[function(require,module,exports){
(function (process){
'use strict';

var inherits = require('inherits')
  , EventEmitter = require('events').EventEmitter
  , EventSourceDriver = require('eventsource')
  ;

var debug = function() {};
if (process.env.NODE_ENV !== 'production') {
  debug = require('debug')('sockjs-client:receiver:eventsource');
}

function EventSourceReceiver(url) {
  debug(url);
  EventEmitter.call(this);

  var self = this;
  var es = this.es = new EventSourceDriver(url);
  es.onmessage = function(e) {
    debug('message', e.data);
    self.emit('message', decodeURI(e.data));
  };
  es.onerror = function(e) {
    debug('error', es.readyState, e);
    // ES on reconnection has readyState = 0 or 1.
    // on network error it's CLOSED = 2
    var reason = (es.readyState !== 2 ? 'network' : 'permanent');
    self._cleanup();
    self._close(reason);
  };
}

inherits(EventSourceReceiver, EventEmitter);

EventSourceReceiver.prototype.abort = function() {
  debug('abort');
  this._cleanup();
  this._close('user');
};

EventSourceReceiver.prototype._cleanup = function() {
  debug('cleanup');
  var es = this.es;
  if (es) {
    es.onmessage = es.onerror = null;
    es.close();
    this.es = null;
  }
};

EventSourceReceiver.prototype._close = function(reason) {
  debug('close', reason);
  var self = this;
  // Safari and chrome < 15 crash if we close window before
  // waiting for ES cleanup. See:
  // https://code.google.com/p/chromium/issues/detail?id=89155
  setTimeout(function() {
    self.emit('close', null, reason);
    self.removeAllListeners();
  }, 200);
};

module.exports = EventSourceReceiver;

}).call(this,{ env: {} })

},{"debug":55,"events":3,"eventsource":18,"inherits":57}],30:[function(require,module,exports){
(function (process,global){
'use strict';

var inherits = require('inherits')
  , iframeUtils = require('../../utils/iframe')
  , urlUtils = require('../../utils/url')
  , EventEmitter = require('events').EventEmitter
  , random = require('../../utils/random')
  ;

var debug = function() {};
if (process.env.NODE_ENV !== 'production') {
  debug = require('debug')('sockjs-client:receiver:htmlfile');
}

function HtmlfileReceiver(url) {
  debug(url);
  EventEmitter.call(this);
  var self = this;
  iframeUtils.polluteGlobalNamespace();

  this.id = 'a' + random.string(6);
  url = urlUtils.addQuery(url, 'c=' + decodeURIComponent(iframeUtils.WPrefix + '.' + this.id));

  debug('using htmlfile', HtmlfileReceiver.htmlfileEnabled);
  var constructFunc = HtmlfileReceiver.htmlfileEnabled ?
      iframeUtils.createHtmlfile : iframeUtils.createIframe;

  global[iframeUtils.WPrefix][this.id] = {
    start: function() {
      debug('start');
      self.iframeObj.loaded();
    }
  , message: function(data) {
      debug('message', data);
      self.emit('message', data);
    }
  , stop: function() {
      debug('stop');
      self._cleanup();
      self._close('network');
    }
  };
  this.iframeObj = constructFunc(url, function() {
    debug('callback');
    self._cleanup();
    self._close('permanent');
  });
}

inherits(HtmlfileReceiver, EventEmitter);

HtmlfileReceiver.prototype.abort = function() {
  debug('abort');
  this._cleanup();
  this._close('user');
};

HtmlfileReceiver.prototype._cleanup = function() {
  debug('_cleanup');
  if (this.iframeObj) {
    this.iframeObj.cleanup();
    this.iframeObj = null;
  }
  delete global[iframeUtils.WPrefix][this.id];
};

HtmlfileReceiver.prototype._close = function(reason) {
  debug('_close', reason);
  this.emit('close', null, reason);
  this.removeAllListeners();
};

HtmlfileReceiver.htmlfileEnabled = false;

// obfuscate to avoid firewalls
var axo = ['Active'].concat('Object').join('X');
if (axo in global) {
  try {
    HtmlfileReceiver.htmlfileEnabled = !!new global[axo]('htmlfile');
  } catch (x) {
    // intentionally empty
  }
}

HtmlfileReceiver.enabled = HtmlfileReceiver.htmlfileEnabled || iframeUtils.iframeEnabled;

module.exports = HtmlfileReceiver;

}).call(this,{ env: {} },typeof global !== "undefined" ? global : typeof self !== "undefined" ? self : typeof window !== "undefined" ? window : {})

},{"../../utils/iframe":47,"../../utils/random":50,"../../utils/url":52,"debug":55,"events":3,"inherits":57}],31:[function(require,module,exports){
(function (process,global){
'use strict';

var utils = require('../../utils/iframe')
  , random = require('../../utils/random')
  , browser = require('../../utils/browser')
  , urlUtils = require('../../utils/url')
  , inherits = require('inherits')
  , EventEmitter = require('events').EventEmitter
  ;

var debug = function() {};
if (process.env.NODE_ENV !== 'production') {
  debug = require('debug')('sockjs-client:receiver:jsonp');
}

function JsonpReceiver(url) {
  debug(url);
  var self = this;
  EventEmitter.call(this);

  utils.polluteGlobalNamespace();

  this.id = 'a' + random.string(6);
  var urlWithId = urlUtils.addQuery(url, 'c=' + encodeURIComponent(utils.WPrefix + '.' + this.id));

  global[utils.WPrefix][this.id] = this._callback.bind(this);
  this._createScript(urlWithId);

  // Fallback mostly for Konqueror - stupid timer, 35 seconds shall be plenty.
  this.timeoutId = setTimeout(function() {
    debug('timeout');
    self._abort(new Error('JSONP script loaded abnormally (timeout)'));
  }, JsonpReceiver.timeout);
}

inherits(JsonpReceiver, EventEmitter);

JsonpReceiver.prototype.abort = function() {
  debug('abort');
  if (global[utils.WPrefix][this.id]) {
    var err = new Error('JSONP user aborted read');
    err.code = 1000;
    this._abort(err);
  }
};

JsonpReceiver.timeout = 35000;
JsonpReceiver.scriptErrorTimeout = 1000;

JsonpReceiver.prototype._callback = function(data) {
  debug('_callback', data);
  this._cleanup();

  if (this.aborting) {
    return;
  }

  if (data) {
    debug('message', data);
    this.emit('message', data);
  }
  this.emit('close', null, 'network');
  this.removeAllListeners();
};

JsonpReceiver.prototype._abort = function(err) {
  debug('_abort', err);
  this._cleanup();
  this.aborting = true;
  this.emit('close', err.code, err.message);
  this.removeAllListeners();
};

JsonpReceiver.prototype._cleanup = function() {
  debug('_cleanup');
  clearTimeout(this.timeoutId);
  if (this.script2) {
    this.script2.parentNode.removeChild(this.script2);
    this.script2 = null;
  }
  if (this.script) {
    var script = this.script;
    // Unfortunately, you can't really abort script loading of
    // the script.
    script.parentNode.removeChild(script);
    script.onreadystatechange = script.onerror =
        script.onload = script.onclick = null;
    this.script = null;
  }
  delete global[utils.WPrefix][this.id];
};

JsonpReceiver.prototype._scriptError = function() {
  debug('_scriptError');
  var self = this;
  if (this.errorTimer) {
    return;
  }

  this.errorTimer = setTimeout(function() {
    if (!self.loadedOkay) {
      self._abort(new Error('JSONP script loaded abnormally (onerror)'));
    }
  }, JsonpReceiver.scriptErrorTimeout);
};

JsonpReceiver.prototype._createScript = function(url) {
  debug('_createScript', url);
  var self = this;
  var script = this.script = global.document.createElement('script');
  var script2;  // Opera synchronous load trick.

  script.id = 'a' + random.string(8);
  script.src = url;
  script.type = 'text/javascript';
  script.charset = 'UTF-8';
  script.onerror = this._scriptError.bind(this);
  script.onload = function() {
    debug('onload');
    self._abort(new Error('JSONP script loaded abnormally (onload)'));
  };

  // IE9 fires 'error' event after onreadystatechange or before, in random order.
  // Use loadedOkay to determine if actually errored
  script.onreadystatechange = function() {
    debug('onreadystatechange', script.readyState);
    if (/loaded|closed/.test(script.readyState)) {
      if (script && script.htmlFor && script.onclick) {
        self.loadedOkay = true;
        try {
          // In IE, actually execute the script.
          script.onclick();
        } catch (x) {
          // intentionally empty
        }
      }
      if (script) {
        self._abort(new Error('JSONP script loaded abnormally (onreadystatechange)'));
      }
    }
  };
  // IE: event/htmlFor/onclick trick.
  // One can't rely on proper order for onreadystatechange. In order to
  // make sure, set a 'htmlFor' and 'event' properties, so that
  // script code will be installed as 'onclick' handler for the
  // script object. Later, onreadystatechange, manually execute this
  // code. FF and Chrome doesn't work with 'event' and 'htmlFor'
  // set. For reference see:
  //   http://jaubourg.net/2010/07/loading-script-as-onclick-handler-of.html
  // Also, read on that about script ordering:
  //   http://wiki.whatwg.org/wiki/Dynamic_Script_Execution_Order
  if (typeof script.async === 'undefined' && global.document.attachEvent) {
    // According to mozilla docs, in recent browsers script.async defaults
    // to 'true', so we may use it to detect a good browser:
    // https://developer.mozilla.org/en/HTML/Element/script
    if (!browser.isOpera()) {
      // Naively assume we're in IE
      try {
        script.htmlFor = script.id;
        script.event = 'onclick';
      } catch (x) {
        // intentionally empty
      }
      script.async = true;
    } else {
      // Opera, second sync script hack
      script2 = this.script2 = global.document.createElement('script');
      script2.text = "try{var a = document.getElementById('" + script.id + "'); if(a)a.onerror();}catch(x){};";
      script.async = script2.async = false;
    }
  }
  if (typeof script.async !== 'undefined') {
    script.async = true;
  }

  var head = global.document.getElementsByTagName('head')[0];
  head.insertBefore(script, head.firstChild);
  if (script2) {
    head.insertBefore(script2, head.firstChild);
  }
};

module.exports = JsonpReceiver;

}).call(this,{ env: {} },typeof global !== "undefined" ? global : typeof self !== "undefined" ? self : typeof window !== "undefined" ? window : {})

},{"../../utils/browser":44,"../../utils/iframe":47,"../../utils/random":50,"../../utils/url":52,"debug":55,"events":3,"inherits":57}],32:[function(require,module,exports){
(function (process){
'use strict';

var inherits = require('inherits')
  , EventEmitter = require('events').EventEmitter
  ;

var debug = function() {};
if (process.env.NODE_ENV !== 'production') {
  debug = require('debug')('sockjs-client:receiver:xhr');
}

function XhrReceiver(url, AjaxObject) {
  debug(url);
  EventEmitter.call(this);
  var self = this;

  this.bufferPosition = 0;

  this.xo = new AjaxObject('POST', url, null);
  this.xo.on('chunk', this._chunkHandler.bind(this));
  this.xo.once('finish', function(status, text) {
    debug('finish', status, text);
    self._chunkHandler(status, text);
    self.xo = null;
    var reason = status === 200 ? 'network' : 'permanent';
    debug('close', reason);
    self.emit('close', null, reason);
    self._cleanup();
  });
}

inherits(XhrReceiver, EventEmitter);

XhrReceiver.prototype._chunkHandler = function(status, text) {
  debug('_chunkHandler', status);
  if (status !== 200 || !text) {
    return;
  }

  for (var idx = -1; ; this.bufferPosition += idx + 1) {
    var buf = text.slice(this.bufferPosition);
    idx = buf.indexOf('\n');
    if (idx === -1) {
      break;
    }
    var msg = buf.slice(0, idx);
    if (msg) {
      debug('message', msg);
      this.emit('message', msg);
    }
  }
};

XhrReceiver.prototype._cleanup = function() {
  debug('_cleanup');
  this.removeAllListeners();
};

XhrReceiver.prototype.abort = function() {
  debug('abort');
  if (this.xo) {
    this.xo.close();
    debug('close');
    this.emit('close', null, 'user');
    this.xo = null;
  }
  this._cleanup();
};

module.exports = XhrReceiver;

}).call(this,{ env: {} })

},{"debug":55,"events":3,"inherits":57}],33:[function(require,module,exports){
(function (process,global){
'use strict';

var random = require('../../utils/random')
  , urlUtils = require('../../utils/url')
  ;

var debug = function() {};
if (process.env.NODE_ENV !== 'production') {
  debug = require('debug')('sockjs-client:sender:jsonp');
}

var form, area;

function createIframe(id) {
  debug('createIframe', id);
  try {
    // ie6 dynamic iframes with target="" support (thanks Chris Lambacher)
    return global.document.createElement('<iframe name="' + id + '">');
  } catch (x) {
    var iframe = global.document.createElement('iframe');
    iframe.name = id;
    return iframe;
  }
}

function createForm() {
  debug('createForm');
  form = global.document.createElement('form');
  form.style.display = 'none';
  form.style.position = 'absolute';
  form.method = 'POST';
  form.enctype = 'application/x-www-form-urlencoded';
  form.acceptCharset = 'UTF-8';

  area = global.document.createElement('textarea');
  area.name = 'd';
  form.appendChild(area);

  global.document.body.appendChild(form);
}

module.exports = function(url, payload, callback) {
  debug(url, payload);
  if (!form) {
    createForm();
  }
  var id = 'a' + random.string(8);
  form.target = id;
  form.action = urlUtils.addQuery(urlUtils.addPath(url, '/jsonp_send'), 'i=' + id);

  var iframe = createIframe(id);
  iframe.id = id;
  iframe.style.display = 'none';
  form.appendChild(iframe);

  try {
    area.value = payload;
  } catch (e) {
    // seriously broken browsers get here
  }
  form.submit();

  var completed = function(err) {
    debug('completed', id, err);
    if (!iframe.onerror) {
      return;
    }
    iframe.onreadystatechange = iframe.onerror = iframe.onload = null;
    // Opera mini doesn't like if we GC iframe
    // immediately, thus this timeout.
    setTimeout(function() {
      debug('cleaning up', id);
      iframe.parentNode.removeChild(iframe);
      iframe = null;
    }, 500);
    area.value = '';
    // It is not possible to detect if the iframe succeeded or
    // failed to submit our form.
    callback(err);
  };
  iframe.onerror = function() {
    debug('onerror', id);
    completed();
  };
  iframe.onload = function() {
    debug('onload', id);
    completed();
  };
  iframe.onreadystatechange = function(e) {
    debug('onreadystatechange', id, iframe.readyState, e);
    if (iframe.readyState === 'complete') {
      completed();
    }
  };
  return function() {
    debug('aborted', id);
    completed(new Error('Aborted'));
  };
};

}).call(this,{ env: {} },typeof global !== "undefined" ? global : typeof self !== "undefined" ? self : typeof window !== "undefined" ? window : {})

},{"../../utils/random":50,"../../utils/url":52,"debug":55}],34:[function(require,module,exports){
(function (process,global){
'use strict';

var EventEmitter = require('events').EventEmitter
  , inherits = require('inherits')
  , eventUtils = require('../../utils/event')
  , browser = require('../../utils/browser')
  , urlUtils = require('../../utils/url')
  ;

var debug = function() {};
if (process.env.NODE_ENV !== 'production') {
  debug = require('debug')('sockjs-client:sender:xdr');
}

// References:
//   http://ajaxian.com/archives/100-line-ajax-wrapper
//   http://msdn.microsoft.com/en-us/library/cc288060(v=VS.85).aspx

function XDRObject(method, url, payload) {
  debug(method, url);
  var self = this;
  EventEmitter.call(this);

  setTimeout(function() {
    self._start(method, url, payload);
  }, 0);
}

inherits(XDRObject, EventEmitter);

XDRObject.prototype._start = function(method, url, payload) {
  debug('_start');
  var self = this;
  var xdr = new global.XDomainRequest();
  // IE caches even POSTs
  url = urlUtils.addQuery(url, 't=' + (+new Date()));

  xdr.onerror = function() {
    debug('onerror');
    self._error();
  };
  xdr.ontimeout = function() {
    debug('ontimeout');
    self._error();
  };
  xdr.onprogress = function() {
    debug('progress', xdr.responseText);
    self.emit('chunk', 200, xdr.responseText);
  };
  xdr.onload = function() {
    debug('load');
    self.emit('finish', 200, xdr.responseText);
    self._cleanup(false);
  };
  this.xdr = xdr;
  this.unloadRef = eventUtils.unloadAdd(function() {
    self._cleanup(true);
  });
  try {
    // Fails with AccessDenied if port number is bogus
    this.xdr.open(method, url);
    if (this.timeout) {
      this.xdr.timeout = this.timeout;
    }
    this.xdr.send(payload);
  } catch (x) {
    this._error();
  }
};

XDRObject.prototype._error = function() {
  this.emit('finish', 0, '');
  this._cleanup(false);
};

XDRObject.prototype._cleanup = function(abort) {
  debug('cleanup', abort);
  if (!this.xdr) {
    return;
  }
  this.removeAllListeners();
  eventUtils.unloadDel(this.unloadRef);

  this.xdr.ontimeout = this.xdr.onerror = this.xdr.onprogress = this.xdr.onload = null;
  if (abort) {
    try {
      this.xdr.abort();
    } catch (x) {
      // intentionally empty
    }
  }
  this.unloadRef = this.xdr = null;
};

XDRObject.prototype.close = function() {
  debug('close');
  this._cleanup(true);
};

// IE 8/9 if the request target uses the same scheme - #79
XDRObject.enabled = !!(global.XDomainRequest && browser.hasDomain());

module.exports = XDRObject;

}).call(this,{ env: {} },typeof global !== "undefined" ? global : typeof self !== "undefined" ? self : typeof window !== "undefined" ? window : {})

},{"../../utils/browser":44,"../../utils/event":46,"../../utils/url":52,"debug":55,"events":3,"inherits":57}],35:[function(require,module,exports){
'use strict';

var inherits = require('inherits')
  , XhrDriver = require('../driver/xhr')
  ;

function XHRCorsObject(method, url, payload, opts) {
  XhrDriver.call(this, method, url, payload, opts);
}

inherits(XHRCorsObject, XhrDriver);

XHRCorsObject.enabled = XhrDriver.enabled && XhrDriver.supportsCORS;

module.exports = XHRCorsObject;

},{"../driver/xhr":17,"inherits":57}],36:[function(require,module,exports){
'use strict';

var EventEmitter = require('events').EventEmitter
  , inherits = require('inherits')
  ;

function XHRFake(/* method, url, payload, opts */) {
  var self = this;
  EventEmitter.call(this);

  this.to = setTimeout(function() {
    self.emit('finish', 200, '{}');
  }, XHRFake.timeout);
}

inherits(XHRFake, EventEmitter);

XHRFake.prototype.close = function() {
  clearTimeout(this.to);
};

XHRFake.timeout = 2000;

module.exports = XHRFake;

},{"events":3,"inherits":57}],37:[function(require,module,exports){
'use strict';

var inherits = require('inherits')
  , XhrDriver = require('../driver/xhr')
  ;

function XHRLocalObject(method, url, payload /*, opts */) {
  XhrDriver.call(this, method, url, payload, {
    noCredentials: true
  });
}

inherits(XHRLocalObject, XhrDriver);

XHRLocalObject.enabled = XhrDriver.enabled;

module.exports = XHRLocalObject;

},{"../driver/xhr":17,"inherits":57}],38:[function(require,module,exports){
(function (process){
'use strict';

var utils = require('../utils/event')
  , urlUtils = require('../utils/url')
  , inherits = require('inherits')
  , EventEmitter = require('events').EventEmitter
  , WebsocketDriver = require('./driver/websocket')
  ;

var debug = function() {};
if (process.env.NODE_ENV !== 'production') {
  debug = require('debug')('sockjs-client:websocket');
}

function WebSocketTransport(transUrl, ignore, options) {
  if (!WebSocketTransport.enabled()) {
    throw new Error('Transport created when disabled');
  }

  EventEmitter.call(this);
  debug('constructor', transUrl);

  var self = this;
  var url = urlUtils.addPath(transUrl, '/websocket');
  if (url.slice(0, 5) === 'https') {
    url = 'wss' + url.slice(5);
  } else {
    url = 'ws' + url.slice(4);
  }
  this.url = url;

  this.ws = new WebsocketDriver(this.url, [], options);
  this.ws.onmessage = function(e) {
    debug('message event', e.data);
    self.emit('message', e.data);
  };
  // Firefox has an interesting bug. If a websocket connection is
  // created after onunload, it stays alive even when user
  // navigates away from the page. In such situation let's lie -
  // let's not open the ws connection at all. See:
  // https://github.com/sockjs/sockjs-client/issues/28
  // https://bugzilla.mozilla.org/show_bug.cgi?id=696085
  this.unloadRef = utils.unloadAdd(function() {
    debug('unload');
    self.ws.close();
  });
  this.ws.onclose = function(e) {
    debug('close event', e.code, e.reason);
    self.emit('close', e.code, e.reason);
    self._cleanup();
  };
  this.ws.onerror = function(e) {
    debug('error event', e);
    self.emit('close', 1006, 'WebSocket connection broken');
    self._cleanup();
  };
}

inherits(WebSocketTransport, EventEmitter);

WebSocketTransport.prototype.send = function(data) {
  var msg = '[' + data + ']';
  debug('send', msg);
  this.ws.send(msg);
};

WebSocketTransport.prototype.close = function() {
  debug('close');
  var ws = this.ws;
  this._cleanup();
  if (ws) {
    ws.close();
  }
};

WebSocketTransport.prototype._cleanup = function() {
  debug('_cleanup');
  var ws = this.ws;
  if (ws) {
    ws.onmessage = ws.onclose = ws.onerror = null;
  }
  utils.unloadDel(this.unloadRef);
  this.unloadRef = this.ws = null;
  this.removeAllListeners();
};

WebSocketTransport.enabled = function() {
  debug('enabled');
  return !!WebsocketDriver;
};
WebSocketTransport.transportName = 'websocket';

// In theory, ws should require 1 round trip. But in chrome, this is
// not very stable over SSL. Most likely a ws connection requires a
// separate SSL connection, in which case 2 round trips are an
// absolute minumum.
WebSocketTransport.roundTrips = 2;

module.exports = WebSocketTransport;

}).call(this,{ env: {} })

},{"../utils/event":46,"../utils/url":52,"./driver/websocket":19,"debug":55,"events":3,"inherits":57}],39:[function(require,module,exports){
'use strict';

var inherits = require('inherits')
  , AjaxBasedTransport = require('./lib/ajax-based')
  , XdrStreamingTransport = require('./xdr-streaming')
  , XhrReceiver = require('./receiver/xhr')
  , XDRObject = require('./sender/xdr')
  ;

function XdrPollingTransport(transUrl) {
  if (!XDRObject.enabled) {
    throw new Error('Transport created when disabled');
  }
  AjaxBasedTransport.call(this, transUrl, '/xhr', XhrReceiver, XDRObject);
}

inherits(XdrPollingTransport, AjaxBasedTransport);

XdrPollingTransport.enabled = XdrStreamingTransport.enabled;
XdrPollingTransport.transportName = 'xdr-polling';
XdrPollingTransport.roundTrips = 2; // preflight, ajax

module.exports = XdrPollingTransport;

},{"./lib/ajax-based":24,"./receiver/xhr":32,"./sender/xdr":34,"./xdr-streaming":40,"inherits":57}],40:[function(require,module,exports){
'use strict';

var inherits = require('inherits')
  , AjaxBasedTransport = require('./lib/ajax-based')
  , XhrReceiver = require('./receiver/xhr')
  , XDRObject = require('./sender/xdr')
  ;

// According to:
//   http://stackoverflow.com/questions/1641507/detect-browser-support-for-cross-domain-xmlhttprequests
//   http://hacks.mozilla.org/2009/07/cross-site-xmlhttprequest-with-cors/

function XdrStreamingTransport(transUrl) {
  if (!XDRObject.enabled) {
    throw new Error('Transport created when disabled');
  }
  AjaxBasedTransport.call(this, transUrl, '/xhr_streaming', XhrReceiver, XDRObject);
}

inherits(XdrStreamingTransport, AjaxBasedTransport);

XdrStreamingTransport.enabled = function(info) {
  if (info.cookie_needed || info.nullOrigin) {
    return false;
  }
  return XDRObject.enabled && info.sameScheme;
};

XdrStreamingTransport.transportName = 'xdr-streaming';
XdrStreamingTransport.roundTrips = 2; // preflight, ajax

module.exports = XdrStreamingTransport;

},{"./lib/ajax-based":24,"./receiver/xhr":32,"./sender/xdr":34,"inherits":57}],41:[function(require,module,exports){
'use strict';

var inherits = require('inherits')
  , AjaxBasedTransport = require('./lib/ajax-based')
  , XhrReceiver = require('./receiver/xhr')
  , XHRCorsObject = require('./sender/xhr-cors')
  , XHRLocalObject = require('./sender/xhr-local')
  ;

function XhrPollingTransport(transUrl) {
  if (!XHRLocalObject.enabled && !XHRCorsObject.enabled) {
    throw new Error('Transport created when disabled');
  }
  AjaxBasedTransport.call(this, transUrl, '/xhr', XhrReceiver, XHRCorsObject);
}

inherits(XhrPollingTransport, AjaxBasedTransport);

XhrPollingTransport.enabled = function(info) {
  if (info.nullOrigin) {
    return false;
  }

  if (XHRLocalObject.enabled && info.sameOrigin) {
    return true;
  }
  return XHRCorsObject.enabled;
};

XhrPollingTransport.transportName = 'xhr-polling';
XhrPollingTransport.roundTrips = 2; // preflight, ajax

module.exports = XhrPollingTransport;

},{"./lib/ajax-based":24,"./receiver/xhr":32,"./sender/xhr-cors":35,"./sender/xhr-local":37,"inherits":57}],42:[function(require,module,exports){
(function (global){
'use strict';

var inherits = require('inherits')
  , AjaxBasedTransport = require('./lib/ajax-based')
  , XhrReceiver = require('./receiver/xhr')
  , XHRCorsObject = require('./sender/xhr-cors')
  , XHRLocalObject = require('./sender/xhr-local')
  , browser = require('../utils/browser')
  ;

function XhrStreamingTransport(transUrl) {
  if (!XHRLocalObject.enabled && !XHRCorsObject.enabled) {
    throw new Error('Transport created when disabled');
  }
  AjaxBasedTransport.call(this, transUrl, '/xhr_streaming', XhrReceiver, XHRCorsObject);
}

inherits(XhrStreamingTransport, AjaxBasedTransport);

XhrStreamingTransport.enabled = function(info) {
  if (info.nullOrigin) {
    return false;
  }
  // Opera doesn't support xhr-streaming #60
  // But it might be able to #92
  if (browser.isOpera()) {
    return false;
  }

  return XHRCorsObject.enabled;
};

XhrStreamingTransport.transportName = 'xhr-streaming';
XhrStreamingTransport.roundTrips = 2; // preflight, ajax

// Safari gets confused when a streaming ajax request is started
// before onload. This causes the load indicator to spin indefinetely.
// Only require body when used in a browser
XhrStreamingTransport.needBody = !!global.document;

module.exports = XhrStreamingTransport;

}).call(this,typeof global !== "undefined" ? global : typeof self !== "undefined" ? self : typeof window !== "undefined" ? window : {})

},{"../utils/browser":44,"./lib/ajax-based":24,"./receiver/xhr":32,"./sender/xhr-cors":35,"./sender/xhr-local":37,"inherits":57}],43:[function(require,module,exports){
(function (global){
'use strict';

if (global.crypto && global.crypto.getRandomValues) {
  module.exports.randomBytes = function(length) {
    var bytes = new Uint8Array(length);
    global.crypto.getRandomValues(bytes);
    return bytes;
  };
} else {
  module.exports.randomBytes = function(length) {
    var bytes = new Array(length);
    for (var i = 0; i < length; i++) {
      bytes[i] = Math.floor(Math.random() * 256);
    }
    return bytes;
  };
}

}).call(this,typeof global !== "undefined" ? global : typeof self !== "undefined" ? self : typeof window !== "undefined" ? window : {})

},{}],44:[function(require,module,exports){
(function (global){
'use strict';

module.exports = {
  isOpera: function() {
    return global.navigator &&
      /opera/i.test(global.navigator.userAgent);
  }

, isKonqueror: function() {
    return global.navigator &&
      /konqueror/i.test(global.navigator.userAgent);
  }

  // #187 wrap document.domain in try/catch because of WP8 from file:///
, hasDomain: function () {
    // non-browser client always has a domain
    if (!global.document) {
      return true;
    }

    try {
      return !!global.document.domain;
    } catch (e) {
      return false;
    }
  }
};

}).call(this,typeof global !== "undefined" ? global : typeof self !== "undefined" ? self : typeof window !== "undefined" ? window : {})

},{}],45:[function(require,module,exports){
'use strict';

var JSON3 = require('json3');

// Some extra characters that Chrome gets wrong, and substitutes with
// something else on the wire.
// eslint-disable-next-line no-control-regex
var extraEscapable = /[\x00-\x1f\ud800-\udfff\ufffe\uffff\u0300-\u0333\u033d-\u0346\u034a-\u034c\u0350-\u0352\u0357-\u0358\u035c-\u0362\u0374\u037e\u0387\u0591-\u05af\u05c4\u0610-\u0617\u0653-\u0654\u0657-\u065b\u065d-\u065e\u06df-\u06e2\u06eb-\u06ec\u0730\u0732-\u0733\u0735-\u0736\u073a\u073d\u073f-\u0741\u0743\u0745\u0747\u07eb-\u07f1\u0951\u0958-\u095f\u09dc-\u09dd\u09df\u0a33\u0a36\u0a59-\u0a5b\u0a5e\u0b5c-\u0b5d\u0e38-\u0e39\u0f43\u0f4d\u0f52\u0f57\u0f5c\u0f69\u0f72-\u0f76\u0f78\u0f80-\u0f83\u0f93\u0f9d\u0fa2\u0fa7\u0fac\u0fb9\u1939-\u193a\u1a17\u1b6b\u1cda-\u1cdb\u1dc0-\u1dcf\u1dfc\u1dfe\u1f71\u1f73\u1f75\u1f77\u1f79\u1f7b\u1f7d\u1fbb\u1fbe\u1fc9\u1fcb\u1fd3\u1fdb\u1fe3\u1feb\u1fee-\u1fef\u1ff9\u1ffb\u1ffd\u2000-\u2001\u20d0-\u20d1\u20d4-\u20d7\u20e7-\u20e9\u2126\u212a-\u212b\u2329-\u232a\u2adc\u302b-\u302c\uaab2-\uaab3\uf900-\ufa0d\ufa10\ufa12\ufa15-\ufa1e\ufa20\ufa22\ufa25-\ufa26\ufa2a-\ufa2d\ufa30-\ufa6d\ufa70-\ufad9\ufb1d\ufb1f\ufb2a-\ufb36\ufb38-\ufb3c\ufb3e\ufb40-\ufb41\ufb43-\ufb44\ufb46-\ufb4e\ufff0-\uffff]/g
  , extraLookup;

// This may be quite slow, so let's delay until user actually uses bad
// characters.
var unrollLookup = function(escapable) {
  var i;
  var unrolled = {};
  var c = [];
  for (i = 0; i < 65536; i++) {
    c.push( String.fromCharCode(i) );
  }
  escapable.lastIndex = 0;
  c.join('').replace(escapable, function(a) {
    unrolled[ a ] = '\\u' + ('0000' + a.charCodeAt(0).toString(16)).slice(-4);
    return '';
  });
  escapable.lastIndex = 0;
  return unrolled;
};

// Quote string, also taking care of unicode characters that browsers
// often break. Especially, take care of unicode surrogates:
// http://en.wikipedia.org/wiki/Mapping_of_Unicode_characters#Surrogates
module.exports = {
  quote: function(string) {
    var quoted = JSON3.stringify(string);

    // In most cases this should be very fast and good enough.
    extraEscapable.lastIndex = 0;
    if (!extraEscapable.test(quoted)) {
      return quoted;
    }

    if (!extraLookup) {
      extraLookup = unrollLookup(extraEscapable);
    }

    return quoted.replace(extraEscapable, function(a) {
      return extraLookup[a];
    });
  }
};

},{"json3":58}],46:[function(require,module,exports){
(function (global){
'use strict';

var random = require('./random');

var onUnload = {}
  , afterUnload = false
    // detect google chrome packaged apps because they don't allow the 'unload' event
  , isChromePackagedApp = global.chrome && global.chrome.app && global.chrome.app.runtime
  ;

module.exports = {
  attachEvent: function(event, listener) {
    if (typeof global.addEventListener !== 'undefined') {
      global.addEventListener(event, listener, false);
    } else if (global.document && global.attachEvent) {
      // IE quirks.
      // According to: http://stevesouders.com/misc/test-postmessage.php
      // the message gets delivered only to 'document', not 'window'.
      global.document.attachEvent('on' + event, listener);
      // I get 'window' for ie8.
      global.attachEvent('on' + event, listener);
    }
  }

, detachEvent: function(event, listener) {
    if (typeof global.addEventListener !== 'undefined') {
      global.removeEventListener(event, listener, false);
    } else if (global.document && global.detachEvent) {
      global.document.detachEvent('on' + event, listener);
      global.detachEvent('on' + event, listener);
    }
  }

, unloadAdd: function(listener) {
    if (isChromePackagedApp) {
      return null;
    }

    var ref = random.string(8);
    onUnload[ref] = listener;
    if (afterUnload) {
      setTimeout(this.triggerUnloadCallbacks, 0);
    }
    return ref;
  }

, unloadDel: function(ref) {
    if (ref in onUnload) {
      delete onUnload[ref];
    }
  }

, triggerUnloadCallbacks: function() {
    for (var ref in onUnload) {
      onUnload[ref]();
      delete onUnload[ref];
    }
  }
};

var unloadTriggered = function() {
  if (afterUnload) {
    return;
  }
  afterUnload = true;
  module.exports.triggerUnloadCallbacks();
};

// 'unload' alone is not reliable in opera within an iframe, but we
// can't use `beforeunload` as IE fires it on javascript: links.
if (!isChromePackagedApp) {
  module.exports.attachEvent('unload', unloadTriggered);
}

}).call(this,typeof global !== "undefined" ? global : typeof self !== "undefined" ? self : typeof window !== "undefined" ? window : {})

},{"./random":50}],47:[function(require,module,exports){
(function (process,global){
'use strict';

var eventUtils = require('./event')
  , JSON3 = require('json3')
  , browser = require('./browser')
  ;

var debug = function() {};
if (process.env.NODE_ENV !== 'production') {
  debug = require('debug')('sockjs-client:utils:iframe');
}

module.exports = {
  WPrefix: '_jp'
, currentWindowId: null

, polluteGlobalNamespace: function() {
    if (!(module.exports.WPrefix in global)) {
      global[module.exports.WPrefix] = {};
    }
  }

, postMessage: function(type, data) {
    if (global.parent !== global) {
      global.parent.postMessage(JSON3.stringify({
        windowId: module.exports.currentWindowId
      , type: type
      , data: data || ''
      }), '*');
    } else {
      debug('Cannot postMessage, no parent window.', type, data);
    }
  }

, createIframe: function(iframeUrl, errorCallback) {
    var iframe = global.document.createElement('iframe');
    var tref, unloadRef;
    var unattach = function() {
      debug('unattach');
      clearTimeout(tref);
      // Explorer had problems with that.
      try {
        iframe.onload = null;
      } catch (x) {
        // intentionally empty
      }
      iframe.onerror = null;
    };
    var cleanup = function() {
      debug('cleanup');
      if (iframe) {
        unattach();
        // This timeout makes chrome fire onbeforeunload event
        // within iframe. Without the timeout it goes straight to
        // onunload.
        setTimeout(function() {
          if (iframe) {
            iframe.parentNode.removeChild(iframe);
          }
          iframe = null;
        }, 0);
        eventUtils.unloadDel(unloadRef);
      }
    };
    var onerror = function(err) {
      debug('onerror', err);
      if (iframe) {
        cleanup();
        errorCallback(err);
      }
    };
    var post = function(msg, origin) {
      debug('post', msg, origin);
      try {
        // When the iframe is not loaded, IE raises an exception
        // on 'contentWindow'.
        setTimeout(function() {
          if (iframe && iframe.contentWindow) {
            iframe.contentWindow.postMessage(msg, origin);
          }
        }, 0);
      } catch (x) {
        // intentionally empty
      }
    };

    iframe.src = iframeUrl;
    iframe.style.display = 'none';
    iframe.style.position = 'absolute';
    iframe.onerror = function() {
      onerror('onerror');
    };
    iframe.onload = function() {
      debug('onload');
      // `onload` is triggered before scripts on the iframe are
      // executed. Give it few seconds to actually load stuff.
      clearTimeout(tref);
      tref = setTimeout(function() {
        onerror('onload timeout');
      }, 2000);
    };
    global.document.body.appendChild(iframe);
    tref = setTimeout(function() {
      onerror('timeout');
    }, 15000);
    unloadRef = eventUtils.unloadAdd(cleanup);
    return {
      post: post
    , cleanup: cleanup
    , loaded: unattach
    };
  }

/* eslint no-undef: "off", new-cap: "off" */
, createHtmlfile: function(iframeUrl, errorCallback) {
    var axo = ['Active'].concat('Object').join('X');
    var doc = new global[axo]('htmlfile');
    var tref, unloadRef;
    var iframe;
    var unattach = function() {
      clearTimeout(tref);
      iframe.onerror = null;
    };
    var cleanup = function() {
      if (doc) {
        unattach();
        eventUtils.unloadDel(unloadRef);
        iframe.parentNode.removeChild(iframe);
        iframe = doc = null;
        CollectGarbage();
      }
    };
    var onerror = function(r) {
      debug('onerror', r);
      if (doc) {
        cleanup();
        errorCallback(r);
      }
    };
    var post = function(msg, origin) {
      try {
        // When the iframe is not loaded, IE raises an exception
        // on 'contentWindow'.
        setTimeout(function() {
          if (iframe && iframe.contentWindow) {
              iframe.contentWindow.postMessage(msg, origin);
          }
        }, 0);
      } catch (x) {
        // intentionally empty
      }
    };

    doc.open();
    doc.write('<html><s' + 'cript>' +
              'document.domain="' + global.document.domain + '";' +
              '</s' + 'cript></html>');
    doc.close();
    doc.parentWindow[module.exports.WPrefix] = global[module.exports.WPrefix];
    var c = doc.createElement('div');
    doc.body.appendChild(c);
    iframe = doc.createElement('iframe');
    c.appendChild(iframe);
    iframe.src = iframeUrl;
    iframe.onerror = function() {
      onerror('onerror');
    };
    tref = setTimeout(function() {
      onerror('timeout');
    }, 15000);
    unloadRef = eventUtils.unloadAdd(cleanup);
    return {
      post: post
    , cleanup: cleanup
    , loaded: unattach
    };
  }
};

module.exports.iframeEnabled = false;
if (global.document) {
  // postMessage misbehaves in konqueror 4.6.5 - the messages are delivered with
  // huge delay, or not at all.
  module.exports.iframeEnabled = (typeof global.postMessage === 'function' ||
    typeof global.postMessage === 'object') && (!browser.isKonqueror());
}

}).call(this,{ env: {} },typeof global !== "undefined" ? global : typeof self !== "undefined" ? self : typeof window !== "undefined" ? window : {})

},{"./browser":44,"./event":46,"debug":55,"json3":58}],48:[function(require,module,exports){
(function (global){
'use strict';

var logObject = {};
['log', 'debug', 'warn'].forEach(function (level) {
  var levelExists;

  try {
    levelExists = global.console && global.console[level] && global.console[level].apply;
  } catch(e) {
    // do nothing
  }

  logObject[level] = levelExists ? function () {
    return global.console[level].apply(global.console, arguments);
  } : (level === 'log' ? function () {} : logObject.log);
});

module.exports = logObject;

}).call(this,typeof global !== "undefined" ? global : typeof self !== "undefined" ? self : typeof window !== "undefined" ? window : {})

},{}],49:[function(require,module,exports){
'use strict';

module.exports = {
  isObject: function(obj) {
    var type = typeof obj;
    return type === 'function' || type === 'object' && !!obj;
  }

, extend: function(obj) {
    if (!this.isObject(obj)) {
      return obj;
    }
    var source, prop;
    for (var i = 1, length = arguments.length; i < length; i++) {
      source = arguments[i];
      for (prop in source) {
        if (Object.prototype.hasOwnProperty.call(source, prop)) {
          obj[prop] = source[prop];
        }
      }
    }
    return obj;
  }
};

},{}],50:[function(require,module,exports){
'use strict';

/* global crypto:true */
var crypto = require('crypto');

// This string has length 32, a power of 2, so the modulus doesn't introduce a
// bias.
var _randomStringChars = 'abcdefghijklmnopqrstuvwxyz012345';
module.exports = {
  string: function(length) {
    var max = _randomStringChars.length;
    var bytes = crypto.randomBytes(length);
    var ret = [];
    for (var i = 0; i < length; i++) {
      ret.push(_randomStringChars.substr(bytes[i] % max, 1));
    }
    return ret.join('');
  }

, number: function(max) {
    return Math.floor(Math.random() * max);
  }

, numberString: function(max) {
    var t = ('' + (max - 1)).length;
    var p = new Array(t + 1).join('0');
    return (p + this.number(max)).slice(-t);
  }
};

},{"crypto":43}],51:[function(require,module,exports){
(function (process){
'use strict';

var debug = function() {};
if (process.env.NODE_ENV !== 'production') {
  debug = require('debug')('sockjs-client:utils:transport');
}

module.exports = function(availableTransports) {
  return {
    filterToEnabled: function(transportsWhitelist, info) {
      var transports = {
        main: []
      , facade: []
      };
      if (!transportsWhitelist) {
        transportsWhitelist = [];
      } else if (typeof transportsWhitelist === 'string') {
        transportsWhitelist = [transportsWhitelist];
      }

      availableTransports.forEach(function(trans) {
        if (!trans) {
          return;
        }

        if (trans.transportName === 'websocket' && info.websocket === false) {
          debug('disabled from server', 'websocket');
          return;
        }

        if (transportsWhitelist.length &&
            transportsWhitelist.indexOf(trans.transportName) === -1) {
          debug('not in whitelist', trans.transportName);
          return;
        }

        if (trans.enabled(info)) {
          debug('enabled', trans.transportName);
          transports.main.push(trans);
          if (trans.facadeTransport) {
            transports.facade.push(trans.facadeTransport);
          }
        } else {
          debug('disabled', trans.transportName);
        }
      });
      return transports;
    }
  };
};

}).call(this,{ env: {} })

},{"debug":55}],52:[function(require,module,exports){
(function (process){
'use strict';

var URL = require('url-parse');

var debug = function() {};
if (process.env.NODE_ENV !== 'production') {
  debug = require('debug')('sockjs-client:utils:url');
}

module.exports = {
  getOrigin: function(url) {
    if (!url) {
      return null;
    }

    var p = new URL(url);
    if (p.protocol === 'file:') {
      return null;
    }

    var port = p.port;
    if (!port) {
      port = (p.protocol === 'https:') ? '443' : '80';
    }

    return p.protocol + '//' + p.hostname + ':' + port;
  }

, isOriginEqual: function(a, b) {
    var res = this.getOrigin(a) === this.getOrigin(b);
    debug('same', a, b, res);
    return res;
  }

, isSchemeEqual: function(a, b) {
    return (a.split(':')[0] === b.split(':')[0]);
  }

, addPath: function (url, path) {
    var qs = url.split('?');
    return qs[0] + path + (qs[1] ? '?' + qs[1] : '');
  }

, addQuery: function (url, q) {
    return url + (url.indexOf('?') === -1 ? ('?' + q) : ('&' + q));
  }
};

}).call(this,{ env: {} })

},{"debug":55,"url-parse":61}],53:[function(require,module,exports){
module.exports = '1.1.4';

},{}],54:[function(require,module,exports){
/**
 * Helpers.
 */

var s = 1000
var m = s * 60
var h = m * 60
var d = h * 24
var y = d * 365.25

/**
 * Parse or format the given `val`.
 *
 * Options:
 *
 *  - `long` verbose formatting [false]
 *
 * @param {String|Number} val
 * @param {Object} [options]
 * @throws {Error} throw an error if val is not a non-empty string or a number
 * @return {String|Number}
 * @api public
 */

module.exports = function (val, options) {
  options = options || {}
  var type = typeof val
  if (type === 'string' && val.length > 0) {
    return parse(val)
  } else if (type === 'number' && isNaN(val) === false) {
    return options.long ?
			fmtLong(val) :
			fmtShort(val)
  }
  throw new Error('val is not a non-empty string or a valid number. val=' + JSON.stringify(val))
}

/**
 * Parse the given `str` and return milliseconds.
 *
 * @param {String} str
 * @return {Number}
 * @api private
 */

function parse(str) {
  str = String(str)
  if (str.length > 10000) {
    return
  }
  var match = /^((?:\d+)?\.?\d+) *(milliseconds?|msecs?|ms|seconds?|secs?|s|minutes?|mins?|m|hours?|hrs?|h|days?|d|years?|yrs?|y)?$/i.exec(str)
  if (!match) {
    return
  }
  var n = parseFloat(match[1])
  var type = (match[2] || 'ms').toLowerCase()
  switch (type) {
    case 'years':
    case 'year':
    case 'yrs':
    case 'yr':
    case 'y':
      return n * y
    case 'days':
    case 'day':
    case 'd':
      return n * d
    case 'hours':
    case 'hour':
    case 'hrs':
    case 'hr':
    case 'h':
      return n * h
    case 'minutes':
    case 'minute':
    case 'mins':
    case 'min':
    case 'm':
      return n * m
    case 'seconds':
    case 'second':
    case 'secs':
    case 'sec':
    case 's':
      return n * s
    case 'milliseconds':
    case 'millisecond':
    case 'msecs':
    case 'msec':
    case 'ms':
      return n
    default:
      return undefined
  }
}

/**
 * Short format for `ms`.
 *
 * @param {Number} ms
 * @return {String}
 * @api private
 */

function fmtShort(ms) {
  if (ms >= d) {
    return Math.round(ms / d) + 'd'
  }
  if (ms >= h) {
    return Math.round(ms / h) + 'h'
  }
  if (ms >= m) {
    return Math.round(ms / m) + 'm'
  }
  if (ms >= s) {
    return Math.round(ms / s) + 's'
  }
  return ms + 'ms'
}

/**
 * Long format for `ms`.
 *
 * @param {Number} ms
 * @return {String}
 * @api private
 */

function fmtLong(ms) {
  return plural(ms, d, 'day') ||
    plural(ms, h, 'hour') ||
    plural(ms, m, 'minute') ||
    plural(ms, s, 'second') ||
    ms + ' ms'
}

/**
 * Pluralization helper.
 */

function plural(ms, n, name) {
  if (ms < n) {
    return
  }
  if (ms < n * 1.5) {
    return Math.floor(ms / n) + ' ' + name
  }
  return Math.ceil(ms / n) + ' ' + name + 's'
}

},{}],55:[function(require,module,exports){
(function (process){
/**
 * This is the web browser implementation of `debug()`.
 *
 * Expose `debug()` as the module.
 */

exports = module.exports = require('./debug');
exports.log = log;
exports.formatArgs = formatArgs;
exports.save = save;
exports.load = load;
exports.useColors = useColors;
exports.storage = 'undefined' != typeof chrome
               && 'undefined' != typeof chrome.storage
                  ? chrome.storage.local
                  : localstorage();

/**
 * Colors.
 */

exports.colors = [
  'lightseagreen',
  'forestgreen',
  'goldenrod',
  'dodgerblue',
  'darkorchid',
  'crimson'
];

/**
 * Currently only WebKit-based Web Inspectors, Firefox >= v31,
 * and the Firebug extension (any Firefox version) are known
 * to support "%c" CSS customizations.
 *
 * TODO: add a `localStorage` variable to explicitly enable/disable colors
 */

function useColors() {
  // NB: In an Electron preload script, document will be defined but not fully
  // initialized. Since we know we're in Chrome, we'll just detect this case
  // explicitly
  if (typeof window !== 'undefined' && window.process && window.process.type === 'renderer') {
    return true;
  }

  // is webkit? http://stackoverflow.com/a/16459606/376773
  // document is undefined in react-native: https://github.com/facebook/react-native/pull/1632
  return (typeof document !== 'undefined' && document && document.documentElement && document.documentElement.style && document.documentElement.style.WebkitAppearance) ||
    // is firebug? http://stackoverflow.com/a/398120/376773
    (typeof window !== 'undefined' && window && window.console && (window.console.firebug || (window.console.exception && window.console.table))) ||
    // is firefox >= v31?
    // https://developer.mozilla.org/en-US/docs/Tools/Web_Console#Styling_messages
    (typeof navigator !== 'undefined' && navigator && navigator.userAgent && navigator.userAgent.toLowerCase().match(/firefox\/(\d+)/) && parseInt(RegExp.$1, 10) >= 31) ||
    // double check webkit in userAgent just in case we are in a worker
    (typeof navigator !== 'undefined' && navigator && navigator.userAgent && navigator.userAgent.toLowerCase().match(/applewebkit\/(\d+)/));
}

/**
 * Map %j to `JSON.stringify()`, since no Web Inspectors do that by default.
 */

exports.formatters.j = function(v) {
  try {
    return JSON.stringify(v);
  } catch (err) {
    return '[UnexpectedJSONParseError]: ' + err.message;
  }
};


/**
 * Colorize log arguments if enabled.
 *
 * @api public
 */

function formatArgs(args) {
  var useColors = this.useColors;

  args[0] = (useColors ? '%c' : '')
    + this.namespace
    + (useColors ? ' %c' : ' ')
    + args[0]
    + (useColors ? '%c ' : ' ')
    + '+' + exports.humanize(this.diff);

  if (!useColors) return;

  var c = 'color: ' + this.color;
  args.splice(1, 0, c, 'color: inherit')

  // the final "%c" is somewhat tricky, because there could be other
  // arguments passed either before or after the %c, so we need to
  // figure out the correct index to insert the CSS into
  var index = 0;
  var lastC = 0;
  args[0].replace(/%[a-zA-Z%]/g, function(match) {
    if ('%%' === match) return;
    index++;
    if ('%c' === match) {
      // we only are interested in the *last* %c
      // (the user may have provided their own)
      lastC = index;
    }
  });

  args.splice(lastC, 0, c);
}

/**
 * Invokes `console.log()` when available.
 * No-op when `console.log` is not a "function".
 *
 * @api public
 */

function log() {
  // this hackery is required for IE8/9, where
  // the `console.log` function doesn't have 'apply'
  return 'object' === typeof console
    && console.log
    && Function.prototype.apply.call(console.log, console, arguments);
}

/**
 * Save `namespaces`.
 *
 * @param {String} namespaces
 * @api private
 */

function save(namespaces) {
  try {
    if (null == namespaces) {
      exports.storage.removeItem('debug');
    } else {
      exports.storage.debug = namespaces;
    }
  } catch(e) {}
}

/**
 * Load `namespaces`.
 *
 * @return {String} returns the previously persisted debug modes
 * @api private
 */

function load() {
  var r;
  try {
    r = exports.storage.debug;
  } catch(e) {}

  // If debug isn't set in LS, and we're in Electron, try to load $DEBUG
  if (!r && typeof process !== 'undefined' && 'env' in process) {
    r = process.env.DEBUG;
  }

  return r;
}

/**
 * Enable namespaces listed in `localStorage.debug` initially.
 */

exports.enable(load());

/**
 * Localstorage attempts to return the localstorage.
 *
 * This is necessary because safari throws
 * when a user disables cookies/localstorage
 * and you attempt to access it.
 *
 * @return {LocalStorage}
 * @api private
 */

function localstorage() {
  try {
    return window.localStorage;
  } catch (e) {}
}

}).call(this,{ env: {} })

},{"./debug":56}],56:[function(require,module,exports){

/**
 * This is the common logic for both the Node.js and web browser
 * implementations of `debug()`.
 *
 * Expose `debug()` as the module.
 */

exports = module.exports = createDebug.debug = createDebug['default'] = createDebug;
exports.coerce = coerce;
exports.disable = disable;
exports.enable = enable;
exports.enabled = enabled;
exports.humanize = require('ms');

/**
 * The currently active debug mode names, and names to skip.
 */

exports.names = [];
exports.skips = [];

/**
 * Map of special "%n" handling functions, for the debug "format" argument.
 *
 * Valid key names are a single, lower or upper-case letter, i.e. "n" and "N".
 */

exports.formatters = {};

/**
 * Previous log timestamp.
 */

var prevTime;

/**
 * Select a color.
 * @param {String} namespace
 * @return {Number}
 * @api private
 */

function selectColor(namespace) {
  var hash = 0, i;

  for (i in namespace) {
    hash  = ((hash << 5) - hash) + namespace.charCodeAt(i);
    hash |= 0; // Convert to 32bit integer
  }

  return exports.colors[Math.abs(hash) % exports.colors.length];
}

/**
 * Create a debugger with the given `namespace`.
 *
 * @param {String} namespace
 * @return {Function}
 * @api public
 */

function createDebug(namespace) {

  function debug() {
    // disabled?
    if (!debug.enabled) return;

    var self = debug;

    // set `diff` timestamp
    var curr = +new Date();
    var ms = curr - (prevTime || curr);
    self.diff = ms;
    self.prev = prevTime;
    self.curr = curr;
    prevTime = curr;

    // turn the `arguments` into a proper Array
    var args = new Array(arguments.length);
    for (var i = 0; i < args.length; i++) {
      args[i] = arguments[i];
    }

    args[0] = exports.coerce(args[0]);

    if ('string' !== typeof args[0]) {
      // anything else let's inspect with %O
      args.unshift('%O');
    }

    // apply any `formatters` transformations
    var index = 0;
    args[0] = args[0].replace(/%([a-zA-Z%])/g, function(match, format) {
      // if we encounter an escaped % then don't increase the array index
      if (match === '%%') return match;
      index++;
      var formatter = exports.formatters[format];
      if ('function' === typeof formatter) {
        var val = args[index];
        match = formatter.call(self, val);

        // now we need to remove `args[index]` since it's inlined in the `format`
        args.splice(index, 1);
        index--;
      }
      return match;
    });

    // apply env-specific formatting (colors, etc.)
    exports.formatArgs.call(self, args);

    var logFn = debug.log || exports.log || console.log.bind(console);
    logFn.apply(self, args);
  }

  debug.namespace = namespace;
  debug.enabled = exports.enabled(namespace);
  debug.useColors = exports.useColors();
  debug.color = selectColor(namespace);

  // env-specific initialization logic for debug instances
  if ('function' === typeof exports.init) {
    exports.init(debug);
  }

  return debug;
}

/**
 * Enables a debug mode by namespaces. This can include modes
 * separated by a colon and wildcards.
 *
 * @param {String} namespaces
 * @api public
 */

function enable(namespaces) {
  exports.save(namespaces);

  exports.names = [];
  exports.skips = [];

  var split = (typeof namespaces === 'string' ? namespaces : '').split(/[\s,]+/);
  var len = split.length;

  for (var i = 0; i < len; i++) {
    if (!split[i]) continue; // ignore empty strings
    namespaces = split[i].replace(/\*/g, '.*?');
    if (namespaces[0] === '-') {
      exports.skips.push(new RegExp('^' + namespaces.substr(1) + '$'));
    } else {
      exports.names.push(new RegExp('^' + namespaces + '$'));
    }
  }
}

/**
 * Disable debug output.
 *
 * @api public
 */

function disable() {
  exports.enable('');
}

/**
 * Returns true if the given mode name is enabled, false otherwise.
 *
 * @param {String} name
 * @return {Boolean}
 * @api public
 */

function enabled(name) {
  var i, len;
  for (i = 0, len = exports.skips.length; i < len; i++) {
    if (exports.skips[i].test(name)) {
      return false;
    }
  }
  for (i = 0, len = exports.names.length; i < len; i++) {
    if (exports.names[i].test(name)) {
      return true;
    }
  }
  return false;
}

/**
 * Coerce `val`.
 *
 * @param {Mixed} val
 * @return {Mixed}
 * @api private
 */

function coerce(val) {
  if (val instanceof Error) return val.stack || val.message;
  return val;
}

},{"ms":54}],57:[function(require,module,exports){
if (typeof Object.create === 'function') {
  // implementation from standard node.js 'util' module
  module.exports = function inherits(ctor, superCtor) {
    ctor.super_ = superCtor
    ctor.prototype = Object.create(superCtor.prototype, {
      constructor: {
        value: ctor,
        enumerable: false,
        writable: true,
        configurable: true
      }
    });
  };
} else {
  // old school shim for old browsers
  module.exports = function inherits(ctor, superCtor) {
    ctor.super_ = superCtor
    var TempCtor = function () {}
    TempCtor.prototype = superCtor.prototype
    ctor.prototype = new TempCtor()
    ctor.prototype.constructor = ctor
  }
}

},{}],58:[function(require,module,exports){
(function (global){
/*! JSON v3.3.2 | http://bestiejs.github.io/json3 | Copyright 2012-2014, Kit Cambridge | http://kit.mit-license.org */
;(function () {
  // Detect the `define` function exposed by asynchronous module loaders. The
  // strict `define` check is necessary for compatibility with `r.js`.
  var isLoader = typeof define === "function" && define.amd;

  // A set of types used to distinguish objects from primitives.
  var objectTypes = {
    "function": true,
    "object": true
  };

  // Detect the `exports` object exposed by CommonJS implementations.
  var freeExports = objectTypes[typeof exports] && exports && !exports.nodeType && exports;

  // Use the `global` object exposed by Node (including Browserify via
  // `insert-module-globals`), Narwhal, and Ringo as the default context,
  // and the `window` object in browsers. Rhino exports a `global` function
  // instead.
  var root = objectTypes[typeof window] && window || this,
      freeGlobal = freeExports && objectTypes[typeof module] && module && !module.nodeType && typeof global == "object" && global;

  if (freeGlobal && (freeGlobal["global"] === freeGlobal || freeGlobal["window"] === freeGlobal || freeGlobal["self"] === freeGlobal)) {
    root = freeGlobal;
  }

  // Public: Initializes JSON 3 using the given `context` object, attaching the
  // `stringify` and `parse` functions to the specified `exports` object.
  function runInContext(context, exports) {
    context || (context = root["Object"]());
    exports || (exports = root["Object"]());

    // Native constructor aliases.
    var Number = context["Number"] || root["Number"],
        String = context["String"] || root["String"],
        Object = context["Object"] || root["Object"],
        Date = context["Date"] || root["Date"],
        SyntaxError = context["SyntaxError"] || root["SyntaxError"],
        TypeError = context["TypeError"] || root["TypeError"],
        Math = context["Math"] || root["Math"],
        nativeJSON = context["JSON"] || root["JSON"];

    // Delegate to the native `stringify` and `parse` implementations.
    if (typeof nativeJSON == "object" && nativeJSON) {
      exports.stringify = nativeJSON.stringify;
      exports.parse = nativeJSON.parse;
    }

    // Convenience aliases.
    var objectProto = Object.prototype,
        getClass = objectProto.toString,
        isProperty, forEach, undef;

    // Test the `Date#getUTC*` methods. Based on work by @Yaffle.
    var isExtended = new Date(-3509827334573292);
    try {
      // The `getUTCFullYear`, `Month`, and `Date` methods return nonsensical
      // results for certain dates in Opera >= 10.53.
      isExtended = isExtended.getUTCFullYear() == -109252 && isExtended.getUTCMonth() === 0 && isExtended.getUTCDate() === 1 &&
        // Safari < 2.0.2 stores the internal millisecond time value correctly,
        // but clips the values returned by the date methods to the range of
        // signed 32-bit integers ([-2 ** 31, 2 ** 31 - 1]).
        isExtended.getUTCHours() == 10 && isExtended.getUTCMinutes() == 37 && isExtended.getUTCSeconds() == 6 && isExtended.getUTCMilliseconds() == 708;
    } catch (exception) {}

    // Internal: Determines whether the native `JSON.stringify` and `parse`
    // implementations are spec-compliant. Based on work by Ken Snyder.
    function has(name) {
      if (has[name] !== undef) {
        // Return cached feature test result.
        return has[name];
      }
      var isSupported;
      if (name == "bug-string-char-index") {
        // IE <= 7 doesn't support accessing string characters using square
        // bracket notation. IE 8 only supports this for primitives.
        isSupported = "a"[0] != "a";
      } else if (name == "json") {
        // Indicates whether both `JSON.stringify` and `JSON.parse` are
        // supported.
        isSupported = has("json-stringify") && has("json-parse");
      } else {
        var value, serialized = '{"a":[1,true,false,null,"\\u0000\\b\\n\\f\\r\\t"]}';
        // Test `JSON.stringify`.
        if (name == "json-stringify") {
          var stringify = exports.stringify, stringifySupported = typeof stringify == "function" && isExtended;
          if (stringifySupported) {
            // A test function object with a custom `toJSON` method.
            (value = function () {
              return 1;
            }).toJSON = value;
            try {
              stringifySupported =
                // Firefox 3.1b1 and b2 serialize string, number, and boolean
                // primitives as object literals.
                stringify(0) === "0" &&
                // FF 3.1b1, b2, and JSON 2 serialize wrapped primitives as object
                // literals.
                stringify(new Number()) === "0" &&
                stringify(new String()) == '""' &&
                // FF 3.1b1, 2 throw an error if the value is `null`, `undefined`, or
                // does not define a canonical JSON representation (this applies to
                // objects with `toJSON` properties as well, *unless* they are nested
                // within an object or array).
                stringify(getClass) === undef &&
                // IE 8 serializes `undefined` as `"undefined"`. Safari <= 5.1.7 and
                // FF 3.1b3 pass this test.
                stringify(undef) === undef &&
                // Safari <= 5.1.7 and FF 3.1b3 throw `Error`s and `TypeError`s,
                // respectively, if the value is omitted entirely.
                stringify() === undef &&
                // FF 3.1b1, 2 throw an error if the given value is not a number,
                // string, array, object, Boolean, or `null` literal. This applies to
                // objects with custom `toJSON` methods as well, unless they are nested
                // inside object or array literals. YUI 3.0.0b1 ignores custom `toJSON`
                // methods entirely.
                stringify(value) === "1" &&
                stringify([value]) == "[1]" &&
                // Prototype <= 1.6.1 serializes `[undefined]` as `"[]"` instead of
                // `"[null]"`.
                stringify([undef]) == "[null]" &&
                // YUI 3.0.0b1 fails to serialize `null` literals.
                stringify(null) == "null" &&
                // FF 3.1b1, 2 halts serialization if an array contains a function:
                // `[1, true, getClass, 1]` serializes as "[1,true,],". FF 3.1b3
                // elides non-JSON values from objects and arrays, unless they
                // define custom `toJSON` methods.
                stringify([undef, getClass, null]) == "[null,null,null]" &&
                // Simple serialization test. FF 3.1b1 uses Unicode escape sequences
                // where character escape codes are expected (e.g., `\b` => `\u0008`).
                stringify({ "a": [value, true, false, null, "\x00\b\n\f\r\t"] }) == serialized &&
                // FF 3.1b1 and b2 ignore the `filter` and `width` arguments.
                stringify(null, value) === "1" &&
                stringify([1, 2], null, 1) == "[\n 1,\n 2\n]" &&
                // JSON 2, Prototype <= 1.7, and older WebKit builds incorrectly
                // serialize extended years.
                stringify(new Date(-8.64e15)) == '"-271821-04-20T00:00:00.000Z"' &&
                // The milliseconds are optional in ES 5, but required in 5.1.
                stringify(new Date(8.64e15)) == '"+275760-09-13T00:00:00.000Z"' &&
                // Firefox <= 11.0 incorrectly serializes years prior to 0 as negative
                // four-digit years instead of six-digit years. Credits: @Yaffle.
                stringify(new Date(-621987552e5)) == '"-000001-01-01T00:00:00.000Z"' &&
                // Safari <= 5.1.5 and Opera >= 10.53 incorrectly serialize millisecond
                // values less than 1000. Credits: @Yaffle.
                stringify(new Date(-1)) == '"1969-12-31T23:59:59.999Z"';
            } catch (exception) {
              stringifySupported = false;
            }
          }
          isSupported = stringifySupported;
        }
        // Test `JSON.parse`.
        if (name == "json-parse") {
          var parse = exports.parse;
          if (typeof parse == "function") {
            try {
              // FF 3.1b1, b2 will throw an exception if a bare literal is provided.
              // Conforming implementations should also coerce the initial argument to
              // a string prior to parsing.
              if (parse("0") === 0 && !parse(false)) {
                // Simple parsing test.
                value = parse(serialized);
                var parseSupported = value["a"].length == 5 && value["a"][0] === 1;
                if (parseSupported) {
                  try {
                    // Safari <= 5.1.2 and FF 3.1b1 allow unescaped tabs in strings.
                    parseSupported = !parse('"\t"');
                  } catch (exception) {}
                  if (parseSupported) {
                    try {
                      // FF 4.0 and 4.0.1 allow leading `+` signs and leading
                      // decimal points. FF 4.0, 4.0.1, and IE 9-10 also allow
                      // certain octal literals.
                      parseSupported = parse("01") !== 1;
                    } catch (exception) {}
                  }
                  if (parseSupported) {
                    try {
                      // FF 4.0, 4.0.1, and Rhino 1.7R3-R4 allow trailing decimal
                      // points. These environments, along with FF 3.1b1 and 2,
                      // also allow trailing commas in JSON objects and arrays.
                      parseSupported = parse("1.") !== 1;
                    } catch (exception) {}
                  }
                }
              }
            } catch (exception) {
              parseSupported = false;
            }
          }
          isSupported = parseSupported;
        }
      }
      return has[name] = !!isSupported;
    }

    if (!has("json")) {
      // Common `[[Class]]` name aliases.
      var functionClass = "[object Function]",
          dateClass = "[object Date]",
          numberClass = "[object Number]",
          stringClass = "[object String]",
          arrayClass = "[object Array]",
          booleanClass = "[object Boolean]";

      // Detect incomplete support for accessing string characters by index.
      var charIndexBuggy = has("bug-string-char-index");

      // Define additional utility methods if the `Date` methods are buggy.
      if (!isExtended) {
        var floor = Math.floor;
        // A mapping between the months of the year and the number of days between
        // January 1st and the first of the respective month.
        var Months = [0, 31, 59, 90, 120, 151, 181, 212, 243, 273, 304, 334];
        // Internal: Calculates the number of days between the Unix epoch and the
        // first day of the given month.
        var getDay = function (year, month) {
          return Months[month] + 365 * (year - 1970) + floor((year - 1969 + (month = +(month > 1))) / 4) - floor((year - 1901 + month) / 100) + floor((year - 1601 + month) / 400);
        };
      }

      // Internal: Determines if a property is a direct property of the given
      // object. Delegates to the native `Object#hasOwnProperty` method.
      if (!(isProperty = objectProto.hasOwnProperty)) {
        isProperty = function (property) {
          var members = {}, constructor;
          if ((members.__proto__ = null, members.__proto__ = {
            // The *proto* property cannot be set multiple times in recent
            // versions of Firefox and SeaMonkey.
            "toString": 1
          }, members).toString != getClass) {
            // Safari <= 2.0.3 doesn't implement `Object#hasOwnProperty`, but
            // supports the mutable *proto* property.
            isProperty = function (property) {
              // Capture and break the object's prototype chain (see section 8.6.2
              // of the ES 5.1 spec). The parenthesized expression prevents an
              // unsafe transformation by the Closure Compiler.
              var original = this.__proto__, result = property in (this.__proto__ = null, this);
              // Restore the original prototype chain.
              this.__proto__ = original;
              return result;
            };
          } else {
            // Capture a reference to the top-level `Object` constructor.
            constructor = members.constructor;
            // Use the `constructor` property to simulate `Object#hasOwnProperty` in
            // other environments.
            isProperty = function (property) {
              var parent = (this.constructor || constructor).prototype;
              return property in this && !(property in parent && this[property] === parent[property]);
            };
          }
          members = null;
          return isProperty.call(this, property);
        };
      }

      // Internal: Normalizes the `for...in` iteration algorithm across
      // environments. Each enumerated key is yielded to a `callback` function.
      forEach = function (object, callback) {
        var size = 0, Properties, members, property;

        // Tests for bugs in the current environment's `for...in` algorithm. The
        // `valueOf` property inherits the non-enumerable flag from
        // `Object.prototype` in older versions of IE, Netscape, and Mozilla.
        (Properties = function () {
          this.valueOf = 0;
        }).prototype.valueOf = 0;

        // Iterate over a new instance of the `Properties` class.
        members = new Properties();
        for (property in members) {
          // Ignore all properties inherited from `Object.prototype`.
          if (isProperty.call(members, property)) {
            size++;
          }
        }
        Properties = members = null;

        // Normalize the iteration algorithm.
        if (!size) {
          // A list of non-enumerable properties inherited from `Object.prototype`.
          members = ["valueOf", "toString", "toLocaleString", "propertyIsEnumerable", "isPrototypeOf", "hasOwnProperty", "constructor"];
          // IE <= 8, Mozilla 1.0, and Netscape 6.2 ignore shadowed non-enumerable
          // properties.
          forEach = function (object, callback) {
            var isFunction = getClass.call(object) == functionClass, property, length;
            var hasProperty = !isFunction && typeof object.constructor != "function" && objectTypes[typeof object.hasOwnProperty] && object.hasOwnProperty || isProperty;
            for (property in object) {
              // Gecko <= 1.0 enumerates the `prototype` property of functions under
              // certain conditions; IE does not.
              if (!(isFunction && property == "prototype") && hasProperty.call(object, property)) {
                callback(property);
              }
            }
            // Manually invoke the callback for each non-enumerable property.
            for (length = members.length; property = members[--length]; hasProperty.call(object, property) && callback(property));
          };
        } else if (size == 2) {
          // Safari <= 2.0.4 enumerates shadowed properties twice.
          forEach = function (object, callback) {
            // Create a set of iterated properties.
            var members = {}, isFunction = getClass.call(object) == functionClass, property;
            for (property in object) {
              // Store each property name to prevent double enumeration. The
              // `prototype` property of functions is not enumerated due to cross-
              // environment inconsistencies.
              if (!(isFunction && property == "prototype") && !isProperty.call(members, property) && (members[property] = 1) && isProperty.call(object, property)) {
                callback(property);
              }
            }
          };
        } else {
          // No bugs detected; use the standard `for...in` algorithm.
          forEach = function (object, callback) {
            var isFunction = getClass.call(object) == functionClass, property, isConstructor;
            for (property in object) {
              if (!(isFunction && property == "prototype") && isProperty.call(object, property) && !(isConstructor = property === "constructor")) {
                callback(property);
              }
            }
            // Manually invoke the callback for the `constructor` property due to
            // cross-environment inconsistencies.
            if (isConstructor || isProperty.call(object, (property = "constructor"))) {
              callback(property);
            }
          };
        }
        return forEach(object, callback);
      };

      // Public: Serializes a JavaScript `value` as a JSON string. The optional
      // `filter` argument may specify either a function that alters how object and
      // array members are serialized, or an array of strings and numbers that
      // indicates which properties should be serialized. The optional `width`
      // argument may be either a string or number that specifies the indentation
      // level of the output.
      if (!has("json-stringify")) {
        // Internal: A map of control characters and their escaped equivalents.
        var Escapes = {
          92: "\\\\",
          34: '\\"',
          8: "\\b",
          12: "\\f",
          10: "\\n",
          13: "\\r",
          9: "\\t"
        };

        // Internal: Converts `value` into a zero-padded string such that its
        // length is at least equal to `width`. The `width` must be <= 6.
        var leadingZeroes = "000000";
        var toPaddedString = function (width, value) {
          // The `|| 0` expression is necessary to work around a bug in
          // Opera <= 7.54u2 where `0 == -0`, but `String(-0) !== "0"`.
          return (leadingZeroes + (value || 0)).slice(-width);
        };

        // Internal: Double-quotes a string `value`, replacing all ASCII control
        // characters (characters with code unit values between 0 and 31) with
        // their escaped equivalents. This is an implementation of the
        // `Quote(value)` operation defined in ES 5.1 section 15.12.3.
        var unicodePrefix = "\\u00";
        var quote = function (value) {
          var result = '"', index = 0, length = value.length, useCharIndex = !charIndexBuggy || length > 10;
          var symbols = useCharIndex && (charIndexBuggy ? value.split("") : value);
          for (; index < length; index++) {
            var charCode = value.charCodeAt(index);
            // If the character is a control character, append its Unicode or
            // shorthand escape sequence; otherwise, append the character as-is.
            switch (charCode) {
              case 8: case 9: case 10: case 12: case 13: case 34: case 92:
                result += Escapes[charCode];
                break;
              default:
                if (charCode < 32) {
                  result += unicodePrefix + toPaddedString(2, charCode.toString(16));
                  break;
                }
                result += useCharIndex ? symbols[index] : value.charAt(index);
            }
          }
          return result + '"';
        };

        // Internal: Recursively serializes an object. Implements the
        // `Str(key, holder)`, `JO(value)`, and `JA(value)` operations.
        var serialize = function (property, object, callback, properties, whitespace, indentation, stack) {
          var value, className, year, month, date, time, hours, minutes, seconds, milliseconds, results, element, index, length, prefix, result;
          try {
            // Necessary for host object support.
            value = object[property];
          } catch (exception) {}
          if (typeof value == "object" && value) {
            className = getClass.call(value);
            if (className == dateClass && !isProperty.call(value, "toJSON")) {
              if (value > -1 / 0 && value < 1 / 0) {
                // Dates are serialized according to the `Date#toJSON` method
                // specified in ES 5.1 section 15.9.5.44. See section 15.9.1.15
                // for the ISO 8601 date time string format.
                if (getDay) {
                  // Manually compute the year, month, date, hours, minutes,
                  // seconds, and milliseconds if the `getUTC*` methods are
                  // buggy. Adapted from @Yaffle's `date-shim` project.
                  date = floor(value / 864e5);
                  for (year = floor(date / 365.2425) + 1970 - 1; getDay(year + 1, 0) <= date; year++);
                  for (month = floor((date - getDay(year, 0)) / 30.42); getDay(year, month + 1) <= date; month++);
                  date = 1 + date - getDay(year, month);
                  // The `time` value specifies the time within the day (see ES
                  // 5.1 section 15.9.1.2). The formula `(A % B + B) % B` is used
                  // to compute `A modulo B`, as the `%` operator does not
                  // correspond to the `modulo` operation for negative numbers.
                  time = (value % 864e5 + 864e5) % 864e5;
                  // The hours, minutes, seconds, and milliseconds are obtained by
                  // decomposing the time within the day. See section 15.9.1.10.
                  hours = floor(time / 36e5) % 24;
                  minutes = floor(time / 6e4) % 60;
                  seconds = floor(time / 1e3) % 60;
                  milliseconds = time % 1e3;
                } else {
                  year = value.getUTCFullYear();
                  month = value.getUTCMonth();
                  date = value.getUTCDate();
                  hours = value.getUTCHours();
                  minutes = value.getUTCMinutes();
                  seconds = value.getUTCSeconds();
                  milliseconds = value.getUTCMilliseconds();
                }
                // Serialize extended years correctly.
                value = (year <= 0 || year >= 1e4 ? (year < 0 ? "-" : "+") + toPaddedString(6, year < 0 ? -year : year) : toPaddedString(4, year)) +
                  "-" + toPaddedString(2, month + 1) + "-" + toPaddedString(2, date) +
                  // Months, dates, hours, minutes, and seconds should have two
                  // digits; milliseconds should have three.
                  "T" + toPaddedString(2, hours) + ":" + toPaddedString(2, minutes) + ":" + toPaddedString(2, seconds) +
                  // Milliseconds are optional in ES 5.0, but required in 5.1.
                  "." + toPaddedString(3, milliseconds) + "Z";
              } else {
                value = null;
              }
            } else if (typeof value.toJSON == "function" && ((className != numberClass && className != stringClass && className != arrayClass) || isProperty.call(value, "toJSON"))) {
              // Prototype <= 1.6.1 adds non-standard `toJSON` methods to the
              // `Number`, `String`, `Date`, and `Array` prototypes. JSON 3
              // ignores all `toJSON` methods on these objects unless they are
              // defined directly on an instance.
              value = value.toJSON(property);
            }
          }
          if (callback) {
            // If a replacement function was provided, call it to obtain the value
            // for serialization.
            value = callback.call(object, property, value);
          }
          if (value === null) {
            return "null";
          }
          className = getClass.call(value);
          if (className == booleanClass) {
            // Booleans are represented literally.
            return "" + value;
          } else if (className == numberClass) {
            // JSON numbers must be finite. `Infinity` and `NaN` are serialized as
            // `"null"`.
            return value > -1 / 0 && value < 1 / 0 ? "" + value : "null";
          } else if (className == stringClass) {
            // Strings are double-quoted and escaped.
            return quote("" + value);
          }
          // Recursively serialize objects and arrays.
          if (typeof value == "object") {
            // Check for cyclic structures. This is a linear search; performance
            // is inversely proportional to the number of unique nested objects.
            for (length = stack.length; length--;) {
              if (stack[length] === value) {
                // Cyclic structures cannot be serialized by `JSON.stringify`.
                throw TypeError();
              }
            }
            // Add the object to the stack of traversed objects.
            stack.push(value);
            results = [];
            // Save the current indentation level and indent one additional level.
            prefix = indentation;
            indentation += whitespace;
            if (className == arrayClass) {
              // Recursively serialize array elements.
              for (index = 0, length = value.length; index < length; index++) {
                element = serialize(index, value, callback, properties, whitespace, indentation, stack);
                results.push(element === undef ? "null" : element);
              }
              result = results.length ? (whitespace ? "[\n" + indentation + results.join(",\n" + indentation) + "\n" + prefix + "]" : ("[" + results.join(",") + "]")) : "[]";
            } else {
              // Recursively serialize object members. Members are selected from
              // either a user-specified list of property names, or the object
              // itself.
              forEach(properties || value, function (property) {
                var element = serialize(property, value, callback, properties, whitespace, indentation, stack);
                if (element !== undef) {
                  // According to ES 5.1 section 15.12.3: "If `gap` {whitespace}
                  // is not the empty string, let `member` {quote(property) + ":"}
                  // be the concatenation of `member` and the `space` character."
                  // The "`space` character" refers to the literal space
                  // character, not the `space` {width} argument provided to
                  // `JSON.stringify`.
                  results.push(quote(property) + ":" + (whitespace ? " " : "") + element);
                }
              });
              result = results.length ? (whitespace ? "{\n" + indentation + results.join(",\n" + indentation) + "\n" + prefix + "}" : ("{" + results.join(",") + "}")) : "{}";
            }
            // Remove the object from the traversed object stack.
            stack.pop();
            return result;
          }
        };

        // Public: `JSON.stringify`. See ES 5.1 section 15.12.3.
        exports.stringify = function (source, filter, width) {
          var whitespace, callback, properties, className;
          if (objectTypes[typeof filter] && filter) {
            if ((className = getClass.call(filter)) == functionClass) {
              callback = filter;
            } else if (className == arrayClass) {
              // Convert the property names array into a makeshift set.
              properties = {};
              for (var index = 0, length = filter.length, value; index < length; value = filter[index++], ((className = getClass.call(value)), className == stringClass || className == numberClass) && (properties[value] = 1));
            }
          }
          if (width) {
            if ((className = getClass.call(width)) == numberClass) {
              // Convert the `width` to an integer and create a string containing
              // `width` number of space characters.
              if ((width -= width % 1) > 0) {
                for (whitespace = "", width > 10 && (width = 10); whitespace.length < width; whitespace += " ");
              }
            } else if (className == stringClass) {
              whitespace = width.length <= 10 ? width : width.slice(0, 10);
            }
          }
          // Opera <= 7.54u2 discards the values associated with empty string keys
          // (`""`) only if they are used directly within an object member list
          // (e.g., `!("" in { "": 1})`).
          return serialize("", (value = {}, value[""] = source, value), callback, properties, whitespace, "", []);
        };
      }

      // Public: Parses a JSON source string.
      if (!has("json-parse")) {
        var fromCharCode = String.fromCharCode;

        // Internal: A map of escaped control characters and their unescaped
        // equivalents.
        var Unescapes = {
          92: "\\",
          34: '"',
          47: "/",
          98: "\b",
          116: "\t",
          110: "\n",
          102: "\f",
          114: "\r"
        };

        // Internal: Stores the parser state.
        var Index, Source;

        // Internal: Resets the parser state and throws a `SyntaxError`.
        var abort = function () {
          Index = Source = null;
          throw SyntaxError();
        };

        // Internal: Returns the next token, or `"$"` if the parser has reached
        // the end of the source string. A token may be a string, number, `null`
        // literal, or Boolean literal.
        var lex = function () {
          var source = Source, length = source.length, value, begin, position, isSigned, charCode;
          while (Index < length) {
            charCode = source.charCodeAt(Index);
            switch (charCode) {
              case 9: case 10: case 13: case 32:
                // Skip whitespace tokens, including tabs, carriage returns, line
                // feeds, and space characters.
                Index++;
                break;
              case 123: case 125: case 91: case 93: case 58: case 44:
                // Parse a punctuator token (`{`, `}`, `[`, `]`, `:`, or `,`) at
                // the current position.
                value = charIndexBuggy ? source.charAt(Index) : source[Index];
                Index++;
                return value;
              case 34:
                // `"` delimits a JSON string; advance to the next character and
                // begin parsing the string. String tokens are prefixed with the
                // sentinel `@` character to distinguish them from punctuators and
                // end-of-string tokens.
                for (value = "@", Index++; Index < length;) {
                  charCode = source.charCodeAt(Index);
                  if (charCode < 32) {
                    // Unescaped ASCII control characters (those with a code unit
                    // less than the space character) are not permitted.
                    abort();
                  } else if (charCode == 92) {
                    // A reverse solidus (`\`) marks the beginning of an escaped
                    // control character (including `"`, `\`, and `/`) or Unicode
                    // escape sequence.
                    charCode = source.charCodeAt(++Index);
                    switch (charCode) {
                      case 92: case 34: case 47: case 98: case 116: case 110: case 102: case 114:
                        // Revive escaped control characters.
                        value += Unescapes[charCode];
                        Index++;
                        break;
                      case 117:
                        // `\u` marks the beginning of a Unicode escape sequence.
                        // Advance to the first character and validate the
                        // four-digit code point.
                        begin = ++Index;
                        for (position = Index + 4; Index < position; Index++) {
                          charCode = source.charCodeAt(Index);
                          // A valid sequence comprises four hexdigits (case-
                          // insensitive) that form a single hexadecimal value.
                          if (!(charCode >= 48 && charCode <= 57 || charCode >= 97 && charCode <= 102 || charCode >= 65 && charCode <= 70)) {
                            // Invalid Unicode escape sequence.
                            abort();
                          }
                        }
                        // Revive the escaped character.
                        value += fromCharCode("0x" + source.slice(begin, Index));
                        break;
                      default:
                        // Invalid escape sequence.
                        abort();
                    }
                  } else {
                    if (charCode == 34) {
                      // An unescaped double-quote character marks the end of the
                      // string.
                      break;
                    }
                    charCode = source.charCodeAt(Index);
                    begin = Index;
                    // Optimize for the common case where a string is valid.
                    while (charCode >= 32 && charCode != 92 && charCode != 34) {
                      charCode = source.charCodeAt(++Index);
                    }
                    // Append the string as-is.
                    value += source.slice(begin, Index);
                  }
                }
                if (source.charCodeAt(Index) == 34) {
                  // Advance to the next character and return the revived string.
                  Index++;
                  return value;
                }
                // Unterminated string.
                abort();
              default:
                // Parse numbers and literals.
                begin = Index;
                // Advance past the negative sign, if one is specified.
                if (charCode == 45) {
                  isSigned = true;
                  charCode = source.charCodeAt(++Index);
                }
                // Parse an integer or floating-point value.
                if (charCode >= 48 && charCode <= 57) {
                  // Leading zeroes are interpreted as octal literals.
                  if (charCode == 48 && ((charCode = source.charCodeAt(Index + 1)), charCode >= 48 && charCode <= 57)) {
                    // Illegal octal literal.
                    abort();
                  }
                  isSigned = false;
                  // Parse the integer component.
                  for (; Index < length && ((charCode = source.charCodeAt(Index)), charCode >= 48 && charCode <= 57); Index++);
                  // Floats cannot contain a leading decimal point; however, this
                  // case is already accounted for by the parser.
                  if (source.charCodeAt(Index) == 46) {
                    position = ++Index;
                    // Parse the decimal component.
                    for (; position < length && ((charCode = source.charCodeAt(position)), charCode >= 48 && charCode <= 57); position++);
                    if (position == Index) {
                      // Illegal trailing decimal.
                      abort();
                    }
                    Index = position;
                  }
                  // Parse exponents. The `e` denoting the exponent is
                  // case-insensitive.
                  charCode = source.charCodeAt(Index);
                  if (charCode == 101 || charCode == 69) {
                    charCode = source.charCodeAt(++Index);
                    // Skip past the sign following the exponent, if one is
                    // specified.
                    if (charCode == 43 || charCode == 45) {
                      Index++;
                    }
                    // Parse the exponential component.
                    for (position = Index; position < length && ((charCode = source.charCodeAt(position)), charCode >= 48 && charCode <= 57); position++);
                    if (position == Index) {
                      // Illegal empty exponent.
                      abort();
                    }
                    Index = position;
                  }
                  // Coerce the parsed value to a JavaScript number.
                  return +source.slice(begin, Index);
                }
                // A negative sign may only precede numbers.
                if (isSigned) {
                  abort();
                }
                // `true`, `false`, and `null` literals.
                if (source.slice(Index, Index + 4) == "true") {
                  Index += 4;
                  return true;
                } else if (source.slice(Index, Index + 5) == "false") {
                  Index += 5;
                  return false;
                } else if (source.slice(Index, Index + 4) == "null") {
                  Index += 4;
                  return null;
                }
                // Unrecognized token.
                abort();
            }
          }
          // Return the sentinel `$` character if the parser has reached the end
          // of the source string.
          return "$";
        };

        // Internal: Parses a JSON `value` token.
        var get = function (value) {
          var results, hasMembers;
          if (value == "$") {
            // Unexpected end of input.
            abort();
          }
          if (typeof value == "string") {
            if ((charIndexBuggy ? value.charAt(0) : value[0]) == "@") {
              // Remove the sentinel `@` character.
              return value.slice(1);
            }
            // Parse object and array literals.
            if (value == "[") {
              // Parses a JSON array, returning a new JavaScript array.
              results = [];
              for (;; hasMembers || (hasMembers = true)) {
                value = lex();
                // A closing square bracket marks the end of the array literal.
                if (value == "]") {
                  break;
                }
                // If the array literal contains elements, the current token
                // should be a comma separating the previous element from the
                // next.
                if (hasMembers) {
                  if (value == ",") {
                    value = lex();
                    if (value == "]") {
                      // Unexpected trailing `,` in array literal.
                      abort();
                    }
                  } else {
                    // A `,` must separate each array element.
                    abort();
                  }
                }
                // Elisions and leading commas are not permitted.
                if (value == ",") {
                  abort();
                }
                results.push(get(value));
              }
              return results;
            } else if (value == "{") {
              // Parses a JSON object, returning a new JavaScript object.
              results = {};
              for (;; hasMembers || (hasMembers = true)) {
                value = lex();
                // A closing curly brace marks the end of the object literal.
                if (value == "}") {
                  break;
                }
                // If the object literal contains members, the current token
                // should be a comma separator.
                if (hasMembers) {
                  if (value == ",") {
                    value = lex();
                    if (value == "}") {
                      // Unexpected trailing `,` in object literal.
                      abort();
                    }
                  } else {
                    // A `,` must separate each object member.
                    abort();
                  }
                }
                // Leading commas are not permitted, object property names must be
                // double-quoted strings, and a `:` must separate each property
                // name and value.
                if (value == "," || typeof value != "string" || (charIndexBuggy ? value.charAt(0) : value[0]) != "@" || lex() != ":") {
                  abort();
                }
                results[value.slice(1)] = get(lex());
              }
              return results;
            }
            // Unexpected token encountered.
            abort();
          }
          return value;
        };

        // Internal: Updates a traversed object member.
        var update = function (source, property, callback) {
          var element = walk(source, property, callback);
          if (element === undef) {
            delete source[property];
          } else {
            source[property] = element;
          }
        };

        // Internal: Recursively traverses a parsed JSON object, invoking the
        // `callback` function for each value. This is an implementation of the
        // `Walk(holder, name)` operation defined in ES 5.1 section 15.12.2.
        var walk = function (source, property, callback) {
          var value = source[property], length;
          if (typeof value == "object" && value) {
            // `forEach` can't be used to traverse an array in Opera <= 8.54
            // because its `Object#hasOwnProperty` implementation returns `false`
            // for array indices (e.g., `![1, 2, 3].hasOwnProperty("0")`).
            if (getClass.call(value) == arrayClass) {
              for (length = value.length; length--;) {
                update(value, length, callback);
              }
            } else {
              forEach(value, function (property) {
                update(value, property, callback);
              });
            }
          }
          return callback.call(source, property, value);
        };

        // Public: `JSON.parse`. See ES 5.1 section 15.12.2.
        exports.parse = function (source, callback) {
          var result, value;
          Index = 0;
          Source = "" + source;
          result = get(lex());
          // If a JSON string contains multiple tokens, it is invalid.
          if (lex() != "$") {
            abort();
          }
          // Reset the parser state.
          Index = Source = null;
          return callback && getClass.call(callback) == functionClass ? walk((value = {}, value[""] = result, value), "", callback) : result;
        };
      }
    }

    exports["runInContext"] = runInContext;
    return exports;
  }

  if (freeExports && !isLoader) {
    // Export for CommonJS environments.
    runInContext(root, freeExports);
  } else {
    // Export for web browsers and JavaScript engines.
    var nativeJSON = root.JSON,
        previousJSON = root["JSON3"],
        isRestored = false;

    var JSON3 = runInContext(root, (root["JSON3"] = {
      // Public: Restores the original value of the global `JSON` object and
      // returns a reference to the `JSON3` object.
      "noConflict": function () {
        if (!isRestored) {
          isRestored = true;
          root.JSON = nativeJSON;
          root["JSON3"] = previousJSON;
          nativeJSON = previousJSON = null;
        }
        return JSON3;
      }
    }));

    root.JSON = {
      "parse": JSON3.parse,
      "stringify": JSON3.stringify
    };
  }

  // Export for asynchronous module loaders.
  if (isLoader) {
    define(function () {
      return JSON3;
    });
  }
}).call(this);

}).call(this,typeof global !== "undefined" ? global : typeof self !== "undefined" ? self : typeof window !== "undefined" ? window : {})

},{}],59:[function(require,module,exports){
'use strict';

var has = Object.prototype.hasOwnProperty;

/**
 * Simple query string parser.
 *
 * @param {String} query The query string that needs to be parsed.
 * @returns {Object}
 * @api public
 */
function querystring(query) {
  var parser = /([^=?&]+)=?([^&]*)/g
    , result = {}
    , part;

  //
  // Little nifty parsing hack, leverage the fact that RegExp.exec increments
  // the lastIndex property so we can continue executing this loop until we've
  // parsed all results.
  //
  for (;
    part = parser.exec(query);
    result[decodeURIComponent(part[1])] = decodeURIComponent(part[2])
  );

  return result;
}

/**
 * Transform a query string to an object.
 *
 * @param {Object} obj Object that should be transformed.
 * @param {String} prefix Optional prefix.
 * @returns {String}
 * @api public
 */
function querystringify(obj, prefix) {
  prefix = prefix || '';

  var pairs = [];

  //
  // Optionally prefix with a '?' if needed
  //
  if ('string' !== typeof prefix) prefix = '?';

  for (var key in obj) {
    if (has.call(obj, key)) {
      pairs.push(encodeURIComponent(key) +'='+ encodeURIComponent(obj[key]));
    }
  }

  return pairs.length ? prefix + pairs.join('&') : '';
}

//
// Expose the module.
//
exports.stringify = querystringify;
exports.parse = querystring;

},{}],60:[function(require,module,exports){
'use strict';

/**
 * Check if we're required to add a port number.
 *
 * @see https://url.spec.whatwg.org/#default-port
 * @param {Number|String} port Port number we need to check
 * @param {String} protocol Protocol we need to check against.
 * @returns {Boolean} Is it a default port for the given protocol
 * @api private
 */
module.exports = function required(port, protocol) {
  protocol = protocol.split(':')[0];
  port = +port;

  if (!port) return false;

  switch (protocol) {
    case 'http':
    case 'ws':
    return port !== 80;

    case 'https':
    case 'wss':
    return port !== 443;

    case 'ftp':
    return port !== 21;

    case 'gopher':
    return port !== 70;

    case 'file':
    return false;
  }

  return port !== 0;
};

},{}],61:[function(require,module,exports){
'use strict';

var required = require('requires-port')
  , lolcation = require('./lolcation')
  , qs = require('querystringify')
  , protocolre = /^([a-z][a-z0-9.+-]*:)?(\/\/)?([\S\s]*)/i;

/**
 * These are the parse rules for the URL parser, it informs the parser
 * about:
 *
 * 0. The char it Needs to parse, if it's a string it should be done using
 *    indexOf, RegExp using exec and NaN means set as current value.
 * 1. The property we should set when parsing this value.
 * 2. Indication if it's backwards or forward parsing, when set as number it's
 *    the value of extra chars that should be split off.
 * 3. Inherit from location if non existing in the parser.
 * 4. `toLowerCase` the resulting value.
 */
var rules = [
  ['#', 'hash'],                        // Extract from the back.
  ['?', 'query'],                       // Extract from the back.
  ['/', 'pathname'],                    // Extract from the back.
  ['@', 'auth', 1],                     // Extract from the front.
  [NaN, 'host', undefined, 1, 1],       // Set left over value.
  [/:(\d+)$/, 'port', undefined, 1],    // RegExp the back.
  [NaN, 'hostname', undefined, 1, 1]    // Set left over.
];

/**
 * @typedef ProtocolExtract
 * @type Object
 * @property {String} protocol Protocol matched in the URL, in lowercase.
 * @property {Boolean} slashes `true` if protocol is followed by "//", else `false`.
 * @property {String} rest Rest of the URL that is not part of the protocol.
 */

/**
 * Extract protocol information from a URL with/without double slash ("//").
 *
 * @param {String} address URL we want to extract from.
 * @return {ProtocolExtract} Extracted information.
 * @api private
 */
function extractProtocol(address) {
  var match = protocolre.exec(address);

  return {
    protocol: match[1] ? match[1].toLowerCase() : '',
    slashes: !!match[2],
    rest: match[3]
  };
}

/**
 * Resolve a relative URL pathname against a base URL pathname.
 *
 * @param {String} relative Pathname of the relative URL.
 * @param {String} base Pathname of the base URL.
 * @return {String} Resolved pathname.
 * @api private
 */
function resolve(relative, base) {
  var path = (base || '/').split('/').slice(0, -1).concat(relative.split('/'))
    , i = path.length
    , last = path[i - 1]
    , unshift = false
    , up = 0;

  while (i--) {
    if (path[i] === '.') {
      path.splice(i, 1);
    } else if (path[i] === '..') {
      path.splice(i, 1);
      up++;
    } else if (up) {
      if (i === 0) unshift = true;
      path.splice(i, 1);
      up--;
    }
  }

  if (unshift) path.unshift('');
  if (last === '.' || last === '..') path.push('');

  return path.join('/');
}

/**
 * The actual URL instance. Instead of returning an object we've opted-in to
 * create an actual constructor as it's much more memory efficient and
 * faster and it pleases my OCD.
 *
 * @constructor
 * @param {String} address URL we want to parse.
 * @param {Object|String} location Location defaults for relative paths.
 * @param {Boolean|Function} parser Parser for the query string.
 * @api public
 */
function URL(address, location, parser) {
  if (!(this instanceof URL)) {
    return new URL(address, location, parser);
  }

  var relative, extracted, parse, instruction, index, key
    , instructions = rules.slice()
    , type = typeof location
    , url = this
    , i = 0;

  //
  // The following if statements allows this module two have compatibility with
  // 2 different API:
  //
  // 1. Node.js's `url.parse` api which accepts a URL, boolean as arguments
  //    where the boolean indicates that the query string should also be parsed.
  //
  // 2. The `URL` interface of the browser which accepts a URL, object as
  //    arguments. The supplied object will be used as default values / fall-back
  //    for relative paths.
  //
  if ('object' !== type && 'string' !== type) {
    parser = location;
    location = null;
  }

  if (parser && 'function' !== typeof parser) parser = qs.parse;

  location = lolcation(location);

  //
  // Extract protocol information before running the instructions.
  //
  extracted = extractProtocol(address || '');
  relative = !extracted.protocol && !extracted.slashes;
  url.slashes = extracted.slashes || relative && location.slashes;
  url.protocol = extracted.protocol || location.protocol || '';
  address = extracted.rest;

  //
  // When the authority component is absent the URL starts with a path
  // component.
  //
  if (!extracted.slashes) instructions[2] = [/(.*)/, 'pathname'];

  for (; i < instructions.length; i++) {
    instruction = instructions[i];
    parse = instruction[0];
    key = instruction[1];

    if (parse !== parse) {
      url[key] = address;
    } else if ('string' === typeof parse) {
      if (~(index = address.indexOf(parse))) {
        if ('number' === typeof instruction[2]) {
          url[key] = address.slice(0, index);
          address = address.slice(index + instruction[2]);
        } else {
          url[key] = address.slice(index);
          address = address.slice(0, index);
        }
      }
    } else if ((index = parse.exec(address))) {
      url[key] = index[1];
      address = address.slice(0, index.index);
    }

    url[key] = url[key] || (
      relative && instruction[3] ? location[key] || '' : ''
    );

    //
    // Hostname, host and protocol should be lowercased so they can be used to
    // create a proper `origin`.
    //
    if (instruction[4]) url[key] = url[key].toLowerCase();
  }

  //
  // Also parse the supplied query string in to an object. If we're supplied
  // with a custom parser as function use that instead of the default build-in
  // parser.
  //
  if (parser) url.query = parser(url.query);

  //
  // If the URL is relative, resolve the pathname against the base URL.
  //
  if (
      relative
    && location.slashes
    && url.pathname.charAt(0) !== '/'
    && (url.pathname !== '' || location.pathname !== '')
  ) {
    url.pathname = resolve(url.pathname, location.pathname);
  }

  //
  // We should not add port numbers if they are already the default port number
  // for a given protocol. As the host also contains the port number we're going
  // override it with the hostname which contains no port number.
  //
  if (!required(url.port, url.protocol)) {
    url.host = url.hostname;
    url.port = '';
  }

  //
  // Parse down the `auth` for the username and password.
  //
  url.username = url.password = '';
  if (url.auth) {
    instruction = url.auth.split(':');
    url.username = instruction[0] || '';
    url.password = instruction[1] || '';
  }

  url.origin = url.protocol && url.host && url.protocol !== 'file:'
    ? url.protocol +'//'+ url.host
    : 'null';

  //
  // The href is just the compiled result.
  //
  url.href = url.toString();
}

/**
 * This is convenience method for changing properties in the URL instance to
 * insure that they all propagate correctly.
 *
 * @param {String} part          Property we need to adjust.
 * @param {Mixed} value          The newly assigned value.
 * @param {Boolean|Function} fn  When setting the query, it will be the function
 *                               used to parse the query.
 *                               When setting the protocol, double slash will be
 *                               removed from the final url if it is true.
 * @returns {URL}
 * @api public
 */
function set(part, value, fn) {
  var url = this;

  switch (part) {
    case 'query':
      if ('string' === typeof value && value.length) {
        value = (fn || qs.parse)(value);
      }

      url[part] = value;
      break;

    case 'port':
      url[part] = value;

      if (!required(value, url.protocol)) {
        url.host = url.hostname;
        url[part] = '';
      } else if (value) {
        url.host = url.hostname +':'+ value;
      }

      break;

    case 'hostname':
      url[part] = value;

      if (url.port) value += ':'+ url.port;
      url.host = value;
      break;

    case 'host':
      url[part] = value;

      if (/:\d+$/.test(value)) {
        value = value.split(':');
        url.port = value.pop();
        url.hostname = value.join(':');
      } else {
        url.hostname = value;
        url.port = '';
      }

      break;

    case 'protocol':
      url.protocol = value.toLowerCase();
      url.slashes = !fn;
      break;

    case 'pathname':
      url.pathname = value.length && value.charAt(0) !== '/' ? '/' + value : value;

      break;

    default:
      url[part] = value;
  }

  for (var i = 0; i < rules.length; i++) {
    var ins = rules[i];

    if (ins[4]) url[ins[1]] = url[ins[1]].toLowerCase();
  }

  url.origin = url.protocol && url.host && url.protocol !== 'file:'
    ? url.protocol +'//'+ url.host
    : 'null';

  url.href = url.toString();

  return url;
};

/**
 * Transform the properties back in to a valid and full URL string.
 *
 * @param {Function} stringify Optional query stringify function.
 * @returns {String}
 * @api public
 */
function toString(stringify) {
  if (!stringify || 'function' !== typeof stringify) stringify = qs.stringify;

  var query
    , url = this
    , protocol = url.protocol;

  if (protocol && protocol.charAt(protocol.length - 1) !== ':') protocol += ':';

  var result = protocol + (url.slashes ? '//' : '');

  if (url.username) {
    result += url.username;
    if (url.password) result += ':'+ url.password;
    result += '@';
  }

  result += url.host + url.pathname;

  query = 'object' === typeof url.query ? stringify(url.query) : url.query;
  if (query) result += '?' !== query.charAt(0) ? '?'+ query : query;

  if (url.hash) result += url.hash;

  return result;
}

URL.prototype = { set: set, toString: toString };

//
// Expose the URL parser and some additional properties that might be useful for
// others or testing.
//
URL.extractProtocol = extractProtocol;
URL.location = lolcation;
URL.qs = qs;

module.exports = URL;

},{"./lolcation":62,"querystringify":59,"requires-port":60}],62:[function(require,module,exports){
(function (global){
'use strict';

var slashes = /^[A-Za-z][A-Za-z0-9+-.]*:\/\//;

/**
 * These properties should not be copied or inherited from. This is only needed
 * for all non blob URL's as a blob URL does not include a hash, only the
 * origin.
 *
 * @type {Object}
 * @private
 */
var ignore = { hash: 1, query: 1 }
  , URL;

/**
 * The location object differs when your code is loaded through a normal page,
 * Worker or through a worker using a blob. And with the blobble begins the
 * trouble as the location object will contain the URL of the blob, not the
 * location of the page where our code is loaded in. The actual origin is
 * encoded in the `pathname` so we can thankfully generate a good "default"
 * location from it so we can generate proper relative URL's again.
 *
 * @param {Object|String} loc Optional default location object.
 * @returns {Object} lolcation object.
 * @api public
 */
module.exports = function lolcation(loc) {
  loc = loc || global.location || {};
  URL = URL || require('./');

  var finaldestination = {}
    , type = typeof loc
    , key;

  if ('blob:' === loc.protocol) {
    finaldestination = new URL(unescape(loc.pathname), {});
  } else if ('string' === type) {
    finaldestination = new URL(loc, {});
    for (key in ignore) delete finaldestination[key];
  } else if ('object' === type) {
    for (key in loc) {
      if (key in ignore) continue;
      finaldestination[key] = loc[key];
    }

    if (finaldestination.slashes === undefined) {
      finaldestination.slashes = slashes.test(loc.href);
    }
  }

  return finaldestination;
};

}).call(this,typeof global !== "undefined" ? global : typeof self !== "undefined" ? self : typeof window !== "undefined" ? window : {})

},{"./":61}]},{},[1])(1)
});


//# sourceMappingURL=sockjs.js.map
/* WEBPACK VAR INJECTION */}.call(exports, __webpack_require__(14)))

/***/ }),
/* 14 */
/***/ (function(module, exports) {

var g;

// This works in non-strict mode
g = (function() {
	return this;
})();

try {
	// This works if eval is allowed (see CSP)
	g = g || Function("return this")() || (1,eval)("this");
} catch(e) {
	// This works if the window reference is available
	if(typeof window === "object")
		g = window;
}

// g can still be undefined, but nothing to do about it...
// We return undefined, instead of nothing here, so it's
// easier to handle this case. if(!global) { ...}

module.exports = g;


/***/ }),
/* 15 */
/***/ (function(module, exports) {

// Generated by CoffeeScript 1.12.6

/*
   Stomp Over WebSocket http://www.jmesnil.net/stomp-websocket/doc/ | Apache License V2.0
   Copyright (C) 2010-2013 [Jeff Mesnil](http://jmesnil.net/)
   Copyright (C) 2012 [FuseSource, Inc.](http://fusesource.com)
   Copyright (C) 2017 [Deepak Kumar](https://www.kreatio.com)
 */

(function() {
  var Byte, Client, Frame, Stomp,
    hasProp = {}.hasOwnProperty,
    slice = [].slice;

  Byte = {
    LF: '\x0A',
    NULL: '\x00'
  };

  Frame = (function() {
    var unmarshallSingle;

    function Frame(command1, headers1, body1, escapeHeaderValues1) {
      this.command = command1;
      this.headers = headers1 != null ? headers1 : {};
      this.body = body1 != null ? body1 : '';
      this.escapeHeaderValues = escapeHeaderValues1 != null ? escapeHeaderValues1 : false;
    }

    Frame.prototype.toString = function() {
      var lines, name, ref, skipContentLength, value;
      lines = [this.command];
      skipContentLength = (this.headers['content-length'] === false) ? true : false;
      if (skipContentLength) {
        delete this.headers['content-length'];
      }
      ref = this.headers;
      for (name in ref) {
        if (!hasProp.call(ref, name)) continue;
        value = ref[name];
        if (this.escapeHeaderValues && this.command !== 'CONNECT' && this.command !== 'CONNECTED') {
          lines.push(name + ":" + (Frame.frEscape(value)));
        } else {
          lines.push(name + ":" + value);
        }
      }
      if (this.body && !skipContentLength) {
        lines.push("content-length:" + (Frame.sizeOfUTF8(this.body)));
      }
      lines.push(Byte.LF + this.body);
      return lines.join(Byte.LF);
    };

    Frame.sizeOfUTF8 = function(s) {
      if (s) {
        return encodeURI(s).match(/%..|./g).length;
      } else {
        return 0;
      }
    };

    unmarshallSingle = function(data, escapeHeaderValues) {
      var body, chr, command, divider, headerLines, headers, i, idx, j, k, len, len1, line, ref, ref1, ref2, start, trim;
      if (escapeHeaderValues == null) {
        escapeHeaderValues = false;
      }
      divider = data.search(RegExp("" + Byte.LF + Byte.LF));
      headerLines = data.substring(0, divider).split(Byte.LF);
      command = headerLines.shift();
      headers = {};
      trim = function(str) {
        return str.replace(/^\s+|\s+$/g, '');
      };
      ref = headerLines.reverse();
      for (j = 0, len1 = ref.length; j < len1; j++) {
        line = ref[j];
        idx = line.indexOf(':');
        if (escapeHeaderValues && command !== 'CONNECT' && command !== 'CONNECTED') {
          headers[trim(line.substring(0, idx))] = Frame.frUnEscape(trim(line.substring(idx + 1)));
        } else {
          headers[trim(line.substring(0, idx))] = trim(line.substring(idx + 1));
        }
      }
      body = '';
      start = divider + 2;
      if (headers['content-length']) {
        len = parseInt(headers['content-length']);
        body = ('' + data).substring(start, start + len);
      } else {
        chr = null;
        for (i = k = ref1 = start, ref2 = data.length; ref1 <= ref2 ? k < ref2 : k > ref2; i = ref1 <= ref2 ? ++k : --k) {
          chr = data.charAt(i);
          if (chr === Byte.NULL) {
            break;
          }
          body += chr;
        }
      }
      return new Frame(command, headers, body, escapeHeaderValues);
    };

    Frame.unmarshall = function(datas, escapeHeaderValues) {
      var frame, frames, last_frame, r;
      if (escapeHeaderValues == null) {
        escapeHeaderValues = false;
      }
      frames = datas.split(RegExp("" + Byte.NULL + Byte.LF + "*"));
      r = {
        frames: [],
        partial: ''
      };
      r.frames = (function() {
        var j, len1, ref, results;
        ref = frames.slice(0, -1);
        results = [];
        for (j = 0, len1 = ref.length; j < len1; j++) {
          frame = ref[j];
          results.push(unmarshallSingle(frame, escapeHeaderValues));
        }
        return results;
      })();
      last_frame = frames.slice(-1)[0];
      if (last_frame === Byte.LF || (last_frame.search(RegExp("" + Byte.NULL + Byte.LF + "*$"))) !== -1) {
        r.frames.push(unmarshallSingle(last_frame, escapeHeaderValues));
      } else {
        r.partial = last_frame;
      }
      return r;
    };

    Frame.marshall = function(command, headers, body, escapeHeaderValues) {
      var frame;
      frame = new Frame(command, headers, body, escapeHeaderValues);
      return frame.toString() + Byte.NULL;
    };

    Frame.frEscape = function(str) {
      return ("" + str).replace(/\\/g, "\\\\").replace(/\r/g, "\\r").replace(/\n/g, "\\n").replace(/:/g, "\\c");
    };

    Frame.frUnEscape = function(str) {
      return ("" + str).replace(/\\r/g, "\r").replace(/\\n/g, "\n").replace(/\\c/g, ":").replace(/\\\\/g, "\\");
    };

    return Frame;

  })();

  Client = (function() {
    var now;

    function Client(ws_fn) {
      this.ws_fn = function() {
        var ws;
        ws = ws_fn();
        ws.binaryType = "arraybuffer";
        return ws;
      };
      this.reconnect_delay = 0;
      this.counter = 0;
      this.connected = false;
      this.heartbeat = {
        outgoing: 10000,
        incoming: 10000
      };
      this.maxWebSocketFrameSize = 16 * 1024;
      this.subscriptions = {};
      this.partialData = '';
    }

    Client.prototype.debug = function(message) {
      var ref;
      return typeof window !== "undefined" && window !== null ? (ref = window.console) != null ? ref.log(message) : void 0 : void 0;
    };

    now = function() {
      if (Date.now) {
        return Date.now();
      } else {
        return new Date().valueOf;
      }
    };

    Client.prototype._transmit = function(command, headers, body) {
      var out;
      out = Frame.marshall(command, headers, body, this.escapeHeaderValues);
      if (typeof this.debug === "function") {
        this.debug(">>> " + out);
      }
      while (true) {
        if (out.length > this.maxWebSocketFrameSize) {
          this.ws.send(out.substring(0, this.maxWebSocketFrameSize));
          out = out.substring(this.maxWebSocketFrameSize);
          if (typeof this.debug === "function") {
            this.debug("remaining = " + out.length);
          }
        } else {
          return this.ws.send(out);
        }
      }
    };

    Client.prototype._setupHeartbeat = function(headers) {
      var ref, ref1, serverIncoming, serverOutgoing, ttl, v;
      if ((ref = headers.version) !== Stomp.VERSIONS.V1_1 && ref !== Stomp.VERSIONS.V1_2) {
        return;
      }
      ref1 = (function() {
        var j, len1, ref1, results;
        ref1 = headers['heart-beat'].split(",");
        results = [];
        for (j = 0, len1 = ref1.length; j < len1; j++) {
          v = ref1[j];
          results.push(parseInt(v));
        }
        return results;
      })(), serverOutgoing = ref1[0], serverIncoming = ref1[1];
      if (!(this.heartbeat.outgoing === 0 || serverIncoming === 0)) {
        ttl = Math.max(this.heartbeat.outgoing, serverIncoming);
        if (typeof this.debug === "function") {
          this.debug("send PING every " + ttl + "ms");
        }
        this.pinger = Stomp.setInterval(ttl, (function(_this) {
          return function() {
            _this.ws.send(Byte.LF);
            return typeof _this.debug === "function" ? _this.debug(">>> PING") : void 0;
          };
        })(this));
      }
      if (!(this.heartbeat.incoming === 0 || serverOutgoing === 0)) {
        ttl = Math.max(this.heartbeat.incoming, serverOutgoing);
        if (typeof this.debug === "function") {
          this.debug("check PONG every " + ttl + "ms");
        }
        return this.ponger = Stomp.setInterval(ttl, (function(_this) {
          return function() {
            var delta;
            delta = now() - _this.serverActivity;
            if (delta > ttl * 2) {
              if (typeof _this.debug === "function") {
                _this.debug("did not receive server activity for the last " + delta + "ms");
              }
              return _this.ws.close();
            }
          };
        })(this));
      }
    };

    Client.prototype._parseConnect = function() {
      var args, connectCallback, errorCallback, headers;
      args = 1 <= arguments.length ? slice.call(arguments, 0) : [];
      headers = {};
      switch (args.length) {
        case 2:
          headers = args[0], connectCallback = args[1];
          break;
        case 3:
          if (args[1] instanceof Function) {
            headers = args[0], connectCallback = args[1], errorCallback = args[2];
          } else {
            headers.login = args[0], headers.passcode = args[1], connectCallback = args[2];
          }
          break;
        case 4:
          headers.login = args[0], headers.passcode = args[1], connectCallback = args[2], errorCallback = args[3];
          break;
        default:
          headers.login = args[0], headers.passcode = args[1], connectCallback = args[2], errorCallback = args[3], headers.host = args[4];
      }
      return [headers, connectCallback, errorCallback];
    };

    Client.prototype.connect = function() {
      var args, out;
      args = 1 <= arguments.length ? slice.call(arguments, 0) : [];
      this.escapeHeaderValues = false;
      out = this._parseConnect.apply(this, args);
      this.headers = out[0], this.connectCallback = out[1], this.errorCallback = out[2];
      return this._connect();
    };

    Client.prototype._connect = function() {
      var errorCallback, headers;
      headers = this.headers;
      errorCallback = this.errorCallback;
      if (typeof this.debug === "function") {
        this.debug("Opening Web Socket...");
      }
      this.ws = this.ws_fn();
      this.ws.onmessage = (function(_this) {
        return function(evt) {
          var arr, c, client, data, frame, j, len1, messageID, onreceive, ref, results, subscription, unmarshalledData;
          data = typeof ArrayBuffer !== 'undefined' && evt.data instanceof ArrayBuffer ? (arr = new Uint8Array(evt.data), typeof _this.debug === "function" ? _this.debug("--- got data length: " + arr.length) : void 0, ((function() {
            var j, len1, results;
            results = [];
            for (j = 0, len1 = arr.length; j < len1; j++) {
              c = arr[j];
              results.push(String.fromCharCode(c));
            }
            return results;
          })()).join('')) : evt.data;
          _this.serverActivity = now();
          if (data === Byte.LF) {
            if (typeof _this.debug === "function") {
              _this.debug("<<< PONG");
            }
            return;
          }
          if (typeof _this.debug === "function") {
            _this.debug("<<< " + data);
          }
          unmarshalledData = Frame.unmarshall(_this.partialData + data, _this.escapeHeaderValues);
          _this.partialData = unmarshalledData.partial;
          ref = unmarshalledData.frames;
          results = [];
          for (j = 0, len1 = ref.length; j < len1; j++) {
            frame = ref[j];
            switch (frame.command) {
              case "CONNECTED":
                if (typeof _this.debug === "function") {
                  _this.debug("connected to server " + frame.headers.server);
                }
                _this.connected = true;
                _this.version = frame.headers.version;
                if (_this.version === Stomp.VERSIONS.V1_2) {
                  _this.escapeHeaderValues = true;
                }
                _this._setupHeartbeat(frame.headers);
                results.push(typeof _this.connectCallback === "function" ? _this.connectCallback(frame) : void 0);
                break;
              case "MESSAGE":
                subscription = frame.headers.subscription;
                onreceive = _this.subscriptions[subscription] || _this.onreceive;
                if (onreceive) {
                  client = _this;
                  if (_this.version === Stomp.VERSIONS.V1_2) {
                    messageID = frame.headers["ack"];
                  } else {
                    messageID = frame.headers["message-id"];
                  }
                  frame.ack = function(headers) {
                    if (headers == null) {
                      headers = {};
                    }
                    return client.ack(messageID, subscription, headers);
                  };
                  frame.nack = function(headers) {
                    if (headers == null) {
                      headers = {};
                    }
                    return client.nack(messageID, subscription, headers);
                  };
                  results.push(onreceive(frame));
                } else {
                  results.push(typeof _this.debug === "function" ? _this.debug("Unhandled received MESSAGE: " + frame) : void 0);
                }
                break;
              case "RECEIPT":
                if (frame.headers["receipt-id"] === _this.closeReceipt) {
                  _this.ws.onclose = null;
                  _this.ws.close();
                  results.push(_this._cleanUp());
                } else {
                  results.push(typeof _this.onreceipt === "function" ? _this.onreceipt(frame) : void 0);
                }
                break;
              case "ERROR":
                results.push(typeof errorCallback === "function" ? errorCallback(frame) : void 0);
                break;
              default:
                results.push(typeof _this.debug === "function" ? _this.debug("Unhandled frame: " + frame) : void 0);
            }
          }
          return results;
        };
      })(this);
      this.ws.onclose = (function(_this) {
        return function() {
          var msg;
          msg = "Whoops! Lost connection to " + _this.ws.url;
          if (typeof _this.debug === "function") {
            _this.debug(msg);
          }
          _this._cleanUp();
          if (typeof errorCallback === "function") {
            errorCallback(msg);
          }
          return _this._schedule_reconnect();
        };
      })(this);
      return this.ws.onopen = (function(_this) {
        return function() {
          if (typeof _this.debug === "function") {
            _this.debug('Web Socket Opened...');
          }
          headers["accept-version"] = Stomp.VERSIONS.supportedVersions();
          headers["heart-beat"] = [_this.heartbeat.outgoing, _this.heartbeat.incoming].join(',');
          return _this._transmit("CONNECT", headers);
        };
      })(this);
    };

    Client.prototype._schedule_reconnect = function() {
      if (this.reconnect_delay > 0) {
        if (typeof this.debug === "function") {
          this.debug("STOMP: scheduling reconnection in " + this.reconnect_delay + "ms");
        }
        return setTimeout((function(_this) {
          return function() {
            if (_this.connected) {
              return typeof _this.debug === "function" ? _this.debug('STOMP: already connected') : void 0;
            } else {
              if (typeof _this.debug === "function") {
                _this.debug('STOMP: attempting to reconnect');
              }
              return _this._connect();
            }
          };
        })(this), this.reconnect_delay);
      }
    };

    Client.prototype.disconnect = function(disconnectCallback, headers) {
      if (headers == null) {
        headers = {};
      }
      if (!headers.receipt) {
        headers.receipt = "close-" + this.counter++;
      }
      this.closeReceipt = headers.receipt;
      this._transmit("DISCONNECT", headers);
      return typeof disconnectCallback === "function" ? disconnectCallback() : void 0;
    };

    Client.prototype._cleanUp = function() {
      this.connected = false;
      this.subscriptions = {};
      this.partial = '';
      if (this.pinger) {
        Stomp.clearInterval(this.pinger);
      }
      if (this.ponger) {
        return Stomp.clearInterval(this.ponger);
      }
    };

    Client.prototype.send = function(destination, headers, body) {
      if (headers == null) {
        headers = {};
      }
      if (body == null) {
        body = '';
      }
      headers.destination = destination;
      return this._transmit("SEND", headers, body);
    };

    Client.prototype.subscribe = function(destination, callback, headers) {
      var client;
      if (headers == null) {
        headers = {};
      }
      if (!headers.id) {
        headers.id = "sub-" + this.counter++;
      }
      headers.destination = destination;
      this.subscriptions[headers.id] = callback;
      this._transmit("SUBSCRIBE", headers);
      client = this;
      return {
        id: headers.id,
        unsubscribe: function(hdrs) {
          return client.unsubscribe(headers.id, hdrs);
        }
      };
    };

    Client.prototype.unsubscribe = function(id, headers) {
      if (headers == null) {
        headers = {};
      }
      delete this.subscriptions[id];
      headers.id = id;
      return this._transmit("UNSUBSCRIBE", headers);
    };

    Client.prototype.begin = function(transaction_id) {
      var client, txid;
      txid = transaction_id || "tx-" + this.counter++;
      this._transmit("BEGIN", {
        transaction: txid
      });
      client = this;
      return {
        id: txid,
        commit: function() {
          return client.commit(txid);
        },
        abort: function() {
          return client.abort(txid);
        }
      };
    };

    Client.prototype.commit = function(transaction_id) {
      return this._transmit("COMMIT", {
        transaction: transaction_id
      });
    };

    Client.prototype.abort = function(transaction_id) {
      return this._transmit("ABORT", {
        transaction: transaction_id
      });
    };

    Client.prototype.ack = function(messageID, subscription, headers) {
      if (headers == null) {
        headers = {};
      }
      if (this.version === Stomp.VERSIONS.V1_2) {
        headers["id"] = messageID;
      } else {
        headers["message-id"] = messageID;
      }
      headers.subscription = subscription;
      return this._transmit("ACK", headers);
    };

    Client.prototype.nack = function(messageID, subscription, headers) {
      if (headers == null) {
        headers = {};
      }
      if (this.version === Stomp.VERSIONS.V1_2) {
        headers["id"] = messageID;
      } else {
        headers["message-id"] = messageID;
      }
      headers.subscription = subscription;
      return this._transmit("NACK", headers);
    };

    return Client;

  })();

  Stomp = {
    VERSIONS: {
      V1_0: '1.0',
      V1_1: '1.1',
      V1_2: '1.2',
      supportedVersions: function() {
        return '1.2,1.1,1.0';
      }
    },
    client: function(url, protocols) {
      var ws_fn;
      if (protocols == null) {
        protocols = ['v10.stomp', 'v11.stomp', 'v12.stomp'];
      }
      ws_fn = function() {
        var klass;
        klass = Stomp.WebSocketClass || WebSocket;
        return new klass(url, protocols);
      };
      return new Client(ws_fn);
    },
    over: function(ws) {
      var ws_fn;
      ws_fn = typeof ws === "function" ? ws : function() {
        return ws;
      };
      return new Client(ws_fn);
    },
    Frame: Frame
  };

  Stomp.setInterval = function(interval, f) {
    return setInterval(f, interval);
  };

  Stomp.clearInterval = function(id) {
    return clearInterval(id);
  };

  if (typeof exports !== "undefined" && exports !== null) {
    exports.Stomp = Stomp;
  }

  if (typeof window !== "undefined" && window !== null) {
    window.Stomp = Stomp;
  } else if (!exports) {
    self.Stomp = Stomp;
  }

}).call(this);

/***/ }),
/* 16 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__logic_geometric_objects__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__run_main__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__command_handlers__ = __webpack_require__(7);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__network_beans__ = __webpack_require__(3);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__board_handling_game_objects__ = __webpack_require__(2);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__logic_accountant__ = __webpack_require__(6);
/**
 * Simulates network communication in an offline situation - for demo issues only
 */






/** Holds the available server command handlers. */
const initHandler = new __WEBPACK_IMPORTED_MODULE_2__command_handlers__["b" /* InitHandler */]();
const updateHandler = new __WEBPACK_IMPORTED_MODULE_2__command_handlers__["e" /* UpdateHandler */]();
const resetHandler = new __WEBPACK_IMPORTED_MODULE_2__command_handlers__["d" /* ResetHandler */]();
const accountHandler = new __WEBPACK_IMPORTED_MODULE_2__command_handlers__["a" /* AccountHandler */]();
/**
 * Handles requests for the server and delivers server messages via callbacks.
 */
class DummyConnector {
    constructor() {
        /**The {@code IngameTriangles} of the player*/
        this.triangles = [];
        /**The account balance of the player*/
        this.money = 0;
    }
    /**
     * Initialises this connector and calls the given method once the connecion is completed.
     * @param readyHandler callback once the connection is established
     */
    init(readyHandler) {
        setInterval(() => {
            for (let triangle of this.triangles)
                this.money += Object(__WEBPACK_IMPORTED_MODULE_5__logic_accountant__["d" /* getTurnIncomeFor */])(triangle);
            Object(__WEBPACK_IMPORTED_MODULE_1__run_main__["setAccount"])(this.money);
        }, 1000);
        readyHandler.call(this, true);
    }
    /**
     * Registers a new player for the game.
     */
    registerPlayer() {
        let player = "[not connected]";
        let v1 = new __WEBPACK_IMPORTED_MODULE_0__logic_geometric_objects__["c" /* Vertex */](0, 0);
        let v2 = new __WEBPACK_IMPORTED_MODULE_0__logic_geometric_objects__["c" /* Vertex */](50, 0);
        let v3 = new __WEBPACK_IMPORTED_MODULE_0__logic_geometric_objects__["c" /* Vertex */](0, 50);
        let edges = Object(__WEBPACK_IMPORTED_MODULE_0__logic_geometric_objects__["d" /* verticesToEdges */])(v1, v2, v3);
        let triangle = new __WEBPACK_IMPORTED_MODULE_0__logic_geometric_objects__["b" /* Triangle */](edges[0]);
        this.triangles.push(__WEBPACK_IMPORTED_MODULE_4__board_handling_game_objects__["a" /* IngameTriangle */].of(triangle, player));
        let vb1 = __WEBPACK_IMPORTED_MODULE_3__network_beans__["f" /* VertexBean */].of(v1, player);
        let vb2 = __WEBPACK_IMPORTED_MODULE_3__network_beans__["f" /* VertexBean */].of(v2, player);
        let vb3 = __WEBPACK_IMPORTED_MODULE_3__network_beans__["f" /* VertexBean */].of(v3, player);
        let triangleBean = new __WEBPACK_IMPORTED_MODULE_3__network_beans__["e" /* TriangleBean */](vb1, vb2, vb3, Object(__WEBPACK_IMPORTED_MODULE_5__logic_accountant__["a" /* getCostsFor */])(triangle), 0, player);
        initHandler.process({
            playerId: player,
            initialState: __WEBPACK_IMPORTED_MODULE_3__network_beans__["c" /* ResetState */].fromJson({ trianglesToCreate: [triangleBean] })
        });
    }
    /**Proposes a {@code Triangle} to the server which might appear in the next {@code UpdateCommand} from the server.
     *@toRequest the {@code Triangle} which shall be proposed
     */
    proposeTriangle(toRequest) {
        if (!toRequest)
            Object(__WEBPACK_IMPORTED_MODULE_1__run_main__["resume"])();
        let upgrade = false;
        for (let triangle of this.triangles) {
            if (triangle.equals(toRequest)) {
                if (!triangle.baseValue)
                    triangle.baseValue = Object(__WEBPACK_IMPORTED_MODULE_5__logic_accountant__["e" /* getValueOf */])(triangle);
                this.money -= Object(__WEBPACK_IMPORTED_MODULE_5__logic_accountant__["b" /* getDefenseUpgradeCosts */])(triangle);
                toRequest.defense += 1;
                upgrade = true;
                break;
            }
        }
        if (!upgrade) {
            toRequest.baseValue = Object(__WEBPACK_IMPORTED_MODULE_5__logic_accountant__["e" /* getValueOf */])(toRequest);
            toRequest.defense = 0;
            this.money -= Object(__WEBPACK_IMPORTED_MODULE_5__logic_accountant__["a" /* getCostsFor */])(toRequest);
            this.triangles.push(toRequest);
        }
        updateHandler.process({
            trianglesToAdd: [toRequest],
            trianglesToRemove: []
        });
    }
    /**
     * Requests the complete server state. Useful if the client's state seems to be inconsistent server's.
     */
    requestCompleteState() {
        resetHandler.process({
            trianglesToCreate: []
        });
    }
    newGame() {
        resetHandler.process({
            trianglesToCreate: []
        });
    }
}
/* harmony export (immutable) */ __webpack_exports__["a"] = DummyConnector;



/***/ })
/******/ ]);
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIndlYnBhY2s6Ly8vd2VicGFjay9ib290c3RyYXAgOTI5NDc4MGFiODk5YTIxYWNiNzIiLCJ3ZWJwYWNrOi8vLy4vdHlwZXNjcmlwdC9ydW4vbWFpbi50cyIsIndlYnBhY2s6Ly8vLi90eXBlc2NyaXB0L2xvZ2ljL2dlb21ldHJpY19vYmplY3RzLnRzIiwid2VicGFjazovLy8uL3R5cGVzY3JpcHQvYm9hcmRfaGFuZGxpbmcvZ2FtZV9vYmplY3RzLnRzIiwid2VicGFjazovLy8uL3R5cGVzY3JpcHQvbmV0d29yay9uZXR3b3JrX2JlYW5zLnRzIiwid2VicGFjazovLy8uL3R5cGVzY3JpcHQvdXRpbC9jb2xvcnMudHMiLCJ3ZWJwYWNrOi8vLy4vdHlwZXNjcmlwdC91dGlsL3NoYXJlZC50cyIsIndlYnBhY2s6Ly8vLi90eXBlc2NyaXB0L2xvZ2ljL2FjY291bnRhbnQudHMiLCJ3ZWJwYWNrOi8vLy4vdHlwZXNjcmlwdC9uZXR3b3JrL2NvbW1hbmRfaGFuZGxlcnMudHMiLCJ3ZWJwYWNrOi8vLy4vdHlwZXNjcmlwdC9ib2FyZF9oYW5kbGluZy9jYW52YXNfaGFuZGxlci50cyIsIndlYnBhY2s6Ly8vLi90eXBlc2NyaXB0L2JvYXJkX2hhbmRsaW5nL2JvYXJkX3N0YXRlLnRzIiwid2VicGFjazovLy8uL3R5cGVzY3JpcHQvYm9hcmRfaGFuZGxpbmcvY2FudmFzLnRzIiwid2VicGFjazovLy8uL3R5cGVzY3JpcHQvbG9naWMvZ2VvbWV0cnkudHMiLCJ3ZWJwYWNrOi8vLy4vdHlwZXNjcmlwdC9uZXR3b3JrL25ldHdvcmsudHMiLCJ3ZWJwYWNrOi8vLy4vcmVzb3VyY2VzL3NvY2tqcy5qcyIsIndlYnBhY2s6Ly8vKHdlYnBhY2spL2J1aWxkaW4vZ2xvYmFsLmpzIiwid2VicGFjazovLy8uL3Jlc291cmNlcy9zdG9tcC5qcyIsIndlYnBhY2s6Ly8vLi90eXBlc2NyaXB0L25ldHdvcmsvb2ZmbGluZS50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiO0FBQUE7QUFDQTs7QUFFQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7O0FBRUE7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7OztBQUdBO0FBQ0E7O0FBRUE7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLGFBQUs7QUFDTDtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBLG1DQUEyQiwwQkFBMEIsRUFBRTtBQUN2RCx5Q0FBaUMsZUFBZTtBQUNoRDtBQUNBO0FBQ0E7O0FBRUE7QUFDQSw4REFBc0QsK0RBQStEOztBQUVySDtBQUNBOztBQUVBO0FBQ0E7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7QUM3REE7QUFBQTs7R0FFRztBQUdILHlHQUF5RztBQUN4QztBQUNGO0FBQ1A7QUFHSjtBQUVwRCwwRUFBMEU7QUFDbkUsSUFBSSxLQUFLLEdBQVcsQ0FBQyxDQUFDLENBQUMsa0RBQWtEO0FBQ2hGLDhFQUE4RTtBQUN2RSxNQUFNLE9BQU8sR0FBRyxHQUFHLENBQUM7QUFBQTtBQUFBO0FBQzNCLGtEQUFrRDtBQUMzQyxNQUFNLGtCQUFrQixHQUFHLEVBQUUsQ0FBQztBQUFBO0FBQUE7QUFDckMsMkNBQTJDO0FBQ3BDLElBQUksYUFBdUMsQ0FBQztBQUNuRCw2QkFBNkI7QUFDN0IsSUFBSSxTQUFTLENBQUM7QUFDZCx3REFBd0Q7QUFDeEQsSUFBSSxhQUE0QixDQUFDO0FBQ2pDLG9CQUFvQjtBQUNwQixJQUFJLEVBQVUsQ0FBQztBQUNmLCtCQUErQjtBQUMvQixJQUFJLE1BQXNCLENBQUM7QUFDM0IsZ0NBQWdDO0FBQ2hDLElBQUksS0FBcUIsQ0FBQztBQUMxQixpRUFBaUU7QUFDakUsUUFBUSxDQUFDLGdCQUFnQixDQUFFLGtCQUFrQixFQUFFLElBQUksRUFBRSxLQUFLLENBQUUsQ0FBQztBQUU3RDs7R0FFRztBQUNIO0lBQ0ksdUNBQXVDO0lBQ3ZDLE1BQU0sR0FBRyxRQUFRLENBQUMsY0FBYyxDQUFFLGFBQWEsQ0FBb0IsQ0FBQztJQUNwRSxLQUFLLEdBQUcsUUFBUSxDQUFDLGNBQWMsQ0FBRSxPQUFPLENBQW9CLENBQUM7SUFDN0QsSUFBSSxDQUFDO1FBQ0QsU0FBUyxHQUFHLElBQUksMEVBQWdCLEVBQUUsQ0FBQztJQUN2QyxDQUFDO0lBQUMsS0FBSyxDQUFDLENBQUUsQ0FBRSxDQUFDLENBQUMsQ0FBQztRQUNYLFNBQVMsR0FBRyxJQUFJLHdFQUFjLEVBQUUsQ0FBQztJQUNyQyxDQUFDO0lBQ0QsU0FBUyxDQUFDLElBQUksQ0FBRSxPQUFPLENBQUMsRUFBRSxHQUFHLEVBQUUsQ0FBQyxDQUFFLE9BQVEsQ0FBQztRQUFDLFdBQVcsRUFBRSxDQUFDLENBQUMsQ0FBQyxDQUFFLENBQUM7QUFDbkUsQ0FBQztBQUVELDBCQUEwQjtBQUMxQjtJQUNJLDBCQUEwQjtJQUMxQixhQUFhLEdBQUcsSUFBSSxxRkFBYSxDQUFFLFNBQVMsQ0FBRSxDQUFDO0lBQy9DLElBQUksS0FBSyxHQUFHLFFBQVEsQ0FBQyxjQUFjLENBQUUsT0FBTyxDQUF1QixDQUFDO0lBQ3BFLGFBQWEsQ0FBQyxJQUFJLENBQUUsS0FBSyxDQUFFLENBQUM7SUFDNUIsRUFBRSxDQUFDLENBQUUsS0FBTSxDQUFDLENBQUMsQ0FBQztRQUNWLGlCQUFpQixFQUFFLENBQUM7UUFDcEIsaUJBQWlCLEVBQUUsQ0FBQztJQUN4QixDQUFDO0FBQ0wsQ0FBQztBQUVEOztHQUVHO0FBQ0g7SUFDSSxJQUFJLFdBQVcsR0FBRyxRQUFRLENBQUMsY0FBYyxDQUFFLE9BQU8sQ0FBRSxDQUFDO0lBQ3JELFdBQVcsQ0FBQyxnQkFBZ0IsQ0FBRSxPQUFPLEVBQUUsQ0FBRSxDQUFDLEVBQUcsRUFBRTtRQUMzQyxJQUFJLGFBQWEsR0FBbUIsSUFBSSxLQUFLLEVBQUUsQ0FBQztRQUNoRCxHQUFHLENBQUMsQ0FBRSxJQUFJLFFBQVEsSUFBSSxhQUFhLENBQUMsS0FBSyxDQUFDLGVBQWUsQ0FBQyxNQUFNLENBQUUsYUFBYSxDQUFDLEtBQUssQ0FBQyxjQUFjLENBQUcsQ0FBQyxDQUFDLENBQUM7WUFDdEcsYUFBYSxDQUFDLElBQUksQ0FBRSw0RUFBWSxDQUFDLEVBQUUsQ0FBRSxRQUFRLENBQUUsQ0FBRSxDQUFDO1FBQ3RELENBQUM7UUFDRCxJQUFJLEdBQUcsR0FBUyxJQUFJLElBQUksRUFBRSxDQUFDO1FBQzNCLFdBQVcsQ0FBQyxZQUFZLENBQUUsVUFBVSxFQUFFLG1CQUFtQixHQUFHLEdBQUcsQ0FBQyxXQUFXLEVBQUUsR0FBRyxHQUFHLENBQUMsUUFBUSxFQUFFLEdBQUcsR0FBRyxDQUFDLE9BQU8sRUFBRSxHQUFHLEdBQUcsQ0FBQyxRQUFRLEVBQUUsR0FBRyxHQUFHLENBQUMsVUFBVSxFQUFFLEdBQUcsR0FBRyxDQUFDLFVBQVUsRUFBRSxHQUFHLE9BQU8sQ0FBRSxDQUFDO1FBQ2xMLFdBQVcsQ0FBQyxZQUFZLENBQUUsTUFBTSxFQUFFLGtEQUFrRCxHQUFHLElBQUksQ0FBQyxTQUFTLENBQUUsYUFBYSxDQUFFLEdBQUcsR0FBRyxDQUFFLENBQUM7SUFDbkksQ0FBQyxDQUFFLENBQUM7SUFDSixXQUFXLENBQUMsS0FBSyxDQUFDLE9BQU8sR0FBRyxPQUFPLENBQUM7QUFDeEMsQ0FBQztBQUVEOzs7R0FHRztBQUNIO0lBQ0ksSUFBSSxXQUFXLEdBQUcsUUFBUSxDQUFDLGNBQWMsQ0FBRSxVQUFVLENBQUUsQ0FBQztJQUN4RCxXQUFXLENBQUMsZ0JBQWdCLENBQUUsT0FBTyxFQUFFLENBQUUsQ0FBQyxFQUFHLEVBQUUsQ0FBQyxTQUFTLENBQUMsT0FBTyxFQUFFLENBQUUsQ0FBQztJQUN0RSxXQUFXLENBQUMsS0FBSyxDQUFDLE9BQU8sR0FBRyxPQUFPLENBQUM7QUFDeEMsQ0FBQztBQUVEOzs7O0dBSUc7QUFDRywyQkFBNkIsUUFBMEIsRUFBRSxLQUF1QjtJQUNsRixFQUFFLENBQUMsQ0FBRSxLQUFNLENBQUM7UUFBQyxLQUFLLENBQUMsT0FBTyxDQUFFLENBQUMsQ0FBQyxFQUFFLENBQUMsS0FBSyxDQUFDLFNBQVMsR0FBRyxNQUFNLENBQUUsQ0FBQyxLQUFLLENBQUMsU0FBUyxHQUFHLENBQUMsQ0FBQyxTQUFTLENBQUUsQ0FBRSxDQUFDO0lBQzlGLGFBQWEsQ0FBQyxpQkFBaUIsQ0FBRSxRQUFRLEVBQUUsS0FBSyxDQUFFLENBQUM7QUFDdkQsQ0FBQztBQUVEOzs7R0FHRztBQUNHLDBCQUE0QixRQUEwQjtJQUN4RCxhQUFhLENBQUMsZ0JBQWdCLENBQUUsUUFBUSxDQUFFLENBQUM7QUFDL0MsQ0FBQztBQUVEOzs7O0dBSUc7QUFDRyw2QkFBK0IsUUFBZ0IsRUFBRSxRQUEwQjtJQUM3RSxFQUFFLENBQUMsQ0FBRSxNQUFPLENBQUM7UUFBQyxNQUFNLENBQUMsU0FBUyxHQUFHLFFBQVEsQ0FBQztJQUMxQyxhQUFhLENBQUMsUUFBUSxDQUFFLFFBQVEsRUFBRSxRQUFRLENBQUUsQ0FBQztBQUNqRCxDQUFDO0FBRUQ7O0dBRUc7QUFDRztJQUNGLGFBQWEsQ0FBQyxNQUFNLEVBQUUsQ0FBQztBQUMzQixDQUFDO0FBRUQ7O0dBRUc7QUFDRyxvQkFBc0IsT0FBZTtJQUN2QyxFQUFFLENBQUMsQ0FBRSxLQUFNLENBQUM7UUFBQyxLQUFLLENBQUMsU0FBUyxHQUFHLE1BQU0sQ0FBRSxPQUFPLENBQUUsQ0FBQztJQUNqRCxhQUFhLENBQUMsVUFBVSxDQUFFLE9BQU8sQ0FBRSxDQUFDO0FBQ3hDLENBQUM7Ozs7Ozs7Ozs7QUNqSUQ7QUFBQTs7R0FFRztBQUd1RTtBQUN2QixDQUFDLG1EQUFtRDtBQUt2Rzs7Ozs7O0dBTUc7QUFDSCxJQUFLLFdBRUo7QUFGRCxXQUFLLFdBQVc7SUFDWix1REFBUztJQUFFLHVFQUFpQjtJQUFFLHFEQUFRO0FBQzFDLENBQUMsRUFGSSxXQUFXLEtBQVgsV0FBVyxRQUVmO0FBRUQsaURBQWlEO0FBQ2pELElBQUksaUJBQWlCLEdBQWdCLFdBQVcsQ0FBQyxTQUFTLENBQUM7QUFFckQ7SUFPRixZQUFhLENBQVMsRUFBRSxDQUFTO1FBcUJqQyxhQUFRLEdBQUcsR0FBVyxFQUFFO1lBQ3BCLE1BQU0sQ0FBQyxJQUFJLENBQUMsQ0FBQyxHQUFHLEdBQUcsR0FBRyxJQUFJLENBQUMsQ0FBQyxDQUFDO1FBQ2pDLENBQUM7UUF0QkcsSUFBSSxDQUFDLEVBQUUsR0FBRyxDQUFDLENBQUM7UUFDWixJQUFJLENBQUMsRUFBRSxHQUFHLENBQUMsQ0FBQztJQUNoQixDQUFDO0lBTkQsSUFBSSxDQUFDLEtBQUssTUFBTSxDQUFDLElBQUksQ0FBQyxFQUFFLENBQUMsQ0FBQyxDQUFDO0lBQzNCLElBQUksQ0FBQyxLQUFLLE1BQU0sQ0FBQyxJQUFJLENBQUMsRUFBRSxDQUFDLENBQUMsQ0FBQztJQU8zQixjQUFjLENBQUUsTUFBYztRQUMxQixNQUFNLENBQUMsSUFBSSxNQUFNLENBQUUsSUFBSSxDQUFDLENBQUMsR0FBRyxNQUFNLENBQUMsQ0FBQyxFQUFFLElBQUksQ0FBQyxDQUFDLEdBQUcsTUFBTSxDQUFDLENBQUMsQ0FBRSxDQUFDO0lBQzlELENBQUM7SUFBQSxDQUFDO0lBRUYsYUFBYSxDQUFFLE1BQWM7UUFDekIsTUFBTSxDQUFDLENBQUUsSUFBSSxDQUFDLENBQUMsR0FBRyxNQUFNLENBQUMsQ0FBQyxDQUFFLEdBQUcsQ0FBRSxJQUFJLENBQUMsQ0FBQyxHQUFHLE1BQU0sQ0FBQyxDQUFDLENBQUUsQ0FBQztJQUN6RCxDQUFDO0lBQUEsQ0FBQztJQUVGLGFBQWE7UUFDVCxNQUFNLENBQUMsSUFBSSxNQUFNLENBQUUsQ0FBQyxJQUFJLENBQUMsQ0FBQyxFQUFFLENBQUMsSUFBSSxDQUFDLENBQUMsQ0FBRSxDQUFDO0lBQzFDLENBQUM7SUFBQSxDQUFDO0lBRUYsa0JBQWtCLENBQUUsTUFBYztRQUM5QixNQUFNLENBQUMsSUFBSSxDQUFDLEdBQUcsQ0FBRSxJQUFJLENBQUMsQ0FBQyxHQUFHLE1BQU0sQ0FBQyxDQUFDLEVBQUUsQ0FBQyxDQUFFLEdBQUcsSUFBSSxDQUFDLEdBQUcsQ0FBRSxJQUFJLENBQUMsQ0FBQyxHQUFHLE1BQU0sQ0FBQyxDQUFDLEVBQUUsQ0FBQyxDQUFFLENBQUM7SUFDL0UsQ0FBQztJQUFBLENBQUM7SUFNRixNQUFNLENBQUUsTUFBYztRQUNsQixFQUFFLENBQUMsQ0FBRSxDQUFDLE1BQU8sQ0FBQztZQUFDLE1BQU0sQ0FBQyxLQUFLLENBQUM7UUFDNUIsRUFBRSxDQUFDLENBQUUsQ0FBQyxDQUFFLE1BQU0sWUFBWSxNQUFNLENBQUcsQ0FBQztZQUFDLE1BQU0sQ0FBQyxLQUFLLENBQUM7UUFDbEQsSUFBSTtZQUFDLE1BQU0sQ0FBQyxJQUFJLENBQUMsQ0FBQyxJQUFJLE1BQU0sQ0FBQyxDQUFDLElBQUksSUFBSSxDQUFDLENBQUMsSUFBSSxNQUFNLENBQUMsQ0FBQyxDQUFDO0lBQ3pELENBQUM7SUFFRCxTQUFTLENBQUUsYUFBcUI7UUFDNUIsSUFBSSxLQUFLLEdBQUcsSUFBSSxDQUFDLENBQUMsR0FBRyxhQUFhLENBQUMsQ0FBQyxDQUFDO1FBQ3JDLEVBQUUsQ0FBQyxDQUFFLEtBQUssSUFBSSxDQUFFLENBQUM7WUFBQyxNQUFNLENBQUMsS0FBSyxDQUFDO1FBQy9CLElBQUk7WUFBQyxNQUFNLENBQUMsSUFBSSxDQUFDLENBQUMsR0FBRyxhQUFhLENBQUMsQ0FBQyxDQUFDO0lBQ3pDLENBQUM7Q0FDSjtBQUFBO0FBQUE7QUFFSztJQTJCRixZQUFhLFdBQW1CLEVBQUUsUUFBYyxFQUFFLFFBQWMsRUFBRSxRQUFlO1FBOEtqRixhQUFRLEdBQUcsR0FBVyxFQUFFO1lBQ3BCLElBQUksSUFBSSxHQUFHLENBQUMsSUFBSSxDQUFDLFFBQVEsQ0FBQyxDQUFDLENBQUMsTUFBTSxDQUFDLENBQUMsQ0FBQyxJQUFJLENBQUMsUUFBUSxDQUFDLFdBQVcsQ0FBQztZQUMvRCxJQUFJLElBQUksR0FBRyxDQUFDLElBQUksQ0FBQyxRQUFRLENBQUMsQ0FBQyxDQUFDLE1BQU0sQ0FBQyxDQUFDLENBQUMsSUFBSSxDQUFDLFFBQVEsQ0FBQyxXQUFXLENBQUM7WUFDL0QsSUFBSSxJQUFJLEdBQUcsQ0FBQyxJQUFJLENBQUMsU0FBUyxDQUFDLENBQUMsQ0FBQyxNQUFNLENBQUMsQ0FBQyxDQUFDLElBQUksQ0FBQyxTQUFTLENBQUMsUUFBUSxDQUFDLFdBQVcsR0FBRyxPQUFPLEdBQUcsSUFBSSxDQUFDLFdBQVcsR0FBRyxPQUFPLEdBQUcsSUFBSSxDQUFDLFNBQVMsQ0FBQyxRQUFRLENBQUMsV0FBVyxDQUFDO1lBQ3ZKLEVBQUUsQ0FBQyxDQUFFLGdEQUFLLEdBQUcsQ0FBRSxDQUFDO2dCQUFDLE1BQU0sQ0FBQyxJQUFJLEdBQUcsT0FBTyxHQUFHLElBQUksQ0FBQyxXQUFXLEdBQUcsT0FBTyxHQUFHLElBQUksR0FBRyxVQUFVLEdBQUcsSUFBSSxDQUFDO1lBQy9GLElBQUk7Z0JBQUMsTUFBTSxDQUFDLElBQUksR0FBRyxPQUFPLEdBQUcsSUFBSSxDQUFDLFdBQVcsR0FBRyxPQUFPLEdBQUcsSUFBSSxDQUFDO1FBQ25FLENBQUM7UUFuTEcsSUFBSSxDQUFDLFlBQVksR0FBRyxXQUFXLENBQUM7UUFDaEMsSUFBSSxDQUFDLFNBQVMsR0FBRyxRQUFRO1FBQ3pCLElBQUksQ0FBQyxTQUFTLEdBQUcsUUFBUSxDQUFDO1FBQzFCLElBQUksQ0FBQyxTQUFTLEdBQUcsUUFBUSxDQUFDO1FBQzFCLElBQUksQ0FBQyxnQkFBZ0I7SUFDekIsQ0FBQztJQTFCRCxJQUFJLFdBQVcsS0FBYSxNQUFNLENBQUMsSUFBSSxDQUFDLFlBQVksQ0FBQyxDQUFDLENBQUM7SUFDdkQsSUFBSSxXQUFXLENBQUUsV0FBbUIsSUFBSyxJQUFJLENBQUMsWUFBWSxHQUFHLFdBQVcsQ0FBQyxDQUFDLENBQUM7SUFDM0UsSUFBSSxRQUFRLEtBQVcsTUFBTSxDQUFDLElBQUksQ0FBQyxTQUFTLENBQUMsQ0FBQyxDQUFDO0lBQy9DLElBQUksUUFBUSxDQUFFLFFBQWMsSUFBSyxJQUFJLENBQUMsU0FBUyxHQUFHLFFBQVEsQ0FBQyxDQUFDLENBQUM7SUFDN0QsSUFBSSxRQUFRLEtBQVcsTUFBTSxDQUFDLElBQUksQ0FBQyxTQUFTLENBQUMsQ0FBQyxDQUFDO0lBQy9DLElBQUksUUFBUSxDQUFFLFFBQWMsSUFBSyxJQUFJLENBQUMsU0FBUyxHQUFHLFFBQVEsQ0FBQyxDQUFDLENBQUM7SUFDN0QsMElBQTBJO0lBQzFJLHFEQUFxRDtJQUNyRCw4Q0FBOEM7SUFDOUMsa0NBQWtDO0lBQ2xDLG9DQUFvQztJQUNwQyxvQ0FBb0M7SUFDcEMsT0FBTztJQUNQLElBQUksZUFBZSxLQUFlLE1BQU0sQ0FBQyxJQUFJLENBQUMsZ0JBQWdCLENBQUMsQ0FBQyxDQUFDO0lBQ2pFLElBQUksZUFBZSxDQUFFLGVBQXlCO1FBQzFDLElBQUksQ0FBQyxnQkFBZ0IsR0FBRyxlQUFlLENBQUM7UUFDeEMsRUFBRSxDQUFDLENBQUUsQ0FBQyxJQUFJLENBQUMsU0FBUyxDQUFDLGVBQWdCLENBQUM7WUFBQyxJQUFJLENBQUMsU0FBUyxDQUFDLGVBQWUsR0FBRyxlQUFlLENBQUM7UUFDeEYsRUFBRSxDQUFDLENBQUUsQ0FBQyxJQUFJLENBQUMsU0FBUyxDQUFDLGVBQWdCLENBQUM7WUFBQyxJQUFJLENBQUMsU0FBUyxDQUFDLGVBQWUsR0FBRyxlQUFlLENBQUM7SUFDNUYsQ0FBQztJQVdEOzs7O09BSUc7SUFDSCxXQUFXLENBQUUsTUFBWTtRQUNyQixFQUFFLENBQUMsQ0FBRSxDQUFDLE1BQU0sSUFBSSxNQUFNLENBQUMsU0FBVSxDQUFDO1lBQUMsTUFBTSxDQUFDLEtBQUssQ0FBQztRQUNoRCxFQUFFLENBQUMsQ0FBRSxDQUFDLElBQUksQ0FBQyxTQUFTLElBQUksSUFBSSxDQUFDLGVBQWUsQ0FBRSxNQUFNLENBQUcsQ0FBQyxDQUFDLENBQUM7WUFDdEQsSUFBSSxDQUFDLFNBQVMsR0FBRyxNQUFNLENBQUM7WUFDeEIsTUFBTSxDQUFDLFNBQVMsR0FBRyxJQUFJLENBQUM7WUFDeEIsTUFBTSxDQUFDLElBQUksQ0FBQztRQUNoQixDQUFDO1FBQUMsSUFBSTtZQUFDLE1BQU0sQ0FBQyxLQUFLLENBQUM7SUFDeEIsQ0FBQztJQUVELFdBQVc7UUFDUCxNQUFNLENBQUMsSUFBSSxDQUFDLFNBQVMsQ0FBQztJQUMxQixDQUFDO0lBRUQ7Ozs7O09BS0c7SUFDSCxNQUFNLENBQUMsZUFBZSxDQUFFLFNBQWU7UUFDbkMsSUFBSSxZQUFZLEdBQVMsU0FBUyxDQUFDLFFBQVEsQ0FBQyxTQUFTLENBQUM7UUFDdEQsRUFBRSxDQUFDLENBQUUsWUFBWSxJQUFJLElBQUssQ0FBQztZQUN2QixZQUFZLEdBQUcsSUFBSSxJQUFJLENBQUUsU0FBUyxDQUFDLFdBQVcsRUFBRSxJQUFJLEVBQUUsSUFBSSxFQUFFLElBQUksQ0FBRSxDQUFDO1FBQ3ZFLE1BQU0sQ0FBQyxJQUFJLElBQUksQ0FBRSxTQUFTLENBQUMsUUFBUSxDQUFDLFdBQVcsRUFBRSxTQUFTLENBQUMsUUFBUSxDQUFDLFNBQVMsRUFBRSxZQUFZLEVBQUUsU0FBUyxDQUFFLENBQUM7SUFDN0csQ0FBQztJQUVELGVBQWUsQ0FBRSxJQUFVO1FBQ3ZCLEVBQUUsQ0FBQyxDQUFFLENBQUMsSUFBSyxDQUFDO1lBQUMsTUFBTSxDQUFDLEtBQUssQ0FBQztRQUMxQixJQUFJO1lBQUMsTUFBTSxDQUFDLElBQUksQ0FBQyxXQUFXLENBQUMsTUFBTSxDQUFFLElBQUksQ0FBQyxRQUFRLENBQUMsV0FBVyxDQUFFLElBQUksSUFBSSxDQUFDLFFBQVEsQ0FBQyxXQUFXLENBQUMsTUFBTSxDQUFFLElBQUksQ0FBQyxXQUFXLENBQUUsQ0FBQztJQUM3SCxDQUFDO0lBRUQsSUFBSTtRQUNBLEVBQUUsQ0FBQyxDQUFFLElBQUksQ0FBQyxTQUFVLENBQUM7WUFBQyxJQUFJLENBQUMsU0FBUyxDQUFDLFNBQVMsR0FBRyxJQUFJLENBQUM7UUFDdEQsRUFBRSxDQUFDLENBQUUsSUFBSSxDQUFDLFFBQVMsQ0FBQztZQUNoQixJQUFJLENBQUMsUUFBUSxDQUFDLFFBQVEsR0FBRyxJQUFJLENBQUM7UUFDbEMsRUFBRSxDQUFDLENBQUUsSUFBSSxDQUFDLFFBQVMsQ0FBQztZQUNoQixJQUFJLENBQUMsUUFBUSxDQUFDLFFBQVEsR0FBRyxJQUFJLENBQUM7SUFDdEMsQ0FBQztJQUVELGtCQUFrQixDQUFFLE1BQWM7UUFDOUIseURBQXlEO1FBQ3pELElBQUksRUFBRSxHQUFHLElBQUksQ0FBQyxXQUFXLENBQUMsQ0FBQyxDQUFDO1FBQzVCLElBQUksRUFBRSxHQUFHLElBQUksQ0FBQyxXQUFXLENBQUMsQ0FBQyxDQUFDO1FBQzVCLElBQUksRUFBRSxHQUFHLElBQUksQ0FBQyxRQUFRLENBQUMsV0FBVyxDQUFDLENBQUMsQ0FBQztRQUNyQyxJQUFJLEVBQUUsR0FBRyxJQUFJLENBQUMsUUFBUSxDQUFDLFdBQVcsQ0FBQyxDQUFDLENBQUM7UUFDckMsSUFBSSxDQUFDLEdBQUcsTUFBTSxDQUFDLENBQUMsR0FBRyxFQUFFLENBQUM7UUFDdEIsSUFBSSxDQUFDLEdBQUcsTUFBTSxDQUFDLENBQUMsR0FBRyxFQUFFLENBQUM7UUFDdEIsSUFBSSxDQUFDLEdBQUcsRUFBRSxHQUFHLEVBQUUsQ0FBQztRQUNoQixJQUFJLENBQUMsR0FBRyxFQUFFLEdBQUcsRUFBRSxDQUFDO1FBRWhCLElBQUksR0FBRyxHQUFHLENBQUMsR0FBRyxDQUFDLEdBQUcsQ0FBQyxHQUFHLENBQUMsQ0FBQztRQUN4QixJQUFJLE1BQU0sR0FBRyxDQUFDLEdBQUcsQ0FBQyxHQUFHLENBQUMsR0FBRyxDQUFDLENBQUM7UUFDM0IsSUFBSSxLQUFLLEdBQUcsQ0FBQyxDQUFDLENBQUM7UUFDZixFQUFFLENBQUMsQ0FBRSxNQUFNLElBQUksQ0FBRSxDQUFDO1lBQ2QsS0FBSyxHQUFHLEdBQUcsR0FBRyxNQUFNLENBQUM7UUFFekIsSUFBSSxFQUFFLEVBQUUsRUFBRSxDQUFDO1FBRVgsRUFBRSxDQUFDLENBQUUsS0FBSyxHQUFHLENBQUUsQ0FBQyxDQUFDLENBQUM7WUFDZCxFQUFFLEdBQUcsRUFBRSxDQUFDO1lBQ1IsRUFBRSxHQUFHLEVBQUUsQ0FBQztRQUNaLENBQUM7UUFDRCxJQUFJLENBQUMsRUFBRSxDQUFDLENBQUUsS0FBSyxHQUFHLENBQUUsQ0FBQyxDQUFDLENBQUM7WUFDbkIsRUFBRSxHQUFHLEVBQUUsQ0FBQztZQUNSLEVBQUUsR0FBRyxFQUFFLENBQUM7UUFDWixDQUFDO1FBQ0QsSUFBSSxDQUFDLENBQUM7WUFDRixFQUFFLEdBQUcsRUFBRSxHQUFHLEtBQUssR0FBRyxDQUFDLENBQUM7WUFDcEIsRUFBRSxHQUFHLEVBQUUsR0FBRyxLQUFLLEdBQUcsQ0FBQyxDQUFDO1FBQ3hCLENBQUM7UUFFRCxJQUFJLEVBQUUsR0FBRyxNQUFNLENBQUMsQ0FBQyxHQUFHLEVBQUUsQ0FBQztRQUN2QixJQUFJLEVBQUUsR0FBRyxNQUFNLENBQUMsQ0FBQyxHQUFHLEVBQUUsQ0FBQztRQUN2QixNQUFNLENBQUMsRUFBRSxHQUFHLEVBQUUsR0FBRyxFQUFFLEdBQUcsRUFBRSxDQUFDO0lBQzdCLENBQUM7SUFBQSxDQUFDO0lBRUYsaUJBQWlCLENBQUUsSUFBVTtRQUN6Qiw4REFBOEQ7UUFDOUQsRUFBRSxDQUFDLENBQUUsSUFBSSxDQUFDLGNBQWMsQ0FBRSxJQUFJLENBQUcsQ0FBQztZQUFDLE1BQU0sQ0FBQyxDQUFDLENBQUM7UUFDNUMsSUFBSSxFQUFFLEdBQUcsSUFBSSxDQUFDLGtCQUFrQixDQUFFLElBQUksQ0FBQyxXQUFXLENBQUUsQ0FBQztRQUNyRCxJQUFJLEVBQUUsR0FBRyxJQUFJLENBQUMsa0JBQWtCLENBQUUsSUFBSSxDQUFDLFFBQVEsQ0FBQyxXQUFXLENBQUUsQ0FBQztRQUM5RCxJQUFJLEVBQUUsR0FBRyxJQUFJLENBQUMsa0JBQWtCLENBQUUsSUFBSSxDQUFDLFdBQVcsQ0FBRSxDQUFDO1FBQ3JELElBQUksRUFBRSxHQUFHLElBQUksQ0FBQyxrQkFBa0IsQ0FBRSxJQUFJLENBQUMsUUFBUSxDQUFDLFdBQVcsQ0FBRSxDQUFDO1FBQzlELE1BQU0sQ0FBQyxpRUFBRyxDQUFFLEVBQUUsRUFBRSxFQUFFLEVBQUUsRUFBRSxFQUFFLEVBQUUsQ0FBRSxDQUFDO0lBQ2pDLENBQUM7SUFBQSxDQUFDO0lBRUY7Ozs7OztPQU1HO0lBQ0gsUUFBUSxDQUFFLE9BQWE7UUFDbkIsRUFBRSxDQUFDLENBQUUsQ0FBQyxPQUFRLENBQUM7WUFBQyxNQUFNLENBQUMsSUFBSSxDQUFDO1FBQzVCLElBQUksQ0FBQyxFQUFFLENBQUMsQ0FBRSxJQUFJLENBQUMsV0FBVyxDQUFDLE1BQU0sQ0FBRSxPQUFPLENBQUMsV0FBVyxDQUFHLENBQUM7WUFBQyxNQUFNLENBQUMsSUFBSSxDQUFDLFdBQVcsQ0FBQztRQUNuRixJQUFJLENBQUMsRUFBRSxDQUFDLENBQUUsSUFBSSxDQUFDLFdBQVcsQ0FBQyxNQUFNLENBQUUsT0FBTyxDQUFDLFFBQVEsQ0FBQyxXQUFXLENBQUcsQ0FBQztZQUFDLE1BQU0sQ0FBQyxJQUFJLENBQUMsV0FBVyxDQUFDO1FBQzVGLElBQUksQ0FBQyxFQUFFLENBQUMsQ0FBRSxJQUFJLENBQUMsUUFBUSxDQUFDLFdBQVcsQ0FBQyxNQUFNLENBQUUsT0FBTyxDQUFDLFdBQVcsQ0FBRyxDQUFDO1lBQUMsTUFBTSxDQUFDLElBQUksQ0FBQyxRQUFRLENBQUMsV0FBVyxDQUFDO1FBQ3JHLElBQUksQ0FBQyxFQUFFLENBQUMsQ0FBRSxJQUFJLENBQUMsUUFBUSxDQUFDLFdBQVcsQ0FBQyxNQUFNLENBQUUsT0FBTyxDQUFDLFFBQVEsQ0FBQyxXQUFXLENBQUcsQ0FBQztZQUFDLE1BQU0sQ0FBQyxJQUFJLENBQUMsUUFBUSxDQUFDLFdBQVcsQ0FBQztRQUM5RyxJQUFJO1lBQUMsTUFBTSxDQUFDLElBQUksQ0FBQztJQUNyQixDQUFDO0lBRUQsY0FBYyxDQUFFLElBQVUsRUFBRSxTQUFrQjtRQUMxQyxFQUFFLENBQUMsQ0FBRSxDQUFDLFNBQVUsQ0FBQyxDQUFDLENBQUM7WUFDZixFQUFFLENBQUMsQ0FBRSxJQUFJLENBQUMsUUFBUSxDQUFFLElBQUksQ0FBRyxDQUFDO2dCQUFDLE1BQU0sQ0FBQyxLQUFLLENBQUM7WUFDMUMsSUFBSSxFQUFFLEdBQWdCLFFBQVEsQ0FBQyxjQUFjLENBQUUsSUFBSSxDQUFDLFdBQVcsRUFBRSxJQUFJLENBQUMsUUFBUSxDQUFDLFdBQVcsRUFBRSxJQUFJLENBQUMsV0FBVyxDQUFFLENBQUM7WUFDL0csSUFBSSxFQUFFLEdBQWdCLFFBQVEsQ0FBQyxjQUFjLENBQUUsSUFBSSxDQUFDLFdBQVcsRUFBRSxJQUFJLENBQUMsUUFBUSxDQUFDLFdBQVcsRUFBRSxJQUFJLENBQUMsUUFBUSxDQUFDLFdBQVcsQ0FBRSxDQUFDO1lBQ3hILElBQUksRUFBRSxHQUFnQixRQUFRLENBQUMsY0FBYyxDQUFFLElBQUksQ0FBQyxXQUFXLEVBQUUsSUFBSSxDQUFDLFFBQVEsQ0FBQyxXQUFXLEVBQUUsSUFBSSxDQUFDLFdBQVcsQ0FBRSxDQUFDO1lBQy9HLElBQUksRUFBRSxHQUFnQixRQUFRLENBQUMsY0FBYyxDQUFFLElBQUksQ0FBQyxXQUFXLEVBQUUsSUFBSSxDQUFDLFFBQVEsQ0FBQyxXQUFXLEVBQUUsSUFBSSxDQUFDLFFBQVEsQ0FBQyxXQUFXLENBQUUsQ0FBQztZQUN4SCxlQUFlO1lBQ2YsRUFBRSxDQUFDLENBQUUsRUFBRSxJQUFJLEVBQUUsSUFBSSxFQUFFLElBQUksRUFBRyxDQUFDO2dCQUFDLE1BQU0sQ0FBQyxJQUFJLENBQUM7WUFDeEMsSUFBSTtnQkFBQyxNQUFNLENBQUMsS0FBSyxDQUFDO1lBQ2xCLHdCQUF3QjtRQUM1QixDQUFDO1FBQ0QsSUFBSTtZQUFDLE1BQU0sQ0FBQyxJQUFJLENBQUMsaUJBQWlCLENBQUUsSUFBSSxDQUFFLEdBQUcsU0FBUyxDQUFDO0lBQzNELENBQUM7SUFFRDs7OztPQUlHO0lBQ0gsMEJBQTBCLENBQUUsS0FBVyxFQUFFLEtBQVc7UUFDaEQsSUFBSSxJQUFJLEdBQVcsS0FBSyxDQUFDLFdBQVcsQ0FBQyxDQUFDLENBQUM7UUFDdkMsSUFBSSxJQUFJLEdBQVcsS0FBSyxDQUFDLFdBQVcsQ0FBQyxDQUFDLENBQUM7UUFDdkMsSUFBSSxJQUFJLEdBQVcsS0FBSyxDQUFDLFFBQVEsQ0FBQyxXQUFXLENBQUMsQ0FBQyxDQUFDO1FBQ2hELElBQUksSUFBSSxHQUFXLEtBQUssQ0FBQyxRQUFRLENBQUMsV0FBVyxDQUFDLENBQUMsQ0FBQztRQUNoRCxJQUFJLElBQUksR0FBVyxLQUFLLENBQUMsV0FBVyxDQUFDLENBQUMsQ0FBQztRQUN2QyxJQUFJLElBQUksR0FBVyxLQUFLLENBQUMsV0FBVyxDQUFDLENBQUMsQ0FBQztRQUN2QyxJQUFJLElBQUksR0FBVyxLQUFLLENBQUMsUUFBUSxDQUFDLFdBQVcsQ0FBQyxDQUFDLENBQUM7UUFDaEQsSUFBSSxJQUFJLEdBQVcsS0FBSyxDQUFDLFFBQVEsQ0FBQyxXQUFXLENBQUMsQ0FBQyxDQUFDO1FBRWhELElBQUksSUFBSSxHQUFXLElBQUksR0FBRyxJQUFJLENBQUM7UUFDL0IsSUFBSSxJQUFJLEdBQVcsSUFBSSxHQUFHLElBQUksQ0FBQztRQUMvQixJQUFJLElBQUksR0FBVyxJQUFJLEdBQUcsSUFBSSxDQUFDO1FBQy9CLElBQUksSUFBSSxHQUFXLElBQUksR0FBRyxJQUFJLENBQUM7UUFDL0Isb0JBQW9CO1FBQ3BCLElBQUksQ0FBQyxHQUFXLENBQUUsQ0FBQyxJQUFJLEdBQUcsQ0FBRSxJQUFJLEdBQUcsSUFBSSxDQUFFLEdBQUcsSUFBSSxHQUFHLENBQUUsSUFBSSxHQUFHLElBQUksQ0FBRSxDQUFFLEdBQUcsQ0FBRSxDQUFDLElBQUksR0FBRyxJQUFJLEdBQUcsSUFBSSxHQUFHLElBQUksQ0FBRSxDQUFDO1FBQ3RHLElBQUksQ0FBQyxHQUFXLENBQUUsSUFBSSxHQUFHLENBQUUsSUFBSSxHQUFHLElBQUksQ0FBRSxHQUFHLElBQUksR0FBRyxDQUFFLElBQUksR0FBRyxJQUFJLENBQUUsQ0FBRSxHQUFHLENBQUUsQ0FBQyxJQUFJLEdBQUcsSUFBSSxHQUFHLElBQUksR0FBRyxJQUFJLENBQUUsQ0FBQztRQUVyRyxFQUFFLENBQUMsQ0FBRSxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBRSxDQUFDLENBQUMsQ0FBQztZQUN6QyxxQkFBcUI7WUFDckIsSUFBSSxHQUFHLEdBQVcsSUFBSSxHQUFHLENBQUUsQ0FBQyxHQUFHLElBQUksQ0FBRSxDQUFDO1lBQ3RDLElBQUksR0FBRyxHQUFXLElBQUksR0FBRyxDQUFFLENBQUMsR0FBRyxJQUFJLENBQUUsQ0FBQztZQUV0QyxNQUFNLENBQUMsSUFBSSxNQUFNLENBQUUsR0FBRyxFQUFFLEdBQUcsQ0FBRSxDQUFDO1FBQ2xDLENBQUM7UUFDRCxNQUFNLENBQUMsSUFBSSxDQUFDO0lBQ2hCLENBQUM7SUFFRDs7OztPQUlHO0lBQ0ksWUFBWTtRQUNmLE1BQU0sQ0FBQyxJQUFJLENBQUMsV0FBVyxDQUFDLGtCQUFrQixDQUFFLElBQUksQ0FBQyxRQUFRLENBQUMsV0FBVyxDQUFFLENBQUM7SUFDNUUsQ0FBQztJQVdELFNBQVMsQ0FBRSxhQUFtQjtRQUMxQixJQUFJLEtBQUssR0FBRyxJQUFJLENBQUMsV0FBVyxDQUFDLFNBQVMsQ0FBRSxhQUFhLENBQUMsV0FBVyxDQUFFLENBQUM7UUFDcEUsRUFBRSxDQUFDLENBQUUsS0FBSyxJQUFJLENBQUUsQ0FBQztZQUFDLE1BQU0sQ0FBQyxLQUFLLENBQUM7UUFDL0IsSUFBSTtZQUFDLE1BQU0sQ0FBQyxJQUFJLENBQUMsUUFBUSxDQUFDLFdBQVcsQ0FBQyxTQUFTLENBQUUsYUFBYSxDQUFDLFNBQVMsQ0FBQyxXQUFXLENBQUUsQ0FBQztJQUMzRixDQUFDO0lBRUQsTUFBTSxDQUFFLElBQVk7UUFDaEIsRUFBRSxDQUFDLENBQUUsQ0FBQyxJQUFLLENBQUM7WUFBQyxNQUFNLENBQUMsS0FBSyxDQUFDO1FBQzFCLEVBQUUsQ0FBQyxDQUFFLENBQUMsQ0FBRSxJQUFJLFlBQVksSUFBSSxDQUFHLENBQUM7WUFBQyxNQUFNLENBQUMsS0FBSyxDQUFDO1FBQzlDLE1BQU0sQ0FBQyxJQUFJLENBQUMsV0FBVyxDQUFDLE1BQU0sQ0FBRSxJQUFJLENBQUMsV0FBVyxDQUFFLElBQUksSUFBSSxDQUFDLFFBQVEsQ0FBQyxXQUFXLENBQUMsTUFBTSxDQUFFLElBQUksQ0FBQyxRQUFRLENBQUMsV0FBVyxDQUFFLENBQUM7SUFDeEgsQ0FBQztDQUNKO0FBQUE7QUFBQTtBQUdLO0lBd0JGLFlBQWEsS0FBVztRQThKeEIsYUFBUSxHQUFHLEdBQVcsRUFBRTtZQUNwQixFQUFFLENBQUMsQ0FBRSxnREFBSyxHQUFHLENBQUUsQ0FBQyxDQUFDLENBQUM7Z0JBQ2QsTUFBTSxDQUFDLGNBQWMsR0FBRyxJQUFJLENBQUMsV0FBVyxHQUFHLGlCQUFpQixHQUFHLElBQUksQ0FBQyxXQUFXLENBQUMsUUFBUSxHQUFHLGdCQUFnQixHQUFHLElBQUksQ0FBQyxXQUFXLENBQUMsUUFBUSxDQUFDO1lBQzVJLENBQUM7WUFBQyxJQUFJO2dCQUFDLE1BQU0sQ0FBQyxJQUFJLENBQUMsV0FBVyxDQUFDLFdBQVcsR0FBRyxHQUFHLEdBQUcsSUFBSSxDQUFDLFdBQVcsQ0FBQyxRQUFRLENBQUMsV0FBVyxHQUFHLEdBQUcsR0FBRyxJQUFJLENBQUMsV0FBVyxDQUFDLFFBQVEsQ0FBQyxXQUFXLENBQUM7UUFDM0ksQ0FBQztRQWpLRyxJQUFJLENBQUMsWUFBWSxHQUFHLEtBQUssQ0FBQztRQUMxQixJQUFJLENBQUMsWUFBWSxDQUFDLGVBQWUsR0FBRyxJQUFJLENBQUM7UUFDekMsSUFBSSxDQUFDLEtBQUssR0FBRyxpRUFBRyxDQUFFLElBQUksQ0FBQyxXQUFXLENBQUMsV0FBVyxDQUFDLENBQUMsRUFBRSxJQUFJLENBQUMsV0FBVyxDQUFDLFFBQVEsQ0FBQyxXQUFXLENBQUMsQ0FBQyxFQUFFLElBQUksQ0FBQyxXQUFXLENBQUMsUUFBUSxDQUFDLFdBQVcsQ0FBQyxDQUFDLENBQUUsQ0FBQztRQUNySSxJQUFJLENBQUMsS0FBSyxHQUFHLGlFQUFHLENBQUUsSUFBSSxDQUFDLFdBQVcsQ0FBQyxXQUFXLENBQUMsQ0FBQyxFQUFFLElBQUksQ0FBQyxXQUFXLENBQUMsUUFBUSxDQUFDLFdBQVcsQ0FBQyxDQUFDLEVBQUUsSUFBSSxDQUFDLFdBQVcsQ0FBQyxRQUFRLENBQUMsV0FBVyxDQUFDLENBQUMsQ0FBRSxDQUFDO1FBQ3JJLElBQUksQ0FBQyxLQUFLLEdBQUcsaUVBQUcsQ0FBRSxJQUFJLENBQUMsV0FBVyxDQUFDLFdBQVcsQ0FBQyxDQUFDLEVBQUUsSUFBSSxDQUFDLFdBQVcsQ0FBQyxRQUFRLENBQUMsV0FBVyxDQUFDLENBQUMsRUFBRSxJQUFJLENBQUMsV0FBVyxDQUFDLFFBQVEsQ0FBQyxXQUFXLENBQUMsQ0FBQyxDQUFFLENBQUM7UUFDckksSUFBSSxDQUFDLEtBQUssR0FBRyxpRUFBRyxDQUFFLElBQUksQ0FBQyxXQUFXLENBQUMsV0FBVyxDQUFDLENBQUMsRUFBRSxJQUFJLENBQUMsV0FBVyxDQUFDLFFBQVEsQ0FBQyxXQUFXLENBQUMsQ0FBQyxFQUFFLElBQUksQ0FBQyxXQUFXLENBQUMsUUFBUSxDQUFDLFdBQVcsQ0FBQyxDQUFDLENBQUUsQ0FBQztRQUNySSw0QkFBNEI7UUFDNUIsSUFBSSxDQUFDLFNBQVMsR0FBRyxDQUFDLElBQUksQ0FBQyxXQUFXLENBQUMsV0FBVyxFQUFFLElBQUksQ0FBQyxXQUFXLENBQUMsUUFBUSxDQUFDLFdBQVcsRUFBRSxJQUFJLENBQUMsV0FBVyxDQUFDLFFBQVEsQ0FBQyxXQUFXLENBQUM7WUFDekgsSUFBSSxDQUFDLENBQUUsRUFBRSxFQUFFLEVBQUUsRUFBRyxFQUFFLENBQUMsRUFBRSxDQUFDLFNBQVMsQ0FBRSxFQUFFLENBQUUsQ0FBOEIsQ0FBQztRQUN4RSxJQUFJLENBQUMsTUFBTSxHQUFHLENBQUMsSUFBSSxDQUFDLFdBQVcsRUFBRSxJQUFJLENBQUMsV0FBVyxDQUFDLFFBQVEsRUFBRSxJQUFJLENBQUMsV0FBVyxDQUFDLFFBQVEsQ0FBQztZQUNsRixJQUFJLENBQUMsQ0FBRSxFQUFFLEVBQUUsRUFBRSxFQUFHLEVBQUUsQ0FBQyxFQUFFLENBQUMsU0FBUyxDQUFFLEVBQUUsQ0FBRSxDQUF3QixDQUFDO0lBQ3RFLENBQUM7SUF2QkQsSUFBSSxXQUFXLEtBQVcsTUFBTSxDQUFDLElBQUksQ0FBQyxZQUFZLENBQUMsQ0FBQyxDQUFDO0lBQ3JELElBQUksV0FBVyxDQUFFLElBQVUsSUFBSyxJQUFJLENBQUMsWUFBWSxHQUFHLElBQUksQ0FBQyxDQUFDLENBQUM7SUFDM0QsSUFBSSxjQUFjLEtBQXFCLE1BQU0sQ0FBQyxJQUFJLENBQUMsU0FBUyxDQUFDLENBQUMsQ0FBQztJQUMvRCxJQUFJLGNBQWMsQ0FBRSxjQUE4QixJQUFLLElBQUksQ0FBQyxTQUFTLEdBQUcsY0FBYyxDQUFDLENBQUMsQ0FBQztJQUN6RixJQUFJLFFBQVE7UUFDUixNQUFNLENBQUMsSUFBSSxDQUFDLFNBQVMsQ0FBQztJQUMxQixDQUFDO0lBQ0QsSUFBSSxLQUFLO1FBQ0wsTUFBTSxDQUFDLElBQUksQ0FBQyxNQUFNLENBQUM7SUFDdkIsQ0FBQztJQWdCRCxNQUFNLENBQUMsRUFBRSxDQUFFLE9BQWUsRUFBRSxPQUFlLEVBQUUsT0FBZTtRQUN4RCxJQUFJLGlCQUFpQixHQUFnQixRQUFRLENBQUMsY0FBYyxDQUFFLE9BQU8sRUFBRSxPQUFPLEVBQUUsT0FBTyxDQUFFLENBQUM7UUFDMUYsRUFBRSxDQUFDLENBQUUsaUJBQWlCLElBQUksV0FBVyxDQUFDLFFBQVMsQ0FBQztZQUFDLE1BQU0sQ0FBQyxJQUFJLENBQUM7UUFDN0QsSUFBSSxFQUFFLEdBQVcsT0FBTyxDQUFDO1FBQ3pCLElBQUksRUFBVSxDQUFDO1FBQ2YsSUFBSSxFQUFVLENBQUM7UUFDZixFQUFFLENBQUMsQ0FBRSxpQkFBaUIsSUFBSSxpQkFBa0IsQ0FBQyxDQUFDLENBQUM7WUFDM0MsRUFBRSxHQUFHLE9BQU8sQ0FBQztZQUNiLEVBQUUsR0FBRyxPQUFPLENBQUM7UUFDakIsQ0FBQztRQUFDLElBQUksQ0FBQyxDQUFDO1lBQ0osRUFBRSxHQUFHLE9BQU8sQ0FBQztZQUNiLEVBQUUsR0FBRyxPQUFPLENBQUM7UUFDakIsQ0FBQztRQUNELElBQUksSUFBSSxHQUFTLElBQUksSUFBSSxDQUFFLEVBQUUsRUFBRSxJQUFJLEVBQUUsSUFBSSxFQUFFLElBQUksQ0FBRSxDQUFDO1FBQ2xELElBQUksSUFBSSxHQUFTLElBQUksSUFBSSxDQUFFLEVBQUUsRUFBRSxJQUFJLEVBQUUsSUFBSSxFQUFFLElBQUksQ0FBRSxDQUFDO1FBQ2xELElBQUksSUFBSSxHQUFTLElBQUksSUFBSSxDQUFFLEVBQUUsRUFBRSxJQUFJLEVBQUUsSUFBSSxFQUFFLElBQUksQ0FBRSxDQUFDO1FBQ2xELElBQUksQ0FBQyxRQUFRLEdBQUcsSUFBSSxDQUFDO1FBQ3JCLElBQUksQ0FBQyxRQUFRLEdBQUcsSUFBSSxDQUFDO1FBQ3JCLElBQUksQ0FBQyxRQUFRLEdBQUcsSUFBSSxDQUFDO1FBQ3JCLE1BQU0sQ0FBQyxJQUFJLFFBQVEsQ0FBRSxJQUFJLENBQUUsQ0FBQztJQUNoQyxDQUFDO0lBRUQsbUJBQW1CO0lBQ25CLDBFQUEwRTtJQUMxRSxNQUFNLENBQUMsY0FBYyxDQUFFLE9BQWUsRUFBRSxPQUFlLEVBQUUsT0FBZTtRQUNwRSxpRUFBaUU7UUFDakUsZ0NBQWdDO1FBQ2hDLElBQUksR0FBRyxHQUFXLENBQUUsT0FBTyxDQUFDLENBQUMsR0FBRyxPQUFPLENBQUMsQ0FBQyxDQUFFLEdBQUcsQ0FBRSxPQUFPLENBQUMsQ0FBQyxHQUFHLE9BQU8sQ0FBQyxDQUFDLENBQUU7WUFDbkUsQ0FBRSxPQUFPLENBQUMsQ0FBQyxHQUFHLE9BQU8sQ0FBQyxDQUFDLENBQUUsR0FBRyxDQUFFLE9BQU8sQ0FBQyxDQUFDLEdBQUcsT0FBTyxDQUFDLENBQUMsQ0FBRSxDQUFDO1FBRTFELEVBQUUsQ0FBQyxDQUFFLEdBQUcsSUFBSSxDQUFFLENBQUM7WUFBQyxNQUFNLENBQUMsV0FBVyxDQUFDLFFBQVEsQ0FBQyxDQUFFLFdBQVc7UUFFekQ7Ozs7V0FJRztRQUNILE1BQU0sQ0FBQyxDQUFFLEdBQUcsR0FBRyxDQUFDLENBQUUsQ0FBQyxDQUFDLENBQUMsV0FBVyxDQUFDLFNBQVMsQ0FBQyxDQUFDLENBQUMsV0FBVyxDQUFDLGlCQUFpQixDQUFDO0lBQy9FLENBQUM7SUFFRCxJQUFJLENBQUUsVUFBbUI7UUFDckIsR0FBRyxDQUFDLENBQUUsSUFBSSxJQUFJLElBQUksSUFBSSxDQUFDLEtBQU0sQ0FBQyxDQUFDLENBQUM7WUFDNUIsSUFBSSxDQUFDLElBQUksRUFBRSxDQUFDO1FBQ2hCLENBQUM7SUFDTCxDQUFDO0lBRUQ7OztPQUdHO0lBQ0ksT0FBTztRQUNWLElBQUksRUFBRSxHQUFHLElBQUksQ0FBQyxRQUFRLENBQUMsQ0FBQyxDQUFDLENBQUM7UUFDMUIsSUFBSSxFQUFFLEdBQUcsSUFBSSxDQUFDLFFBQVEsQ0FBQyxDQUFDLENBQUMsQ0FBQztRQUMxQixJQUFJLEVBQUUsR0FBRyxJQUFJLENBQUMsUUFBUSxDQUFDLENBQUMsQ0FBQyxDQUFDO1FBQzFCLElBQUksS0FBSyxHQUFHLEVBQUUsQ0FBQyxDQUFDLEdBQUcsQ0FBRSxFQUFFLENBQUMsQ0FBQyxHQUFHLEVBQUUsQ0FBQyxDQUFDLENBQUUsQ0FBQztRQUNuQyxJQUFJLEtBQUssR0FBRyxFQUFFLENBQUMsQ0FBQyxHQUFHLENBQUUsRUFBRSxDQUFDLENBQUMsR0FBRyxFQUFFLENBQUMsQ0FBQyxDQUFFLENBQUM7UUFDbkMsSUFBSSxLQUFLLEdBQUcsRUFBRSxDQUFDLENBQUMsR0FBRyxDQUFFLEVBQUUsQ0FBQyxDQUFDLEdBQUcsRUFBRSxDQUFDLENBQUMsQ0FBRSxDQUFDO1FBQ25DLE1BQU0sQ0FBQyxJQUFJLENBQUMsR0FBRyxDQUFFLEtBQUssR0FBRyxLQUFLLEdBQUcsS0FBSyxDQUFFLElBQUksQ0FBQyxDQUFDO0lBQ2xELENBQUM7SUFHRCxRQUFRLENBQUUsTUFBYztRQUNwQixFQUFFLENBQUMsQ0FBRSxDQUFDLE1BQU8sQ0FBQztZQUFDLE1BQU0sQ0FBQyxLQUFLLENBQUM7UUFDNUIsRUFBRSxDQUFDLENBQUUsSUFBSSxDQUFDLEtBQUssR0FBRyxNQUFNLENBQUMsQ0FBQyxJQUFJLElBQUksQ0FBQyxLQUFLLEdBQUcsTUFBTSxDQUFDLENBQUM7ZUFDNUMsSUFBSSxDQUFDLEtBQUssR0FBRyxNQUFNLENBQUMsQ0FBQyxJQUFJLElBQUksQ0FBQyxLQUFLLEdBQUcsTUFBTSxDQUFDLENBQUUsQ0FBQztZQUFDLE1BQU0sQ0FBQyxLQUFLLENBQUM7UUFHckUscURBQXFEO1FBRXJELGtCQUFrQjtRQUNsQixJQUFJLEVBQUUsR0FBVyxJQUFJLENBQUMsV0FBVyxDQUFDLFFBQVEsQ0FBQyxXQUFXLENBQUMsY0FBYyxDQUFFLElBQUksQ0FBQyxXQUFXLENBQUMsV0FBVyxDQUFFLENBQUM7UUFDdEcsSUFBSSxFQUFFLEdBQVcsSUFBSSxDQUFDLFdBQVcsQ0FBQyxRQUFRLENBQUMsV0FBVyxDQUFDLGNBQWMsQ0FBRSxJQUFJLENBQUMsV0FBVyxDQUFDLFdBQVcsQ0FBRSxDQUFDO1FBQ3RHLElBQUksRUFBRSxHQUFXLE1BQU0sQ0FBQyxjQUFjLENBQUUsSUFBSSxDQUFDLFdBQVcsQ0FBQyxXQUFXLENBQUUsQ0FBQztRQUV2RSx1QkFBdUI7UUFDdkIsSUFBSSxLQUFLLEdBQVcsRUFBRSxDQUFDLGFBQWEsQ0FBRSxFQUFFLENBQUUsQ0FBQztRQUMzQyxJQUFJLEtBQUssR0FBVyxFQUFFLENBQUMsYUFBYSxDQUFFLEVBQUUsQ0FBRSxDQUFDO1FBQzNDLElBQUksS0FBSyxHQUFXLEVBQUUsQ0FBQyxhQUFhLENBQUUsRUFBRSxDQUFFLENBQUM7UUFDM0MsSUFBSSxLQUFLLEdBQVcsRUFBRSxDQUFDLGFBQWEsQ0FBRSxFQUFFLENBQUUsQ0FBQztRQUMzQyxJQUFJLEtBQUssR0FBVyxFQUFFLENBQUMsYUFBYSxDQUFFLEVBQUUsQ0FBRSxDQUFDO1FBRTNDLG1DQUFtQztRQUNuQyxJQUFJLFFBQVEsR0FBVyxDQUFDLEdBQUcsQ0FBRSxLQUFLLEdBQUcsS0FBSyxHQUFHLEtBQUssR0FBRyxLQUFLLENBQUUsQ0FBQztRQUM3RCxJQUFJLENBQUMsR0FBVyxDQUFFLEtBQUssR0FBRyxLQUFLLEdBQUcsS0FBSyxHQUFHLEtBQUssQ0FBRSxHQUFHLFFBQVEsQ0FBQztRQUM3RCxJQUFJLENBQUMsR0FBVyxDQUFFLEtBQUssR0FBRyxLQUFLLEdBQUcsS0FBSyxHQUFHLEtBQUssQ0FBRSxHQUFHLFFBQVEsQ0FBQztRQUU3RCxnQ0FBZ0M7UUFDaEMsTUFBTSxDQUFDLENBQUUsQ0FBQyxJQUFJLENBQUMsQ0FBRSxJQUFJLENBQUUsQ0FBQyxJQUFJLENBQUMsQ0FBRSxJQUFJLENBQUUsQ0FBQyxHQUFHLENBQUMsR0FBRyxDQUFDLENBQUUsQ0FBQztJQUNyRCxDQUFDO0lBRUQ7Ozs7Ozs7Ozs7T0FVRztJQUNILHNCQUFzQixDQUFFLFFBQWtCO1FBQ3RDLEVBQUUsQ0FBQyxDQUFFLENBQUMsUUFBUyxDQUFDO1lBQUMsTUFBTSxDQUFDLEtBQUssQ0FBQztRQUM5QixFQUFFLENBQUMsQ0FBRSxJQUFJLENBQUMsS0FBSyxHQUFHLFFBQVEsQ0FBQyxLQUFLLElBQUksSUFBSSxDQUFDLEtBQUssR0FBRyxRQUFRLENBQUMsS0FBSztlQUN4RCxJQUFJLENBQUMsS0FBSyxHQUFHLFFBQVEsQ0FBQyxLQUFLLElBQUksSUFBSSxDQUFDLEtBQUssR0FBRyxRQUFRLENBQUMsS0FBTSxDQUFDLENBQUMsQ0FBQztZQUNqRSxNQUFNLENBQUMsS0FBSyxDQUFDO1FBQ2pCLENBQUM7UUFDRCxFQUFFLENBQUMsQ0FBRSxJQUFJLENBQUMsV0FBVyxDQUFFLFFBQVEsQ0FBRyxDQUFDO1lBQUMsTUFBTSxDQUFDLEtBQUssQ0FBQztRQUdqRCxHQUFHLENBQUMsQ0FBRSxJQUFJLE1BQU0sSUFBSSxRQUFRLENBQUMsUUFBUyxDQUFDLENBQUMsQ0FBQztZQUNyQyw0REFBNEQ7WUFDNUQsaUVBQWlFO1lBQ2pFLG1EQUFtRDtZQUVuRCxHQUFHLENBQUMsQ0FBRSxJQUFJLFlBQVksSUFBSSxJQUFJLENBQUMsS0FBTSxDQUFDLENBQUMsQ0FBQztnQkFDcEMsR0FBRyxDQUFDLENBQUUsSUFBSSxXQUFXLElBQUksUUFBUSxDQUFDLEtBQU0sQ0FBQyxDQUFDLENBQUM7b0JBQ3ZDLEVBQUUsQ0FBQyxDQUFFLFlBQVksQ0FBQyxjQUFjLENBQUUsV0FBVyxDQUFHLENBQUM7d0JBQUMsTUFBTSxDQUFDLElBQUksQ0FBQztnQkFDbEUsQ0FBQztZQUNMLENBQUM7UUFDTCxDQUFDO1FBQ0QsTUFBTSxDQUFDLEtBQUssQ0FBQztJQUNqQixDQUFDO0lBRUQsV0FBVyxDQUFFLFFBQWtCO1FBQzNCLEVBQUUsQ0FBQyxDQUFFLENBQUMsUUFBUyxDQUFDO1lBQUMsTUFBTSxDQUFDLEtBQUssQ0FBQztRQUM5QixHQUFHLENBQUMsQ0FBRSxJQUFJLElBQUksSUFBSSxRQUFRLENBQUMsS0FBTSxDQUFDLENBQUMsQ0FBQztZQUNoQyxHQUFHLENBQUMsQ0FBRSxJQUFJLE9BQU8sSUFBSSxJQUFJLENBQUMsS0FBTSxDQUFDLENBQUMsQ0FBQztnQkFDL0IsRUFBRSxDQUFDLENBQUUsT0FBTyxDQUFDLFdBQVcsRUFBRSxJQUFJLE9BQU8sQ0FBQyxXQUFXLEVBQUUsQ0FBQyxNQUFNLENBQUUsSUFBSSxDQUFHLENBQUM7b0JBQUMsTUFBTSxDQUFDLElBQUksQ0FBQztZQUNyRixDQUFDO1FBQ0wsQ0FBQztRQUNELE1BQU0sQ0FBQyxLQUFLLENBQUM7SUFDakIsQ0FBQztJQUVEOzs7T0FHRztJQUNILFdBQVc7UUFDUCxJQUFJLENBQUMsR0FBRyxDQUFFLElBQUksQ0FBQyxRQUFRLENBQUMsQ0FBQyxDQUFDLENBQUMsQ0FBQyxHQUFHLElBQUksQ0FBQyxRQUFRLENBQUMsQ0FBQyxDQUFDLENBQUMsQ0FBQyxHQUFHLElBQUksQ0FBQyxRQUFRLENBQUMsQ0FBQyxDQUFDLENBQUMsQ0FBQyxDQUFFLEdBQUcsS0FBSyxDQUFDO1FBQ2pGLElBQUksQ0FBQyxHQUFHLENBQUUsSUFBSSxDQUFDLFFBQVEsQ0FBQyxDQUFDLENBQUMsQ0FBQyxDQUFDLEdBQUcsSUFBSSxDQUFDLFFBQVEsQ0FBQyxDQUFDLENBQUMsQ0FBQyxDQUFDLEdBQUcsSUFBSSxDQUFDLFFBQVEsQ0FBQyxDQUFDLENBQUMsQ0FBQyxDQUFDLENBQUUsR0FBRyxLQUFLLENBQUM7UUFDakYsTUFBTSxDQUFDLElBQUksTUFBTSxDQUFFLENBQUMsRUFBRSxDQUFDLENBQUUsQ0FBQztJQUM5QixDQUFDO0lBUUQsU0FBUyxDQUFFLGFBQXVCO1FBQzlCLElBQUksUUFBUSxHQUFHLElBQUksQ0FBQyxLQUFLLENBQUM7UUFDMUIsSUFBSSxVQUFVLEdBQUcsYUFBYSxDQUFDLEtBQUssQ0FBQztRQUNyQyxJQUFJLE1BQU0sR0FBRyxRQUFRLENBQUMsQ0FBQyxDQUFDLENBQUMsU0FBUyxDQUFFLFVBQVUsQ0FBQyxDQUFDLENBQUMsQ0FBRSxDQUFDO1FBQ3BELEVBQUUsQ0FBQyxDQUFFLE1BQU0sSUFBSSxDQUFFLENBQUM7WUFBQyxNQUFNLENBQUMsTUFBTSxDQUFDO1FBQ2pDLElBQUksTUFBTSxHQUFHLFFBQVEsQ0FBQyxDQUFDLENBQUMsQ0FBQyxTQUFTLENBQUUsVUFBVSxDQUFDLENBQUMsQ0FBQyxDQUFFLENBQUM7UUFDcEQsRUFBRSxDQUFDLENBQUUsTUFBTSxJQUFJLENBQUUsQ0FBQztZQUFDLE1BQU0sQ0FBQyxNQUFNLENBQUM7UUFDakMsSUFBSTtZQUFDLE1BQU0sQ0FBQyxRQUFRLENBQUMsQ0FBQyxDQUFDLENBQUMsU0FBUyxDQUFFLFVBQVUsQ0FBQyxDQUFDLENBQUMsQ0FBRSxDQUFDO0lBQ3ZELENBQUM7SUFFRCxNQUFNLENBQUUsUUFBZ0I7UUFDcEIsRUFBRSxDQUFDLENBQUUsQ0FBQyxDQUFFLFFBQVEsWUFBWSxRQUFRLENBQUcsQ0FBQztZQUFDLE1BQU0sQ0FBQyxLQUFLLENBQUM7UUFDdEQsSUFBSSxXQUFXLEdBQUcsSUFBSSxDQUFDLFFBQVEsQ0FBQztRQUNoQyxJQUFJLGFBQWEsR0FBRyxRQUFRLENBQUMsUUFBUSxDQUFDO1FBQ3RDLE1BQU0sQ0FBQyxDQUFFLFdBQVcsQ0FBQyxDQUFDLENBQUMsQ0FBQyxNQUFNLENBQUUsYUFBYSxDQUFDLENBQUMsQ0FBQyxDQUFFLElBQUksV0FBVyxDQUFDLENBQUMsQ0FBQyxDQUFDLE1BQU0sQ0FBRSxhQUFhLENBQUMsQ0FBQyxDQUFDLENBQUUsSUFBSSxXQUFXLENBQUMsQ0FBQyxDQUFDLENBQUMsTUFBTSxDQUFFLGFBQWEsQ0FBQyxDQUFDLENBQUMsQ0FBRSxDQUFFLENBQUM7SUFDbkosQ0FBQztDQUNKO0FBQUE7QUFBQTtBQUVEOzs7OztHQUtHO0FBQ0cseUJBQTJCLEdBQUcsUUFBa0I7SUFDbEQsRUFBRSxDQUFDLENBQUUsUUFBUSxDQUFDLE1BQU0sR0FBRyxDQUFFLENBQUM7UUFBQyxNQUFNLENBQUM7SUFDbEMsSUFBSSxLQUFLLEdBQVcsRUFBRSxDQUFDO0lBQ3ZCLEdBQUcsQ0FBQyxDQUFFLElBQUksQ0FBQyxHQUFHLENBQUMsRUFBRSxDQUFDLEdBQUcsUUFBUSxDQUFDLE1BQU0sRUFBRSxDQUFDLEVBQUUsRUFBRyxDQUFDO1FBQ3pDLEtBQUssR0FBRyxLQUFLLENBQUMsTUFBTSxDQUFFLElBQUksSUFBSSxDQUFFLFFBQVEsQ0FBQyxDQUFDLENBQUMsRUFBRSxJQUFJLEVBQUUsSUFBSSxDQUFFLENBQUUsQ0FBQztJQUNoRSxDQUFDO0lBQ0QsR0FBRyxDQUFDLENBQUUsSUFBSSxDQUFDLEdBQUcsQ0FBQyxFQUFFLENBQUMsR0FBRyxLQUFLLENBQUMsTUFBTSxHQUFHLENBQUMsRUFBRSxDQUFDLEVBQUUsRUFBRyxDQUFDO1FBQzFDLEtBQUssQ0FBQyxDQUFDLENBQUMsQ0FBQyxRQUFRLEdBQUcsS0FBSyxDQUFDLENBQUMsR0FBRyxDQUFDLENBQUMsQ0FBQztRQUNqQyxLQUFLLENBQUMsQ0FBQyxDQUFDLENBQUMsUUFBUSxHQUFHLEtBQUssQ0FBQyxDQUFDLEdBQUcsQ0FBQyxDQUFDLENBQUM7SUFDckMsQ0FBQztJQUNELEtBQUssQ0FBQyxDQUFDLENBQUMsQ0FBQyxRQUFRLEdBQUcsS0FBSyxDQUFDLENBQUMsQ0FBQyxDQUFDO0lBQzdCLEtBQUssQ0FBQyxDQUFDLENBQUMsQ0FBQyxRQUFRLEdBQUcsS0FBSyxDQUFDLEtBQUssQ0FBQyxNQUFNLEdBQUcsQ0FBQyxDQUFDLENBQUM7SUFDNUMsS0FBSyxDQUFDLEtBQUssQ0FBQyxNQUFNLEdBQUcsQ0FBQyxDQUFDLENBQUMsUUFBUSxHQUFHLEtBQUssQ0FBQyxDQUFDLENBQUMsQ0FBQztJQUM1QyxLQUFLLENBQUMsS0FBSyxDQUFDLE1BQU0sR0FBRyxDQUFDLENBQUMsQ0FBQyxRQUFRLEdBQUcsS0FBSyxDQUFDLEtBQUssQ0FBQyxNQUFNLEdBQUcsQ0FBQyxDQUFDLENBQUM7SUFDM0QsTUFBTSxDQUFDLEtBQUssQ0FBQztBQUNqQixDQUFDOzs7Ozs7OztBQ2pnQks7SUFvQkY7Ozs7O09BS0c7SUFDSCxZQUFxQixhQUF1QixFQUFFLEtBQWM7UUFqQjVELHdFQUF3RTtRQUNoRSxhQUFRLEdBQVcsQ0FBQyxDQUFDO1FBaUJ6QixJQUFJLENBQUMsU0FBUyxHQUFHLGFBQWEsQ0FBQztRQUMvQixJQUFJLENBQUMsTUFBTSxHQUFHLEtBQUssQ0FBQztJQUN4QixDQUFDO0lBbEJELElBQUksU0FBUyxLQUFhLE1BQU0sQ0FBQyxJQUFJLENBQUMsVUFBVSxDQUFDLENBQUMsQ0FBQztJQUNuRCxJQUFJLFNBQVMsQ0FBRSxTQUFpQixJQUFLLElBQUksQ0FBQyxVQUFVLEdBQUcsU0FBUyxDQUFDLENBQUMsQ0FBQztJQUNuRSxJQUFJLFFBQVEsS0FBZSxNQUFNLENBQUMsSUFBSSxDQUFDLFNBQVMsQ0FBQyxDQUFDLENBQUM7SUFDbkQsSUFBSSxRQUFRLENBQUUsUUFBa0IsSUFBSyxJQUFJLENBQUMsU0FBUyxHQUFHLFFBQVEsQ0FBQyxDQUFDLENBQUM7SUFDakUsSUFBSSxLQUFLLEtBQWEsTUFBTSxDQUFDLElBQUksQ0FBQyxNQUFNLENBQUMsQ0FBQyxDQUFDO0lBQzNDLElBQUksS0FBSyxDQUFFLEtBQWEsSUFBSyxJQUFJLENBQUMsTUFBTSxHQUFHLEtBQUssQ0FBQyxDQUFDLENBQUM7SUFDbkQsSUFBSSxPQUFPLEtBQWEsTUFBTSxDQUFDLElBQUksQ0FBQyxRQUFRLENBQUMsQ0FBQyxDQUFDO0lBQy9DLElBQUksT0FBTyxDQUFFLFlBQW9CLElBQUssRUFBRSxDQUFDLENBQUUsWUFBWSxJQUFJLGNBQWMsQ0FBQyxXQUFZLENBQUM7UUFBQyxJQUFJLENBQUMsUUFBUSxHQUFHLFlBQVksQ0FBQyxDQUFDLENBQUM7SUFhdkg7Ozs7T0FJRztJQUNILE1BQU0sQ0FBQyxFQUFFLENBQUUsYUFBdUIsRUFBRSxLQUFhO1FBQzdDLEVBQUUsQ0FBQyxDQUFFLENBQUMsYUFBYyxDQUFDO1lBQUMsTUFBTSxDQUFDLElBQUksQ0FBQztRQUNsQyxJQUFJLFFBQVEsR0FBRyxJQUFJLGNBQWMsQ0FBRSxhQUFhLEVBQUUsS0FBSyxDQUFFLENBQUM7UUFDMUQsUUFBUSxDQUFDLFFBQVEsQ0FBQyxjQUFjLEdBQUcsUUFBUSxDQUFDO1FBQzVDLE1BQU0sQ0FBQyxRQUFRLENBQUM7SUFDcEIsQ0FBQztJQUVEOztPQUVHO0lBQ0gsSUFBSTtRQUNBLElBQUksQ0FBQyxRQUFRLENBQUMsSUFBSSxFQUFFLENBQUM7SUFDekIsQ0FBQztJQUVEOzs7OztPQUtHO0lBQ0gsb0JBQW9CLENBQUUsV0FBMkI7UUFDN0MsSUFBSSxPQUFPLEdBQVksS0FBSyxDQUFDO1FBQzdCLEVBQUUsQ0FBQyxDQUFFLElBQUksQ0FBQyxLQUFLLElBQUksV0FBVyxDQUFDLEtBQU0sQ0FBQyxDQUFDLENBQUM7WUFBQyxPQUFPLEdBQUcsSUFBSSxDQUFDO1lBQUMsSUFBSSxDQUFDLEtBQUssR0FBRyxXQUFXLENBQUMsS0FBSyxDQUFDO1FBQUMsQ0FBQztRQUMxRixFQUFFLENBQUMsQ0FBRSxJQUFJLENBQUMsU0FBUyxJQUFJLFdBQVcsQ0FBQyxTQUFVLENBQUMsQ0FBQyxDQUFDO1lBQUMsT0FBTyxHQUFHLElBQUksQ0FBQztZQUFDLElBQUksQ0FBQyxTQUFTLEdBQUcsV0FBVyxDQUFDLFNBQVMsQ0FBQztRQUFDLENBQUM7UUFDMUcsRUFBRSxDQUFDLENBQUUsSUFBSSxDQUFDLE9BQU8sSUFBSSxXQUFXLENBQUMsT0FBUSxDQUFDLENBQUMsQ0FBQztZQUFDLE9BQU8sR0FBRyxJQUFJLENBQUM7WUFBQyxJQUFJLENBQUMsT0FBTyxHQUFHLFdBQVcsQ0FBQyxPQUFPLENBQUM7UUFBQyxDQUFDO1FBQ2xHLE1BQU0sQ0FBQyxPQUFPLENBQUM7SUFDbkIsQ0FBQztJQUVELE1BQU0sQ0FBRSxLQUFVO1FBQ2QsRUFBRSxDQUFDLENBQUUsS0FBSyxZQUFZLGNBQWUsQ0FBQyxDQUFDLENBQUM7WUFDcEMsTUFBTSxDQUFDLENBQUUsSUFBSSxDQUFDLFFBQVEsQ0FBQyxNQUFNLENBQUUsS0FBSyxDQUFDLFFBQVEsQ0FBRSxJQUFJLElBQUksQ0FBQyxLQUFLLEtBQUssS0FBSyxDQUFDLEtBQUssQ0FBRSxDQUFDO1FBQ3BGLENBQUM7UUFBQyxJQUFJO1lBQUMsTUFBTSxDQUFDLEtBQUssQ0FBQztJQUN4QixDQUFDO0lBRUQsUUFBUTtRQUNKLE1BQU0sQ0FBQyxJQUFJLENBQUMsS0FBSyxHQUFHLElBQUksR0FBRyxJQUFJLENBQUMsUUFBUSxDQUFDO0lBQzdDLENBQUM7Ozs7QUF2RUQscURBQXFEO0FBQ3JDLDBCQUFXLEdBQUcsRUFBRSxDQUFDO0FBeUVyQzs7R0FFRztBQUNHO0lBY0Ysd0ZBQXdGO0lBRXhGLFlBQWEsY0FBOEI7UUFkbkMsb0JBQWUsR0FBVyxDQUFDLENBQUM7UUFlaEMsSUFBSSxDQUFDLGVBQWUsR0FBRyxjQUFjLENBQUM7UUFDdEMsSUFBSSxDQUFDLFFBQVEsR0FBRyxjQUFjLENBQUMsT0FBTyxDQUFDO0lBQzNDLENBQUM7SUFkRCxJQUFJLGNBQWMsS0FBcUIsTUFBTSxDQUFDLElBQUksQ0FBQyxlQUFlLENBQUMsQ0FBQyxDQUFDO0lBQ3JFLHFHQUFxRztJQUNyRyxJQUFJLGNBQWMsS0FBYSxNQUFNLENBQUMsSUFBSSxDQUFDLGVBQWUsQ0FBQyxDQUFDLENBQUM7SUFDN0QsSUFBSSxjQUFjLENBQUUsY0FBc0IsSUFBSyxJQUFJLENBQUMsZUFBZSxHQUFHLGNBQWMsQ0FBQyxDQUFDLENBQUM7SUFDdkYsSUFBSSxRQUFRLEtBQWUsTUFBTSxDQUFDLElBQUksQ0FBQyxlQUFlLENBQUMsUUFBUSxDQUFDLENBQUMsQ0FBQztJQUNsRSxJQUFJLE9BQU8sS0FBYSxNQUFNLENBQUMsSUFBSSxDQUFDLFFBQVEsQ0FBQyxDQUFDLENBQUM7SUFDL0MsSUFBSSxPQUFPLENBQUUsWUFBb0IsSUFBSyxJQUFJLENBQUMsUUFBUSxHQUFHLFlBQVksQ0FBQyxDQUFDLENBQUM7SUFDckUsSUFBSSxTQUFTLEtBQWEsTUFBTSxDQUFDLElBQUksQ0FBQyxlQUFlLENBQUMsU0FBUyxDQUFDLENBQUMsQ0FBQztJQUNsRSxJQUFJLEtBQUssS0FBYSxNQUFNLENBQUMsSUFBSSxDQUFDLGVBQWUsQ0FBQyxLQUFLLEVBQUMsQ0FBQztJQUFBLENBQUM7SUFRMUQsTUFBTSxDQUFFLEdBQVc7UUFDZixFQUFFLENBQUMsQ0FBRSxDQUFDLENBQUUsR0FBRyxZQUFZLGVBQWUsQ0FBRyxDQUFDO1lBQUMsTUFBTSxDQUFDLEtBQUssQ0FBQztRQUN4RCxFQUFFLENBQUMsQ0FBRSxJQUFJLElBQUksR0FBSSxDQUFDO1lBQUMsTUFBTSxDQUFDLElBQUksQ0FBQztRQUMvQixNQUFNLENBQUMsSUFBSSxDQUFDLGVBQWUsQ0FBQyxNQUFNLENBQUUsR0FBRyxDQUFDLGVBQWUsQ0FBRSxDQUFDO0lBQzlELENBQUM7SUFFRCxRQUFRO1FBQ0osTUFBTSxDQUFDLElBQUksQ0FBQyxlQUFlLENBQUMsUUFBUSxFQUFFLENBQUM7SUFDM0MsQ0FBQztDQUNKO0FBQUE7QUFBQTs7Ozs7Ozs7O0FDbkhEO0FBQUE7O0dBRUc7QUFFSDs7R0FFRztBQUUyRDtBQUNFO0FBRTFEO0lBSUYsWUFBYSxDQUFTLEVBQUUsQ0FBUyxFQUFFLE1BQWM7UUFDN0MsSUFBSSxDQUFDLENBQUMsR0FBRyxDQUFDLENBQUM7UUFDWCxJQUFJLENBQUMsQ0FBQyxHQUFHLENBQUMsQ0FBQztRQUNYLElBQUksQ0FBQyxNQUFNLEdBQUcsTUFBTSxDQUFDO0lBQ3pCLENBQUM7SUFFTSxNQUFNLENBQUMsRUFBRSxDQUFFLFNBQWlCLEVBQUUsTUFBYztRQUMvQyxNQUFNLENBQUMsSUFBSSxVQUFVLENBQUUsU0FBUyxDQUFDLENBQUMsRUFBRSxTQUFTLENBQUMsQ0FBQyxFQUFFLE1BQU0sQ0FBRSxDQUFDO0lBQzlELENBQUM7SUFFTSxNQUFNLENBQUMsUUFBUSxDQUFFLFNBQTJCO1FBQy9DLElBQUksQ0FBQyxFQUFFLENBQVMsQ0FBQztRQUNqQixFQUFFLENBQUMsQ0FBRSxTQUFTLFlBQVksVUFBVyxDQUFDLENBQUMsQ0FBQztZQUNwQyxDQUFDLEdBQUcsU0FBUyxDQUFDLENBQUMsQ0FBQztZQUNoQixDQUFDLEdBQUcsU0FBUyxDQUFDLENBQUMsQ0FBQztRQUNwQixDQUFDO1FBQUMsSUFBSSxDQUFDLENBQUM7WUFDSixDQUFDLEdBQUcsU0FBUyxDQUFDLENBQUMsQ0FBQztZQUNoQixDQUFDLEdBQUcsU0FBUyxDQUFDLENBQUMsQ0FBQztRQUNwQixDQUFDO1FBQ0QsTUFBTSxDQUFDLElBQUksd0VBQU0sQ0FBRSxDQUFDLEVBQUUsQ0FBQyxDQUFFLENBQUM7SUFDOUIsQ0FBQztDQUNKO0FBQUE7QUFBQTtBQUVLO0lBVUYsWUFBYSxNQUFrQixFQUFFLE1BQWtCLEVBQUUsTUFBa0IsRUFBRSxTQUFpQixFQUFFLE9BQWUsRUFBRSxLQUFhO1FBQ3RILElBQUksQ0FBQyxNQUFNLEdBQUcsTUFBTSxDQUFDO1FBQ3JCLElBQUksQ0FBQyxNQUFNLEdBQUcsTUFBTSxDQUFDO1FBQ3JCLElBQUksQ0FBQyxNQUFNLEdBQUcsTUFBTSxDQUFDO1FBQ3JCLElBQUksQ0FBQyxLQUFLLEdBQUcsS0FBSyxDQUFDO1FBQ25CLElBQUksQ0FBQyxPQUFPLEdBQUcsT0FBTyxDQUFDO1FBQ3ZCLElBQUksQ0FBQyxTQUFTLEdBQUcsU0FBUyxDQUFDO0lBQy9CLENBQUM7SUFFTSxNQUFNLENBQUMsRUFBRSxDQUFFLFNBQStCO1FBQzdDLElBQUksS0FBSyxHQUFXLFNBQVMsQ0FBQyxLQUFLLENBQUM7UUFDcEMsSUFBSSxRQUFRLEdBQTZCLFNBQVMsWUFBWSxvRkFBYyxDQUFDLENBQUM7WUFDMUUsU0FBUyxDQUFDLFFBQVEsQ0FBQyxRQUFRLENBQUMsQ0FBQyxDQUFDLENBQUMsU0FBUyxDQUFDLE1BQU0sRUFBRSxTQUFTLENBQUMsTUFBTSxFQUFFLFNBQVMsQ0FBQyxNQUFNLENBQUMsQ0FBQztRQUN6RixNQUFNLENBQUMsSUFBSSxZQUFZLENBQ25CLFVBQVUsQ0FBQyxFQUFFLENBQUUsUUFBUSxDQUFDLENBQUMsQ0FBQyxFQUFFLEtBQUssQ0FBRSxFQUNuQyxVQUFVLENBQUMsRUFBRSxDQUFFLFFBQVEsQ0FBQyxDQUFDLENBQUMsRUFBRSxLQUFLLENBQUUsRUFDbkMsVUFBVSxDQUFDLEVBQUUsQ0FBRSxRQUFRLENBQUMsQ0FBQyxDQUFDLEVBQUUsS0FBSyxDQUFFLEVBQUUsU0FBUyxDQUFDLFNBQVMsRUFBRSxTQUFTLENBQUMsT0FBTyxFQUFFLEtBQUssQ0FBRSxDQUFDO0lBQzdGLENBQUM7SUFFTSxNQUFNLENBQUMsZ0JBQWdCLENBQUUsU0FBNkI7UUFDekQsSUFBSSxFQUFFLEVBQUUsRUFBRSxFQUFFLEVBQVUsQ0FBQztRQUN2QixJQUFJLE1BQWMsQ0FBQztRQUNuQixFQUFFLENBQUMsQ0FBRSxTQUFTLFlBQVksWUFBYSxDQUFDLENBQUMsQ0FBQztZQUN0QyxFQUFFLEdBQUcsVUFBVSxDQUFDLFFBQVEsQ0FBRSxTQUFTLENBQUMsTUFBTSxDQUFFLENBQUM7WUFDN0MsRUFBRSxHQUFHLFVBQVUsQ0FBQyxRQUFRLENBQUUsU0FBUyxDQUFDLE1BQU0sQ0FBRSxDQUFDO1lBQzdDLEVBQUUsR0FBRyxVQUFVLENBQUMsUUFBUSxDQUFFLFNBQVMsQ0FBQyxNQUFNLENBQUUsQ0FBQztZQUM3QyxNQUFNLEdBQUcsU0FBUyxDQUFDLEtBQUssQ0FBQztRQUM3QixDQUFDO1FBQUMsSUFBSSxDQUFDLENBQUM7WUFDSixFQUFFLEdBQUcsVUFBVSxDQUFDLFFBQVEsQ0FBRSxTQUFTLENBQUMsTUFBTSxDQUFFLENBQUM7WUFDN0MsRUFBRSxHQUFHLFVBQVUsQ0FBQyxRQUFRLENBQUUsU0FBUyxDQUFDLE1BQU0sQ0FBRSxDQUFDO1lBQzdDLEVBQUUsR0FBRyxVQUFVLENBQUMsUUFBUSxDQUFFLFNBQVMsQ0FBQyxNQUFNLENBQUUsQ0FBQztZQUM3QyxNQUFNLEdBQUcsU0FBUyxDQUFDLE1BQU0sQ0FBQztRQUM5QixDQUFDO1FBQ0QsSUFBSSxRQUFRLEdBQUcsMEVBQVEsQ0FBQyxFQUFFLENBQUUsRUFBRSxFQUFFLEVBQUUsRUFBRSxFQUFFLENBQUUsQ0FBQztRQUN6QyxJQUFJLFdBQVcsR0FBRyxvRkFBYyxDQUFDLEVBQUUsQ0FBRSxRQUFRLEVBQUUsTUFBTSxDQUFFLENBQUM7UUFDeEQsV0FBVyxDQUFDLFNBQVMsR0FBRyxTQUFTLENBQUMsU0FBUyxDQUFDO1FBQzVDLFdBQVcsQ0FBQyxPQUFPLEdBQUcsU0FBUyxDQUFDLE9BQU8sQ0FBQztRQUN4QyxNQUFNLENBQUMsV0FBVyxDQUFDO0lBQ3ZCLENBQUM7Q0FDSjtBQUFBO0FBQUE7QUFTRDs7R0FFRztBQUNHO0lBSUYsSUFBSSxjQUFjLEtBQUssTUFBTSxDQUFDLElBQUksQ0FBQyxlQUFlLENBQUMsQ0FBQyxDQUFDO0lBQ3JELElBQUksY0FBYyxDQUFFLGNBQThCLElBQUssSUFBSSxDQUFDLGVBQWUsR0FBRyxjQUFjLENBQUMsQ0FBQyxDQUFDO0lBQy9GLElBQUksaUJBQWlCLEtBQUssTUFBTSxDQUFDLElBQUksQ0FBQyxrQkFBa0IsQ0FBQyxDQUFDLENBQUM7SUFDM0QsSUFBSSxpQkFBaUIsQ0FBRSxpQkFBaUMsSUFBSyxJQUFJLENBQUMsa0JBQWtCLEdBQUcsaUJBQWlCLENBQUMsQ0FBQyxDQUFDO0lBRTNHOzs7O09BSUc7SUFDSCxvQ0FBb0M7SUFDN0IsTUFBTTtRQUNULE1BQU0sQ0FBQztZQUNILGlCQUFpQixFQUFFLElBQUksQ0FBQyxrQkFBa0I7WUFDMUMsY0FBYyxFQUFFLElBQUksQ0FBQyxlQUFlO1NBQ3ZDLENBQUM7SUFDTixDQUFDO0lBRUQ7OztPQUdHO0lBQ0ksTUFBTSxDQUFDLFFBQVEsQ0FBRSxTQUFjO1FBQ2xDLElBQUksUUFBUSxHQUFpQixJQUFJLFlBQVksRUFBRSxDQUFDO1FBQ2hELElBQUksUUFBUSxHQUFtQixTQUFTLENBQUMsaUJBQWlCLENBQUMsR0FBRyxDQUFFLENBQUMsQ0FBQyxFQUFFLENBQUMsWUFBWSxDQUFDLEVBQUUsQ0FBRSxDQUFDLENBQUUsQ0FBRSxDQUFDO1FBQzVGLElBQUksS0FBSyxHQUFtQixTQUFTLENBQUMsY0FBYyxDQUFDLEdBQUcsQ0FBRSxDQUFDLENBQUMsRUFBRSxDQUFDLFlBQVksQ0FBQyxFQUFFLENBQUUsQ0FBQyxDQUFFLENBQUUsQ0FBQztRQUN0RixRQUFRLENBQUMsaUJBQWlCLEdBQUcsUUFBUSxDQUFDO1FBQ3RDLFFBQVEsQ0FBQyxjQUFjLEdBQUcsS0FBSyxDQUFDO1FBQ2hDLE1BQU0sQ0FBQyxRQUFRLENBQUM7SUFDcEIsQ0FBQztDQUNKO0FBQUE7QUFBQTtBQUVEOztHQUVHO0FBQ0c7SUFHRixJQUFJLGlCQUFpQixLQUFxQixNQUFNLENBQUMsSUFBSSxDQUFDLGtCQUFrQixDQUFDLENBQUMsQ0FBQztJQUUzRSxJQUFJLGlCQUFpQixDQUFFLGlCQUFpQztRQUNwRCxJQUFJLENBQUMsa0JBQWtCLEdBQUcsaUJBQWlCLENBQUM7SUFDaEQsQ0FBQztJQUVNLE1BQU0sQ0FBQyxRQUFRLENBQUUsU0FBYztRQUNsQyxJQUFJLFFBQVEsR0FBZSxJQUFJLFVBQVUsRUFBRSxDQUFDO1FBQzVDLFFBQVEsQ0FBQyxpQkFBaUIsR0FBRyxTQUFTLENBQUMsaUJBQWlCLENBQUMsR0FBRyxDQUFFLENBQUMsQ0FBQyxFQUFFLENBQUMsWUFBWSxDQUFDLEVBQUUsQ0FBRSxDQUFDLENBQUUsQ0FBRSxDQUFDO1FBQzFGLE1BQU0sQ0FBQyxRQUFRLENBQUM7SUFDcEIsQ0FBQztJQUVNLE1BQU07UUFDVCxNQUFNLENBQUM7WUFDSCxpQkFBaUIsRUFBRSxJQUFJLENBQUMsa0JBQWtCO1lBQzFDLGlCQUFpQixFQUFFLEtBQUs7U0FDM0IsQ0FBQztJQUNOLENBQUM7Q0FDSjtBQUFBO0FBQUE7QUFFRDs7R0FFRztBQUNHO0lBSUYsSUFBSSxRQUFRLEtBQWEsTUFBTSxDQUFDLElBQUksQ0FBQyxTQUFTLENBQUMsQ0FBQyxDQUFDO0lBQ2pELElBQUksUUFBUSxDQUFFLFFBQWdCLElBQUssSUFBSSxDQUFDLFNBQVMsR0FBRyxRQUFRLENBQUMsQ0FBQyxDQUFDO0lBQy9ELElBQUksWUFBWSxLQUFpQixNQUFNLENBQUMsSUFBSSxDQUFDLGFBQWEsQ0FBQyxDQUFDLENBQUM7SUFDN0QsSUFBSSxZQUFZLENBQUUsWUFBd0IsSUFBSyxJQUFJLENBQUMsYUFBYSxHQUFHLFlBQVksQ0FBQyxDQUFDLENBQUM7SUFFNUUsTUFBTSxDQUFDLFFBQVEsQ0FBRSxTQUFjO1FBQ2xDLElBQUksUUFBUSxHQUFhLElBQUksUUFBUSxFQUFFLENBQUM7UUFDeEMsUUFBUSxDQUFDLFFBQVEsR0FBRyxTQUFTLENBQUMsUUFBUSxDQUFDO1FBQ3ZDLFFBQVEsQ0FBQyxZQUFZLEdBQUcsVUFBVSxDQUFDLFFBQVEsQ0FBRSxTQUFTLENBQUMsWUFBWSxDQUFFLENBQUM7UUFDdEUsTUFBTSxDQUFDLFFBQVEsQ0FBQztJQUNwQixDQUFDO0lBRU0sTUFBTTtRQUNULE1BQU0sQ0FBQztZQUNILElBQUksRUFBRSxJQUFJLENBQUMsUUFBUTtTQUN0QixDQUFDO0lBQ04sQ0FBQztDQUNKO0FBQUE7QUFBQTtBQUVEOztHQUVHO0FBQ0c7SUFHRixJQUFJLE9BQU8sS0FBYSxNQUFNLENBQUMsSUFBSSxDQUFDLFFBQVEsQ0FBQyxDQUFDLENBQUM7SUFDL0MsSUFBSSxPQUFPLENBQUUsT0FBZSxJQUFLLElBQUksQ0FBQyxRQUFRLEdBQUcsT0FBTyxDQUFDLENBQUMsQ0FBQztJQUVwRCxNQUFNLENBQUMsUUFBUSxDQUFFLFNBQWM7UUFDbEMsSUFBSSxRQUFRLEdBQWdCLElBQUksV0FBVyxFQUFFLENBQUM7UUFDOUMsUUFBUSxDQUFDLE9BQU8sR0FBRyxTQUFTLENBQUMsT0FBTyxDQUFDO1FBQ3JDLE1BQU0sQ0FBQyxRQUFRLENBQUM7SUFDcEIsQ0FBQztJQUVNLE1BQU07UUFDVCxNQUFNLENBQUM7WUFDSCxPQUFPLEVBQUUsSUFBSSxDQUFDLE9BQU87U0FDeEIsQ0FBQztJQUNOLENBQUM7Q0FDSjtBQUFBO0FBQUE7Ozs7Ozs7O0FDOU1ELHFCQUFxQjtBQUNyQix3Q0FBd0M7QUFDeEMsR0FBRztBQUlILDRCQUE2QixRQUFnQjtJQUN6QyxJQUFJLFFBQVEsR0FBYSxJQUFJLEtBQUssRUFBRSxDQUFDO0lBQ3JDLElBQUksSUFBSSxHQUFHLFFBQVEsQ0FBQyxPQUFPLENBQUUsS0FBSyxFQUFFLE1BQU0sQ0FBRSxDQUFDO0lBQzdDLEdBQUcsQ0FBQyxDQUFFLElBQUksS0FBSyxHQUFHLENBQUMsRUFBRSxLQUFLLElBQUksRUFBRSxFQUFFLEtBQUssSUFBSSxJQUFJLEVBQUcsQ0FBQztRQUMvQyxRQUFRLENBQUMsSUFBSSxDQUFFLElBQUksQ0FBQyxPQUFPLENBQUUsR0FBRyxFQUFFLEdBQUcsR0FBRyxNQUFNLENBQUUsS0FBSyxDQUFFLEdBQUcsR0FBRyxDQUFFLENBQUUsQ0FBQztJQUN0RSxDQUFDO0lBQ0QsTUFBTSxDQUFDLFFBQVEsQ0FBQztBQUNwQixDQUFDO0FBRUQsSUFBSSxRQUFRLEdBQUcsY0FBYyxDQUFDO0FBQzlCLElBQUksTUFBTSxHQUFHLGdCQUFnQixDQUFDO0FBQzlCLElBQUksUUFBUSxHQUFHLFlBQVksQ0FBQztBQUU1QixNQUFNLFVBQVUsR0FBYSxrQkFBa0IsQ0FBRSxRQUFRLENBQUUsQ0FBQztBQUM1RCxNQUFNLFFBQVEsR0FBYSxrQkFBa0IsQ0FBRSxNQUFNLENBQUUsQ0FBQztBQUN4RCxNQUFNLFVBQVUsR0FBYSxrQkFBa0IsQ0FBRSxRQUFRLENBQUUsQ0FBQztBQUd0RDtJQUVGLEdBQUcsQ0FBRSxRQUFpQixJQUFhLE1BQU0sQ0FBQyxRQUFRLElBQUksSUFBSSxJQUFJLFFBQVEsSUFBSSxTQUFTLElBQUksUUFBUSxHQUFHLElBQUksQ0FBQyxPQUFPLENBQUMsTUFBTSxDQUFDLENBQUMsQ0FBQyxJQUFJLENBQUMsT0FBTyxDQUFDLFFBQVEsQ0FBQyxDQUFDLENBQUMsQ0FBQyxJQUFJLENBQUMsT0FBTyxDQUFDLElBQUksQ0FBQyxPQUFPLENBQUMsTUFBTSxHQUFHLENBQUMsQ0FBQyxDQUFDLENBQUMsQ0FBQztJQUN6TCxZQUFhLE1BQWdCO1FBQ3pCLElBQUksQ0FBQyxPQUFPLEdBQUcsTUFBTSxDQUFDO0lBQzFCLENBQUM7Q0FDSjtBQUFBO0FBQUE7QUFDTSxNQUFNLEtBQUssR0FBVSxJQUFJLEtBQUssQ0FBRSxVQUFVLENBQUUsQ0FBQztBQUFBO0FBQUE7QUFDN0MsTUFBTSxHQUFHLEdBQVUsSUFBSSxLQUFLLENBQUUsUUFBUSxDQUFFLENBQUM7QUFBQTtBQUFBO0FBQ3pDLE1BQU0sS0FBSyxHQUFVLElBQUksS0FBSyxDQUFFLFVBQVUsQ0FBRSxDQUFDO0FBQUE7QUFBQTs7Ozs7Ozs7Ozs7QUM3QjlDLHdCQUEwQixNQUFzQixFQUFFLElBQXNCO0lBQzFFLEdBQUcsQ0FBQyxDQUFFLElBQUksQ0FBQyxHQUFHLENBQUMsRUFBRSxDQUFDLEdBQUcsSUFBSSxDQUFDLE1BQU0sRUFBRSxDQUFDLEVBQUUsRUFBRyxDQUFDO1FBQ3JDLElBQUksS0FBSyxHQUFHLElBQUksQ0FBQyxDQUFDLENBQUMsQ0FBQyxNQUFNLENBQUUsTUFBTSxDQUFFLENBQUM7UUFDckMsRUFBRSxDQUFDLENBQUUsS0FBTSxDQUFDLENBQUMsQ0FBQztZQUNWLElBQUksQ0FBQyxNQUFNLENBQUUsQ0FBQyxFQUFFLENBQUMsQ0FBRSxDQUFDO1FBQ3hCLENBQUM7SUFDTCxDQUFDO0FBQ0wsQ0FBQztBQUVLLGFBQWUsR0FBRyxNQUFnQjtJQUNwQyxJQUFJLEdBQUcsR0FBVyxJQUFJLENBQUM7SUFDdkIsR0FBRyxDQUFDLENBQUUsSUFBSSxLQUFLLElBQUksTUFBTyxDQUFDLENBQUMsQ0FBQztRQUN6QixHQUFHLEdBQUcsR0FBRyxJQUFJLElBQUksQ0FBQyxDQUFDLENBQUMsS0FBSyxDQUFDLENBQUMsQ0FBQyxJQUFJLENBQUMsR0FBRyxDQUFFLEdBQUcsRUFBRSxLQUFLLENBQUUsQ0FBQztJQUN2RCxDQUFDO0lBQ0QsTUFBTSxDQUFDLEdBQUcsQ0FBQztBQUNmLENBQUM7QUFFSyxhQUFlLEdBQUcsTUFBZ0I7SUFDcEMsSUFBSSxHQUFHLEdBQVcsSUFBSSxDQUFDO0lBQ3ZCLEdBQUcsQ0FBQyxDQUFFLElBQUksS0FBSyxJQUFJLE1BQU8sQ0FBQyxDQUFDLENBQUM7UUFDekIsR0FBRyxHQUFHLEdBQUcsR0FBRyxJQUFJLENBQUMsQ0FBQyxDQUFDLEtBQUssQ0FBQyxDQUFDLENBQUMsSUFBSSxDQUFDLEdBQUcsQ0FBRSxHQUFHLEVBQUUsS0FBSyxDQUFFLENBQUM7SUFDdEQsQ0FBQztJQUNELE1BQU0sQ0FBQyxHQUFHLENBQUM7QUFDZixDQUFDOzs7Ozs7Ozs7Ozs7Ozs7QUM1QkQ7QUFBQTs7O0dBR0c7QUFHSCxvQ0FBb0M7QUFDNkM7QUFDbEM7QUFFL0MsTUFBTSxXQUFXLEdBQUcsSUFBSSxDQUFDO0FBRXpCOzs7Ozs7R0FNRztBQUNHLG9CQUFzQixRQUFxRDtJQUM3RSxJQUFJLENBQUMsR0FBRyxRQUFRLFlBQVksb0VBQVEsQ0FBQyxDQUFDLENBQUMsUUFBUSxDQUFDLENBQUMsQ0FBQyxRQUFRLENBQUMsUUFBUSxDQUFDO0lBQ3BFLElBQUksYUFBYSxHQUFHLElBQUksQ0FBQyxLQUFLLENBQUUsSUFBSSxDQUFDLEdBQUcsQ0FBRSxDQUFDLEVBQUUsQ0FBQyxDQUFDLE9BQU8sRUFBRSxHQUFHLFdBQVcsQ0FBRSxDQUFFLENBQUM7SUFDM0UsTUFBTSxDQUFDLENBQUUsUUFBUSxZQUFZLHFGQUFlLENBQUUsQ0FBQyxDQUFDLENBQUMsUUFBUSxDQUFDLGNBQWMsR0FBRyxhQUFhLENBQUMsQ0FBQyxDQUFDLGFBQWEsQ0FBQztBQUM3RyxDQUFDO0FBRUQ7Ozs7OztHQU1HO0FBQ0csMEJBQTRCLFFBQW1DO0lBQ2pFLElBQUksQ0FBQyxHQUFHLFFBQVEsWUFBWSxvRkFBYyxDQUFDLENBQUMsQ0FBQyxRQUFRLENBQUMsQ0FBQyxDQUFDLFFBQVEsQ0FBQyxjQUFjLENBQUM7SUFDaEYsdUNBQXVDO0lBQ3ZDLHNEQUFzRDtJQUN0RCxNQUFNLENBQUMsSUFBSSxDQUFDLEdBQUcsQ0FBRSxDQUFDLEVBQUUsQ0FBQyxDQUFDLFNBQVMsSUFBSSxDQUFDLENBQUUsQ0FBQyxDQUFDLHlCQUF5QjtBQUNyRSxDQUFDO0FBRUQ7Ozs7OztHQU1HO0FBQ0cscUJBQXVCLFFBQXFEO0lBQzlFLE1BQU0sQ0FBQyxVQUFVLENBQUUsUUFBUSxDQUFFLENBQUM7QUFDbEMsQ0FBQztBQUVEOzs7O0dBSUc7QUFDRyxnQ0FBa0MsUUFBd0I7SUFDNUQsTUFBTSxDQUFDLFVBQVUsQ0FBRSxRQUFRLENBQUUsR0FBRyxtQkFBbUIsQ0FBRSxRQUFRLENBQUUsQ0FBQztBQUNwRSxDQUFDO0FBRUQ7Ozs7Ozs7O0dBUUc7QUFDRyxnQ0FBa0MsU0FBMkM7SUFDL0UsTUFBTSxDQUFDLFNBQVMsQ0FBQyxTQUFTLEdBQUcsQ0FBRSxTQUFTLENBQUMsT0FBTyxHQUFHLENBQUMsQ0FBRSxHQUFHLENBQUMsQ0FBQztBQUMvRCxDQUFDO0FBRUQ7Ozs7Ozs7O0dBUUc7QUFDRyw2QkFBK0IsUUFBd0I7SUFDekQsTUFBTSxDQUFDLFFBQVEsQ0FBQyxPQUFPLEdBQUcsRUFBRSxDQUFDO0FBQ2pDLENBQUM7Ozs7Ozs7Ozs7QUNuRkQ7QUFBQTs7R0FFRztBQUVIOztHQUVHO0FBQ3dHO0FBQ1g7QUFVaEc7O0dBRUc7QUFDSCxJQUFZLFdBRVg7QUFGRCxXQUFZLFdBQVc7SUFDbkIsZ0NBQWlCO0lBQUUsOEJBQWU7SUFBRSw0QkFBYTtJQUFFLDRDQUE2QjtBQUNwRixDQUFDLEVBRlcsV0FBVyxLQUFYLFdBQVcsUUFFdEI7QUFFRDs7R0FFRztBQUNHO0lBQU47UUFFYSxnQkFBVyxHQUFHLHFEQUFVLENBQUM7SUFNdEMsQ0FBQztJQUxHLE9BQU8sS0FBSyxNQUFNLENBQUMsY0FBYyxDQUFDLElBQUksRUFBQyxDQUFDO0lBQUEsQ0FBQztJQUN6QyxPQUFPLENBQUUsT0FBWTtRQUNqQixJQUFJLE9BQU8sR0FBZ0IsbUVBQVcsQ0FBQyxRQUFRLENBQUUsT0FBTyxDQUFFO1FBQzFELElBQUksQ0FBQyxXQUFXLENBQUMsSUFBSSxDQUFFLElBQUksRUFBRSxPQUFPLENBQUMsT0FBTyxDQUFFLENBQUM7SUFDbkQsQ0FBQzs7OztBQU5lLG1CQUFJLEdBQUcsV0FBVyxDQUFDLFlBQVksQ0FBQztBQVNwRDs7R0FFRztBQUNHO0lBQU47UUFFYSxpQkFBWSxHQUFHLDREQUFpQixDQUFDO0lBUTlDLENBQUM7SUFQRyxPQUFPLEtBQUssTUFBTSxDQUFDLGFBQWEsQ0FBQyxJQUFJLEVBQUMsQ0FBQztJQUFBLENBQUM7SUFDeEMsT0FBTyxDQUFFLE9BQVk7UUFDakIsSUFBSSxNQUFNLEdBQWlCLG9FQUFZLENBQUMsUUFBUSxDQUFFLE9BQU8sQ0FBRTtRQUMzRCxJQUFJLEtBQUssR0FBcUIsTUFBTSxDQUFDLGNBQWMsQ0FBQyxHQUFHLENBQUUsQ0FBQyxDQUFDLEVBQUUsQ0FBQyxvRUFBWSxDQUFDLGdCQUFnQixDQUFFLENBQUMsQ0FBRSxDQUFFLENBQUM7UUFDbkcsSUFBSSxRQUFRLEdBQXFCLE1BQU0sQ0FBQyxpQkFBaUIsQ0FBQyxHQUFHLENBQUUsQ0FBQyxDQUFDLEVBQUUsQ0FBQyxvRUFBWSxDQUFDLGdCQUFnQixDQUFFLENBQUMsQ0FBRSxDQUFFLENBQUM7UUFDekcsSUFBSSxDQUFDLFlBQVksQ0FBQyxJQUFJLENBQUUsSUFBSSxFQUFFLFFBQVEsRUFBRSxLQUFLLENBQUUsQ0FBQztJQUNwRCxDQUFDOzs7O0FBUmUsa0JBQUksR0FBRyxXQUFXLENBQUMsTUFBTSxDQUFDO0FBVzlDOztHQUVHO0FBQ0c7SUFBTjtRQUdhLGdCQUFXLEdBQUcsMkRBQWdCLENBQUM7SUFLNUMsQ0FBQztJQU5HLE9BQU8sS0FBSyxNQUFNLENBQUMsWUFBWSxDQUFDLElBQUksRUFBQyxDQUFDO0lBQUEsQ0FBQztJQUV2QyxPQUFPLENBQUUsT0FBWTtRQUNqQixJQUFJLEtBQUssR0FBZSxrRUFBVSxDQUFDLFFBQVEsQ0FBRSxPQUFPLENBQUUsQ0FBQztRQUN2RCxJQUFJLENBQUMsV0FBVyxDQUFDLElBQUksQ0FBRSxJQUFJLEVBQUUsS0FBSyxDQUFDLGlCQUFpQixDQUFDLEdBQUcsQ0FBRSxDQUFDLENBQUMsRUFBRSxDQUFDLG9FQUFZLENBQUMsZ0JBQWdCLENBQUUsQ0FBQyxDQUFFLENBQUUsQ0FBRSxDQUFDO0lBQzFHLENBQUM7Ozs7QUFOZSxpQkFBSSxHQUFHLFdBQVcsQ0FBQyxLQUFLLENBQUM7QUFTN0M7O0dBRUc7QUFDRztJQUFOO1FBR2EsbUJBQWMsR0FBRyw4REFBbUIsQ0FBQztJQUtsRCxDQUFDO0lBTkcsT0FBTyxLQUFLLE1BQU0sQ0FBQyxXQUFXLENBQUMsSUFBSSxFQUFDLENBQUM7SUFBQSxDQUFDO0lBRXRDLE9BQU8sQ0FBRSxPQUFZO1FBQ2pCLElBQUksWUFBWSxHQUFhLGdFQUFRLENBQUMsUUFBUSxDQUFFLE9BQU8sQ0FBRSxDQUFDO1FBQzFELElBQUksQ0FBQyxjQUFjLENBQUMsSUFBSSxDQUFFLElBQUksRUFBRSxZQUFZLENBQUMsUUFBUSxFQUFFLFlBQVksQ0FBQyxZQUFZLENBQUMsaUJBQWlCLENBQUMsR0FBRyxDQUFFLENBQUMsQ0FBQyxFQUFFLENBQUMsb0VBQVksQ0FBQyxnQkFBZ0IsQ0FBRSxDQUFDLENBQUUsQ0FBRSxDQUFFLENBQUM7SUFDeEosQ0FBQzs7OztBQU5lLGdCQUFJLEdBQUcsV0FBVyxDQUFDLElBQUksQ0FBQztBQVM1Qzs7RUFFRTtBQUNJO0lBQU47UUFHYSxnQkFBVyxHQUFHLGlEQUFNLENBQUM7SUFJbEMsQ0FBQztJQUxHLE9BQU8sS0FBSyxNQUFNLENBQUMscUJBQXFCLENBQUMsSUFBSSxFQUFDLENBQUM7SUFBQSxDQUFDO0lBRWhELE9BQU8sQ0FBRSxPQUFhO1FBQ2xCLHlEQUFNLEVBQUUsQ0FBQztJQUNiLENBQUM7Ozs7QUFMZSwwQkFBSSxHQUFHLFNBQVMsQ0FBQzs7Ozs7Ozs7Ozs7Ozs7Ozs7QUNuRlc7QUFFb0I7QUFDdkI7QUFDRjtBQUNUO0FBQ1c7QUFDb0I7QUFDaUM7QUFDdEQ7QUFFNUM7O0dBRUc7QUFDSCxnQ0FBZ0M7QUFDaEMsOERBQThEO0FBQzlELGtCQUFrQjtBQUNsQixvQkFBb0I7QUFDZDtJQW9CRjs7O09BR0c7SUFDSCxZQUFhLE1BQXdCO1FBdEJyQyw0QkFBNEI7UUFDcEIsZUFBVSxHQUFXLElBQUksQ0FBQztRQUMxQixhQUFRLEdBQVcsSUFBSSxDQUFDO1FBSXhCLFdBQU0sR0FBZSxJQUFJLGdFQUFVLEVBQUUsQ0FBQztRQVM5QywyRkFBMkY7UUFDbkYsdUJBQWtCLEdBQVksSUFBSSxDQUFDO1FBT3ZDLElBQUksQ0FBQyxNQUFNLEdBQUcsTUFBTSxDQUFDO1FBQ3JCLHNFQUFzRTtRQUN0RSxJQUFJLENBQUMsaUJBQWlCLEdBQUcsSUFBSSxDQUFDLGlCQUFpQixDQUFDLElBQUksQ0FBRSxJQUFJLENBQUUsQ0FBQztRQUM3RCxJQUFJLENBQUMsY0FBYyxHQUFHLElBQUksQ0FBQyxjQUFjLENBQUMsSUFBSSxDQUFFLElBQUksQ0FBRSxDQUFDO1FBQ3ZELElBQUksQ0FBQyxjQUFjLEdBQUcsSUFBSSxDQUFDLGNBQWMsQ0FBQyxJQUFJLENBQUUsSUFBSSxDQUFFLENBQUM7UUFDdkQsSUFBSSxDQUFDLFlBQVksR0FBRyxJQUFJLENBQUMsWUFBWSxDQUFDLElBQUksQ0FBRSxJQUFJLENBQUUsQ0FBQztJQUN2RCxDQUFDO0lBcEJELElBQVcsS0FBSztRQUNaLE1BQU0sQ0FBQyxJQUFJLENBQUMsTUFBTSxDQUFDO0lBQ3ZCLENBQUM7SUFxQkQ7OztPQUdHO0lBQ0gsSUFBSSxDQUFFLEtBQXdCO1FBQzFCLElBQUksQ0FBQyxLQUFLLEdBQUcsS0FBSyxDQUFDO1FBQ25CLElBQUksQ0FBQyxNQUFNLEdBQUcsSUFBSSx1REFBTSxDQUFFLEtBQUssQ0FBRSxDQUFDO1FBQ2xDLElBQUksQ0FBQyxPQUFPLEdBQUcsSUFBSSxpRUFBUSxDQUFFLElBQUksQ0FBQyxNQUFNLENBQUUsQ0FBQztRQUMzQyxJQUFJLENBQUMsTUFBTSxDQUFDLE1BQU0sR0FBRyxJQUFJLENBQUMsS0FBSyxDQUFDLFVBQVUsQ0FBRSxJQUFJLENBQUUsQ0FBQztRQUNuRCxFQUFFLENBQUMsQ0FBRSxDQUFDLElBQUksQ0FBQyxNQUFPLENBQUMsQ0FBQyxDQUFDO1lBQUMsS0FBSyxDQUFFLG1DQUFtQyxDQUFFLENBQUM7WUFBQyxNQUFNLENBQUM7UUFBQyxDQUFDO1FBQzdFLDRDQUE0QztRQUM1QyxJQUFJLENBQUMsS0FBSyxDQUFDLGdCQUFnQixDQUFFLFdBQVcsRUFBRSxJQUFJLENBQUMsY0FBYyxDQUFFLENBQUM7UUFDaEUsSUFBSSxDQUFDLEtBQUssQ0FBQyxnQkFBZ0IsQ0FBRSxTQUFTLEVBQUUsSUFBSSxDQUFDLFlBQVksQ0FBRSxDQUFDO1FBQzVELElBQUksQ0FBQyxLQUFLLENBQUMsZ0JBQWdCLENBQUUsV0FBVyxFQUFFLElBQUksQ0FBQyxjQUFjLENBQUU7UUFDL0QsSUFBSSxDQUFDLEtBQUssQ0FBQyxnQkFBZ0IsQ0FBRSxXQUFXLEVBQUUsR0FBRyxFQUFFLENBQUMsS0FBSyxDQUFDLGNBQWMsRUFBRSxDQUFFO1FBQ3hFLElBQUksQ0FBQyxRQUFRLEdBQUcsSUFBSSxRQUFRLENBQUUsSUFBSSxDQUFDLEtBQUssQ0FBRSxDQUFDO1FBQzNDLElBQUksQ0FBQyxNQUFNLENBQUMsY0FBYyxFQUFFLENBQUM7SUFDakMsQ0FBQztJQUVEOzs7T0FHRztJQUNILFFBQVEsQ0FBRSxJQUFZLEVBQUUsUUFBMEI7UUFDOUMsSUFBSSxDQUFDLE1BQU0sQ0FBQyxRQUFRLEdBQUcsSUFBSSxDQUFDO1FBQzVCLElBQUksQ0FBQyxnQkFBZ0IsQ0FBRSxRQUFRLENBQUUsQ0FBQztJQUN0QyxDQUFDO0lBRUQ7Ozs7T0FJRztJQUNLLGdCQUFnQixDQUFFLFVBQTBCO1FBQ2hELElBQUksQ0FBQyxLQUFLLENBQUMsbUJBQW1CLENBQUUsVUFBVSxDQUFDLEtBQUssQ0FBRSxDQUFDLElBQUksQ0FBRSxVQUFVLENBQUUsQ0FBQztJQUMxRSxDQUFDO0lBRUQ7OztPQUdHO0lBQ0sseUJBQXlCLENBQUUsUUFBd0I7UUFDdkQsR0FBRyxDQUFDLENBQUUsSUFBSSxJQUFJLElBQUksUUFBUSxDQUFDLFFBQVEsQ0FBQyxLQUFNLENBQUMsQ0FBQyxDQUFDO1lBQ3pDLElBQUksSUFBSSxHQUFHLElBQUksQ0FBQyxXQUFXLEVBQUUsQ0FBQztZQUM5QixFQUFFLENBQUMsQ0FBRSxJQUFLLENBQUM7Z0JBQUMsSUFBSSxDQUFDLEtBQUssQ0FBQyxhQUFhLENBQUMsSUFBSSxDQUFFLElBQUksQ0FBRSxDQUFDO1FBQ3RELENBQUM7UUFDRCw0RUFBYyxDQUFFLFFBQVEsRUFBRSxJQUFJLENBQUMsS0FBSyxDQUFDLG1CQUFtQixDQUFFLFFBQVEsQ0FBQyxLQUFLLENBQUUsQ0FBRSxDQUFDO1FBQzdFLFFBQVEsQ0FBQyxJQUFJLEVBQUUsQ0FBQztJQUNwQixDQUFDO0lBRUQ7OztPQUdHO0lBQ0ssY0FBYyxDQUFFLENBQWE7UUFDakMsS0FBSyxDQUFDLGNBQWMsRUFBRSxDQUFDO1FBQ3ZCLEVBQUUsQ0FBQyxDQUFFLElBQUksQ0FBQyxrQkFBbUIsQ0FBQztZQUFDLE1BQU0sQ0FBQztRQUN0QyxJQUFJLENBQUMsVUFBVSxHQUFHLElBQUksd0VBQU0sQ0FBRSxDQUFDLENBQUMsS0FBSyxHQUFHLElBQUksQ0FBQyxLQUFLLENBQUMsVUFBVSxFQUFFLENBQUMsQ0FBQyxLQUFLLEdBQUcsSUFBSSxDQUFDLEtBQUssQ0FBQyxTQUFTLENBQUUsQ0FBQztRQUNoRyxFQUFFLENBQUMsQ0FBRSxDQUFDLElBQUksQ0FBQyxPQUFPLENBQUMscUJBQXFCLENBQUUsSUFBSSxDQUFDLFVBQVUsRUFBRSxJQUFJLENBQUMsTUFBTSxDQUFHLENBQUMsQ0FBQyxDQUFDO1lBQ3hFLElBQUksQ0FBQyxVQUFVLEdBQUcsSUFBSSxDQUFDO1lBQ3ZCLElBQUksQ0FBQyxRQUFRLEdBQUcsSUFBSSxDQUFDO1lBQ3JCLE1BQU0sQ0FBQztRQUNYLENBQUM7UUFDRCxJQUFJLENBQUMsUUFBUSxHQUFHLElBQUksQ0FBQztJQUN6QixDQUFDO0lBRUQ7OztPQUdHO0lBQ0ssY0FBYyxDQUFFLENBQWE7UUFDakMsS0FBSyxDQUFDLGNBQWMsRUFBRSxDQUFDO1FBQ3ZCLEVBQUUsQ0FBQyxDQUFFLElBQUksQ0FBQyxrQkFBbUIsQ0FBQztZQUFDLE1BQU0sQ0FBQztRQUN0QyxFQUFFLENBQUMsQ0FBRSxDQUFDLElBQUksQ0FBQyxVQUFXLENBQUMsQ0FBQyxDQUFDO1lBQ3JCLElBQUksQ0FBQyxlQUFlLEdBQUcsSUFBSSxDQUFDLHFCQUFxQixDQUFFLElBQUksd0VBQU0sQ0FBRSxDQUFDLENBQUMsS0FBSyxHQUFHLElBQUksQ0FBQyxLQUFLLENBQUMsVUFBVSxFQUFFLENBQUMsQ0FBQyxLQUFLLEdBQUcsSUFBSSxDQUFDLEtBQUssQ0FBQyxTQUFTLENBQUUsQ0FBRTtZQUNsSSxFQUFFLENBQUMsQ0FBRSxJQUFJLENBQUMsZUFBZSxJQUFJLElBQUksQ0FBQyxlQUFlLENBQUMsS0FBSyxLQUFLLElBQUksQ0FBQyxLQUFLLENBQUMsUUFBUyxDQUFDO2dCQUFDLElBQUksQ0FBQyw4QkFBOEIsRUFBRSxDQUFDO1lBQ3hILE1BQU0sQ0FBQztRQUNYLENBQUM7UUFBQyxJQUFJO1lBQUMsSUFBSSxDQUFDLGVBQWUsR0FBRyxJQUFJLENBQUMsQ0FBQyxxR0FBcUc7UUFDekksSUFBSSxDQUFDLE1BQU0sQ0FBQyxVQUFVLENBQUUsSUFBSSxDQUFDLEtBQUssQ0FBRSxDQUFDO1FBQ3JDLElBQUksQ0FBQyxRQUFRLEdBQUcsSUFBSSx3RUFBTSxDQUFFLENBQUMsQ0FBQyxLQUFLLEdBQUcsSUFBSSxDQUFDLEtBQUssQ0FBQyxVQUFVLEVBQUUsQ0FBQyxDQUFDLEtBQUssR0FBRyxJQUFJLENBQUMsS0FBSyxDQUFDLFNBQVMsQ0FBRSxDQUFDO1FBQzlGLEVBQUUsQ0FBQyxDQUFFLGdEQUFLLElBQUksQ0FBRSxDQUFDLENBQUMsQ0FBQztZQUNmLElBQUksQ0FBQyxNQUFNLENBQUMsUUFBUSxDQUFFLElBQUksQ0FBQyxVQUFVLEVBQUUsSUFBSSxDQUFDLFFBQVEsQ0FBRSxDQUFDO1FBQzNELENBQUM7UUFBQyxJQUFJLENBQUMsQ0FBQztZQUNKLElBQUksQ0FBQyxlQUFlLEdBQUcscUVBQWMsQ0FBQyxFQUFFLENBQUUsSUFBSSxDQUFDLE9BQU8sQ0FBQyxxQkFBcUIsQ0FBRSxJQUFJLENBQUMsVUFBVSxFQUFFLElBQUksQ0FBQyxRQUFRLEVBQUUsSUFBSSxDQUFDLE1BQU0sQ0FBRSxFQUFFLElBQUksQ0FBQyxNQUFNLENBQUMsUUFBUSxDQUFFLENBQUM7WUFDcEosRUFBRSxDQUFDLENBQUUsSUFBSSxDQUFDLGVBQWdCLENBQUMsQ0FBQyxDQUFDO2dCQUN6QixFQUFFLENBQUMsQ0FBRSxJQUFJLENBQUMsdUJBQXVCLEVBQUcsQ0FBQztvQkFDakMsSUFBSSxDQUFDLE1BQU0sQ0FBQyxJQUFJLENBQUUsSUFBSSxDQUFDLGVBQWUsQ0FBRSxDQUFDO1lBQ2pELENBQUM7UUFDTCxDQUFDO1FBQ0QsSUFBSSxDQUFDLDhCQUE4QixFQUFFLENBQUM7SUFDMUMsQ0FBQztJQUVEOztPQUVHO0lBQ0ssdUJBQXVCO1FBQzNCLHlEQUF5RDtRQUN6RCxJQUFJLFVBQVUsR0FBZSxJQUFJLENBQUMsT0FBTyxDQUFDLHFCQUFxQixDQUFFLElBQUksQ0FBQyxlQUFlLENBQUMsUUFBUSxFQUFFLElBQUksQ0FBQyxLQUFLLENBQUUsQ0FBQztRQUM3RyxJQUFJLGdCQUFnQixHQUFHLENBQUMsQ0FBQztRQUN6QixHQUFHLENBQUMsQ0FBRSxJQUFJLFNBQVMsSUFBSSxVQUFXLENBQUMsQ0FBQyxDQUFDO1lBQ2pDLEVBQUUsQ0FBQyxDQUFFLFNBQVMsQ0FBQyxjQUFjLENBQUMsS0FBSyxLQUFLLElBQUksQ0FBQyxLQUFLLENBQUMsUUFBUyxDQUFDO2dCQUFDLE1BQU0sQ0FBQyxLQUFLLENBQUM7WUFDM0UsZ0JBQWdCLElBQUkseUZBQXNCLENBQUUsU0FBUyxDQUFDLGNBQWMsQ0FBRSxDQUFDO1FBQzNFLENBQUM7UUFDRCxFQUFFLENBQUMsQ0FBRSxJQUFJLENBQUMsZUFBZSxZQUFZLHFFQUFlLENBQUM7WUFDakQsSUFBSSxDQUFDLGVBQWUsR0FBRyxJQUFJLHNFQUFlLENBQUUsSUFBSSxDQUFDLGVBQWUsQ0FBRSxDQUFDO1FBQ3ZFLElBQUksQ0FBQyxlQUFlLENBQUMsY0FBYyxHQUFHLGdCQUFnQixDQUFDO1FBQ3ZELElBQUksQ0FBQyw4QkFBOEIsRUFBRSxDQUFDO1FBQ3RDLE1BQU0sQ0FBQyxJQUFJLENBQUM7SUFDaEIsQ0FBQztJQUVEOzs7T0FHRztJQUNLLFlBQVksQ0FBRSxDQUFhO1FBQy9CLEtBQUssQ0FBQyxjQUFjLEVBQUUsQ0FBQztRQUN2QixJQUFJLENBQUMsUUFBUSxDQUFDLFNBQVMsQ0FBRSxJQUFJLENBQUUsQ0FBQztRQUNoQyxFQUFFLENBQUMsQ0FBRSxJQUFJLENBQUMsa0JBQW1CLENBQUM7WUFBQyxNQUFNLENBQUM7UUFDdEMsRUFBRSxDQUFDLENBQUUsQ0FBQyxJQUFJLENBQUMsUUFBUyxDQUFDO1lBQUMsSUFBSSxDQUFDLFFBQVEsR0FBRyxJQUFJLHdFQUFNLENBQUUsQ0FBQyxDQUFDLEtBQUssR0FBRyxJQUFJLENBQUMsS0FBSyxDQUFDLFVBQVUsRUFBRSxDQUFDLENBQUMsS0FBSyxHQUFHLElBQUksQ0FBQyxLQUFLLENBQUMsU0FBUyxDQUFFLENBQUM7UUFDcEgsRUFBRSxDQUFDLENBQUUsSUFBSSxDQUFDLGVBQWUsSUFBSSxJQUFJLENBQUMsZUFBZSxDQUFDLEtBQUssS0FBSyxJQUFJLENBQUMsS0FBSyxDQUFDLFFBQVMsQ0FBQyxDQUFDLENBQUM7WUFDL0UsK0NBQStDO1lBQy9DLEVBQUUsQ0FBQyxDQUFFLElBQUksQ0FBQyxlQUFlLENBQUMsT0FBTyxHQUFHLHFFQUFjLENBQUMsV0FBWSxDQUFDLENBQUMsQ0FBQztnQkFDOUQsSUFBSSxlQUFlLEdBQUcsSUFBSSxzRUFBZSxDQUFFLElBQUksQ0FBQyxlQUFlLENBQUUsQ0FBQztnQkFDbEUsSUFBSSxZQUFZLEdBQUcseUZBQXNCLENBQUUsSUFBSSxDQUFDLGVBQWUsQ0FBRSxDQUFDO2dCQUNsRSxFQUFFLENBQUMsQ0FBRSxZQUFZLElBQUksSUFBSSxDQUFDLEtBQUssQ0FBQyxPQUFRLENBQUMsQ0FBQyxDQUFDO29CQUN2QyxlQUFlLENBQUMsT0FBTyxFQUFFLENBQUM7b0JBQzFCLElBQUksQ0FBQyxNQUFNLENBQUMsZUFBZSxDQUFFLGVBQWUsQ0FBQyxjQUFjLENBQUUsQ0FBQztvQkFDOUQsSUFBSSxDQUFDLEtBQUssQ0FBQyxPQUFPLElBQUksWUFBWSxDQUFDLENBQUMsNENBQTRDO2dCQUNwRixDQUFDO1lBQ0wsQ0FBQztZQUNELElBQUksQ0FBQyxVQUFVLEdBQUcsSUFBSSxDQUFDO1lBQ3ZCLElBQUksQ0FBQyxRQUFRLEdBQUcsSUFBSSxDQUFDO1lBQ3JCLE1BQU0sQ0FBQztRQUNYLENBQUM7UUFFRCxFQUFFLENBQUMsQ0FBRSxnREFBSyxJQUFJLENBQUUsQ0FBQyxDQUFDLENBQUM7WUFDZixJQUFJLENBQUMsZUFBZSxHQUFHLHFFQUFjLENBQUMsRUFBRSxDQUFFLElBQUksQ0FBQyxPQUFPLENBQUMscUJBQXFCLENBQUUsSUFBSSxDQUFDLFVBQVUsRUFBRSxJQUFJLENBQUMsUUFBUSxFQUFFLElBQUksQ0FBQyxNQUFNLENBQUUsRUFBRSxJQUFJLENBQUMsTUFBTSxDQUFDLFFBQVEsQ0FBRSxDQUFDO1lBQ3BKLEVBQUUsQ0FBQyxDQUFFLElBQUksQ0FBQyxlQUFnQixDQUFDO2dCQUFDLElBQUksQ0FBQyxNQUFNLENBQUMsSUFBSSxDQUFFLElBQUksQ0FBQyxlQUFlLENBQUUsQ0FBQztRQUN6RSxDQUFDO1FBQ0QsRUFBRSxDQUFDLENBQUUsZ0RBQU0sQ0FBQztZQUFDLElBQUksQ0FBQyxlQUFlLEdBQUcscUVBQWMsQ0FBQyxFQUFFLENBQUUsSUFBSSxDQUFDLE9BQU8sQ0FBQyxxQkFBcUIsQ0FBRSxJQUFJLENBQUMsVUFBVSxFQUFFLElBQUksQ0FBQyxRQUFRLEVBQUUsSUFBSSxDQUFDLE1BQU0sQ0FBRSxFQUFFLElBQUksQ0FBQyxNQUFNLENBQUMsUUFBUSxDQUFFLENBQUM7UUFDakssRUFBRSxDQUFDLENBQUUsSUFBSSxDQUFDLGVBQWdCLENBQUMsQ0FBQyxDQUFDO1lBQ3pCLElBQUksYUFBYSxHQUFHLDhFQUFXLENBQUUsSUFBSSxDQUFDLGVBQWUsQ0FBRSxDQUFDO1lBQ3hELEVBQUUsQ0FBQyxDQUFFLGFBQWEsSUFBSSxJQUFJLENBQUMsS0FBSyxDQUFDLE9BQVEsQ0FBQyxDQUFDLENBQUM7Z0JBQ3hDLElBQUksQ0FBQyxNQUFNLENBQUMsZUFBZSxDQUFFLElBQUksQ0FBQyxlQUFlLFlBQVksc0VBQWUsQ0FBQyxDQUFDLENBQUMsSUFBSSxDQUFDLGVBQWUsQ0FBQyxjQUFjLENBQUMsQ0FBQyxDQUFDLElBQUksQ0FBQyxlQUFlLENBQUUsQ0FBQztnQkFDNUksSUFBSSxDQUFDLEtBQUssQ0FBQyxPQUFPLElBQUksYUFBYSxDQUFDLENBQUMsOENBQThDO1lBQ3ZGLENBQUM7WUFDRCxJQUFJLENBQUMsTUFBTSxDQUFDLFVBQVUsQ0FBRSxJQUFJLENBQUMsS0FBSyxDQUFFLENBQUM7WUFDckMsSUFBSSxDQUFDLGVBQWUsR0FBRyxJQUFJLENBQUM7UUFDaEMsQ0FBQztRQUNELElBQUksQ0FBQyxVQUFVLEdBQUcsSUFBSSxDQUFDO1FBQ3ZCLElBQUksQ0FBQyxRQUFRLEdBQUcsSUFBSSxDQUFDO0lBQ3pCLENBQUM7SUFFRDs7OztPQUlHO0lBQ0gsaUJBQWlCLENBQUUsUUFBMEIsRUFBRSxLQUF1QjtRQUNsRSxHQUFHLENBQUMsQ0FBRSxJQUFJLE1BQU0sSUFBSSxRQUFTLENBQUMsQ0FBQyxDQUFDO1lBQzVCLElBQUksT0FBTyxHQUFZLElBQUksQ0FBQyxvQkFBb0IsQ0FBRSxNQUFNLENBQUUsQ0FBQztZQUMzRCxFQUFFLENBQUMsQ0FBRSxDQUFDLE9BQVEsQ0FBQyxDQUFDLENBQUM7Z0JBQ2IsSUFBSSxDQUFDLE1BQU0sQ0FBQyxvQkFBb0IsRUFBRSxDQUFDO2dCQUNuQyxJQUFJLENBQUMsa0JBQWtCLEdBQUcsSUFBSSxDQUFDO2dCQUMvQixNQUFNLENBQUM7WUFDWCxDQUFDO1FBQ0wsQ0FBQztRQUNELEdBQUcsQ0FBQyxDQUFFLElBQUksR0FBRyxJQUFJLEtBQU0sQ0FBQyxDQUFDLENBQUM7WUFDdEIsSUFBSSxPQUFPLEdBQVksSUFBSSxDQUFDLGlCQUFpQixDQUFFLEdBQUcsQ0FBRSxDQUFDO1lBQ3JELEVBQUUsQ0FBQyxDQUFFLENBQUMsT0FBUSxDQUFDLENBQUMsQ0FBQztnQkFDYixJQUFJLENBQUMsTUFBTSxDQUFDLG9CQUFvQixFQUFFLENBQUM7Z0JBQ25DLElBQUksQ0FBQyxrQkFBa0IsR0FBRyxJQUFJLENBQUM7Z0JBQy9CLE1BQU0sQ0FBQztZQUNYLENBQUM7UUFDTCxDQUFDO1FBQ0QsSUFBSSxDQUFDLHdCQUF3QixFQUFFO0lBQ25DLENBQUM7SUFFRDs7O09BR0c7SUFDSCxnQkFBZ0IsQ0FBRSxRQUEwQjtRQUN4QyxJQUFJLENBQUMsTUFBTSxDQUFDLGVBQWUsR0FBRyxFQUFFLENBQUM7UUFDakMsSUFBSSxDQUFDLE1BQU0sQ0FBQyxjQUFjLEdBQUcsRUFBRSxDQUFDO1FBQ2hDLElBQUksQ0FBQyxNQUFNLENBQUMsYUFBYSxHQUFHLEVBQUUsQ0FBQztRQUMvQixJQUFJLENBQUMsTUFBTSxDQUFDLFlBQVksR0FBRyxFQUFFLENBQUM7UUFDOUIsR0FBRyxDQUFDLENBQUUsSUFBSSxZQUFZLElBQUksUUFBUyxDQUFDLENBQUMsQ0FBQztZQUNsQyxJQUFJLE9BQU8sR0FBWSxJQUFJLENBQUMsaUJBQWlCLENBQUUsWUFBWSxDQUFFLENBQUM7WUFDOUQsRUFBRSxDQUFDLENBQUUsQ0FBQyxPQUFRLENBQUMsQ0FBQyxDQUFDO2dCQUNiLEtBQUssQ0FBRSw4QkFBOEIsQ0FBRSxDQUFDO2dCQUN4QyxNQUFNLENBQUM7WUFDWCxDQUFDO1FBQ0wsQ0FBQztRQUNELElBQUksQ0FBQyx3QkFBd0IsRUFBRSxDQUFDO0lBQ3BDLENBQUM7SUFFRCwwQ0FBMEM7SUFDMUMsTUFBTTtRQUNGLElBQUksQ0FBQyx3QkFBd0IsRUFBRSxDQUFDO0lBQ3BDLENBQUM7SUFFRDs7O09BR0c7SUFDSCxVQUFVLENBQUUsT0FBZTtRQUN2QixJQUFJLENBQUMsS0FBSyxDQUFDLE9BQU8sR0FBRyxPQUFPLENBQUM7UUFDN0IsRUFBRSxDQUFDLENBQUUsSUFBSSxDQUFDLFVBQVcsQ0FBQztZQUFDLElBQUksQ0FBQyw4QkFBOEIsRUFBRSxDQUFDO1FBQzdELElBQUk7WUFBQyxJQUFJLENBQUMsOEJBQThCLEVBQUUsQ0FBQztJQUMvQyxDQUFDO0lBRUQ7Ozs7T0FJRztJQUNLLHFCQUFxQixDQUFFLE1BQWM7UUFDekMsR0FBRyxDQUFDLENBQUUsSUFBSSxRQUFRLElBQUksSUFBSSxDQUFDLEtBQUssQ0FBQyxlQUFnQixDQUFDO1lBQzlDLEVBQUUsQ0FBQyxDQUFFLFFBQVEsQ0FBQyxRQUFRLENBQUMsUUFBUSxDQUFFLE1BQU0sQ0FBRyxDQUFDO2dCQUFDLE1BQU0sQ0FBQyxRQUFRLENBQUM7UUFDaEUsR0FBRyxDQUFDLENBQUUsSUFBSSxRQUFRLElBQUksSUFBSSxDQUFDLEtBQUssQ0FBQyxjQUFlLENBQUM7WUFDN0MsRUFBRSxDQUFDLENBQUUsUUFBUSxDQUFDLFFBQVEsQ0FBQyxRQUFRLENBQUUsTUFBTSxDQUFHLENBQUM7Z0JBQUMsTUFBTSxDQUFDLFFBQVEsQ0FBQztRQUNoRSxNQUFNLENBQUMsSUFBSSxDQUFDO0lBQ2hCLENBQUM7SUFFRDs7T0FFRztJQUNILGdFQUFnRTtJQUN4RCw4QkFBOEI7UUFDbEMsRUFBRSxDQUFDLENBQUUsSUFBSSxDQUFDLGVBQWUsSUFBSSxJQUFJLENBQUMsZUFBZSxDQUFDLE9BQU8sR0FBRyxxRUFBYyxDQUFDLFdBQVksQ0FBQyxDQUFDLENBQUM7WUFDdEYsSUFBSSxNQUFNLEdBQUcsSUFBSSxDQUFDLGVBQWUsQ0FBQyxRQUFRLENBQUMsV0FBVyxFQUFFLENBQUM7WUFDekQsSUFBSSxLQUFLLEdBQUcseUZBQXNCLENBQUUsSUFBSSxDQUFDLGVBQWUsQ0FBRSxDQUFDO1lBQzNELElBQUksQ0FBQyxRQUFRLENBQUMsUUFBUSxDQUFFLEtBQUssSUFBSSxJQUFJLENBQUMsS0FBSyxDQUFDLE9BQU8sQ0FBQyxDQUFDLENBQUMsMkRBQUssQ0FBQyxHQUFHLEVBQUUsQ0FBQyxDQUFDLENBQUMseURBQUcsQ0FBQyxHQUFHLEVBQUUsQ0FBRSxDQUFDO1lBQ2hGLElBQUksQ0FBQyxRQUFRLENBQUMsU0FBUyxDQUFFLEtBQUssQ0FBRSxDQUFDO1lBQ2pDLElBQUksQ0FBQyxRQUFRLENBQUMsYUFBYSxDQUFFLGlCQUFpQixFQUFFLEtBQUssQ0FBRSxDQUFDO1lBQ3hELElBQUksQ0FBQyxRQUFRLENBQUMsTUFBTSxDQUFFLE1BQU0sQ0FBRSxDQUFDO1FBQ25DLENBQUM7UUFBQyxJQUFJO1lBQUMsSUFBSSxDQUFDLFFBQVEsQ0FBQyxTQUFTLENBQUUsSUFBSSxDQUFFLENBQUM7SUFDM0MsQ0FBQztJQUVEOztPQUVHO0lBQ0ssOEJBQThCO1FBQ2xDLEVBQUUsQ0FBQyxDQUFFLElBQUksQ0FBQyxlQUFnQixDQUFDLENBQUMsQ0FBQztZQUN6QixJQUFJLE1BQU0sR0FBRyxJQUFJLENBQUMsZUFBZSxDQUFDLFFBQVEsQ0FBQyxXQUFXLEVBQUUsQ0FBQztZQUN6RCxJQUFJLEtBQUssR0FBRyw4RUFBVyxDQUFFLElBQUksQ0FBQyxlQUFlLENBQUUsQ0FBQztZQUNoRCxJQUFJLENBQUMsUUFBUSxDQUFDLFFBQVEsQ0FBRSxLQUFLLElBQUksSUFBSSxDQUFDLEtBQUssQ0FBQyxPQUFPLENBQUMsQ0FBQyxDQUFDLDJEQUFLLENBQUMsR0FBRyxFQUFFLENBQUMsQ0FBQyxDQUFDLHlEQUFHLENBQUMsR0FBRyxFQUFFLENBQUUsQ0FBQztZQUNoRixJQUFJLENBQUMsUUFBUSxDQUFDLFNBQVMsQ0FBRSxLQUFLLENBQUUsQ0FBQztZQUNqQyxJQUFJLENBQUMsUUFBUSxDQUFDLGFBQWEsQ0FBRSxpQkFBaUIsRUFBRSxLQUFLLENBQUUsQ0FBQztZQUN4RCxJQUFJLENBQUMsUUFBUSxDQUFDLE1BQU0sQ0FBRSxNQUFNLENBQUUsQ0FBQztRQUNuQyxDQUFDO1FBQUMsSUFBSTtZQUFDLElBQUksQ0FBQyxRQUFRLENBQUMsU0FBUyxDQUFFLElBQUksQ0FBRSxDQUFDO0lBQzNDLENBQUM7SUFFRDs7OztPQUlHO0lBQ0ssb0JBQW9CLENBQUUsUUFBd0I7UUFDbEQsSUFBSSxRQUFRLEdBQW1CLElBQUksQ0FBQyxvQkFBb0IsQ0FBRSxRQUFRLENBQUU7UUFDcEUsRUFBRSxDQUFDLENBQUUsQ0FBQyxRQUFTLENBQUMsQ0FBQyxDQUFDO1lBQ2QsTUFBTSxDQUFDLEtBQUssQ0FBQztRQUNqQixDQUFDO1FBQ0QsSUFBSSxVQUFVLEdBQVcsUUFBUSxDQUFDLFFBQVEsQ0FBQyxLQUFLLENBQUMsTUFBTSxDQUFFLENBQUMsQ0FBQyxFQUFFLENBQUMsQ0FBQyxDQUFDLFdBQVcsRUFBRSxDQUFFLENBQUMsR0FBRyxDQUFFLENBQUMsQ0FBQyxFQUFFLENBQUMsQ0FBQyxDQUFDLFdBQVcsRUFBRSxDQUFFLENBQUM7UUFDNUcsSUFBSSxDQUFDLGFBQWEsQ0FBRSxRQUFRLENBQUMsS0FBSyxFQUFFLFFBQVEsQ0FBQyxRQUFRLENBQUMsS0FBSyxFQUFFLFVBQVUsQ0FBRSxDQUFDO1FBQzFFLElBQUksQ0FBQyx5QkFBeUIsQ0FBRSxRQUFRLENBQUUsQ0FBQztRQUMzQyxNQUFNLENBQUMsSUFBSSxDQUFDO0lBQ2hCLENBQUM7SUFFRDs7Ozs7T0FLRztJQUNLLGlCQUFpQixDQUFFLGdCQUFnQztRQUN2RCxFQUFFLENBQUMsQ0FBRSxDQUFDLGdCQUFnQixJQUFJLENBQUMsZ0JBQWdCLENBQUMsS0FBSyxJQUFJLENBQUMsZ0JBQWdCLENBQUMsUUFBUSxDQUFDLFdBQVc7ZUFDcEYsZ0JBQWdCLENBQUMsUUFBUSxDQUFDLEtBQUssQ0FBQyxJQUFJLENBQUUsQ0FBQyxDQUFDLEVBQUUsQ0FBQyxDQUFDLENBQUMsSUFBSSxDQUFDLENBQUMsQ0FBQyxRQUFRLElBQUksQ0FBQyxDQUFDLENBQUMsUUFBUSxDQUFHLENBQUMsQ0FBQyxDQUFDO1lBQ3BGLElBQUksQ0FBQyx3QkFBd0IsRUFBRSxDQUFDO1lBQ2hDLE1BQU0sQ0FBQyxLQUFLLENBQUM7UUFDakIsQ0FBQztRQUVELElBQUksUUFBUSxHQUFHLElBQUksQ0FBQyxvQkFBb0IsQ0FBRSxnQkFBZ0IsQ0FBRTtRQUM1RCxFQUFFLENBQUMsQ0FBRSxRQUFTLENBQUMsQ0FBQyxDQUFDO1lBQ2IsUUFBUSxDQUFDLG9CQUFvQixDQUFFLGdCQUFnQixDQUFFLENBQUM7WUFDbEQsTUFBTSxDQUFDLElBQUksQ0FBQztRQUNoQixDQUFDO1FBR0QsSUFBSSxRQUFRLEdBQVcsSUFBSSxLQUFLLEVBQUUsQ0FBQztRQUNuQyx1Q0FBdUM7UUFDdkMsSUFBSSxRQUFRLEdBQUcsSUFBSSxDQUFDLEtBQUssQ0FBQyxpQkFBaUIsRUFBRSxDQUFDO1FBQzlDLEdBQUcsQ0FBQyxDQUFFLElBQUksV0FBVyxJQUFJLFFBQVMsQ0FBQyxDQUFDLENBQUM7WUFDakMsR0FBRyxDQUFDLENBQUUsSUFBSSxPQUFPLElBQUksZ0JBQWdCLENBQUMsUUFBUSxDQUFDLEtBQU0sQ0FBQyxDQUFDLENBQUM7Z0JBQ3BELEVBQUUsQ0FBQyxDQUFFLE9BQU8sQ0FBQyxlQUFlLENBQUUsV0FBVyxDQUFHLENBQUMsQ0FBQyxDQUFDO29CQUMzQyxFQUFFLENBQUMsQ0FBRSxnQkFBZ0IsQ0FBQyxLQUFLLElBQUksV0FBVyxDQUFDLGVBQWUsQ0FBQyxjQUFjLENBQUMsS0FBTSxDQUFDLENBQUMsQ0FBQzt3QkFDL0UsSUFBSSxDQUFDLHdCQUF3QixFQUFFLENBQUM7d0JBQ2hDLE1BQU0sQ0FBQyxLQUFLLENBQUM7b0JBQ2pCLENBQUM7b0JBQUMsSUFBSSxDQUFDLENBQUM7d0JBQ0osT0FBTyxDQUFDLFdBQVcsQ0FBRSxXQUFXLENBQUUsQ0FBQzt3QkFDbkMsUUFBUSxDQUFDLElBQUksQ0FBRSxXQUFXLENBQUUsQ0FBQztvQkFDakMsQ0FBQztnQkFDTCxDQUFDO1lBQ0wsQ0FBQztRQUNMLENBQUM7UUFHRCxJQUFJLEtBQUssR0FBVyxJQUFJLEtBQUssRUFBRSxDQUFDO1FBQ2hDLEdBQUcsQ0FBQyxDQUFFLElBQUksSUFBSSxJQUFJLGdCQUFnQixDQUFDLFFBQVEsQ0FBQyxLQUFNLENBQUMsQ0FBQyxDQUFDO1lBQ2pELEVBQUUsQ0FBQyxDQUFFLENBQUMsSUFBSSxDQUFDLFdBQVcsRUFBRyxDQUFDO2dCQUFDLEtBQUssQ0FBQyxJQUFJLENBQUUsSUFBSSxDQUFFLENBQUM7UUFDbEQsQ0FBQztRQUVELGdGQUFnRjtRQUNoRixrSkFBa0o7UUFDbEosa0dBQWtHO1FBQ2xHLHVDQUF1QztRQUN2QyxvQkFBb0I7UUFDcEIsSUFBSTtRQUNKLElBQUksQ0FBQyxnQkFBZ0IsQ0FBRSxnQkFBZ0IsQ0FBRSxDQUFDO1FBQzFDLElBQUksQ0FBQyxhQUFhLENBQUUsZ0JBQWdCLENBQUMsS0FBSyxFQUFFLFFBQVEsRUFBRSxLQUFLLENBQUUsQ0FBQztRQUM5RCxJQUFJLENBQUMsd0JBQXdCLEVBQUUsQ0FBQztRQUNoQyxNQUFNLENBQUMsSUFBSSxDQUFDO0lBQ2hCLENBQUM7SUFFRDs7T0FFRztJQUNLLHdCQUF3QjtRQUM1QixJQUFJLENBQUMsTUFBTSxDQUFDLFVBQVUsQ0FBRSxJQUFJLENBQUMsS0FBSyxDQUFFLENBQUM7UUFDckMsSUFBSSxDQUFDLGtCQUFrQixHQUFHLEtBQUssQ0FBQztJQUNwQyxDQUFDO0lBR0QsbUZBQW1GO0lBR25GOzs7T0FHRztJQUNILGdCQUFnQixDQUFFLE1BQWM7UUFDNUIsR0FBRyxDQUFDLENBQUUsSUFBSSxJQUFJLElBQUksSUFBSSxDQUFDLE1BQU0sQ0FBQyxhQUFjLENBQUMsQ0FBQyxDQUFDO1lBQzNDLElBQUksQ0FBQyxNQUFNLENBQUMsU0FBUyxDQUFFLElBQUksQ0FBRSxDQUFDO1FBQ2xDLENBQUM7SUFDTCxDQUFDO0lBRUQ7OztPQUdHO0lBQ0gsa0JBQWtCLENBQUUsTUFBYztRQUM5QixHQUFHLENBQUMsQ0FBRSxJQUFJLElBQUksSUFBSSxJQUFJLENBQUMsTUFBTSxDQUFDLGFBQWMsQ0FBQyxDQUFDLENBQUM7WUFDM0MsSUFBSSxDQUFDLE1BQU0sQ0FBQyxXQUFXLENBQUUsSUFBSSxDQUFFLENBQUM7UUFDcEMsQ0FBQztJQUNMLENBQUM7SUFFRDs7Ozs7T0FLRztJQUNLLGFBQWEsQ0FBRSxNQUFjLEVBQUUsUUFBZ0IsRUFBRSxLQUFhO1FBQ2xFLDBEQUEwRDtRQUMxRCxJQUFJLE9BQU8sR0FBVyxNQUFNLEtBQUssSUFBSSxDQUFDLE1BQU0sQ0FBQyxRQUFRLENBQUMsQ0FBQyxDQUFDLElBQUksQ0FBQyxNQUFNLENBQUMsYUFBYSxDQUFDLENBQUMsQ0FBQyxJQUFJLENBQUMsTUFBTSxDQUFDLFlBQVksQ0FBQztRQUM3RyxHQUFHLENBQUMsQ0FBRSxJQUFJLElBQUksSUFBSSxRQUFTLENBQUM7WUFBQyw0RUFBYyxDQUFFLElBQUksRUFBRSxPQUFPLENBQUUsQ0FBQztRQUM3RCxFQUFFLENBQUMsQ0FBRSxNQUFNLEtBQUssSUFBSSxDQUFDLE1BQU0sQ0FBQyxRQUFTLENBQUM7WUFBQyxJQUFJLENBQUMsTUFBTSxDQUFDLGFBQWEsR0FBRyxPQUFPLENBQUMsTUFBTSxDQUFFLEtBQUssQ0FBRSxDQUFDO1FBQzNGLElBQUk7WUFBQyxJQUFJLENBQUMsTUFBTSxDQUFDLFlBQVksR0FBRyxPQUFPLENBQUMsTUFBTSxDQUFFLEtBQUssQ0FBRSxDQUFDO0lBQzVELENBQUM7SUFFRDs7Ozs7O09BTUc7SUFDSyxnQkFBZ0IsQ0FBRSxJQUFVLEVBQUUseUJBQTJDO1FBQzdFLDJFQUEyRTtRQUMzRSxJQUFJLENBQUMsa0JBQWtCLENBQUUsSUFBSSxDQUFDLE1BQU0sQ0FBQyxRQUFRLENBQUUsQ0FBQztRQUNoRDs7V0FFRztRQUNILElBQUksV0FBVyxHQUFTLHlCQUF5QixDQUFDLElBQUksQ0FBRSxJQUFJLEVBQUUsSUFBSSxDQUFFLENBQUM7UUFDckUsRUFBRSxDQUFDLENBQUUsQ0FBQyxXQUFZLENBQUM7WUFBQyxNQUFNLENBQUMsSUFBSSxDQUFDO1FBQ2hDOzs7V0FHRztRQUNILE9BQVEsQ0FBQyxJQUFJLENBQUMsZUFBZSxDQUFFLFdBQVcsQ0FBRSxFQUFHLENBQUM7WUFDNUMsRUFBRSxDQUFDLENBQUUsQ0FBQyxXQUFXLENBQUMsV0FBVyxFQUFHLENBQUM7Z0JBQUMsTUFBTSxDQUFDLElBQUksQ0FBQztZQUM5QyxXQUFXLEdBQUcseUJBQXlCLENBQUMsSUFBSSxDQUFFLElBQUksRUFBRSxXQUFXLENBQUMsV0FBVyxFQUFFLENBQUUsQ0FBQztZQUNoRiwyQ0FBMkM7WUFDM0MscURBQXFEO1lBQ3JELHVEQUF1RDtZQUN2RCx1REFBdUQ7WUFDdkQsZUFBZTtRQUNuQixDQUFDO1FBQ0QsTUFBTSxDQUFDLFdBQVcsQ0FBQztJQUN2QixDQUFDO0lBR0Q7Ozs7T0FJRztJQUNLLG9CQUFvQixDQUFFLGVBQStCO1FBQ3pELEdBQUcsQ0FBQyxDQUFFLElBQUksYUFBYSxJQUFJLElBQUksQ0FBQyxLQUFLLENBQUMsbUJBQW1CLENBQUUsZUFBZSxDQUFDLEtBQUssQ0FBRyxDQUFDLENBQUMsQ0FBQztZQUNsRixFQUFFLENBQUMsQ0FBRSxlQUFlLENBQUMsTUFBTSxDQUFFLGFBQWEsQ0FBRyxDQUFDO2dCQUFDLE1BQU0sQ0FBQyxhQUFhLENBQUM7UUFDeEUsQ0FBQztRQUNELE1BQU0sQ0FBQyxJQUFJLENBQUM7SUFDaEIsQ0FBQztDQUNKO0FBQUE7QUFBQTtBQUVEOztHQUVHO0FBQ0g7SUFNSSxZQUFhLEtBQXdCO1FBQ2pDLElBQUksTUFBTSxHQUFHLFFBQVEsQ0FBQyxjQUFjLENBQUUsZUFBZSxDQUFvQixDQUFDO1FBQzFFLElBQUksQ0FBQyxZQUFZLEdBQUcsUUFBUSxDQUFDLGNBQWMsQ0FBRSxVQUFVLENBQW9CLENBQUM7UUFDNUUsSUFBSSxTQUFTLEdBQUcsUUFBUSxDQUFDLGFBQWEsQ0FBRSxLQUFLLENBQW9CLENBQUM7UUFDbEUsU0FBUyxDQUFDLEtBQUssQ0FBQyxVQUFVLEdBQUcsYUFBYSxDQUFDO1FBQzNDLFNBQVMsQ0FBQyxLQUFLLENBQUMsS0FBSyxHQUFHLE9BQU8sQ0FBQztRQUNoQyxTQUFTLENBQUMsS0FBSyxDQUFDLFFBQVEsR0FBRyxVQUFVLENBQUM7UUFDdEMsU0FBUyxDQUFDLEtBQUssQ0FBQyxVQUFVLEdBQUcsTUFBTSxDQUFDO1FBQ3BDLFNBQVMsQ0FBQyxLQUFLLENBQUMsYUFBYSxHQUFHLE1BQU0sQ0FBQztRQUN2QyxTQUFTLENBQUMsZ0JBQWdCLENBQUUsU0FBUyxFQUFFLEdBQUcsRUFBRSxDQUFDLEtBQUssQ0FBQyxjQUFjLEVBQUUsQ0FBRSxDQUFDO1FBQ3RFLFNBQVMsQ0FBQyxnQkFBZ0IsQ0FBRSxXQUFXLEVBQUUsR0FBRyxFQUFFLENBQUMsS0FBSyxDQUFDLGNBQWMsRUFBRSxDQUFFLENBQUM7UUFDeEUsU0FBUyxDQUFDLGdCQUFnQixDQUFFLFdBQVcsRUFBRSxHQUFHLEVBQUUsQ0FBQyxLQUFLLENBQUMsY0FBYyxFQUFFLENBQUUsQ0FBQztRQUN4RSxTQUFTLENBQUMsZ0JBQWdCLENBQUUsT0FBTyxFQUFFLEdBQUcsRUFBRSxDQUFDLEtBQUssQ0FBQyxjQUFjLEVBQUUsQ0FBRSxDQUFDO1FBQ3BFLFNBQVMsQ0FBQyxnQkFBZ0IsQ0FBRSxXQUFXLEVBQUUsR0FBRyxFQUFFLENBQUMsS0FBSyxDQUFDLGNBQWMsRUFBRSxDQUFFO1FBQ3ZFLFFBQVEsQ0FBQyxJQUFJLENBQUMsWUFBWSxDQUFFLFNBQVMsRUFBRSxLQUFLLENBQUUsQ0FBQztRQUMvQyxJQUFJLENBQUMsU0FBUyxHQUFHLFNBQVMsQ0FBQztRQUMzQixJQUFJLENBQUMsS0FBSyxHQUFHLEtBQUssQ0FBQztRQUNuQixJQUFJLENBQUMsT0FBTyxHQUFHLE1BQU0sQ0FBQyxTQUFTLENBQUM7UUFDaEMsSUFBSSxDQUFDLE9BQU8sR0FBRyxNQUFNLENBQUMsVUFBVSxDQUFDO1FBQ2pDLFNBQVMsQ0FBQyxNQUFNLEdBQUcsSUFBSSxDQUFDO0lBQzVCLENBQUM7SUFDRCxTQUFTLENBQUUsSUFBYTtRQUNwQixJQUFJLENBQUMsU0FBUyxDQUFDLE1BQU0sR0FBRyxJQUFJLENBQUM7SUFDakMsQ0FBQztJQUNELFFBQVE7UUFDSixNQUFNLENBQUMsSUFBSSxDQUFDLFNBQVMsQ0FBQyxNQUFNLENBQUM7SUFDakMsQ0FBQztJQUNELE9BQU8sQ0FBRSxJQUFTO1FBQ2QsSUFBSSxDQUFDLFNBQVMsQ0FBQyxXQUFXLEdBQUcsTUFBTSxDQUFFLElBQUksQ0FBRSxDQUFDO1FBQzVDLElBQUksQ0FBQyxZQUFZLENBQUMsV0FBVyxHQUFHLE1BQU0sQ0FBRSxJQUFJLENBQUUsQ0FBQztJQUNuRCxDQUFDO0lBRUQsYUFBYSxDQUFFLE1BQWMsRUFBRSxLQUFhO1FBQ3hDLElBQUksQ0FBQyxTQUFTLENBQUMsV0FBVyxHQUFHLE1BQU0sQ0FBRSxLQUFLLENBQUUsQ0FBQztRQUM3QyxJQUFJLENBQUMsWUFBWSxDQUFDLFdBQVcsR0FBRyxNQUFNLEdBQUcsSUFBSSxHQUFHLE1BQU0sQ0FBRSxLQUFLLENBQUUsQ0FBQztJQUNwRSxDQUFDO0lBQ0QsT0FBTztRQUNILE1BQU0sQ0FBQyxJQUFJLENBQUMsU0FBUyxDQUFDLFdBQVcsQ0FBQztJQUN0QyxDQUFDO0lBQ0QsUUFBUSxDQUFFLEtBQWE7UUFDbkIsSUFBSSxDQUFDLFNBQVMsQ0FBQyxLQUFLLENBQUMsS0FBSyxHQUFHLEtBQUssQ0FBQztJQUN2QyxDQUFDO0lBQ0Qsb0NBQW9DO0lBQ3BDLE1BQU0sQ0FBRSxNQUFjO1FBQ2xCLElBQUksSUFBSSxHQUFHLE1BQU0sQ0FBQyxDQUFDLEdBQUcsSUFBSSxDQUFDLE9BQU8sR0FBRyxJQUFJLENBQUM7UUFDMUMsSUFBSSxJQUFJLEdBQUcsTUFBTSxDQUFDLENBQUMsR0FBRyxJQUFJLENBQUMsT0FBTyxHQUFHLElBQUksQ0FBQztRQUMxQyxJQUFJLEtBQUssR0FBRyxZQUFZLEdBQUcsSUFBSSxHQUFHLEdBQUcsR0FBRyxJQUFJLEdBQUcsR0FBRyxDQUFDO1FBQ25ELElBQUksQ0FBQyxTQUFTLENBQUMsS0FBSyxDQUFDLFNBQVMsR0FBRyxLQUFLLENBQUM7SUFDM0MsQ0FBQztDQUNKOzs7Ozs7Ozs7QUMzZ0JrRDtBQUVuRDs7R0FFRztBQUNHO0lBQU47UUFHSSxzQ0FBc0M7UUFDdEMsZ0JBQVcsR0FBVSwyREFBSyxDQUFDO1FBQzNCLDZDQUE2QztRQUM3QyxlQUFVLEdBQVUseURBQUcsQ0FBQztRQUN4Qiw0Q0FBNEM7UUFDNUMsb0JBQWUsR0FBcUIsRUFBRSxDQUFDO1FBQ3ZDLG1EQUFtRDtRQUNuRCxtQkFBYyxHQUFxQixFQUFFLENBQUM7UUFDdEMseURBQXlEO1FBQ3pELGtCQUFhLEdBQVcsRUFBRSxDQUFDO1FBQzNCLCtEQUErRDtRQUMvRCxpQkFBWSxHQUFXLEVBQUUsQ0FBQztRQUcxQiwwQkFBMEI7UUFDbEIsYUFBUSxHQUFXLENBQUMsQ0FBQztJQXFDakMsQ0FBQztJQXBDRyxJQUFJLE9BQU8sS0FBYSxNQUFNLENBQUMsSUFBSSxDQUFDLFFBQVEsRUFBQyxDQUFDO0lBQzlDLElBQUksT0FBTyxDQUFFLE9BQWUsSUFBSyxJQUFJLENBQUMsUUFBUSxHQUFHLE9BQU8sQ0FBQyxDQUFDLENBQUM7SUFHM0Q7OztPQUdHO0lBQ0gsaUJBQWlCO1FBQ2IsTUFBTSxDQUFDLElBQUksQ0FBQyxhQUFhLENBQUMsTUFBTSxDQUFFLElBQUksQ0FBQyxZQUFZLENBQUUsQ0FBQztJQUMxRCxDQUFDO0lBRUQ7Ozs7O09BS0c7SUFDSCxtQkFBbUIsQ0FBRSxLQUFhO1FBQzlCLE1BQU0sQ0FBQyxLQUFLLEtBQUssSUFBSSxDQUFDLFFBQVEsQ0FBQyxDQUFDLENBQUMsSUFBSSxDQUFDLGVBQWUsQ0FBQyxDQUFDLENBQUMsSUFBSSxDQUFDLGNBQWMsQ0FBQztJQUNoRixDQUFDO0lBRUQ7OztPQUdHO0lBQ0gsV0FBVyxDQUFFLEtBQWM7UUFDdkIsSUFBSSxLQUFLLEdBQUcsQ0FBRSxDQUFDLEtBQUssSUFBSSxLQUFLLEtBQUssSUFBSSxDQUFDLFFBQVEsQ0FBRSxDQUFDLENBQUMsQ0FBQyxJQUFJLENBQUMsZUFBZSxDQUFDLENBQUMsQ0FBQyxFQUFFLENBQUM7UUFDOUUsSUFBSSxLQUFLLEdBQUcsQ0FBQyxLQUFLLENBQUMsQ0FBQyxDQUFDLElBQUksQ0FBQyxjQUFjLENBQUMsQ0FBQyxDQUFDLEVBQUUsQ0FBQztRQUM5QyxNQUFNLENBQUMsS0FBSyxDQUFDLE1BQU0sQ0FBRSxLQUFLLENBQUUsQ0FBQyxHQUFHLENBQUUsQ0FBQyxDQUFDLEVBQUUsQ0FBQyxDQUFDLENBQUMsUUFBUSxDQUFDLEtBQUssQ0FBRSxDQUFDLE1BQU0sQ0FBQyxDQUFFLENBQUMsRUFBRSxDQUFDLEVBQUcsRUFBRSxDQUFDLENBQUMsQ0FBQyxNQUFNLENBQUUsQ0FBQyxDQUFFLEVBQUUsRUFBRSxDQUFFLENBQUM7SUFDckcsQ0FBQztJQUVELGdCQUFnQjtRQUNaLE1BQU0sQ0FBQyxJQUFJLENBQUMsY0FBYyxDQUFDLEdBQUcsQ0FBRSxDQUFDLENBQUMsRUFBRSxDQUFDLENBQUMsQ0FBQyxRQUFRLENBQUMsS0FBSyxDQUFFLENBQUMsTUFBTSxDQUFDLENBQUUsQ0FBQyxFQUFFLENBQUMsRUFBRyxFQUFFLENBQUMsQ0FBQyxDQUFDLE1BQU0sQ0FBRSxDQUFDLENBQUUsRUFBRSxFQUFFLENBQUUsQ0FBQztJQUNuRyxDQUFDO0NBRUo7QUFBQTtBQUFBOzs7Ozs7Ozs7OztBQy9Eb0Y7QUFFaEQ7QUFHNEI7QUFHakU7O0dBRUc7QUFDRztJQU9GLFlBQWEsTUFBeUIsRUFBRSxjQUFzQjtRQUMxRCxJQUFJLENBQUMsYUFBYSxHQUFHLE1BQU0sQ0FBQyxVQUFVLENBQUUsSUFBSSxDQUFFLENBQUM7UUFDL0MsSUFBSSxDQUFDLE1BQU0sR0FBRyxNQUFNLENBQUM7UUFDckIsSUFBSSxDQUFDLGVBQWUsR0FBRyxJQUFJLENBQUMsYUFBYSxDQUFDLFNBQVMsQ0FBQztRQUNwRCxJQUFJLENBQUMsU0FBUyxHQUFHLElBQUksQ0FBQyxhQUFhLENBQUMsV0FBVyxDQUFDO1FBQ2hELElBQUksQ0FBQyxjQUFjLEdBQUcsY0FBYyxDQUFDLENBQUMsQ0FBQyxjQUFjLENBQUMsQ0FBQyxDQUFDLHlEQUFHLENBQUM7SUFDaEUsQ0FBQztJQUVEOztPQUVHO0lBQ0gsVUFBVSxDQUFFLEtBQWlCO1FBQ3pCLElBQUksQ0FBQyxhQUFhLENBQUMsU0FBUyxDQUFFLENBQUMsRUFBRSxDQUFDLEVBQUUsSUFBSSxDQUFDLE1BQU0sQ0FBQyxLQUFLLEVBQUUsSUFBSSxDQUFDLE1BQU0sQ0FBQyxNQUFNLENBQUUsQ0FBQyxDQUFDLHNCQUFzQjtRQUNuRyxHQUFHLENBQUMsQ0FBRSxJQUFJLFFBQVEsSUFBSSxLQUFLLENBQUMsZUFBZ0IsQ0FBQyxDQUFDLENBQUM7WUFDM0MsSUFBSSxDQUFDLElBQUksQ0FBRSxRQUFRLEVBQUUsS0FBSyxDQUFDLFdBQVcsQ0FBRSxDQUFDO1FBQzdDLENBQUM7UUFDRCxHQUFHLENBQUMsQ0FBRSxJQUFJLFFBQVEsSUFBSSxLQUFLLENBQUMsY0FBZSxDQUFDLENBQUMsQ0FBQztZQUMxQyxJQUFJLENBQUMsSUFBSSxDQUFFLFFBQVEsRUFBRSxLQUFLLENBQUMsVUFBVSxDQUFFLENBQUM7UUFDNUMsQ0FBQztJQUNMLENBQUM7SUFFRCxJQUFJLENBQUUsTUFBVyxFQUFFLEtBQWE7UUFDNUIsRUFBRSxDQUFDLENBQUUsTUFBTSxZQUFZLHdFQUFPLENBQUMsQ0FBQyxDQUFDO1lBQzdCLElBQUksQ0FBQyxhQUFhLENBQUMsUUFBUSxDQUFFLE1BQU0sQ0FBQyxDQUFDLEVBQUUsTUFBTSxDQUFDLENBQUMsRUFBRSxDQUFDLEVBQUUsQ0FBQyxDQUFFLENBQUM7UUFDNUQsQ0FBQztRQUFDLElBQUksQ0FBQyxFQUFFLENBQUMsQ0FBRSxNQUFNLFlBQVksc0VBQUssQ0FBQyxDQUFDLENBQUM7WUFDbEMsSUFBSSxRQUFRLEdBQUcsSUFBSSxDQUFDLGFBQWEsQ0FBQyxXQUFXLENBQUM7WUFDOUMsRUFBRSxDQUFDLENBQUUsS0FBTSxDQUFDO2dCQUFDLElBQUksQ0FBQyxhQUFhLENBQUMsV0FBVyxHQUFHLEtBQUssQ0FBQyxHQUFHLEVBQUUsQ0FBQztZQUMxRCwrRkFBK0Y7WUFDL0YsSUFBSSxDQUFDLGFBQWEsQ0FBQyxTQUFTLEVBQUUsQ0FBQztZQUMvQixJQUFJLENBQUMsYUFBYSxDQUFDLE1BQU0sQ0FBRSxNQUFNLENBQUMsV0FBVyxDQUFDLENBQUMsRUFBRSxNQUFNLENBQUMsV0FBVyxDQUFDLENBQUMsQ0FBRSxDQUFDO1lBQ3hFLElBQUksQ0FBQyxhQUFhLENBQUMsTUFBTSxDQUFFLE1BQU0sQ0FBQyxRQUFRLENBQUMsV0FBVyxDQUFDLENBQUMsRUFBRSxNQUFNLENBQUMsUUFBUSxDQUFDLFdBQVcsQ0FBQyxDQUFDLENBQUUsQ0FBQztZQUMxRixJQUFJLENBQUMsYUFBYSxDQUFDLE1BQU0sRUFBRSxDQUFDO1lBQzVCLElBQUksQ0FBQyxhQUFhLENBQUMsV0FBVyxHQUFHLFFBQVEsQ0FBQztRQUM5QyxDQUFDO1FBQUMsSUFBSSxDQUFDLEVBQUUsQ0FBQyxDQUFFLE1BQU0sWUFBWSwwRUFBUyxDQUFDLENBQUMsQ0FBQztZQUN0QyxHQUFHLENBQUMsQ0FBRSxJQUFJLElBQUksSUFBSSxNQUFNLENBQUMsS0FBTSxDQUFDO2dCQUFDLElBQUksQ0FBQyxJQUFJLENBQUUsSUFBSSxFQUFFLEtBQUssQ0FBRSxDQUFDO1FBQzlELENBQUM7UUFBQyxJQUFJLENBQUMsRUFBRSxDQUFDLENBQUUsTUFBTSxZQUFZLHFFQUFjLElBQUksTUFBTSxZQUFZLHNFQUFnQixDQUFDLENBQUMsQ0FBQztZQUNqRixJQUFJLENBQUMsSUFBSSxDQUFFLE1BQU0sQ0FBQyxRQUFRLEVBQUUsS0FBSyxDQUFFLENBQUM7UUFDeEMsQ0FBQztJQUNMLENBQUM7SUFFRCxJQUFJLENBQUUsTUFBbUQsRUFBRSxTQUFnQjtRQUN2RSxJQUFJLFFBQVEsR0FBRyxNQUFNLFlBQVksMEVBQVEsQ0FBQyxDQUFDLENBQUMsQ0FBQyxDQUFDLENBQUMsQ0FBQyxNQUFNLENBQUMsT0FBTyxDQUFDO1FBQy9ELElBQUksQ0FBQyxhQUFhLENBQUMsU0FBUyxHQUFHLFNBQVMsQ0FBQyxHQUFHLENBQUUsUUFBUSxDQUFFLENBQUM7UUFDekQsSUFBSSxRQUFRLEdBQUcsTUFBTSxZQUFZLDBFQUFRLENBQUMsQ0FBQyxDQUFDLE1BQU0sQ0FBQyxRQUFRLENBQUMsQ0FBQyxDQUFDLE1BQU0sQ0FBQyxRQUFRLENBQUMsUUFBUSxDQUFDO1FBQ3ZGLElBQUksQ0FBQyxhQUFhLENBQUMsU0FBUyxFQUFFLENBQUM7UUFDL0IsSUFBSSxDQUFDLGFBQWEsQ0FBQyxNQUFNLENBQUUsUUFBUSxDQUFDLENBQUMsQ0FBQyxDQUFDLENBQUMsRUFBRSxRQUFRLENBQUMsQ0FBQyxDQUFDLENBQUMsQ0FBQyxDQUFFLENBQUM7UUFDMUQsSUFBSSxDQUFDLGFBQWEsQ0FBQyxNQUFNLENBQUUsUUFBUSxDQUFDLENBQUMsQ0FBQyxDQUFDLENBQUMsRUFBRSxRQUFRLENBQUMsQ0FBQyxDQUFDLENBQUMsQ0FBQyxDQUFFLENBQUM7UUFDMUQsSUFBSSxDQUFDLGFBQWEsQ0FBQyxNQUFNLENBQUUsUUFBUSxDQUFDLENBQUMsQ0FBQyxDQUFDLENBQUMsRUFBRSxRQUFRLENBQUMsQ0FBQyxDQUFDLENBQUMsQ0FBQyxDQUFFLENBQUM7UUFDMUQsSUFBSSxDQUFDLGFBQWEsQ0FBQyxTQUFTLEVBQUUsQ0FBQztRQUMvQixJQUFJLENBQUMsYUFBYSxDQUFDLE1BQU0sRUFBRSxDQUFDO1FBQzVCLElBQUksQ0FBQyxhQUFhLENBQUMsSUFBSSxFQUFFLENBQUM7SUFDOUIsQ0FBQztJQUVELFFBQVEsQ0FBRSxJQUFZLEVBQUUsRUFBVTtRQUM5QixJQUFJLENBQUMsYUFBYSxDQUFDLE1BQU0sQ0FBRSxJQUFJLENBQUMsQ0FBQyxFQUFFLElBQUksQ0FBQyxDQUFDLENBQUUsQ0FBQztRQUM1QyxJQUFJLENBQUMsYUFBYSxDQUFDLE1BQU0sQ0FBRSxFQUFFLENBQUMsQ0FBQyxFQUFFLEVBQUUsQ0FBQyxDQUFDLENBQUUsQ0FBQztRQUN4QyxJQUFJLENBQUMsYUFBYSxDQUFDLE1BQU0sRUFBRSxDQUFDO0lBQ2hDLENBQUM7SUFFRCxTQUFTLENBQUUsTUFBdUI7UUFDOUIsRUFBRSxDQUFDLENBQUUsTUFBTSxZQUFZLHdFQUFPLENBQUMsQ0FBQyxDQUFDO1lBQzdCLElBQUksUUFBUSxHQUFHLElBQUksQ0FBQyxhQUFhLENBQUMsU0FBUyxDQUFDO1lBQzVDLElBQUksQ0FBQyxhQUFhLENBQUMsU0FBUyxHQUFHLElBQUksQ0FBQyxjQUFjLENBQUMsR0FBRyxFQUFFLENBQUM7WUFDekQsSUFBSSxDQUFDLGFBQWEsQ0FBQyxTQUFTLEVBQUUsQ0FBQztZQUMvQixJQUFJLENBQUMsYUFBYSxDQUFDLEdBQUcsQ0FBRSxNQUFNLENBQUMsQ0FBQyxFQUFFLE1BQU0sQ0FBQyxDQUFDLEVBQUUsQ0FBQyxFQUFFLENBQUMsRUFBRSxDQUFDLEdBQUcsSUFBSSxDQUFDLEVBQUUsQ0FBRSxDQUFDO1lBQ2hFLElBQUksQ0FBQyxhQUFhLENBQUMsSUFBSSxFQUFFLENBQUM7WUFDMUIsSUFBSSxDQUFDLGFBQWEsQ0FBQyxTQUFTLEdBQUcsUUFBUSxDQUFDO1FBQzVDLENBQUM7UUFBQyxJQUFJLENBQUMsRUFBRSxDQUFDLENBQUUsTUFBTSxZQUFZLHNFQUFLLENBQUMsQ0FBQyxDQUFDO1lBQ2xDLElBQUksQ0FBQyxJQUFJLENBQUUsTUFBTSxFQUFFLElBQUksQ0FBQyxjQUFjLENBQUUsQ0FBQztRQUM3QyxDQUFDO1FBQUMsSUFBSSxDQUFDLEVBQUUsQ0FBQyxDQUFFLE1BQU0sWUFBWSxxRUFBZSxDQUFDLENBQUMsQ0FBQztZQUM1QyxJQUFJLENBQUMsSUFBSSxDQUFFLE1BQU0sRUFBRSxJQUFJLENBQUMsY0FBYyxDQUFFLENBQUM7UUFDN0MsQ0FBQztJQUNMLENBQUM7SUFFRCxXQUFXLENBQUUsYUFBOEI7UUFDdkMsRUFBRSxDQUFDLENBQUUsYUFBYSxZQUFZLHdFQUFPLENBQUMsQ0FBQyxDQUFDO1lBQ3BDLE1BQU0sMENBQTBDO1FBQ3BELENBQUM7UUFBQyxJQUFJO1lBQUMsSUFBSSxDQUFDLElBQUksQ0FBRSxhQUFhLENBQUUsQ0FBQztJQUN0QyxDQUFDO0NBRUo7QUFBQTtBQUFBOzs7Ozs7Ozs7O0FDbEdtRTtBQUNIO0FBSWpFOztHQUVHO0FBQ0c7SUFHRjs7O09BR0c7SUFDSCxZQUFhLE1BQWU7UUFDeEIsSUFBSSxDQUFDLE1BQU0sR0FBRyxNQUFNLENBQUM7SUFDekIsQ0FBQztJQUVEOzs7Ozs7T0FNRztJQUNILHFCQUFxQixDQUFFLFVBQWtCLEVBQUUsUUFBZ0IsRUFBRSxLQUFpQjtRQUMxRSxFQUFFLENBQUMsQ0FBRSxDQUFDLFVBQVcsQ0FBQyxDQUFDLENBQUM7WUFDaEIsTUFBTSxDQUFDLElBQUksQ0FBQztRQUNoQixDQUFDO1FBQ0QsSUFBSSxhQUFhLEdBQXNCLElBQUksQ0FBQyx1QkFBdUIsQ0FDL0QsSUFBSSxzRUFBSSxDQUFFLFVBQVUsRUFBRSxJQUFJLEVBQUUsSUFBSSxzRUFBSSxDQUFFLFFBQVEsRUFBRSxJQUFJLEVBQUUsSUFBSSxDQUFFLENBQUUsRUFDOUQsS0FBSyxDQUFFLENBQUM7UUFDWixJQUFJLFFBQVEsR0FBRyxhQUFhLENBQUMsb0JBQW9CLEVBQUUsQ0FBQztRQUNwRCx5QkFBeUI7UUFDekIsSUFBSSxTQUFTLEdBQVMsUUFBUSxDQUFDLENBQUMsQ0FBQyxDQUFDO1FBQ2xDLHlEQUF5RDtRQUN6RCxFQUFFLENBQUMsQ0FBRSxDQUFDLFNBQVUsQ0FBQyxDQUFDLENBQUM7WUFDZixNQUFNLENBQUMsSUFBSSxDQUFDO1FBQ2hCLENBQUM7UUFDRCxJQUFJLFdBQVcsR0FBUyxRQUFRLENBQUMsQ0FBQyxDQUFDLENBQUM7UUFFcEMscUlBQXFJO1FBQ3JJLEVBQUUsQ0FBQyxDQUFFLENBQUMsV0FBVyxJQUFJLFNBQVMsQ0FBQyxrQkFBa0IsQ0FBRSxRQUFRLENBQUUsSUFBSSxrREFBTyxHQUFHLDZEQUFtQixDQUFDO1lBQUMsTUFBTSxDQUFDLElBQUksQ0FBQztRQUU1RyxFQUFFLENBQUMsQ0FBRSxTQUFTLENBQUMsa0JBQWtCLENBQUUsUUFBUSxDQUFFLEdBQUcsa0RBQVEsQ0FBQyxDQUFDLENBQUM7WUFDdkQsTUFBTSxDQUFDLElBQUksQ0FBQztRQUNoQixDQUFDO1FBQ0QsMkJBQTJCO1FBQzNCLElBQUksZUFBeUIsQ0FBQztRQUM5QixJQUFJLFNBQWUsQ0FBQztRQUNwQixJQUFJLFVBQWdCLENBQUM7UUFDckIsSUFBSSxTQUFlLENBQUM7UUFDcEIsbUVBQW1FO1FBQ25FLElBQUksa0JBQXdCLENBQUM7UUFFN0IsMkVBQTJFO1FBQzNFLEVBQUUsQ0FBQyxDQUFFLENBQUMsV0FBVyxJQUFJLGtEQUFPLEdBQUcsQ0FBRSxDQUFDLENBQUMsQ0FBQztZQUNoQyxJQUFJLFdBQVcsR0FBVyxJQUFJLENBQUMsYUFBYSxDQUFFLFFBQVEsRUFBRSxLQUFLLENBQUUsQ0FBQztZQUNoRSxHQUFHLENBQUMsQ0FBRSxJQUFJLElBQUksSUFBSSxXQUFZLENBQUMsQ0FBQyxDQUFDO2dCQUM3QixrQkFBa0IsR0FBRyxJQUFJLENBQUMsZUFBZSxDQUFFLFNBQVMsRUFBRSxJQUFJLENBQUUsQ0FBQztnQkFDN0QsRUFBRSxDQUFDLENBQUUsa0JBQW1CLENBQUMsQ0FBQyxDQUFDO29CQUN2QixXQUFXLEdBQUcsSUFBSSxDQUFDO29CQUNuQixrQ0FBa0M7b0JBQ2xDLEtBQUssQ0FBQztnQkFDVixDQUFDO1lBQ0wsQ0FBQztZQUNELDZEQUE2RDtZQUM3RCwrREFBK0Q7WUFDL0QsbURBQW1EO1lBQ25ELEVBQUUsQ0FBQyxDQUFFLFdBQVcsQ0FBQyxNQUFNLEdBQUcsQ0FBQyxJQUFJLENBQUMsV0FBWSxDQUFDLENBQUMsQ0FBQztnQkFDM0MsZUFBZSxHQUFHLElBQUksQ0FBQyxzQkFBc0IsQ0FBRSxTQUFTLEVBQUUsV0FBVyxFQUFFLEtBQUssRUFBRSxRQUFRLENBQUUsQ0FBQztnQkFDekYsRUFBRSxDQUFDLENBQUUsQ0FBQyxlQUFnQixDQUFDLENBQUMsQ0FBQztvQkFDckIsTUFBTSxDQUFDLElBQUksQ0FBQztnQkFDaEIsQ0FBQztZQUNMLENBQUM7UUFDTCxDQUFDO1FBQ0QsRUFBRSxDQUFDLENBQUUsQ0FBQyxlQUFnQixDQUFDLENBQUMsQ0FBQztZQUNyQix1RUFBdUU7WUFDdkUsYUFBYTtZQUNiLEVBQUUsQ0FBQyxDQUFFLFdBQVksQ0FBQyxDQUFDLENBQUM7Z0JBQ2hCLGVBQWUsR0FBRyxJQUFJLENBQUMseUJBQXlCLENBQUUsU0FBUyxFQUFFLFdBQVcsRUFBRSxLQUFLLENBQUUsQ0FBQztZQUN0RixDQUFDO1lBQUMsSUFBSSxDQUFDLENBQUM7Z0JBQ0osU0FBUyxHQUFHLHNFQUFJLENBQUMsZUFBZSxDQUFFLFNBQVMsQ0FBRSxDQUFDO2dCQUM5QyxVQUFVLEdBQUcsSUFBSSxzRUFBSSxDQUFFLFNBQVMsQ0FBQyxXQUFXLEVBQUUsU0FBUyxFQUFFLElBQUksQ0FBRSxDQUFDO2dCQUNoRSxTQUFTLEdBQUcsSUFBSSxzRUFBSSxDQUFFLFFBQVEsRUFBRSxVQUFVLEVBQUUsU0FBUyxDQUFFLENBQUM7Z0JBQ3hELFNBQVMsQ0FBQyxRQUFRLEdBQUcsU0FBUyxDQUFDO2dCQUMvQixTQUFTLENBQUMsUUFBUSxHQUFHLFVBQVUsQ0FBQztnQkFDaEMsVUFBVSxDQUFDLFFBQVEsR0FBRyxTQUFTLENBQUM7Z0JBQ2hDLGVBQWUsR0FBRyxJQUFJLDBFQUFRLENBQUUsU0FBUyxDQUFFLENBQUM7WUFDaEQsQ0FBQztRQUNMLENBQUM7UUFFRCw2REFBNkQ7UUFDN0Qsc0RBQXNEO1FBQ3RELGtDQUFrQztRQUNsQyxFQUFFLENBQUMsQ0FBRSxDQUFDLGVBQWdCLENBQUMsQ0FBQyxDQUFDO1lBQ3JCLE1BQU0sQ0FBQyxJQUFJLENBQUM7UUFDaEIsQ0FBQztRQUNELE1BQU0sQ0FBQyxlQUFlLENBQUM7SUFDM0IsQ0FBQztJQUVEOzs7O09BSUc7SUFDSCxpRkFBaUY7SUFDakYsdUJBQXVCLENBQUUsU0FBZSxFQUFFLEtBQWlCO1FBQ3ZELHFGQUFxRjtRQUNyRixnR0FBZ0c7UUFDaEcsZ0dBQWdHO1FBQ2hHLCtHQUErRztRQUMvRyxJQUFJLFFBQVEsR0FBVyxLQUFLLENBQUMsYUFBYSxDQUFDLE1BQU0sQ0FBRSxLQUFLLENBQUMsZ0JBQWdCLEVBQUUsQ0FBRSxDQUFDO1FBQzlFLElBQUksV0FBVyxHQUFHLFNBQVMsQ0FBQyxXQUFXLENBQUM7UUFDeEMsOEZBQThGO1FBQzlGLElBQUksTUFBTSxHQUFzQixJQUFJLGlCQUFpQixDQUFFLEtBQUssQ0FBQyxRQUFRLENBQUUsQ0FBQztRQUN4RSxnR0FBZ0c7UUFDaEcsR0FBRyxDQUFDLENBQUUsSUFBSSxJQUFJLElBQUksUUFBUyxDQUFDLENBQUMsQ0FBQztZQUMxQixzRUFBc0U7WUFDdEUsRUFBRSxDQUFDLENBQUUsU0FBUyxDQUFDLGNBQWMsQ0FBRSxJQUFJLENBQUcsQ0FBQyxDQUFDLENBQUM7Z0JBQ3JDLElBQUksUUFBUSxHQUFXLFNBQVMsQ0FBQyxrQkFBa0IsQ0FBRSxXQUFXLENBQUUsQ0FBQztnQkFDbkUsTUFBTSxDQUFDLFdBQVcsQ0FBRSxJQUFJLGVBQWUsQ0FBRSxJQUFJLEVBQUUsUUFBUSxDQUFFLENBQUUsQ0FBQztZQUNoRSxDQUFDO1lBQ0Qsd0VBQXdFO1FBQzVFLENBQUM7UUFDRCxNQUFNLENBQUMsTUFBTSxDQUFDLFFBQVEsRUFBRSxDQUFDO0lBQzdCLENBQUM7SUFFRDs7OztPQUlHO0lBQ0gsYUFBYSxDQUFFLFFBQWdCLEVBQUUsS0FBaUI7UUFDOUMsSUFBSSxXQUFXLEdBQVcsRUFBRSxDQUFDO1FBQzdCLEdBQUcsQ0FBQyxDQUFFLElBQUksSUFBSSxJQUFJLEtBQUssQ0FBQyxhQUFjLENBQUMsQ0FBQyxDQUFDO1lBQ3JDLDBFQUEwRTtZQUMxRSxJQUFJLFNBQVMsR0FBRyxJQUFJLENBQUMsa0JBQWtCLENBQUUsUUFBUSxDQUFFLENBQUM7WUFDcEQsRUFBRSxDQUFDLENBQUUsU0FBUyxHQUFHLGtEQUFRLENBQUM7Z0JBQUMsV0FBVyxDQUFDLElBQUksQ0FBRSxJQUFJLENBQUUsQ0FBQztZQUNwRCw0RUFBNEU7UUFDaEYsQ0FBQztRQUNELEVBQUUsQ0FBQyxDQUFFLGdEQUFLLEdBQUcsQ0FBQyxJQUFJLElBQUksQ0FBQyxNQUFPLENBQUM7WUFBQyxHQUFHLENBQUMsQ0FBRSxJQUFJLElBQUksSUFBSSxXQUFZLENBQUM7Z0JBQUMsSUFBSSxDQUFDLE1BQU0sQ0FBQyxTQUFTLENBQUUsSUFBSSxDQUFFLENBQUM7UUFDOUYsTUFBTSxDQUFDLFdBQVcsQ0FBQztJQUN2QixDQUFDO0lBRUQ7Ozs7O09BS0c7SUFDSCxlQUFlLENBQUUsS0FBVyxFQUFFLEtBQVc7UUFDckMsRUFBRSxDQUFDLENBQUUsS0FBSyxDQUFDLFdBQVcsQ0FBQyxNQUFNLENBQUUsS0FBSyxDQUFDLFFBQVEsQ0FBQyxXQUFXLENBQUcsQ0FBQztZQUFDLE1BQU0sQ0FBQyxLQUFLLENBQUM7UUFDM0UsSUFBSSxDQUFDLEVBQUUsQ0FBQyxDQUFFLEtBQUssQ0FBQyxRQUFRLENBQUMsV0FBVyxDQUFDLE1BQU0sQ0FBRSxLQUFLLENBQUMsV0FBVyxDQUFHLENBQUM7WUFBQyxNQUFNLENBQUMsS0FBSyxDQUFDO1FBQ2hGLElBQUk7WUFBQyxNQUFNLENBQUMsSUFBSSxDQUFDO0lBQ3JCLENBQUM7SUFFRDs7OztPQUlHO0lBQ0gscUJBQXFCLENBQUUsUUFBa0IsRUFBRSxLQUFpQjtRQUN4RCxJQUFJLFNBQVMsR0FBZSxFQUFFLENBQUM7UUFDL0IsR0FBRyxDQUFDLENBQUUsSUFBSSxJQUFJLElBQUksS0FBSyxDQUFDLFdBQVcsRUFBRyxDQUFDLENBQUMsQ0FBQztZQUNyQyxHQUFHLENBQUMsQ0FBRSxJQUFJLFlBQVksSUFBSSxRQUFRLENBQUMsS0FBTSxDQUFDLENBQUMsQ0FBQztnQkFDeEMsRUFBRSxDQUFDLENBQUUsWUFBWSxDQUFDLGNBQWMsQ0FBRSxJQUFJLENBQUcsQ0FBQyxDQUFDLENBQUM7b0JBQ3hDLEVBQUUsQ0FBQyxDQUFFLFNBQVMsQ0FBQyxPQUFPLENBQUUsSUFBSSxDQUFDLGVBQWUsQ0FBRSxHQUFHLENBQUUsQ0FBQzt3QkFBQyxTQUFTLENBQUMsSUFBSSxDQUFFLElBQUksQ0FBQyxlQUFlLENBQUUsQ0FBQztvQkFDNUYsS0FBSyxDQUFDO2dCQUNWLENBQUM7WUFDTCxDQUFDO1FBQ0wsQ0FBQztRQUNELE1BQU0sQ0FBQyxTQUFTLENBQUM7SUFDckIsQ0FBQztJQUVEOzs7Ozs7T0FNRztJQUNILHNCQUFzQixDQUFFLFNBQWUsRUFBRSxXQUFtQixFQUFFLEtBQWlCLEVBQUUsUUFBZ0I7UUFDN0YsMkRBQTJEO1FBQzNELElBQUksU0FBZSxDQUFDO1FBQ3BCLDREQUE0RDtRQUM1RCxJQUFJLFVBQWdCLENBQUM7UUFDckIsNERBQTREO1FBQzVELElBQUksU0FBZSxDQUFDO1FBRXBCLElBQUksbUJBQXNDLENBQUM7UUFDM0M7OztXQUdHO1FBQ0gsR0FBRyxDQUFDLENBQUUsSUFBSSxJQUFJLElBQUksV0FBWSxDQUFDLENBQUMsQ0FBQztZQUM3Qjs7O2VBR0c7WUFDSCxJQUFJLFFBQVEsR0FBVyxJQUFJLENBQUMsV0FBVyxDQUFDLGtCQUFrQixDQUFFLFFBQVEsQ0FBRSxDQUFDO1lBQ3ZFLEVBQUUsQ0FBQyxDQUFFLFFBQVEsR0FBRyxrREFBTyxHQUFHLENBQUMsSUFBSSxDQUFFLENBQUMsbUJBQW1CLElBQUksbUJBQW1CLENBQUMsUUFBUSxHQUFHLFFBQVEsQ0FBRyxDQUFDO2dCQUNoRyxtQkFBbUIsR0FBRyxJQUFJLGlCQUFpQixDQUFFLElBQUksQ0FBQyxXQUFXLEVBQUUsUUFBUSxDQUFFLENBQUM7WUFDOUUsSUFBSSxDQUFDLENBQUM7Z0JBQ0YsUUFBUSxHQUFHLElBQUksQ0FBQyxRQUFRLENBQUMsV0FBVyxDQUFDLGtCQUFrQixDQUFFLFFBQVEsQ0FBRSxDQUFDO2dCQUNwRSxFQUFFLENBQUMsQ0FBRSxRQUFRLEdBQUcsa0RBQU8sR0FBRyxDQUFDLElBQUksQ0FBRSxDQUFDLG1CQUFtQixJQUFJLG1CQUFtQixDQUFDLFFBQVEsR0FBRyxRQUFRLENBQUcsQ0FBQztvQkFDaEcsbUJBQW1CLEdBQUcsSUFBSSxpQkFBaUIsQ0FBRSxJQUFJLENBQUMsUUFBUSxDQUFDLFdBQVcsRUFBRSxRQUFRLENBQUUsQ0FBQztZQUMzRixDQUFDO1lBQ0QscUNBQXFDO1FBQ3pDLENBQUM7UUFDRCxFQUFFLENBQUMsQ0FBRSxtQkFBb0IsQ0FBQyxDQUFDLENBQUM7WUFDeEIsc0VBQXNFO1lBQ3RFLFNBQVMsR0FBRyxzRUFBSSxDQUFDLGVBQWUsQ0FBRSxTQUFTLENBQUUsQ0FBQztZQUM5QyxVQUFVLEdBQUcsSUFBSSxzRUFBSSxDQUFFLFNBQVMsQ0FBQyxXQUFXLEVBQUUsU0FBUyxFQUFFLElBQUksQ0FBRSxDQUFDO1lBQ2hFLFNBQVMsR0FBRyxJQUFJLHNFQUFJLENBQUUsbUJBQW1CLENBQUMsTUFBTSxFQUFFLFVBQVUsRUFBRSxTQUFTLENBQUUsQ0FBQztZQUMxRSxTQUFTLENBQUMsUUFBUSxHQUFHLFNBQVMsQ0FBQztZQUMvQixTQUFTLENBQUMsUUFBUSxHQUFHLFVBQVUsQ0FBQztZQUNoQyxVQUFVLENBQUMsUUFBUSxHQUFHLFNBQVMsQ0FBQztZQUNoQyxNQUFNLENBQUMsSUFBSSwwRUFBUSxDQUFFLFNBQVMsQ0FBRSxDQUFDO1FBQ3JDLENBQUM7SUFDTCxDQUFDO0lBRUQ7Ozs7Ozs7T0FPRztJQUNILHlCQUF5QixDQUFFLFNBQWUsRUFBRSxXQUFpQixFQUFFLEtBQWlCO1FBQzVFLEVBQUUsQ0FBQyxDQUFFLENBQUMsQ0FBRSxTQUFTLElBQUksV0FBVyxDQUFHLENBQUM7WUFBQyxNQUFNLENBQUMsSUFBSSxDQUFDO1FBQ2pELElBQUksU0FBZSxDQUFDO1FBQ3BCLElBQUksVUFBZ0IsQ0FBQztRQUNyQixJQUFJLFNBQWUsQ0FBQztRQUNwQixJQUFJLGtCQUFrQixHQUFTLElBQUksQ0FBQyxlQUFlLENBQUUsU0FBUyxFQUFFLFdBQVcsQ0FBRTtRQUM3RSxtREFBbUQ7UUFDbkQsRUFBRSxDQUFDLENBQUUsZ0RBQUssR0FBRyxDQUFDLElBQUksSUFBSSxDQUFDLE1BQU8sQ0FBQyxDQUFDLENBQUM7WUFBQyxJQUFJLENBQUMsTUFBTSxDQUFDLFNBQVMsQ0FBRSxTQUFTLENBQUUsQ0FBQztZQUFDLElBQUksQ0FBQyxNQUFNLENBQUMsU0FBUyxDQUFFLFdBQVcsQ0FBRSxDQUFDO1FBQUMsQ0FBQztRQUFBLENBQUM7UUFDOUcsRUFBRSxDQUFDLENBQUUsQ0FBQyxrQkFBbUIsQ0FBQztZQUFDLE1BQU0sQ0FBQyxJQUFJLENBQUM7UUFDdkMsSUFBSSxDQUFDLEVBQUUsQ0FBQyxDQUFFLFNBQVMsQ0FBQyxNQUFNLENBQUUsa0JBQWtCLENBQUcsQ0FBQyxDQUFDLENBQUM7WUFDaEQsU0FBUyxHQUFHLHNFQUFJLENBQUMsZUFBZSxDQUFFLFNBQVMsQ0FBRSxDQUFDO1lBQzlDLFVBQVUsR0FBRyxzRUFBSSxDQUFDLGVBQWUsQ0FBRSxXQUFXLENBQUUsQ0FBQztZQUNqRCxTQUFTLEdBQUcsSUFBSSxzRUFBSSxDQUFFLFdBQVcsQ0FBQyxXQUFXLEVBQUUsVUFBVSxFQUFFLFNBQVMsQ0FBRSxDQUFDO1FBQzNFLENBQUM7UUFBQyxJQUFJLENBQUMsRUFBRSxDQUFDLENBQUUsV0FBVyxDQUFDLE1BQU0sQ0FBRSxrQkFBa0IsQ0FBRyxDQUFDLENBQUMsQ0FBQztZQUNwRCxTQUFTLEdBQUcsc0VBQUksQ0FBQyxlQUFlLENBQUUsV0FBVyxDQUFFLENBQUM7WUFDaEQsVUFBVSxHQUFHLHNFQUFJLENBQUMsZUFBZSxDQUFFLFNBQVMsQ0FBRSxDQUFDO1lBQy9DLFNBQVMsR0FBRyxJQUFJLHNFQUFJLENBQUUsU0FBUyxDQUFDLFdBQVcsRUFBRSxVQUFVLEVBQUUsU0FBUyxDQUFFLENBQUM7UUFDekUsQ0FBQztRQUFDLElBQUk7WUFBQyxNQUFNLENBQUMsSUFBSSxDQUFDO1FBQ25CLFNBQVMsQ0FBQyxRQUFRLEdBQUcsU0FBUyxDQUFDO1FBQy9CLFNBQVMsQ0FBQyxRQUFRLEdBQUcsVUFBVSxDQUFDO1FBQ2hDLFVBQVUsQ0FBQyxRQUFRLEdBQUcsU0FBUyxDQUFDO1FBQ2hDLFVBQVUsQ0FBQyxRQUFRLEdBQUcsU0FBUyxDQUFDO1FBQ2hDLDZGQUE2RjtRQUM3Riw2RUFBNkU7UUFDN0UsMEZBQTBGO1FBQzFGLHlHQUF5RztRQUN6RyxNQUFNLENBQUMsSUFBSSwwRUFBUSxDQUFFLFNBQVMsQ0FBRSxDQUFDO0lBQ3JDLENBQUM7SUFFRDs7Ozs7OztPQU9HO0lBQ0gsZ0JBQWdCLENBQUUsU0FBZSxFQUFFLEtBQWlCO1FBQ2hELElBQUksT0FBTyxHQUFXLENBQUMsU0FBUyxDQUFDLENBQUM7UUFDbEMsSUFBSSxXQUFXLEdBQVMsQ0FBQyxTQUFTLENBQUMsUUFBUSxDQUFDLFdBQVcsRUFBRSxDQUFDLENBQUMsQ0FBQyxTQUFTLENBQUMsUUFBUSxDQUFDLENBQUMsQ0FBQyxTQUFTLENBQUMsUUFBUSxDQUFDLFdBQVcsRUFBRSxDQUFDLFFBQVEsQ0FBQztRQUMzSCxPQUFRLENBQUMsV0FBVyxDQUFDLE1BQU0sQ0FBRSxTQUFTLENBQUUsRUFBRyxDQUFDO1lBQ3hDLEVBQUUsQ0FBQyxDQUFFLGdEQUFLLElBQUksSUFBSSxDQUFDLE1BQU8sQ0FBQztnQkFBQyxJQUFJLENBQUMsTUFBTSxDQUFDLFNBQVMsQ0FBRSxXQUFXLENBQUUsQ0FBQztZQUNqRSxPQUFPLEdBQUcsT0FBTyxDQUFDLE1BQU0sQ0FBRSxXQUFXLENBQUUsQ0FBQztZQUN4QyxFQUFFLENBQUMsQ0FBRSxnREFBSyxJQUFJLElBQUksQ0FBQyxNQUFPLENBQUM7Z0JBQUMsSUFBSSxDQUFDLE1BQU0sQ0FBQyxXQUFXLENBQUUsV0FBVyxDQUFFLENBQUM7WUFDbkUsV0FBVyxHQUFHLENBQUMsV0FBVyxDQUFDLFFBQVEsQ0FBQyxXQUFXLEVBQUUsQ0FBQyxDQUFDLENBQUMsV0FBVyxDQUFDLFFBQVEsQ0FBQyxDQUFDLENBQUMsV0FBVyxDQUFDLFFBQVEsQ0FBQyxXQUFXLEVBQUUsQ0FBQyxRQUFRLENBQUM7UUFDM0gsQ0FBQztRQUNELE1BQU0sQ0FBQyxPQUFPLENBQUM7SUFDbkIsQ0FBQztJQUVEOzs7OztPQUtHO0lBQ0gscURBQXFEO0lBQ3JELHFCQUFxQixDQUFFLE1BQWMsRUFBRSxLQUFpQjtRQUNwRCxHQUFHLENBQUMsQ0FBRSxJQUFJLFFBQVEsSUFBSSxLQUFLLENBQUMsZUFBZ0IsQ0FBQyxDQUFDLENBQUM7WUFDM0MsSUFBSSxpQkFBaUIsR0FBRyxRQUFRLENBQUMsUUFBUSxDQUFDO1lBQzFDLEVBQUUsQ0FBQyxDQUF5QyxpQkFBaUIsQ0FBQyxRQUFRLENBQUUsTUFBTSxDQUFHLENBQUM7Z0JBQUMsTUFBTSxDQUFDLGlCQUFpQixDQUFDO1FBQ2hILENBQUM7UUFDRCxHQUFHLENBQUMsQ0FBRSxJQUFJLFFBQVEsSUFBSSxLQUFLLENBQUMsY0FBZSxDQUFDLENBQUMsQ0FBQztZQUMxQyxJQUFJLGlCQUFpQixHQUFHLFFBQVEsQ0FBQyxRQUFRLENBQUM7WUFDMUMsRUFBRSxDQUFDLENBQUUsaUJBQWlCLENBQUMsUUFBUSxDQUFFLE1BQU0sQ0FBRyxDQUFDO2dCQUFDLE1BQU0sQ0FBQyxpQkFBaUIsQ0FBQztRQUN6RSxDQUFDO1FBQ0QsTUFBTSxDQUFDLElBQUksQ0FBQztJQUNoQixDQUFDO0lBRUQ7Ozs7O09BS0c7SUFDSCxlQUFlLENBQUUsR0FBRyxRQUFrQjtRQUNsQyxFQUFFLENBQUMsQ0FBRSxRQUFRLENBQUMsTUFBTSxHQUFHLENBQUUsQ0FBQztZQUFDLE1BQU0sQ0FBQztRQUNsQyxJQUFJLEtBQUssR0FBVyxFQUFFLENBQUM7UUFDdkIsR0FBRyxDQUFDLENBQUUsSUFBSSxDQUFDLEdBQUcsQ0FBQyxFQUFFLENBQUMsR0FBRyxRQUFRLENBQUMsTUFBTSxFQUFFLENBQUMsRUFBRSxFQUFHLENBQUM7WUFDekMsS0FBSyxHQUFHLEtBQUssQ0FBQyxNQUFNLENBQUUsSUFBSSxzRUFBSSxDQUFFLFFBQVEsQ0FBQyxDQUFDLENBQUMsRUFBRSxJQUFJLEVBQUUsSUFBSSxDQUFFLENBQUUsQ0FBQztRQUNoRSxDQUFDO1FBQ0QsR0FBRyxDQUFDLENBQUUsSUFBSSxDQUFDLEdBQUcsQ0FBQyxFQUFFLENBQUMsR0FBRyxLQUFLLENBQUMsTUFBTSxHQUFHLENBQUMsRUFBRSxDQUFDLEVBQUUsRUFBRyxDQUFDO1lBQzFDLEtBQUssQ0FBQyxDQUFDLENBQUMsQ0FBQyxRQUFRLEdBQUcsS0FBSyxDQUFDLENBQUMsR0FBRyxDQUFDLENBQUMsQ0FBQztZQUNqQyxLQUFLLENBQUMsQ0FBQyxDQUFDLENBQUMsUUFBUSxHQUFHLEtBQUssQ0FBQyxDQUFDLEdBQUcsQ0FBQyxDQUFDLENBQUM7UUFDckMsQ0FBQztRQUNELEtBQUssQ0FBQyxDQUFDLENBQUMsQ0FBQyxRQUFRLEdBQUcsS0FBSyxDQUFDLENBQUMsQ0FBQyxDQUFDO1FBQzdCLEtBQUssQ0FBQyxDQUFDLENBQUMsQ0FBQyxRQUFRLEdBQUcsS0FBSyxDQUFDLEtBQUssQ0FBQyxNQUFNLEdBQUcsQ0FBQyxDQUFDLENBQUM7UUFDNUMsS0FBSyxDQUFDLEtBQUssQ0FBQyxNQUFNLEdBQUcsQ0FBQyxDQUFDLENBQUMsUUFBUSxHQUFHLEtBQUssQ0FBQyxDQUFDLENBQUMsQ0FBQztRQUM1QyxLQUFLLENBQUMsS0FBSyxDQUFDLE1BQU0sR0FBRyxDQUFDLENBQUMsQ0FBQyxRQUFRLEdBQUcsS0FBSyxDQUFDLEtBQUssQ0FBQyxNQUFNLEdBQUcsQ0FBQyxDQUFDLENBQUM7UUFDM0QsTUFBTSxDQUFDLEtBQUssQ0FBQztJQUNqQixDQUFDO0NBQ0o7QUFBQTtBQUFBO0FBR0Q7O0dBRUc7QUFDSDtJQUlJLElBQUksSUFBSSxLQUFXLE1BQU0sQ0FBQyxJQUFJLENBQUMsS0FBSyxDQUFDLENBQUMsQ0FBQztJQUN2QyxJQUFJLFFBQVEsS0FBYSxNQUFNLENBQUMsSUFBSSxDQUFDLFNBQVMsQ0FBQyxDQUFDLENBQUM7SUFFakQsWUFBYSxJQUFVLEVBQUUsUUFBZ0I7UUFDckMsSUFBSSxDQUFDLEtBQUssR0FBRyxJQUFJLENBQUM7UUFDbEIsSUFBSSxDQUFDLFNBQVMsR0FBRyxRQUFRLENBQUM7SUFDOUIsQ0FBQztDQUNKO0FBQ0Q7O0dBRUc7QUFDSDtJQUdJLElBQUksTUFBTSxLQUFhLE1BQU0sQ0FBQyxJQUFJLENBQUMsT0FBTyxDQUFDLENBQUMsQ0FBQztJQUM3QyxJQUFJLFFBQVEsS0FBYSxNQUFNLENBQUMsSUFBSSxDQUFDLFNBQVMsQ0FBQyxDQUFDLENBQUM7SUFHakQsWUFBYSxNQUFNLEVBQUUsUUFBUTtRQUN6QixJQUFJLENBQUMsT0FBTyxHQUFHLE1BQU0sQ0FBQztRQUN0QixJQUFJLENBQUMsU0FBUyxHQUFHLFFBQVEsQ0FBQztJQUM5QixDQUFDO0NBQ0o7QUFFRDs7O0dBR0c7QUFDSDtJQWNJOzs7T0FHRztJQUNILFlBQWEsT0FBZTtRQWpCNUIsd0RBQXdEO1FBQ3hELDBFQUEwRTtRQUNsRSxxQkFBZ0IsR0FBc0IsRUFBRSxDQUFDO1FBR2pELDhCQUE4QjtRQUN0Qix5QkFBb0IsR0FBc0IsRUFBRSxDQUFDO1FBS3JELGtIQUFrSDtRQUMxRyxjQUFTLEdBQVksS0FBSyxDQUFDO1FBTS9CLElBQUksQ0FBQyxRQUFRLEdBQUcsT0FBTyxDQUFDO0lBQzVCLENBQUM7SUFFRDs7Ozs7T0FLRztJQUNJLFdBQVcsQ0FBRSxTQUEwQjtRQUMxQyxFQUFFLENBQUMsQ0FBRSxJQUFJLENBQUMsU0FBVSxDQUFDO1lBQUMsTUFBTSxtQ0FBbUM7UUFDL0Qsa0lBQWtJO1FBQ2xJLEVBQUUsQ0FBQyxDQUFFLElBQUksQ0FBQyxRQUFRLElBQUksU0FBUyxDQUFDLElBQUksQ0FBQyxlQUFlLENBQUMsY0FBYyxDQUFDLEtBQU0sQ0FBQztZQUFDLElBQUksQ0FBQyxvQkFBb0IsQ0FBQyxJQUFJLENBQUUsU0FBUyxDQUFFLENBQUM7UUFDeEgsSUFBSTtZQUFDLElBQUksQ0FBQyxnQkFBZ0IsQ0FBQyxJQUFJLENBQUUsU0FBUyxDQUFFLENBQUM7SUFDakQsQ0FBQztJQUVEOzs7O09BSUc7SUFDSSxRQUFRO1FBQ1gsRUFBRSxDQUFDLENBQUUsSUFBSSxDQUFDLFNBQVUsQ0FBQztZQUFDLE1BQU0sbUNBQW1DO1FBQy9ELElBQUksQ0FBQyxTQUFTLEdBQUcsSUFBSSxDQUFDO1FBQ3RCLEVBQUUsQ0FBQyxDQUFFLENBQUMsSUFBSSxDQUFDLG9CQUFvQixFQUFHLENBQUM7WUFDL0IsSUFBSSxDQUFDLGdCQUFnQixHQUFHLElBQUksQ0FBQyxnQkFBZ0IsQ0FBQyxJQUFJLENBQUMsQ0FBRSxFQUFtQixFQUFFLEVBQW1CLEVBQUcsRUFBRSxDQUFDLEVBQUUsQ0FBQyxRQUFRLEdBQUcsRUFBRSxDQUFDLFFBQVEsQ0FBRSxDQUFDO1FBQ25JLElBQUksWUFBWSxHQUFvQixJQUFJLENBQUMsZ0JBQWdCLENBQUMsQ0FBQyxDQUFDLENBQUM7UUFDN0QsSUFBSSxZQUFZLEdBQW9CLElBQUksQ0FBQyxnQkFBZ0IsQ0FBQyxDQUFDLENBQUMsQ0FBQztRQUM3RCxJQUFJLGlCQUFpQixHQUF1QyxDQUFDLFlBQVksRUFBRSxZQUFZLENBQUMsQ0FBQztRQUV6RixFQUFFLENBQUMsQ0FBRSxDQUFDLFlBQWEsQ0FBQztZQUFDLE1BQU0sQ0FBQyxJQUFJLENBQUM7UUFDakMsSUFBSSxHQUFHLEdBQUcsWUFBWSxDQUFDLFFBQVEsQ0FBQztRQUNoQyxJQUFJLEdBQUcsR0FBRyxZQUFZLENBQUMsQ0FBQyxDQUFDLFlBQVksQ0FBQyxRQUFRLENBQUMsQ0FBQyxDQUFDLElBQUksQ0FBQztRQUN0RCxJQUFJLG9CQUFvQixDQUFDO1FBQ3pCLElBQUksQ0FBQztZQUNELG9CQUFvQixHQUFHLElBQUksQ0FBQyxvQkFBb0IsQ0FBQyxHQUFHLENBQUUsQ0FBQyxDQUFDLEVBQUUsR0FBRyxFQUFFLENBQUMsQ0FBRSxDQUFDLENBQUMsUUFBUSxHQUFHLEdBQUksQ0FBQztnQkFBQyxNQUFNLEVBQUUsQ0FBQztZQUFDLElBQUk7Z0JBQUMsTUFBTSxDQUFDLENBQUMsRUFBQyxDQUFDLENBQUU7Z0JBQzVHLE1BQU0sQ0FBQyxDQUFFLENBQUMsQ0FBQyxFQUFFLENBQUMsQ0FBQyxDQUFDLFFBQVEsSUFBSSxHQUFHLElBQUksQ0FBRSxDQUFDLEdBQUcsSUFBSSxDQUFDLENBQUMsUUFBUSxJQUFJLEdBQUcsQ0FBRSxDQUFFLENBQUUsQ0FBQztRQUM3RSxDQUFDO1FBQUMsS0FBSyxDQUFDLENBQUUsQ0FBRSxDQUFDLENBQUMsQ0FBQztZQUNYLE1BQU0sQ0FBQyxJQUFJLENBQUMsQ0FBQyx1RkFBdUY7UUFDeEcsQ0FBQztRQUNELElBQUksQ0FBQyxpQkFBaUIsR0FBRyxpQkFBaUIsQ0FBQztRQUMzQyxJQUFJLENBQUMsb0JBQW9CLEdBQUcsb0JBQW9CLENBQUM7UUFDakQsTUFBTSxDQUFDLElBQUksQ0FBQztJQUNoQixDQUFDO0lBQ0Q7Ozs7T0FJRztJQUNJLG9CQUFvQjtRQUN2QixFQUFFLENBQUMsQ0FBRSxDQUFDLElBQUksQ0FBQyxTQUFVLENBQUM7WUFBQyxNQUFNLCtCQUErQixDQUFDO1FBQzdELElBQUksRUFBRSxHQUFvQixJQUFJLENBQUMsZ0JBQWdCLENBQUMsQ0FBQyxDQUFDLENBQUM7UUFDbkQsSUFBSSxFQUFFLEdBQW9CLElBQUksQ0FBQyxnQkFBZ0IsQ0FBQyxDQUFDLENBQUMsQ0FBQztRQUNuRCxJQUFJLENBQUMsaUJBQWlCLEdBQUcsQ0FBQyxFQUFFLEVBQUUsRUFBRSxDQUFDLENBQUM7UUFDbEMsTUFBTSxDQUFDLENBQUMsRUFBRSxDQUFDLENBQUMsQ0FBQyxFQUFFLENBQUMsSUFBSSxDQUFDLENBQUMsQ0FBQyxJQUFJLEVBQUUsRUFBRSxDQUFDLENBQUMsQ0FBQyxFQUFFLENBQUMsSUFBSSxDQUFDLENBQUMsQ0FBQyxJQUFJLENBQUMsQ0FBQztJQUN0RCxDQUFDO0lBQ0Q7Ozs7T0FJRztJQUNJLHNCQUFzQjtRQUN6QixFQUFFLENBQUMsQ0FBRSxDQUFDLElBQUksQ0FBQyxTQUFVLENBQUM7WUFBQyxNQUFNLCtCQUErQixDQUFDO1FBQzdELE1BQU0sQ0FBQyxJQUFJLENBQUMsb0JBQW9CLENBQUMsR0FBRyxDQUFFLENBQUMsQ0FBQyxFQUFFLENBQUMsQ0FBQyxDQUFDLElBQUksQ0FBRSxDQUFDO0lBQ3hELENBQUM7Q0FDSjs7Ozs7Ozs7Ozs7Ozs7QUMxYkQ7QUFBQTs7R0FFRztBQUk0RjtBQUMzQztBQUNGO0FBQ21FO0FBQ3RFO0FBRy9DOztHQUVHO0FBR0gsbURBQW1EO0FBQ25ELE1BQU0sZUFBZSxHQUFHLENBQUMsSUFBSSx3RUFBYSxFQUFFLEVBQUUsSUFBSSx1RUFBWSxFQUFFLEVBQUUsSUFBSSxzRUFBVyxFQUFFLEVBQUUsSUFBSSx5RUFBYyxFQUFFLENBQUMsQ0FBQztBQUUzRywyQkFBMkI7QUFDM0IsTUFBTSxhQUFhLEdBQUcsb0JBQW9CLENBQUM7QUFDM0MsOENBQThDO0FBQzlDLE1BQU0sa0JBQWtCLEdBQUcsV0FBVyxDQUFDO0FBQ3ZDLHFEQUFxRDtBQUNyRCxNQUFNLG9CQUFvQixHQUFHLE9BQU8sQ0FBQztBQUNyQyw4Q0FBOEM7QUFDOUMsTUFBTSxtQkFBbUIsR0FBRyxtQkFBbUIsQ0FBQztBQUNoRCw4Q0FBOEM7QUFDOUMsTUFBTSx3QkFBd0IsR0FBRyxZQUFZLENBQUM7QUFDOUMsaUNBQWlDO0FBQ2pDLE1BQU0sV0FBVyxHQUFHLGlCQUFpQixDQUFDO0FBQ3RDLGtDQUFrQztBQUNsQyxNQUFNLGNBQWMsR0FBRyxtQkFBbUIsQ0FBQztBQUMzQyxtREFBbUQ7QUFDbkQsTUFBTSxnQkFBZ0IsR0FBRyxTQUFTLENBQUM7QUFJbkM7O0dBRUc7QUFDRztJQUFOO1FBRUksdUNBQXVDO1FBQy9CLFdBQU0sR0FBRyxJQUFJLGtEQUFNLENBQUUsYUFBYSxDQUFFLENBQUM7UUFDN0MscURBQXFEO1FBQzdDLGdCQUFXLEdBQUcsMERBQVcsQ0FBQyxJQUFJLENBQUUsSUFBSSxDQUFDLE1BQU0sQ0FBRSxDQUFDO1FBQ3RELHVEQUF1RDtRQUMvQyxtQkFBYyxHQUFHLElBQUksZ0ZBQXFCLEVBQUUsQ0FBQztRQUNyRCxrQ0FBa0M7UUFDMUIsV0FBTSxHQUFRLEVBQUUsQ0FBQztJQTZFN0IsQ0FBQztJQTNFRzs7O09BR0c7SUFDSSxJQUFJLENBQUUsWUFBaUM7UUFDMUMsSUFBSSxDQUFDLFdBQVcsQ0FBQyxPQUFPLENBQUUsb0JBQW9CLEtBQUksQ0FBQyxNQUFNLEVBQUUsQ0FBRSxLQUFLLEVBQUcsRUFBRTtZQUNuRSxJQUFJLENBQUMsU0FBUyxDQUFFLElBQUksRUFBRSxjQUFjLENBQUUsQ0FBQyxJQUFJLENBQUUsS0FBSyxDQUFFLENBQUM7WUFDckQsSUFBSSxDQUFDLFNBQVMsQ0FBRSxJQUFJLEVBQUUsZ0JBQWdCLENBQUUsQ0FBQyxJQUFJLENBQUUsS0FBSyxDQUFFLENBQUM7WUFDdkQsWUFBWSxDQUFDLElBQUksQ0FBRSxJQUFJLEVBQUUsSUFBSSxDQUFFLENBQUM7UUFDcEMsQ0FBQyxDQUFFLENBQUM7SUFDUixDQUFDO0lBQ0Q7OztPQUdHO0lBQ0ssU0FBUyxDQUFFLFNBQTJCLEVBQUUsT0FBZTtRQUMzRCxNQUFNLENBQUMsVUFBVSxLQUFLO1lBQ2xCLE9BQU8sQ0FBQyxHQUFHLENBQUUsYUFBYSxHQUFHLEtBQUssQ0FBRSxDQUFDO1lBQ3JDLHdCQUF3QjtZQUN4QixJQUFJLFdBQVcsR0FBRyxTQUFTLENBQUMsV0FBVyxDQUFDLFNBQVMsQ0FBRSxPQUFPLEVBQUUsVUFBVSxZQUFpQjtnQkFDbkYsSUFBSSxDQUFDO29CQUNELElBQUksUUFBUSxHQUFRLElBQUksQ0FBQyxLQUFLLENBQUUsWUFBWSxDQUFDLElBQUksQ0FBRSxDQUFDO29CQUNwRCxJQUFJLE9BQU8sR0FBRyxlQUFlLENBQUMsSUFBSSxDQUFFLENBQUMsQ0FBQyxFQUFFLENBQUMsQ0FBQyxDQUFDLE9BQU8sRUFBRSxLQUFLLFFBQVEsQ0FBQyxJQUFJLENBQUUsQ0FBQztvQkFDekUsRUFBRSxDQUFDLENBQUUsQ0FBQyxPQUFRLENBQUM7d0JBQUMsTUFBTSwrQkFBK0IsR0FBRyxRQUFRLENBQUMsSUFBSSxDQUFDO29CQUN0RSxJQUFJO3dCQUFDLE9BQU8sQ0FBQyxPQUFPLENBQUUsUUFBUSxDQUFFLENBQUM7Z0JBQ3JDLENBQUM7Z0JBQUMsS0FBSyxDQUFDLENBQUUsQ0FBRSxDQUFDLENBQUMsQ0FBQztvQkFDWCxPQUFPLENBQUMsR0FBRyxDQUFFLCtCQUErQixHQUFHLENBQUMsQ0FBRSxDQUFDO29CQUNuRCxTQUFTLENBQUMsY0FBYyxDQUFDLE9BQU8sRUFBRSxDQUFDO2dCQUN2QyxDQUFDO1lBQ0wsQ0FBQyxDQUFFLENBQUM7UUFDUixDQUFDO0lBQ0wsQ0FBQztJQUVEOztPQUVHO0lBQ0ksY0FBYztRQUNqQixJQUFJLENBQUM7WUFDRCxJQUFJLENBQUMsV0FBVyxDQUFDLElBQUksQ0FBRSxrQkFBa0IsRUFBRSxJQUFJLENBQUMsTUFBTSxFQUFFLElBQUksQ0FBRSxDQUFDO1FBQ25FLENBQUM7UUFBQyxLQUFLLENBQUMsQ0FBRSxDQUFFLENBQUMsQ0FBQyxDQUFDO1lBQ1gsT0FBTyxDQUFDLEdBQUcsQ0FBRSxzQkFBc0IsR0FBRyxDQUFDLENBQUUsQ0FBQztZQUMxQyx5REFBTSxFQUFFLENBQUM7UUFDYixDQUFDO0lBQ0wsQ0FBQztJQUVEOztPQUVHO0lBQ0ksZUFBZSxDQUFFLFNBQXlCO1FBQzdDLEVBQUUsQ0FBQyxDQUFFLENBQUMsU0FBVSxDQUFDO1lBQUMseURBQU0sRUFBRSxDQUFDO1FBQzNCLElBQUksQ0FBQztZQUNELElBQUksVUFBVSxHQUFpQixvRUFBWSxDQUFDLEVBQUUsQ0FBRSxTQUFTLENBQUUsQ0FBQztZQUM1RCxJQUFJLENBQUMsV0FBVyxDQUFDLElBQUksQ0FBRSxtQkFBbUIsRUFBRSxJQUFJLENBQUMsTUFBTSxFQUFFLElBQUksQ0FBQyxTQUFTLENBQUUsVUFBVSxDQUFFLENBQUUsQ0FBQztRQUM1RixDQUFDO1FBQUMsS0FBSyxDQUFDLENBQUUsQ0FBRSxDQUFDLENBQUMsQ0FBQztZQUNYLE9BQU8sQ0FBQyxHQUFHLENBQUUsOEJBQThCLEdBQUcsU0FBUyxDQUFDLFFBQVEsRUFBRSxHQUFHLFdBQVcsR0FBRyxDQUFDLENBQUUsQ0FBQztZQUN2Rix5REFBTSxFQUFFLENBQUM7UUFDYixDQUFDO0lBQ0wsQ0FBQztJQUVEOztPQUVHO0lBQ0ksb0JBQW9CO1FBQ3ZCLElBQUksQ0FBQztZQUVELElBQUksQ0FBQyxXQUFXLENBQUMsSUFBSSxDQUFFLHdCQUF3QixFQUFFLElBQUksQ0FBQyxNQUFNLEVBQUUsSUFBSSxDQUFFLENBQUM7UUFDekUsQ0FBQztRQUFDLEtBQUssQ0FBQyxDQUFFLENBQUUsQ0FBQyxDQUFDLENBQUM7WUFDWCxPQUFPLENBQUMsR0FBRyxDQUFFLG9CQUFvQixHQUFHLENBQUMsQ0FBRSxDQUFDO1lBQ3hDLHlEQUFNLEVBQUUsQ0FBQztRQUNiLENBQUM7SUFDTCxDQUFDO0lBRU0sT0FBTztRQUNWLElBQUksQ0FBQyxXQUFXLENBQUMsSUFBSSxDQUFFLFdBQVcsRUFBRSxJQUFJLENBQUMsTUFBTSxFQUFFLElBQUksQ0FBRSxDQUFDO0lBQzVELENBQUM7Q0FDSjtBQUFBO0FBQUE7Ozs7Ozs7MERDbElEO0FBQ0EsYUFBYSxTQUEyRCxtQkFBbUIsZ0RBQWdELGFBQWEsS0FBSyxNQUFNLGdDQUFnQyxTQUFTLHFDQUFxQyxTQUFTLG1DQUFtQyxPQUFPLEtBQUssT0FBTyxnQkFBZ0IsYUFBYSwwQkFBMEIsMEJBQTBCLGdCQUFnQixVQUFVLFVBQVUsMENBQTBDLDhCQUF3QixvQkFBb0IsOENBQThDLGtDQUFrQyxZQUFZLFlBQVksbUNBQW1DLGlCQUFpQixnQkFBZ0Isc0JBQXNCLG9CQUFvQiwwQ0FBMEMsWUFBWSxXQUFXLFlBQVksU0FBUyxHQUFHO0FBQzF5QjtBQUNBOztBQUVBOztBQUVBOztBQUVBO0FBQ0E7QUFDQTtBQUNBOztBQUVBLENBQUMscUlBQXFJOztBQUV0SSxDQUFDLEVBQUUsa0NBQWtDO0FBQ3JDOztBQUVBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTs7QUFFQTs7QUFFQSxDQUFDLEVBQUUsMEJBQTBCO0FBQzdCOztBQUVBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7O0FBRUE7O0FBRUE7QUFDQTtBQUNBO0FBQ0EsR0FBRztBQUNIO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0Esa0JBQWtCLFFBQVE7QUFDMUI7QUFDQTtBQUNBLGlCQUFpQixzQkFBc0I7QUFDdkM7QUFDQTtBQUNBOztBQUVBO0FBQ0E7O0FBRUE7O0FBRUEsQ0FBQyxFQUFFLGdDQUFnQztBQUNuQzs7QUFFQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7O0FBRUE7O0FBRUEsQ0FBQyxHQUFHO0FBQ0o7O0FBRUE7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxLQUFLO0FBQ0w7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsZ0RBQWdEO0FBQ2hEO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLG1CQUFtQixzQkFBc0I7QUFDekM7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7O0FBRUEsQ0FBQyxHQUFHO0FBQ0o7O0FBRUE7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7O0FBRUE7O0FBRUEsQ0FBQyxFQUFFLDBCQUEwQjtBQUM3Qjs7QUFFQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTs7QUFFQSxDQUFDLEVBQUUsK0JBQStCO0FBQ2xDO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxHQUFHOztBQUVIO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQSxPQUFPO0FBQ1A7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxTQUFTO0FBQ1Q7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7O0FBRUEsQ0FBQyxhQUFhLFFBQVEsRUFBRTs7QUFFeEIsQ0FBQyxFQUFFLHVJQUF1STtBQUMxSTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxTQUFTO0FBQ1Q7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLEdBQUc7QUFDSDs7QUFFQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTs7QUFFQSxDQUFDLGFBQWEsUUFBUSxFQUFFOztBQUV4QixDQUFDLEVBQUUsbUVBQW1FO0FBQ3RFOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQSxHQUFHO0FBQ0g7O0FBRUE7O0FBRUE7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7O0FBRUEsQ0FBQyxFQUFFLHNGQUFzRjtBQUN6RjtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxTQUFTO0FBQ1Q7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLEtBQUs7O0FBRUw7QUFDQTtBQUNBO0FBQ0EsS0FBSztBQUNMOztBQUVBO0FBQ0E7QUFDQTtBQUNBLEdBQUc7QUFDSDtBQUNBO0FBQ0E7O0FBRUE7O0FBRUE7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQUVBOztBQUVBLENBQUMsYUFBYSxRQUFRLEVBQUUsMEhBQTBIOztBQUVsSixDQUFDLEVBQUUsc0hBQXNIO0FBQ3pIO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBLEdBQUc7QUFDSDs7QUFFQTs7QUFFQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQUVBOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsR0FBRzs7QUFFSDtBQUNBO0FBQ0E7QUFDQTtBQUNBLEdBQUc7QUFDSDs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTs7QUFFQTs7QUFFQSxDQUFDLGFBQWEsUUFBUSxFQUFFOztBQUV4QixDQUFDLEVBQUUsd05BQXdOO0FBQzNOO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQSxDQUFDLHFJQUFxSTs7QUFFdEksQ0FBQyxHQUFHO0FBQ0o7QUFDQTs7QUFFQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQSxHQUFHO0FBQ0g7QUFDQTtBQUNBO0FBQ0EsR0FBRztBQUNIO0FBQ0E7O0FBRUE7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQSxHQUFHO0FBQ0g7QUFDQSxHQUFHO0FBQ0g7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsR0FBRztBQUNIO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsR0FBRzs7QUFFSDtBQUNBO0FBQ0E7O0FBRUE7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7O0FBRUE7O0FBRUE7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQUVBOztBQUVBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTs7QUFFQTtBQUNBLGdEQUFnRCxXQUFXO0FBQzNEO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQSxLQUFLO0FBQ0w7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLFNBQVM7QUFDVDtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxHQUFHO0FBQ0g7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0EsR0FBRztBQUNIOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsbUJBQW1CO0FBQ25CO0FBQ0EsbUJBQW1CO0FBQ25COztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUEsQ0FBQyxhQUFhLFFBQVEsRUFBRSwwSEFBMEg7O0FBRWxKLENBQUMsRUFBRSw4WEFBOFg7QUFDalk7QUFDQTtBQUNBOztBQUVBOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBLGdDQUFnQyxTQUFTO0FBQ3pDO0FBQ0EsS0FBSyxZQUFZO0FBQ2pCO0FBQ0E7QUFDQSxDQUFDOztBQUVEO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSwrQ0FBK0MsUUFBUTtBQUN2RDtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsU0FBUztBQUNUO0FBQ0EsQ0FBQztBQUNEO0FBQ0EsK0NBQStDLFFBQVE7QUFDdkQ7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQSxvQkFBb0I7QUFDcEI7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQSxrQkFBa0I7QUFDbEI7QUFDQSxLQUFLO0FBQ0w7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7O0FBRUE7O0FBRUE7QUFDQSwrQkFBK0I7QUFDL0I7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0Esa0RBQWtEO0FBQ2xEO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUEsYUFBYTtBQUNiO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTs7QUFFQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQUVBOztBQUVBO0FBQ0E7QUFDQTtBQUNBLHVCQUF1QixpQkFBaUI7QUFDeEM7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxzRkFBc0Ysc0NBQXNDLEVBQUU7O0FBRTlIO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBLHFEQUFxRDtBQUNyRCxxRUFBcUU7QUFDckU7QUFDQTtBQUNBLHdEQUF3RDtBQUN4RCw4RUFBOEU7QUFDOUU7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQSxDQUFDOztBQUVEO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBLHlCQUF5QixtQkFBbUI7OztBQUc1QztBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLDhDQUE4QyxnQ0FBZ0M7QUFDOUUsU0FBUzs7QUFFVDtBQUNBO0FBQ0E7QUFDQSxTQUFTO0FBQ1Q7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQSxrQ0FBa0M7QUFDbEM7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsQ0FBQzs7QUFFRDtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0EsY0FBYyxZQUFZO0FBQzFCO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLENBQUM7O0FBRUQ7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsOERBQThEOztBQUU5RDtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLHlCQUF5QjtBQUN6QjtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLDZEQUE2RDtBQUM3RDtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSwyQ0FBMkMsMEJBQTBCO0FBQ3JFO0FBQ0E7QUFDQTtBQUNBO0FBQ0EseUJBQXlCO0FBQ3pCO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSwwQ0FBMEM7QUFDMUM7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsYUFBYTtBQUNiO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsS0FBSzs7QUFFTDtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxDQUFDO0FBQ0Q7QUFDQSxrREFBa0QsV0FBVztBQUM3RDtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsQ0FBQzs7QUFFRCxDQUFDLEdBQUc7QUFDSjs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQUVBLENBQUMsRUFBRSwwUUFBMFE7QUFDN1E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBLEdBQUc7QUFDSDs7QUFFQTs7QUFFQTtBQUNBOztBQUVBO0FBQ0E7QUFDQSxHQUFHO0FBQ0g7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsR0FBRztBQUNIO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsR0FBRztBQUNIO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxTQUFTO0FBQ1Q7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQSxHQUFHO0FBQ0g7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQSxLQUFLO0FBQ0w7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxLQUFLO0FBQ0w7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQSxDQUFDO0FBQ0Q7QUFDQTs7QUFFQTs7QUFFQTs7QUFFQSxDQUFDLGFBQWEsUUFBUSxFQUFFLDBIQUEwSDs7QUFFbEosQ0FBQyxFQUFFLGdGQUFnRjtBQUNuRjtBQUNBOztBQUVBLENBQUMscUlBQXFJOztBQUV0SSxDQUFDLEdBQUc7QUFDSjtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxDQUFDO0FBQ0Q7QUFDQTs7QUFFQSxDQUFDLHFJQUFxSTs7QUFFdEksQ0FBQyxHQUFHO0FBQ0o7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7O0FBRUE7O0FBRUE7QUFDQTtBQUNBOztBQUVBO0FBQ0E7O0FBRUE7O0FBRUEsQ0FBQyxFQUFFLHdHQUF3RztBQUMzRzs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTs7QUFFQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTs7QUFFQTs7QUFFQSxDQUFDLEVBQUUscUZBQXFGO0FBQ3hGO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQSxHQUFHOztBQUVIO0FBQ0E7QUFDQTs7QUFFQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxLQUFLO0FBQ0w7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBLEdBQUc7QUFDSDtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsS0FBSztBQUNMO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsR0FBRztBQUNIOztBQUVBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBOztBQUVBOztBQUVBLENBQUMsYUFBYSxRQUFRLEVBQUU7O0FBRXhCLENBQUMsRUFBRSwrSUFBK0k7QUFDbEo7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTs7QUFFQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBOztBQUVBOztBQUVBLENBQUMscUlBQXFJOztBQUV0SSxDQUFDLEVBQUUsbUZBQW1GO0FBQ3RGO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLHFCQUFxQjtBQUNyQjtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQSxLQUFLO0FBQ0w7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTs7QUFFQTs7QUFFQTs7QUFFQSxDQUFDLGFBQWEsUUFBUSxFQUFFOztBQUV4QixDQUFDLEVBQUUscUVBQXFFO0FBQ3hFO0FBQ0E7O0FBRUE7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQUVBOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLEdBQUc7QUFDSDs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsT0FBTztBQUNQO0FBQ0E7QUFDQSxLQUFLO0FBQ0w7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7O0FBRUEsQ0FBQyxhQUFhLFFBQVEsRUFBRTs7QUFFeEIsQ0FBQyxFQUFFLG9DQUFvQztBQUN2QztBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBOztBQUVBOztBQUVBO0FBQ0E7QUFDQTs7QUFFQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQSwwQ0FBMEM7QUFDMUM7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQSx5RkFBeUY7O0FBRXpGOztBQUVBO0FBQ0E7O0FBRUEsQ0FBQyxxSUFBcUk7O0FBRXRJLENBQUMsRUFBRSxxREFBcUQ7QUFDeEQ7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQSxHQUFHOztBQUVIO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQSxPQUFPO0FBQ1A7QUFDQTtBQUNBO0FBQ0E7QUFDQSxHQUFHO0FBQ0g7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTs7QUFFQSxDQUFDLGFBQWEsUUFBUSxFQUFFOztBQUV4QixDQUFDLEVBQUUsb0NBQW9DO0FBQ3ZDO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsR0FBRztBQUNIO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxHQUFHO0FBQ0g7O0FBRUE7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQUVBOztBQUVBLENBQUMsYUFBYSxRQUFRLEVBQUU7O0FBRXhCLENBQUMsRUFBRSxvRkFBb0Y7QUFDdkY7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQUVBOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLEdBQUc7QUFDSDs7QUFFQTs7QUFFQSxDQUFDLGFBQWEsUUFBUSxFQUFFOztBQUV4QixDQUFDLEVBQUUscURBQXFEO0FBQ3hEO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxHQUFHO0FBQ0g7O0FBRUE7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLEdBQUc7QUFDSDtBQUNBO0FBQ0E7O0FBRUE7O0FBRUE7O0FBRUEsQ0FBQyxhQUFhLFFBQVEsRUFBRSwwSEFBMEg7O0FBRWxKLENBQUMsRUFBRSx5R0FBeUc7QUFDNUc7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTs7QUFFQTtBQUNBOztBQUVBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQSxHQUFHO0FBQ0g7O0FBRUE7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLEdBQUc7QUFDSDs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLGNBQWM7O0FBRWQ7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsU0FBUztBQUNUO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLE9BQU87QUFDUDtBQUNBO0FBQ0E7QUFDQSxLQUFLO0FBQ0w7QUFDQTtBQUNBLDBCQUEwQixxREFBcUQsbUJBQW1CLFdBQVc7QUFDN0c7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTs7QUFFQSxDQUFDLGFBQWEsUUFBUSxFQUFFLDBIQUEwSDs7QUFFbEosQ0FBQyxFQUFFLGtJQUFrSTtBQUNySTtBQUNBOztBQUVBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLEdBQUc7QUFDSDs7QUFFQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQUVBLG9CQUFvQixFQUFFO0FBQ3RCO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTs7QUFFQSxDQUFDLGFBQWEsUUFBUSxFQUFFOztBQUV4QixDQUFDLEVBQUUsb0NBQW9DO0FBQ3ZDO0FBQ0E7O0FBRUE7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBOztBQUVBOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxHQUFHO0FBQ0g7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBLEdBQUc7QUFDSDtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsS0FBSztBQUNMO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQSxDQUFDLGFBQWEsUUFBUSxFQUFFLDBIQUEwSDs7QUFFbEosQ0FBQyxFQUFFLHdEQUF3RDtBQUMzRDtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBLEdBQUc7QUFDSDs7QUFFQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLEdBQUc7QUFDSDtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLEdBQUc7QUFDSDtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQSxLQUFLO0FBQ0w7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBOztBQUVBOztBQUVBLENBQUMsYUFBYSxRQUFRLEVBQUUsMEhBQTBIOztBQUVsSixDQUFDLEVBQUUseUdBQXlHO0FBQzVHOztBQUVBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7O0FBRUE7O0FBRUE7O0FBRUE7O0FBRUEsQ0FBQyxFQUFFLGlDQUFpQztBQUNwQzs7QUFFQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBOztBQUVBO0FBQ0EsZ0NBQWdDO0FBQ2hDLEdBQUc7QUFDSDs7QUFFQTs7QUFFQTtBQUNBO0FBQ0E7O0FBRUE7O0FBRUE7O0FBRUEsQ0FBQyxFQUFFLHlCQUF5QjtBQUM1Qjs7QUFFQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0EsR0FBRztBQUNIOztBQUVBOztBQUVBOztBQUVBOztBQUVBLENBQUMsRUFBRSxpQ0FBaUM7QUFDcEM7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLEdBQUc7QUFDSDtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLEdBQUc7QUFDSDtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQUVBOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7O0FBRUEsQ0FBQyxhQUFhLFFBQVEsRUFBRTs7QUFFeEIsQ0FBQyxFQUFFLGtHQUFrRztBQUNyRzs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQUVBOztBQUVBO0FBQ0E7QUFDQSxtQ0FBbUM7O0FBRW5DOztBQUVBLENBQUMsRUFBRSwrRkFBK0Y7QUFDbEc7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQUVBOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBLHFDQUFxQzs7QUFFckM7O0FBRUEsQ0FBQyxFQUFFLDBFQUEwRTtBQUM3RTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQUVBOztBQUVBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQSxtQ0FBbUM7O0FBRW5DOztBQUVBLENBQUMsRUFBRSx1R0FBdUc7QUFDMUc7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7O0FBRUE7QUFDQSxxQ0FBcUM7O0FBRXJDO0FBQ0E7QUFDQTtBQUNBOztBQUVBOztBQUVBLENBQUMscUlBQXFJOztBQUV0SSxDQUFDLEVBQUUsNkhBQTZIO0FBQ2hJO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsQ0FBQztBQUNEO0FBQ0E7QUFDQSxtQkFBbUIsWUFBWTtBQUMvQjtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQUVBLENBQUMscUlBQXFJOztBQUV0SSxDQUFDLEdBQUc7QUFDSjtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQSxLQUFLO0FBQ0w7QUFDQTtBQUNBO0FBQ0E7O0FBRUEsQ0FBQyxxSUFBcUk7O0FBRXRJLENBQUMsR0FBRztBQUNKOztBQUVBOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsYUFBYSxXQUFXO0FBQ3hCO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLEdBQUc7QUFDSDtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQSxLQUFLO0FBQ0w7QUFDQTs7QUFFQSxDQUFDLEVBQUUsV0FBVztBQUNkO0FBQ0E7O0FBRUE7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLEtBQUs7QUFDTDtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBLEtBQUs7QUFDTDtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUEsQ0FBQyxxSUFBcUk7O0FBRXRJLENBQUMsRUFBRSxjQUFjO0FBQ2pCO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsT0FBTztBQUNQLEtBQUs7QUFDTDtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsT0FBTztBQUNQO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsU0FBUztBQUNUO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsU0FBUztBQUNULE9BQU87QUFDUDtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxPQUFPO0FBQ1A7QUFDQTtBQUNBO0FBQ0E7QUFDQSxLQUFLO0FBQ0w7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxTQUFTO0FBQ1QsT0FBTztBQUNQO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0EsZ0VBQWdFO0FBQ2hFO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsS0FBSztBQUNMO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUEsQ0FBQyxhQUFhLFFBQVEsRUFBRSwwSEFBMEg7O0FBRWxKLENBQUMsRUFBRSxrREFBa0Q7QUFDckQ7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBLEdBQUc7QUFDSDtBQUNBOztBQUVBO0FBQ0E7QUFDQSxHQUFHLG9DQUFvQztBQUN2QyxDQUFDOztBQUVEOztBQUVBLENBQUMscUlBQXFJOztBQUV0SSxDQUFDLEdBQUc7QUFDSjs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSw4Q0FBOEMsWUFBWTtBQUMxRDtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQSxDQUFDLEdBQUc7QUFDSjs7QUFFQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxtQkFBbUIsWUFBWTtBQUMvQjtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQUVBLENBQUMsRUFBRSxZQUFZO0FBQ2Y7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxPQUFPO0FBQ1A7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLFNBQVM7QUFDVDtBQUNBO0FBQ0EsT0FBTztBQUNQO0FBQ0E7QUFDQTtBQUNBOztBQUVBLENBQUMsYUFBYSxRQUFRLEVBQUU7O0FBRXhCLENBQUMsRUFBRSxXQUFXO0FBQ2Q7QUFDQTs7QUFFQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7O0FBRUEsQ0FBQyxhQUFhLFFBQVEsRUFBRTs7QUFFeEIsQ0FBQyxFQUFFLDBCQUEwQjtBQUM3Qjs7QUFFQSxDQUFDLEdBQUc7QUFDSjtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLFdBQVcsY0FBYztBQUN6QixXQUFXLE9BQU87QUFDbEIsWUFBWSxNQUFNO0FBQ2xCLFlBQVk7QUFDWjtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxHQUFHO0FBQ0g7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBLFdBQVcsT0FBTztBQUNsQixZQUFZO0FBQ1o7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBLFdBQVcsT0FBTztBQUNsQixZQUFZO0FBQ1o7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0EsV0FBVyxPQUFPO0FBQ2xCLFlBQVk7QUFDWjtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUEsQ0FBQyxHQUFHO0FBQ0o7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBLEdBQUc7QUFDSDtBQUNBO0FBQ0E7OztBQUdBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7O0FBRUE7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLEdBQUc7O0FBRUg7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0EsV0FBVyxPQUFPO0FBQ2xCO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQSxLQUFLO0FBQ0w7QUFDQTtBQUNBLEdBQUc7QUFDSDs7QUFFQTtBQUNBO0FBQ0E7QUFDQSxZQUFZLE9BQU87QUFDbkI7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLEdBQUc7O0FBRUg7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7O0FBRUE7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxZQUFZO0FBQ1o7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQSxHQUFHO0FBQ0g7O0FBRUEsQ0FBQyxhQUFhLFFBQVEsRUFBRTs7QUFFeEIsQ0FBQyxFQUFFLGFBQWE7O0FBRWhCO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBOztBQUVBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTs7QUFFQTtBQUNBO0FBQ0E7O0FBRUE7O0FBRUE7QUFDQTtBQUNBLFdBQVcsT0FBTztBQUNsQixZQUFZO0FBQ1o7QUFDQTs7QUFFQTtBQUNBOztBQUVBO0FBQ0E7QUFDQSxjQUFjO0FBQ2Q7O0FBRUE7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQSxXQUFXLE9BQU87QUFDbEIsWUFBWTtBQUNaO0FBQ0E7O0FBRUE7O0FBRUE7QUFDQTtBQUNBOztBQUVBOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQSxtQkFBbUIsaUJBQWlCO0FBQ3BDO0FBQ0E7O0FBRUE7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLEtBQUs7O0FBRUw7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLFdBQVcsT0FBTztBQUNsQjtBQUNBOztBQUVBO0FBQ0E7O0FBRUE7QUFDQTs7QUFFQTtBQUNBOztBQUVBLGlCQUFpQixTQUFTO0FBQzFCLDRCQUE0QjtBQUM1QjtBQUNBO0FBQ0E7QUFDQSxLQUFLO0FBQ0w7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0EsV0FBVyxPQUFPO0FBQ2xCLFlBQVk7QUFDWjtBQUNBOztBQUVBO0FBQ0E7QUFDQSx5Q0FBeUMsU0FBUztBQUNsRDtBQUNBO0FBQ0E7QUFDQTtBQUNBLHlDQUF5QyxTQUFTO0FBQ2xEO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQSxXQUFXLE1BQU07QUFDakIsWUFBWTtBQUNaO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7O0FBRUEsQ0FBQyxFQUFFLFFBQVE7QUFDWDtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsS0FBSztBQUNMO0FBQ0EsQ0FBQztBQUNEO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQSxDQUFDLEdBQUc7QUFDSjtBQUNBO0FBQ0EsQ0FBQztBQUNEO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsS0FBSzs7QUFFTDtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxPQUFPO0FBQ1A7QUFDQTtBQUNBO0FBQ0EsT0FBTztBQUNQLGtDQUFrQyxpREFBaUQ7QUFDbkY7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxhQUFhO0FBQ2I7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsMkJBQTJCLG9EQUFvRDtBQUMvRTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsYUFBYTtBQUNiO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLG1CQUFtQjtBQUNuQjtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxxQkFBcUI7QUFDckI7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxxQkFBcUI7QUFDckI7QUFDQTtBQUNBO0FBQ0EsYUFBYTtBQUNiO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLDBCQUEwQjtBQUMxQjtBQUNBO0FBQ0E7QUFDQTtBQUNBLFdBQVc7QUFDWDtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsV0FBVztBQUNYO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxTQUFTOztBQUVUO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0Esb0NBQW9DO0FBQ3BDO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSx5Q0FBeUMsOEJBQThCO0FBQ3ZFO0FBQ0EsU0FBUztBQUNUO0FBQ0E7QUFDQTtBQUNBLDRCQUE0QjtBQUM1QjtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxTQUFTO0FBQ1QsOEJBQThCO0FBQzlCO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsZ0JBQWdCLGdCQUFnQjtBQUNoQztBQUNBO0FBQ0EseUNBQXlDO0FBQ3pDO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxXQUFXO0FBQ1g7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsZ0VBQWdFLDZCQUE2QjtBQUM3Rix1RUFBdUUsaUNBQWlDO0FBQ3hHO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLGlCQUFpQjtBQUNqQjtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSw0QkFBNEI7QUFDNUI7QUFDQTtBQUNBO0FBQ0EsZUFBZTtBQUNmO0FBQ0E7QUFDQSxhQUFhO0FBQ2I7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxXQUFXO0FBQ1g7QUFDQTtBQUNBO0FBQ0EsV0FBVztBQUNYO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxvRUFBb0U7QUFDcEU7QUFDQSx1Q0FBdUMsVUFBVTtBQUNqRDtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLG9EQUFvRCxnQkFBZ0I7QUFDcEU7QUFDQTtBQUNBO0FBQ0E7QUFDQSxhQUFhO0FBQ2I7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EscUVBQXFFO0FBQ3JFLDREQUE0RDtBQUM1RDtBQUNBO0FBQ0EsaURBQWlELE1BQU07QUFDdkQ7QUFDQTtBQUNBO0FBQ0EsZUFBZTtBQUNmLHdEQUF3RCwwRUFBMEUsT0FBTywwQkFBMEIsU0FBUztBQUM1SztBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsYUFBYTtBQUNiO0FBQ0E7QUFDQSxnRUFBZ0UsZ0JBQWdCO0FBQ2hGO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsaUVBQWlFLDJCQUEyQjtBQUM1RjtBQUNBLGFBQWE7QUFDYjtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsOEJBQThCLE9BQU87QUFDckMsMENBQTBDO0FBQzFDO0FBQ0E7O0FBRUE7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLCtDQUErQyxLQUFLO0FBQ3BEO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSw4Q0FBOEM7QUFDOUM7QUFDQTtBQUNBO0FBQ0EsMENBQTBDLGdCQUFnQjtBQUMxRDtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsbUJBQW1CO0FBQ25CO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLGtEQUFrRCxrQkFBa0I7QUFDcEU7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsbUJBQW1CO0FBQ25CO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLHdCQUF3Qiw2RkFBNkY7QUFDckgsbUVBQW1FO0FBQ25FO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsMEJBQTBCLG1HQUFtRztBQUM3SDtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsMENBQTBDLG1HQUFtRztBQUM3STtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsaUJBQWlCO0FBQ2pCO0FBQ0E7QUFDQSxpQkFBaUI7QUFDakI7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLHFCQUFxQjtBQUNyQjtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxtQkFBbUI7QUFDbkI7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLGFBQWEscUJBQXFCO0FBQ2xDO0FBQ0E7QUFDQSxxQkFBcUI7QUFDckI7QUFDQTtBQUNBLCtCQUErQjtBQUMvQjtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLG1DQUFtQztBQUNuQztBQUNBO0FBQ0E7QUFDQSxtQkFBbUI7QUFDbkI7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLFdBQVc7QUFDWDtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSx5Q0FBeUMsVUFBVTtBQUNuRDtBQUNBO0FBQ0EsYUFBYTtBQUNiO0FBQ0E7QUFDQSxlQUFlO0FBQ2Y7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0Esd0ZBQXdGO0FBQ3hGO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0EsR0FBRztBQUNIO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLEtBQUs7O0FBRUw7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLEtBQUs7QUFDTDtBQUNBLENBQUM7O0FBRUQsQ0FBQyxxSUFBcUk7O0FBRXRJLENBQUMsR0FBRztBQUNKOztBQUVBOztBQUVBO0FBQ0E7QUFDQTtBQUNBLFdBQVcsT0FBTztBQUNsQixhQUFhO0FBQ2I7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBLFdBQVcsT0FBTztBQUNsQixXQUFXLE9BQU87QUFDbEIsYUFBYTtBQUNiO0FBQ0E7QUFDQTtBQUNBOztBQUVBOztBQUVBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQUVBLENBQUMsR0FBRztBQUNKOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsV0FBVyxjQUFjO0FBQ3pCLFdBQVcsT0FBTztBQUNsQixhQUFhLFFBQVE7QUFDckI7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTs7QUFFQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBOztBQUVBLENBQUMsR0FBRztBQUNKOztBQUVBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQSxjQUFjLE9BQU87QUFDckIsY0FBYyxRQUFRO0FBQ3RCLGNBQWMsT0FBTztBQUNyQjs7QUFFQTtBQUNBO0FBQ0E7QUFDQSxXQUFXLE9BQU87QUFDbEIsWUFBWSxnQkFBZ0I7QUFDNUI7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBLFdBQVcsT0FBTztBQUNsQixXQUFXLE9BQU87QUFDbEIsWUFBWSxPQUFPO0FBQ25CO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0EsS0FBSztBQUNMO0FBQ0E7QUFDQSxLQUFLO0FBQ0w7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBOztBQUVBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsV0FBVyxPQUFPO0FBQ2xCLFdBQVcsY0FBYztBQUN6QixXQUFXLGlCQUFpQjtBQUM1QjtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7O0FBRUE7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQUVBLFFBQVEseUJBQXlCO0FBQ2pDO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0EsS0FBSztBQUNMO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsU0FBUztBQUNUO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsS0FBSztBQUNMO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQSxXQUFXLE9BQU87QUFDbEIsV0FBVyxNQUFNO0FBQ2pCLFdBQVcsaUJBQWlCO0FBQzVCO0FBQ0E7QUFDQTtBQUNBLGFBQWE7QUFDYjtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7O0FBRUE7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQSxPQUFPO0FBQ1A7QUFDQTs7QUFFQTs7QUFFQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsT0FBTztBQUNQO0FBQ0E7QUFDQTs7QUFFQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBOztBQUVBOztBQUVBO0FBQ0E7QUFDQTs7QUFFQSxpQkFBaUIsa0JBQWtCO0FBQ25DOztBQUVBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBOztBQUVBOztBQUVBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0EsV0FBVyxTQUFTO0FBQ3BCLGFBQWE7QUFDYjtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7O0FBRUE7O0FBRUE7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTs7QUFFQTtBQUNBOztBQUVBOztBQUVBO0FBQ0E7O0FBRUEsaUJBQWlCOztBQUVqQjtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTs7QUFFQSxDQUFDLEVBQUUsd0RBQXdEO0FBQzNEO0FBQ0E7O0FBRUE7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLFVBQVU7QUFDVjtBQUNBO0FBQ0EsY0FBYztBQUNkOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxXQUFXLGNBQWM7QUFDekIsYUFBYSxPQUFPO0FBQ3BCO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBOztBQUVBO0FBQ0EseURBQXlEO0FBQ3pELEdBQUc7QUFDSCxzQ0FBc0M7QUFDdEM7QUFDQSxHQUFHO0FBQ0g7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTs7QUFFQSxDQUFDLHFJQUFxSTs7QUFFdEksQ0FBQyxFQUFFLFFBQVEsRUFBRSxHQUFHO0FBQ2hCLENBQUM7OztBQUdELGtDOzs7Ozs7O0FDcG1MQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQSxDQUFDOztBQUVEO0FBQ0E7QUFDQTtBQUNBLENBQUM7QUFDRDtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0EsNENBQTRDOztBQUU1Qzs7Ozs7OztBQ3BCQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBLGdCQUFnQjtBQUNoQjs7QUFFQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLFNBQVM7QUFDVDtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0EsT0FBTztBQUNQO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLG9DQUFvQyxVQUFVO0FBQzlDO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsU0FBUztBQUNUO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxPQUFPO0FBQ1A7QUFDQSxzREFBc0Qsb0NBQW9DO0FBQzFGO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0Esc0NBQXNDLFVBQVU7QUFDaEQ7QUFDQTtBQUNBO0FBQ0E7QUFDQSxPQUFPO0FBQ1A7QUFDQTtBQUNBO0FBQ0EsT0FBTztBQUNQO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTs7QUFFQTs7QUFFQSxHQUFHOztBQUVIO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBLE9BQU87QUFDUDtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxTQUFTO0FBQ1Q7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsdUNBQXVDLFVBQVU7QUFDakQ7QUFDQTtBQUNBO0FBQ0E7QUFDQSxPQUFPO0FBQ1A7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxTQUFTO0FBQ1Q7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLFNBQVM7QUFDVDtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxXQUFXO0FBQ1g7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSwwQ0FBMEMsVUFBVTtBQUNwRDtBQUNBO0FBQ0E7QUFDQTtBQUNBLFdBQVc7QUFDWDtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0Esd0NBQXdDLFVBQVU7QUFDbEQ7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsbUJBQW1CO0FBQ25CO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLGlCQUFpQjtBQUNqQjtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsaUJBQWlCO0FBQ2pCO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLE9BQU87QUFDUDtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLE9BQU87QUFDUDtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxPQUFPO0FBQ1A7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsYUFBYTtBQUNiO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLFNBQVM7QUFDVDtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxPQUFPO0FBQ1A7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLFNBQVM7QUFDVDtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBLE9BQU87QUFDUDs7QUFFQTtBQUNBO0FBQ0E7QUFDQSxPQUFPO0FBQ1A7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsT0FBTztBQUNQO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsT0FBTztBQUNQO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7O0FBRUEsR0FBRzs7QUFFSDtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsS0FBSztBQUNMO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxLQUFLO0FBQ0w7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsS0FBSztBQUNMO0FBQ0E7O0FBRUE7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBLEdBQUc7QUFDSDtBQUNBOztBQUVBLENBQUMsYTs7Ozs7Ozs7Ozs7O0FDbmxCRDtBQUFBOztHQUVHO0FBRWtGO0FBRXNCO0FBR1U7QUFDOUM7QUFDUDtBQUN3QztBQUl4RyxtREFBbUQ7QUFDbkQsTUFBTSxXQUFXLEdBQUcsSUFBSSxzRUFBVyxFQUFFLENBQUM7QUFDdEMsTUFBTSxhQUFhLEdBQUcsSUFBSSx3RUFBYSxFQUFFLENBQUM7QUFDMUMsTUFBTSxZQUFZLEdBQUcsSUFBSSx1RUFBWSxFQUFFLENBQUM7QUFDeEMsTUFBTSxjQUFjLEdBQUcsSUFBSSx5RUFBYyxFQUFFLENBQUM7QUFFNUM7O0dBRUc7QUFDRztJQUFOO1FBQ0ksOENBQThDO1FBQ3RDLGNBQVMsR0FBcUIsRUFBRSxDQUFDO1FBQ3pDLHNDQUFzQztRQUM5QixVQUFLLEdBQUcsQ0FBQyxDQUFDO0lBd0V0QixDQUFDO0lBdkVHOzs7T0FHRztJQUNJLElBQUksQ0FBRSxZQUFpQztRQUMxQyxXQUFXLENBQUMsR0FBRyxFQUFFO1lBQ2IsR0FBRyxDQUFDLENBQUUsSUFBSSxRQUFRLElBQUksSUFBSSxDQUFDLFNBQVUsQ0FBQztnQkFBQyxJQUFJLENBQUMsS0FBSyxJQUFJLG1GQUFnQixDQUFFLFFBQVEsQ0FBRSxDQUFDO1lBQ2xGLDZEQUFVLENBQUUsSUFBSSxDQUFDLEtBQUssQ0FBRSxDQUFDO1FBQzdCLENBQUMsRUFBRSxJQUFJLENBQUUsQ0FBQztRQUNWLFlBQVksQ0FBQyxJQUFJLENBQUUsSUFBSSxFQUFFLElBQUksQ0FBRSxDQUFDO0lBQ3BDLENBQUM7SUFHRDs7T0FFRztJQUNJLGNBQWM7UUFDakIsSUFBSSxNQUFNLEdBQUcsaUJBQWlCLENBQUM7UUFDL0IsSUFBSSxFQUFFLEdBQUcsSUFBSSx3RUFBTSxDQUFFLENBQUMsRUFBRSxDQUFDLENBQUUsQ0FBQztRQUFDLElBQUksRUFBRSxHQUFHLElBQUksd0VBQU0sQ0FBRSxFQUFFLEVBQUUsQ0FBQyxDQUFFLENBQUM7UUFBQyxJQUFJLEVBQUUsR0FBRyxJQUFJLHdFQUFNLENBQUUsQ0FBQyxFQUFFLEVBQUUsQ0FBRSxDQUFDO1FBQ3hGLElBQUksS0FBSyxHQUFHLHlGQUFlLENBQUUsRUFBRSxFQUFFLEVBQUUsRUFBRSxFQUFFLENBQUUsQ0FBQztRQUMxQyxJQUFJLFFBQVEsR0FBRyxJQUFJLDBFQUFRLENBQUUsS0FBSyxDQUFDLENBQUMsQ0FBQyxDQUFFLENBQUM7UUFDeEMsSUFBSSxDQUFDLFNBQVMsQ0FBQyxJQUFJLENBQUUsb0ZBQWMsQ0FBQyxFQUFFLENBQUUsUUFBUSxFQUFFLE1BQU0sQ0FBRSxDQUFFLENBQUM7UUFDN0QsSUFBSSxHQUFHLEdBQUcsa0VBQVUsQ0FBQyxFQUFFLENBQUUsRUFBRSxFQUFFLE1BQU0sQ0FBRSxDQUFDO1FBQUMsSUFBSSxHQUFHLEdBQUcsa0VBQVUsQ0FBQyxFQUFFLENBQUUsRUFBRSxFQUFFLE1BQU0sQ0FBRSxDQUFDO1FBQUMsSUFBSSxHQUFHLEdBQUcsa0VBQVUsQ0FBQyxFQUFFLENBQUUsRUFBRSxFQUFFLE1BQU0sQ0FBRSxDQUFDO1FBQ3BILElBQUksWUFBWSxHQUFHLElBQUksb0VBQVksQ0FBRSxHQUFHLEVBQUUsR0FBRyxFQUFFLEdBQUcsRUFBRSw4RUFBVyxDQUFFLFFBQVEsQ0FBRSxFQUFFLENBQUMsRUFBRSxNQUFNLENBQUUsQ0FBQztRQUN6RixXQUFXLENBQUMsT0FBTyxDQUFFO1lBQ2pCLFFBQVEsRUFBRSxNQUFNO1lBQ2hCLFlBQVksRUFBRSxrRUFBVSxDQUFDLFFBQVEsQ0FBRSxFQUFFLGlCQUFpQixFQUFFLENBQUMsWUFBWSxDQUFDLEVBQUUsQ0FBRTtTQUM3RSxDQUFFLENBQUM7SUFDUixDQUFDO0lBRUQ7O09BRUc7SUFDSSxlQUFlLENBQUUsU0FBeUI7UUFDN0MsRUFBRSxDQUFDLENBQUUsQ0FBQyxTQUFVLENBQUM7WUFBQyx5REFBTSxFQUFFLENBQUM7UUFDM0IsSUFBSSxPQUFPLEdBQUcsS0FBSyxDQUFDO1FBQ3BCLEdBQUcsQ0FBQyxDQUFFLElBQUksUUFBUSxJQUFJLElBQUksQ0FBQyxTQUFVLENBQUMsQ0FBQyxDQUFDO1lBQ3BDLEVBQUUsQ0FBQyxDQUFFLFFBQVEsQ0FBQyxNQUFNLENBQUUsU0FBUyxDQUFHLENBQUMsQ0FBQyxDQUFDO2dCQUNqQyxFQUFFLENBQUMsQ0FBRSxDQUFDLFFBQVEsQ0FBQyxTQUFVLENBQUM7b0JBQUMsUUFBUSxDQUFDLFNBQVMsR0FBRyw2RUFBVSxDQUFFLFFBQVEsQ0FBRSxDQUFDO2dCQUN2RSxJQUFJLENBQUMsS0FBSyxJQUFJLHlGQUFzQixDQUFFLFFBQVEsQ0FBRSxDQUFDO2dCQUNqRCxTQUFTLENBQUMsT0FBTyxJQUFJLENBQUMsQ0FBQztnQkFDdkIsT0FBTyxHQUFHLElBQUksQ0FBQztnQkFDZixLQUFLLENBQUM7WUFDVixDQUFDO1FBQ0wsQ0FBQztRQUNELEVBQUUsQ0FBQyxDQUFFLENBQUMsT0FBUSxDQUFDLENBQUMsQ0FBQztZQUNiLFNBQVMsQ0FBQyxTQUFTLEdBQUcsNkVBQVUsQ0FBRSxTQUFTLENBQUUsQ0FBQztZQUM5QyxTQUFTLENBQUMsT0FBTyxHQUFHLENBQUMsQ0FBQztZQUN0QixJQUFJLENBQUMsS0FBSyxJQUFJLDhFQUFXLENBQUUsU0FBUyxDQUFFLENBQUM7WUFDdkMsSUFBSSxDQUFDLFNBQVMsQ0FBQyxJQUFJLENBQUUsU0FBUyxDQUFFLENBQUM7UUFDckMsQ0FBQztRQUNELGFBQWEsQ0FBQyxPQUFPLENBQUU7WUFDbkIsY0FBYyxFQUFFLENBQUMsU0FBUyxDQUFDO1lBQzNCLGlCQUFpQixFQUFFLEVBQUU7U0FDeEIsQ0FBRSxDQUFDO0lBQ1IsQ0FBQztJQUVEOztPQUVHO0lBQ0ksb0JBQW9CO1FBQ3ZCLFlBQVksQ0FBQyxPQUFPLENBQUU7WUFDbEIsaUJBQWlCLEVBQUUsRUFBRTtTQUN4QixDQUFFLENBQUM7SUFDUixDQUFDO0lBRU0sT0FBTztRQUNWLFlBQVksQ0FBQyxPQUFPLENBQUU7WUFDbEIsaUJBQWlCLEVBQUUsRUFBRTtTQUN4QixDQUFFLENBQUM7SUFDUixDQUFDO0NBQ0o7QUFBQTtBQUFBIiwiZmlsZSI6ImFwcC5qcyIsInNvdXJjZXNDb250ZW50IjpbIiBcdC8vIFRoZSBtb2R1bGUgY2FjaGVcbiBcdHZhciBpbnN0YWxsZWRNb2R1bGVzID0ge307XG5cbiBcdC8vIFRoZSByZXF1aXJlIGZ1bmN0aW9uXG4gXHRmdW5jdGlvbiBfX3dlYnBhY2tfcmVxdWlyZV9fKG1vZHVsZUlkKSB7XG5cbiBcdFx0Ly8gQ2hlY2sgaWYgbW9kdWxlIGlzIGluIGNhY2hlXG4gXHRcdGlmKGluc3RhbGxlZE1vZHVsZXNbbW9kdWxlSWRdKSB7XG4gXHRcdFx0cmV0dXJuIGluc3RhbGxlZE1vZHVsZXNbbW9kdWxlSWRdLmV4cG9ydHM7XG4gXHRcdH1cbiBcdFx0Ly8gQ3JlYXRlIGEgbmV3IG1vZHVsZSAoYW5kIHB1dCBpdCBpbnRvIHRoZSBjYWNoZSlcbiBcdFx0dmFyIG1vZHVsZSA9IGluc3RhbGxlZE1vZHVsZXNbbW9kdWxlSWRdID0ge1xuIFx0XHRcdGk6IG1vZHVsZUlkLFxuIFx0XHRcdGw6IGZhbHNlLFxuIFx0XHRcdGV4cG9ydHM6IHt9XG4gXHRcdH07XG5cbiBcdFx0Ly8gRXhlY3V0ZSB0aGUgbW9kdWxlIGZ1bmN0aW9uXG4gXHRcdG1vZHVsZXNbbW9kdWxlSWRdLmNhbGwobW9kdWxlLmV4cG9ydHMsIG1vZHVsZSwgbW9kdWxlLmV4cG9ydHMsIF9fd2VicGFja19yZXF1aXJlX18pO1xuXG4gXHRcdC8vIEZsYWcgdGhlIG1vZHVsZSBhcyBsb2FkZWRcbiBcdFx0bW9kdWxlLmwgPSB0cnVlO1xuXG4gXHRcdC8vIFJldHVybiB0aGUgZXhwb3J0cyBvZiB0aGUgbW9kdWxlXG4gXHRcdHJldHVybiBtb2R1bGUuZXhwb3J0cztcbiBcdH1cblxuXG4gXHQvLyBleHBvc2UgdGhlIG1vZHVsZXMgb2JqZWN0IChfX3dlYnBhY2tfbW9kdWxlc19fKVxuIFx0X193ZWJwYWNrX3JlcXVpcmVfXy5tID0gbW9kdWxlcztcblxuIFx0Ly8gZXhwb3NlIHRoZSBtb2R1bGUgY2FjaGVcbiBcdF9fd2VicGFja19yZXF1aXJlX18uYyA9IGluc3RhbGxlZE1vZHVsZXM7XG5cbiBcdC8vIGRlZmluZSBnZXR0ZXIgZnVuY3Rpb24gZm9yIGhhcm1vbnkgZXhwb3J0c1xuIFx0X193ZWJwYWNrX3JlcXVpcmVfXy5kID0gZnVuY3Rpb24oZXhwb3J0cywgbmFtZSwgZ2V0dGVyKSB7XG4gXHRcdGlmKCFfX3dlYnBhY2tfcmVxdWlyZV9fLm8oZXhwb3J0cywgbmFtZSkpIHtcbiBcdFx0XHRPYmplY3QuZGVmaW5lUHJvcGVydHkoZXhwb3J0cywgbmFtZSwge1xuIFx0XHRcdFx0Y29uZmlndXJhYmxlOiBmYWxzZSxcbiBcdFx0XHRcdGVudW1lcmFibGU6IHRydWUsXG4gXHRcdFx0XHRnZXQ6IGdldHRlclxuIFx0XHRcdH0pO1xuIFx0XHR9XG4gXHR9O1xuXG4gXHQvLyBnZXREZWZhdWx0RXhwb3J0IGZ1bmN0aW9uIGZvciBjb21wYXRpYmlsaXR5IHdpdGggbm9uLWhhcm1vbnkgbW9kdWxlc1xuIFx0X193ZWJwYWNrX3JlcXVpcmVfXy5uID0gZnVuY3Rpb24obW9kdWxlKSB7XG4gXHRcdHZhciBnZXR0ZXIgPSBtb2R1bGUgJiYgbW9kdWxlLl9fZXNNb2R1bGUgP1xuIFx0XHRcdGZ1bmN0aW9uIGdldERlZmF1bHQoKSB7IHJldHVybiBtb2R1bGVbJ2RlZmF1bHQnXTsgfSA6XG4gXHRcdFx0ZnVuY3Rpb24gZ2V0TW9kdWxlRXhwb3J0cygpIHsgcmV0dXJuIG1vZHVsZTsgfTtcbiBcdFx0X193ZWJwYWNrX3JlcXVpcmVfXy5kKGdldHRlciwgJ2EnLCBnZXR0ZXIpO1xuIFx0XHRyZXR1cm4gZ2V0dGVyO1xuIFx0fTtcblxuIFx0Ly8gT2JqZWN0LnByb3RvdHlwZS5oYXNPd25Qcm9wZXJ0eS5jYWxsXG4gXHRfX3dlYnBhY2tfcmVxdWlyZV9fLm8gPSBmdW5jdGlvbihvYmplY3QsIHByb3BlcnR5KSB7IHJldHVybiBPYmplY3QucHJvdG90eXBlLmhhc093blByb3BlcnR5LmNhbGwob2JqZWN0LCBwcm9wZXJ0eSk7IH07XG5cbiBcdC8vIF9fd2VicGFja19wdWJsaWNfcGF0aF9fXG4gXHRfX3dlYnBhY2tfcmVxdWlyZV9fLnAgPSBcIlwiO1xuXG4gXHQvLyBMb2FkIGVudHJ5IG1vZHVsZSBhbmQgcmV0dXJuIGV4cG9ydHNcbiBcdHJldHVybiBfX3dlYnBhY2tfcmVxdWlyZV9fKF9fd2VicGFja19yZXF1aXJlX18ucyA9IDApO1xuXG5cblxuLy8gV0VCUEFDSyBGT09URVIgLy9cbi8vIHdlYnBhY2svYm9vdHN0cmFwIDkyOTQ3ODBhYjg5OWEyMWFjYjcyIiwiLypcclxuICogV29ya3MgYXMgYSBjb250cm9sbGVyLiBNYW5hZ2VzIHRoZSBtYXRjaCwgZXNwZWNpYWxseSBpdHMgcHJvcGVyIGluaXRhbGl6YXRpb24uIFdvcmtzIHdpdGggdGhlIGh0bWwgZmlsZS5cclxuICovXG5cclxuXHJcbi8qKk9ubHkgZm9yIGRlYnVnZ2luZyBpc3N1ZXMsIGNhbiBiZSBzaGFyZWQgdG8gb3RoZXIgbW9kdWxlcyB0byBkcmF3IGluZm9ybWF0aW9uIHJlbGV2YW50IGZvciBkZWJ1Z2dpbmcqL1xyXG5pbXBvcnQgeyBDYW52YXNIYW5kbGVyIH0gZnJvbSBcIi4uL2JvYXJkX2hhbmRsaW5nL2NhbnZhc19oYW5kbGVyXCI7XHJcbmltcG9ydCB7IE5ldHdvcmtDb25uZWN0b3IsIE5ldHdvcmsgfSBmcm9tIFwiLi4vbmV0d29yay9uZXR3b3JrXCI7XHJcbmltcG9ydCB7IFRyaWFuZ2xlQmVhbiB9IGZyb20gXCIuLi9uZXR3b3JrL25ldHdvcmtfYmVhbnNcIjtcclxuaW1wb3J0IHsgSW5nYW1lVHJpYW5nbGUgfSBmcm9tIFwiLi4vYm9hcmRfaGFuZGxpbmcvZ2FtZV9vYmplY3RzXCI7XHJcbmltcG9ydCB7IFZlcnRleCwgVHJpYW5nbGUsIHZlcnRpY2VzVG9FZGdlcyB9IGZyb20gXCIuLi9sb2dpYy9nZW9tZXRyaWNfb2JqZWN0c1wiO1xyXG5pbXBvcnQgeyBEdW1teUNvbm5lY3RvciB9IGZyb20gXCIuLi9uZXR3b3JrL29mZmxpbmVcIjtcclxuXHJcbi8qKjA6IG5vIGRlYnVnZ2luZywgMTogbm9ybWFsIGRlYnVnIG1vZGUsIDI6IGRvbid0IGRyYXcgcHJldmlldyB0cmlhbmdsZSovXHJcbmV4cG9ydCBsZXQgZGVidWc6IG51bWJlciA9IDE7IC8vX19XRUJQQUNLX0lNUE9SVEVEX01PRFVMRV8yX19ydW5fbWFpbl9fW1wiZGVidWdcIl1cclxuLyoqUmVwcmVzZW50cyB0aGUgKHNxdWFyZWQpIHByb3hpbWl0eSB3aGVyZSB7QGNvZGUgVmVydGljZXN9IHdpbGwgYmUgbWF0Y2hlZCovXHJcbmV4cG9ydCBjb25zdCBlcHNpbG9uID0gMTAwO1xyXG4vKipEZXRlcm1pbmVzIHRoZSBtaW4gc2l6ZSBvZiBhIHtAY29kZSBUcmlhbmdsZX0qL1xyXG5leHBvcnQgY29uc3QgbWluUmFuZ2VNdWx0aXBsaWVyID0gMTA7XHJcbi8qKlRoZSBjYW52YXMgZXZlcnl0aGluZyB3aWxsIGJlIGRyYXduIGluKi9cclxuZXhwb3J0IGxldCBjYW52YXNDb250ZXh0OiBDYW52YXNSZW5kZXJpbmdDb250ZXh0MkQ7XHJcbi8qKkNvbm5lY3Rpb24gdG8gdGhlIHNlcnZlciovXHJcbmxldCBjb25uZWN0b3I7XHJcbi8qKkhvbGRzIHRoZSBnYW1lIGJvYXJkIGFuZCBtYW5hZ2VzIHRoZSBwbGF5ZXIncyBpbnB1dCovXHJcbmxldCBjYW52YXNIYW5kbGVyOiBDYW52YXNIYW5kbGVyO1xyXG4vKipUaGUgcGxheWVyJ3MgaWQqL1xyXG5sZXQgaWQ6IHN0cmluZztcclxuLyoqRGlzcGxheXMgdGhlIHBsYXllcidzIG5hbWUqL1xyXG5sZXQgcGxheWVyOiBIVE1MRGl2RWxlbWVudDtcclxuLyoqRGlzcGxheXMgdGhlIHBsYXllcidzIG1vbmV5Ki9cclxubGV0IG1vbmV5OiBIVE1MRGl2RWxlbWVudDtcclxuLyoqSW5pdGlhbGl6ZXMgdGhlIGdhbWUgYXMgc29vbiBhcyB0aGUgd2Vic2l0ZSBpcyBmdWxseSBsb2FkZWQuKi9cclxuZG9jdW1lbnQuYWRkRXZlbnRMaXN0ZW5lciggJ0RPTUNvbnRlbnRMb2FkZWQnLCBpbml0LCBmYWxzZSApO1xyXG5cclxuLyoqXHJcbiAqIEluaXRpYWxpemVzIHRoZSBnYW1lLlxyXG4gKi9cclxuZnVuY3Rpb24gaW5pdCgpIHtcclxuICAgIC8vICAgIGxpdmVJbmZvID0gY3JlYXRlTGl2ZUluZm9GaWVsZCgpO1xyXG4gICAgcGxheWVyID0gZG9jdW1lbnQuZ2V0RWxlbWVudEJ5SWQoIFwicGxheWVyX25hbWVcIiApIGFzIEhUTUxEaXZFbGVtZW50O1xyXG4gICAgbW9uZXkgPSBkb2N1bWVudC5nZXRFbGVtZW50QnlJZCggXCJtb25leVwiICkgYXMgSFRNTERpdkVsZW1lbnQ7XHJcbiAgICB0cnkge1xyXG4gICAgICAgIGNvbm5lY3RvciA9IG5ldyBOZXR3b3JrQ29ubmVjdG9yKCk7XHJcbiAgICB9IGNhdGNoICggZSApIHsgLy8gVW5jYXVnaHQgU3ludGF4RXJyb3I6IFRoZSBVUkwgJy90cmlnb3R5LXdlYnNvY2tldCcgaXMgaW52YWxpZFxyXG4gICAgICAgIGNvbm5lY3RvciA9IG5ldyBEdW1teUNvbm5lY3RvcigpO1xyXG4gICAgfVxyXG4gICAgY29ubmVjdG9yLmluaXQoIHN1Y2Nlc3MgPT4geyBpZiAoIHN1Y2Nlc3MgKSBjcmVhdGVCb2FyZCgpOyB9ICk7XHJcbn1cclxuXHJcbi8qKkluaXRpYWxpemVzIHRoZSBib2FyZCovXHJcbmZ1bmN0aW9uIGNyZWF0ZUJvYXJkKCkge1xyXG4gICAgLy9UT0RPIHJlY2VpdmUgY2FudmFzIHNpemVcclxuICAgIGNhbnZhc0hhbmRsZXIgPSBuZXcgQ2FudmFzSGFuZGxlciggY29ubmVjdG9yICk7XHJcbiAgICBsZXQgYm9hcmQgPSBkb2N1bWVudC5nZXRFbGVtZW50QnlJZCggXCJib2FyZFwiICkgYXMgSFRNTENhbnZhc0VsZW1lbnQ7XHJcbiAgICBjYW52YXNIYW5kbGVyLmluaXQoIGJvYXJkICk7XHJcbiAgICBpZiAoIGRlYnVnICkge1xyXG4gICAgICAgIGVuYWJsZVN0b3JlQnV0dG9uKCk7XHJcbiAgICAgICAgZW5hYmxlUmVzZXRCdXR0b24oKTtcclxuICAgIH1cclxufVxyXG5cclxuLyoqXHJcbiAqIEVuYWJsZXMgYnV0dG9uIHRvIHN0b3JlIHRoZSBzdGF0ZSBpbiBhIGpzb24gZmlsZS5cclxuICovXHJcbmZ1bmN0aW9uIGVuYWJsZVN0b3JlQnV0dG9uKCkge1xyXG4gICAgbGV0IHN0b3JlQnV0dG9uID0gZG9jdW1lbnQuZ2V0RWxlbWVudEJ5SWQoIFwic3RvcmVcIiApO1xyXG4gICAgc3RvcmVCdXR0b24uYWRkRXZlbnRMaXN0ZW5lciggXCJjbGlja1wiLCAoIGUgKSA9PiB7XHJcbiAgICAgICAgbGV0IHRyaWFuZ2xlQmVhbnM6IFRyaWFuZ2xlQmVhbltdID0gbmV3IEFycmF5KCk7XHJcbiAgICAgICAgZm9yICggbGV0IHRyaWFuZ2xlIG9mIGNhbnZhc0hhbmRsZXIuc3RhdGUucGxheWVyVHJpYW5nbGVzLmNvbmNhdCggY2FudmFzSGFuZGxlci5zdGF0ZS5vdGhlclRyaWFuZ2xlcyApICkge1xyXG4gICAgICAgICAgICB0cmlhbmdsZUJlYW5zLnB1c2goIFRyaWFuZ2xlQmVhbi5vZiggdHJpYW5nbGUgKSApO1xyXG4gICAgICAgIH1cclxuICAgICAgICBsZXQgbm93OiBEYXRlID0gbmV3IERhdGUoKTtcclxuICAgICAgICBzdG9yZUJ1dHRvbi5zZXRBdHRyaWJ1dGUoIFwiZG93bmxvYWRcIiwgXCJ0cmlnb3R5X3NuYXBzaG90X1wiICsgbm93LmdldEZ1bGxZZWFyKCkgKyBub3cuZ2V0TW9udGgoKSArIG5vdy5nZXREYXRlKCkgKyBub3cuZ2V0SG91cnMoKSArIG5vdy5nZXRNaW51dGVzKCkgKyBub3cuZ2V0U2Vjb25kcygpICsgXCIuanNvblwiICk7XHJcbiAgICAgICAgc3RvcmVCdXR0b24uc2V0QXR0cmlidXRlKCBcImhyZWZcIiwgJ2RhdGE6YXBwbGljYXRpb24veG1sO2NoYXJzZXQ9dXRmLTgse1widHJpYW5nbGVzXCI6JyArIEpTT04uc3RyaW5naWZ5KCB0cmlhbmdsZUJlYW5zICkgKyAnfScgKTtcclxuICAgIH0gKTtcclxuICAgIHN0b3JlQnV0dG9uLnN0eWxlLmRpc3BsYXkgPSBcImJsb2NrXCI7XHJcbn1cclxuXHJcbi8qKlxyXG4gKiBFbmFibGVzIGJ1dHRvbiB0byByZXNldCB0aGUgZ2FtZSdzIHN0YXRlLiBTaW5jZSB0aGlzIGJhY2stZW5kIGFsbG93cyBvbmx5IGxpbWl0ZWQgcGxheWVycyBhdG0sXHJcbiAqIHRoaXMgYnV0dG9uIGFsbG93cyB0byByZXN0YXJ0IHNvIG5ldyBwbGF5ZXJzIGNhbiBiZSByZWdpc3RlcmVkLiBcclxuICovXHJcbmZ1bmN0aW9uIGVuYWJsZVJlc2V0QnV0dG9uKCkge1xyXG4gICAgbGV0IHJlc2V0QnV0dG9uID0gZG9jdW1lbnQuZ2V0RWxlbWVudEJ5SWQoIFwibmV3X2dhbWVcIiApO1xyXG4gICAgcmVzZXRCdXR0b24uYWRkRXZlbnRMaXN0ZW5lciggXCJjbGlja1wiLCAoIGUgKSA9PiBjb25uZWN0b3IubmV3R2FtZSgpICk7XHJcbiAgICByZXNldEJ1dHRvbi5zdHlsZS5kaXNwbGF5ID0gXCJibG9ja1wiO1xyXG59XHJcblxyXG4vKipcclxuICogUmVkaXJlY3RzIHVwZGF0ZXMgZnJvbSB0aGUgc2VydmVyIHRvIHRoZSBib2FyZC5cclxuICogQHBhcmFtIHRvQWRkIHtAY29kZSBUcmlhbmdsZXN9IHRvIGJlIGFkZGVkXHJcbiAqIEBwYXJhbSB0b1JlbW92ZSB7QGNvZGUgVHJpYW5nbGVzfSB0byBiZSByZW1vdmVkXHJcbiAqL1xyXG5leHBvcnQgZnVuY3Rpb24gYXBwbHlTZXJ2ZXJVcGRhdGUoIHRvUmVtb3ZlOiBJbmdhbWVUcmlhbmdsZVtdLCB0b0FkZDogSW5nYW1lVHJpYW5nbGVbXSApIHtcclxuICAgIGlmICggbW9uZXkgKSB0b0FkZC5mb3JFYWNoKCB0ID0+IG1vbmV5LmlubmVyVGV4dCA9IFN0cmluZyggK21vbmV5LmlubmVyVGV4dCAtIHQuYmFzZVZhbHVlICkgKTtcclxuICAgIGNhbnZhc0hhbmRsZXIuYXBwbHlTZXJ2ZXJVcGRhdGUoIHRvUmVtb3ZlLCB0b0FkZCApO1xyXG59XHJcblxyXG4vKipcclxuICogUmVkaXJlY3RzIGEgcmVzZXQgdG8gdGhlIHNlcnZlcidzIHN0YXRlIHRvIHRoZSBib2FyZC5cclxuICogQHBhcmFtIHRvQ3JlYXRlIHtAY29kZSBUcmlhbmdsZXN9IGZyb20gdGhlIHNlcnZlcidzIHN0YXRlXHJcbiAqL1xyXG5leHBvcnQgZnVuY3Rpb24gYXBwbHlTZXJ2ZXJSZXNldCggdG9DcmVhdGU6IEluZ2FtZVRyaWFuZ2xlW10gKSB7XHJcbiAgICBjYW52YXNIYW5kbGVyLmFwcGx5U2VydmVyUmVzZXQoIHRvQ3JlYXRlICk7XHJcbn1cclxuXHJcbi8qKlxyXG4gKiBSZWRpcmVjdHMgc3VjY2Vzc2Z1bCByZWdpc3RyYXRpb24gYXQgdGhlIHNlcnZlciB0byB0aGUgYm9hcmQuXHJcbiAqIEBwYXJhbSBwbGF5ZXJJZCBvZiB0aGUgcGxheWVyLCB1c2VkIHRvIGlkZW50aWZ5IGhpbSBvbiB0aGUgc2VydmVyXHJcbiAqIEBwYXJhbSB0b0NyZWF0ZSB7QGNvZGUgVHJpYW5nbGVzfSBmcm9tIHRoZSBzZXJ2ZXIncyBzdGF0ZVxyXG4gKi9cclxuZXhwb3J0IGZ1bmN0aW9uIGFwcGx5U2VydmVyUmVnaXN0ZXIoIHBsYXllcklkOiBzdHJpbmcsIHRvQ3JlYXRlOiBJbmdhbWVUcmlhbmdsZVtdICkge1xyXG4gICAgaWYgKCBwbGF5ZXIgKSBwbGF5ZXIuaW5uZXJUZXh0ID0gcGxheWVySWQ7XHJcbiAgICBjYW52YXNIYW5kbGVyLnJlZ2lzdGVyKCBwbGF5ZXJJZCwgdG9DcmVhdGUgKTtcclxufVxyXG5cclxuLyoqXHJcbiAqIFJlc3VtZXMgdGhlIGJvYXJkIHdoaWNoIGlzIHdlaWdodGluZyBmb3IgYSBzZXJ2ZXIgcmVzcG9uc2UuXHJcbiAqL1xyXG5leHBvcnQgZnVuY3Rpb24gcmVzdW1lKCkge1xyXG4gICAgY2FudmFzSGFuZGxlci5yZXN1bWUoKTtcclxufVxyXG5cclxuLyoqXHJcbiAqIFJlc3VtZXMgdGhlIGJvYXJkIHdoaWNoIGlzIHdlaWdodGluZyBmb3IgYSBzZXJ2ZXIgcmVzcG9uc2UuXHJcbiAqL1xyXG5leHBvcnQgZnVuY3Rpb24gc2V0QWNjb3VudCggYmFsYW5jZTogbnVtYmVyICkge1xyXG4gICAgaWYgKCBtb25leSApIG1vbmV5LmlubmVyVGV4dCA9IFN0cmluZyggYmFsYW5jZSApO1xyXG4gICAgY2FudmFzSGFuZGxlci5zZXRBY2NvdW50KCBiYWxhbmNlICk7XHJcbn1cclxuXHJcblxuXG5cbi8vIFdFQlBBQ0sgRk9PVEVSIC8vXG4vLyAuL3R5cGVzY3JpcHQvcnVuL21haW4udHMiLCIvKipcclxuICogSG9sZHMgdGhlIGdlb21ldHJpY2FsIG9iamVjdCBwcm90b3R5cGVzLlxyXG4gKi9cclxuXHJcblxyXG5pbXBvcnQgeyBtaW4sIG1heCwgU3RhbmRhcmRPYmplY3QsIHJlbW92ZUZyb21MaXN0IH0gZnJvbSBcIi4uL3V0aWwvc2hhcmVkXCI7XHJcbmltcG9ydCB7IGRlYnVnLCBjYW52YXNDb250ZXh0IH0gZnJvbSBcIi4uL3J1bi9tYWluXCI7IC8vQm90aCBpbmZvcm1hdGlvbiBzaGFsbCBvbmx5IGJlIHVzZWQgZm9yIGRlYnVnZ2luZ1xyXG5pbXBvcnQgeyBJbmdhbWVUcmlhbmdsZSB9IGZyb20gXCIuLi9ib2FyZF9oYW5kbGluZy9nYW1lX29iamVjdHNcIjtcclxuXHJcbmV4cG9ydCBpbnRlcmZhY2UgR2VvbWV0cmljT2JqZWN0IGV4dGVuZHMgU3RhbmRhcmRPYmplY3QgeyB9XHJcblxyXG4vKipcclxuICogUmVwcmVzZW50cyB0aGUgb3JpZW50YXRpb24gb2YgdGhyZWUgdmVydGljZXMgdGhhdCBjb25zdGl0dXRlIGFcclxuICoge0Bjb2RlIFRyaWFuZ2xlfSBpbiAyRCwgYmFzZWQgb24gYSBwb2ludCBvZiBvcmlnaW4gaW4gdGhlIGJvdHRvbSBsZWZ0XHJcbiAqIFxyXG4gKiBAYXV0aG9yIEpcclxuICpcclxuICovXHJcbmVudW0gT3JpZW50YXRpb24ge1xyXG4gICAgQ0xPQ0tXSVNFLCBDT1VOVEVSX0NMT0NLV0lTRSwgQ09MSU5FQVIsXHJcbn1cclxuXHJcbi8qKkFsbCBnZW9tZXRyeSBpcyBhZGp1c3RlZCBvbiB0aGlzIG9yaWVudGF0aW9uKi9cclxubGV0IHRhcmdldE9yaWVudGF0aW9uOiBPcmllbnRhdGlvbiA9IE9yaWVudGF0aW9uLkNMT0NLV0lTRTtcclxuXHJcbmV4cG9ydCBjbGFzcyBWZXJ0ZXggaW1wbGVtZW50cyBHZW9tZXRyaWNPYmplY3Qge1xyXG4gICAgcHJpdmF0ZSBfeDogbnVtYmVyO1xyXG4gICAgcHJpdmF0ZSBfeTogbnVtYmVyO1xyXG5cclxuICAgIGdldCB4KCkgeyByZXR1cm4gdGhpcy5feDsgfVxyXG4gICAgZ2V0IHkoKSB7IHJldHVybiB0aGlzLl95OyB9XHJcblxyXG4gICAgY29uc3RydWN0b3IoIHg6IG51bWJlciwgeTogbnVtYmVyICkge1xyXG4gICAgICAgIHRoaXMuX3ggPSB4O1xyXG4gICAgICAgIHRoaXMuX3kgPSB5O1xyXG4gICAgfVxyXG5cclxuICAgIHN1YnRyYWN0VmVydGV4KCB2ZXJ0ZXg6IFZlcnRleCApOiBWZXJ0ZXgge1xyXG4gICAgICAgIHJldHVybiBuZXcgVmVydGV4KCB0aGlzLnggLSB2ZXJ0ZXgueCwgdGhpcy55IC0gdmVydGV4LnkgKTtcclxuICAgIH07XHJcblxyXG4gICAgc2thbGFyUHJvZHVjdCggdmVydGV4OiBWZXJ0ZXggKTogbnVtYmVyIHtcclxuICAgICAgICByZXR1cm4gKCB0aGlzLnggKiB2ZXJ0ZXgueCApICsgKCB0aGlzLnkgKiB2ZXJ0ZXgueSApO1xyXG4gICAgfTtcclxuXHJcbiAgICBpbnZlcnNlVmVydGV4KCk6IFZlcnRleCB7XHJcbiAgICAgICAgcmV0dXJuIG5ldyBWZXJ0ZXgoIC10aGlzLngsIC10aGlzLnkgKTtcclxuICAgIH07XHJcblxyXG4gICAgc3FyRGlzdGFuY2VUb1BvaW50KCB2ZXJ0ZXg6IFZlcnRleCApOiBudW1iZXIge1xyXG4gICAgICAgIHJldHVybiBNYXRoLnBvdyggdGhpcy54IC0gdmVydGV4LngsIDIgKSArIE1hdGgucG93KCB0aGlzLnkgLSB2ZXJ0ZXgueSwgMiApO1xyXG4gICAgfTtcclxuXHJcbiAgICB0b1N0cmluZyA9ICgpOiBzdHJpbmcgPT4ge1xyXG4gICAgICAgIHJldHVybiB0aGlzLnggKyBcIixcIiArIHRoaXMueTtcclxuICAgIH1cclxuXHJcbiAgICBlcXVhbHMoIHZlcnRleDogb2JqZWN0ICk6IGJvb2xlYW4ge1xyXG4gICAgICAgIGlmICggIXZlcnRleCApIHJldHVybiBmYWxzZTtcclxuICAgICAgICBpZiAoICEoIHZlcnRleCBpbnN0YW5jZW9mIFZlcnRleCApICkgcmV0dXJuIGZhbHNlO1xyXG4gICAgICAgIGVsc2UgcmV0dXJuIHRoaXMueCA9PSB2ZXJ0ZXgueCAmJiB0aGlzLnkgPT0gdmVydGV4Lnk7XHJcbiAgICB9XHJcblxyXG4gICAgY29tcGFyZVRvKCB0b0NvbXBhcmVXaXRoOiBWZXJ0ZXggKTogbnVtYmVyIHtcclxuICAgICAgICBsZXQgeENvbXAgPSB0aGlzLnggLSB0b0NvbXBhcmVXaXRoLng7XHJcbiAgICAgICAgaWYgKCB4Q29tcCAhPSAwICkgcmV0dXJuIHhDb21wO1xyXG4gICAgICAgIGVsc2UgcmV0dXJuIHRoaXMueSAtIHRvQ29tcGFyZVdpdGgueTtcclxuICAgIH1cclxufVxyXG5cclxuZXhwb3J0IGNsYXNzIEVkZ2UgaW1wbGVtZW50cyBHZW9tZXRyaWNPYmplY3Qge1xyXG4gICAgcHJpdmF0ZSBfc3RhcnRWZXJ0ZXg6IFZlcnRleDtcclxuICAgIHByaXZhdGUgX3ByZXZFZGdlOiBFZGdlO1xyXG4gICAgcHJpdmF0ZSBfbmV4dEVkZ2U6IEVkZ2U7XHJcbiAgICBwcml2YXRlIF90d2luRWRnZTogRWRnZTtcclxuICAgIHByaXZhdGUgX3JlbGF0ZWRUcmlhbmdsZTogVHJpYW5nbGU7XHJcblxyXG4gICAgZ2V0IHN0YXJ0VmVydGV4KCk6IFZlcnRleCB7IHJldHVybiB0aGlzLl9zdGFydFZlcnRleDsgfVxyXG4gICAgc2V0IHN0YXJ0VmVydGV4KCBzdGFydFZlcnRleDogVmVydGV4ICkgeyB0aGlzLl9zdGFydFZlcnRleCA9IHN0YXJ0VmVydGV4OyB9XHJcbiAgICBnZXQgcHJldkVkZ2UoKTogRWRnZSB7IHJldHVybiB0aGlzLl9wcmV2RWRnZTsgfVxyXG4gICAgc2V0IHByZXZFZGdlKCBwcmV2RWRnZTogRWRnZSApIHsgdGhpcy5fcHJldkVkZ2UgPSBwcmV2RWRnZTsgfVxyXG4gICAgZ2V0IG5leHRFZGdlKCk6IEVkZ2UgeyByZXR1cm4gdGhpcy5fbmV4dEVkZ2U7IH1cclxuICAgIHNldCBuZXh0RWRnZSggbmV4dEVkZ2U6IEVkZ2UgKSB7IHRoaXMuX25leHRFZGdlID0gbmV4dEVkZ2U7IH1cclxuICAgIC8vU0lOQ0UgcHVibGljIGdldCBhbmQgcHJpdmF0ZSBzZXQgYXJlIG5vdCBhbGxvd2VkIHlldCwgYnV0IHNldCBpcyBjb21wbGV4IGFuZCBtYXkgZmFpbCAoc2VlIGxpbmtBc1R3aW5zKSwgd2Ugc2tpcCB0aGUgd2hvbGUgYXBwcm9hY2ggaGVyZVxyXG4gICAgLy8gICAgZ2V0IHR3aW5FZGdlKCk6IEVkZ2UgeyByZXR1cm4gdGhpcy5fdHdpbkVkZ2U7IH1cclxuICAgIC8vICAgIHByaXZhdGUgc2V0IHR3aW5FZGdlKCB0d2luRWRnZTogRWRnZSApIHtcclxuICAgIC8vICAgICAgICBpZiAoICF0d2luRWRnZSApIHJldHVybjtcclxuICAgIC8vICAgICAgICB0aGlzLl90d2luRWRnZSA9IHR3aW5FZGdlO1xyXG4gICAgLy8gICAgICAgIHR3aW5FZGdlLl90d2luRWRnZSA9IHRoaXM7XHJcbiAgICAvLyAgICB9XHJcbiAgICBnZXQgcmVsYXRlZFRyaWFuZ2xlKCk6IFRyaWFuZ2xlIHsgcmV0dXJuIHRoaXMuX3JlbGF0ZWRUcmlhbmdsZTsgfVxyXG4gICAgc2V0IHJlbGF0ZWRUcmlhbmdsZSggcmVsYXRlZFRyaWFuZ2xlOiBUcmlhbmdsZSApIHtcclxuICAgICAgICB0aGlzLl9yZWxhdGVkVHJpYW5nbGUgPSByZWxhdGVkVHJpYW5nbGU7XHJcbiAgICAgICAgaWYgKCAhdGhpcy5fcHJldkVkZ2UucmVsYXRlZFRyaWFuZ2xlICkgdGhpcy5fcHJldkVkZ2UucmVsYXRlZFRyaWFuZ2xlID0gcmVsYXRlZFRyaWFuZ2xlO1xyXG4gICAgICAgIGlmICggIXRoaXMuX25leHRFZGdlLnJlbGF0ZWRUcmlhbmdsZSApIHRoaXMuX25leHRFZGdlLnJlbGF0ZWRUcmlhbmdsZSA9IHJlbGF0ZWRUcmlhbmdsZTtcclxuICAgIH1cclxuXHJcbiAgICBjb25zdHJ1Y3Rvciggc3RhcnRWZXJ0ZXg6IFZlcnRleCwgcHJldkVkZ2U6IEVkZ2UsIG5leHRFZGdlOiBFZGdlLCB0d2luRWRnZT86IEVkZ2UgKSB7XHJcbiAgICAgICAgdGhpcy5fc3RhcnRWZXJ0ZXggPSBzdGFydFZlcnRleDtcclxuICAgICAgICB0aGlzLl9wcmV2RWRnZSA9IHByZXZFZGdlXHJcbiAgICAgICAgdGhpcy5fbmV4dEVkZ2UgPSBuZXh0RWRnZTtcclxuICAgICAgICB0aGlzLl90d2luRWRnZSA9IHR3aW5FZGdlO1xyXG4gICAgICAgIHRoaXMuX3JlbGF0ZWRUcmlhbmdsZVxyXG4gICAgfVxyXG5cclxuXHJcbiAgICAvKipcclxuICAgICAqIFJlZ2lzdGVycyBhIGdpdmVuIHtAY29kZSBFZGdlfSBhcyB0d2luIGZvciB0aGlzIHtAY29kZSBFZGdlfSwgaWYgcG9zc2libGUuIEZhaWxzIGlmIHRoZXJlIGlzIGFscmVhZHkgYSB0d2luIHtAY29kZSBFZGdlfSBvciBpdCdzIG5vIHByb3BlciB0d2luLlxyXG4gICAgICogQHBhcmFtIHRvUmVnaXN0ZXIgdGhlIHByb3Bvc2VkIHR3aW4ge0Bjb2RlIEVkZ2V9XHJcbiAgICAgKiBAcmV0dXJuIHtAY29kZSB0cnVlfSBpZiB0aGUgdHdpbiB3YXMgc3VjY2Vzc2Z1bGx5IHJlZ2lzdGVyZWQsIG90aGVyd2lzZSB7QGNvZGUgZmFsc2V9XHJcbiAgICAgKi9cclxuICAgIGxpbmtBc1R3aW5zKCB0b0xpbms6IEVkZ2UgKTogYm9vbGVhbiB7XHJcbiAgICAgICAgaWYgKCAhdG9MaW5rIHx8IHRvTGluay5fdHdpbkVkZ2UgKSByZXR1cm4gZmFsc2U7XHJcbiAgICAgICAgaWYgKCAhdGhpcy5fdHdpbkVkZ2UgJiYgdGhpcy5pc1Byb3BlclR3aW5Gb3IoIHRvTGluayApICkge1xyXG4gICAgICAgICAgICB0aGlzLl90d2luRWRnZSA9IHRvTGluaztcclxuICAgICAgICAgICAgdG9MaW5rLl90d2luRWRnZSA9IHRoaXM7XHJcbiAgICAgICAgICAgIHJldHVybiB0cnVlO1xyXG4gICAgICAgIH0gZWxzZSByZXR1cm4gZmFsc2U7XHJcbiAgICB9XHJcblxyXG4gICAgZ2V0VHdpbkVkZ2UoKTogRWRnZSB7XHJcbiAgICAgICAgcmV0dXJuIHRoaXMuX3R3aW5FZGdlO1xyXG4gICAgfVxyXG5cclxuICAgIC8qKlxyXG4gICAgICogQ3JlYXRlcyBhIHR3aW4gZm9yIGEgZ2l2ZW4ge0Bjb2RlIEVkZ2V9LiBUaGUgZ2l2ZW4ge0Bjb2RlIEVkZ2V9IHdvbid0IGJlIGxpbmtlZCB0byB0aGUgY3JlYXRlZCB0d2luIG1vZGVsLlxyXG4gICAgICogRm9yIHRoYXQsIHVzZSB7QGxpbmsgRWRnZSNsaW5rQXNUd2lucyhFZGdlKX0uXHJcbiAgICAgKiBAcGFyYW0gdG9Db252ZXJ0IGEgdHdpbiB3aWxsIGJlIGNyZWF0ZWQgZm9yXHJcbiAgICAgKiBAcmV0dXJuIGFuIHtAY29kZSBFZGdlfSB3b3JraW5nIGFzIHByb3BlciB0d2luIFxyXG4gICAgICovXHJcbiAgICBzdGF0aWMgY3JlYXRlVHdpbk1vZGVsKCB0b0NvbnZlcnQ6IEVkZ2UgKTogRWRnZSB7XHJcbiAgICAgICAgbGV0IHR3aW5OZXh0RWRnZTogRWRnZSA9IHRvQ29udmVydC5wcmV2RWRnZS5fdHdpbkVkZ2U7XHJcbiAgICAgICAgaWYgKCB0d2luTmV4dEVkZ2UgPT0gbnVsbCApXHJcbiAgICAgICAgICAgIHR3aW5OZXh0RWRnZSA9IG5ldyBFZGdlKCB0b0NvbnZlcnQuc3RhcnRWZXJ0ZXgsIG51bGwsIG51bGwsIG51bGwgKTtcclxuICAgICAgICByZXR1cm4gbmV3IEVkZ2UoIHRvQ29udmVydC5uZXh0RWRnZS5zdGFydFZlcnRleCwgdG9Db252ZXJ0Lm5leHRFZGdlLl90d2luRWRnZSwgdHdpbk5leHRFZGdlLCB0b0NvbnZlcnQgKTtcclxuICAgIH1cclxuXHJcbiAgICBpc1Byb3BlclR3aW5Gb3IoIGVkZ2U6IEVkZ2UgKTogYm9vbGVhbiB7XHJcbiAgICAgICAgaWYgKCAhZWRnZSApIHJldHVybiBmYWxzZTtcclxuICAgICAgICBlbHNlIHJldHVybiB0aGlzLnN0YXJ0VmVydGV4LmVxdWFscyggZWRnZS5uZXh0RWRnZS5zdGFydFZlcnRleCApICYmIHRoaXMubmV4dEVkZ2Uuc3RhcnRWZXJ0ZXguZXF1YWxzKCBlZGdlLnN0YXJ0VmVydGV4ICk7XHJcbiAgICB9XHJcblxyXG4gICAgZHJvcCgpIHtcclxuICAgICAgICBpZiAoIHRoaXMuX3R3aW5FZGdlICkgdGhpcy5fdHdpbkVkZ2UuX3R3aW5FZGdlID0gbnVsbDtcclxuICAgICAgICBpZiAoIHRoaXMubmV4dEVkZ2UgKVxyXG4gICAgICAgICAgICB0aGlzLm5leHRFZGdlLnByZXZFZGdlID0gbnVsbDtcclxuICAgICAgICBpZiAoIHRoaXMucHJldkVkZ2UgKVxyXG4gICAgICAgICAgICB0aGlzLnByZXZFZGdlLm5leHRFZGdlID0gbnVsbDtcclxuICAgIH1cclxuXHJcbiAgICBzcXJEaXN0YW5jZVRvUG9pbnQoIHZlcnRleDogVmVydGV4ICk6IG51bWJlciB7XHJcbiAgICAgICAgLy8gc3RvbGVuIGZyb20gaHR0cDovL3N0YWNrb3ZlcmZsb3cuY29tL2EvNjg1MzkyNi81NzY3NDg0XHJcbiAgICAgICAgdmFyIHkxID0gdGhpcy5zdGFydFZlcnRleC55O1xyXG4gICAgICAgIHZhciB4MSA9IHRoaXMuc3RhcnRWZXJ0ZXgueDtcclxuICAgICAgICB2YXIgeDIgPSB0aGlzLm5leHRFZGdlLnN0YXJ0VmVydGV4Lng7XHJcbiAgICAgICAgdmFyIHkyID0gdGhpcy5uZXh0RWRnZS5zdGFydFZlcnRleC55O1xyXG4gICAgICAgIHZhciBBID0gdmVydGV4LnggLSB4MTtcclxuICAgICAgICB2YXIgQiA9IHZlcnRleC55IC0geTE7XHJcbiAgICAgICAgdmFyIEMgPSB4MiAtIHgxO1xyXG4gICAgICAgIHZhciBEID0geTIgLSB5MTtcclxuXHJcbiAgICAgICAgdmFyIGRvdCA9IEEgKiBDICsgQiAqIEQ7XHJcbiAgICAgICAgdmFyIGxlbl9zcSA9IEMgKiBDICsgRCAqIEQ7XHJcbiAgICAgICAgdmFyIHBhcmFtID0gLTE7XHJcbiAgICAgICAgaWYgKCBsZW5fc3EgIT0gMCApIC8vIGluIGNhc2Ugb2YgMCBsZW5ndGggbGluZVxyXG4gICAgICAgICAgICBwYXJhbSA9IGRvdCAvIGxlbl9zcTtcclxuXHJcbiAgICAgICAgdmFyIHh4LCB5eTtcclxuXHJcbiAgICAgICAgaWYgKCBwYXJhbSA8IDAgKSB7XHJcbiAgICAgICAgICAgIHh4ID0geDE7XHJcbiAgICAgICAgICAgIHl5ID0geTE7XHJcbiAgICAgICAgfVxyXG4gICAgICAgIGVsc2UgaWYgKCBwYXJhbSA+IDEgKSB7XHJcbiAgICAgICAgICAgIHh4ID0geDI7XHJcbiAgICAgICAgICAgIHl5ID0geTI7XHJcbiAgICAgICAgfVxyXG4gICAgICAgIGVsc2Uge1xyXG4gICAgICAgICAgICB4eCA9IHgxICsgcGFyYW0gKiBDO1xyXG4gICAgICAgICAgICB5eSA9IHkxICsgcGFyYW0gKiBEO1xyXG4gICAgICAgIH1cclxuXHJcbiAgICAgICAgdmFyIGR4ID0gdmVydGV4LnggLSB4eDtcclxuICAgICAgICB2YXIgZHkgPSB2ZXJ0ZXgueSAtIHl5O1xyXG4gICAgICAgIHJldHVybiBkeCAqIGR4ICsgZHkgKiBkeTtcclxuICAgIH07XHJcblxyXG4gICAgc3FyRGlzdGFuY2VUb0VkZ2UoIGVkZ2U6IEVkZ2UgKTogbnVtYmVyIHtcclxuICAgICAgICAvLyBUT0RPIHJlbW92ZSBiZWNhdXNlIGNhbid0IGhhcHBlbiBpbiBjdXJyZW50IGltcGxlbWVudGF0aW9uP1xyXG4gICAgICAgIGlmICggdGhpcy5pc0ludGVyc2VjdGluZyggZWRnZSApICkgcmV0dXJuIDA7XHJcbiAgICAgICAgdmFyIGQxID0gdGhpcy5zcXJEaXN0YW5jZVRvUG9pbnQoIGVkZ2Uuc3RhcnRWZXJ0ZXggKTtcclxuICAgICAgICB2YXIgZDIgPSB0aGlzLnNxckRpc3RhbmNlVG9Qb2ludCggZWRnZS5uZXh0RWRnZS5zdGFydFZlcnRleCApO1xyXG4gICAgICAgIHZhciBkMyA9IGVkZ2Uuc3FyRGlzdGFuY2VUb1BvaW50KCB0aGlzLnN0YXJ0VmVydGV4ICk7XHJcbiAgICAgICAgdmFyIGQ0ID0gZWRnZS5zcXJEaXN0YW5jZVRvUG9pbnQoIHRoaXMubmV4dEVkZ2Uuc3RhcnRWZXJ0ZXggKTtcclxuICAgICAgICByZXR1cm4gbWluKCBkMSwgZDIsIGQzLCBkNCApO1xyXG4gICAgfTtcclxuXHJcbiAgICAvKipcclxuICAgICAqIFJldHVybnMgdGhlIHtAY29kZSBWZXJ0ZXh9IGJvdGgge0Bjb2RlIEVkZ2VzfSBzaGFyZSwgaWYgYW55LlxyXG4gICAgICpcclxuICAgICAqIEBwYXJhbSB0b0NoZWNrXHJcbiAgICAgKiAgICAgICAgICAgIHRoZSB7QGNvZGUgRWRnZX0gdG8gYmUgY2hlY2tlZCBmb3IgYSBjb21tb24ge0Bjb2RlIFZlcnRleH1cclxuICAgICAqIEByZXR1cm4gdGhlIHNoYXJlZCB7QGNvZGUgVmVydGV4fSwgaWYgYW55LCBvdGhlcndpc2Uge0Bjb2RlIG51bGx9XHJcbiAgICAgKi9cclxuICAgIGdldFRvdWNoKCB0b0NoZWNrOiBFZGdlICk6IFZlcnRleCB7XHJcbiAgICAgICAgaWYgKCAhdG9DaGVjayApIHJldHVybiBudWxsO1xyXG4gICAgICAgIGVsc2UgaWYgKCB0aGlzLnN0YXJ0VmVydGV4LmVxdWFscyggdG9DaGVjay5zdGFydFZlcnRleCApICkgcmV0dXJuIHRoaXMuc3RhcnRWZXJ0ZXg7XHJcbiAgICAgICAgZWxzZSBpZiAoIHRoaXMuc3RhcnRWZXJ0ZXguZXF1YWxzKCB0b0NoZWNrLm5leHRFZGdlLnN0YXJ0VmVydGV4ICkgKSByZXR1cm4gdGhpcy5zdGFydFZlcnRleDtcclxuICAgICAgICBlbHNlIGlmICggdGhpcy5uZXh0RWRnZS5zdGFydFZlcnRleC5lcXVhbHMoIHRvQ2hlY2suc3RhcnRWZXJ0ZXggKSApIHJldHVybiB0aGlzLm5leHRFZGdlLnN0YXJ0VmVydGV4O1xyXG4gICAgICAgIGVsc2UgaWYgKCB0aGlzLm5leHRFZGdlLnN0YXJ0VmVydGV4LmVxdWFscyggdG9DaGVjay5uZXh0RWRnZS5zdGFydFZlcnRleCApICkgcmV0dXJuIHRoaXMubmV4dEVkZ2Uuc3RhcnRWZXJ0ZXg7XHJcbiAgICAgICAgZWxzZSByZXR1cm4gbnVsbDtcclxuICAgIH1cclxuXHJcbiAgICBpc0ludGVyc2VjdGluZyggZWRnZTogRWRnZSwgcHJveGltaXR5PzogbnVtYmVyICk6IGJvb2xlYW4ge1xyXG4gICAgICAgIGlmICggIXByb3hpbWl0eSApIHtcclxuICAgICAgICAgICAgaWYgKCB0aGlzLmdldFRvdWNoKCBlZGdlICkgKSByZXR1cm4gZmFsc2U7XHJcbiAgICAgICAgICAgIGxldCBvMTogT3JpZW50YXRpb24gPSBUcmlhbmdsZS5nZXRPcmllbnRhdGlvbiggdGhpcy5zdGFydFZlcnRleCwgdGhpcy5uZXh0RWRnZS5zdGFydFZlcnRleCwgZWRnZS5zdGFydFZlcnRleCApO1xyXG4gICAgICAgICAgICBsZXQgbzI6IE9yaWVudGF0aW9uID0gVHJpYW5nbGUuZ2V0T3JpZW50YXRpb24oIHRoaXMuc3RhcnRWZXJ0ZXgsIHRoaXMubmV4dEVkZ2Uuc3RhcnRWZXJ0ZXgsIGVkZ2UubmV4dEVkZ2Uuc3RhcnRWZXJ0ZXggKTtcclxuICAgICAgICAgICAgbGV0IG8zOiBPcmllbnRhdGlvbiA9IFRyaWFuZ2xlLmdldE9yaWVudGF0aW9uKCBlZGdlLnN0YXJ0VmVydGV4LCBlZGdlLm5leHRFZGdlLnN0YXJ0VmVydGV4LCB0aGlzLnN0YXJ0VmVydGV4ICk7XHJcbiAgICAgICAgICAgIGxldCBvNDogT3JpZW50YXRpb24gPSBUcmlhbmdsZS5nZXRPcmllbnRhdGlvbiggZWRnZS5zdGFydFZlcnRleCwgZWRnZS5uZXh0RWRnZS5zdGFydFZlcnRleCwgdGhpcy5uZXh0RWRnZS5zdGFydFZlcnRleCApO1xyXG4gICAgICAgICAgICAvLyBHZW5lcmFsIGNhc2VcclxuICAgICAgICAgICAgaWYgKCBvMSAhPSBvMiAmJiBvMyAhPSBvNCApIHJldHVybiB0cnVlO1xyXG4gICAgICAgICAgICBlbHNlIHJldHVybiBmYWxzZTtcclxuICAgICAgICAgICAgLy8gY29saW5lYXIgY2FzZSBza2lwcGVkXHJcbiAgICAgICAgfVxyXG4gICAgICAgIGVsc2UgcmV0dXJuIHRoaXMuc3FyRGlzdGFuY2VUb0VkZ2UoIGVkZ2UgKSA8IHByb3hpbWl0eTtcclxuICAgIH1cclxuXHJcbiAgICAvKipcclxuICAgICAqIFNsb3dlciBtZXRob2Qgd2hpY2ggcmV0dXJucyB0aGUgZXhhY3QgcG9pbnQgb2YgY29sbGlzaW9uIHRob3VnaC4gU2hvdWxkXHJcbiAgICAgKiBiZSBvbmx5IHVzZWQgaWYgbmVlZGVkIC4uLiBzdG9sZW4gZnJvbVxyXG4gICAgICogaHR0cDovL3N0YWNrb3ZlcmZsb3cuY29tL2EvMTk2ODM0NS81NzY3NDg0XHJcbiAgICAgKi9cclxuICAgIGNhbGN1bGF0ZUludGVyc2VjdGlvblBvaW50KCBlZGdlMTogRWRnZSwgZWRnZTI6IEVkZ2UgKSB7XHJcbiAgICAgICAgbGV0IHAwX3g6IG51bWJlciA9IGVkZ2UxLnN0YXJ0VmVydGV4Lng7XHJcbiAgICAgICAgbGV0IHAwX3k6IG51bWJlciA9IGVkZ2UxLnN0YXJ0VmVydGV4Lnk7XHJcbiAgICAgICAgbGV0IHAxX3g6IG51bWJlciA9IGVkZ2UxLm5leHRFZGdlLnN0YXJ0VmVydGV4Lng7XHJcbiAgICAgICAgbGV0IHAxX3k6IG51bWJlciA9IGVkZ2UxLm5leHRFZGdlLnN0YXJ0VmVydGV4Lnk7XHJcbiAgICAgICAgbGV0IHAyX3g6IG51bWJlciA9IGVkZ2UyLnN0YXJ0VmVydGV4Lng7XHJcbiAgICAgICAgbGV0IHAyX3k6IG51bWJlciA9IGVkZ2UyLnN0YXJ0VmVydGV4Lnk7XHJcbiAgICAgICAgbGV0IHAzX3g6IG51bWJlciA9IGVkZ2UyLm5leHRFZGdlLnN0YXJ0VmVydGV4Lng7XHJcbiAgICAgICAgbGV0IHAzX3k6IG51bWJlciA9IGVkZ2UyLm5leHRFZGdlLnN0YXJ0VmVydGV4Lnk7XHJcblxyXG4gICAgICAgIGxldCBzMV94OiBudW1iZXIgPSBwMV94IC0gcDBfeDtcclxuICAgICAgICBsZXQgczFfeTogbnVtYmVyID0gcDFfeSAtIHAwX3k7XHJcbiAgICAgICAgbGV0IHMyX3g6IG51bWJlciA9IHAzX3ggLSBwMl94O1xyXG4gICAgICAgIGxldCBzMl95OiBudW1iZXIgPSBwM195IC0gcDJfeTtcclxuICAgICAgICAvLyBUT0RPOiBkaXZpc29yID0gMFxyXG4gICAgICAgIGxldCBzOiBudW1iZXIgPSAoIC1zMV95ICogKCBwMF94IC0gcDJfeCApICsgczFfeCAqICggcDBfeSAtIHAyX3kgKSApIC8gKCAtczJfeCAqIHMxX3kgKyBzMV94ICogczJfeSApO1xyXG4gICAgICAgIGxldCB0OiBudW1iZXIgPSAoIHMyX3ggKiAoIHAwX3kgLSBwMl95ICkgLSBzMl95ICogKCBwMF94IC0gcDJfeCApICkgLyAoIC1zMl94ICogczFfeSArIHMxX3ggKiBzMl95ICk7XHJcblxyXG4gICAgICAgIGlmICggcyA+PSAwICYmIHMgPD0gMSAmJiB0ID49IDAgJiYgdCA8PSAxICkge1xyXG4gICAgICAgICAgICAvLyBDb2xsaXNpb24gZGV0ZWN0ZWRcclxuICAgICAgICAgICAgbGV0IGlfeDogbnVtYmVyID0gcDBfeCArICggdCAqIHMxX3ggKTtcclxuICAgICAgICAgICAgbGV0IGlfeTogbnVtYmVyID0gcDBfeSArICggdCAqIHMxX3kgKTtcclxuXHJcbiAgICAgICAgICAgIHJldHVybiBuZXcgVmVydGV4KCBpX3gsIGlfeSApO1xyXG4gICAgICAgIH1cclxuICAgICAgICByZXR1cm4gbnVsbDtcclxuICAgIH1cclxuXHJcbiAgICAvKipcclxuICAgICAqIFJldHVybnMgdGhlIHNxdWFyZWQgbGVuZ3RoIG9mIHRoaXMge0Bjb2RlIEVkZ2V9LlxyXG4gICAgICogXHJcbiAgICAgKiBAcmV0dXJuIHRoZSBzcXVhcmVkIGxlbmd0aCBvZiB0aGlzIHtAY29kZSBFZGdlfVxyXG4gICAgICovXHJcbiAgICBwdWJsaWMgZ2V0U3FyTGVuZ3RoKCk6IG51bWJlciB7XHJcbiAgICAgICAgcmV0dXJuIHRoaXMuc3RhcnRWZXJ0ZXguc3FyRGlzdGFuY2VUb1BvaW50KCB0aGlzLm5leHRFZGdlLnN0YXJ0VmVydGV4ICk7XHJcbiAgICB9XHJcblxyXG5cclxuICAgIHRvU3RyaW5nID0gKCk6IHN0cmluZyA9PiB7XHJcbiAgICAgICAgdmFyIHByZXYgPSAhdGhpcy5wcmV2RWRnZSA/IFwibnVsbFwiIDogdGhpcy5wcmV2RWRnZS5zdGFydFZlcnRleDtcclxuICAgICAgICB2YXIgbmV4dCA9ICF0aGlzLm5leHRFZGdlID8gXCJudWxsXCIgOiB0aGlzLm5leHRFZGdlLnN0YXJ0VmVydGV4O1xyXG4gICAgICAgIHZhciB0d2luID0gIXRoaXMuX3R3aW5FZGdlID8gXCJudWxsXCIgOiB0aGlzLl90d2luRWRnZS5wcmV2RWRnZS5zdGFydFZlcnRleCArIFwiIDwtLSBcIiArIHRoaXMuc3RhcnRWZXJ0ZXggKyBcIiAtLT4gXCIgKyB0aGlzLl90d2luRWRnZS5uZXh0RWRnZS5zdGFydFZlcnRleDtcclxuICAgICAgICBpZiAoIGRlYnVnID4gMCApIHJldHVybiBwcmV2ICsgXCIgPC0tIFwiICsgdGhpcy5zdGFydFZlcnRleCArIFwiIC0tPiBcIiArIG5leHQgKyBcIlxcblR3aW46IFwiICsgdHdpbjtcclxuICAgICAgICBlbHNlIHJldHVybiBwcmV2ICsgXCIgPC0tIFwiICsgdGhpcy5zdGFydFZlcnRleCArIFwiIC0tPiBcIiArIG5leHQ7XHJcbiAgICB9XHJcblxyXG4gICAgY29tcGFyZVRvKCB0b0NvbXBhcmVXaXRoOiBFZGdlICk6IG51bWJlciB7XHJcbiAgICAgICAgbGV0IHhDb21wID0gdGhpcy5zdGFydFZlcnRleC5jb21wYXJlVG8oIHRvQ29tcGFyZVdpdGguc3RhcnRWZXJ0ZXggKTtcclxuICAgICAgICBpZiAoIHhDb21wICE9IDAgKSByZXR1cm4geENvbXA7XHJcbiAgICAgICAgZWxzZSByZXR1cm4gdGhpcy5uZXh0RWRnZS5zdGFydFZlcnRleC5jb21wYXJlVG8oIHRvQ29tcGFyZVdpdGguX25leHRFZGdlLnN0YXJ0VmVydGV4ICk7XHJcbiAgICB9XHJcblxyXG4gICAgZXF1YWxzKCBlZGdlOiBvYmplY3QgKTogYm9vbGVhbiB7XHJcbiAgICAgICAgaWYgKCAhZWRnZSApIHJldHVybiBmYWxzZTtcclxuICAgICAgICBpZiAoICEoIGVkZ2UgaW5zdGFuY2VvZiBFZGdlICkgKSByZXR1cm4gZmFsc2U7XHJcbiAgICAgICAgcmV0dXJuIHRoaXMuc3RhcnRWZXJ0ZXguZXF1YWxzKCBlZGdlLnN0YXJ0VmVydGV4ICkgJiYgdGhpcy5uZXh0RWRnZS5zdGFydFZlcnRleC5lcXVhbHMoIGVkZ2UubmV4dEVkZ2Uuc3RhcnRWZXJ0ZXggKTtcclxuICAgIH1cclxufVxyXG5cclxuXHJcbmV4cG9ydCBjbGFzcyBUcmlhbmdsZSBpbXBsZW1lbnRzIEdlb21ldHJpY09iamVjdCB7XHJcbiAgICBwcml2YXRlIF9yZWxhdGVkRWRnZTogRWRnZTtcclxuICAgIHByaXZhdGUgX21pblg6IG51bWJlcjtcclxuICAgIHByaXZhdGUgX21heFg6IG51bWJlcjtcclxuICAgIHByaXZhdGUgX21pblk6IG51bWJlcjtcclxuICAgIHByaXZhdGUgX21heFk6IG51bWJlcjtcclxuICAgIC8qKlJlZmVyZW5jZSB0byB0aGUgY29udGFpbmluZyB7QGNvZGUgSW5nYW1lVHJpYW5nbGVdKi9cclxuICAgIHByaXZhdGUgY29udGFpbmVyOiBJbmdhbWVUcmlhbmdsZTtcclxuICAgIC8vVE9ETyBtYWtlIHRoZXNlIGltbXV0YWJsZSBzb21laG93XHJcbiAgICAvKipTdG9yZWQgZm9yIGZhc3RlciBhY2Nlc3MgaW4ge0Bjb2RlIGVxdWFscygpfSBjaGVja3MgZXRjLiovXHJcbiAgICBwcml2YXRlIF92ZXJ0aWNlczogW1ZlcnRleCwgVmVydGV4LCBWZXJ0ZXhdO1xyXG4gICAgcHJpdmF0ZSBfZWRnZXM6IFtFZGdlLCBFZGdlLCBFZGdlXTtcclxuXHJcbiAgICBnZXQgcmVsYXRlZEVkZ2UoKTogRWRnZSB7IHJldHVybiB0aGlzLl9yZWxhdGVkRWRnZTsgfVxyXG4gICAgc2V0IHJlbGF0ZWRFZGdlKCBlZGdlOiBFZGdlICkgeyB0aGlzLl9yZWxhdGVkRWRnZSA9IGVkZ2U7IH1cclxuICAgIGdldCBpbmdhbWVUcmlhbmdsZSgpOiBJbmdhbWVUcmlhbmdsZSB7IHJldHVybiB0aGlzLmNvbnRhaW5lcjsgfVxyXG4gICAgc2V0IGluZ2FtZVRyaWFuZ2xlKCBpbmdhbWVUcmlhbmdsZTogSW5nYW1lVHJpYW5nbGUgKSB7IHRoaXMuY29udGFpbmVyID0gaW5nYW1lVHJpYW5nbGU7IH1cclxuICAgIGdldCB2ZXJ0aWNlcygpOiBbVmVydGV4LCBWZXJ0ZXgsIFZlcnRleF0ge1xyXG4gICAgICAgIHJldHVybiB0aGlzLl92ZXJ0aWNlcztcclxuICAgIH1cclxuICAgIGdldCBlZGdlcygpOiBbRWRnZSwgRWRnZSwgRWRnZV0ge1xyXG4gICAgICAgIHJldHVybiB0aGlzLl9lZGdlcztcclxuICAgIH1cclxuXHJcbiAgICBjb25zdHJ1Y3RvciggZWRnZTE6IEVkZ2UgKSB7XHJcbiAgICAgICAgdGhpcy5fcmVsYXRlZEVkZ2UgPSBlZGdlMTtcclxuICAgICAgICB0aGlzLl9yZWxhdGVkRWRnZS5yZWxhdGVkVHJpYW5nbGUgPSB0aGlzO1xyXG4gICAgICAgIHRoaXMuX21pblggPSBtaW4oIHRoaXMucmVsYXRlZEVkZ2Uuc3RhcnRWZXJ0ZXgueCwgdGhpcy5yZWxhdGVkRWRnZS5uZXh0RWRnZS5zdGFydFZlcnRleC54LCB0aGlzLnJlbGF0ZWRFZGdlLnByZXZFZGdlLnN0YXJ0VmVydGV4LnggKTtcclxuICAgICAgICB0aGlzLl9tYXhYID0gbWF4KCB0aGlzLnJlbGF0ZWRFZGdlLnN0YXJ0VmVydGV4LngsIHRoaXMucmVsYXRlZEVkZ2UubmV4dEVkZ2Uuc3RhcnRWZXJ0ZXgueCwgdGhpcy5yZWxhdGVkRWRnZS5wcmV2RWRnZS5zdGFydFZlcnRleC54ICk7XHJcbiAgICAgICAgdGhpcy5fbWluWSA9IG1pbiggdGhpcy5yZWxhdGVkRWRnZS5zdGFydFZlcnRleC55LCB0aGlzLnJlbGF0ZWRFZGdlLm5leHRFZGdlLnN0YXJ0VmVydGV4LnksIHRoaXMucmVsYXRlZEVkZ2UucHJldkVkZ2Uuc3RhcnRWZXJ0ZXgueSApO1xyXG4gICAgICAgIHRoaXMuX21heFkgPSBtYXgoIHRoaXMucmVsYXRlZEVkZ2Uuc3RhcnRWZXJ0ZXgueSwgdGhpcy5yZWxhdGVkRWRnZS5uZXh0RWRnZS5zdGFydFZlcnRleC55LCB0aGlzLnJlbGF0ZWRFZGdlLnByZXZFZGdlLnN0YXJ0VmVydGV4LnkgKTtcclxuICAgICAgICAvL3NvcnQgZm9yIGVhc2llciBjb21wYXJpc29uXHJcbiAgICAgICAgdGhpcy5fdmVydGljZXMgPSBbdGhpcy5yZWxhdGVkRWRnZS5zdGFydFZlcnRleCwgdGhpcy5yZWxhdGVkRWRnZS5uZXh0RWRnZS5zdGFydFZlcnRleCwgdGhpcy5yZWxhdGVkRWRnZS5wcmV2RWRnZS5zdGFydFZlcnRleF0uXHJcbiAgICAgICAgICAgIHNvcnQoKCB2MSwgdjIgKSA9PiB2MS5jb21wYXJlVG8oIHYyICkgKSBhcyBbVmVydGV4LCBWZXJ0ZXgsIFZlcnRleF07XHJcbiAgICAgICAgdGhpcy5fZWRnZXMgPSBbdGhpcy5yZWxhdGVkRWRnZSwgdGhpcy5yZWxhdGVkRWRnZS5uZXh0RWRnZSwgdGhpcy5yZWxhdGVkRWRnZS5wcmV2RWRnZV0uXHJcbiAgICAgICAgICAgIHNvcnQoKCBlMSwgZTIgKSA9PiBlMS5jb21wYXJlVG8oIGUyICkgKSBhcyBbRWRnZSwgRWRnZSwgRWRnZV07XHJcbiAgICB9XHJcblxyXG4gICAgc3RhdGljIG9mKCB2ZXJ0ZXgxOiBWZXJ0ZXgsIHZlcnRleDI6IFZlcnRleCwgdmVydGV4MzogVmVydGV4ICk6IFRyaWFuZ2xlIHtcclxuICAgICAgICBsZXQgYWN0dWFsT3JpZW50YXRpb246IE9yaWVudGF0aW9uID0gVHJpYW5nbGUuZ2V0T3JpZW50YXRpb24oIHZlcnRleDEsIHZlcnRleDIsIHZlcnRleDMgKTtcclxuICAgICAgICBpZiAoIGFjdHVhbE9yaWVudGF0aW9uID09IE9yaWVudGF0aW9uLkNPTElORUFSICkgcmV0dXJuIG51bGw7XHJcbiAgICAgICAgbGV0IHYxOiBWZXJ0ZXggPSB2ZXJ0ZXgxO1xyXG4gICAgICAgIGxldCB2MjogVmVydGV4O1xyXG4gICAgICAgIGxldCB2MzogVmVydGV4O1xyXG4gICAgICAgIGlmICggYWN0dWFsT3JpZW50YXRpb24gPT0gdGFyZ2V0T3JpZW50YXRpb24gKSB7XHJcbiAgICAgICAgICAgIHYyID0gdmVydGV4MjtcclxuICAgICAgICAgICAgdjMgPSB2ZXJ0ZXgzO1xyXG4gICAgICAgIH0gZWxzZSB7XHJcbiAgICAgICAgICAgIHYyID0gdmVydGV4MztcclxuICAgICAgICAgICAgdjMgPSB2ZXJ0ZXgyO1xyXG4gICAgICAgIH1cclxuICAgICAgICBsZXQgdjF2MjogRWRnZSA9IG5ldyBFZGdlKCB2MSwgbnVsbCwgbnVsbCwgbnVsbCApO1xyXG4gICAgICAgIGxldCB2MnYzOiBFZGdlID0gbmV3IEVkZ2UoIHYyLCB2MXYyLCBudWxsLCBudWxsICk7XHJcbiAgICAgICAgbGV0IHYzdjE6IEVkZ2UgPSBuZXcgRWRnZSggdjMsIHYydjMsIHYxdjIsIG51bGwgKTtcclxuICAgICAgICB2MXYyLm5leHRFZGdlID0gdjJ2MztcclxuICAgICAgICB2MXYyLnByZXZFZGdlID0gdjN2MTtcclxuICAgICAgICB2MnYzLm5leHRFZGdlID0gdjN2MTtcclxuICAgICAgICByZXR1cm4gbmV3IFRyaWFuZ2xlKCB2MXYyICk7XHJcbiAgICB9XHJcblxyXG4gICAgLy8gaWRlYSBzdG9sZW4gZnJvbVxyXG4gICAgLy8gaHR0cDovL3d3dy5nZWVrc2ZvcmdlZWtzLm9yZy9jaGVjay1pZi10d28tZ2l2ZW4tbGluZS1zZWdtZW50cy1pbnRlcnNlY3RcclxuICAgIHN0YXRpYyBnZXRPcmllbnRhdGlvbiggdmVydGV4MTogVmVydGV4LCB2ZXJ0ZXgyOiBWZXJ0ZXgsIHZlcnRleDM6IFZlcnRleCApOiBPcmllbnRhdGlvbiB7XHJcbiAgICAgICAgLy8gU2VlIGh0dHA6Ly93d3cuZ2Vla3Nmb3JnZWVrcy5vcmcvb3JpZW50YXRpb24tMy1vcmRlcmVkLXBvaW50cy9cclxuICAgICAgICAvLyBmb3IgZGV0YWlscyBvZiBiZWxvdyBmb3JtdWxhLlxyXG4gICAgICAgIGxldCB2YWw6IG51bWJlciA9ICggdmVydGV4Mi55IC0gdmVydGV4MS55ICkgKiAoIHZlcnRleDMueCAtIHZlcnRleDIueCApIC1cclxuICAgICAgICAgICAgKCB2ZXJ0ZXgyLnggLSB2ZXJ0ZXgxLnggKSAqICggdmVydGV4My55IC0gdmVydGV4Mi55ICk7XHJcblxyXG4gICAgICAgIGlmICggdmFsID09IDAgKSByZXR1cm4gT3JpZW50YXRpb24uQ09MSU5FQVI7ICAvLyBjb2xpbmVhclxyXG5cclxuICAgICAgICAvKlxyXG4gICAgICAgICAqIENhbnZhcyAoMCwwKSBsaWVzIGluIHRoZSB1cHBlciBsZWZ0IGNvcm5lciwgd2hpbGUgdGhlIHJlc3VsdCBpcyBiYXNlZFxyXG4gICAgICAgICAqIG9uIHRoZSBjbGFzc2ljYWwgcG9pbnQgb2Ygb3JpZ2luLiBXb25cInQgbWFrZSBhIGRpZmZlcmVuY2UgaW4gdGhlXHJcbiAgICAgICAgICogY2hlY2tJbnRlcnNlY3RpbmctZXZhbHVhdGlvbiB0aG91Z2hcclxuICAgICAgICAgKi9cclxuICAgICAgICByZXR1cm4gKCB2YWwgPiAwICkgPyBPcmllbnRhdGlvbi5DTE9DS1dJU0UgOiBPcmllbnRhdGlvbi5DT1VOVEVSX0NMT0NLV0lTRTtcclxuICAgIH1cclxuXHJcbiAgICBkcm9wKCBjbGVhbkxpc3RzPzogRWRnZVtdICkge1xyXG4gICAgICAgIGZvciAoIGxldCBlZGdlIG9mIHRoaXMuZWRnZXMgKSB7XHJcbiAgICAgICAgICAgIGVkZ2UuZHJvcCgpO1xyXG4gICAgICAgIH1cclxuICAgIH1cclxuXHJcbiAgICAvKipcclxuICAgICAqIFJldHVybnMgdGhlIGFyZWEgZW5jbG9zZWQgYnkgdGhpcyB7QGNvZGUgVHJpYW5nbGV9LlxyXG4gICAgICogQHJldHVybiB0aGUgYXJlYSBvZiB0aGlzIHtAY29kZSBUcmlhbmdsZX1cclxuICAgICAqL1xyXG4gICAgcHVibGljIGdldEFyZWEoKTogbnVtYmVyIHtcclxuICAgICAgICBsZXQgdjEgPSB0aGlzLnZlcnRpY2VzWzBdO1xyXG4gICAgICAgIGxldCB2MiA9IHRoaXMudmVydGljZXNbMV07XHJcbiAgICAgICAgbGV0IHYzID0gdGhpcy52ZXJ0aWNlc1syXTtcclxuICAgICAgICBsZXQgcGFydDEgPSB2MS54ICogKCB2Mi55IC0gdjMueSApO1xyXG4gICAgICAgIGxldCBwYXJ0MiA9IHYyLnggKiAoIHYzLnkgLSB2MS55ICk7XHJcbiAgICAgICAgbGV0IHBhcnQzID0gdjMueCAqICggdjEueSAtIHYyLnkgKTtcclxuICAgICAgICByZXR1cm4gTWF0aC5hYnMoIHBhcnQxICsgcGFydDIgKyBwYXJ0MyApID4+IDE7XHJcbiAgICB9XHJcblxyXG5cclxuICAgIGNvbnRhaW5zKCB2ZXJ0ZXg6IFZlcnRleCApOiBib29sZWFuIHtcclxuICAgICAgICBpZiAoICF2ZXJ0ZXggKSByZXR1cm4gZmFsc2U7XHJcbiAgICAgICAgaWYgKCB0aGlzLl9tYXhYIDwgdmVydGV4LnggfHwgdGhpcy5fbWluWCA+IHZlcnRleC54XHJcbiAgICAgICAgICAgIHx8IHRoaXMuX21heFkgPCB2ZXJ0ZXgueSB8fCB0aGlzLl9taW5ZID4gdmVydGV4LnkgKSByZXR1cm4gZmFsc2U7XHJcblxyXG5cclxuICAgICAgICAvLyBTdG9sZW4gZnJvbSBodHRwOi8vYmxhY2twYXduLmNvbS90ZXh0cy9wb2ludGlucG9seVxyXG5cclxuICAgICAgICAvLyBDb21wdXRlIHZlY3RvcnNcclxuICAgICAgICBsZXQgdjA6IFZlcnRleCA9IHRoaXMucmVsYXRlZEVkZ2UucHJldkVkZ2Uuc3RhcnRWZXJ0ZXguc3VidHJhY3RWZXJ0ZXgoIHRoaXMucmVsYXRlZEVkZ2Uuc3RhcnRWZXJ0ZXggKTtcclxuICAgICAgICBsZXQgdjE6IFZlcnRleCA9IHRoaXMucmVsYXRlZEVkZ2UubmV4dEVkZ2Uuc3RhcnRWZXJ0ZXguc3VidHJhY3RWZXJ0ZXgoIHRoaXMucmVsYXRlZEVkZ2Uuc3RhcnRWZXJ0ZXggKTtcclxuICAgICAgICBsZXQgdjI6IFZlcnRleCA9IHZlcnRleC5zdWJ0cmFjdFZlcnRleCggdGhpcy5yZWxhdGVkRWRnZS5zdGFydFZlcnRleCApO1xyXG5cclxuICAgICAgICAvLyBDb21wdXRlIGRvdCBwcm9kdWN0c1xyXG4gICAgICAgIGxldCBkb3QwMDogbnVtYmVyID0gdjAuc2thbGFyUHJvZHVjdCggdjAgKTtcclxuICAgICAgICBsZXQgZG90MDE6IG51bWJlciA9IHYwLnNrYWxhclByb2R1Y3QoIHYxICk7XHJcbiAgICAgICAgbGV0IGRvdDAyOiBudW1iZXIgPSB2MC5za2FsYXJQcm9kdWN0KCB2MiApO1xyXG4gICAgICAgIGxldCBkb3QxMTogbnVtYmVyID0gdjEuc2thbGFyUHJvZHVjdCggdjEgKTtcclxuICAgICAgICBsZXQgZG90MTI6IG51bWJlciA9IHYxLnNrYWxhclByb2R1Y3QoIHYyICk7XHJcblxyXG4gICAgICAgIC8vIENvbXB1dGUgYmFyeWNlbnRyaWMgdmVydGV4aW5hdGVzXHJcbiAgICAgICAgbGV0IGludkRlbm9tOiBudW1iZXIgPSAxIC8gKCBkb3QwMCAqIGRvdDExIC0gZG90MDEgKiBkb3QwMSApO1xyXG4gICAgICAgIGxldCB1OiBudW1iZXIgPSAoIGRvdDExICogZG90MDIgLSBkb3QwMSAqIGRvdDEyICkgKiBpbnZEZW5vbTtcclxuICAgICAgICBsZXQgdjogbnVtYmVyID0gKCBkb3QwMCAqIGRvdDEyIC0gZG90MDEgKiBkb3QwMiApICogaW52RGVub207XHJcblxyXG4gICAgICAgIC8vIENoZWNrIGlmIHBvaW50IGlzIGluIHRyaWFuZ2xlXHJcbiAgICAgICAgcmV0dXJuICggdSA+PSAwICkgJiYgKCB2ID49IDAgKSAmJiAoIHUgKyB2IDwgMSApO1xyXG4gICAgfVxyXG5cclxuICAgIC8qKlxyXG4gICAgICogQ2hlY2tzIHdoZXRoZXIgYSB7QGNvZGUgVHJpYW5nbGV9IGludGVyc2VjdHMgd2l0aCB0aGlzIG9uZS4gQSBjb21tb25cclxuICAgICAqIHtAY29kZSBWZXJ0ZXh9IGRvZXMgbm90IGNvdW50IGFzIGludGVyc2VjdGlvbi4gTm90ZSB0aGlzIG9ubHkgYSBmYXN0IGNoZWNrIHdpbGwgYmUgZG9uZSBoZXJlLFxyXG4gICAgICogcmVseWluZyBvbiB0aGUgZmFjdCB0aGF0IHNvbWUgc2l0dWF0aW9ucyBtaWdodCBub3QgaGFwcGVuIGR1ZSB0byB0aGUgd2F5IHtAY29kZSBUcmlhbmdlc30gYXJlXHJcbiAgICAgKiBjb25zdHJ1Y3RlZCBoZXJlICh2aWEgdGhlIGV2YWx1YXRpb24gb2YgYSBkcmF3biBsaW5lKS4gVGhlIGluLWRlcHRoIGNoZWNrIHdpbGwgb25seSBoYXBwZW5cclxuICAgICAqIG9uIGJhY2tlbmQgc2lkZS5cclxuICAgICAqXHJcbiAgICAgKiBAcGFyYW0gdG9DaGVja1xyXG4gICAgICogICAgICAgICAgICB0aGUge0Bjb2RlIFRyaWFuZ2xlfSB0byBiZSBjaGVja2VkIGZvciBpbnRlcnNlY3Rpb25cclxuICAgICAqIEByZXR1cm4ge0Bjb2RlIHRydWV9LCBpZiB0aGUge0Bjb2RlIFRyaWFuZ2xlfSBpbnRlcnNlY3RzIHdpdGggb24gdGhpcyBvbmVcclxuICAgICAqL1xyXG4gICAgaW50ZXJzZWN0c1dpdGhUcmlhbmdsZSggdHJpYW5nbGU6IFRyaWFuZ2xlICk6IGJvb2xlYW4ge1xyXG4gICAgICAgIGlmICggIXRyaWFuZ2xlICkgcmV0dXJuIGZhbHNlO1xyXG4gICAgICAgIGlmICggdGhpcy5fbWF4WCA8IHRyaWFuZ2xlLl9taW5YIHx8IHRoaXMuX21pblggPiB0cmlhbmdsZS5fbWF4WFxyXG4gICAgICAgICAgICB8fCB0aGlzLl9tYXhZIDwgdHJpYW5nbGUuX21pblkgfHwgdGhpcy5fbWluWSA+IHRyaWFuZ2xlLl9tYXhZICkge1xyXG4gICAgICAgICAgICByZXR1cm4gZmFsc2U7XHJcbiAgICAgICAgfVxyXG4gICAgICAgIGlmICggdGhpcy5pc05laWdoYm91ciggdHJpYW5nbGUgKSApIHJldHVybiBmYWxzZTtcclxuXHJcblxyXG4gICAgICAgIGZvciAoIGxldCB2ZXJ0ZXggb2YgdHJpYW5nbGUudmVydGljZXMgKSB7XHJcbiAgICAgICAgICAgIC8vIENoZWNrIGFsbCB2ZXJ0aWNlcyBvZiB0aGUgbmV3IHRyaWFuZ2xlOiBpZiB0aGVyZSBhIHZlcnRleFxyXG4gICAgICAgICAgICAvLyBjb250YWluZWQgaW4gdGhpcyB0cmlhbmdsZSBhbmQgdGhpcyB2ZXJ0ZXggaXMgbm90IGFuIHZlcnRleCBvZlxyXG4gICAgICAgICAgICAvLyB0aGlzIHRyaWFuZ2xlLCB0b28sIHRoZW4gdGhlcmUncyBhbiBpbnRlcnNlY3Rpb25cclxuXHJcbiAgICAgICAgICAgIGZvciAoIGxldCB0cmlhbmdsZUVkZ2Ugb2YgdGhpcy5lZGdlcyApIHtcclxuICAgICAgICAgICAgICAgIGZvciAoIGxldCB0b0NoZWNrRWRnZSBvZiB0cmlhbmdsZS5lZGdlcyApIHtcclxuICAgICAgICAgICAgICAgICAgICBpZiAoIHRyaWFuZ2xlRWRnZS5pc0ludGVyc2VjdGluZyggdG9DaGVja0VkZ2UgKSApIHJldHVybiB0cnVlO1xyXG4gICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICB9XHJcbiAgICAgICAgfVxyXG4gICAgICAgIHJldHVybiBmYWxzZTtcclxuICAgIH1cclxuXHJcbiAgICBpc05laWdoYm91ciggdHJpYW5nbGU6IFRyaWFuZ2xlICk6IGJvb2xlYW4ge1xyXG4gICAgICAgIGlmICggIXRyaWFuZ2xlICkgcmV0dXJuIGZhbHNlO1xyXG4gICAgICAgIGZvciAoIGxldCBlZGdlIG9mIHRyaWFuZ2xlLmVkZ2VzICkge1xyXG4gICAgICAgICAgICBmb3IgKCBsZXQgb3duRWRnZSBvZiB0aGlzLmVkZ2VzICkge1xyXG4gICAgICAgICAgICAgICAgaWYgKCBvd25FZGdlLmdldFR3aW5FZGdlKCkgJiYgb3duRWRnZS5nZXRUd2luRWRnZSgpLmVxdWFscyggZWRnZSApICkgcmV0dXJuIHRydWU7XHJcbiAgICAgICAgICAgIH1cclxuICAgICAgICB9XHJcbiAgICAgICAgcmV0dXJuIGZhbHNlO1xyXG4gICAgfVxyXG5cclxuICAgIC8qKlxyXG4gICAgICogUmV0dXJucyB0aGUge0Bjb2RlIFRyaWFuZ2xlJ3N9IGNlbnRyb2lkLlxyXG4gICAgICogQHJldHVybiB0aGUgY2VudHJvaWRcclxuICAgICAqL1xyXG4gICAgZ2V0Q2VudHJvaWQoKTogVmVydGV4IHtcclxuICAgICAgICBsZXQgeCA9ICggdGhpcy52ZXJ0aWNlc1swXS54ICsgdGhpcy52ZXJ0aWNlc1sxXS54ICsgdGhpcy52ZXJ0aWNlc1syXS54ICkgKiAwLjMzMztcclxuICAgICAgICBsZXQgeSA9ICggdGhpcy52ZXJ0aWNlc1swXS55ICsgdGhpcy52ZXJ0aWNlc1sxXS55ICsgdGhpcy52ZXJ0aWNlc1syXS55ICkgKiAwLjMzMztcclxuICAgICAgICByZXR1cm4gbmV3IFZlcnRleCggeCwgeSApO1xyXG4gICAgfVxyXG5cclxuICAgIHRvU3RyaW5nID0gKCk6IHN0cmluZyA9PiB7XHJcbiAgICAgICAgaWYgKCBkZWJ1ZyA+IDAgKSB7XHJcbiAgICAgICAgICAgIHJldHVybiBcIlN0YXJ0IGVkZ2U6IFwiICsgdGhpcy5yZWxhdGVkRWRnZSArIFwiXFxuU2Vjb25kIGVkZ2U6IFwiICsgdGhpcy5yZWxhdGVkRWRnZS5uZXh0RWRnZSArIFwiXFxuVGhpcmQgZWRnZTogXCIgKyB0aGlzLnJlbGF0ZWRFZGdlLnByZXZFZGdlO1xyXG4gICAgICAgIH0gZWxzZSByZXR1cm4gdGhpcy5yZWxhdGVkRWRnZS5zdGFydFZlcnRleCArIFwiOlwiICsgdGhpcy5yZWxhdGVkRWRnZS5uZXh0RWRnZS5zdGFydFZlcnRleCArIFwiOlwiICsgdGhpcy5yZWxhdGVkRWRnZS5wcmV2RWRnZS5zdGFydFZlcnRleDtcclxuICAgIH1cclxuXHJcbiAgICBjb21wYXJlVG8oIHRvQ29tcGFyZVdpdGg6IFRyaWFuZ2xlICk6IG51bWJlciB7XHJcbiAgICAgICAgbGV0IG93bkVkZ2VzID0gdGhpcy5lZGdlcztcclxuICAgICAgICBsZXQgb3RoZXJFZGdlcyA9IHRvQ29tcGFyZVdpdGguZWRnZXM7XHJcbiAgICAgICAgbGV0IHhDb21wMSA9IG93bkVkZ2VzWzBdLmNvbXBhcmVUbyggb3RoZXJFZGdlc1swXSApO1xyXG4gICAgICAgIGlmICggeENvbXAxICE9IDAgKSByZXR1cm4geENvbXAxO1xyXG4gICAgICAgIGxldCB4Q29tcDIgPSBvd25FZGdlc1sxXS5jb21wYXJlVG8oIG90aGVyRWRnZXNbMV0gKTtcclxuICAgICAgICBpZiAoIHhDb21wMiAhPSAwICkgcmV0dXJuIHhDb21wMjtcclxuICAgICAgICBlbHNlIHJldHVybiBvd25FZGdlc1syXS5jb21wYXJlVG8oIG90aGVyRWRnZXNbMl0gKTtcclxuICAgIH1cclxuXHJcbiAgICBlcXVhbHMoIHRyaWFuZ2xlOiBvYmplY3QgKTogYm9vbGVhbiB7XHJcbiAgICAgICAgaWYgKCAhKCB0cmlhbmdsZSBpbnN0YW5jZW9mIFRyaWFuZ2xlICkgKSByZXR1cm4gZmFsc2U7XHJcbiAgICAgICAgbGV0IG93blZlcnRpY2VzID0gdGhpcy52ZXJ0aWNlcztcclxuICAgICAgICBsZXQgb3RoZXJWZXJ0aWNlcyA9IHRyaWFuZ2xlLnZlcnRpY2VzO1xyXG4gICAgICAgIHJldHVybiAoIG93blZlcnRpY2VzWzBdLmVxdWFscyggb3RoZXJWZXJ0aWNlc1swXSApICYmIG93blZlcnRpY2VzWzFdLmVxdWFscyggb3RoZXJWZXJ0aWNlc1sxXSApICYmIG93blZlcnRpY2VzWzJdLmVxdWFscyggb3RoZXJWZXJ0aWNlc1syXSApICk7XHJcbiAgICB9XHJcbn1cclxuXHJcbi8qKlxyXG4gKiBUYWtlcyB2ZXJ0aWNlcyBhbmQgYnVpbGRzIGVkZ2VzIGZyb20gdGhlbS4gQWxsIGVkZ2VzIHdpbGwgYmUgbGlua2VkIGluIHRoZWlyXHJcbiAqIG9yZGVyIGFuZCB0aGUgZmlyc3Qgd2l0aCB0aGUgbGFzdCBvbmUuXHJcbiAqIFxyXG4gKiBAcmV0dXJucyBhcnJheSBvZiBjcmVhdGVkIGVkZ2VzXHJcbiAqL1xyXG5leHBvcnQgZnVuY3Rpb24gdmVydGljZXNUb0VkZ2VzKCAuLi52ZXJ0aWNlczogVmVydGV4W10gKSB7XHJcbiAgICBpZiAoIHZlcnRpY2VzLmxlbmd0aCA8IDMgKSByZXR1cm47XHJcbiAgICBsZXQgZWRnZXM6IEVkZ2VbXSA9IFtdO1xyXG4gICAgZm9yICggdmFyIGkgPSAwOyBpIDwgdmVydGljZXMubGVuZ3RoOyBpKysgKSB7XHJcbiAgICAgICAgZWRnZXMgPSBlZGdlcy5jb25jYXQoIG5ldyBFZGdlKCB2ZXJ0aWNlc1tpXSwgbnVsbCwgbnVsbCApICk7XHJcbiAgICB9XHJcbiAgICBmb3IgKCBsZXQgaSA9IDE7IGkgPCBlZGdlcy5sZW5ndGggLSAxOyBpKysgKSB7XHJcbiAgICAgICAgZWRnZXNbaV0ucHJldkVkZ2UgPSBlZGdlc1tpIC0gMV07XHJcbiAgICAgICAgZWRnZXNbaV0ubmV4dEVkZ2UgPSBlZGdlc1tpICsgMV07XHJcbiAgICB9XHJcbiAgICBlZGdlc1swXS5uZXh0RWRnZSA9IGVkZ2VzWzFdO1xyXG4gICAgZWRnZXNbMF0ucHJldkVkZ2UgPSBlZGdlc1tlZGdlcy5sZW5ndGggLSAxXTtcclxuICAgIGVkZ2VzW2VkZ2VzLmxlbmd0aCAtIDFdLm5leHRFZGdlID0gZWRnZXNbMF07XHJcbiAgICBlZGdlc1tlZGdlcy5sZW5ndGggLSAxXS5wcmV2RWRnZSA9IGVkZ2VzW2VkZ2VzLmxlbmd0aCAtIDJdO1xyXG4gICAgcmV0dXJuIGVkZ2VzO1xyXG59XG5cblxuLy8gV0VCUEFDSyBGT09URVIgLy9cbi8vIC4vdHlwZXNjcmlwdC9sb2dpYy9nZW9tZXRyaWNfb2JqZWN0cy50cyIsIi8qKlxyXG4gKiBSZXByZXNlbnRzIGEgZ2VvbWV0cmljIHtAY29kZSBUcmlhbmdsZX0gd3JhcHBlZCB3aXRoIHNvbWUgZ2FtZSByZWxldmFudCBpbmZvcm1hdGlvbi4gIFxyXG4gKi9cclxuaW1wb3J0IHsgVHJpYW5nbGUgfSBmcm9tIFwiLi4vbG9naWMvZ2VvbWV0cmljX29iamVjdHNcIjtcclxuaW1wb3J0IHsgQm9hcmRTdGF0ZSB9IGZyb20gXCIuL2JvYXJkX3N0YXRlXCI7XHJcbmltcG9ydCB7IFN0YW5kYXJkT2JqZWN0IH0gZnJvbSBcIi4uL3V0aWwvc2hhcmVkXCI7XHJcblxyXG5leHBvcnQgY2xhc3MgSW5nYW1lVHJpYW5nbGUgaW1wbGVtZW50cyBTdGFuZGFyZE9iamVjdCB7XHJcbiAgICAvKipUaGUgbWF4aW11bSBkZWZlbnNlIGZvciBhIHtAY29kZSBJbmdhbWVUcmlhbmdsZX0qL1xyXG4gICAgc3RhdGljIHJlYWRvbmx5IE1BWF9ERUZFTlNFID0gMTA7XHJcbiAgICAvKiogVGhlIGJhc2UgdmFsdWUgdG8gY2FsY3VsYXRlIGVmZmVjdCBpbXBhY3RzIHdpdGggKi9cclxuICAgIHByaXZhdGUgX2Jhc2VWYWx1ZTogbnVtYmVyO1xyXG4gICAgLyoqR2VvbWV0cmljIHtAY29kZSBUcmlhbmdsZX0gaGVsZCBieSB0aGlzIGluc3RhbmNlKi9cclxuICAgIHByaXZhdGUgX3RyaWFuZ2xlOiBUcmlhbmdsZTtcclxuICAgIC8qKlBsYXllciB3aG8gb3ducyB0aGlzIHtAY29kZSBJbmdhbWVUcmlhbmdsZX0qL1xyXG4gICAgcHJpdmF0ZSBfb3duZXI6IHN0cmluZztcclxuICAgIC8qKkluZmx1ZW5jZXMgdGhlIGVuZW1pZXMgY29zdHMgZG8gZGVzdHJveSB0aGlzIHtAY29kZSBJbmdhbWVUcmlhbmdsZX0qL1xyXG4gICAgcHJpdmF0ZSBfZGVmZW5zZTogbnVtYmVyID0gMDtcclxuICAgIGdldCBiYXNlVmFsdWUoKTogbnVtYmVyIHsgcmV0dXJuIHRoaXMuX2Jhc2VWYWx1ZTsgfVxyXG4gICAgc2V0IGJhc2VWYWx1ZSggYmFzZVZhbHVlOiBudW1iZXIgKSB7IHRoaXMuX2Jhc2VWYWx1ZSA9IGJhc2VWYWx1ZTsgfVxyXG4gICAgZ2V0IHRyaWFuZ2xlKCk6IFRyaWFuZ2xlIHsgcmV0dXJuIHRoaXMuX3RyaWFuZ2xlOyB9XHJcbiAgICBzZXQgdHJpYW5nbGUoIHRyaWFuZ2xlOiBUcmlhbmdsZSApIHsgdGhpcy5fdHJpYW5nbGUgPSB0cmlhbmdsZTsgfVxyXG4gICAgZ2V0IG93bmVyKCk6IHN0cmluZyB7IHJldHVybiB0aGlzLl9vd25lcjsgfVxyXG4gICAgc2V0IG93bmVyKCBvd25lcjogc3RyaW5nICkgeyB0aGlzLl9vd25lciA9IG93bmVyOyB9XHJcbiAgICBnZXQgZGVmZW5zZSgpOiBudW1iZXIgeyByZXR1cm4gdGhpcy5fZGVmZW5zZTsgfVxyXG4gICAgc2V0IGRlZmVuc2UoIGRlZmVuc2VWYWx1ZTogbnVtYmVyICkgeyBpZiAoIGRlZmVuc2VWYWx1ZSA8PSBJbmdhbWVUcmlhbmdsZS5NQVhfREVGRU5TRSApIHRoaXMuX2RlZmVuc2UgPSBkZWZlbnNlVmFsdWU7IH1cclxuXHJcbiAgICAvKipcclxuICAgICAqIENyZWF0ZXMgYSBuZXcge0Bjb2RlIEluZ2FtZVRyaWFuZ2xlfS4gUHJpdmF0ZSB0byBtYWtlIHN1cmUgaXQgY2FuIGJlIG9ubHkgY2FsbGVkIGZyb20gdGhlIGZhY3RvcnkgbWV0aG9kXHJcbiAgICAgKiB3aGljaCBlbnN1cmVzIHRoZSByZXF1cmlyZWQgc2lkZSBlZmZlY3RzLlxyXG4gICAgICogQHBhcmFtIGlubmVyVHJpYW5nbGUgdG8gYmUgaGVsZCBieSB0aGlzIGluc3RhbmNlXHJcbiAgICAgKiBAcGFyYW0gb3duZXIgdGhlIHBsYXllciB3aG8gb253cyB0aGlzIHtAY29kZSBJbmdhbWVUcmlhbmdsZX1cclxuICAgICAqL1xyXG4gICAgcHJpdmF0ZSBjb25zdHJ1Y3RvciggaW5uZXJUcmlhbmdsZTogVHJpYW5nbGUsIG93bmVyPzogc3RyaW5nICkge1xyXG4gICAgICAgIHRoaXMuX3RyaWFuZ2xlID0gaW5uZXJUcmlhbmdsZTtcclxuICAgICAgICB0aGlzLl9vd25lciA9IG93bmVyO1xyXG4gICAgfVxyXG5cclxuICAgIC8qKlxyXG4gICAgICogQ3JlYXRlcyBhbiByZXR1cm5zIGEgbmV3IHtAY29kZSBJbmdhbWVUcmlhbmdsZX0uIExpbmtzIHRoZSBpbm5lciB7QGNvZGUgVHJpYW5nbGV9IHRvIHRoaXMgaW5zdGFuY2UuXHJcbiAgICAgKiBAcGFyYW0gaW5uZXJUcmlhbmdsZSB0byBiZSBoZWxkIGJ5IHRoaXMgaW5zdGFuY2VcclxuICAgICAqIEBwYXJhbSBvd25lciB0aGUgcGxheWVyIHdobyBvbndzIHRoaXMge0Bjb2RlIEluZ2FtZVRyaWFuZ2xlfVxyXG4gICAgICovXHJcbiAgICBzdGF0aWMgb2YoIGlubmVyVHJpYW5nbGU6IFRyaWFuZ2xlLCBvd25lcjogc3RyaW5nICkge1xyXG4gICAgICAgIGlmICggIWlubmVyVHJpYW5nbGUgKSByZXR1cm4gbnVsbDtcclxuICAgICAgICBsZXQgdG9SZXR1cm4gPSBuZXcgSW5nYW1lVHJpYW5nbGUoIGlubmVyVHJpYW5nbGUsIG93bmVyICk7XHJcbiAgICAgICAgdG9SZXR1cm4udHJpYW5nbGUuaW5nYW1lVHJpYW5nbGUgPSB0b1JldHVybjtcclxuICAgICAgICByZXR1cm4gdG9SZXR1cm47XHJcbiAgICB9XHJcblxyXG4gICAgLyoqXHJcbiAgICAgKiBSZW1vdmVzIGFsbCByZWZlcmVuY2VzIG9mIHRoZSBoZWxkIGlubmVyIHtAY29kZSBUcmlhbmdsZX0uXHJcbiAgICAgKi9cclxuICAgIGRyb3AoKTogdm9pZCB7XHJcbiAgICAgICAgdGhpcy50cmlhbmdsZS5kcm9wKCk7XHJcbiAgICB9XHJcblxyXG4gICAgLyoqXHJcbiAgICAgKiBSZXBsYWNlcyB0aGUgdmFsdWVzIG9mIHRoZSB7QEluZ2FtZVRyaWFuZ2xlJ3N9IGF0dHJpYnV0ZXMgd2l0aCB0aGUgdmFsdWVzIGZyb20gdGhlIGdpdmVuIG9uZS5cclxuICAgICAqIE5vdGUgdGhhdCB0aGUgdW5kZXJseWluZyBnZW9tZXRyaWMge0Bjb2RlIFRyaWFuZ2xlfSB3b24ndCBiZSByZXBsYWNlZC5cclxuICAgICAqIEBwYXJhbSByZXBsYWNlbWVudCB3aG9zZSBhdHRyaWJ1dGVzIHNoYWxsIHJlcGxhY2UgdGhlIGN1cnJlbnQgb25lc1xyXG4gICAgICogQHJldHVybiB7QGNvZGUgdHJ1ZX0gaWYgYW55dGhpbmcgd2FzIHVwZGF0ZWQsIG90aGVyaXNlIHtAY29kZSBmYWxzZX1cclxuICAgICAqL1xyXG4gICAgdXBkYXRlQXR0cmlidXRlc1dpdGgoIHJlcGxhY2VtZW50OiBJbmdhbWVUcmlhbmdsZSApOiBib29sZWFuIHtcclxuICAgICAgICBsZXQgY2hhbmdlZDogYm9vbGVhbiA9IGZhbHNlO1xyXG4gICAgICAgIGlmICggdGhpcy5vd25lciAhPSByZXBsYWNlbWVudC5vd25lciApIHsgY2hhbmdlZCA9IHRydWU7IHRoaXMub3duZXIgPSByZXBsYWNlbWVudC5vd25lcjsgfVxyXG4gICAgICAgIGlmICggdGhpcy5iYXNlVmFsdWUgIT0gcmVwbGFjZW1lbnQuYmFzZVZhbHVlICkgeyBjaGFuZ2VkID0gdHJ1ZTsgdGhpcy5iYXNlVmFsdWUgPSByZXBsYWNlbWVudC5iYXNlVmFsdWU7IH1cclxuICAgICAgICBpZiAoIHRoaXMuZGVmZW5zZSAhPSByZXBsYWNlbWVudC5kZWZlbnNlICkgeyBjaGFuZ2VkID0gdHJ1ZTsgdGhpcy5kZWZlbnNlID0gcmVwbGFjZW1lbnQuZGVmZW5zZTsgfVxyXG4gICAgICAgIHJldHVybiBjaGFuZ2VkO1xyXG4gICAgfVxyXG5cclxuICAgIGVxdWFscyggb3RoZXI6IGFueSApOiBib29sZWFuIHtcclxuICAgICAgICBpZiAoIG90aGVyIGluc3RhbmNlb2YgSW5nYW1lVHJpYW5nbGUgKSB7XHJcbiAgICAgICAgICAgIHJldHVybiAoIHRoaXMudHJpYW5nbGUuZXF1YWxzKCBvdGhlci50cmlhbmdsZSApICYmIHRoaXMub3duZXIgPT09IG90aGVyLm93bmVyICk7XHJcbiAgICAgICAgfSBlbHNlIHJldHVybiBmYWxzZTtcclxuICAgIH1cclxuXHJcbiAgICB0b1N0cmluZygpOiBzdHJpbmcge1xyXG4gICAgICAgIHJldHVybiB0aGlzLm93bmVyICsgXCI6IFwiICsgdGhpcy50cmlhbmdsZTtcclxuICAgIH1cclxufVxyXG5cclxuLyoqXHJcbiAqIEhvbGRzIGFuIHtAY29kZSBJbmdhbWVUcmlhbmdsZX0gYXMgd2VsbCBhcyBhZGRpdGlvbmFsIGluZm9zIHJlcXVpcmVkIHRvIGRpc3BsYXkgbGl2ZSBmZWVkYmFjay5cclxuICovXHJcbmV4cG9ydCBjbGFzcyBQcmV2aWV3VHJpYW5nbGUgaW1wbGVtZW50cyBTdGFuZGFyZE9iamVjdCB7XHJcbiAgICBwcml2YXRlIF9pbmdhbWVUcmlhbmdsZTogSW5nYW1lVHJpYW5nbGU7XHJcbiAgICBwcml2YXRlIF9jb2xsaXNpb25Db3N0czogbnVtYmVyID0gMDtcclxuICAgIC8qKk5vdCB0aGUgcmVhbCBkZWZlbnNlIG9mIHRoZSB7QGNvZGUgSW5nYW1lVHJpYW5nbGV9IGJ1dCB0aGUgZGVmZW5zZSBvZiB0aGUgdHJpYW5nbGUgd2hpY2ggbWlnaHQgcHJvcG9zZWQgdG8gdGhlIHNlcnZlciovXHJcbiAgICBwcml2YXRlIF9kZWZlbnNlOiBudW1iZXI7XHJcbiAgICBnZXQgaW5nYW1lVHJpYW5nbGUoKTogSW5nYW1lVHJpYW5nbGUgeyByZXR1cm4gdGhpcy5faW5nYW1lVHJpYW5nbGU7IH1cclxuICAgIC8vICAgIHNldCBpbmdhbWVUcmlhbmdsZSggaW5nYW1lVHJpYW5nbGU6IEluZ2FtZVRyaWFuZ2xlICkgeyB0aGlzLl9pbmdhbWVUcmlhbmdsZSA9IGluZ2FtZVRyaWFuZ2xlOyB9XHJcbiAgICBnZXQgY29sbGlzaW9uQ29zdHMoKTogbnVtYmVyIHsgcmV0dXJuIHRoaXMuX2NvbGxpc2lvbkNvc3RzOyB9XHJcbiAgICBzZXQgY29sbGlzaW9uQ29zdHMoIGNvbGxpc2lvbkNvc3RzOiBudW1iZXIgKSB7IHRoaXMuX2NvbGxpc2lvbkNvc3RzID0gY29sbGlzaW9uQ29zdHM7IH1cclxuICAgIGdldCB0cmlhbmdsZSgpOiBUcmlhbmdsZSB7IHJldHVybiB0aGlzLl9pbmdhbWVUcmlhbmdsZS50cmlhbmdsZTsgfVxyXG4gICAgZ2V0IGRlZmVuc2UoKTogbnVtYmVyIHsgcmV0dXJuIHRoaXMuX2RlZmVuc2U7IH1cclxuICAgIHNldCBkZWZlbnNlKCBkZWZlbnNlVmFsdWU6IG51bWJlciApIHsgdGhpcy5fZGVmZW5zZSA9IGRlZmVuc2VWYWx1ZTsgfVxyXG4gICAgZ2V0IGJhc2VWYWx1ZSgpOiBudW1iZXIgeyByZXR1cm4gdGhpcy5faW5nYW1lVHJpYW5nbGUuYmFzZVZhbHVlOyB9XHJcbiAgICBnZXQgb3duZXIoKTogc3RyaW5nIHsgcmV0dXJuIHRoaXMuX2luZ2FtZVRyaWFuZ2xlLm93bmVyIH07XHJcbiAgICAvLyAgICBzZXQgYmFzZVZhbHVlKCBiYXNlVmFsdWU6IG51bWJlciApIHsgdGhpcy5faW5nYW1lVHJpYW5nbGUuYmFzZVZhbHVlID0gYmFzZVZhbHVlOyB9XHJcblxyXG4gICAgY29uc3RydWN0b3IoIGluZ2FtZVRyaWFuZ2xlOiBJbmdhbWVUcmlhbmdsZSApIHtcclxuICAgICAgICB0aGlzLl9pbmdhbWVUcmlhbmdsZSA9IGluZ2FtZVRyaWFuZ2xlO1xyXG4gICAgICAgIHRoaXMuX2RlZmVuc2UgPSBpbmdhbWVUcmlhbmdsZS5kZWZlbnNlO1xyXG4gICAgfVxyXG5cclxuICAgIGVxdWFscyggb2JqOiBvYmplY3QgKTogYm9vbGVhbiB7XHJcbiAgICAgICAgaWYgKCAhKCBvYmogaW5zdGFuY2VvZiBQcmV2aWV3VHJpYW5nbGUgKSApIHJldHVybiBmYWxzZTtcclxuICAgICAgICBpZiAoIHRoaXMgPT0gb2JqICkgcmV0dXJuIHRydWU7XHJcbiAgICAgICAgcmV0dXJuIHRoaXMuX2luZ2FtZVRyaWFuZ2xlLmVxdWFscyggb2JqLl9pbmdhbWVUcmlhbmdsZSApO1xyXG4gICAgfVxyXG5cclxuICAgIHRvU3RyaW5nKCk6IHN0cmluZyB7XHJcbiAgICAgICAgcmV0dXJuIHRoaXMuX2luZ2FtZVRyaWFuZ2xlLnRvU3RyaW5nKCk7XHJcbiAgICB9XHJcbn1cclxuXHJcblxuXG5cbi8vIFdFQlBBQ0sgRk9PVEVSIC8vXG4vLyAuL3R5cGVzY3JpcHQvYm9hcmRfaGFuZGxpbmcvZ2FtZV9vYmplY3RzLnRzIiwiLypcclxuICogSGFuZGxlcyB0aGUgKGRlLSlzZXJpYWxpemF0aW9uIG9mIG9iamVjdHMuXHJcbiAqL1xyXG5cclxuLypcclxuICogT0JKRUNUIFNFUklBTElaQVRJT05cclxuICovXHJcblxyXG5pbXBvcnQgeyBWZXJ0ZXgsIFRyaWFuZ2xlIH0gZnJvbSBcIi4uL2xvZ2ljL2dlb21ldHJpY19vYmplY3RzXCI7XHJcbmltcG9ydCB7IEluZ2FtZVRyaWFuZ2xlIH0gZnJvbSBcIi4uL2JvYXJkX2hhbmRsaW5nL2dhbWVfb2JqZWN0c1wiO1xyXG5cclxuZXhwb3J0IGNsYXNzIFZlcnRleEJlYW4ge1xyXG4gICAgcHVibGljIHg6IG51bWJlcjtcclxuICAgIHB1YmxpYyB5OiBudW1iZXI7XHJcbiAgICBwdWJsaWMgcGxheWVyOiBzdHJpbmc7XHJcbiAgICBjb25zdHJ1Y3RvciggeDogbnVtYmVyLCB5OiBudW1iZXIsIHBsYXllcjogc3RyaW5nICkge1xyXG4gICAgICAgIHRoaXMueCA9IHg7XHJcbiAgICAgICAgdGhpcy55ID0geTtcclxuICAgICAgICB0aGlzLnBsYXllciA9IHBsYXllcjtcclxuICAgIH1cclxuXHJcbiAgICBwdWJsaWMgc3RhdGljIG9mKCB0b0NvbnZlcnQ6IFZlcnRleCwgcGxheWVyOiBzdHJpbmcgKTogVmVydGV4QmVhbiB7XHJcbiAgICAgICAgcmV0dXJuIG5ldyBWZXJ0ZXhCZWFuKCB0b0NvbnZlcnQueCwgdG9Db252ZXJ0LnksIHBsYXllciApO1xyXG4gICAgfVxyXG5cclxuICAgIHB1YmxpYyBzdGF0aWMgYXNWZXJ0ZXgoIHRvQ29udmVydDogVmVydGV4QmVhbiB8IGFueSApOiBWZXJ0ZXgge1xyXG4gICAgICAgIGxldCB4LCB5OiBudW1iZXI7XHJcbiAgICAgICAgaWYgKCB0b0NvbnZlcnQgaW5zdGFuY2VvZiBWZXJ0ZXhCZWFuICkge1xyXG4gICAgICAgICAgICB4ID0gdG9Db252ZXJ0Lng7XHJcbiAgICAgICAgICAgIHkgPSB0b0NvbnZlcnQueTtcclxuICAgICAgICB9IGVsc2UgeyAvLyB3ZWxsIC4uLlxyXG4gICAgICAgICAgICB4ID0gdG9Db252ZXJ0Lng7XHJcbiAgICAgICAgICAgIHkgPSB0b0NvbnZlcnQueTtcclxuICAgICAgICB9XHJcbiAgICAgICAgcmV0dXJuIG5ldyBWZXJ0ZXgoIHgsIHkgKTtcclxuICAgIH1cclxufVxyXG5cclxuZXhwb3J0IGNsYXNzIFRyaWFuZ2xlQmVhbiB7XHJcbiAgICBwdWJsaWMgcG9pbnQxOiBWZXJ0ZXhCZWFuO1xyXG4gICAgcHVibGljIHBvaW50MjogVmVydGV4QmVhbjtcclxuICAgIHB1YmxpYyBwb2ludDM6IFZlcnRleEJlYW47XHJcbiAgICBwdWJsaWMgb3duZXI6IHN0cmluZztcclxuICAgIC8qKiBUaGUgYmFzZSB2YWx1ZSB0byBjYWxjdWxhdGUgZWZmZWN0IGltcGFjdHMgd2l0aCAqL1xyXG4gICAgcHVibGljIGJhc2VWYWx1ZTogbnVtYmVyO1xyXG4gICAgLyoqVGhlIGRlZmVuc2UgdmFsdWUgd2hpY2ggaW5jcmVhc2VzIHRoZSBjb3N0cyBvZiBkZXN0cnVjdGlvbiovXHJcbiAgICBwdWJsaWMgZGVmZW5zZTtcclxuXHJcbiAgICBjb25zdHJ1Y3RvciggcG9pbnQxOiBWZXJ0ZXhCZWFuLCBwb2ludDI6IFZlcnRleEJlYW4sIHBvaW50MzogVmVydGV4QmVhbiwgYmFzZVZhbHVlOiBudW1iZXIsIGRlZmVuc2U6IG51bWJlciwgb3duZXI6IHN0cmluZyApIHtcclxuICAgICAgICB0aGlzLnBvaW50MSA9IHBvaW50MTtcclxuICAgICAgICB0aGlzLnBvaW50MiA9IHBvaW50MjtcclxuICAgICAgICB0aGlzLnBvaW50MyA9IHBvaW50MztcclxuICAgICAgICB0aGlzLm93bmVyID0gb3duZXI7XHJcbiAgICAgICAgdGhpcy5kZWZlbnNlID0gZGVmZW5zZTtcclxuICAgICAgICB0aGlzLmJhc2VWYWx1ZSA9IGJhc2VWYWx1ZTtcclxuICAgIH1cclxuXHJcbiAgICBwdWJsaWMgc3RhdGljIG9mKCB0b0NvbnZlcnQ6IEluZ2FtZVRyaWFuZ2xlIHwgYW55ICk6IFRyaWFuZ2xlQmVhbiB7XHJcbiAgICAgICAgbGV0IG93bmVyOiBzdHJpbmcgPSB0b0NvbnZlcnQub3duZXI7XHJcbiAgICAgICAgbGV0IHZlcnRpY2VzOiBbVmVydGV4LCBWZXJ0ZXgsIFZlcnRleF0gPSB0b0NvbnZlcnQgaW5zdGFuY2VvZiBJbmdhbWVUcmlhbmdsZSA/XHJcbiAgICAgICAgICAgIHRvQ29udmVydC50cmlhbmdsZS52ZXJ0aWNlcyA6IFt0b0NvbnZlcnQucG9pbnQxLCB0b0NvbnZlcnQucG9pbnQyLCB0b0NvbnZlcnQucG9pbnQzXTtcclxuICAgICAgICByZXR1cm4gbmV3IFRyaWFuZ2xlQmVhbihcclxuICAgICAgICAgICAgVmVydGV4QmVhbi5vZiggdmVydGljZXNbMF0sIG93bmVyICksXHJcbiAgICAgICAgICAgIFZlcnRleEJlYW4ub2YoIHZlcnRpY2VzWzFdLCBvd25lciApLFxyXG4gICAgICAgICAgICBWZXJ0ZXhCZWFuLm9mKCB2ZXJ0aWNlc1syXSwgb3duZXIgKSwgdG9Db252ZXJ0LmJhc2VWYWx1ZSwgdG9Db252ZXJ0LmRlZmVuc2UsIG93bmVyICk7XHJcbiAgICB9XHJcblxyXG4gICAgcHVibGljIHN0YXRpYyBhc0luZ2FtZVRyaWFuZ2xlKCB0b0NvbnZlcnQ6IFRyaWFuZ2xlQmVhbiB8IGFueSApOiBJbmdhbWVUcmlhbmdsZSB7XHJcbiAgICAgICAgbGV0IHYxLCB2MiwgdjM6IFZlcnRleDtcclxuICAgICAgICBsZXQgcGxheWVyOiBzdHJpbmc7XHJcbiAgICAgICAgaWYgKCB0b0NvbnZlcnQgaW5zdGFuY2VvZiBUcmlhbmdsZUJlYW4gKSB7XHJcbiAgICAgICAgICAgIHYxID0gVmVydGV4QmVhbi5hc1ZlcnRleCggdG9Db252ZXJ0LnBvaW50MSApO1xyXG4gICAgICAgICAgICB2MiA9IFZlcnRleEJlYW4uYXNWZXJ0ZXgoIHRvQ29udmVydC5wb2ludDIgKTtcclxuICAgICAgICAgICAgdjMgPSBWZXJ0ZXhCZWFuLmFzVmVydGV4KCB0b0NvbnZlcnQucG9pbnQzICk7XHJcbiAgICAgICAgICAgIHBsYXllciA9IHRvQ29udmVydC5vd25lcjtcclxuICAgICAgICB9IGVsc2UgeyAvL1RPRE8gd2VsbCAuLi5cclxuICAgICAgICAgICAgdjEgPSBWZXJ0ZXhCZWFuLmFzVmVydGV4KCB0b0NvbnZlcnQucG9pbnQxICk7XHJcbiAgICAgICAgICAgIHYyID0gVmVydGV4QmVhbi5hc1ZlcnRleCggdG9Db252ZXJ0LnBvaW50MiApO1xyXG4gICAgICAgICAgICB2MyA9IFZlcnRleEJlYW4uYXNWZXJ0ZXgoIHRvQ29udmVydC5wb2ludDMgKTtcclxuICAgICAgICAgICAgcGxheWVyID0gdG9Db252ZXJ0LnBsYXllcjtcclxuICAgICAgICB9XHJcbiAgICAgICAgbGV0IHRyaWFuZ2xlID0gVHJpYW5nbGUub2YoIHYxLCB2MiwgdjMgKTtcclxuICAgICAgICBsZXQgaW5nVHJpYW5nbGUgPSBJbmdhbWVUcmlhbmdsZS5vZiggdHJpYW5nbGUsIHBsYXllciApO1xyXG4gICAgICAgIGluZ1RyaWFuZ2xlLmJhc2VWYWx1ZSA9IHRvQ29udmVydC5iYXNlVmFsdWU7XHJcbiAgICAgICAgaW5nVHJpYW5nbGUuZGVmZW5zZSA9IHRvQ29udmVydC5kZWZlbnNlO1xyXG4gICAgICAgIHJldHVybiBpbmdUcmlhbmdsZTtcclxuICAgIH1cclxufVxyXG5cclxuXHJcbi8qXHJcbiAqIFNFUlZFUiBDT01NQU5EUyBcclxuICovXHJcblxuZXhwb3J0IGludGVyZmFjZSBTZXJ2ZXJDb21tYW5kIHsgfVxyXG5cclxuLyoqXHJcbiAqIFJlcHJlc2VudHMgYSBsaXN0IG9mIGFjdGlvbnMgaXNzdWVkIGJ5IHRoZSBzZXJ2ZXIuXHJcbiAqL1xyXG5leHBvcnQgY2xhc3MgU2VydmVyVXBkYXRlIGltcGxlbWVudHMgU2VydmVyQ29tbWFuZCB7XHJcbiAgICBwcml2YXRlIF90cmlhbmdsZXNUb1JlbW92ZTogVHJpYW5nbGVCZWFuW107XHJcbiAgICBwcml2YXRlIF90cmlhbmdsZXNUb0FkZDogVHJpYW5nbGVCZWFuW107XHJcblxyXG4gICAgZ2V0IHRyaWFuZ2xlc1RvQWRkKCkgeyByZXR1cm4gdGhpcy5fdHJpYW5nbGVzVG9BZGQ7IH1cclxuICAgIHNldCB0cmlhbmdsZXNUb0FkZCggdHJpYW5nbGVzVG9BZGQ6IFRyaWFuZ2xlQmVhbltdICkgeyB0aGlzLl90cmlhbmdsZXNUb0FkZCA9IHRyaWFuZ2xlc1RvQWRkOyB9XHJcbiAgICBnZXQgdHJpYW5nbGVzVG9SZW1vdmUoKSB7IHJldHVybiB0aGlzLl90cmlhbmdsZXNUb1JlbW92ZTsgfVxyXG4gICAgc2V0IHRyaWFuZ2xlc1RvUmVtb3ZlKCB0cmlhbmdsZXNUb1JlbW92ZTogVHJpYW5nbGVCZWFuW10gKSB7IHRoaXMuX3RyaWFuZ2xlc1RvUmVtb3ZlID0gdHJpYW5nbGVzVG9SZW1vdmU7IH1cclxuXHJcbiAgICAvKipcclxuICAgICAqIFJldHVybnMgYSBKU09OIHZpZXcgb2YgdGhpcyBvYmplY3QuXHJcbiAgICAgKiBEdWUgdG8gaXNzdWVzIHdpdGggdGhlIHByaXZhdGUgbmFtZSBmaWVsZHMgdGhpcyBtZXRob2QgbXVzdCBiZSB1c2VkIHRvIHNlcmlhbGl6ZSBhIHtAY29kZSBTZXJ2ZXJVcGRhdGV9IGFzIEpTT04uXHJcbiAgICAgKiBAcmV0dXJuIGEgSlNPTiB2aWV3IG9mIHRoaXMgb2JqZWN0LiBOb3RlIHRoYXQgbW9kaWZpY2F0aW9ucyBvbiB0aGlzIHZpZXcgd29uJ3QgYWZmZWN0IHRoZSB1bmRlcmx5aW5nIHtAY29kZSBTZXJ2ZXJVcGRhdGV9IG9iamVjdC5cclxuICAgICAqL1xyXG4gICAgLy9UT0RPIGlzIHRoaXMgbWV0aG9kIGV2ZW4gcmVxdWlyZWQ/XHJcbiAgICBwdWJsaWMgdG9Kc29uKCkge1xyXG4gICAgICAgIHJldHVybiB7XHJcbiAgICAgICAgICAgIHRyaWFuZ2xlc1RvUmVtb3ZlOiB0aGlzLl90cmlhbmdsZXNUb1JlbW92ZSxcclxuICAgICAgICAgICAgdHJpYW5nbGVzVG9BZGQ6IHRoaXMuX3RyaWFuZ2xlc1RvQWRkXHJcbiAgICAgICAgfTtcclxuICAgIH1cclxuXHJcbiAgICAvKipcclxuICAgICAqIENyZWF0ZXMgYSBuZXcge0Bjb2RlIFNlcnZlclVwZGF0ZX0gYmFzZWQgb24gYSBKU09OIG9iamVjdC5cclxuICAgICAqIEBwYXJhbSB0b0NvbnZlcnQgaG9sZHMgdGhlIGluZm9ybWF0aW9uIG9mIHRoZSB7QGNvZGUgU2VydmVyVXBkYXRlfSB0byBiZSBjcmVhdGVkXHJcbiAgICAgKi9cclxuICAgIHB1YmxpYyBzdGF0aWMgZnJvbUpzb24oIHRvQ29udmVydDogYW55ICk6IFNlcnZlclVwZGF0ZSB7XHJcbiAgICAgICAgbGV0IHRvUmV0dXJuOiBTZXJ2ZXJVcGRhdGUgPSBuZXcgU2VydmVyVXBkYXRlKCk7XHJcbiAgICAgICAgbGV0IHRvUmVtb3ZlOiBUcmlhbmdsZUJlYW5bXSA9IHRvQ29udmVydC50cmlhbmdsZXNUb1JlbW92ZS5tYXAoIHQgPT4gVHJpYW5nbGVCZWFuLm9mKCB0ICkgKTtcclxuICAgICAgICBsZXQgdG9BZGQ6IFRyaWFuZ2xlQmVhbltdID0gdG9Db252ZXJ0LnRyaWFuZ2xlc1RvQWRkLm1hcCggdCA9PiBUcmlhbmdsZUJlYW4ub2YoIHQgKSApO1xyXG4gICAgICAgIHRvUmV0dXJuLnRyaWFuZ2xlc1RvUmVtb3ZlID0gdG9SZW1vdmU7XHJcbiAgICAgICAgdG9SZXR1cm4udHJpYW5nbGVzVG9BZGQgPSB0b0FkZDtcclxuICAgICAgICByZXR1cm4gdG9SZXR1cm47XHJcbiAgICB9XHJcbn1cclxuXHJcbi8qKlxyXG4gKiBIb2xkcyB0aGUgY29tcGxldGUgc3RhdGUgb2YgdGhlIHNlcnZlciBhbmQgYWxsb3dzIHRvIHJlLWJ1aWxkIHRoZSBjbGllbnQncyBzdGF0ZS5cclxuICovXHJcbmV4cG9ydCBjbGFzcyBSZXNldFN0YXRlIGltcGxlbWVudHMgU2VydmVyQ29tbWFuZCB7XHJcblxyXG4gICAgcHJpdmF0ZSBfdHJpYW5nbGVzVG9DcmVhdGU6IFRyaWFuZ2xlQmVhbltdO1xyXG4gICAgZ2V0IHRyaWFuZ2xlc1RvQ3JlYXRlKCk6IFRyaWFuZ2xlQmVhbltdIHsgcmV0dXJuIHRoaXMuX3RyaWFuZ2xlc1RvQ3JlYXRlOyB9XHJcblxyXG4gICAgc2V0IHRyaWFuZ2xlc1RvQ3JlYXRlKCB0cmlhbmdsZXNUb0NyZWF0ZTogVHJpYW5nbGVCZWFuW10gKSB7XHJcbiAgICAgICAgdGhpcy5fdHJpYW5nbGVzVG9DcmVhdGUgPSB0cmlhbmdsZXNUb0NyZWF0ZTtcclxuICAgIH1cclxuXHJcbiAgICBwdWJsaWMgc3RhdGljIGZyb21Kc29uKCB0b0NvbnZlcnQ6IGFueSApOiBSZXNldFN0YXRlIHtcclxuICAgICAgICBsZXQgdG9SZXR1cm46IFJlc2V0U3RhdGUgPSBuZXcgUmVzZXRTdGF0ZSgpO1xyXG4gICAgICAgIHRvUmV0dXJuLnRyaWFuZ2xlc1RvQ3JlYXRlID0gdG9Db252ZXJ0LnRyaWFuZ2xlc1RvQ3JlYXRlLm1hcCggdCA9PiBUcmlhbmdsZUJlYW4ub2YoIHQgKSApO1xyXG4gICAgICAgIHJldHVybiB0b1JldHVybjtcclxuICAgIH1cclxuXHJcbiAgICBwdWJsaWMgdG9Kc29uKCkge1xyXG4gICAgICAgIHJldHVybiB7XHJcbiAgICAgICAgICAgIHRyaWFuZ2xlc1RvQ3JlYXRlOiB0aGlzLl90cmlhbmdsZXNUb0NyZWF0ZSxcclxuICAgICAgICAgICAgaW5jcmVtZW50YWxVcGRhdGU6IGZhbHNlXHJcbiAgICAgICAgfTtcclxuICAgIH1cclxufVxyXG5cclxuLyoqXHJcbiAqIEhvbGRzIHRoZSBhY2tub3dsZWRnZWQgcGxheWVyJ3MgbmFtZS5cclxuICovXHJcbmV4cG9ydCBjbGFzcyBJbml0SW5mbyBpbXBsZW1lbnRzIFNlcnZlckNvbW1hbmQge1xyXG5cclxuICAgIHByaXZhdGUgX3BsYXllcklkOiBzdHJpbmc7XHJcbiAgICBwcml2YXRlIF9pbml0aWFsU3RhdGU6IFJlc2V0U3RhdGU7XHJcbiAgICBnZXQgcGxheWVySWQoKTogc3RyaW5nIHsgcmV0dXJuIHRoaXMuX3BsYXllcklkOyB9XHJcbiAgICBzZXQgcGxheWVySWQoIHBsYXllcklkOiBzdHJpbmcgKSB7IHRoaXMuX3BsYXllcklkID0gcGxheWVySWQ7IH1cclxuICAgIGdldCBpbml0aWFsU3RhdGUoKTogUmVzZXRTdGF0ZSB7IHJldHVybiB0aGlzLl9pbml0aWFsU3RhdGU7IH1cclxuICAgIHNldCBpbml0aWFsU3RhdGUoIGluaXRpYWxTdGF0ZTogUmVzZXRTdGF0ZSApIHsgdGhpcy5faW5pdGlhbFN0YXRlID0gaW5pdGlhbFN0YXRlOyB9XHJcblxyXG4gICAgcHVibGljIHN0YXRpYyBmcm9tSnNvbiggdG9Db252ZXJ0OiBhbnkgKTogSW5pdEluZm8ge1xyXG4gICAgICAgIGxldCB0b1JldHVybjogSW5pdEluZm8gPSBuZXcgSW5pdEluZm8oKTtcclxuICAgICAgICB0b1JldHVybi5wbGF5ZXJJZCA9IHRvQ29udmVydC5wbGF5ZXJJZDtcclxuICAgICAgICB0b1JldHVybi5pbml0aWFsU3RhdGUgPSBSZXNldFN0YXRlLmZyb21Kc29uKCB0b0NvbnZlcnQuaW5pdGlhbFN0YXRlICk7XHJcbiAgICAgICAgcmV0dXJuIHRvUmV0dXJuO1xyXG4gICAgfVxyXG5cclxuICAgIHB1YmxpYyB0b0pzb24oKSB7XHJcbiAgICAgICAgcmV0dXJuIHtcclxuICAgICAgICAgICAgbmFtZTogdGhpcy5wbGF5ZXJJZFxyXG4gICAgICAgIH07XHJcbiAgICB9XHJcbn1cclxuXHJcbi8qKlxyXG4gKiBUZWxscyBhIHBsYXllcidzIGFjY291bnQgYmFsYW5jZS5cclxuICovXHJcbmV4cG9ydCBjbGFzcyBBY2NvdW50SW5mbyBpbXBsZW1lbnRzIFNlcnZlckNvbW1hbmQge1xyXG5cclxuICAgIHByaXZhdGUgX2JhbGFuY2U6IG51bWJlcjtcclxuICAgIGdldCBiYWxhbmNlKCk6IG51bWJlciB7IHJldHVybiB0aGlzLl9iYWxhbmNlOyB9XHJcbiAgICBzZXQgYmFsYW5jZSggYmFsYW5jZTogbnVtYmVyICkgeyB0aGlzLl9iYWxhbmNlID0gYmFsYW5jZTsgfVxyXG5cclxuICAgIHB1YmxpYyBzdGF0aWMgZnJvbUpzb24oIHRvQ29udmVydDogYW55ICk6IEFjY291bnRJbmZvIHtcclxuICAgICAgICBsZXQgdG9SZXR1cm46IEFjY291bnRJbmZvID0gbmV3IEFjY291bnRJbmZvKCk7XHJcbiAgICAgICAgdG9SZXR1cm4uYmFsYW5jZSA9IHRvQ29udmVydC5iYWxhbmNlO1xyXG4gICAgICAgIHJldHVybiB0b1JldHVybjtcclxuICAgIH1cclxuXHJcbiAgICBwdWJsaWMgdG9Kc29uKCkge1xyXG4gICAgICAgIHJldHVybiB7XHJcbiAgICAgICAgICAgIGJhbGFuY2U6IHRoaXMuYmFsYW5jZVxyXG4gICAgICAgIH07XHJcbiAgICB9XHJcbn1cblxuXG4vLyBXRUJQQUNLIEZPT1RFUiAvL1xuLy8gLi90eXBlc2NyaXB0L25ldHdvcmsvbmV0d29ya19iZWFucy50cyIsIlxyXG4vL2V4cG9ydCBlbnVtIENvbG9yIHtcclxuLy8gICAgUkVEID0gXCIjZmYwMDAwXCIsIEJMQUNLID0gXCIjMDAwMDAwXCJcclxuLy99XHJcblxyXG5cclxuXHJcbmZ1bmN0aW9uIGNyZWF0ZU9wYWNpdHlTdGVwcyggcmdiQ29sb3I6IHN0cmluZyApOiBzdHJpbmdbXSB7XHJcbiAgICBsZXQgdG9SZXR1cm46IHN0cmluZ1tdID0gbmV3IEFycmF5KCk7XHJcbiAgICBsZXQgcmdiYSA9IHJnYkNvbG9yLnJlcGxhY2UoIFwicmdiXCIsIFwicmdiYVwiICk7XHJcbiAgICBmb3IgKCBsZXQgYWxwaGEgPSAwOyBhbHBoYSA8PSAxMDsgYWxwaGEgKz0gMC4xMCApIHtcclxuICAgICAgICB0b1JldHVybi5wdXNoKCByZ2JhLnJlcGxhY2UoIFwiKVwiLCBcIixcIiArIFN0cmluZyggYWxwaGEgKSArIFwiKVwiICkgKTtcclxuICAgIH1cclxuICAgIHJldHVybiB0b1JldHVybjtcclxufVxyXG5cclxubGV0IGdyZWVuUkdCID0gXCJyZ2IoMCwxMjgsMClcIjtcclxubGV0IHJlZFJHQiA9IFwicmdiKDIyMCwyMCw2MClcIjtcclxubGV0IGJsYWNrUkdCID0gXCJyZ2IoMCwwLDApXCI7XHJcblxyXG5jb25zdCBncmVlblN0ZXBzOiBzdHJpbmdbXSA9IGNyZWF0ZU9wYWNpdHlTdGVwcyggZ3JlZW5SR0IgKTtcclxuY29uc3QgcmVkU3RlcHM6IHN0cmluZ1tdID0gY3JlYXRlT3BhY2l0eVN0ZXBzKCByZWRSR0IgKTtcclxuY29uc3QgYmxhY2tTdGVwczogc3RyaW5nW10gPSBjcmVhdGVPcGFjaXR5U3RlcHMoIGJsYWNrUkdCICk7XHJcblxyXG5cclxuZXhwb3J0IGNsYXNzIENvbG9yIHtcclxuICAgIHByaXZhdGUgX2NvbG9yczogc3RyaW5nW107XHJcbiAgICB2YWwoIHN0cmVuZ3RoPzogbnVtYmVyICk6IHN0cmluZyB7IHJldHVybiBzdHJlbmd0aCAhPSBudWxsICYmIHN0cmVuZ3RoICE9IHVuZGVmaW5lZCAmJiBzdHJlbmd0aCA8IHRoaXMuX2NvbG9ycy5sZW5ndGggPyB0aGlzLl9jb2xvcnNbc3RyZW5ndGhdIDogdGhpcy5fY29sb3JzW3RoaXMuX2NvbG9ycy5sZW5ndGggLSAxXTsgfVxyXG4gICAgY29uc3RydWN0b3IoIGNvbG9yczogc3RyaW5nW10gKSB7XHJcbiAgICAgICAgdGhpcy5fY29sb3JzID0gY29sb3JzO1xyXG4gICAgfVxyXG59XHJcbmV4cG9ydCBjb25zdCBHUkVFTjogQ29sb3IgPSBuZXcgQ29sb3IoIGdyZWVuU3RlcHMgKTtcclxuZXhwb3J0IGNvbnN0IFJFRDogQ29sb3IgPSBuZXcgQ29sb3IoIHJlZFN0ZXBzICk7XHJcbmV4cG9ydCBjb25zdCBCTEFDSzogQ29sb3IgPSBuZXcgQ29sb3IoIGJsYWNrU3RlcHMgKTtcclxuXG5cblxuLy8gV0VCUEFDSyBGT09URVIgLy9cbi8vIC4vdHlwZXNjcmlwdC91dGlsL2NvbG9ycy50cyIsImV4cG9ydCBpbnRlcmZhY2UgU3RhbmRhcmRPYmplY3Qge1xyXG4gICAgdG9TdHJpbmcoKTogc3RyaW5nO1xyXG4gICAgZXF1YWxzKCBvYmo6IG9iamVjdCApOiBib29sZWFuO1xyXG59XHJcblxyXG5leHBvcnQgZnVuY3Rpb24gcmVtb3ZlRnJvbUxpc3QoIG9iamVjdDogU3RhbmRhcmRPYmplY3QsIGxpc3Q6IFN0YW5kYXJkT2JqZWN0W10gKSB7XHJcbiAgICBmb3IgKCBsZXQgaSA9IDA7IGkgPCBsaXN0Lmxlbmd0aDsgaSsrICkge1xyXG4gICAgICAgIGxldCBlcXVhbCA9IGxpc3RbaV0uZXF1YWxzKCBvYmplY3QgKTtcclxuICAgICAgICBpZiAoIGVxdWFsICkge1xyXG4gICAgICAgICAgICBsaXN0LnNwbGljZSggaSwgMSApO1xyXG4gICAgICAgIH1cclxuICAgIH1cclxufVxyXG5cclxuZXhwb3J0IGZ1bmN0aW9uIG1pbiggLi4udmFsdWVzOiBudW1iZXJbXSApOiBudW1iZXIge1xyXG4gICAgbGV0IG1pbjogbnVtYmVyID0gbnVsbDtcclxuICAgIGZvciAoIGxldCB2YWx1ZSBvZiB2YWx1ZXMgKSB7XHJcbiAgICAgICAgbWluID0gbWluID09IG51bGwgPyB2YWx1ZSA6IE1hdGgubWluKCBtaW4sIHZhbHVlICk7XHJcbiAgICB9XHJcbiAgICByZXR1cm4gbWluO1xyXG59XHJcblxyXG5leHBvcnQgZnVuY3Rpb24gbWF4KCAuLi52YWx1ZXM6IG51bWJlcltdICk6IG51bWJlciB7XHJcbiAgICBsZXQgbWF4OiBudW1iZXIgPSBudWxsO1xyXG4gICAgZm9yICggdmFyIHZhbHVlIG9mIHZhbHVlcyApIHtcclxuICAgICAgICBtYXggPSBtYXggPSBudWxsID8gdmFsdWUgOiBNYXRoLm1heCggbWF4LCB2YWx1ZSApO1xyXG4gICAgfVxyXG4gICAgcmV0dXJuIG1heDtcclxufVxyXG5cblxuXG4vLyBXRUJQQUNLIEZPT1RFUiAvL1xuLy8gLi90eXBlc2NyaXB0L3V0aWwvc2hhcmVkLnRzIiwiLyoqXHJcbiAqIEhvbGRzIGFsbCB0aGUgbG9naWMgcmVsYXRpbmcgcGxheWVycycgcmVzb3VyY2VzIGFuZCBjb3N0cyBvZiBhY3Rpb25zLlxyXG4gKiBCYWxhbmNpbmcgYWRqdXN0bWVudHMgc2hvdWxkIGJlIGRvbmUgaGVyZSAoaW4gaXRzIGJhY2tlbmQgY291bnRlcnBhcnQpLlxyXG4gKi9cclxuXHJcblxyXG4vKiogVXNlZCB0byBzY2FsZSBkb3duIGFsbCB2YWx1ZXMgKi9cclxuaW1wb3J0IHsgSW5nYW1lVHJpYW5nbGUsIFByZXZpZXdUcmlhbmdsZSB9IGZyb20gXCIuLi9ib2FyZF9oYW5kbGluZy9nYW1lX29iamVjdHNcIjtcclxuaW1wb3J0IHsgVHJpYW5nbGUgfSBmcm9tIFwiLi9nZW9tZXRyaWNfb2JqZWN0c1wiO1xyXG5cclxuY29uc3QgQVJFQV9GQUNUT1IgPSAuMDAxO1xyXG5cclxuLyoqXHJcbiAqIFJldHVybnMgdGhlIHZhbHVlIG9mIGFuIHRyaWFuZ2xlLlxyXG4gKlxyXG4gKiBAcGFyYW0gdHJpYW5nbGVcclxuICogICAgICAgICAgICB0byBiZSBldmFsdWF0ZWQuXHJcbiAqIEByZXR1cm4gdGhlIHtAY29kZSBUcmlhbmdsZX0gdmFsdWVcclxuICovXHJcbmV4cG9ydCBmdW5jdGlvbiBnZXRWYWx1ZU9mKCB0cmlhbmdsZTogVHJpYW5nbGUgfCBJbmdhbWVUcmlhbmdsZSB8IFByZXZpZXdUcmlhbmdsZSApOiBudW1iZXIge1xyXG4gICAgbGV0IHQgPSB0cmlhbmdsZSBpbnN0YW5jZW9mIFRyaWFuZ2xlID8gdHJpYW5nbGUgOiB0cmlhbmdsZS50cmlhbmdsZTtcclxuICAgIGxldCB0cmlhbmdsZVByaWNlID0gTWF0aC50cnVuYyggTWF0aC5tYXgoIDEsIHQuZ2V0QXJlYSgpICogQVJFQV9GQUNUT1IgKSApO1xyXG4gICAgcmV0dXJuICggdHJpYW5nbGUgaW5zdGFuY2VvZiBQcmV2aWV3VHJpYW5nbGUgKSA/IHRyaWFuZ2xlLmNvbGxpc2lvbkNvc3RzICsgdHJpYW5nbGVQcmljZSA6IHRyaWFuZ2xlUHJpY2U7XHJcbn1cclxuXHJcbi8qKlxyXG4gKiBSZXR1cm5zIHRoZSBpbmNvbWUgYSBnaXZlbiB0cmlhbmdsZSBzdXBwbGllcy5cclxuICpcclxuICogQHBhcmFtIHRyaWFuZ2xlXHJcbiAqICAgICAgICAgICAgd2hvc2Ugc3VwcGxpZWQgaW5jb21lIGlzIHJlcXVlc3RlZFxyXG4gKiBAcmV0dXJuIHRoZSBzdXBwbGllZCBpbmNvbWVcclxuICovXHJcbmV4cG9ydCBmdW5jdGlvbiBnZXRUdXJuSW5jb21lRm9yKCB0cmlhbmdsZTogSW5nYW1lVHJpYW5nbGUgfCBUcmlhbmdsZSApOiBudW1iZXIge1xyXG4gICAgbGV0IHQgPSB0cmlhbmdsZSBpbnN0YW5jZW9mIEluZ2FtZVRyaWFuZ2xlID8gdHJpYW5nbGUgOiB0cmlhbmdsZS5pbmdhbWVUcmlhbmdsZTtcclxuICAgIC8vIFRPRE8gb25seSAzMiBiaXQgc3VwcG9ydHMgYml0IHNoaWZ0P1xyXG4gICAgLy8gd2lsbCBiZWNvbWUgbW9yZSBjb21wbGV4IHdpdGggbW9yZSBmYWN0b3JzIGxhdGVyIG9uXHJcbiAgICByZXR1cm4gTWF0aC5tYXgoIDEsIHQuYmFzZVZhbHVlID4+IDIgKTsgLy8gYXRtOiAyNSUgb2YgYmFzZSB2YWx1ZVxyXG59XHJcblxyXG4vKipcclxuICogUmV0dXJucyB0aGUgY3JlYXRpb24gY29zdHMgb2YgdGhlIGdpdmVuIHRyaWFuZ2xlLlxyXG4gKlxyXG4gKiBAcGFyYW0gdHJpYW5nbGVcclxuICogICAgICAgICAgICB3aG9zZSBjcmVhdGlvbiBjb3N0cyBzaGFsbCBiZSBkZXRlcm1pbmVkXHJcbiAqIEByZXR1cm4gdGhlIGNyZWF0aW9uIGNvc3RzIG9mIHRoZSBnaXZlbiB0cmlhbmdsZVxyXG4gKi9cclxuZXhwb3J0IGZ1bmN0aW9uIGdldENvc3RzRm9yKCB0cmlhbmdsZTogVHJpYW5nbGUgfCBJbmdhbWVUcmlhbmdsZSB8IFByZXZpZXdUcmlhbmdsZSApOiBudW1iZXIge1xyXG4gICAgcmV0dXJuIGdldFZhbHVlT2YoIHRyaWFuZ2xlICk7XHJcbn1cclxuXHJcbi8qKlxyXG4gKiBSZXR1cm5zIHRoZSBjb3N0cyB0byBkZXN0cm95IGFuIHRyaWFuZ2xlLlxyXG4gKiBAcGFyYW0gdHJpYW5nbGUgd2hvc2UgZGVzdHJ1Y3Rpb24gY29zdHMgc2hhbGwgYmUgZGV0ZXJtaW5lZFxyXG4gKiBAcmV0dXJuIHRoZSBjb3N0cyB0byBkZXN0cnVjdCB0aGUgZ2l2ZW4gdHJpYW5nbGVcclxuICovXHJcbmV4cG9ydCBmdW5jdGlvbiBnZXREZXN0cnVjdGlvbkNvc3RzRm9yKCB0cmlhbmdsZTogSW5nYW1lVHJpYW5nbGUgKTogbnVtYmVyIHtcclxuICAgIHJldHVybiBnZXRWYWx1ZU9mKCB0cmlhbmdsZSApICsgZ2V0RWZmZWN0aXZlRGVmZW5zZSggdHJpYW5nbGUgKTtcclxufVxyXG5cclxuLyoqXHJcbiAqIFJldHVybnMgdGhlIGNvc3RzIHRvIHVwZ3JhZGUgdGhlIGRlZmVuc2Ugb2YgYSBnaXZlbiB7QGNvZGUgSW5nYW1lVHJpYW5nbGV9IGJ5XHJcbiAqIHtAY29kZSAxfS5cclxuICogXHJcbiAqIEBwYXJhbSB0b1VwZ3JhZGVcclxuICogICAgICAgICAgICB3aG9zZSB1cGdyYWRlIGNvc3RzIHNoYWxsIGJlIGRldGVybWluZWRcclxuICogQHJldHVybiB0aGUgY29zdHMgdG8gdXBncmFkZSB0aGUgZGVmZW5zZSBvZiB0aGUgZ2l2ZW4ge0Bjb2RlIEluZ2FtZVRyaWFuZ2xlfVxyXG4gKiAgICAgICAgIGJ5IHtAY29kZSAxfVxyXG4gKi9cclxuZXhwb3J0IGZ1bmN0aW9uIGdldERlZmVuc2VVcGdyYWRlQ29zdHMoIHRvVXBncmFkZTogSW5nYW1lVHJpYW5nbGUgfCBQcmV2aWV3VHJpYW5nbGUgKTogbnVtYmVyIHtcclxuICAgIHJldHVybiB0b1VwZ3JhZGUuYmFzZVZhbHVlICogKCB0b1VwZ3JhZGUuZGVmZW5zZSArIDEgKSAqIDQ7XHJcbn1cclxuXHJcbi8qKlxyXG4gKiBSZXR1cm5zIHRoZSBlZmZlY3RpdmUgZGVmZW5zZSB2YWx1ZSBiYXNlZCBvbiBhIGdpdmVuIHtAY29kZSBJbmdhbWVUcmlhbmdsZSdzfVxyXG4gKiBkZWZlbnNlLlxyXG4gKiBcclxuICogQHBhcmFtIHRyaWFuZ2xlXHJcbiAqICAgICAgICAgICAgaG93c2UgZWZmZWN0aXZlIGRlZmVuc2UgdmFsdWUgc2hhbGwgYmUgZGV0ZXJtaW5lZFxyXG4gKiBAcmV0dXJuIHRoZSBlZmZlY3RpdmUgZGVmZW5zZSB2YWx1ZSBiYXNlZCBvbiB0aGUgZ2l2ZW5cclxuICogICAgICAgICB7QGNvZGUgSW5nYW1lVHJpYW5nbGUnc30gZGVmZW5zZVxyXG4gKi9cclxuZXhwb3J0IGZ1bmN0aW9uIGdldEVmZmVjdGl2ZURlZmVuc2UoIHRyaWFuZ2xlOiBJbmdhbWVUcmlhbmdsZSApOiBudW1iZXIge1xyXG4gICAgcmV0dXJuIHRyaWFuZ2xlLmRlZmVuc2UgKiAxMDtcclxufVxuXG5cbi8vIFdFQlBBQ0sgRk9PVEVSIC8vXG4vLyAuL3R5cGVzY3JpcHQvbG9naWMvYWNjb3VudGFudC50cyIsIi8qXHJcbiAqIFJFU1BPTlNFIEhBTkRMSU5HXHJcbiAqL1xyXG5cclxuLyoqXHJcbiAqIFByb2Nlc3NlcyBhIHNlcnZlciBjb21tYW5kIGZvciBhIGdpdmVuIGNvbW1hbmQgdHlwZS4gXHJcbiAqL1xyXG5pbXBvcnQgeyByZXN1bWUsIGFwcGx5U2VydmVyUmVnaXN0ZXIsIGFwcGx5U2VydmVyUmVzZXQsIGFwcGx5U2VydmVyVXBkYXRlLCBzZXRBY2NvdW50IH0gZnJvbSBcIi4uL3J1bi9tYWluXCI7XHJcbmltcG9ydCB7IEluaXRJbmZvLCBUcmlhbmdsZUJlYW4sIFJlc2V0U3RhdGUsIFNlcnZlclVwZGF0ZSwgQWNjb3VudEluZm8gfSBmcm9tIFwiLi9uZXR3b3JrX2JlYW5zXCI7XHJcbmltcG9ydCB7IEluZ2FtZVRyaWFuZ2xlIH0gZnJvbSBcIi4uL2JvYXJkX2hhbmRsaW5nL2dhbWVfb2JqZWN0c1wiO1xyXG5cclxuZXhwb3J0IGludGVyZmFjZSBTZXJ2ZXJDb21tYW5kSGFuZGxlciB7XHJcbiAgICAvKipSZXR1cm5zIHRoZSB0eXBlIG9mIHRoZSBzZXJ2ZXIgY29tbWFuZC4qL1xyXG4gICAgZ2V0VHlwZSgpOiBDb21tYW5kVHlwZTtcclxuICAgIC8qKlByb2Nlc3NlcyB0aGUgY29tbWFuZCdzIGNvbnRlbnQuKi9cclxuICAgIHByb2Nlc3MoIGNvbW1hbmQ6IGFueSApOiB2b2lkO1xyXG59XHJcblxyXG4vKipcclxuICogUG9zc2libGUgdHlwZXMgb2YgcmVzcG9uc2UgZnJvbSB0aGUgc2VydmVyLiBUZWxscyBob3cgdG8gcGFyc2UgYW5kIHByb2Nlc3MgdGhlIHNlcnZlcidzIHJlc3BvbnNlLiBcclxuICovXHJcbmV4cG9ydCBlbnVtIENvbW1hbmRUeXBlIHtcclxuICAgIFVQREFURSA9IFwiVVBEQVRFXCIsIFJFU0VUID0gXCJSRVNFVFwiLCBJTklUID0gXCJJTklUXCIsIEFDQ09VTlRfSU5GTyA9IFwiQUNDT1VOVF9JTkZPXCJcclxufVxyXG5cclxuLyoqXHJcbiAqIFByb2Nlc3NlcyBhIGFjY291bnQgaW5mb3JhdGlvbiBmcm9tIHRoZSBzZXJ2ZXIuXHJcbiAqL1xyXG5leHBvcnQgY2xhc3MgQWNjb3VudEhhbmRsZXIgaW1wbGVtZW50cyBTZXJ2ZXJDb21tYW5kSGFuZGxlciB7XHJcbiAgICBzdGF0aWMgcmVhZG9ubHkgVFlQRSA9IENvbW1hbmRUeXBlLkFDQ09VTlRfSU5GTztcclxuICAgIHJlYWRvbmx5IGFjY291bnRJbmZvID0gc2V0QWNjb3VudDtcclxuICAgIGdldFR5cGUoKSB7IHJldHVybiBBY2NvdW50SGFuZGxlci5UWVBFIH07XHJcbiAgICBwcm9jZXNzKCBjb21tYW5kOiBhbnkgKSB7XHJcbiAgICAgICAgbGV0IGFjY0luZm86IEFjY291bnRJbmZvID0gQWNjb3VudEluZm8uZnJvbUpzb24oIGNvbW1hbmQgKVxyXG4gICAgICAgIHRoaXMuYWNjb3VudEluZm8uY2FsbCggdGhpcywgYWNjSW5mby5iYWxhbmNlICk7XHJcbiAgICB9XHJcbn1cclxuXHJcbi8qKlxyXG4gKiBQcm9jZXNzZXMgYSB1cGRhdGUgY29tbWFuZCBmcm9tIHRoZSBzZXJ2ZXIuXHJcbiAqL1xyXG5leHBvcnQgY2xhc3MgVXBkYXRlSGFuZGxlciBpbXBsZW1lbnRzIFNlcnZlckNvbW1hbmRIYW5kbGVyIHtcclxuICAgIHN0YXRpYyByZWFkb25seSBUWVBFID0gQ29tbWFuZFR5cGUuVVBEQVRFO1xyXG4gICAgcmVhZG9ubHkgc2VydmVyVXBkYXRlID0gYXBwbHlTZXJ2ZXJVcGRhdGU7XHJcbiAgICBnZXRUeXBlKCkgeyByZXR1cm4gVXBkYXRlSGFuZGxlci5UWVBFIH07XHJcbiAgICBwcm9jZXNzKCBjb21tYW5kOiBhbnkgKSB7XHJcbiAgICAgICAgbGV0IHVwZGF0ZTogU2VydmVyVXBkYXRlID0gU2VydmVyVXBkYXRlLmZyb21Kc29uKCBjb21tYW5kIClcclxuICAgICAgICBsZXQgdG9BZGQ6IEluZ2FtZVRyaWFuZ2xlW10gPSB1cGRhdGUudHJpYW5nbGVzVG9BZGQubWFwKCB0ID0+IFRyaWFuZ2xlQmVhbi5hc0luZ2FtZVRyaWFuZ2xlKCB0ICkgKTtcclxuICAgICAgICBsZXQgdG9SZW1vdmU6IEluZ2FtZVRyaWFuZ2xlW10gPSB1cGRhdGUudHJpYW5nbGVzVG9SZW1vdmUubWFwKCB0ID0+IFRyaWFuZ2xlQmVhbi5hc0luZ2FtZVRyaWFuZ2xlKCB0ICkgKTtcclxuICAgICAgICB0aGlzLnNlcnZlclVwZGF0ZS5jYWxsKCB0aGlzLCB0b1JlbW92ZSwgdG9BZGQgKTtcclxuICAgIH1cclxufVxyXG5cclxuLyoqXHJcbiAqIFByb2Nlc3NlcyBhIHJlc2V0IGNvbW1hbmQgZnJvbSB0aGUgc2VydmVyLiBcclxuICovXHJcbmV4cG9ydCBjbGFzcyBSZXNldEhhbmRsZXIgaW1wbGVtZW50cyBTZXJ2ZXJDb21tYW5kSGFuZGxlciB7XHJcbiAgICBzdGF0aWMgcmVhZG9ubHkgVFlQRSA9IENvbW1hbmRUeXBlLlJFU0VUO1xyXG4gICAgZ2V0VHlwZSgpIHsgcmV0dXJuIFJlc2V0SGFuZGxlci5UWVBFIH07XHJcbiAgICByZWFkb25seSBzZXJ2ZXJSZXNldCA9IGFwcGx5U2VydmVyUmVzZXQ7XHJcbiAgICBwcm9jZXNzKCBjb21tYW5kOiBhbnkgKSB7XHJcbiAgICAgICAgbGV0IHJlc2V0OiBSZXNldFN0YXRlID0gUmVzZXRTdGF0ZS5mcm9tSnNvbiggY29tbWFuZCApO1xyXG4gICAgICAgIHRoaXMuc2VydmVyUmVzZXQuY2FsbCggdGhpcywgcmVzZXQudHJpYW5nbGVzVG9DcmVhdGUubWFwKCB0ID0+IFRyaWFuZ2xlQmVhbi5hc0luZ2FtZVRyaWFuZ2xlKCB0ICkgKSApO1xyXG4gICAgfVxyXG59XHJcblxyXG4vKipcclxuICogUHJvY2Vzc2VzIGEgcmVnaXN0ZXJpbmcgYWNrbm93bGVkZ2VtZW50IGZyb20gdGhlIHNlcnZlci4gXHJcbiAqL1xyXG5leHBvcnQgY2xhc3MgSW5pdEhhbmRsZXIgaW1wbGVtZW50cyBTZXJ2ZXJDb21tYW5kSGFuZGxlciB7XHJcbiAgICBzdGF0aWMgcmVhZG9ubHkgVFlQRSA9IENvbW1hbmRUeXBlLklOSVQ7XHJcbiAgICBnZXRUeXBlKCkgeyByZXR1cm4gSW5pdEhhbmRsZXIuVFlQRSB9O1xyXG4gICAgcmVhZG9ubHkgc2VydmVyUmVnaXN0ZXIgPSBhcHBseVNlcnZlclJlZ2lzdGVyO1xyXG4gICAgcHJvY2VzcyggY29tbWFuZDogYW55ICkge1xyXG4gICAgICAgIGxldCByZWdpc3RyYXRpb246IEluaXRJbmZvID0gSW5pdEluZm8uZnJvbUpzb24oIGNvbW1hbmQgKTtcclxuICAgICAgICB0aGlzLnNlcnZlclJlZ2lzdGVyLmNhbGwoIHRoaXMsIHJlZ2lzdHJhdGlvbi5wbGF5ZXJJZCwgcmVnaXN0cmF0aW9uLmluaXRpYWxTdGF0ZS50cmlhbmdsZXNUb0NyZWF0ZS5tYXAoIHQgPT4gVHJpYW5nbGVCZWFuLmFzSW5nYW1lVHJpYW5nbGUoIHQgKSApICk7XHJcbiAgICB9XHJcbn1cclxuXHJcbi8qKlxyXG4qIFByb2Nlc3NlcyBhbiBpbnZhbGlkIGNvbW1hbmQgZnJvbSB0aGUgc2VydmVyIGJ5IGFsbG93aW5nIHRoZSBmcm9udCBlbmQgdG8gcHJvY2VlZCB3aXRob3V0IHdhaXRpbmcgZm9yIHByb3BlciBzZXJ2ZXIgY29tbWFuZC4gXHJcbiovXHJcbmV4cG9ydCBjbGFzcyBJbnZhbGlkQ29tbWFuZEhhbmRsZXIgaW1wbGVtZW50cyBTZXJ2ZXJDb21tYW5kSGFuZGxlciB7XHJcbiAgICBzdGF0aWMgcmVhZG9ubHkgVFlQRSA9IHVuZGVmaW5lZDtcclxuICAgIGdldFR5cGUoKSB7IHJldHVybiBJbnZhbGlkQ29tbWFuZEhhbmRsZXIuVFlQRSB9O1xyXG4gICAgcmVhZG9ubHkgc2VydmVyUmVzZXQgPSByZXN1bWU7XHJcbiAgICBwcm9jZXNzKCBjb21tYW5kPzogYW55ICkge1xyXG4gICAgICAgIHJlc3VtZSgpO1xyXG4gICAgfVxyXG59XHJcblxyXG5cblxuXG4vLyBXRUJQQUNLIEZPT1RFUiAvL1xuLy8gLi90eXBlc2NyaXB0L25ldHdvcmsvY29tbWFuZF9oYW5kbGVycy50cyIsImltcG9ydCB7IHJlbW92ZUZyb21MaXN0IH0gZnJvbSBcIi4uL3V0aWwvc2hhcmVkXCI7XHJcbmltcG9ydCB7IE5ldHdvcmtDb25uZWN0b3IgfSBmcm9tIFwiLi4vbmV0d29yay9uZXR3b3JrXCI7XHJcbmltcG9ydCB7IEVkZ2UsIFZlcnRleCwgVHJpYW5nbGUgfSBmcm9tIFwiLi4vbG9naWMvZ2VvbWV0cmljX29iamVjdHNcIjtcclxuaW1wb3J0IHsgZGVidWcsIGVwc2lsb24gfSBmcm9tIFwiLi4vcnVuL21haW5cIjtcclxuaW1wb3J0IHsgQm9hcmRTdGF0ZSB9IGZyb20gXCIuL2JvYXJkX3N0YXRlXCI7XHJcbmltcG9ydCB7IENhbnZhcyB9IGZyb20gXCIuL2NhbnZhc1wiO1xyXG5pbXBvcnQgeyBHZW9tZXRyeSB9IGZyb20gXCIuLi9sb2dpYy9nZW9tZXRyeVwiO1xyXG5pbXBvcnQgeyBJbmdhbWVUcmlhbmdsZSwgUHJldmlld1RyaWFuZ2xlIH0gZnJvbSBcIi4vZ2FtZV9vYmplY3RzXCI7XHJcbmltcG9ydCB7IGdldENvc3RzRm9yLCBnZXREZXN0cnVjdGlvbkNvc3RzRm9yLCBnZXREZWZlbnNlVXBncmFkZUNvc3RzIH0gZnJvbSBcIi4uL2xvZ2ljL2FjY291bnRhbnRcIjtcclxuaW1wb3J0IHsgQkxBQ0ssIFJFRCB9IGZyb20gXCIuLi91dGlsL2NvbG9yc1wiO1xyXG5cclxuLyoqXHJcbiAqIERpc3BsYXlzIHRoZSB7QGNvZGUgVHJpYW5nbGVzfSBhbmQgYWxsb3dzIG5ldyBvbmVzIHRvIGJlIGRyYXduLlxyXG4gKi9cclxuLy9UT0RPIGFsbG93IG1vcmUgYXN5bmMgYWN0aW9uOiBcclxuLy8tIGRvbid0IHNldCB3YWl0aW5nZm9ycmVzcG9ucyBleGNlcHQgb24gcmVzZXQgZHVlIHRvIGZhaWx1cmVcclxuLy8tIGVzdGltYXRlIGNvc3RzXHJcbi8vLSBlc3RpbWF0ZSBpbmNvbWU/XHJcbmV4cG9ydCBjbGFzcyBDYW52YXNIYW5kbGVyIHtcclxuICAgIHByaXZhdGUgZ2VvQ2FsYzogR2VvbWV0cnk7XHJcbiAgICAvL1RPRE8gc3VwcG9ydCB0b3VjaCBhcyB3ZWxsXHJcbiAgICBwcml2YXRlIG1vdXNlU3RhcnQ6IFZlcnRleCA9IG51bGw7XHJcbiAgICBwcml2YXRlIG1vdXNlRW5kOiBWZXJ0ZXggPSBudWxsO1xyXG4gICAgcHJpdmF0ZSBjYW52YXM6IENhbnZhcztcclxuICAgIHByaXZhdGUgYm9hcmQ6IEhUTUxDYW52YXNFbGVtZW50O1xyXG4gICAgcHJpdmF0ZSBzZXJ2ZXI6IE5ldHdvcmtDb25uZWN0b3I7XHJcbiAgICBwcml2YXRlIF9zdGF0ZTogQm9hcmRTdGF0ZSA9IG5ldyBCb2FyZFN0YXRlKCk7XHJcbiAgICBwcml2YXRlIGxpdmVJbmZvOiBMaXZlSW5mbztcclxuICAgIHByaXZhdGUgaG92ZXJlZFRyaWFuZ2xlOiBJbmdhbWVUcmlhbmdsZTtcclxuICAgIHB1YmxpYyBnZXQgc3RhdGUoKTogQm9hcmRTdGF0ZSB7XHJcbiAgICAgICAgcmV0dXJuIHRoaXMuX3N0YXRlO1xyXG4gICAgfVxyXG5cclxuICAgIC8qKiBIb2xkcyB0aGUgY3VycmVudGx5IGRyYXduIHtAY29kZSBUcmlhbmdsZX0gd2hpY2ggaXMgcHJvcG9zZWQgdG8gdGhlIHBsYXllciBhbmQgbWlnaHQgYmUgcmVxdWVzdGVkIGZyb20gdGhlIHNlcnZlciovXHJcbiAgICBwcml2YXRlIHByZXZpZXdUcmlhbmdsZTogUHJldmlld1RyaWFuZ2xlIHwgSW5nYW1lVHJpYW5nbGU7XHJcbiAgICAvKiBUZWxscyB3aGV0aGVyIGEgdHJpYW5nbGUgd2FzIHByb3Bvc2VkIHRvIHRoZSBzZXJ2ZXIgYW5kIHRoZSByZXNwb25zZSBpcyBzdGlsbCBwZW5kaW5nICovXHJcbiAgICBwcml2YXRlIHdhaXRpbmdGb3JSZXNwb25zZTogYm9vbGVhbiA9IHRydWU7XHJcblxyXG4gICAgLyoqXHJcbiAgICAgKiBDcmVhdGVzIGEgbmV3IHtAY29kZSBDYW52YXNIYW5kbGVyfS5cclxuICAgICAqIEBwYXJhbSBzZXJ2ZXIgdGhlIGNvbm5lY3Rpb24gdG8gdGhlIHNlcnZlciBhbGwgY29tbXVuaWNhdGlvbiB3aWxsIGJlIHJlYWxpemVkIHdpdGguXHJcbiAgICAgKi9cclxuICAgIGNvbnN0cnVjdG9yKCBzZXJ2ZXI6IE5ldHdvcmtDb25uZWN0b3IgKSB7XHJcbiAgICAgICAgdGhpcy5zZXJ2ZXIgPSBzZXJ2ZXI7XHJcbiAgICAgICAgLy93b3JrYXJvdW5kIHRvIHByZXZlbnQgdXMgZnJvbSByZWZlcnJpbmcgdG8gdGhlIGNhbGxiYWNrICd0aGlzJyB0aGVyZVxyXG4gICAgICAgIHRoaXMuYXBwbHlTZXJ2ZXJVcGRhdGUgPSB0aGlzLmFwcGx5U2VydmVyVXBkYXRlLmJpbmQoIHRoaXMgKTtcclxuICAgICAgICB0aGlzLm1vdXNlRG93bkV2ZW50ID0gdGhpcy5tb3VzZURvd25FdmVudC5iaW5kKCB0aGlzICk7XHJcbiAgICAgICAgdGhpcy5tb3VzZU1vdmVFdmVudCA9IHRoaXMubW91c2VNb3ZlRXZlbnQuYmluZCggdGhpcyApO1xyXG4gICAgICAgIHRoaXMubW91c2VVcEV2ZW50ID0gdGhpcy5tb3VzZVVwRXZlbnQuYmluZCggdGhpcyApO1xyXG4gICAgfVxyXG5cclxuXHJcbiAgICAvKipcclxuICAgICAqIEluaXRpYWxpemVzIHRoaXMge0Bjb2RlIENhbnZhc0hhbmRsZXJ9LlxyXG4gICAgICogQHBhcmFtIGJvYXJkIHRoZSBjYW52YXMgdGhpcyBoYW5kbGVyIHdpbGwgd29yayBvblxyXG4gICAgICovXHJcbiAgICBpbml0KCBib2FyZDogSFRNTENhbnZhc0VsZW1lbnQgKSB7XHJcbiAgICAgICAgdGhpcy5ib2FyZCA9IGJvYXJkO1xyXG4gICAgICAgIHRoaXMuY2FudmFzID0gbmV3IENhbnZhcyggYm9hcmQgKTtcclxuICAgICAgICB0aGlzLmdlb0NhbGMgPSBuZXcgR2VvbWV0cnkoIHRoaXMuY2FudmFzICk7XHJcbiAgICAgICAgdGhpcy5fc3RhdGUuY2FudmFzID0gdGhpcy5ib2FyZC5nZXRDb250ZXh0KCBcIjJkXCIgKTtcclxuICAgICAgICBpZiAoICF0aGlzLmNhbnZhcyApIHsgYWxlcnQoIFwiQ291bGQgbm90IGdldCB0aGUgY2FudmFzIGVsZW1lbnQhXCIgKTsgcmV0dXJuOyB9XHJcbiAgICAgICAgLy8gdGhpcy5jYW52YXNDb250ZXh0LnRyYW5zbGF0ZSggMC41LCAwLjUgKTtcclxuICAgICAgICB0aGlzLmJvYXJkLmFkZEV2ZW50TGlzdGVuZXIoIFwibW91c2Vkb3duXCIsIHRoaXMubW91c2VEb3duRXZlbnQgKTtcclxuICAgICAgICB0aGlzLmJvYXJkLmFkZEV2ZW50TGlzdGVuZXIoIFwibW91c2V1cFwiLCB0aGlzLm1vdXNlVXBFdmVudCApO1xyXG4gICAgICAgIHRoaXMuYm9hcmQuYWRkRXZlbnRMaXN0ZW5lciggXCJtb3VzZW1vdmVcIiwgdGhpcy5tb3VzZU1vdmVFdmVudCApXHJcbiAgICAgICAgdGhpcy5ib2FyZC5hZGRFdmVudExpc3RlbmVyKCBcIm1vdXNlb3ZlclwiLCAoKSA9PiBldmVudC5wcmV2ZW50RGVmYXVsdCgpIClcclxuICAgICAgICB0aGlzLmxpdmVJbmZvID0gbmV3IExpdmVJbmZvKCB0aGlzLmJvYXJkICk7XHJcbiAgICAgICAgdGhpcy5zZXJ2ZXIucmVnaXN0ZXJQbGF5ZXIoKTtcclxuICAgIH1cclxuXHJcbiAgICAvKipcclxuICAgICAqIEZpbmFsaXppZXMgdGhlIHJlZ2lzdHJhdGlvbiBvZiB0aGlzIGJvYXJkIGFuIGZldGNoZXMgdGhlIGluaXRpYWwgaW5mb3JtYXRpb24uIFRoaXMgbWV0aG9kIGlzIHN1cHBvc2VkIHRvIGJlIGludm9rZWQgYnkgYSBzZXJ2ZXIncyBjb21tYW5kLlxyXG4gICAgICogQHBhcmFtIG5hbWUgdGhlIHBsYXllciBpcyByZWdpc3RlcmVkIHdpdGggYXQgdGhlIHNlcnZlclxyXG4gICAgICovXHJcbiAgICByZWdpc3RlciggbmFtZTogc3RyaW5nLCB0b0NyZWF0ZTogSW5nYW1lVHJpYW5nbGVbXSApIHtcclxuICAgICAgICB0aGlzLl9zdGF0ZS5wbGF5ZXJJZCA9IG5hbWU7XHJcbiAgICAgICAgdGhpcy5hcHBseVNlcnZlclJlc2V0KCB0b0NyZWF0ZSApO1xyXG4gICAgfVxyXG5cclxuICAgIC8qKlxyXG4gICAgICogQ3JlYXRlcyBhIG5ldyB7QGNvZGUgVHJpYW5nbGV9IGFuZCByZWdpc3RlcnMgaXQgZm9yIHRoZSBvd25lci4gTm90ZSB0aGF0IHRoZSBvdXRsaW5lIHdvbid0IGJlIHVwZGF0ZWQgaGVyZS5cclxuICAgICAqIEBwYXJhbSByZWxhdGVkRWRnZSB3aGljaCBmb3JtcyB0aGUge0Bjb2RlIFRyaWFuZ2xlfSB0b2dldGhlciB3aXRoIGl0cyBwcmVkZWNlc3NvciBhbmQgc3VjY2Vzc29yXHJcbiAgICAgKiBAcGFyYW0gb3duZXIgdGhlIHtAY29kZSBUcmlhbmdsZX0gYmVsb25ncyB0byBhbmQgd2lsbCBiZSByZWdpc3RlcmVkIGZvclxyXG4gICAgICovXHJcbiAgICBwcml2YXRlIHJlZ2lzdGVyVHJpYW5nbGUoIHRvUmVnaXN0ZXI6IEluZ2FtZVRyaWFuZ2xlICk6IHZvaWQge1xyXG4gICAgICAgIHRoaXMuc3RhdGUuZ2V0VHJpYW5nbGVzQnlPd25lciggdG9SZWdpc3Rlci5vd25lciApLnB1c2goIHRvUmVnaXN0ZXIgKTtcclxuICAgIH1cclxuXHJcbiAgICAvKipcclxuICAgICAqIFVucmVnaXN0ZXJzIGEgZ2l2ZW4ge0Bjb2RlIFRyaWFuZ2xlfSBmcm9tIG5laWdoYm9ycyBhbmQgb3RoZXIgZmllbGRzLiBOb3RlIHRoYXQgdGhlIG91dGxpbmUgd29uJ3QgYmUgdXBkYXRlZCBoZXJlLlxyXG4gICAgICogQHBhcmFtIHRvUmVtb3ZlIHRoZSB7QGNvZGUgVHJpYW5nbGV9IHRvIGJlIGRyb3BwZWQgYW5kIHVucmVnaXN0ZXJlZFxyXG4gICAgICovXHJcbiAgICBwcml2YXRlIGRyb3BBbmRVbnJlZ2lzdGVyVHJpYW5nbGUoIHRvUmVtb3ZlOiBJbmdhbWVUcmlhbmdsZSApOiB2b2lkIHtcclxuICAgICAgICBmb3IgKCBsZXQgZWRnZSBvZiB0b1JlbW92ZS50cmlhbmdsZS5lZGdlcyApIHtcclxuICAgICAgICAgICAgbGV0IHR3aW4gPSBlZGdlLmdldFR3aW5FZGdlKCk7XHJcbiAgICAgICAgICAgIGlmICggdHdpbiApIHRoaXMuc3RhdGUucGxheWVyT3V0bGluZS5wdXNoKCB0d2luICk7XHJcbiAgICAgICAgfVxyXG4gICAgICAgIHJlbW92ZUZyb21MaXN0KCB0b1JlbW92ZSwgdGhpcy5zdGF0ZS5nZXRUcmlhbmdsZXNCeU93bmVyKCB0b1JlbW92ZS5vd25lciApICk7XHJcbiAgICAgICAgdG9SZW1vdmUuZHJvcCgpO1xyXG4gICAgfVxyXG5cclxuICAgIC8qKlxyXG4gICAgICogU3RhcnRzIHRoZSBwcm9jZXNzIHdoaWNoIGNyZWF0ZXMvZHJhd2VzIGEgbmV3IHtAY29kZSBUcmlhbmdsZX0uXHJcbiAgICAgKiBAcGFyYW0gZSB0aGUgbW91c2UgZXZlbnQgdHJpZ2dlcmluZyB0aGlzIG1ldGhvZFxyXG4gICAgICovXHJcbiAgICBwcml2YXRlIG1vdXNlRG93bkV2ZW50KCBlOiBNb3VzZUV2ZW50ICk6IHZvaWQge1xyXG4gICAgICAgIGV2ZW50LnByZXZlbnREZWZhdWx0KCk7XHJcbiAgICAgICAgaWYgKCB0aGlzLndhaXRpbmdGb3JSZXNwb25zZSApIHJldHVybjtcclxuICAgICAgICB0aGlzLm1vdXNlU3RhcnQgPSBuZXcgVmVydGV4KCBlLnBhZ2VYIC0gdGhpcy5ib2FyZC5vZmZzZXRMZWZ0LCBlLnBhZ2VZIC0gdGhpcy5ib2FyZC5vZmZzZXRUb3AgKTtcclxuICAgICAgICBpZiAoICF0aGlzLmdlb0NhbGMuZ2V0Q29udGFpbmluZ1RyaWFuZ2xlKCB0aGlzLm1vdXNlU3RhcnQsIHRoaXMuX3N0YXRlICkgKSB7XHJcbiAgICAgICAgICAgIHRoaXMubW91c2VTdGFydCA9IG51bGw7XHJcbiAgICAgICAgICAgIHRoaXMubW91c2VFbmQgPSBudWxsO1xyXG4gICAgICAgICAgICByZXR1cm47XHJcbiAgICAgICAgfVxyXG4gICAgICAgIHRoaXMubW91c2VFbmQgPSBudWxsO1xyXG4gICAgfVxyXG5cclxuICAgIC8qKlxyXG4gICAgICogUHJvY2Vzc2VzIHRoZSBjdXJyZW50IGRyYXduIGxpbmUgdG8gY2FsY3VsYXRlIGEgbmV3IHtAY29kZSBUcmlhbmdsZX0uXHJcbiAgICAgKiBAcGFyYW0gZSB0aGUgbW91c2UgZXZlbnQgdHJpZ2dlcmluZyB0aGlzIG1ldGhvZFxyXG4gICAgICovXHJcbiAgICBwcml2YXRlIG1vdXNlTW92ZUV2ZW50KCBlOiBNb3VzZUV2ZW50ICk6IHZvaWQge1xyXG4gICAgICAgIGV2ZW50LnByZXZlbnREZWZhdWx0KCk7XHJcbiAgICAgICAgaWYgKCB0aGlzLndhaXRpbmdGb3JSZXNwb25zZSApIHJldHVybjtcclxuICAgICAgICBpZiAoICF0aGlzLm1vdXNlU3RhcnQgKSB7XHJcbiAgICAgICAgICAgIHRoaXMuaG92ZXJlZFRyaWFuZ2xlID0gdGhpcy5nZXRDb250YWluaW5nVHJpYW5nbGUoIG5ldyBWZXJ0ZXgoIGUucGFnZVggLSB0aGlzLmJvYXJkLm9mZnNldExlZnQsIGUucGFnZVkgLSB0aGlzLmJvYXJkLm9mZnNldFRvcCApIClcclxuICAgICAgICAgICAgaWYgKCB0aGlzLmhvdmVyZWRUcmlhbmdsZSAmJiB0aGlzLmhvdmVyZWRUcmlhbmdsZS5vd25lciA9PT0gdGhpcy5zdGF0ZS5wbGF5ZXJJZCApIHRoaXMudXBkYXRlVXBncmFkZVRyaWFuZ2xlTGl2ZUluZm9zKCk7XHJcbiAgICAgICAgICAgIHJldHVybjtcclxuICAgICAgICB9IGVsc2UgdGhpcy5ob3ZlcmVkVHJpYW5nbGUgPSBudWxsOyAvL1RPRE8gY291bnQgYXMgY2xpY2sgZXZlbiB3aGVuIG1vdXNlU3RhcnQgZXhpc3RzLCBhcyBsb25nIGFzIGl0IGlzIGNsb3NlIGJ5IGFuIGluIHRoZSBzYW1lIHRyaWFuZ2xlP1xyXG4gICAgICAgIHRoaXMuY2FudmFzLmRyYXdDYW52YXMoIHRoaXMuc3RhdGUgKTtcclxuICAgICAgICB0aGlzLm1vdXNlRW5kID0gbmV3IFZlcnRleCggZS5wYWdlWCAtIHRoaXMuYm9hcmQub2Zmc2V0TGVmdCwgZS5wYWdlWSAtIHRoaXMuYm9hcmQub2Zmc2V0VG9wICk7XHJcbiAgICAgICAgaWYgKCBkZWJ1ZyA9PSAyICkge1xyXG4gICAgICAgICAgICB0aGlzLmNhbnZhcy5kcmF3TGluZSggdGhpcy5tb3VzZVN0YXJ0LCB0aGlzLm1vdXNlRW5kICk7XHJcbiAgICAgICAgfSBlbHNlIHtcclxuICAgICAgICAgICAgdGhpcy5wcmV2aWV3VHJpYW5nbGUgPSBJbmdhbWVUcmlhbmdsZS5vZiggdGhpcy5nZW9DYWxjLmNyZWF0ZVByZXZpZXdUcmlhbmdsZSggdGhpcy5tb3VzZVN0YXJ0LCB0aGlzLm1vdXNlRW5kLCB0aGlzLl9zdGF0ZSApLCB0aGlzLl9zdGF0ZS5wbGF5ZXJJZCApO1xyXG4gICAgICAgICAgICBpZiAoIHRoaXMucHJldmlld1RyaWFuZ2xlICkge1xyXG4gICAgICAgICAgICAgICAgaWYgKCB0aGlzLnZhbGlkYXRlUHJldmlld1RyaWFuZ2xlKCkgKVxyXG4gICAgICAgICAgICAgICAgICAgIHRoaXMuY2FudmFzLmRyYXcoIHRoaXMucHJldmlld1RyaWFuZ2xlICk7XHJcbiAgICAgICAgICAgIH1cclxuICAgICAgICB9XHJcbiAgICAgICAgdGhpcy51cGRhdGVQcmV2aWV3VHJpYW5nbGVMaXZlSW5mb3MoKTtcclxuICAgIH1cclxuXHJcbiAgICAvKipcclxuICAgICAqIFZhbGlkYXRlcyBhIGdpdmVuIHByZXZpZXcgdHJpYW5nbGUgYW5kIHVwZGF0ZSBpbmZvcm1hdGlvbiBhYm91dCB0aGUgY29zdHMgZXRjLlxyXG4gICAgICovXHJcbiAgICBwcml2YXRlIHZhbGlkYXRlUHJldmlld1RyaWFuZ2xlKCk6IGJvb2xlYW4ge1xyXG4gICAgICAgIC8vVE9ETyBpcyBhbGwgaGVyZSB0aGUgY2FudmFzIGhhbmRsZXIncyBqb2IgdG8gZGV0ZXJtaW5lP1xyXG4gICAgICAgIGxldCBjb2xsaXNpb25zOiBUcmlhbmdsZVtdID0gdGhpcy5nZW9DYWxjLmludGVyc2VjdGluZ1RyaWFuZ2xlcyggdGhpcy5wcmV2aWV3VHJpYW5nbGUudHJpYW5nbGUsIHRoaXMuc3RhdGUgKTtcclxuICAgICAgICBsZXQgZGVzdHJ1Y3Rpb25Db3N0cyA9IDA7XHJcbiAgICAgICAgZm9yICggbGV0IGNvbGxpc2lvbiBvZiBjb2xsaXNpb25zICkge1xyXG4gICAgICAgICAgICBpZiAoIGNvbGxpc2lvbi5pbmdhbWVUcmlhbmdsZS5vd25lciA9PT0gdGhpcy5zdGF0ZS5wbGF5ZXJJZCApIHJldHVybiBmYWxzZTtcclxuICAgICAgICAgICAgZGVzdHJ1Y3Rpb25Db3N0cyArPSBnZXREZXN0cnVjdGlvbkNvc3RzRm9yKCBjb2xsaXNpb24uaW5nYW1lVHJpYW5nbGUgKTtcclxuICAgICAgICB9XHJcbiAgICAgICAgaWYgKCB0aGlzLnByZXZpZXdUcmlhbmdsZSBpbnN0YW5jZW9mIEluZ2FtZVRyaWFuZ2xlIClcclxuICAgICAgICAgICAgdGhpcy5wcmV2aWV3VHJpYW5nbGUgPSBuZXcgUHJldmlld1RyaWFuZ2xlKCB0aGlzLnByZXZpZXdUcmlhbmdsZSApO1xyXG4gICAgICAgIHRoaXMucHJldmlld1RyaWFuZ2xlLmNvbGxpc2lvbkNvc3RzID0gZGVzdHJ1Y3Rpb25Db3N0cztcclxuICAgICAgICB0aGlzLnVwZGF0ZVByZXZpZXdUcmlhbmdsZUxpdmVJbmZvcygpO1xyXG4gICAgICAgIHJldHVybiB0cnVlO1xyXG4gICAgfVxyXG5cclxuICAgIC8qKlxyXG4gICAgICogQ2hlY2tzIHdoZXRoZXIgYSB2YWxpZCB7QGNvZGUgVHJpYW5nbGV9IGNhbiBiZSBjcmVhdGVkIGZyb20gdGhlIGRyYXduIGxpbmUuIElmIHRoYXQncyB0aGUgY2FzZSwgdGhlIG5ldyB7QGNvZGUgVHJpYW5nbGV9IHdpbGwgYmUgY3JlYXRlZC5cclxuICAgICAqIEBwYXJhbSBlIHRoZSBtb3VzZSBldmVudCB0cmlnZ2VyaW5nIHRoaXMgbWV0aG9kXHJcbiAgICAgKi9cclxuICAgIHByaXZhdGUgbW91c2VVcEV2ZW50KCBlOiBNb3VzZUV2ZW50ICk6IHZvaWQge1xyXG4gICAgICAgIGV2ZW50LnByZXZlbnREZWZhdWx0KCk7XHJcbiAgICAgICAgdGhpcy5saXZlSW5mby5zZXRIaWRkZW4oIHRydWUgKTtcclxuICAgICAgICBpZiAoIHRoaXMud2FpdGluZ0ZvclJlc3BvbnNlICkgcmV0dXJuO1xyXG4gICAgICAgIGlmICggIXRoaXMubW91c2VFbmQgKSB0aGlzLm1vdXNlRW5kID0gbmV3IFZlcnRleCggZS5wYWdlWCAtIHRoaXMuYm9hcmQub2Zmc2V0TGVmdCwgZS5wYWdlWSAtIHRoaXMuYm9hcmQub2Zmc2V0VG9wICk7XHJcbiAgICAgICAgaWYgKCB0aGlzLmhvdmVyZWRUcmlhbmdsZSAmJiB0aGlzLmhvdmVyZWRUcmlhbmdsZS5vd25lciA9PT0gdGhpcy5zdGF0ZS5wbGF5ZXJJZCApIHtcclxuICAgICAgICAgICAgLy9jaGVjayB3aGV0aGVyIHN0YXJ0IHdhcyBpbiB0aGUgc2FtZSB0cmlhbmdsZT9cclxuICAgICAgICAgICAgaWYgKCB0aGlzLmhvdmVyZWRUcmlhbmdsZS5kZWZlbnNlIDwgSW5nYW1lVHJpYW5nbGUuTUFYX0RFRkVOU0UgKSB7XHJcbiAgICAgICAgICAgICAgICBsZXQgdXBncmFkZVByb3Bvc2FsID0gbmV3IFByZXZpZXdUcmlhbmdsZSggdGhpcy5ob3ZlcmVkVHJpYW5nbGUgKTtcclxuICAgICAgICAgICAgICAgIGxldCB1cGdyYWRlQ29zdHMgPSBnZXREZWZlbnNlVXBncmFkZUNvc3RzKCB0aGlzLmhvdmVyZWRUcmlhbmdsZSApO1xyXG4gICAgICAgICAgICAgICAgaWYgKCB1cGdyYWRlQ29zdHMgPD0gdGhpcy5zdGF0ZS5iYWxhbmNlICkge1xyXG4gICAgICAgICAgICAgICAgICAgIHVwZ3JhZGVQcm9wb3NhbC5kZWZlbnNlKys7XHJcbiAgICAgICAgICAgICAgICAgICAgdGhpcy5zZXJ2ZXIucHJvcG9zZVRyaWFuZ2xlKCB1cGdyYWRlUHJvcG9zYWwuaW5nYW1lVHJpYW5nbGUgKTtcclxuICAgICAgICAgICAgICAgICAgICB0aGlzLnN0YXRlLmJhbGFuY2UgLT0gdXBncmFkZUNvc3RzOyAvL3JlamVjdGVkIHVwZ3JhZGUgY29zdHMgcmVzZXQgYXV0b21hdGljYWxseVxyXG4gICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICB9XHJcbiAgICAgICAgICAgIHRoaXMubW91c2VTdGFydCA9IG51bGw7XHJcbiAgICAgICAgICAgIHRoaXMubW91c2VFbmQgPSBudWxsO1xyXG4gICAgICAgICAgICByZXR1cm47XHJcbiAgICAgICAgfVxyXG5cclxuICAgICAgICBpZiAoIGRlYnVnID09IDIgKSB7XHJcbiAgICAgICAgICAgIHRoaXMucHJldmlld1RyaWFuZ2xlID0gSW5nYW1lVHJpYW5nbGUub2YoIHRoaXMuZ2VvQ2FsYy5jcmVhdGVQcmV2aWV3VHJpYW5nbGUoIHRoaXMubW91c2VTdGFydCwgdGhpcy5tb3VzZUVuZCwgdGhpcy5fc3RhdGUgKSwgdGhpcy5fc3RhdGUucGxheWVySWQgKTtcclxuICAgICAgICAgICAgaWYgKCB0aGlzLnByZXZpZXdUcmlhbmdsZSApIHRoaXMuY2FudmFzLmRyYXcoIHRoaXMucHJldmlld1RyaWFuZ2xlICk7XHJcbiAgICAgICAgfVxyXG4gICAgICAgIGlmICggZGVidWcgKSB0aGlzLnByZXZpZXdUcmlhbmdsZSA9IEluZ2FtZVRyaWFuZ2xlLm9mKCB0aGlzLmdlb0NhbGMuY3JlYXRlUHJldmlld1RyaWFuZ2xlKCB0aGlzLm1vdXNlU3RhcnQsIHRoaXMubW91c2VFbmQsIHRoaXMuX3N0YXRlICksIHRoaXMuX3N0YXRlLnBsYXllcklkICk7XHJcbiAgICAgICAgaWYgKCB0aGlzLnByZXZpZXdUcmlhbmdsZSApIHtcclxuICAgICAgICAgICAgbGV0IGNyZWF0aW9uQ29zdHMgPSBnZXRDb3N0c0ZvciggdGhpcy5wcmV2aWV3VHJpYW5nbGUgKTtcclxuICAgICAgICAgICAgaWYgKCBjcmVhdGlvbkNvc3RzIDw9IHRoaXMuc3RhdGUuYmFsYW5jZSApIHtcclxuICAgICAgICAgICAgICAgIHRoaXMuc2VydmVyLnByb3Bvc2VUcmlhbmdsZSggdGhpcy5wcmV2aWV3VHJpYW5nbGUgaW5zdGFuY2VvZiBQcmV2aWV3VHJpYW5nbGUgPyB0aGlzLnByZXZpZXdUcmlhbmdsZS5pbmdhbWVUcmlhbmdsZSA6IHRoaXMucHJldmlld1RyaWFuZ2xlICk7XHJcbiAgICAgICAgICAgICAgICB0aGlzLnN0YXRlLmJhbGFuY2UgLT0gY3JlYXRpb25Db3N0czsgLy9yZWplY3RlZCBjcmVhdGVpb24gY29zdHMgcmVzZXQgYXV0b21hdGljYWxseVxyXG4gICAgICAgICAgICB9XHJcbiAgICAgICAgICAgIHRoaXMuY2FudmFzLmRyYXdDYW52YXMoIHRoaXMuc3RhdGUgKTtcclxuICAgICAgICAgICAgdGhpcy5wcmV2aWV3VHJpYW5nbGUgPSBudWxsO1xyXG4gICAgICAgIH1cclxuICAgICAgICB0aGlzLm1vdXNlU3RhcnQgPSBudWxsO1xyXG4gICAgICAgIHRoaXMubW91c2VFbmQgPSBudWxsO1xyXG4gICAgfVxyXG5cclxuICAgIC8qKlxyXG4gICAgICogUmVtb3ZlcyBhbmQgYWRkcyB0aGUgc3BlY2lmaWVkIHtAY29kZSBUcmlhbmdsZXN9LiBUaGlzIG1ldGhvZCBpcyBzdXBwb3NlZCB0byBiZSBpbnZva2VkIGJ5IGEgc2VydmVyJ3MgY29tbWFuZC5cclxuICAgICAqIEBwYXJhbSB0b1JlbW92ZSB7QGNvZGUgVHJpYW5nbGVzfSB3aGljaCBzaGFsbCBiZSByZW1vdmVkLiBXaWxsIGJlIHByb2Nlc3NlZCBmaXJzdC5cclxuICAgICAqIEBwYXJhbSB0b0FkZCB7QGNvZGUgVHJpYW5nbGVzfSB3aGljaCBzaGFsbCBiZSBhZGRlZC4gV2lsbCBiZSBwcm9jZXNzZWQgc2Vjb25kIGFuZCB0aGVyZWZvcmUgc2hvdWxkIG5vdCByZWx5IHtAY29kZSBUcmlhbmdsZXN9IG91Z2h0IHRvIGJlIHJlbW92ZWQuXHJcbiAgICAgKi9cclxuICAgIGFwcGx5U2VydmVyVXBkYXRlKCB0b1JlbW92ZTogSW5nYW1lVHJpYW5nbGVbXSwgdG9BZGQ6IEluZ2FtZVRyaWFuZ2xlW10gKTogdm9pZCB7XHJcbiAgICAgICAgZm9yICggbGV0IHJlbW92ZSBvZiB0b1JlbW92ZSApIHtcclxuICAgICAgICAgICAgbGV0IHN1Y2Nlc3M6IGJvb2xlYW4gPSB0aGlzLnJlbW92ZVNlcnZlclRyaWFuZ2xlKCByZW1vdmUgKTtcclxuICAgICAgICAgICAgaWYgKCAhc3VjY2VzcyApIHtcclxuICAgICAgICAgICAgICAgIHRoaXMuc2VydmVyLnJlcXVlc3RDb21wbGV0ZVN0YXRlKCk7XHJcbiAgICAgICAgICAgICAgICB0aGlzLndhaXRpbmdGb3JSZXNwb25zZSA9IHRydWU7XHJcbiAgICAgICAgICAgICAgICByZXR1cm47XHJcbiAgICAgICAgICAgIH1cclxuICAgICAgICB9XHJcbiAgICAgICAgZm9yICggbGV0IGFkZCBvZiB0b0FkZCApIHtcclxuICAgICAgICAgICAgbGV0IHN1Y2Nlc3M6IGJvb2xlYW4gPSB0aGlzLmFkZFNlcnZlclRyaWFuZ2xlKCBhZGQgKTtcclxuICAgICAgICAgICAgaWYgKCAhc3VjY2VzcyApIHtcclxuICAgICAgICAgICAgICAgIHRoaXMuc2VydmVyLnJlcXVlc3RDb21wbGV0ZVN0YXRlKCk7XHJcbiAgICAgICAgICAgICAgICB0aGlzLndhaXRpbmdGb3JSZXNwb25zZSA9IHRydWU7XHJcbiAgICAgICAgICAgICAgICByZXR1cm47XHJcbiAgICAgICAgICAgIH1cclxuICAgICAgICB9XHJcbiAgICAgICAgdGhpcy5maW5hbGl6ZVJlc3BvbnNlSGFuZGxpbmcoKVxyXG4gICAgfVxyXG5cclxuICAgIC8qKlxyXG4gICAgICogUmVzZXRzIHRoZSBzdGF0ZSBhbmQgcmVwbGFjZXMgaXQgd2l0aCB0aGUgZ2l2ZW4ge0Bjb2RlIFRyaWFuZ2xlc30uIFRoaXMgbWV0aG9kIGlzIHN1cHBvc2VkIHRvIGJlIGludm9rZWQgYnkgYSBzZXJ2ZXIncyBjb21tYW5kLlxyXG4gICAgICogQHBhcmFtIHRvQ3JlYXRlIHtAY29kZSBUcmlhbmdsZXN9IGNvbnN0aXR1dGluZyB0aGUgbmV3IHN0YXRlLiBUaGV5IHdpbGwgYmUgY29uc3RydWN0ZWQgaW4gdGhlIGdpdmVuIG9yZGVyLiBcclxuICAgICAqL1xyXG4gICAgYXBwbHlTZXJ2ZXJSZXNldCggdG9DcmVhdGU6IEluZ2FtZVRyaWFuZ2xlW10gKTogdm9pZCB7XHJcbiAgICAgICAgdGhpcy5fc3RhdGUucGxheWVyVHJpYW5nbGVzID0gW107XHJcbiAgICAgICAgdGhpcy5fc3RhdGUub3RoZXJUcmlhbmdsZXMgPSBbXTtcclxuICAgICAgICB0aGlzLl9zdGF0ZS5wbGF5ZXJPdXRsaW5lID0gW107XHJcbiAgICAgICAgdGhpcy5fc3RhdGUub3RoZXJPdXRsaW5lID0gW107XHJcbiAgICAgICAgZm9yICggbGV0IG5leHRUb0NyZWF0ZSBvZiB0b0NyZWF0ZSApIHtcclxuICAgICAgICAgICAgbGV0IHN1Y2Nlc3M6IGJvb2xlYW4gPSB0aGlzLmFkZFNlcnZlclRyaWFuZ2xlKCBuZXh0VG9DcmVhdGUgKTtcclxuICAgICAgICAgICAgaWYgKCAhc3VjY2VzcyApIHtcclxuICAgICAgICAgICAgICAgIGFsZXJ0KCBcIlJlc2V0IGZhaWxlZCwgc3RvcHBpbmcgZ2FtZS5cIiApO1xyXG4gICAgICAgICAgICAgICAgcmV0dXJuO1xyXG4gICAgICAgICAgICB9XHJcbiAgICAgICAgfVxyXG4gICAgICAgIHRoaXMuZmluYWxpemVSZXNwb25zZUhhbmRsaW5nKCk7XHJcbiAgICB9XHJcblxyXG4gICAgLyoqQ2FuY2VscyBhbnkgYmxvY2tpbmdzIG9mIHVzZXIgYWN0aW9ucyovXHJcbiAgICByZXN1bWUoKTogdm9pZCB7XHJcbiAgICAgICAgdGhpcy5maW5hbGl6ZVJlc3BvbnNlSGFuZGxpbmcoKTtcclxuICAgIH1cclxuXHJcbiAgICAvKipcclxuICAgICAqIFNldHMgdGhlIHBsYXllcnMgYWNjb3VudCB0byB0aGUgZ2l2ZW4gYmFsYW5jZS5cclxuICAgICAqIEBwYXJhbSBiYWxhbmNlIHRoZSBhY2NvdW50IHNoYWxsIGJlIHNldCB0b1xyXG4gICAgICovXHJcbiAgICBzZXRBY2NvdW50KCBiYWxhbmNlOiBudW1iZXIgKSB7XHJcbiAgICAgICAgdGhpcy5zdGF0ZS5iYWxhbmNlID0gYmFsYW5jZTtcclxuICAgICAgICBpZiAoIHRoaXMubW91c2VTdGFydCApIHRoaXMudXBkYXRlUHJldmlld1RyaWFuZ2xlTGl2ZUluZm9zKCk7XHJcbiAgICAgICAgZWxzZSB0aGlzLnVwZGF0ZVVwZ3JhZGVUcmlhbmdsZUxpdmVJbmZvcygpO1xyXG4gICAgfVxyXG5cclxuICAgIC8qKlxyXG4gICAgICogUmV0dXJucyB7QGNvZGUgSW5nYW1lVHJpYW5nbGV9IGNvbnRhaW5pbmcgdGhlIHtAY29kZSBWZXJ0ZXh9IChtb3N0IGxpa2VseSB0aGUgbW91c2UgcG9zaXRpb24pXHJcbiAgICAgKiBAcGFyYW0gdmVydGV4IHRoZSBtb3VzZSBwb3NpdGlvbiBhIG1hdGNoaW5nIHtAY29kZSBJbmdhbWVUcmlhbmdsZX0gd2lsbCBiZSBzZWFyY2hlZCBmb3JcclxuICAgICAqIEByZXR1cm4gdGhlIHtAY29kZSBJbmdhbWVUcmlhbmdsZX0gY29udGFpbmluZyB0aGUgZ2l2ZW4ge0Bjb2RlIFZlcnRleH1cclxuICAgICAqL1xyXG4gICAgcHJpdmF0ZSBnZXRDb250YWluaW5nVHJpYW5nbGUoIHZlcnRleDogVmVydGV4ICk6IEluZ2FtZVRyaWFuZ2xlIHtcclxuICAgICAgICBmb3IgKCBsZXQgdHJpYW5nbGUgb2YgdGhpcy5zdGF0ZS5wbGF5ZXJUcmlhbmdsZXMgKVxyXG4gICAgICAgICAgICBpZiAoIHRyaWFuZ2xlLnRyaWFuZ2xlLmNvbnRhaW5zKCB2ZXJ0ZXggKSApIHJldHVybiB0cmlhbmdsZTtcclxuICAgICAgICBmb3IgKCBsZXQgdHJpYW5nbGUgb2YgdGhpcy5zdGF0ZS5vdGhlclRyaWFuZ2xlcyApXHJcbiAgICAgICAgICAgIGlmICggdHJpYW5nbGUudHJpYW5nbGUuY29udGFpbnMoIHZlcnRleCApICkgcmV0dXJuIHRyaWFuZ2xlO1xyXG4gICAgICAgIHJldHVybiBudWxsO1xyXG4gICAgfVxyXG5cclxuICAgIC8qKlxyXG4gICAgICogVXBkYXRlcyB0ZWggbGl2ZSBpbmZvcyBiYXNlZCBvbiB0aGUgdHJpYW5nbGUgdGhlIG1vdXNlIGlmIGhvdmVyaW5nIG92ZXJcclxuICAgICAqL1xyXG4gICAgLy9UT0RPIHJlZmFjdG9yIHdpdGggdGhlIG9uZSBiZWxvdyAuLi4gcHV0IGEgbWV0aG9kIGluIExpdmVJbmZvP1xyXG4gICAgcHJpdmF0ZSB1cGRhdGVVcGdyYWRlVHJpYW5nbGVMaXZlSW5mb3MoKTogdm9pZCB7XHJcbiAgICAgICAgaWYgKCB0aGlzLmhvdmVyZWRUcmlhbmdsZSAmJiB0aGlzLmhvdmVyZWRUcmlhbmdsZS5kZWZlbnNlIDwgSW5nYW1lVHJpYW5nbGUuTUFYX0RFRkVOU0UgKSB7XHJcbiAgICAgICAgICAgIGxldCBjZW50ZXIgPSB0aGlzLmhvdmVyZWRUcmlhbmdsZS50cmlhbmdsZS5nZXRDZW50cm9pZCgpO1xyXG4gICAgICAgICAgICBsZXQgY29zdHMgPSBnZXREZWZlbnNlVXBncmFkZUNvc3RzKCB0aGlzLmhvdmVyZWRUcmlhbmdsZSApO1xyXG4gICAgICAgICAgICB0aGlzLmxpdmVJbmZvLnNldENvbG9yKCBjb3N0cyA8PSB0aGlzLnN0YXRlLmJhbGFuY2UgPyBCTEFDSy52YWwoKSA6IFJFRC52YWwoKSApO1xyXG4gICAgICAgICAgICB0aGlzLmxpdmVJbmZvLnNldEhpZGRlbiggZmFsc2UgKTtcclxuICAgICAgICAgICAgdGhpcy5saXZlSW5mby5zZXRBY3Rpb25JbmZvKCBcIlVwZ3JhZGUgZGVmZW5zZVwiLCBjb3N0cyApO1xyXG4gICAgICAgICAgICB0aGlzLmxpdmVJbmZvLm1vdmVUbyggY2VudGVyICk7XHJcbiAgICAgICAgfSBlbHNlIHRoaXMubGl2ZUluZm8uc2V0SGlkZGVuKCB0cnVlICk7XHJcbiAgICB9XHJcblxyXG4gICAgLyoqXHJcbiAgICAgKiBVcGRhdGVzIHRoZSBsaXZlIGluZm9zIGJhc2VkIG9uIHRoZSBwcmV2aWV3IHRyaWFuZ2xlLlxyXG4gICAgICovXHJcbiAgICBwcml2YXRlIHVwZGF0ZVByZXZpZXdUcmlhbmdsZUxpdmVJbmZvcygpOiB2b2lkIHtcclxuICAgICAgICBpZiAoIHRoaXMucHJldmlld1RyaWFuZ2xlICkge1xyXG4gICAgICAgICAgICBsZXQgY2VudGVyID0gdGhpcy5wcmV2aWV3VHJpYW5nbGUudHJpYW5nbGUuZ2V0Q2VudHJvaWQoKTtcclxuICAgICAgICAgICAgbGV0IGNvc3RzID0gZ2V0Q29zdHNGb3IoIHRoaXMucHJldmlld1RyaWFuZ2xlICk7XHJcbiAgICAgICAgICAgIHRoaXMubGl2ZUluZm8uc2V0Q29sb3IoIGNvc3RzIDw9IHRoaXMuc3RhdGUuYmFsYW5jZSA/IEJMQUNLLnZhbCgpIDogUkVELnZhbCgpICk7XHJcbiAgICAgICAgICAgIHRoaXMubGl2ZUluZm8uc2V0SGlkZGVuKCBmYWxzZSApO1xyXG4gICAgICAgICAgICB0aGlzLmxpdmVJbmZvLnNldEFjdGlvbkluZm8oIFwiQ3JlYXRlIHRyaWFuZ2xlXCIsIGNvc3RzICk7XHJcbiAgICAgICAgICAgIHRoaXMubGl2ZUluZm8ubW92ZVRvKCBjZW50ZXIgKTtcclxuICAgICAgICB9IGVsc2UgdGhpcy5saXZlSW5mby5zZXRIaWRkZW4oIHRydWUgKTtcclxuICAgIH1cclxuXHJcbiAgICAvKipcclxuICAgICAqIFJlbW92ZXMgYSBnaXZlbiB7QGNvZGUgVHJpYW5nbGV9IGZyb20gdGhlIGJvYXJkLlxyXG4gICAgICogQHBhcmFtIHRvUmVtb3ZlIHtAY29kZSBUcmlhbmdsZX0gdG8gYmUgcmVtb3ZlZFxyXG4gICAgICogQHJldHVybiB7QGNvZGUgdHJ1ZX0gaWYgdGhlIHtAY29kZSBUcmlhbmdsZX0gd2FzIHJlbW92ZWQgc3VjY2Vzc2Z1bGx5LCBvdGhlcndpc2Uge0Bjb2RlIGZhbHNlfVxyXG4gICAgICovXHJcbiAgICBwcml2YXRlIHJlbW92ZVNlcnZlclRyaWFuZ2xlKCB0b1JlbW92ZTogSW5nYW1lVHJpYW5nbGUgKTogYm9vbGVhbiB7XHJcbiAgICAgICAgbGV0IGV4aXN0aW5nOiBJbmdhbWVUcmlhbmdsZSA9IHRoaXMuZmluZEV4aXN0aW5nVHJpYW5nbGUoIHRvUmVtb3ZlIClcclxuICAgICAgICBpZiAoICFleGlzdGluZyApIHtcclxuICAgICAgICAgICAgcmV0dXJuIGZhbHNlO1xyXG4gICAgICAgIH1cclxuICAgICAgICBsZXQgbmV3T3V0bGluZTogRWRnZVtdID0gdG9SZW1vdmUudHJpYW5nbGUuZWRnZXMuZmlsdGVyKCB0ID0+IHQuZ2V0VHdpbkVkZ2UoKSApLm1hcCggdCA9PiB0LmdldFR3aW5FZGdlKCkgKTtcclxuICAgICAgICB0aGlzLnVwZGF0ZU91dGxpbmUoIHRvUmVtb3ZlLm93bmVyLCB0b1JlbW92ZS50cmlhbmdsZS5lZGdlcywgbmV3T3V0bGluZSApO1xyXG4gICAgICAgIHRoaXMuZHJvcEFuZFVucmVnaXN0ZXJUcmlhbmdsZSggZXhpc3RpbmcgKTtcclxuICAgICAgICByZXR1cm4gdHJ1ZTtcclxuICAgIH1cclxuXHJcbiAgICAvKipcclxuICAgICAqIENyZWF0ZXMgYSB7QGNvZGUgVHJpYW5nbGV9IHJlY2VpdmVkIGZyb20gdGhlIHNlcnZlciBvciB1cGRhdGVzIGEgZXhpc3Rpbmcgb25lLiBcclxuICAgICAqIEBwYXJhbSBhc3NpZ25lZFRyaWFuZ2xlIHRoZSB7QGNvZGUgVHJpYW5nbGV9IHJlY2VpdmVkIGZyb20gdGhlIHNlcnZlclxyXG4gICAgICogQHJldHVybiB7QGNvZGUgdHJ1ZX0gaWYgdGhlIHtAY29kZSBUcmlhbmdsZX0gd2FzIHN1Y2Nlc3NmdWxseSBhZGRlZCBvciB3YXMgdXNlZCBmb3IgYW4gdXBkYXRlLFxyXG4gICAgICogb3RoZXJ3aXNlIHtAY29kZSBmYWxzZX0gIFxyXG4gICAgICovXHJcbiAgICBwcml2YXRlIGFkZFNlcnZlclRyaWFuZ2xlKCBhc3NpZ25lZFRyaWFuZ2xlOiBJbmdhbWVUcmlhbmdsZSApOiBib29sZWFuIHtcclxuICAgICAgICBpZiAoICFhc3NpZ25lZFRyaWFuZ2xlIHx8ICFhc3NpZ25lZFRyaWFuZ2xlLm93bmVyIHx8ICFhc3NpZ25lZFRyaWFuZ2xlLnRyaWFuZ2xlLnJlbGF0ZWRFZGdlXHJcbiAgICAgICAgICAgIHx8IGFzc2lnbmVkVHJpYW5nbGUudHJpYW5nbGUuZWRnZXMuZmluZCggZSA9PiAhZSB8fCAhZS5wcmV2RWRnZSB8fCAhZS5uZXh0RWRnZSApICkge1xyXG4gICAgICAgICAgICB0aGlzLmZpbmFsaXplUmVzcG9uc2VIYW5kbGluZygpO1xyXG4gICAgICAgICAgICByZXR1cm4gZmFsc2U7XHJcbiAgICAgICAgfVxyXG5cclxuICAgICAgICBsZXQgZXhpc3RpbmcgPSB0aGlzLmZpbmRFeGlzdGluZ1RyaWFuZ2xlKCBhc3NpZ25lZFRyaWFuZ2xlIClcclxuICAgICAgICBpZiAoIGV4aXN0aW5nICkge1xyXG4gICAgICAgICAgICBleGlzdGluZy51cGRhdGVBdHRyaWJ1dGVzV2l0aCggYXNzaWduZWRUcmlhbmdsZSApO1xyXG4gICAgICAgICAgICByZXR1cm4gdHJ1ZTtcclxuICAgICAgICB9XHJcblxyXG5cclxuICAgICAgICBsZXQgdG9SZW1vdmU6IEVkZ2VbXSA9IG5ldyBBcnJheSgpO1xyXG4gICAgICAgIC8vZmluZCBwcm9wZXIgdHdpbiBlZGdlcyBpbiBvbGQgb3V0bGluZVxyXG4gICAgICAgIGxldCBvdXRsaW5lcyA9IHRoaXMuc3RhdGUuZ2V0TWVyZ2VkT3V0bGluZXMoKTtcclxuICAgICAgICBmb3IgKCBsZXQgb3V0bGluZUVkZ2Ugb2Ygb3V0bGluZXMgKSB7XHJcbiAgICAgICAgICAgIGZvciAoIGxldCBuZXdFZGdlIG9mIGFzc2lnbmVkVHJpYW5nbGUudHJpYW5nbGUuZWRnZXMgKSB7XHJcbiAgICAgICAgICAgICAgICBpZiAoIG5ld0VkZ2UuaXNQcm9wZXJUd2luRm9yKCBvdXRsaW5lRWRnZSApICkge1xyXG4gICAgICAgICAgICAgICAgICAgIGlmICggYXNzaWduZWRUcmlhbmdsZS5vd25lciAhPSBvdXRsaW5lRWRnZS5yZWxhdGVkVHJpYW5nbGUuaW5nYW1lVHJpYW5nbGUub3duZXIgKSB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIHRoaXMuZmluYWxpemVSZXNwb25zZUhhbmRsaW5nKCk7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIHJldHVybiBmYWxzZTtcclxuICAgICAgICAgICAgICAgICAgICB9IGVsc2Uge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICBuZXdFZGdlLmxpbmtBc1R3aW5zKCBvdXRsaW5lRWRnZSApO1xyXG4gICAgICAgICAgICAgICAgICAgICAgICB0b1JlbW92ZS5wdXNoKCBvdXRsaW5lRWRnZSApO1xyXG4gICAgICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgfVxyXG4gICAgICAgIH1cclxuXHJcblxyXG4gICAgICAgIGxldCB0b0FkZDogRWRnZVtdID0gbmV3IEFycmF5KCk7XHJcbiAgICAgICAgZm9yICggbGV0IGVkZ2Ugb2YgYXNzaWduZWRUcmlhbmdsZS50cmlhbmdsZS5lZGdlcyApIHtcclxuICAgICAgICAgICAgaWYgKCAhZWRnZS5nZXRUd2luRWRnZSgpICkgdG9BZGQucHVzaCggZWRnZSApO1xyXG4gICAgICAgIH1cclxuXHJcbiAgICAgICAgLy9kbyBub3QgY2hlY2sgZm9yIGZsb2F0aW5nIHRyaWFuZ2xlcyBhbnltb3JlIHNpbmNlIHRoaXMgY2FuIGhhcHBlbiBieSBkZWxldGlvbnNcclxuICAgICAgICAvLyBsZXQgdHJpYW5nbGVzT2ZQbGF5ZXI6IFRyaWFuZ2xlW10gPSB0aGlzLnN0YXRlLmdldFRyaWFuZ2xlc0J5T3duZXIoIGFzc2lnbmVkVHJpYW5nbGUub3duZXIgKS5maWx0ZXIoIHQgPT4gdC5vd25lciA9PT0gYXNzaWduZWRUcmlhbmdsZS5vd25lciApO1xyXG4gICAgICAgIC8vIGlmICggdHJpYW5nbGVzT2ZQbGF5ZXIubGVuZ3RoID4gMCAmJiB0b0FkZC5sZW5ndGggPiAyICkgeyAvLyBpbGxlZ2FsOiB0cmlhbmdsZSBpcyBub3QgY29ubmVjdGVkXHJcbiAgICAgICAgLy8gICAgIHRoaXMuZmluYWxpemVSZXNwb25zZUhhbmRsaW5nKCk7XHJcbiAgICAgICAgLy8gICAgIHJldHVybiBmYWxzZTtcclxuICAgICAgICAvLyB9XHJcbiAgICAgICAgdGhpcy5yZWdpc3RlclRyaWFuZ2xlKCBhc3NpZ25lZFRyaWFuZ2xlICk7XHJcbiAgICAgICAgdGhpcy51cGRhdGVPdXRsaW5lKCBhc3NpZ25lZFRyaWFuZ2xlLm93bmVyLCB0b1JlbW92ZSwgdG9BZGQgKTtcclxuICAgICAgICB0aGlzLmZpbmFsaXplUmVzcG9uc2VIYW5kbGluZygpO1xyXG4gICAgICAgIHJldHVybiB0cnVlO1xyXG4gICAgfVxyXG5cclxuICAgIC8qKlxyXG4gICAgICogUmVmcmVzaGVzIHRoZSBjYW52YXMgYW5kIGFsbG93cyBmdXJ0aGVyIHBsYXllciBhY3Rpb25zLlxyXG4gICAgICovXHJcbiAgICBwcml2YXRlIGZpbmFsaXplUmVzcG9uc2VIYW5kbGluZygpIHtcclxuICAgICAgICB0aGlzLmNhbnZhcy5kcmF3Q2FudmFzKCB0aGlzLnN0YXRlICk7XHJcbiAgICAgICAgdGhpcy53YWl0aW5nRm9yUmVzcG9uc2UgPSBmYWxzZTtcclxuICAgIH1cclxuXHJcblxyXG4gICAgLy9UT0RPIGRyb3AgdGhpcy5wcmV2aWV3VHJpYW5nbGUgb24gZmFpbHVyZSB0byBjcmVhdGUgYSBuZXcgb25lIC4uLiBzdGlsbCByZXF1aXJlZD9cclxuXHJcblxyXG4gICAgLyoqXHJcbiAgICAgKiBIaWdobGlnaHRzIHRoZSBwb2x5Z29uJ3Mgb3V0bGluZSBvZiBhIGdpdmVuIHBsYXllclxyXG4gICAgICogQHBhcmFtIHBsYXllciB3aG9zZSBwb2x5Z29uIG91dGxpbmUgc2hhbGwgYmUgaGlnaGxpZ2h0ZWRcclxuICAgICAqL1xyXG4gICAgaGlnaGxpZ2h0T3V0bGluZSggcGxheWVyOiBzdHJpbmcgKSB7XHJcbiAgICAgICAgZm9yICggbGV0IGVkZ2Ugb2YgdGhpcy5fc3RhdGUucGxheWVyT3V0bGluZSApIHtcclxuICAgICAgICAgICAgdGhpcy5jYW52YXMuaGlnaGxpZ2h0KCBlZGdlICk7XHJcbiAgICAgICAgfVxyXG4gICAgfVxyXG5cclxuICAgIC8qKlxyXG4gICAgICogUmV2ZXJ0cyB0aGUgaGlnbGlnaGluZyBvZiBhIHBsYXllcidzIHBvbHlnb24gb3V0bGluZVxyXG4gICAgICogQHBhcmFtIHBsYXllciB3aG9zZSBwb2x5Z29uIG91dGxpbmUgc2hhbGwgYmUgdW5oaWdobGlnaHRlZFxyXG4gICAgICovXHJcbiAgICB1bmhpZ2hsaWdodE91dGxpbmUoIHBsYXllcjogc3RyaW5nICkge1xyXG4gICAgICAgIGZvciAoIGxldCBlZGdlIG9mIHRoaXMuX3N0YXRlLnBsYXllck91dGxpbmUgKSB7XHJcbiAgICAgICAgICAgIHRoaXMuY2FudmFzLnVuaGlnaGxpZ2h0KCBlZGdlICk7XHJcbiAgICAgICAgfVxyXG4gICAgfVxyXG5cclxuICAgIC8qKlxyXG4gICAgICogVXBkYXRlcyB0aGUgb3V0bGluZSBvZiBhIHBsYXllcidzIHBvbHlnb24gYnkge0Bjb2RlIEVkZ2VzfSB3aGljaCBzaGFsbCBiZSByZW1vdmVkIG9yIGFkZGVkXHJcbiAgICAgKiBAcGFyYW0gcGxheWVyIHdob3NlIHBvbHlnb24gb3V0bGluZSBzaGFsbCBiZSB1cGRhdGVkXHJcbiAgICAgKiBAcGFyYW0gdG9SZW1vdmUge0Bjb2RlIEVkZ2VzfSB3aGljaCB3aWxsIGJlIHJlbW92ZWQgZnJvbSB0aGUgcGxheWVyJ3MgcG9seWdvbiBvdXRsaW5lXHJcbiAgICAgKiBAcGFyYW0gdG9BZGQge0Bjb2RlIEVkZ2VzfSB3aGljaCB3aWxsIGJlIGFkZGVkIHRvIHRoZSBwbGF5ZXIncyBwb2x5Z29uIG91dGxpbmVcclxuICAgICAqL1xyXG4gICAgcHJpdmF0ZSB1cGRhdGVPdXRsaW5lKCBwbGF5ZXI6IHN0cmluZywgdG9SZW1vdmU6IEVkZ2VbXSwgdG9BZGQ6IEVkZ2VbXSApIHtcclxuICAgICAgICAvLyBjb3VsZCBiZSBvcHRpbWl6ZWQgYnkgdXRpbGl6aW5nIHRoZSBzcGxpY2luZyBvcHRpb24gLi4uXHJcbiAgICAgICAgbGV0IG91dGxpbmU6IEVkZ2VbXSA9IHBsYXllciA9PT0gdGhpcy5fc3RhdGUucGxheWVySWQgPyB0aGlzLl9zdGF0ZS5wbGF5ZXJPdXRsaW5lIDogdGhpcy5fc3RhdGUub3RoZXJPdXRsaW5lO1xyXG4gICAgICAgIGZvciAoIGxldCBlZGdlIG9mIHRvUmVtb3ZlICkgcmVtb3ZlRnJvbUxpc3QoIGVkZ2UsIG91dGxpbmUgKTtcclxuICAgICAgICBpZiAoIHBsYXllciA9PT0gdGhpcy5fc3RhdGUucGxheWVySWQgKSB0aGlzLl9zdGF0ZS5wbGF5ZXJPdXRsaW5lID0gb3V0bGluZS5jb25jYXQoIHRvQWRkICk7XHJcbiAgICAgICAgZWxzZSB0aGlzLl9zdGF0ZS5vdGhlck91dGxpbmUgPSBvdXRsaW5lLmNvbmNhdCggdG9BZGQgKTtcclxuICAgIH1cclxuXHJcbiAgICAvKipcclxuICAgICAqIFN0YXJ0aW5nIGZyb20gYSBnaXZlbiB7QGNvZGUgRWRnZX0gYW4gZXhpc3RpbmcgdHdpbiB7QGNvZGUgRWRnZX0gY2FuZGlkYXRlIHdpbGwgYmUgc2VhcmNoZWQuXHJcbiAgICAgKiBAcGFyYW0gZWRnZSBhIHR3aW4ge0Bjb2RlIEVkZ2V9IHNoYWxsIGJlIHNlYXJjaGVkIGZvclxyXG4gICAgICogQHBhcmFtIG5leHRFZGdlU2VsZWN0aW9uRnVuY3Rpb24gZGVzY3JpYmVzIGhvdyB0aGUgbWVzaCBvZiB7QGNvZGUgRWRnZXN9IHdpbGwgYmUgdHJhdmVyZWQgdG8gZmluZCBhIHR3aW4uXHJcbiAgICAgKiBFeHBlY3RlZCB0byBlaXRoZXIgcmV0dXJuIHRoZSBwcmV2aW91cyBvciB0aGUgbmV4dCBjb25uZWN0ZWQge0Bjb2RlIEVkZ2V9IHRvIHdvcmsgcHJvcGVybHkuXHJcbiAgICAgKiBAcmV0dXJuIHRoZSBtYXRjaGluZyB0d2luIHtAY29kZSBFZGdlfSBjYW5kaWRhdGUsIGlmIGFueSwgb3RoZXJ3aXNlIHtAY29kZSBudWxsfVxyXG4gICAgICovXHJcbiAgICBwcml2YXRlIGZpbmRFeGlzdGluZ1R3aW4oIGVkZ2U6IEVkZ2UsIG5leHRFZGdlU2VsZWN0aW9uRnVuY3Rpb246ICggRWRnZSApID0+IEVkZ2UgKTogRWRnZSB7XHJcbiAgICAgICAgLy8gaWYgKGRlYnVnKSBmb3IgKHRyaWFuZ2xlIG9mIHRyaWFuZ2xlcykgY29uc29sZS5sb2codHJpYW5nbGUudG9TdHJpbmcoKSk7XHJcbiAgICAgICAgdGhpcy51bmhpZ2hsaWdodE91dGxpbmUoIHRoaXMuX3N0YXRlLnBsYXllcklkICk7XHJcbiAgICAgICAgLypcclxuICAgICAgICAgKiBpZiAoZGVidWcpeyBlZGdlLmRyYXcoQ29sb3IuUkVEKTsgZWRnZS5kcmF3KENvbG9yLkJMQUNLKTsgfVxyXG4gICAgICAgICAqL1xyXG4gICAgICAgIGxldCBjdXJyZW50RWRnZTogRWRnZSA9IG5leHRFZGdlU2VsZWN0aW9uRnVuY3Rpb24uY2FsbCggdGhpcywgZWRnZSApO1xyXG4gICAgICAgIGlmICggIWN1cnJlbnRFZGdlICkgcmV0dXJuIG51bGw7XHJcbiAgICAgICAgLypcclxuICAgICAgICAgKiBpZiAoZGVidWcgJiYgY3VycmVudEVkZ2UpIGN1cnJlbnRFZGdlLmRyYXcoQ29sb3IuUkVEKTsgaWYgKGRlYnVnICYmXHJcbiAgICAgICAgICogY3VycmVudEVkZ2UpIGN1cnJlbnRFZGdlLmRyYXcoQ29sb3IuQkxBQ0spO1xyXG4gICAgICAgICAqL1xyXG4gICAgICAgIHdoaWxlICggIWVkZ2UuaXNQcm9wZXJUd2luRm9yKCBjdXJyZW50RWRnZSApICkge1xyXG4gICAgICAgICAgICBpZiAoICFjdXJyZW50RWRnZS5nZXRUd2luRWRnZSgpICkgcmV0dXJuIG51bGw7XHJcbiAgICAgICAgICAgIGN1cnJlbnRFZGdlID0gbmV4dEVkZ2VTZWxlY3Rpb25GdW5jdGlvbi5jYWxsKCB0aGlzLCBjdXJyZW50RWRnZS5nZXRUd2luRWRnZSgpICk7XHJcbiAgICAgICAgICAgIC8vICAgICAgICAgICAgaWYgKCBkZWJ1ZyAmJiBjdXJyZW50RWRnZSApIHtcclxuICAgICAgICAgICAgLy8gICAgICAgICAgICAgICAgdGhpcy5jYW52YXMuaGlnaGxpZ2h0KGN1cnJlbnRFZGdlKTtcclxuICAgICAgICAgICAgLy8gICAgICAgICAgICAgICAgIGNvbnNvbGUubG9nKGN1cnJlbnRFZGdlLnRvU3RyaW5nKCkpO1xyXG4gICAgICAgICAgICAvLyAgICAgICAgICAgICAgICB0aGlzLmNhbnZhcy51bmhpZ2hsaWdodChjdXJyZW50RWRnZSk7XHJcbiAgICAgICAgICAgIC8vICAgICAgICAgICAgfVxyXG4gICAgICAgIH1cclxuICAgICAgICByZXR1cm4gY3VycmVudEVkZ2U7XHJcbiAgICB9XHJcblxyXG5cclxuICAgIC8qKlxyXG4gICAgICogU2VhcmNoZXMgZm9yIGFuIGV4aXN0aW5nIHtAY29kZSBUcmlhbmdsZX0gd2hpY2ggbWF0Y2hlcyB0aGUgZ2l2ZW4gb25lJ3MgZ2VvbWV0cnkgYW5kIG93bmVyLlxyXG4gICAgICogQHBhcmFtIGZpbmRFeGlzdGluZ0ZvciB7QGNvZGUgVHJpYW5nbGV9IGEgZXhpc3Rpbmcgb25lIHNoYWxsIGJlIHNlYXJjaGVkIGZvclxyXG4gICAgICogQHJldHVybiBhbiBtYXRjaGluZyBleGlzdGluZyB7QGNvZGUgVHJpYW5nbGV9LCBpZiBhbnksIG90aGVyd3NlIHtAY29kZSBudWxsfVxyXG4gICAgICovXHJcbiAgICBwcml2YXRlIGZpbmRFeGlzdGluZ1RyaWFuZ2xlKCBmaW5kRXhpc3RpbmdGb3I6IEluZ2FtZVRyaWFuZ2xlICk6IEluZ2FtZVRyaWFuZ2xlIHtcclxuICAgICAgICBmb3IgKCBsZXQgb3RoZXJUcmlhbmdsZSBvZiB0aGlzLnN0YXRlLmdldFRyaWFuZ2xlc0J5T3duZXIoIGZpbmRFeGlzdGluZ0Zvci5vd25lciApICkge1xyXG4gICAgICAgICAgICBpZiAoIGZpbmRFeGlzdGluZ0Zvci5lcXVhbHMoIG90aGVyVHJpYW5nbGUgKSApIHJldHVybiBvdGhlclRyaWFuZ2xlO1xyXG4gICAgICAgIH1cclxuICAgICAgICByZXR1cm4gbnVsbDtcclxuICAgIH1cclxufVxyXG5cclxuLyoqXHJcbiAqIFRoZSBmaWVsZCBkaXNwbGF5aW5nIGxpdmUgaW5mb3JtYXRpb24uXHJcbiAqL1xyXG5jbGFzcyBMaXZlSW5mbyB7XHJcbiAgICBwcml2YXRlIGxpdmVGaWVsZDogSFRNTERpdkVsZW1lbnQ7XHJcbiAgICBwcml2YXRlIHZlcmJvc2VGaWVsZDogSFRNTERpdkVsZW1lbnQ7XHJcbiAgICBvZmZzZXRYOiBudW1iZXI7XHJcbiAgICBvZmZzZXRZOiBudW1iZXI7XHJcbiAgICBib2FyZDogSFRNTENhbnZhc0VsZW1lbnQ7XHJcbiAgICBjb25zdHJ1Y3RvciggYm9hcmQ6IEhUTUxDYW52YXNFbGVtZW50ICkge1xyXG4gICAgICAgIGxldCBhbmNob3IgPSBkb2N1bWVudC5nZXRFbGVtZW50QnlJZCggXCJvZmZzZXRfYW5jaG9yXCIgKSBhcyBIVE1MRGl2RWxlbWVudDtcclxuICAgICAgICB0aGlzLnZlcmJvc2VGaWVsZCA9IGRvY3VtZW50LmdldEVsZW1lbnRCeUlkKCBcImxpdmVpbmZvXCIgKSBhcyBIVE1MRGl2RWxlbWVudDtcclxuICAgICAgICBsZXQgbGl2ZUZpZWxkID0gZG9jdW1lbnQuY3JlYXRlRWxlbWVudCggXCJkaXZcIiApIGFzIEhUTUxEaXZFbGVtZW50O1xyXG4gICAgICAgIGxpdmVGaWVsZC5zdHlsZS5iYWNrZ3JvdW5kID0gXCJ0cmFuc3BhcmVudFwiO1xyXG4gICAgICAgIGxpdmVGaWVsZC5zdHlsZS5jb2xvciA9IFwiYmxhY2tcIjtcclxuICAgICAgICBsaXZlRmllbGQuc3R5bGUucG9zaXRpb24gPSBcImFic29sdXRlXCI7XHJcbiAgICAgICAgbGl2ZUZpZWxkLnN0eWxlLnVzZXJTZWxlY3QgPSBcIm5vbmVcIjtcclxuICAgICAgICBsaXZlRmllbGQuc3R5bGUucG9pbnRlckV2ZW50cyA9IFwibm9uZVwiO1xyXG4gICAgICAgIGxpdmVGaWVsZC5hZGRFdmVudExpc3RlbmVyKCBcIm1vdXNldXBcIiwgKCkgPT4gZXZlbnQucHJldmVudERlZmF1bHQoKSApO1xyXG4gICAgICAgIGxpdmVGaWVsZC5hZGRFdmVudExpc3RlbmVyKCBcIm1vdXNlZG93blwiLCAoKSA9PiBldmVudC5wcmV2ZW50RGVmYXVsdCgpICk7XHJcbiAgICAgICAgbGl2ZUZpZWxkLmFkZEV2ZW50TGlzdGVuZXIoIFwibW91c2Vtb3ZlXCIsICgpID0+IGV2ZW50LnByZXZlbnREZWZhdWx0KCkgKTtcclxuICAgICAgICBsaXZlRmllbGQuYWRkRXZlbnRMaXN0ZW5lciggXCJjbGlja1wiLCAoKSA9PiBldmVudC5wcmV2ZW50RGVmYXVsdCgpICk7XHJcbiAgICAgICAgbGl2ZUZpZWxkLmFkZEV2ZW50TGlzdGVuZXIoIFwibW91c2VvdmVyXCIsICgpID0+IGV2ZW50LnByZXZlbnREZWZhdWx0KCkgKVxyXG4gICAgICAgIGRvY3VtZW50LmJvZHkuaW5zZXJ0QmVmb3JlKCBsaXZlRmllbGQsIGJvYXJkICk7XHJcbiAgICAgICAgdGhpcy5saXZlRmllbGQgPSBsaXZlRmllbGQ7XHJcbiAgICAgICAgdGhpcy5ib2FyZCA9IGJvYXJkO1xyXG4gICAgICAgIHRoaXMub2Zmc2V0WSA9IGFuY2hvci5vZmZzZXRUb3A7XHJcbiAgICAgICAgdGhpcy5vZmZzZXRYID0gYW5jaG9yLm9mZnNldExlZnQ7XHJcbiAgICAgICAgbGl2ZUZpZWxkLmhpZGRlbiA9IHRydWU7XHJcbiAgICB9XHJcbiAgICBzZXRIaWRkZW4oIGhpZGU6IGJvb2xlYW4gKTogdm9pZCB7XHJcbiAgICAgICAgdGhpcy5saXZlRmllbGQuaGlkZGVuID0gaGlkZTtcclxuICAgIH1cclxuICAgIGlzSGlkZGVuKCk6IGJvb2xlYW4ge1xyXG4gICAgICAgIHJldHVybiB0aGlzLmxpdmVGaWVsZC5oaWRkZW47XHJcbiAgICB9XHJcbiAgICBzZXRUZXh0KCB0ZXh0OiBhbnkgKTogdm9pZCB7XHJcbiAgICAgICAgdGhpcy5saXZlRmllbGQudGV4dENvbnRlbnQgPSBTdHJpbmcoIHRleHQgKTtcclxuICAgICAgICB0aGlzLnZlcmJvc2VGaWVsZC50ZXh0Q29udGVudCA9IFN0cmluZyggdGV4dCApO1xyXG4gICAgfVxyXG5cclxuICAgIHNldEFjdGlvbkluZm8oIGFjdGlvbjogc3RyaW5nLCBjb3N0czogbnVtYmVyICk6IHZvaWQge1xyXG4gICAgICAgIHRoaXMubGl2ZUZpZWxkLnRleHRDb250ZW50ID0gU3RyaW5nKCBjb3N0cyApO1xyXG4gICAgICAgIHRoaXMudmVyYm9zZUZpZWxkLnRleHRDb250ZW50ID0gYWN0aW9uICsgXCI6IFwiICsgU3RyaW5nKCBjb3N0cyApO1xyXG4gICAgfVxyXG4gICAgZ2V0VGV4dCgpOiBzdHJpbmcge1xyXG4gICAgICAgIHJldHVybiB0aGlzLmxpdmVGaWVsZC50ZXh0Q29udGVudDtcclxuICAgIH1cclxuICAgIHNldENvbG9yKCBjb2xvcjogc3RyaW5nICk6IHZvaWQge1xyXG4gICAgICAgIHRoaXMubGl2ZUZpZWxkLnN0eWxlLmNvbG9yID0gY29sb3I7XHJcbiAgICB9XHJcbiAgICAvL1RPRE8gc3RpbGwgbm90IHBlcmZlY3QgcG9zaXRpb25pbmdcclxuICAgIG1vdmVUbyggY2VudGVyOiBWZXJ0ZXggKTogdm9pZCB7XHJcbiAgICAgICAgbGV0IHBvc1ggPSBjZW50ZXIueCAtIHRoaXMub2Zmc2V0WCArIFwicHhcIjtcclxuICAgICAgICBsZXQgcG94WSA9IGNlbnRlci55IC0gdGhpcy5vZmZzZXRZICsgXCJweFwiO1xyXG4gICAgICAgIGxldCB0cmFucyA9IFwidHJhbnNsYXRlKFwiICsgcG9zWCArIFwiLFwiICsgcG94WSArIFwiKVwiO1xyXG4gICAgICAgIHRoaXMubGl2ZUZpZWxkLnN0eWxlLnRyYW5zZm9ybSA9IHRyYW5zO1xyXG4gICAgfVxyXG59XG5cblxuLy8gV0VCUEFDSyBGT09URVIgLy9cbi8vIC4vdHlwZXNjcmlwdC9ib2FyZF9oYW5kbGluZy9jYW52YXNfaGFuZGxlci50cyIsImltcG9ydCB7IEVkZ2UgfSBmcm9tIFwiLi4vbG9naWMvZ2VvbWV0cmljX29iamVjdHNcIjtcclxuaW1wb3J0IHsgQ2FudmFzSGFuZGxlciB9IGZyb20gXCIuL2NhbnZhc19oYW5kbGVyXCI7XHJcbmltcG9ydCB7IEluZ2FtZVRyaWFuZ2xlIH0gZnJvbSBcIi4vZ2FtZV9vYmplY3RzXCI7XHJcbmltcG9ydCB7IENvbG9yLCBHUkVFTiwgUkVEIH0gZnJvbSBcIi4uL3V0aWwvY29sb3JzXCI7XHJcblxyXG4vKipcclxuICogRGF0YSBjbGFzcyB3aGljaCBob2xkcyB0aGUgc3RhdGUgb2YgdGhlIGJvYXJkLlxyXG4gKi9cclxuZXhwb3J0IGNsYXNzIEJvYXJkU3RhdGUge1xyXG4gICAgLyoqVGhlIHBsYXllcidzIGlkLCByZWNlaXZlZCBmcm9tIHRoZSBzZXJ2ZXIqL1xyXG4gICAgcGxheWVySWQ6IHN0cmluZztcclxuICAgIC8qKlRoZSBjb2xvciByZXByZXNlbnRpbmcgdGhlIHBsYXllciovXHJcbiAgICBwbGF5ZXJDb2xvcjogQ29sb3IgPSBHUkVFTjtcclxuICAgIC8qKlRoZSBjb2xvciByZXByZXNlbnRpbmcgdGhlIG90aGVyIHBsYXllcnMqL1xyXG4gICAgb3RoZXJDb2xvcjogQ29sb3IgPSBSRUQ7XHJcbiAgICAvKip7QGNvZGUgVHJpYW5nbGVzfSBiZWxvbmcgdG8gdGhlIHBsYXllciAqL1xyXG4gICAgcGxheWVyVHJpYW5nbGVzOiBJbmdhbWVUcmlhbmdsZVtdID0gW107XHJcbiAgICAvKip7QGNvZGUgVHJpYW5nbGVzfSBiZWxvbmcgdG8gdGhlIG90aGVyIHBsYXllcnMgKi9cclxuICAgIG90aGVyVHJpYW5nbGVzOiBJbmdhbWVUcmlhbmdsZVtdID0gW107XHJcbiAgICAvKiogVGhlIHRoZSBvdXRsaW5lcyBvZiB0aGUgcGxheWVyJ3Mge0Bjb2RlIFRyaWFuZ2xlc30gKi9cclxuICAgIHBsYXllck91dGxpbmU6IEVkZ2VbXSA9IFtdO1xyXG4gICAgLyoqIFRoZSB0aGUgb3V0bGluZXMgb2YgYWxsIG90aGVyIHBsYXllcnMnIHtAY29kZSBUcmlhbmdsZXN9ICovXHJcbiAgICBvdGhlck91dGxpbmU6IEVkZ2VbXSA9IFtdO1xyXG4gICAgLyoqIFNob3VsZCBiZSBvbmx5IGFjY2Vzc2VkIGZvciBkZWJ1Z2dpbmcsIGkuZS4gaGlnaGxpZ2h0aW5nKi9cclxuICAgIGNhbnZhczogQ2FudmFzUmVuZGVyaW5nQ29udGV4dDJEO1xyXG4gICAgLyoqIE1vbmV5IG9mIHRoZSBwbGF5ZXIuKi9cclxuICAgIHByaXZhdGUgX2JhbGFuY2U6IG51bWJlciA9IDA7XHJcbiAgICBnZXQgYmFsYW5jZSgpOiBudW1iZXIgeyByZXR1cm4gdGhpcy5fYmFsYW5jZSB9XHJcbiAgICBzZXQgYmFsYW5jZSggYmFsYW5jZTogbnVtYmVyICkgeyB0aGlzLl9iYWxhbmNlID0gYmFsYW5jZTsgfVxyXG5cclxuXHJcbiAgICAvKipcclxuICAgICAqIFJldHVybnMgYWxsIHtAY29kZSBFZGdlc30gZnJvbSBhbGwgcGxheWVycyB0aGF0IHJlcHJlc2VudCBhbiBvdXRsaW5lLlxyXG4gICAgICogQHJldHVybiBMaXN0IG9mIGFsbCB7QGNvZGUgRWRnZXN9IGJlbG9uZ2luZyB0byBhbiBvdXRsaW5lXHJcbiAgICAgKi9cclxuICAgIGdldE1lcmdlZE91dGxpbmVzKCk6IEVkZ2VbXSB7XHJcbiAgICAgICAgcmV0dXJuIHRoaXMucGxheWVyT3V0bGluZS5jb25jYXQoIHRoaXMub3RoZXJPdXRsaW5lICk7XHJcbiAgICB9XHJcblxyXG4gICAgLyoqXHJcbiAgICAgKiBSZXR1cm5zIGFsbCB7QGNvZGUgVHJpYW5nbGVzfSBiZWxvbmdpbmcgdG8gdGhlIHBsYXllciBvciBhbGwge0Bjb2RlIFRyaWFuZ2xlc30gYmVsb25naW5nIHRvIHRoZSBvdGhlciBwbGF5ZXJzLFxyXG4gICAgICogZGVwZW5kaW5nIG9uIHRoZSByZXF1ZXN0ZWQgb3duZXIuXHJcbiAgICAgKiBAcGFyYW0gb3duZXIgdXNlZCB0byBkZXRlcm1pbmUgd2hldGhlcmUgdGhlIHBsYXllcidzIHtAY29kZSBUcmlhbmdsZXN9IG9yIHRoZSBvdGhlcnMnIHtAY29kZSBUcmlhbmdsZXN9IHNoYWxsIGJlIHJldHVybmVkLlxyXG4gICAgICogQHJldHVybiB0aGUge0Bjb2RlIFRyaWFuZ2xlc31cclxuICAgICAqL1xyXG4gICAgZ2V0VHJpYW5nbGVzQnlPd25lciggb3duZXI6IHN0cmluZyApOiBJbmdhbWVUcmlhbmdsZVtdIHtcclxuICAgICAgICByZXR1cm4gb3duZXIgPT09IHRoaXMucGxheWVySWQgPyB0aGlzLnBsYXllclRyaWFuZ2xlcyA6IHRoaXMub3RoZXJUcmlhbmdsZXM7XHJcbiAgICB9XHJcblxyXG4gICAgLyoqXHJcbiAgICAgKiBSZXR1cm5zIGFsbCBleGlzdGluZyB7QGNvZGUgRWRnZXN9LlxyXG4gICAgICogQHJldHVybiBhbGwgZXhpc3Rpbmcge0Bjb2RlIEVkZ2VzfS5cclxuICAgICAqL1xyXG4gICAgZ2V0QWxsRWRnZXMoIG93bmVyPzogc3RyaW5nICk6IEVkZ2VbXSB7XHJcbiAgICAgICAgbGV0IGxpc3QxID0gKCAhb3duZXIgfHwgb3duZXIgPT09IHRoaXMucGxheWVySWQgKSA/IHRoaXMucGxheWVyVHJpYW5nbGVzIDogW107XHJcbiAgICAgICAgbGV0IGxpc3QyID0gIW93bmVyID8gdGhpcy5vdGhlclRyaWFuZ2xlcyA6IFtdO1xyXG4gICAgICAgIHJldHVybiBsaXN0MS5jb25jYXQoIGxpc3QyICkubWFwKCB0ID0+IHQudHJpYW5nbGUuZWRnZXMgKS5yZWR1Y2UoKCB4LCB5ICkgPT4geC5jb25jYXQoIHkgKSwgW10gKTtcclxuICAgIH1cclxuXHJcbiAgICBnZXRBbGxPdGhlckVkZ2VzKCk6IEVkZ2VbXSB7XHJcbiAgICAgICAgcmV0dXJuIHRoaXMub3RoZXJUcmlhbmdsZXMubWFwKCB0ID0+IHQudHJpYW5nbGUuZWRnZXMgKS5yZWR1Y2UoKCB4LCB5ICkgPT4geC5jb25jYXQoIHkgKSwgW10gKTtcclxuICAgIH1cclxuXHJcbn1cblxuXG4vLyBXRUJQQUNLIEZPT1RFUiAvL1xuLy8gLi90eXBlc2NyaXB0L2JvYXJkX2hhbmRsaW5nL2JvYXJkX3N0YXRlLnRzIiwiaW1wb3J0IHsgVmVydGV4LCBFZGdlLCBHZW9tZXRyaWNPYmplY3QsIFRyaWFuZ2xlIH0gZnJvbSBcIi4uL2xvZ2ljL2dlb21ldHJpY19vYmplY3RzXCI7XHJcbmltcG9ydCB7IENvbG9yIH0gZnJvbSBcIi4uL3V0aWwvY29sb3JzXCI7XHJcbmltcG9ydCB7IFJFRCB9IGZyb20gXCIuLi91dGlsL2NvbG9yc1wiO1xyXG5pbXBvcnQgeyBkZWJ1ZyB9IGZyb20gXCIuLi9ydW4vbWFpblwiO1xyXG5pbXBvcnQgeyBCb2FyZFN0YXRlIH0gZnJvbSBcIi4vYm9hcmRfc3RhdGVcIjtcclxuaW1wb3J0IHsgSW5nYW1lVHJpYW5nbGUsIFByZXZpZXdUcmlhbmdsZSB9IGZyb20gXCIuL2dhbWVfb2JqZWN0c1wiO1xyXG5pbXBvcnQgeyBTdGFuZGFyZE9iamVjdCB9IGZyb20gXCIuLi91dGlsL3NoYXJlZFwiO1xyXG5cclxuLyoqXHJcbiAqIERyYXdzIHRoZSBlbGVtZW50cy5cclxuICovXHJcbmV4cG9ydCBjbGFzcyBDYW52YXMge1xyXG4gICAgcHJpdmF0ZSBjYW52YXNDb250ZXh0OiBDYW52YXNSZW5kZXJpbmdDb250ZXh0MkQ7XHJcbiAgICBwcml2YXRlIGNhbnZhczogSFRNTENhbnZhc0VsZW1lbnQ7XHJcbiAgICBwcml2YXRlIGJhY2tncm91bmRDb2xvcjogc3RyaW5nIHwgQ2FudmFzR3JhZGllbnQgfCBDYW52YXNQYXR0ZXJuO1xyXG4gICAgcHJpdmF0ZSBsaW5lQ29sb3I6IHN0cmluZyB8IENhbnZhc0dyYWRpZW50IHwgQ2FudmFzUGF0dGVybjtcclxuICAgIHByaXZhdGUgaGlnaGxpZ2h0Q29sb3I6IENvbG9yO1xyXG5cclxuICAgIGNvbnN0cnVjdG9yKCBjYW52YXM6IEhUTUxDYW52YXNFbGVtZW50LCBoaWdobGlnaHRDb2xvcj86IENvbG9yICkge1xyXG4gICAgICAgIHRoaXMuY2FudmFzQ29udGV4dCA9IGNhbnZhcy5nZXRDb250ZXh0KCBcIjJkXCIgKTtcclxuICAgICAgICB0aGlzLmNhbnZhcyA9IGNhbnZhcztcclxuICAgICAgICB0aGlzLmJhY2tncm91bmRDb2xvciA9IHRoaXMuY2FudmFzQ29udGV4dC5maWxsU3R5bGU7XHJcbiAgICAgICAgdGhpcy5saW5lQ29sb3IgPSB0aGlzLmNhbnZhc0NvbnRleHQuc3Ryb2tlU3R5bGU7XHJcbiAgICAgICAgdGhpcy5oaWdobGlnaHRDb2xvciA9IGhpZ2hsaWdodENvbG9yID8gaGlnaGxpZ2h0Q29sb3IgOiBSRUQ7XHJcbiAgICB9XHJcblxyXG4gICAgLyoqXHJcbiAgICAgKiBDbGVhbnMgYW5kIHJlZHJhd3MgdGhlIHdob2xlIGNhbnZhcy5cclxuICAgICAqL1xyXG4gICAgZHJhd0NhbnZhcyggc3RhdGU6IEJvYXJkU3RhdGUgKTogdm9pZCB7XHJcbiAgICAgICAgdGhpcy5jYW52YXNDb250ZXh0LmNsZWFyUmVjdCggMCwgMCwgdGhpcy5jYW52YXMud2lkdGgsIHRoaXMuY2FudmFzLmhlaWdodCApOyAvL3VzZSBiYWNrZ3JvdW5kQ29sb3I/XHJcbiAgICAgICAgZm9yICggbGV0IHRyaWFuZ2xlIG9mIHN0YXRlLnBsYXllclRyaWFuZ2xlcyApIHtcclxuICAgICAgICAgICAgdGhpcy5maWxsKCB0cmlhbmdsZSwgc3RhdGUucGxheWVyQ29sb3IgKTtcclxuICAgICAgICB9XHJcbiAgICAgICAgZm9yICggbGV0IHRyaWFuZ2xlIG9mIHN0YXRlLm90aGVyVHJpYW5nbGVzICkge1xyXG4gICAgICAgICAgICB0aGlzLmZpbGwoIHRyaWFuZ2xlLCBzdGF0ZS5vdGhlckNvbG9yICk7XHJcbiAgICAgICAgfVxyXG4gICAgfVxyXG5cclxuICAgIGRyYXcoIHRvRHJhdzogYW55LCBjb2xvcj86IENvbG9yICk6IHZvaWQge1xyXG4gICAgICAgIGlmICggdG9EcmF3IGluc3RhbmNlb2YgVmVydGV4ICkge1xyXG4gICAgICAgICAgICB0aGlzLmNhbnZhc0NvbnRleHQuZmlsbFJlY3QoIHRvRHJhdy54LCB0b0RyYXcueSwgMSwgMSApO1xyXG4gICAgICAgIH0gZWxzZSBpZiAoIHRvRHJhdyBpbnN0YW5jZW9mIEVkZ2UgKSB7XHJcbiAgICAgICAgICAgIGxldCBvbGRDb2xvciA9IHRoaXMuY2FudmFzQ29udGV4dC5zdHJva2VTdHlsZTtcclxuICAgICAgICAgICAgaWYgKCBjb2xvciApIHRoaXMuY2FudmFzQ29udGV4dC5zdHJva2VTdHlsZSA9IGNvbG9yLnZhbCgpO1xyXG4gICAgICAgICAgICAvLyAgICAgICAgICAgIHRoaXMuY2FudmFzQ29udGV4dC5saW5lV2lkdGggPSB0aGlzLmhpZ2hsaWdodENvbG9yID09IGNvbG9yICYmIGRlYnVnID4gMCA/IDUgOiAxO1xyXG4gICAgICAgICAgICB0aGlzLmNhbnZhc0NvbnRleHQuYmVnaW5QYXRoKCk7XHJcbiAgICAgICAgICAgIHRoaXMuY2FudmFzQ29udGV4dC5tb3ZlVG8oIHRvRHJhdy5zdGFydFZlcnRleC54LCB0b0RyYXcuc3RhcnRWZXJ0ZXgueSApO1xyXG4gICAgICAgICAgICB0aGlzLmNhbnZhc0NvbnRleHQubGluZVRvKCB0b0RyYXcubmV4dEVkZ2Uuc3RhcnRWZXJ0ZXgueCwgdG9EcmF3Lm5leHRFZGdlLnN0YXJ0VmVydGV4LnkgKTtcclxuICAgICAgICAgICAgdGhpcy5jYW52YXNDb250ZXh0LnN0cm9rZSgpO1xyXG4gICAgICAgICAgICB0aGlzLmNhbnZhc0NvbnRleHQuc3Ryb2tlU3R5bGUgPSBvbGRDb2xvcjtcclxuICAgICAgICB9IGVsc2UgaWYgKCB0b0RyYXcgaW5zdGFuY2VvZiBUcmlhbmdsZSApIHtcclxuICAgICAgICAgICAgZm9yICggbGV0IGVkZ2Ugb2YgdG9EcmF3LmVkZ2VzICkgdGhpcy5kcmF3KCBlZGdlLCBjb2xvciApO1xyXG4gICAgICAgIH0gZWxzZSBpZiAoIHRvRHJhdyBpbnN0YW5jZW9mIEluZ2FtZVRyaWFuZ2xlIHx8IHRvRHJhdyBpbnN0YW5jZW9mIFByZXZpZXdUcmlhbmdsZSApIHtcclxuICAgICAgICAgICAgdGhpcy5kcmF3KCB0b0RyYXcudHJpYW5nbGUsIGNvbG9yICk7XHJcbiAgICAgICAgfVxyXG4gICAgfVxyXG5cclxuICAgIGZpbGwoIHRvRmlsbDogVHJpYW5nbGUgfCBJbmdhbWVUcmlhbmdsZSB8IFByZXZpZXdUcmlhbmdsZSwgZmlsbENvbG9yOiBDb2xvciApOiB2b2lkIHtcclxuICAgICAgICBsZXQgc3RyZW5ndGggPSB0b0ZpbGwgaW5zdGFuY2VvZiBUcmlhbmdsZSA/IDAgOiB0b0ZpbGwuZGVmZW5zZTtcclxuICAgICAgICB0aGlzLmNhbnZhc0NvbnRleHQuZmlsbFN0eWxlID0gZmlsbENvbG9yLnZhbCggc3RyZW5ndGggKTtcclxuICAgICAgICBsZXQgdmVydGljZXMgPSB0b0ZpbGwgaW5zdGFuY2VvZiBUcmlhbmdsZSA/IHRvRmlsbC52ZXJ0aWNlcyA6IHRvRmlsbC50cmlhbmdsZS52ZXJ0aWNlcztcclxuICAgICAgICB0aGlzLmNhbnZhc0NvbnRleHQuYmVnaW5QYXRoKCk7XHJcbiAgICAgICAgdGhpcy5jYW52YXNDb250ZXh0Lm1vdmVUbyggdmVydGljZXNbMF0ueCwgdmVydGljZXNbMF0ueSApO1xyXG4gICAgICAgIHRoaXMuY2FudmFzQ29udGV4dC5saW5lVG8oIHZlcnRpY2VzWzFdLngsIHZlcnRpY2VzWzFdLnkgKTtcclxuICAgICAgICB0aGlzLmNhbnZhc0NvbnRleHQubGluZVRvKCB2ZXJ0aWNlc1syXS54LCB2ZXJ0aWNlc1syXS55ICk7XHJcbiAgICAgICAgdGhpcy5jYW52YXNDb250ZXh0LmNsb3NlUGF0aCgpO1xyXG4gICAgICAgIHRoaXMuY2FudmFzQ29udGV4dC5zdHJva2UoKTtcclxuICAgICAgICB0aGlzLmNhbnZhc0NvbnRleHQuZmlsbCgpO1xyXG4gICAgfVxyXG5cclxuICAgIGRyYXdMaW5lKCBmcm9tOiBWZXJ0ZXgsIHRvOiBWZXJ0ZXggKTogdm9pZCB7IC8vVE9ETyBzaW1pbGFyIHRvIGRyYXcgZWRnZVxyXG4gICAgICAgIHRoaXMuY2FudmFzQ29udGV4dC5tb3ZlVG8oIGZyb20ueCwgZnJvbS55ICk7XHJcbiAgICAgICAgdGhpcy5jYW52YXNDb250ZXh0LmxpbmVUbyggdG8ueCwgdG8ueSApO1xyXG4gICAgICAgIHRoaXMuY2FudmFzQ29udGV4dC5zdHJva2UoKTtcclxuICAgIH1cclxuXHJcbiAgICBoaWdobGlnaHQoIHRvRHJhdzogR2VvbWV0cmljT2JqZWN0ICk6IHZvaWQge1xyXG4gICAgICAgIGlmICggdG9EcmF3IGluc3RhbmNlb2YgVmVydGV4ICkge1xyXG4gICAgICAgICAgICB2YXIgb2xkQ29sb3IgPSB0aGlzLmNhbnZhc0NvbnRleHQuZmlsbFN0eWxlO1xyXG4gICAgICAgICAgICB0aGlzLmNhbnZhc0NvbnRleHQuZmlsbFN0eWxlID0gdGhpcy5oaWdobGlnaHRDb2xvci52YWwoKTtcclxuICAgICAgICAgICAgdGhpcy5jYW52YXNDb250ZXh0LmJlZ2luUGF0aCgpO1xyXG4gICAgICAgICAgICB0aGlzLmNhbnZhc0NvbnRleHQuYXJjKCB0b0RyYXcueCwgdG9EcmF3LnksIDUsIDAsIDIgKiBNYXRoLlBJICk7XHJcbiAgICAgICAgICAgIHRoaXMuY2FudmFzQ29udGV4dC5maWxsKCk7XHJcbiAgICAgICAgICAgIHRoaXMuY2FudmFzQ29udGV4dC5maWxsU3R5bGUgPSBvbGRDb2xvcjtcclxuICAgICAgICB9IGVsc2UgaWYgKCB0b0RyYXcgaW5zdGFuY2VvZiBFZGdlICkge1xyXG4gICAgICAgICAgICB0aGlzLmRyYXcoIHRvRHJhdywgdGhpcy5oaWdobGlnaHRDb2xvciApO1xyXG4gICAgICAgIH0gZWxzZSBpZiAoIHRvRHJhdyBpbnN0YW5jZW9mIEluZ2FtZVRyaWFuZ2xlICkge1xyXG4gICAgICAgICAgICB0aGlzLmRyYXcoIHRvRHJhdywgdGhpcy5oaWdobGlnaHRDb2xvciApO1xyXG4gICAgICAgIH1cclxuICAgIH1cclxuXHJcbiAgICB1bmhpZ2hsaWdodCggdG9VbmhpZ2hsaWdodDogR2VvbWV0cmljT2JqZWN0ICk6IHZvaWQge1xyXG4gICAgICAgIGlmICggdG9VbmhpZ2hsaWdodCBpbnN0YW5jZW9mIFZlcnRleCApIHtcclxuICAgICAgICAgICAgdGhyb3cgXCJVbmhpZ2hsaWdoaW5nIHZlcnRpY2VzIGlzIG5vdCBzdXBwb3J0ZWQuXCJcclxuICAgICAgICB9IGVsc2UgdGhpcy5kcmF3KCB0b1VuaGlnaGxpZ2h0ICk7XHJcbiAgICB9XHJcblxyXG59XG5cblxuLy8gV0VCUEFDSyBGT09URVIgLy9cbi8vIC4vdHlwZXNjcmlwdC9ib2FyZF9oYW5kbGluZy9jYW52YXMudHMiLCJpbXBvcnQgeyBWZXJ0ZXgsIEVkZ2UsIFRyaWFuZ2xlIH0gZnJvbSBcIi4uL2xvZ2ljL2dlb21ldHJpY19vYmplY3RzXCI7XHJcbmltcG9ydCB7IGRlYnVnLCBlcHNpbG9uLCBtaW5SYW5nZU11bHRpcGxpZXIgfSBmcm9tIFwiLi4vcnVuL21haW5cIjtcclxuaW1wb3J0IHsgQ2FudmFzIH0gZnJvbSBcIi4uL2JvYXJkX2hhbmRsaW5nL2NhbnZhc1wiO1xyXG5pbXBvcnQgeyBCb2FyZFN0YXRlIH0gZnJvbSBcIi4uL2JvYXJkX2hhbmRsaW5nL2JvYXJkX3N0YXRlXCI7XHJcblxyXG4vKipcclxuICogSG9sZHMgZnVuY3Rpb25hbGl0eSB0byBjcmVhdGUge0Bjb2RlIFRyaWFuZ2xlc30gYmFzZWQgb24gdGhlIHVzZXIgaW5wdXQgYW5kIHRoZSBjdXJyZW50IHtAY29kZSBCb2FyZFN0YXRlfS5cclxuICovXHJcbmV4cG9ydCBjbGFzcyBHZW9tZXRyeSB7XHJcbiAgICBwcml2YXRlIGNhbnZhczogQ2FudmFzO1xyXG5cclxuICAgIC8qKlxyXG4gICAgICogQ3JlYXRlcyBhIG5ldyB7QEdlb21ldHJ5fSBpbnN0YW5jZS5cclxuICAgICAqIEBwYXJhbSBjYW52YXMgbW9zdGx5IGZvciBkZWJ1ZywgYWxsb3dzIGhpZ2hsaWdodGluZyBzb21lIGVsZW1lbnRzXHJcbiAgICAgKi9cclxuICAgIGNvbnN0cnVjdG9yKCBjYW52YXM/OiBDYW52YXMgKSB7XHJcbiAgICAgICAgdGhpcy5jYW52YXMgPSBjYW52YXM7XHJcbiAgICB9XHJcblxyXG4gICAgLyoqXHJcbiAgICAgKiBDcmVhdGVzIGEgcHJldmlldyB7QGNvZGUgVHJpYW5nbGV9IGJhc2VkIG9uIHRoZSBsaW5lIHRoZSBwbGF5ZXIgaXMgZHJhd2luZy5cclxuICAgICAqIE5vdGUgdGhhdCBubyBpbi1kZXB0aCB2YWxpZGF0aW9uIGZvciBjb3N0cyBvciBjb2xsaXNpb25zIHdpdGgge0Bjb2RlIFRyaWFuZ2xlc30gd2lsbCBiZSB0YWtlblxyXG4gICAgICogaW50byBhY2NvdW50IGhlcmUgYW5kIHRoZXJlIG11c3QgYmUgZG9uZSBzZXBhcmF0bHksIGlmIHJlcXVpcmVkLiBcclxuICAgICAqIFRoaXMge0Bjb2RlIFRyaWFuZ2xlfSB3aWxsIHVzZSB0aGUgZmlyc3QgaW50ZXJzZWN0ZWQgZWRnZSBhcyB3ZWxsIGFzIGFueSBzZWNvbmQgb25lXHJcbiAgICAgKiAoaWYgYm90aCBhcmUgY29ubmVjdGVkIGFuZCB0aGVyZWZvcmUgZm9ybSB0d28gc2lkZXMgb2YgdGhlIHtAY29kZSBUcmlhbmdsZX0uIFxyXG4gICAgICovXHJcbiAgICBjcmVhdGVQcmV2aWV3VHJpYW5nbGUoIG1vdXNlU3RhcnQ6IFZlcnRleCwgbW91c2VFbmQ6IFZlcnRleCwgc3RhdGU6IEJvYXJkU3RhdGUgKTogVHJpYW5nbGUge1xyXG4gICAgICAgIGlmICggIW1vdXNlU3RhcnQgKSB7XHJcbiAgICAgICAgICAgIHJldHVybiBudWxsO1xyXG4gICAgICAgIH1cclxuICAgICAgICBsZXQgaW52b2x2ZWRFZGdlczogRWRnZUludGVyc2VjdGlvbnMgPSB0aGlzLmdldEVkZ2VJbnRlcnNlY3Rpb25JbmZvKFxyXG4gICAgICAgICAgICBuZXcgRWRnZSggbW91c2VTdGFydCwgbnVsbCwgbmV3IEVkZ2UoIG1vdXNlRW5kLCBudWxsLCBudWxsICkgKSxcclxuICAgICAgICAgICAgc3RhdGUgKTtcclxuICAgICAgICBsZXQgb3duRWRnZXMgPSBpbnZvbHZlZEVkZ2VzLmdldE93bkVuY2xvc2luZ0VkZ2VzKCk7XHJcbiAgICAgICAgLy9maW5kIGZpcnN0IG91dGxpbmUgZWRnZVxyXG4gICAgICAgIGxldCBzdGFydEVkZ2U6IEVkZ2UgPSBvd25FZGdlc1swXTtcclxuICAgICAgICAvLyBubyBlZGdlcyBpbnZvbHZlZCBvciBsaW5lIHN0YXJ0cyBpbiBhIGZvcmVpZ24gdHJpYW5nbGVcclxuICAgICAgICBpZiAoICFzdGFydEVkZ2UgKSB7XHJcbiAgICAgICAgICAgIHJldHVybiBudWxsO1xyXG4gICAgICAgIH1cclxuICAgICAgICBsZXQgY2xvc2VzdEVkZ2U6IEVkZ2UgPSBvd25FZGdlc1sxXTtcclxuXHJcbiAgICAgICAgLy8gaWYgdGhlcmUncyBvbmx5IGEgc3RhcnQgZWRnZSBhbiB0aGUgZGlzdGFuY2UgYmV0d2VlbiBzdGFydCBlZGdlIGFuZCBzdGFydCBlZGdlIGlzIHRvIHNtYWxsIC0+IHJldHVybiBudWxsIHRvIGF2b2lkIG1pY3JvIHRyaWFuZ2xlc1xyXG4gICAgICAgIGlmICggIWNsb3Nlc3RFZGdlICYmIHN0YXJ0RWRnZS5zcXJEaXN0YW5jZVRvUG9pbnQoIG1vdXNlRW5kICkgPD0gZXBzaWxvbiAqIG1pblJhbmdlTXVsdGlwbGllciApIHJldHVybiBudWxsO1xyXG5cclxuICAgICAgICBpZiAoIHN0YXJ0RWRnZS5zcXJEaXN0YW5jZVRvUG9pbnQoIG1vdXNlRW5kICkgPCBlcHNpbG9uICkge1xyXG4gICAgICAgICAgICByZXR1cm4gbnVsbDtcclxuICAgICAgICB9XHJcbiAgICAgICAgLy8gVE9ETyB1c2UgdmVydGljZXNUb0VkZ2VzXHJcbiAgICAgICAgbGV0IHByZXZpZXdUcmlhbmdsZTogVHJpYW5nbGU7XHJcbiAgICAgICAgbGV0IGZpcnN0RWRnZTogRWRnZTtcclxuICAgICAgICBsZXQgc2Vjb25kRWRnZTogRWRnZTtcclxuICAgICAgICBsZXQgdGhpcmRFZGdlOiBFZGdlO1xyXG4gICAgICAgIC8qKiB0aGUgZmlyc3QgZWRnZSBvZiB0d286IHRoZSBzdGFydEVkZ2UgYW5kIGFub3RoZXIgb25lLCBpZiBhbnkgKi9cclxuICAgICAgICBsZXQgZmlyc3RDb25uZWN0ZWRFZGdlOiBFZGdlO1xyXG5cclxuICAgICAgICAvLyBJZiB0aGVyZSdzIG5vIG90aGVyIGludGVyc2VjdGVkIGVkZ2UgdG8gY29ubmVjdCB3aXRoLCBzZWFyY2ggaW4gcHJveG1pdHlcclxuICAgICAgICBpZiAoICFjbG9zZXN0RWRnZSAmJiBlcHNpbG9uID4gMCApIHsgLy8gVE9ETyBhcHBseSBwcm94aW1pdHkgbG9naWMgYmVmb3JlP1xyXG4gICAgICAgICAgICBsZXQgaW5Qcm94aW1pdHk6IEVkZ2VbXSA9IHRoaXMuZmluZE5lYXJFZGdlcyggbW91c2VFbmQsIHN0YXRlICk7XHJcbiAgICAgICAgICAgIGZvciAoIGxldCBlZGdlIG9mIGluUHJveGltaXR5ICkge1xyXG4gICAgICAgICAgICAgICAgZmlyc3RDb25uZWN0ZWRFZGdlID0gdGhpcy5jaGVja0Nvbm5lY3Rpb24oIHN0YXJ0RWRnZSwgZWRnZSApO1xyXG4gICAgICAgICAgICAgICAgaWYgKCBmaXJzdENvbm5lY3RlZEVkZ2UgKSB7XHJcbiAgICAgICAgICAgICAgICAgICAgY2xvc2VzdEVkZ2UgPSBlZGdlO1xyXG4gICAgICAgICAgICAgICAgICAgIC8vIGludm9sdmVkRWRnZXNbMV0gPSBjbG9zZXN0RWRnZTtcclxuICAgICAgICAgICAgICAgICAgICBicmVhaztcclxuICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICAvLyBpZiB0aGVyZSBpcyBhbm90aGVyIGVkZ2UgbWV0IHdpdGggZG9lcyBub3Qgc2hhcmUgYSB2ZXJ0ZXg6XHJcbiAgICAgICAgICAgIC8vIGNoZWNrIGlmIGl0J3MgdmVydGl4IGlzIG5lYXIuIGlmIHRoYXQncyB0aGUgY2FzZSwgY29ubmVjdCB0b1xyXG4gICAgICAgICAgICAvLyB0aGlzIHZlcnRleCwgZWxzZSBkb24ndCBhbGxvdyB0aGUgZHJhd24gdHJpYW5nbGVcclxuICAgICAgICAgICAgaWYgKCBpblByb3hpbWl0eS5sZW5ndGggPiAwICYmICFjbG9zZXN0RWRnZSApIHtcclxuICAgICAgICAgICAgICAgIHByZXZpZXdUcmlhbmdsZSA9IHRoaXMudHJ5VG9Db25uZWN0V2l0aFZlcnRleCggc3RhcnRFZGdlLCBpblByb3hpbWl0eSwgc3RhdGUsIG1vdXNlRW5kICk7XHJcbiAgICAgICAgICAgICAgICBpZiAoICFwcmV2aWV3VHJpYW5nbGUgKSB7XHJcbiAgICAgICAgICAgICAgICAgICAgcmV0dXJuIG51bGw7XHJcbiAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgIH1cclxuICAgICAgICB9XHJcbiAgICAgICAgaWYgKCAhcHJldmlld1RyaWFuZ2xlICkgeyAvLyBpZiB0aGVyZSdzIG5vIHRyaWFuZ2xlIGNyZWF0ZWQgeWV0IC4uLlxyXG4gICAgICAgICAgICAvLyBpZiB0d28gb3V0bGluZXMgaW52b2x2ZWQgLT4gaWYgdGhleSBhcmUgY29ubmVjdGVkLCBjcmVhdGUgYSB0cmlhbmdsZVxyXG4gICAgICAgICAgICAvLyBmcm9tIHRoZXJlXHJcbiAgICAgICAgICAgIGlmICggY2xvc2VzdEVkZ2UgKSB7XHJcbiAgICAgICAgICAgICAgICBwcmV2aWV3VHJpYW5nbGUgPSB0aGlzLnRyeVRvQ29ubmVjdEludGVyc2VjdGlvbnMoIHN0YXJ0RWRnZSwgY2xvc2VzdEVkZ2UsIHN0YXRlICk7XHJcbiAgICAgICAgICAgIH0gZWxzZSB7XHJcbiAgICAgICAgICAgICAgICBmaXJzdEVkZ2UgPSBFZGdlLmNyZWF0ZVR3aW5Nb2RlbCggc3RhcnRFZGdlICk7XHJcbiAgICAgICAgICAgICAgICBzZWNvbmRFZGdlID0gbmV3IEVkZ2UoIHN0YXJ0RWRnZS5zdGFydFZlcnRleCwgZmlyc3RFZGdlLCBudWxsICk7XHJcbiAgICAgICAgICAgICAgICB0aGlyZEVkZ2UgPSBuZXcgRWRnZSggbW91c2VFbmQsIHNlY29uZEVkZ2UsIGZpcnN0RWRnZSApO1xyXG4gICAgICAgICAgICAgICAgZmlyc3RFZGdlLnByZXZFZGdlID0gdGhpcmRFZGdlO1xyXG4gICAgICAgICAgICAgICAgZmlyc3RFZGdlLm5leHRFZGdlID0gc2Vjb25kRWRnZTtcclxuICAgICAgICAgICAgICAgIHNlY29uZEVkZ2UubmV4dEVkZ2UgPSB0aGlyZEVkZ2U7XHJcbiAgICAgICAgICAgICAgICBwcmV2aWV3VHJpYW5nbGUgPSBuZXcgVHJpYW5nbGUoIGZpcnN0RWRnZSApO1xyXG4gICAgICAgICAgICB9XHJcbiAgICAgICAgfVxyXG5cclxuICAgICAgICAvLyBDaGVjayB3aGV0aGVyIHRoZSB0cmlhbmdsZSBpbnRlcnNlY3RzIHdpdGggYW5vdGhlciBvbmUuIElmXHJcbiAgICAgICAgLy8gdGhhdCdzIHRoZSBjYXNlIGRlcmVnaXN0ZXIgYWxsIHNldCB0d2lucyBhbmQgY2FuY2VsXHJcbiAgICAgICAgLy8gQ3JlYXRlIG5ldywgcmVnaXN0ZXJlZCB0cmlhbmdsZVxyXG4gICAgICAgIGlmICggIXByZXZpZXdUcmlhbmdsZSApIHtcclxuICAgICAgICAgICAgcmV0dXJuIG51bGw7XHJcbiAgICAgICAgfVxyXG4gICAgICAgIHJldHVybiBwcmV2aWV3VHJpYW5nbGU7XHJcbiAgICB9XHJcblxyXG4gICAgLyoqXHJcbiAgICAgKiBSZXR1cm5zIGFsbCB7QGNvZGUgRWRnZXN9IHdoaWNoIGFyZSBpbnRlcnNlY3RlZCwgaW4gdGhlIG9yZGVyIG9yIHRoZWlyIGludGVyc2VjdGlvbi4gVGhpcyBpbnRlcnNlY3Rpb25zIG1pZ2h0IHJlcHJlc2VudCB0aGUgZWRnZXMgb2YgYSBuZXcge0Bjb2RlIFRyaWFuZ2xlfS4gXHJcbiAgICAgKiBAcGFyYW0gZHJhd25FZGdlIGEgbGluZSB3aGljaCB3aWxsIGJlIGNoZWNrZWQgd2hldGhlciBpdCBpbnRlcnNlY3RzIGV4aXN0aW5nIHtAY29kZSBFZGdlc31cclxuICAgICAqIEByZXR1cm4gdGhlIGludGVyc2VjdGluZyB7QGNvZGUgRWRnZXN9LCBpbiB0aGUgb3JkZXIgb2YgdGhlaXIgaW50ZXJzZWN0aW9uXHJcbiAgICAgKi9cclxuICAgIC8vRklYTUUgdGhpcyBsaW5lIGlzIG5vdCBlbm91Z2ggdG8gdGVsbCBhYm91dCBhbGwgdHJpYW5nbGVzIHdoaWNoIG11c3QgYmUgZGVsZXRlZFxyXG4gICAgZ2V0RWRnZUludGVyc2VjdGlvbkluZm8oIGRyYXduRWRnZTogRWRnZSwgc3RhdGU6IEJvYXJkU3RhdGUgKTogRWRnZUludGVyc2VjdGlvbnMge1xyXG4gICAgICAgIC8vIFRPRE8gdXNlIHBsYXllcj8gTWF5YmU6IHVzZSBwbGF5ZXJPdXRsaW5lICsgYWxsIGVkZ2VzIG9mIG90aGVyIHBsYXllcidzIHRyaWFuZ2xlcyBcclxuICAgICAgICAvLyBzaW5jZSB0aGV5J3JlIGRlc3Ryb3lhYmxlLiBUaGVuOiByZXR1cm4gYWxsIG91dGxpbmVzLCBvcmRlcmQsIHRvIGFsbG93IGEgcG9zc2libGUgZGVzdHJ1Y3Rpb25cclxuICAgICAgICAvLyBvZiBmb3JlaWduIHRyaWFuZ2xlcy4gTW9yZSBwZXJmb3JtYW50OiBjaGVjayBhbGwgZW5lbWllcycgZWRnZXMgYXMgc29vbiBhcyBhbiBlbmVteSdzIG91dGxpbmVcclxuICAgICAgICAvLyBoYXMgYmVlbiBjcm9zc2VkLCBidXQgbm90IGJlZm9yZSAoPT5jb25jYXQgbGlzdCB3aXRoIGFsbCBmb3JlaW5nIG91dGxpbmUgZWRnZXMgd2l0aCB0aG9zZSB3aGljaCBoYXZlIGEgdHdpbilcclxuICAgICAgICBsZXQgYWxsRWRnZXM6IEVkZ2VbXSA9IHN0YXRlLnBsYXllck91dGxpbmUuY29uY2F0KCBzdGF0ZS5nZXRBbGxPdGhlckVkZ2VzKCkgKTtcclxuICAgICAgICBsZXQgc3RhcnRWZXJ0ZXggPSBkcmF3bkVkZ2Uuc3RhcnRWZXJ0ZXg7XHJcbiAgICAgICAgLy8gICAgICAgIGlmICggZGVidWcgKSBmb3IgKCBsZXQgZWRnZSBvZiBvdXRsaW5lICkgZWRnZS5kcmF3KCB0aGlzLmNhbnZhc0NvbnRleHQsIENvbG9yLlJFRCApO1xyXG4gICAgICAgIGxldCByZXN1bHQ6IEVkZ2VJbnRlcnNlY3Rpb25zID0gbmV3IEVkZ2VJbnRlcnNlY3Rpb25zKCBzdGF0ZS5wbGF5ZXJJZCApO1xyXG4gICAgICAgIC8vICAgICAgICBpZiAoIGRlYnVnICkgZm9yICggbGV0IGVkZ2Ugb2Ygb3V0bGluZSApIGVkZ2UuZHJhdyggdGhpcy5jYW52YXNDb250ZXh0LCBDb2xvci5CTEFDSyApO1xyXG4gICAgICAgIGZvciAoIGxldCBsaW5lIG9mIGFsbEVkZ2VzICkge1xyXG4gICAgICAgICAgICAvLyAgICAgICAgICAgIGlmICggZGVidWcgKSBsaW5lLmRyYXcoIHRoaXMuY2FudmFzQ29udGV4dCwgQ29sb3IuUkVEICk7XHJcbiAgICAgICAgICAgIGlmICggZHJhd25FZGdlLmlzSW50ZXJzZWN0aW5nKCBsaW5lICkgKSB7XHJcbiAgICAgICAgICAgICAgICBsZXQgZGlzdGFuY2U6IG51bWJlciA9IGRyYXduRWRnZS5zcXJEaXN0YW5jZVRvUG9pbnQoIHN0YXJ0VmVydGV4ICk7XHJcbiAgICAgICAgICAgICAgICByZXN1bHQucHJvY2Vzc0VkZ2UoIG5ldyBFZGdlQW5kRGlzdGFuY2UoIGxpbmUsIGRpc3RhbmNlICkgKTtcclxuICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICAvLyAgICAgICAgICAgIGlmICggZGVidWcgKSBsaW5lLmRyYXcoIHRoaXMuY2FudmFzQ29udGV4dCwgQ29sb3IuQkxBQ0sgKTtcclxuICAgICAgICB9XHJcbiAgICAgICAgcmV0dXJuIHJlc3VsdC5maW5hbGl6ZSgpO1xyXG4gICAgfVxyXG5cclxuICAgIC8qKlxyXG4gICAgICogU2VhcmNoZXMgZm9yIHtAY29kZSBFZGdlc30gY2xvc2UgdG8gYSBnaXZlbiB7QGNvZGUgVmVydGV4fS4gVXNlZCB0byBhY2hpZXZlIGEgXCJtYWduZXRpc21cIiBlZmZlY3Qgd2hlbiBkcmF3aW5nIHtAY29kZSBUcmlhbmdsZXN9IGNsb3NlIHRvIGV4aXN0aW5nIG9uZXMuXHJcbiAgICAgKiBAcGFyYW0gbW91c2VFbmQgdGhlIHtAY29kZSBWZXJ0ZXh9IGluIHdob3NlIHByb3hpbWl0eSB7QGNvZGUgRWRnZXN9IHdpbGwgYmUgc2VhcmNoZWRcclxuICAgICAqIEByZXR1cm4gYWxsIHtAY29kZSBFZGdlc30gb3duZWQgYnkgdGhlIGdpdmVuIHBsYXllciBpbiBwcm94aW1pdHkgb2YgdGhlIGdpdmVuIHtAY29kZSBWZXJ0ZXh9XHJcbiAgICAgKi9cclxuICAgIGZpbmROZWFyRWRnZXMoIG1vdXNlRW5kOiBWZXJ0ZXgsIHN0YXRlOiBCb2FyZFN0YXRlICk6IEVkZ2VbXSB7XHJcbiAgICAgICAgbGV0IGluUHJveGltaXR5OiBFZGdlW10gPSBbXTtcclxuICAgICAgICBmb3IgKCBsZXQgbGluZSBvZiBzdGF0ZS5wbGF5ZXJPdXRsaW5lICkge1xyXG4gICAgICAgICAgICAvLyAgICAgICAgICAgIGlmICggZGVidWcgPiAwICkgbGluZS5kcmF3KCB0aGlzLmNhbnZhc0NvbnRleHQsIENvbG9yLlJFRCApO1xyXG4gICAgICAgICAgICBsZXQgcHJveGltaXR5ID0gbGluZS5zcXJEaXN0YW5jZVRvUG9pbnQoIG1vdXNlRW5kICk7XHJcbiAgICAgICAgICAgIGlmICggcHJveGltaXR5IDwgZXBzaWxvbiApIGluUHJveGltaXR5LnB1c2goIGxpbmUgKTtcclxuICAgICAgICAgICAgLy8gICAgICAgICAgICBpZiAoIGRlYnVnID4gMCApIGxpbmUuZHJhdyggdGhpcy5jYW52YXNDb250ZXh0LCBDb2xvci5CTEFDSyApO1xyXG4gICAgICAgIH1cclxuICAgICAgICBpZiAoIGRlYnVnID4gMCAmJiB0aGlzLmNhbnZhcyApIGZvciAoIGxldCBlZGdlIG9mIGluUHJveGltaXR5ICkgdGhpcy5jYW52YXMuaGlnaGxpZ2h0KCBlZGdlICk7XHJcbiAgICAgICAgcmV0dXJuIGluUHJveGltaXR5O1xyXG4gICAgfVxyXG5cclxuICAgIC8qKlxyXG4gICAgICogQ2hlY2tzIHdoZXRoZXIgdHdvIGVkZ2VzIHNoYXJlIGEgdmVydGV4LlxyXG4gICAgICogQHBhcmFtIGVkZ2UxIHRoZSBmaXJzdCB7QGNvZGUgRWRnZX0sIGNoZWNrZWQgd2hldGhlciBpdCBpc3QgY29ubmVjdGVkIHdpdGggdGhlIHNlY29uZCBvbmVcclxuICAgICAqIEBwYXJhbSBlZGdlMiB0aGUgc2Vjb25kIHtAY29kZSBFZGdlfSwgY2hlY2tlZCB3aGV0aGVyIGl0IGlzdCBjb25uZWN0ZWQgd2l0aCB0aGUgZmlyc3Qgb25lXHJcbiAgICAgKiBAcmV0dXJuIFRoZSBmaXJzdCB7QGNvZGUgRWRnZX0gb2YgdGhlIGNoYWluLCBpZiBib3RoIHtAY29kZSBFZGdlc30gYXJlIGNvbm5lY3RlZCwgdGhlcndpc2Uge0Bjb2RlIG51bGx9XHJcbiAgICAgKi9cclxuICAgIGNoZWNrQ29ubmVjdGlvbiggZWRnZTE6IEVkZ2UsIGVkZ2UyOiBFZGdlICkge1xyXG4gICAgICAgIGlmICggZWRnZTEuc3RhcnRWZXJ0ZXguZXF1YWxzKCBlZGdlMi5uZXh0RWRnZS5zdGFydFZlcnRleCApICkgcmV0dXJuIGVkZ2UxO1xyXG4gICAgICAgIGVsc2UgaWYgKCBlZGdlMS5uZXh0RWRnZS5zdGFydFZlcnRleC5lcXVhbHMoIGVkZ2UyLnN0YXJ0VmVydGV4ICkgKSByZXR1cm4gZWRnZTI7XHJcbiAgICAgICAgZWxzZSByZXR1cm4gbnVsbDtcclxuICAgIH1cclxuXHJcbiAgICAvKipcclxuICAgICAqIENoZWNrcyBhbGwge0Bjb2RlIFRyaWFuZ2xlfSBmb3IgaW50ZXJzZWN0aW9uIGFuZCByZXR1cm5zIHRob3NlLlxyXG4gICAgICogQHBhcmFtIHRyaWFuZ2xlIHRvIGJlIGNoZWNrZWQgZm9yIGludGVyc2VjdGlvbiB3aXRoIGFueSB7QGNvZGUgVHJpYW5nbGVzfVxyXG4gICAgICogQHJldHVybiBhbGwge0Bjb2RlIFRyaWFuZ2xlc30gd2hpY2ggaW50ZXJzZWN0cyB3aXRoIHRoZSBnaXZlbiBvbmVcclxuICAgICAqL1xyXG4gICAgaW50ZXJzZWN0aW5nVHJpYW5nbGVzKCB0cmlhbmdsZTogVHJpYW5nbGUsIHN0YXRlOiBCb2FyZFN0YXRlICk6IFRyaWFuZ2xlW10ge1xyXG4gICAgICAgIGxldCBjb2xsaXNpb246IFRyaWFuZ2xlW10gPSBbXTtcclxuICAgICAgICBmb3IgKCBsZXQgZWRnZSBvZiBzdGF0ZS5nZXRBbGxFZGdlcygpICkge1xyXG4gICAgICAgICAgICBmb3IgKCBsZXQgdHJpYW5nbGVFZGdlIG9mIHRyaWFuZ2xlLmVkZ2VzICkge1xyXG4gICAgICAgICAgICAgICAgaWYgKCB0cmlhbmdsZUVkZ2UuaXNJbnRlcnNlY3RpbmcoIGVkZ2UgKSApIHsvL1RPRE8gc2V0cyB3b3VsZCBiZSBuaWNlIGhlcmVcclxuICAgICAgICAgICAgICAgICAgICBpZiAoIGNvbGxpc2lvbi5pbmRleE9mKCBlZGdlLnJlbGF0ZWRUcmlhbmdsZSApIDwgMCApIGNvbGxpc2lvbi5wdXNoKCBlZGdlLnJlbGF0ZWRUcmlhbmdsZSApO1xyXG4gICAgICAgICAgICAgICAgICAgIGJyZWFrO1xyXG4gICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICB9XHJcbiAgICAgICAgfVxyXG4gICAgICAgIHJldHVybiBjb2xsaXNpb247XHJcbiAgICB9XHJcblxyXG4gICAgLyoqXHJcbiAgICAgKiBUcmllcyB0byBmb3JtIGEge0Bjb2RlIFRyaWFuZ2xlfSBieSBcIm1hZ25ldGlzbVwiIHRvd2FyZHMgYSB7QGNvZGUgVmVydGV4fS4gVGhhdCBtZWFucywgaWYgcG9zc2libGUsIFxyXG4gICAgICogYSB7QGNvZGUgVHJpYW5nbGV9IHdpbGwgYmUgY3JlYXRlZCBieSBhIGdpdmVuIHtAY29kZSBFZGdlfSBhbmQgdHdvIG90aGVyIG9uZXMgY29ubmVjdGluZyB0byBhIHtAY29kZSBWZXJ0ZXh9IGluIHByb3hpbWl0eS5cclxuICAgICAqIEBwYXJhbSBzdGFydEVkZ2UgdGhlIGJhc2lzLiBNaWdodCBmb3JtIGEge0Bjb2RlIFRyaWFuZ2xlfSB0b2dldGhlciB3aXRoIGEge0Bjb2RlIFZlcnRleH0gd2hpY2ggZG9lcyBub3QgbGllIG9uIHRoaXMge0Bjb2RlIEVkZ2V9LiBcclxuICAgICAqIEBwYXJhbSBpblByb3hpbWl0eSB7QGNvZGUgRWRnZXN9IHdob3NlIHtAY29kZSBWZXJ0aWNlc30gd2lsbCBiZSBjaGVja2VkIHdoZXRoZXIgdGhleSBmb3JtIGEge0Bjb2RlIFRyaWFuZ2xlfSB0b2dldGhlciB3aXRoIHRoZSBnaXZlbiBzdGFydEVkZ2VcclxuICAgICAqIEByZXR1cm4gYSBwcmV2aWV3IHRyaWFuZ2xlIGZvcm1lZCBieSB0aGUgZ2l2ZW4gYmFzaXMge0Bjb2RlIEVkZ2V9IGFuZCB0aGUgY2xvc2VzdCB7QGNvZGUgVmVydGV4fSBpbiBwcm94aW1pdHksIGlmIGFueSwgb3RoZXJ3aXNlIHtAY29kZSBudWxsfVxyXG4gICAgICovXHJcbiAgICB0cnlUb0Nvbm5lY3RXaXRoVmVydGV4KCBzdGFydEVkZ2U6IEVkZ2UsIGluUHJveGltaXR5OiBFZGdlW10sIHN0YXRlOiBCb2FyZFN0YXRlLCBtb3VzZUVuZDogVmVydGV4ICk6IFRyaWFuZ2xlIHtcclxuICAgICAgICAvKiogZmlyc3QgZWRnZSBvZiB0aGUgdGhlIG5ld2x5IGNyZWF0ZWQgdHJpYW5nbGUsIGlmIGFueSAqL1xyXG4gICAgICAgIGxldCBmaXJzdEVkZ2U6IEVkZ2U7XHJcbiAgICAgICAgLyoqIHNlY29uZCBlZGdlIG9mIHRoZSB0aGUgbmV3bHkgY3JlYXRlZCB0cmlhbmdsZSwgaWYgYW55ICovXHJcbiAgICAgICAgbGV0IHNlY29uZEVkZ2U6IEVkZ2U7XHJcbiAgICAgICAgLyoqIHNlY29uZCBlZGdlIG9mIHRoZSB0aGUgbmV3bHkgY3JlYXRlZCB0cmlhbmdsZSwgaWYgYW55ICovXHJcbiAgICAgICAgbGV0IHRoaXJkRWRnZTogRWRnZTtcclxuXHJcbiAgICAgICAgbGV0IGNsb3Nlc3RDb21tb25WZXJ0ZXg6IFZlcnRleEFuZERpc3RhbmNlO1xyXG4gICAgICAgIC8qXHJcbiAgICAgICAgICogaWYgKGRlYnVnKSBkcmF3Q2FudmFzKCk7IGlmIChkZWJ1ZykgaW5Qcm94aW1pdHlbMF0uZHJhdyhDb2xvci5SRUQpOyBpZlxyXG4gICAgICAgICAqIChkZWJ1ZykgaW5Qcm94aW1pdHlbMF0uZHJhdyhDb2xvci5CTEFDSyk7XHJcbiAgICAgICAgICovXHJcbiAgICAgICAgZm9yICggbGV0IGVkZ2Ugb2YgaW5Qcm94aW1pdHkgKSB7XHJcbiAgICAgICAgICAgIC8qXHJcbiAgICAgICAgICAgICAqIGlmIChkZWJ1ZykgZWRnZS5kcmF3KENvbG9yLlJFRCk7IGlmIChkZWJ1ZykgY29uc29sZS5sb2coXCJlZGdlOiBcIiArXHJcbiAgICAgICAgICAgICAqIGVkZ2UudG9TdHJpbmcoKSk7XHJcbiAgICAgICAgICAgICAqL1xyXG4gICAgICAgICAgICBsZXQgZGlzdGFuY2U6IG51bWJlciA9IGVkZ2Uuc3RhcnRWZXJ0ZXguc3FyRGlzdGFuY2VUb1BvaW50KCBtb3VzZUVuZCApO1xyXG4gICAgICAgICAgICBpZiAoIGRpc3RhbmNlIDwgZXBzaWxvbiAqIDIgJiYgKCAhY2xvc2VzdENvbW1vblZlcnRleCB8fCBjbG9zZXN0Q29tbW9uVmVydGV4LmRpc3RhbmNlIDwgZGlzdGFuY2UgKSApXHJcbiAgICAgICAgICAgICAgICBjbG9zZXN0Q29tbW9uVmVydGV4ID0gbmV3IFZlcnRleEFuZERpc3RhbmNlKCBlZGdlLnN0YXJ0VmVydGV4LCBkaXN0YW5jZSApO1xyXG4gICAgICAgICAgICBlbHNlIHtcclxuICAgICAgICAgICAgICAgIGRpc3RhbmNlID0gZWRnZS5uZXh0RWRnZS5zdGFydFZlcnRleC5zcXJEaXN0YW5jZVRvUG9pbnQoIG1vdXNlRW5kICk7XHJcbiAgICAgICAgICAgICAgICBpZiAoIGRpc3RhbmNlIDwgZXBzaWxvbiAqIDIgJiYgKCAhY2xvc2VzdENvbW1vblZlcnRleCB8fCBjbG9zZXN0Q29tbW9uVmVydGV4LmRpc3RhbmNlIDwgZGlzdGFuY2UgKSApXHJcbiAgICAgICAgICAgICAgICAgICAgY2xvc2VzdENvbW1vblZlcnRleCA9IG5ldyBWZXJ0ZXhBbmREaXN0YW5jZSggZWRnZS5uZXh0RWRnZS5zdGFydFZlcnRleCwgZGlzdGFuY2UgKTtcclxuICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICAvLyBpZiAoZGVidWcpIGVkZ2UuZHJhdyhDb2xvci5CTEFDSyk7XHJcbiAgICAgICAgfVxyXG4gICAgICAgIGlmICggY2xvc2VzdENvbW1vblZlcnRleCApIHtcclxuICAgICAgICAgICAgLy8gaWYgKGRlYnVnKSBjb25zb2xlLmxvZyhcImNvbW1vblZlcnRleDogXCIgKyBjb21tb25WZXJ0ZXgudG9TdHJpbmcoKSk7XHJcbiAgICAgICAgICAgIGZpcnN0RWRnZSA9IEVkZ2UuY3JlYXRlVHdpbk1vZGVsKCBzdGFydEVkZ2UgKTtcclxuICAgICAgICAgICAgc2Vjb25kRWRnZSA9IG5ldyBFZGdlKCBzdGFydEVkZ2Uuc3RhcnRWZXJ0ZXgsIGZpcnN0RWRnZSwgbnVsbCApO1xyXG4gICAgICAgICAgICB0aGlyZEVkZ2UgPSBuZXcgRWRnZSggY2xvc2VzdENvbW1vblZlcnRleC52ZXJ0ZXgsIHNlY29uZEVkZ2UsIGZpcnN0RWRnZSApO1xyXG4gICAgICAgICAgICBmaXJzdEVkZ2UucHJldkVkZ2UgPSB0aGlyZEVkZ2U7XHJcbiAgICAgICAgICAgIGZpcnN0RWRnZS5uZXh0RWRnZSA9IHNlY29uZEVkZ2U7XHJcbiAgICAgICAgICAgIHNlY29uZEVkZ2UubmV4dEVkZ2UgPSB0aGlyZEVkZ2U7XHJcbiAgICAgICAgICAgIHJldHVybiBuZXcgVHJpYW5nbGUoIGZpcnN0RWRnZSApO1xyXG4gICAgICAgIH1cclxuICAgIH1cclxuXHJcbiAgICAvKipcclxuICAgICAqIEZvcm1zIGFuIHtAY29kZSBUcmlhbmdsZX0gYnkgdGhlIHR3byBnaXZlbiB7QGNvZGUgRWRnZXN9IHdoaWNoIGludGVyc2VjdGVkIHRoZSBkcmF3biBsaW5lLlxyXG4gICAgICogSWYgdGhlc2UgdHdvIHtAY29kZSBFZGdlc30gYXJlbid0IGNvbm5lY3RlZCBubyB7QGNvZGUgVHJpYW5nbGV9IHdpbGwgYmUgZm9ybWVkIGJ5IHRoaXMgbWV0aG9kLlxyXG4gICAgICogT3RoZXJ3aXNlIGEge0Bjb2RlIFRyaWFuZ2xlfSB3aWxsIGJlIGZvcm1lZC4gSWYgdGhlcmUgaXMgYSBmaXJzdCBlZGdlIGF2YWlsYWJsZSB3aGljaCB3b3JrcyBhcyB0aGlyZCB7QGNvZGUgRWRnZX0sXHJcbiAgICAgKiBpdCB3aWxsIGJlIHVzZWQgYXMgd2VsbC4gXHJcbiAgICAgKiBAcGFyYW0gaW52b2x2ZWRFZGdlcyB0aGUgdHdvIHtAY29kZSBFZGdlc30gd2hvIHdpbGwgdGFrZXMgYXMgdHdvIHNpZGVzIG9mIGEge0Bjb2RlIFRyaWFuZ2xlfSBpZiB0aGV5J3JlIGFyZSBjb25uZWN0ZWRcclxuICAgICAqIEByZXR1cm4gYSBwcmV2aWV3IHRyaWFuZ2xlIGZvcm1lZCBieSB0aGUgdHdvIHtAY29kZSBFZGdlc30sIGlmIGFueSwgb3RoZXJ3aXNlIHtAY29kZSBudWxsfVxyXG4gICAgICovXHJcbiAgICB0cnlUb0Nvbm5lY3RJbnRlcnNlY3Rpb25zKCBzdGFydEVkZ2U6IEVkZ2UsIGNsb3Nlc3RFZGdlOiBFZGdlLCBzdGF0ZTogQm9hcmRTdGF0ZSApOiBUcmlhbmdsZSB7XHJcbiAgICAgICAgaWYgKCAhKCBzdGFydEVkZ2UgJiYgY2xvc2VzdEVkZ2UgKSApIHJldHVybiBudWxsO1xyXG4gICAgICAgIGxldCBmaXJzdEVkZ2U6IEVkZ2U7XHJcbiAgICAgICAgbGV0IHNlY29uZEVkZ2U6IEVkZ2U7XHJcbiAgICAgICAgbGV0IHRoaXJkRWRnZTogRWRnZTtcclxuICAgICAgICBsZXQgZmlyc3RDb25uZWN0ZWRFZGdlOiBFZGdlID0gdGhpcy5jaGVja0Nvbm5lY3Rpb24oIHN0YXJ0RWRnZSwgY2xvc2VzdEVkZ2UgKVxyXG4gICAgICAgIC8vIGlmIHRoZSBhcmUgY29ubmVjdGVkLCBjbG9zZSB0aGUgZ2FwIGJldHdlZW4gdGhlbVxyXG4gICAgICAgIGlmICggZGVidWcgPiAwICYmIHRoaXMuY2FudmFzICkgeyB0aGlzLmNhbnZhcy5oaWdobGlnaHQoIHN0YXJ0RWRnZSApOyB0aGlzLmNhbnZhcy5oaWdobGlnaHQoIGNsb3Nlc3RFZGdlICk7IH07XHJcbiAgICAgICAgaWYgKCAhZmlyc3RDb25uZWN0ZWRFZGdlICkgcmV0dXJuIG51bGw7XHJcbiAgICAgICAgZWxzZSBpZiAoIHN0YXJ0RWRnZS5lcXVhbHMoIGZpcnN0Q29ubmVjdGVkRWRnZSApICkge1xyXG4gICAgICAgICAgICBmaXJzdEVkZ2UgPSBFZGdlLmNyZWF0ZVR3aW5Nb2RlbCggc3RhcnRFZGdlICk7XHJcbiAgICAgICAgICAgIHNlY29uZEVkZ2UgPSBFZGdlLmNyZWF0ZVR3aW5Nb2RlbCggY2xvc2VzdEVkZ2UgKTtcclxuICAgICAgICAgICAgdGhpcmRFZGdlID0gbmV3IEVkZ2UoIGNsb3Nlc3RFZGdlLnN0YXJ0VmVydGV4LCBzZWNvbmRFZGdlLCBmaXJzdEVkZ2UgKTtcclxuICAgICAgICB9IGVsc2UgaWYgKCBjbG9zZXN0RWRnZS5lcXVhbHMoIGZpcnN0Q29ubmVjdGVkRWRnZSApICkge1xyXG4gICAgICAgICAgICBmaXJzdEVkZ2UgPSBFZGdlLmNyZWF0ZVR3aW5Nb2RlbCggY2xvc2VzdEVkZ2UgKTtcclxuICAgICAgICAgICAgc2Vjb25kRWRnZSA9IEVkZ2UuY3JlYXRlVHdpbk1vZGVsKCBzdGFydEVkZ2UgKTtcclxuICAgICAgICAgICAgdGhpcmRFZGdlID0gbmV3IEVkZ2UoIHN0YXJ0RWRnZS5zdGFydFZlcnRleCwgc2Vjb25kRWRnZSwgZmlyc3RFZGdlICk7XHJcbiAgICAgICAgfSBlbHNlIHJldHVybiBudWxsO1xyXG4gICAgICAgIGZpcnN0RWRnZS5wcmV2RWRnZSA9IHRoaXJkRWRnZTtcclxuICAgICAgICBmaXJzdEVkZ2UubmV4dEVkZ2UgPSBzZWNvbmRFZGdlO1xyXG4gICAgICAgIHNlY29uZEVkZ2UucHJldkVkZ2UgPSBmaXJzdEVkZ2U7XHJcbiAgICAgICAgc2Vjb25kRWRnZS5uZXh0RWRnZSA9IHRoaXJkRWRnZTtcclxuICAgICAgICAvLyBjaGVjayBpZiBhIGhvbGUgYXMgYmVlbiBjbG9zZWQsIGkuZS4gdGhlcmUgaXMgYSAoeWV0IHVubGlua2VkKSB0d2luIGVkZ2UgZm9yIHRoZSB0aGlyZEVkZ2VcclxuICAgICAgICAvLyBUT0RPOiBBbHRlcm5hdGl2ZToganVzdCB3YWxrIHRoZSBvdXRsaW5lIGFuZCBjaGVjayBmb3IgbWF0Y2hpbmcgdHdpbiBlZGdlP1xyXG4gICAgICAgIC8vICAgICAgICBsZXQgcG9zc2libGVUd2luRWRnZTpFZGdlID0gdGhpcy5maW5kRXhpc3RpbmdUd2luKCB0aGlyZEVkZ2UsIGUgPT4gZS5uZXh0RWRnZSApO1xyXG4gICAgICAgIC8vICAgICAgICBpZiAocG9zc2libGVUd2luRWRnZSkgdGhpcmRFZGdlLnR3aW5FZGdlID0gdGhpcy5maW5kRXhpc3RpbmdUd2luKCB0aGlyZEVkZ2UsIGUgPT4gZS5wcmV2RWRnZSApO1xyXG4gICAgICAgIHJldHVybiBuZXcgVHJpYW5nbGUoIGZpcnN0RWRnZSApO1xyXG4gICAgfVxyXG5cclxuICAgIC8qKlxyXG4gICAgICogTm90IHVzZWQgYXRtLiBTdGFydGluZyBmcm9tIGEgb3V0bGluZSBlZGdlLCB0aGlzIG1ldGhvZCBjb2xsZWN0cyBhbGwgZWRnZXNcclxuICAgICAqIGZyb20gdGhlIG91dGxpbmUuXHJcbiAgICAgKiBcclxuICAgICAqIEBwYXJhbSByZWxhdGVkRWRnZVxyXG4gICAgICogICAgICAgICAgICBmcm9tIHRoZSBvdXRsaW5lXHJcbiAgICAgKiBAcmV0dXJucyBhcnJheSB3aXRoIGFsbCB7QGNvZGUgRWRnZXN9IGVzdGFibGlzaGluZyB0aGUgb3V0bGluZVxyXG4gICAgICovXHJcbiAgICBjYWxjdWxhdGVPdXRsaW5lKCBzdGFydEVkZ2U6IEVkZ2UsIHN0YXRlOiBCb2FyZFN0YXRlICk6IEVkZ2VbXSB7XHJcbiAgICAgICAgbGV0IG91dGxpbmU6IEVkZ2VbXSA9IFtzdGFydEVkZ2VdO1xyXG4gICAgICAgIGxldCBjdXJyZW50RWRnZTogRWRnZSA9ICFzdGFydEVkZ2UubmV4dEVkZ2UuZ2V0VHdpbkVkZ2UoKSA/IHN0YXJ0RWRnZS5uZXh0RWRnZSA6IHN0YXJ0RWRnZS5uZXh0RWRnZS5nZXRUd2luRWRnZSgpLm5leHRFZGdlO1xyXG4gICAgICAgIHdoaWxlICggIWN1cnJlbnRFZGdlLmVxdWFscyggc3RhcnRFZGdlICkgKSB7XHJcbiAgICAgICAgICAgIGlmICggZGVidWcgJiYgdGhpcy5jYW52YXMgKSB0aGlzLmNhbnZhcy5oaWdobGlnaHQoIGN1cnJlbnRFZGdlICk7XHJcbiAgICAgICAgICAgIG91dGxpbmUgPSBvdXRsaW5lLmNvbmNhdCggY3VycmVudEVkZ2UgKTtcclxuICAgICAgICAgICAgaWYgKCBkZWJ1ZyAmJiB0aGlzLmNhbnZhcyApIHRoaXMuY2FudmFzLnVuaGlnaGxpZ2h0KCBjdXJyZW50RWRnZSApO1xyXG4gICAgICAgICAgICBjdXJyZW50RWRnZSA9ICFjdXJyZW50RWRnZS5uZXh0RWRnZS5nZXRUd2luRWRnZSgpID8gY3VycmVudEVkZ2UubmV4dEVkZ2UgOiBjdXJyZW50RWRnZS5uZXh0RWRnZS5nZXRUd2luRWRnZSgpLm5leHRFZGdlO1xyXG4gICAgICAgIH1cclxuICAgICAgICByZXR1cm4gb3V0bGluZTtcclxuICAgIH1cclxuXHJcbiAgICAvKipcclxuICAgICAqIFJldHVybnMgdGhlIHtAY29kZSBUcmlhbmdsZX0gY29udGFpbmluZyBhIGdpdmVuIHtAY29kZSBWZXJ0ZXh9LlxyXG4gICAgICogQHBhcmFtIHZlcnRleCBhIGNvbnRhaW5pbmcge0Bjb2RlIFRyaWFuZ2xlfSBpcyByZXF1ZXN0ZWQgZm9yXHJcbiAgICAgKiBAcGFyYW0gY3VycmVudFBsYXllciB3aG9zZSB7QGNvZGUgVHJpYW5nbGVzfSBzaGFsbCBiZSBjaGVja2VkXHJcbiAgICAgKiBAcGFyYW0gdGhlIGNvbnRhaW5pbmcge0Bjb2RlIFRyaWFuZ2xlfSwgaWYgYW55LCBvdGhlcndpc2Uge0Bjb2RlIG51bGx9XHJcbiAgICAgKi9cclxuICAgIC8vVE9ETyBoYW5kbGUgY29sbGlzaW9ucyB3aXRoIHRyaWFuZ2xlcyBhbiBvdGhlciB3YXk/XHJcbiAgICBnZXRDb250YWluaW5nVHJpYW5nbGUoIHZlcnRleDogVmVydGV4LCBzdGF0ZTogQm9hcmRTdGF0ZSApOiBUcmlhbmdsZSB7XHJcbiAgICAgICAgZm9yICggbGV0IHRyaWFuZ2xlIG9mIHN0YXRlLnBsYXllclRyaWFuZ2xlcyApIHtcclxuICAgICAgICAgICAgbGV0IGdlb21ldHJpY1RyaWFuZ2xlID0gdHJpYW5nbGUudHJpYW5nbGU7XHJcbiAgICAgICAgICAgIGlmICgvKiB0cmlhbmdsZS5vd25lciA9PSBjdXJyZW50UGxheWVyICYmKi8gZ2VvbWV0cmljVHJpYW5nbGUuY29udGFpbnMoIHZlcnRleCApICkgcmV0dXJuIGdlb21ldHJpY1RyaWFuZ2xlO1xyXG4gICAgICAgIH1cclxuICAgICAgICBmb3IgKCBsZXQgdHJpYW5nbGUgb2Ygc3RhdGUub3RoZXJUcmlhbmdsZXMgKSB7XHJcbiAgICAgICAgICAgIGxldCBnZW9tZXRyaWNUcmlhbmdsZSA9IHRyaWFuZ2xlLnRyaWFuZ2xlO1xyXG4gICAgICAgICAgICBpZiAoIGdlb21ldHJpY1RyaWFuZ2xlLmNvbnRhaW5zKCB2ZXJ0ZXggKSApIHJldHVybiBnZW9tZXRyaWNUcmlhbmdsZTtcclxuICAgICAgICB9XHJcbiAgICAgICAgcmV0dXJuIG51bGw7XHJcbiAgICB9XHJcblxyXG4gICAgLyoqXHJcbiAgICAgKiBUYWtlcyB2ZXJ0aWNlcyBhbmQgYnVpbGRzIGVkZ2VzIGZyb20gdGhlbS4gQWxsIGVkZ2VzIHdpbGwgYmUgbGlua2VkIGluIHRoZWlyXHJcbiAgICAgKiBvcmRlciBhbmQgdGhlIGZpcnN0IHdpdGggdGhlIGxhc3Qgb25lLlxyXG4gICAgICogQHBhcmFtIHZlcnRpY2VzIGZvcm1pbmcge0Bjb2RlIEVkZ2VzfSBpbiB0aGUgb3JkZXIgdGhleSdyZSBwYXNzZWRcclxuICAgICAqIEByZXR1cm5zIGFycmF5IG9mIGNyZWF0ZWQgZWRnZXNcclxuICAgICAqL1xyXG4gICAgdmVydGljZXNUb0VkZ2VzKCAuLi52ZXJ0aWNlczogVmVydGV4W10gKTogRWRnZVtdIHtcclxuICAgICAgICBpZiAoIHZlcnRpY2VzLmxlbmd0aCA8IDMgKSByZXR1cm47XHJcbiAgICAgICAgbGV0IGVkZ2VzOiBFZGdlW10gPSBbXTtcclxuICAgICAgICBmb3IgKCBsZXQgaSA9IDA7IGkgPCB2ZXJ0aWNlcy5sZW5ndGg7IGkrKyApIHtcclxuICAgICAgICAgICAgZWRnZXMgPSBlZGdlcy5jb25jYXQoIG5ldyBFZGdlKCB2ZXJ0aWNlc1tpXSwgbnVsbCwgbnVsbCApICk7XHJcbiAgICAgICAgfVxyXG4gICAgICAgIGZvciAoIGxldCBpID0gMTsgaSA8IGVkZ2VzLmxlbmd0aCAtIDE7IGkrKyApIHtcclxuICAgICAgICAgICAgZWRnZXNbaV0ucHJldkVkZ2UgPSBlZGdlc1tpIC0gMV07XHJcbiAgICAgICAgICAgIGVkZ2VzW2ldLm5leHRFZGdlID0gZWRnZXNbaSArIDFdO1xyXG4gICAgICAgIH1cclxuICAgICAgICBlZGdlc1swXS5uZXh0RWRnZSA9IGVkZ2VzWzFdO1xyXG4gICAgICAgIGVkZ2VzWzBdLnByZXZFZGdlID0gZWRnZXNbZWRnZXMubGVuZ3RoIC0gMV07XHJcbiAgICAgICAgZWRnZXNbZWRnZXMubGVuZ3RoIC0gMV0ubmV4dEVkZ2UgPSBlZGdlc1swXTtcclxuICAgICAgICBlZGdlc1tlZGdlcy5sZW5ndGggLSAxXS5wcmV2RWRnZSA9IGVkZ2VzW2VkZ2VzLmxlbmd0aCAtIDJdO1xyXG4gICAgICAgIHJldHVybiBlZGdlcztcclxuICAgIH1cclxufVxyXG5cclxuXHJcbi8qKlxyXG4gKiBQdXJlIGRhdGEgY2xhc3MuIEhvbGRzIHRoZSBhbiB7QGNvZGUgRWRnZX0gYW4gdGhlIGRpc3RhbmNlIHRvd2FyZHMgaXQuIFVzZWQgZm9yIGNhbGN1bGF0aW9ucyBvZiBuZXcge0Bjb2RlIFRyaWFuZ2xlc30uXHJcbiAqL1xyXG5jbGFzcyBFZGdlQW5kRGlzdGFuY2Uge1xyXG4gICAgcHJpdmF0ZSBfZWRnZTogRWRnZTtcclxuICAgIHByaXZhdGUgX2Rpc3RhbmNlOiBudW1iZXI7XHJcblxyXG4gICAgZ2V0IGVkZ2UoKTogRWRnZSB7IHJldHVybiB0aGlzLl9lZGdlOyB9XHJcbiAgICBnZXQgZGlzdGFuY2UoKTogbnVtYmVyIHsgcmV0dXJuIHRoaXMuX2Rpc3RhbmNlOyB9XHJcblxyXG4gICAgY29uc3RydWN0b3IoIGVkZ2U6IEVkZ2UsIGRpc3RhbmNlOiBudW1iZXIgKSB7XHJcbiAgICAgICAgdGhpcy5fZWRnZSA9IGVkZ2U7XHJcbiAgICAgICAgdGhpcy5fZGlzdGFuY2UgPSBkaXN0YW5jZTtcclxuICAgIH1cclxufVxyXG4vKipcclxuICogUHVyZSBkYXRhIGNsYXNzLiBIb2xkcyBhbiB7QGNvZGUgVmVydGV4fSBhbmQgdGhlIGRpc3RhbmNlIHRvd2FyZHMgaXQuIFVzZWQgZm9yIGNhbGN1bGF0aW9ucyBvZiBuZXcge0Bjb2RlIFRyaWFuZ2xlc30uXHJcbiAqL1xyXG5jbGFzcyBWZXJ0ZXhBbmREaXN0YW5jZSB7XHJcbiAgICBwcml2YXRlIF92ZXJ0ZXg6IFZlcnRleDtcclxuXHJcbiAgICBnZXQgdmVydGV4KCk6IFZlcnRleCB7IHJldHVybiB0aGlzLl92ZXJ0ZXg7IH1cclxuICAgIGdldCBkaXN0YW5jZSgpOiBudW1iZXIgeyByZXR1cm4gdGhpcy5fZGlzdGFuY2U7IH1cclxuXHJcbiAgICBwcml2YXRlIF9kaXN0YW5jZTogbnVtYmVyO1xyXG4gICAgY29uc3RydWN0b3IoIHZlcnRleCwgZGlzdGFuY2UgKSB7XHJcbiAgICAgICAgdGhpcy5fdmVydGV4ID0gdmVydGV4O1xyXG4gICAgICAgIHRoaXMuX2Rpc3RhbmNlID0gZGlzdGFuY2U7XHJcbiAgICB9XHJcbn1cclxuXHIvKipcclxuICogSGVscGVyIGNsYXNzIHdoaWNoIHRlbGxzIGFib3V0IHRoZSBvbmUgb3IgdHdvIG93biB7QGNvZGUgRWRnZXN9IHdoaWNoIGFyZSByZWxldmFudCBcclxuICogdG8gY29uc3RpdHV0ZSBhIHtAY29kZSBUcmlhbmdsZX0gYXMgd2VsbCBhcyBhYm91dCBhZmZlY3RlZCBmb3JlaWduIHtAY29kZSBFZGdlc30gd2hpY2ggbWlnaHQgZ2V0IGRlc3Ryb3llZC4gXHJcbiAqL1xyXG5jbGFzcyBFZGdlSW50ZXJzZWN0aW9ucyB7XHJcbiAgICAvL1RPRE8gdXNlIEltbXV0YWJsZUpTIC0gU29ydGVkU2V0IGZvciBtb3JlIHBlcmZvcm1hbmNlP1xyXG4gICAgLyoqQWxsIG93biB7QGNvZGUgRWRnZXN9LCB1c2VkIHRvIGRldGVybWluZSB0aGUgb25lIG9yIHR3byByZWxldmFudCBvbmVzKi9cclxuICAgIHByaXZhdGUgb3duSW52b2x2ZWRFZGdlczogRWRnZUFuZERpc3RhbmNlW10gPSBbXTtcclxuICAgIC8qVGhlIG9uZSBvciB0d28gcmVsZXZhbnQgb3duIHtAY29kZSBFZGdlc30gdG8gY29uc3RpdHV0ZSBhIHtAY29kZSBUcmlhbmdsZX0qL1xyXG4gICAgcHJpdmF0ZSBvd25FbmNsb3NpbmdFZGdlczogW0VkZ2VBbmREaXN0YW5jZSwgRWRnZUFuZERpc3RhbmNlXTtcclxuICAgIC8qKkFsbCBmb3JlaW5ne0Bjb2RlIEVkZ2VzfS4qL1xyXG4gICAgcHJpdmF0ZSBmb3JlaWduSW52b2x2ZWRFZGdlczogRWRnZUFuZERpc3RhbmNlW10gPSBbXTtcclxuICAgIC8qKkFsbCBmb3JlaW5nIHtAY29kZSBFZGdlc30gd2hpY2ggbWlnaHQgYmUgYWZmZWN0ZWQgaWYgYSB7QGNvZGUgVHJpYW5nbGV9IHdpbGwgYmUgY3JlYXRlZC4qL1xyXG4gICAgcHJpdmF0ZSBmb3JlaWduRW5jbG9zZWRFZGdlczogRWRnZUFuZERpc3RhbmNlW107XHJcbiAgICAvKipEZXRlcm1pbmVzIHdoaWNoIHtAY29kZSBFZGdlc30gY291bnQgYXMgb3duIG9uZXMqL1xyXG4gICAgcHJpdmF0ZSBwbGF5ZXJJZDogc3RyaW5nO1xyXG4gICAgLyoqVGVsbHMgd2hldGhlciBtb3JlIHtAY29kZSBFZGdlc0FuZERpc3RhbmNlc30gY2FuIGJlIHByY29lc3NlZCBvciB3aGV0aGVyIGEgcmVzdWx0IGhhcyBhbHJlYWR5IGJlZW4gY2FsY3VsYXRlZCovXHJcbiAgICBwcml2YXRlIGZpbmFsaXplZDogYm9vbGVhbiA9IGZhbHNlO1xyXG4gICAgLyoqXHJcbiAgICAgKiBDcmVhdGVzIGEgbmV3IHtAY29kZSBFZGdlSW50ZXJzZWNpb25zfVxyXG4gICAgICogQHBhcmFtIHByb2Nlc3MgZGV0ZXJtaW5lcyB3aGljaCB7QGNvZGUgRWRnZXN9IGNvdW50IGFzIG93biBvbmVzXHJcbiAgICAgKi9cclxuICAgIGNvbnN0cnVjdG9yKCBwcm9jZXNzOiBzdHJpbmcgKSB7XHJcbiAgICAgICAgdGhpcy5wbGF5ZXJJZCA9IHByb2Nlc3M7XHJcbiAgICB9XHJcblxyXG4gICAgLyoqXHJcbiAgICAgKiBQdXRzIGFub3RoZXIge0Bjb2RlIEVkZ2VBbmREaXN0YW5jZX0gdG8gdGhlIGludGVybmFsIGRhdGEgbW9kZWwuXHJcbiAgICAgKiBAcGFyYW0gdG9Qcm9jZXNzIHRvIGJlIHByb2Nlc3NlZCBhbiBwdXQgdG8gdGhlIGRhdGEgbW9kZWxcclxuICAgICAqIEByZXR1cm4ge0Bjb2RlIHRoaXN9LCBmb3IgY2hhaW5pbmdcclxuICAgICAqIEB0aHJvd3MgRXhjZXB0aW9uIGlmIHRoaXMgaW5zdGFuY2UgaGFzIGFscmVhZHkgYmVlbiBmaW5hbGl6ZWQgYW5kIHRoZXJlZm9yZSBjYW4ndCBwcm9jZXNzIGFueSBuZXcgZGF0YS5cclxuICAgICAqL1xyXG4gICAgcHVibGljIHByb2Nlc3NFZGdlKCB0b1Byb2Nlc3M6IEVkZ2VBbmREaXN0YW5jZSApOiB2b2lkIHtcclxuICAgICAgICBpZiAoIHRoaXMuZmluYWxpemVkICkgdGhyb3cgXCJJbGxlZ2FsIFN0YXRlOiBhbHJlYWR5IGZpbmFsaXplZC5cIlxyXG4gICAgICAgIC8vICAgICAgaWYgKHRoaXMub3duSW52b2x2ZWRFZGdlc1swXT09bnVsbCYmdGhpcy5vd25JbnZvbHZlZEVkZ2VzWzFdPT1udWxsJiZ0aGlzLnBsYXllcklkIT10b1JlZ2lzdGVyLmVkZ2UucmVsYXRlZFRyaWFuZ2xlLm93bmVyKXtcclxuICAgICAgICBpZiAoIHRoaXMucGxheWVySWQgIT0gdG9Qcm9jZXNzLmVkZ2UucmVsYXRlZFRyaWFuZ2xlLmluZ2FtZVRyaWFuZ2xlLm93bmVyICkgdGhpcy5mb3JlaWduSW52b2x2ZWRFZGdlcy5wdXNoKCB0b1Byb2Nlc3MgKTtcclxuICAgICAgICBlbHNlIHRoaXMub3duSW52b2x2ZWRFZGdlcy5wdXNoKCB0b1Byb2Nlc3MgKTtcclxuICAgIH1cclxuXHJcbiAgICAvKipcclxuICAgICAqIERpc2FsbG93cyBmdXJ0aGVyIGl0ZW1zIHRvIGJlIHByb2Nlc3NlZCBhbmQgZGV0ZXJtaW5lcyB0aGUgcmVzdWx0cy5cclxuICAgICAqIEB0aHJvd3MgRXhjZXB0aW9uIGlmIHRoaXMgaW5zdGFuY2UgaGFzIGFscmVhZHkgYmVlbiBmaW5hbGl6ZWRcclxuICAgICAqIEByZXR1cm4ge0Bjb2RlIHRoaXN9LCBmb3IgY2hhaW5pbmdcclxuICAgICAqL1xyXG4gICAgcHVibGljIGZpbmFsaXplKCk6IEVkZ2VJbnRlcnNlY3Rpb25zIHtcclxuICAgICAgICBpZiAoIHRoaXMuZmluYWxpemVkICkgdGhyb3cgXCJJbGxlZ2FsIFN0YXRlOiBhbHJlYWR5IGZpbmFsaXplZC5cIlxyXG4gICAgICAgIHRoaXMuZmluYWxpemVkID0gdHJ1ZTtcclxuICAgICAgICBpZiAoICF0aGlzLmdldE93bkVuY2xvc2luZ0VkZ2VzKCkgKVxyXG4gICAgICAgICAgICB0aGlzLm93bkludm9sdmVkRWRnZXMgPSB0aGlzLm93bkludm9sdmVkRWRnZXMuc29ydCgoIGkxOiBFZGdlQW5kRGlzdGFuY2UsIGkyOiBFZGdlQW5kRGlzdGFuY2UgKSA9PiBpMS5kaXN0YW5jZSAtIGkyLmRpc3RhbmNlICk7XHJcbiAgICAgICAgbGV0IG93bkludm9sdmVkMTogRWRnZUFuZERpc3RhbmNlID0gdGhpcy5vd25JbnZvbHZlZEVkZ2VzWzBdO1xyXG4gICAgICAgIGxldCBvd25JbnZvbHZlZDI6IEVkZ2VBbmREaXN0YW5jZSA9IHRoaXMub3duSW52b2x2ZWRFZGdlc1sxXTtcclxuICAgICAgICBsZXQgb3duRW5jbG9zaW5nRWRnZXM6IFtFZGdlQW5kRGlzdGFuY2UsIEVkZ2VBbmREaXN0YW5jZV0gPSBbb3duSW52b2x2ZWQxLCBvd25JbnZvbHZlZDJdO1xyXG5cclxuICAgICAgICBpZiAoICFvd25JbnZvbHZlZDEgKSByZXR1cm4gdGhpcztcclxuICAgICAgICBsZXQgbWluID0gb3duSW52b2x2ZWQxLmRpc3RhbmNlO1xyXG4gICAgICAgIGxldCBtYXggPSBvd25JbnZvbHZlZDIgPyBvd25JbnZvbHZlZDIuZGlzdGFuY2UgOiBudWxsO1xyXG4gICAgICAgIGxldCBmb3JlaWduRW5jbG9zZWRFZGdlcztcclxuICAgICAgICB0cnkge1xyXG4gICAgICAgICAgICBmb3JlaWduRW5jbG9zZWRFZGdlcyA9IHRoaXMuZm9yZWlnbkludm9sdmVkRWRnZXMubWFwKCBlID0+IHsgaWYgKCBlLmRpc3RhbmNlIDwgbWluICkgdGhyb3cgXCJcIjsgZWxzZSByZXR1cm4gZSB9ICkuXHJcbiAgICAgICAgICAgICAgICBmaWx0ZXIoKCBlID0+IGUuZGlzdGFuY2UgPj0gbWluICYmICggIW1heCB8fCBlLmRpc3RhbmNlIDw9IG1heCApICkgKTtcclxuICAgICAgICB9IGNhdGNoICggZSApIHtcclxuICAgICAgICAgICAgcmV0dXJuIHRoaXM7IC8vbmFzdHkgd29ya2Fyb3VuZCB0byBzdG9wIGFzIHNvb24gYXMgaXRzIGNsZWFyIHRoZSBsaW5lIHN0YXJ0ZWQgbm90IGluIGFuIG93biB0cmlhbmdsZVxyXG4gICAgICAgIH1cclxuICAgICAgICB0aGlzLm93bkVuY2xvc2luZ0VkZ2VzID0gb3duRW5jbG9zaW5nRWRnZXM7XHJcbiAgICAgICAgdGhpcy5mb3JlaWduRW5jbG9zZWRFZGdlcyA9IGZvcmVpZ25FbmNsb3NlZEVkZ2VzO1xyXG4gICAgICAgIHJldHVybiB0aGlzO1xyXG4gICAgfVxyXG4gICAgLyoqXHJcbiAgICAgKiBSZXR1cm5zIHRoZSBvbmUgb3IgdHdvIG93biB7QGNvZGUgRWRnZXN9IHdoaWNoIG1pZ2h0IGNvbnN0aXR1dGUgYSB7QGNvZGUgVHJpYW5nbGV9LlxyXG4gICAgICogQHJldHVybiB0aGUge0Bjb2RlIEVkZ2Uocyl9IChvciBiZXR0ZXI6IHdob3NlIG5vdC15ZXQtZXhpc3RpbmctdHdpbnMpIHdoaWNoIG1pZ2h0IGNvbnN0aXR1dGUgYSB7QGNvZGUgVHJpYW5nbGV9LiAgXHJcbiAgICAgKiBAdGhyb3dzIEV4Y2VwdGlvbiBpZiB0aGlzIGluc3RhbmNlIGhhcyBub3QgYmVlbiBmaW5hbGl6ZWQgeWV0XHJcbiAgICAgKi9cclxuICAgIHB1YmxpYyBnZXRPd25FbmNsb3NpbmdFZGdlcygpOiBbRWRnZSwgRWRnZV0ge1xyXG4gICAgICAgIGlmICggIXRoaXMuZmluYWxpemVkICkgdGhyb3cgXCJJbGxlZ2FsIFN0YXRlOiBub3QgZmluYWxpemVkLlwiO1xyXG4gICAgICAgIGxldCBlMTogRWRnZUFuZERpc3RhbmNlID0gdGhpcy5vd25JbnZvbHZlZEVkZ2VzWzBdO1xyXG4gICAgICAgIGxldCBlMjogRWRnZUFuZERpc3RhbmNlID0gdGhpcy5vd25JbnZvbHZlZEVkZ2VzWzFdO1xyXG4gICAgICAgIHRoaXMub3duRW5jbG9zaW5nRWRnZXMgPSBbZTEsIGUyXTtcclxuICAgICAgICByZXR1cm4gW2UxID8gZTEuZWRnZSA6IG51bGwsIGUyID8gZTIuZWRnZSA6IG51bGxdO1xyXG4gICAgfVxyXG4gICAgLyoqXHJcbiAgICAgKiBSZXR1cm5zIGFsbCBmb3JlaWduIHtAY29kZSBFZGdlc30gd2hpY2ggbWlnaHQgYmUgZGVzdHJveWVkIGlmIHRoZSB7QGNvZGUgVHJpYW5nbGV9IGlzIGNyZWF0ZWQuXHJcbiAgICAgKiBAcmV0dXJuIGFsbCBmb3JlaWduIHtAY29kZSBFZGdlc30gd2hpY2ggbWlnaHQgYmUgZGVzdHJveWVkXHJcbiAgICAgKiBAdGhyb3dzIEV4Y2VwdGlvbiBpZiB0aGlzIGluc3RhbmNlIGhhcyBub3QgYmVlbiBmaW5hbGl6ZWQgeWV0XHJcbiAgICAgKi9cclxuICAgIHB1YmxpYyBnZXRGb3JlaWduRW5jbG9zZWRFZGdlKCk6IEVkZ2VbXSB7XHJcbiAgICAgICAgaWYgKCAhdGhpcy5maW5hbGl6ZWQgKSB0aHJvdyBcIklsbGVnYWwgU3RhdGU6IG5vdCBmaW5hbGl6ZWQuXCI7XHJcbiAgICAgICAgcmV0dXJuIHRoaXMuZm9yZWlnbkVuY2xvc2VkRWRnZXMubWFwKCBlID0+IGUuZWRnZSApO1xyXG4gICAgfVxyXG59XHJcblxuXG5cbi8vIFdFQlBBQ0sgRk9PVEVSIC8vXG4vLyAuL3R5cGVzY3JpcHQvbG9naWMvZ2VvbWV0cnkudHMiLCJcclxuLyoqXHJcbiAqIE5ldHdvcmsgY29tbXVuaWNhdGlvblxyXG4gKi9cclxuXHJcbmltcG9ydCB7IFZlcnRleCwgRWRnZSB9IGZyb20gXCIuLi9sb2dpYy9nZW9tZXRyaWNfb2JqZWN0c1wiO1xyXG5pbXBvcnQgeyBDYW52YXNIYW5kbGVyIH0gZnJvbSBcIi4uL2JvYXJkX2hhbmRsaW5nL2NhbnZhc19oYW5kbGVyXCI7XHJcbmltcG9ydCB7IGFwcGx5U2VydmVyVXBkYXRlLCBhcHBseVNlcnZlclJlc2V0LCBhcHBseVNlcnZlclJlZ2lzdGVyLCByZXN1bWUgfSBmcm9tIFwiLi4vcnVuL21haW5cIjtcclxuaW1wb3J0ICogYXMgU29ja0pTIGZyb20gJy4uLy4uL3Jlc291cmNlcy9zb2NranMuanMnO1xyXG5pbXBvcnQgKiBhcyBTdG9tcCBmcm9tICcuLi8uLi9yZXNvdXJjZXMvc3RvbXAuanMnO1xyXG5pbXBvcnQgeyBVcGRhdGVIYW5kbGVyLCBSZXNldEhhbmRsZXIsIEluaXRIYW5kbGVyLCBJbnZhbGlkQ29tbWFuZEhhbmRsZXIsIEFjY291bnRIYW5kbGVyIH0gZnJvbSBcIi4vY29tbWFuZF9oYW5kbGVyc1wiO1xyXG5pbXBvcnQgeyBUcmlhbmdsZUJlYW4gfSBmcm9tIFwiLi9uZXR3b3JrX2JlYW5zXCI7XHJcbmltcG9ydCB7IEluZ2FtZVRyaWFuZ2xlIH0gZnJvbSBcIi4uL2JvYXJkX2hhbmRsaW5nL2dhbWVfb2JqZWN0c1wiO1xyXG5cclxuLypcclxuICogIE5FVFdPUksgQ09NTVVOSUNBVElPTiBcclxuICovXHJcblxyXG5cclxuLyoqIEhvbGRzIHRoZSBhdmFpbGFibGUgc2VydmVyIGNvbW1hbmQgaGFuZGxlcnMuICovXHJcbmNvbnN0IENvbW1hbmRIYW5kbGVycyA9IFtuZXcgVXBkYXRlSGFuZGxlcigpLCBuZXcgUmVzZXRIYW5kbGVyKCksIG5ldyBJbml0SGFuZGxlcigpLCBuZXcgQWNjb3VudEhhbmRsZXIoKV07XHJcblxyXG4vKipCYXNlIHBhdGggZm9yIHJlcXVlc3RzKi9cclxuY29uc3Qgd2ViU29ja2V0UGF0aCA9IFwiL3RyaWdvdHktd2Vic29ja2V0XCI7XHJcbi8qKiBSZXF1ZXN0IHBhdGggdG8gcmVnaXN0ZXIgYXMgYSBuZXcgcGxheWVyKi9cclxuY29uc3QgcmVnaXN0ZXJQbGF5ZXJQYXRoID0gXCIvcmVnaXN0ZXJcIjtcclxuLyoqIFJlcXVlc3QgcGF0aCB0byByZXF1ZXN0IHRoZSBpbml0aWFsIGluZm9ybWF0aW9uKi9cclxuY29uc3QgcmVxdWVzdEluaXRJbmZvc1BhdGggPSBcIi9pbml0XCI7XHJcbi8qKiBSZXF1ZXN0IHBhdGggdG8gcHJvcG9zZSB7QGNvZGUgVHJpYW5nbGV9Ki9cclxuY29uc3QgcHJvcG9zZVRyaWFuZ2xlUGF0aCA9IFwiL3Byb3Bvc2UvdHJpYW5nbGVcIjtcclxuLyoqIFJlcXVlc3QgcGF0aCBmb3IgdGhlIGNvbXBsZXRlIGdhbWUgc3RhdGUqL1xyXG5jb25zdCByZXF1ZXN0Q29tcGxldGVTdGF0ZVBhdGggPSBcIi9mdWxsc3RhdGVcIjtcclxuLyoqUGF0aCB0byByZXF1ZXN0IGEgZ2FtZSByZXNldCovXHJcbmNvbnN0IG5ld0dhbWVQYXRoID0gXCIvZGVidWcvbmV3X2dhbWVcIjtcclxuLyoqIENoYW5uZWwgZm9yIHByaXZhdGUgbWVzc2FnZXMqL1xyXG5jb25zdCB3aGlzcGVyQ2hhbm5lbCA9IFwiL3VzZXIvcXVldWUvcmVwbHlcIjtcclxuLyoqIENoYW5uZWwgZm9yIHB1YmxpYywgaS5lLiBicm9hZGNhc3RlZCBtZXNzYWdlcyovXHJcbmNvbnN0IGJyb2FkY2FzdENoYW5uZWwgPSBcIi91cGRhdGVcIjtcclxuXHJcbmV4cG9ydCBpbnRlcmZhY2UgTmV0d29yayB7IH1cclxuXG4vKipcclxuICogSGFuZGxlcyByZXF1ZXN0cyBmb3IgdGhlIHNlcnZlciBhbmQgZGVsaXZlcnMgc2VydmVyIG1lc3NhZ2VzIHZpYSBjYWxsYmFja3MuXHJcbiAqL1xyXG5leHBvcnQgY2xhc3MgTmV0d29ya0Nvbm5lY3RvciBpbXBsZW1lbnRzIE5ldHdvcmsge1xyXG5cclxuICAgIC8qKkhvbGRzIHRoZSBjb25uZWN0aW9uIHRvIHRoZSBzZXJ2ZXIqL1xyXG4gICAgcHJpdmF0ZSBzb2NrZXQgPSBuZXcgU29ja0pTKCB3ZWJTb2NrZXRQYXRoICk7XHJcbiAgICAvKipIYW5kbGVzIHRoZSBjb21tdW5pY2F0aW9uIHZpYSB0aGUgU3RvbXAgcHJvdG9jb2wqL1xyXG4gICAgcHJpdmF0ZSBzdG9tcENsaWVudCA9IFN0b21wLlN0b21wLm92ZXIoIHRoaXMuc29ja2V0ICk7XHJcbiAgICAvKipIYW5kbGVzIHNlcnZlciBjb21tYW5kcyB3aGljaCBoYXZlIGNhdXNlZCBhbiBlcnJvciovXHJcbiAgICBwcml2YXRlIGRlZmF1bHRIYW5kbGVyID0gbmV3IEludmFsaWRDb21tYW5kSGFuZGxlcigpO1xyXG4gICAgLyoqQWRkaXRpb25hbCBoZWFkZXIgaW5mb3JtYXRpb24qL1xyXG4gICAgcHJpdmF0ZSBoZWFkZXI6IGFueSA9IHt9O1xyXG5cclxuICAgIC8qKlxyXG4gICAgICogSW5pdGlhbGlzZXMgdGhpcyBjb25uZWN0b3IgYW5kIGNhbGxzIHRoZSBnaXZlbiBtZXRob2Qgb25jZSB0aGUgY29ubmVjaW9uIGlzIGNvbXBsZXRlZC5cclxuICAgICAqIEBwYXJhbSByZWFkeUhhbmRsZXIgY2FsbGJhY2sgb25jZSB0aGUgY29ubmVjdGlvbiBpcyBlc3RhYmxpc2hlZFxyXG4gICAgICovXHJcbiAgICBwdWJsaWMgaW5pdCggcmVhZHlIYW5kbGVyOiAoIGJvb2xlYW4gKSA9PiB2b2lkICk6IHZvaWQge1xyXG4gICAgICAgIHRoaXMuc3RvbXBDbGllbnQuY29ubmVjdCggLyonZ3Vlc3QnLCAnZ3Vlc3QnKi90aGlzLmhlYWRlciwgKCBmcmFtZSApID0+IHtcclxuICAgICAgICAgICAgdGhpcy5zdWJzY3JpYmUoIHRoaXMsIHdoaXNwZXJDaGFubmVsICkuY2FsbCggZnJhbWUgKTtcclxuICAgICAgICAgICAgdGhpcy5zdWJzY3JpYmUoIHRoaXMsIGJyb2FkY2FzdENoYW5uZWwgKS5jYWxsKCBmcmFtZSApO1xyXG4gICAgICAgICAgICByZWFkeUhhbmRsZXIuY2FsbCggdGhpcywgdHJ1ZSApO1xyXG4gICAgICAgIH0gKTtcclxuICAgIH1cclxuICAgIC8qKlxyXG4gICAgICogU3Vic2NyaWJlcyB0byB0aGUgd2Vic29ja2V0J3MgY2hhbm5lbCBhbGwgdGhlIGNvbW11bmljYXRpb24gd2lsbCBiZSByZWFsaXplZCB3aXRoLlxyXG4gICAgICpAcGFyYW0gY29ubmVjdG9yIGhvbGRpbmcgdGhlIHdlYnNvY2tldC4gUmVxdWlyZWQgYmVjYXVzZSB7QGNvZGUgdGhpc30gd2lsbCBiZSBvdmVyd3JpdHRlbiB3aGVuIHRoaXMgbWV0aG9kIGlzIGludm9rZWQgdmlhIGNhbGxiYWNrLlxyXG4gICAgICovXHJcbiAgICBwcml2YXRlIHN1YnNjcmliZSggY29ubmVjdG9yOiBOZXR3b3JrQ29ubmVjdG9yLCBjaGFubmVsOiBzdHJpbmcgKSB7XHJcbiAgICAgICAgcmV0dXJuIGZ1bmN0aW9uKCBmcmFtZSApIHtcclxuICAgICAgICAgICAgY29uc29sZS5sb2coICdDb25uZWN0ZWQ6ICcgKyBmcmFtZSApO1xyXG4gICAgICAgICAgICAvL1RPRE8gdXNlIHN1YnNjcmlwdGlvbj9cclxuICAgICAgICAgICAgbGV0IHN1YmNyaXB0aW9uID0gY29ubmVjdG9yLnN0b21wQ2xpZW50LnN1YnNjcmliZSggY2hhbm5lbCwgZnVuY3Rpb24oIHVwZGF0ZU9iamVjdDogYW55ICkge1xyXG4gICAgICAgICAgICAgICAgdHJ5IHtcclxuICAgICAgICAgICAgICAgICAgICBsZXQgcmVzcG9uc2U6IGFueSA9IEpTT04ucGFyc2UoIHVwZGF0ZU9iamVjdC5ib2R5ICk7XHJcbiAgICAgICAgICAgICAgICAgICAgbGV0IGhhbmRsZXIgPSBDb21tYW5kSGFuZGxlcnMuZmluZCggaCA9PiBoLmdldFR5cGUoKSA9PT0gcmVzcG9uc2UudHlwZSApO1xyXG4gICAgICAgICAgICAgICAgICAgIGlmICggIWhhbmRsZXIgKSB0aHJvdyBcIk5vIGhhbmRsZXIgZm9yIHJlc3BvbnNlIHR5cGUgXCIgKyByZXNwb25zZS50eXBlO1xyXG4gICAgICAgICAgICAgICAgICAgIGVsc2UgaGFuZGxlci5wcm9jZXNzKCByZXNwb25zZSApO1xyXG4gICAgICAgICAgICAgICAgfSBjYXRjaCAoIGUgKSB7XHJcbiAgICAgICAgICAgICAgICAgICAgY29uc29sZS5sb2coIFwiUGFyc2luZyB0aGUgcmVzcG9uc2UgZmFpbGVkOiBcIiArIGUgKTtcclxuICAgICAgICAgICAgICAgICAgICBjb25uZWN0b3IuZGVmYXVsdEhhbmRsZXIucHJvY2VzcygpO1xyXG4gICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICB9ICk7XHJcbiAgICAgICAgfVxyXG4gICAgfVxyXG5cclxuICAgIC8qKlxyXG4gICAgICogUmVnaXN0ZXJzIGEgbmV3IHBsYXllciBmb3IgdGhlIGdhbWUuXHJcbiAgICAgKi9cclxuICAgIHB1YmxpYyByZWdpc3RlclBsYXllcigpIHtcclxuICAgICAgICB0cnkge1xyXG4gICAgICAgICAgICB0aGlzLnN0b21wQ2xpZW50LnNlbmQoIHJlZ2lzdGVyUGxheWVyUGF0aCwgdGhpcy5oZWFkZXIsIG51bGwgKTtcclxuICAgICAgICB9IGNhdGNoICggZSApIHtcclxuICAgICAgICAgICAgY29uc29sZS5sb2coIFwiUmVnaXN0ZXJpbmcgZmFpbGVkOiBcIiArIGUgKTtcclxuICAgICAgICAgICAgcmVzdW1lKCk7XHJcbiAgICAgICAgfVxyXG4gICAgfVxyXG5cclxuICAgIC8qKlByb3Bvc2VzIGEge0Bjb2RlIFRyaWFuZ2xlfSB0byB0aGUgc2VydmVyIHdoaWNoIG1pZ2h0IGFwcGVhciBpbiB0aGUgbmV4dCB7QGNvZGUgVXBkYXRlQ29tbWFuZH0gZnJvbSB0aGUgc2VydmVyLlxyXG4gICAgICpAdG9SZXF1ZXN0IHRoZSB7QGNvZGUgVHJpYW5nbGV9IHdoaWNoIHNoYWxsIGJlIHByb3Bvc2VkXHJcbiAgICAgKi9cclxuICAgIHB1YmxpYyBwcm9wb3NlVHJpYW5nbGUoIHRvUmVxdWVzdDogSW5nYW1lVHJpYW5nbGUgKSB7XHJcbiAgICAgICAgaWYgKCAhdG9SZXF1ZXN0ICkgcmVzdW1lKCk7XHJcbiAgICAgICAgdHJ5IHtcclxuICAgICAgICAgICAgbGV0IHJlcXVlc3RPYmo6IFRyaWFuZ2xlQmVhbiA9IFRyaWFuZ2xlQmVhbi5vZiggdG9SZXF1ZXN0ICk7XHJcbiAgICAgICAgICAgIHRoaXMuc3RvbXBDbGllbnQuc2VuZCggcHJvcG9zZVRyaWFuZ2xlUGF0aCwgdGhpcy5oZWFkZXIsIEpTT04uc3RyaW5naWZ5KCByZXF1ZXN0T2JqICkgKTtcclxuICAgICAgICB9IGNhdGNoICggZSApIHtcclxuICAgICAgICAgICAgY29uc29sZS5sb2coIFwiUmVxdWVzdGluZyB0aGUgdGhlIHRyaWFuZ2xlIFwiICsgdG9SZXF1ZXN0LnRvU3RyaW5nKCkgKyBcIiBmYWlsZWQ6IFwiICsgZSApO1xyXG4gICAgICAgICAgICByZXN1bWUoKTtcclxuICAgICAgICB9XHJcbiAgICB9XHJcblxyXG4gICAgLyoqXHJcbiAgICAgKiBSZXF1ZXN0cyB0aGUgY29tcGxldGUgc2VydmVyIHN0YXRlLiBVc2VmdWwgaWYgdGhlIGNsaWVudCdzIHN0YXRlIHNlZW1zIHRvIGJlIGluY29uc2lzdGVudCBzZXJ2ZXIncy5cclxuICAgICAqL1xyXG4gICAgcHVibGljIHJlcXVlc3RDb21wbGV0ZVN0YXRlKCkge1xyXG4gICAgICAgIHRyeSB7XHJcblxyXG4gICAgICAgICAgICB0aGlzLnN0b21wQ2xpZW50LnNlbmQoIHJlcXVlc3RDb21wbGV0ZVN0YXRlUGF0aCwgdGhpcy5oZWFkZXIsIG51bGwgKTtcclxuICAgICAgICB9IGNhdGNoICggZSApIHtcclxuICAgICAgICAgICAgY29uc29sZS5sb2coIFwiUmVzZXR0aW5nIGZhaWxlZDogXCIgKyBlICk7XHJcbiAgICAgICAgICAgIHJlc3VtZSgpO1xyXG4gICAgICAgIH1cclxuICAgIH1cclxuXHJcbiAgICBwdWJsaWMgbmV3R2FtZSgpIHtcclxuICAgICAgICB0aGlzLnN0b21wQ2xpZW50LnNlbmQoIG5ld0dhbWVQYXRoLCB0aGlzLmhlYWRlciwgbnVsbCApO1xyXG4gICAgfVxyXG59XHJcblxyXG5cblxuXG4vLyBXRUJQQUNLIEZPT1RFUiAvL1xuLy8gLi90eXBlc2NyaXB0L25ldHdvcmsvbmV0d29yay50cyIsIi8qIHNvY2tqcy1jbGllbnQgdjEuMS40IHwgaHR0cDovL3NvY2tqcy5vcmcgfCBNSVQgbGljZW5zZSAqL1xyXG4oZnVuY3Rpb24oZil7aWYodHlwZW9mIGV4cG9ydHM9PT1cIm9iamVjdFwiJiZ0eXBlb2YgbW9kdWxlIT09XCJ1bmRlZmluZWRcIil7bW9kdWxlLmV4cG9ydHM9ZigpfWVsc2UgaWYodHlwZW9mIGRlZmluZT09PVwiZnVuY3Rpb25cIiYmZGVmaW5lLmFtZCl7ZGVmaW5lKFtdLGYpfWVsc2V7dmFyIGc7aWYodHlwZW9mIHdpbmRvdyE9PVwidW5kZWZpbmVkXCIpe2c9d2luZG93fWVsc2UgaWYodHlwZW9mIGdsb2JhbCE9PVwidW5kZWZpbmVkXCIpe2c9Z2xvYmFsfWVsc2UgaWYodHlwZW9mIHNlbGYhPT1cInVuZGVmaW5lZFwiKXtnPXNlbGZ9ZWxzZXtnPXRoaXN9Zy5Tb2NrSlMgPSBmKCl9fSkoZnVuY3Rpb24oKXt2YXIgZGVmaW5lLG1vZHVsZSxleHBvcnRzO3JldHVybiAoZnVuY3Rpb24gZSh0LG4scil7ZnVuY3Rpb24gcyhvLHUpe2lmKCFuW29dKXtpZighdFtvXSl7dmFyIGE9dHlwZW9mIHJlcXVpcmU9PVwiZnVuY3Rpb25cIiYmcmVxdWlyZTtpZighdSYmYSlyZXR1cm4gYShvLCEwKTtpZihpKXJldHVybiBpKG8sITApO3ZhciBmPW5ldyBFcnJvcihcIkNhbm5vdCBmaW5kIG1vZHVsZSAnXCIrbytcIidcIik7dGhyb3cgZi5jb2RlPVwiTU9EVUxFX05PVF9GT1VORFwiLGZ9dmFyIGw9bltvXT17ZXhwb3J0czp7fX07dFtvXVswXS5jYWxsKGwuZXhwb3J0cyxmdW5jdGlvbihlKXt2YXIgbj10W29dWzFdW2VdO3JldHVybiBzKG4/bjplKX0sbCxsLmV4cG9ydHMsZSx0LG4scil9cmV0dXJuIG5bb10uZXhwb3J0c312YXIgaT10eXBlb2YgcmVxdWlyZT09XCJmdW5jdGlvblwiJiZyZXF1aXJlO2Zvcih2YXIgbz0wO288ci5sZW5ndGg7bysrKXMocltvXSk7cmV0dXJuIHN9KSh7MTpbZnVuY3Rpb24ocmVxdWlyZSxtb2R1bGUsZXhwb3J0cyl7XHJcbihmdW5jdGlvbiAoZ2xvYmFsKXtcclxuJ3VzZSBzdHJpY3QnO1xyXG5cclxudmFyIHRyYW5zcG9ydExpc3QgPSByZXF1aXJlKCcuL3RyYW5zcG9ydC1saXN0Jyk7XHJcblxyXG5tb2R1bGUuZXhwb3J0cyA9IHJlcXVpcmUoJy4vbWFpbicpKHRyYW5zcG9ydExpc3QpO1xyXG5cclxuLy8gVE9ETyBjYW4ndCBnZXQgcmlkIG9mIHRoaXMgdW50aWwgYWxsIHNlcnZlcnMgZG9cclxuaWYgKCdfc29ja2pzX29ubG9hZCcgaW4gZ2xvYmFsKSB7XHJcbiAgc2V0VGltZW91dChnbG9iYWwuX3NvY2tqc19vbmxvYWQsIDEpO1xyXG59XHJcblxyXG59KS5jYWxsKHRoaXMsdHlwZW9mIGdsb2JhbCAhPT0gXCJ1bmRlZmluZWRcIiA/IGdsb2JhbCA6IHR5cGVvZiBzZWxmICE9PSBcInVuZGVmaW5lZFwiID8gc2VsZiA6IHR5cGVvZiB3aW5kb3cgIT09IFwidW5kZWZpbmVkXCIgPyB3aW5kb3cgOiB7fSlcclxuXHJcbn0se1wiLi9tYWluXCI6MTQsXCIuL3RyYW5zcG9ydC1saXN0XCI6MTZ9XSwyOltmdW5jdGlvbihyZXF1aXJlLG1vZHVsZSxleHBvcnRzKXtcclxuJ3VzZSBzdHJpY3QnO1xyXG5cclxudmFyIGluaGVyaXRzID0gcmVxdWlyZSgnaW5oZXJpdHMnKVxyXG4gICwgRXZlbnQgPSByZXF1aXJlKCcuL2V2ZW50JylcclxuICA7XHJcblxyXG5mdW5jdGlvbiBDbG9zZUV2ZW50KCkge1xyXG4gIEV2ZW50LmNhbGwodGhpcyk7XHJcbiAgdGhpcy5pbml0RXZlbnQoJ2Nsb3NlJywgZmFsc2UsIGZhbHNlKTtcclxuICB0aGlzLndhc0NsZWFuID0gZmFsc2U7XHJcbiAgdGhpcy5jb2RlID0gMDtcclxuICB0aGlzLnJlYXNvbiA9ICcnO1xyXG59XHJcblxyXG5pbmhlcml0cyhDbG9zZUV2ZW50LCBFdmVudCk7XHJcblxyXG5tb2R1bGUuZXhwb3J0cyA9IENsb3NlRXZlbnQ7XHJcblxyXG59LHtcIi4vZXZlbnRcIjo0LFwiaW5oZXJpdHNcIjo1N31dLDM6W2Z1bmN0aW9uKHJlcXVpcmUsbW9kdWxlLGV4cG9ydHMpe1xyXG4ndXNlIHN0cmljdCc7XHJcblxyXG52YXIgaW5oZXJpdHMgPSByZXF1aXJlKCdpbmhlcml0cycpXHJcbiAgLCBFdmVudFRhcmdldCA9IHJlcXVpcmUoJy4vZXZlbnR0YXJnZXQnKVxyXG4gIDtcclxuXHJcbmZ1bmN0aW9uIEV2ZW50RW1pdHRlcigpIHtcclxuICBFdmVudFRhcmdldC5jYWxsKHRoaXMpO1xyXG59XHJcblxyXG5pbmhlcml0cyhFdmVudEVtaXR0ZXIsIEV2ZW50VGFyZ2V0KTtcclxuXHJcbkV2ZW50RW1pdHRlci5wcm90b3R5cGUucmVtb3ZlQWxsTGlzdGVuZXJzID0gZnVuY3Rpb24odHlwZSkge1xyXG4gIGlmICh0eXBlKSB7XHJcbiAgICBkZWxldGUgdGhpcy5fbGlzdGVuZXJzW3R5cGVdO1xyXG4gIH0gZWxzZSB7XHJcbiAgICB0aGlzLl9saXN0ZW5lcnMgPSB7fTtcclxuICB9XHJcbn07XHJcblxyXG5FdmVudEVtaXR0ZXIucHJvdG90eXBlLm9uY2UgPSBmdW5jdGlvbih0eXBlLCBsaXN0ZW5lcikge1xyXG4gIHZhciBzZWxmID0gdGhpc1xyXG4gICAgLCBmaXJlZCA9IGZhbHNlO1xyXG5cclxuICBmdW5jdGlvbiBnKCkge1xyXG4gICAgc2VsZi5yZW1vdmVMaXN0ZW5lcih0eXBlLCBnKTtcclxuXHJcbiAgICBpZiAoIWZpcmVkKSB7XHJcbiAgICAgIGZpcmVkID0gdHJ1ZTtcclxuICAgICAgbGlzdGVuZXIuYXBwbHkodGhpcywgYXJndW1lbnRzKTtcclxuICAgIH1cclxuICB9XHJcblxyXG4gIHRoaXMub24odHlwZSwgZyk7XHJcbn07XHJcblxyXG5FdmVudEVtaXR0ZXIucHJvdG90eXBlLmVtaXQgPSBmdW5jdGlvbigpIHtcclxuICB2YXIgdHlwZSA9IGFyZ3VtZW50c1swXTtcclxuICB2YXIgbGlzdGVuZXJzID0gdGhpcy5fbGlzdGVuZXJzW3R5cGVdO1xyXG4gIGlmICghbGlzdGVuZXJzKSB7XHJcbiAgICByZXR1cm47XHJcbiAgfVxyXG4gIC8vIGVxdWl2YWxlbnQgb2YgQXJyYXkucHJvdG90eXBlLnNsaWNlLmNhbGwoYXJndW1lbnRzLCAxKTtcclxuICB2YXIgbCA9IGFyZ3VtZW50cy5sZW5ndGg7XHJcbiAgdmFyIGFyZ3MgPSBuZXcgQXJyYXkobCAtIDEpO1xyXG4gIGZvciAodmFyIGFpID0gMTsgYWkgPCBsOyBhaSsrKSB7XHJcbiAgICBhcmdzW2FpIC0gMV0gPSBhcmd1bWVudHNbYWldO1xyXG4gIH1cclxuICBmb3IgKHZhciBpID0gMDsgaSA8IGxpc3RlbmVycy5sZW5ndGg7IGkrKykge1xyXG4gICAgbGlzdGVuZXJzW2ldLmFwcGx5KHRoaXMsIGFyZ3MpO1xyXG4gIH1cclxufTtcclxuXHJcbkV2ZW50RW1pdHRlci5wcm90b3R5cGUub24gPSBFdmVudEVtaXR0ZXIucHJvdG90eXBlLmFkZExpc3RlbmVyID0gRXZlbnRUYXJnZXQucHJvdG90eXBlLmFkZEV2ZW50TGlzdGVuZXI7XHJcbkV2ZW50RW1pdHRlci5wcm90b3R5cGUucmVtb3ZlTGlzdGVuZXIgPSBFdmVudFRhcmdldC5wcm90b3R5cGUucmVtb3ZlRXZlbnRMaXN0ZW5lcjtcclxuXHJcbm1vZHVsZS5leHBvcnRzLkV2ZW50RW1pdHRlciA9IEV2ZW50RW1pdHRlcjtcclxuXHJcbn0se1wiLi9ldmVudHRhcmdldFwiOjUsXCJpbmhlcml0c1wiOjU3fV0sNDpbZnVuY3Rpb24ocmVxdWlyZSxtb2R1bGUsZXhwb3J0cyl7XHJcbid1c2Ugc3RyaWN0JztcclxuXHJcbmZ1bmN0aW9uIEV2ZW50KGV2ZW50VHlwZSkge1xyXG4gIHRoaXMudHlwZSA9IGV2ZW50VHlwZTtcclxufVxyXG5cclxuRXZlbnQucHJvdG90eXBlLmluaXRFdmVudCA9IGZ1bmN0aW9uKGV2ZW50VHlwZSwgY2FuQnViYmxlLCBjYW5jZWxhYmxlKSB7XHJcbiAgdGhpcy50eXBlID0gZXZlbnRUeXBlO1xyXG4gIHRoaXMuYnViYmxlcyA9IGNhbkJ1YmJsZTtcclxuICB0aGlzLmNhbmNlbGFibGUgPSBjYW5jZWxhYmxlO1xyXG4gIHRoaXMudGltZVN0YW1wID0gK25ldyBEYXRlKCk7XHJcbiAgcmV0dXJuIHRoaXM7XHJcbn07XHJcblxyXG5FdmVudC5wcm90b3R5cGUuc3RvcFByb3BhZ2F0aW9uID0gZnVuY3Rpb24oKSB7fTtcclxuRXZlbnQucHJvdG90eXBlLnByZXZlbnREZWZhdWx0ID0gZnVuY3Rpb24oKSB7fTtcclxuXHJcbkV2ZW50LkNBUFRVUklOR19QSEFTRSA9IDE7XHJcbkV2ZW50LkFUX1RBUkdFVCA9IDI7XHJcbkV2ZW50LkJVQkJMSU5HX1BIQVNFID0gMztcclxuXHJcbm1vZHVsZS5leHBvcnRzID0gRXZlbnQ7XHJcblxyXG59LHt9XSw1OltmdW5jdGlvbihyZXF1aXJlLG1vZHVsZSxleHBvcnRzKXtcclxuJ3VzZSBzdHJpY3QnO1xyXG5cclxuLyogU2ltcGxpZmllZCBpbXBsZW1lbnRhdGlvbiBvZiBET00yIEV2ZW50VGFyZ2V0LlxyXG4gKiAgIGh0dHA6Ly93d3cudzMub3JnL1RSL0RPTS1MZXZlbC0yLUV2ZW50cy9ldmVudHMuaHRtbCNFdmVudHMtRXZlbnRUYXJnZXRcclxuICovXHJcblxyXG5mdW5jdGlvbiBFdmVudFRhcmdldCgpIHtcclxuICB0aGlzLl9saXN0ZW5lcnMgPSB7fTtcclxufVxyXG5cclxuRXZlbnRUYXJnZXQucHJvdG90eXBlLmFkZEV2ZW50TGlzdGVuZXIgPSBmdW5jdGlvbihldmVudFR5cGUsIGxpc3RlbmVyKSB7XHJcbiAgaWYgKCEoZXZlbnRUeXBlIGluIHRoaXMuX2xpc3RlbmVycykpIHtcclxuICAgIHRoaXMuX2xpc3RlbmVyc1tldmVudFR5cGVdID0gW107XHJcbiAgfVxyXG4gIHZhciBhcnIgPSB0aGlzLl9saXN0ZW5lcnNbZXZlbnRUeXBlXTtcclxuICAvLyAjNFxyXG4gIGlmIChhcnIuaW5kZXhPZihsaXN0ZW5lcikgPT09IC0xKSB7XHJcbiAgICAvLyBNYWtlIGEgY29weSBzbyBhcyBub3QgdG8gaW50ZXJmZXJlIHdpdGggYSBjdXJyZW50IGRpc3BhdGNoRXZlbnQuXHJcbiAgICBhcnIgPSBhcnIuY29uY2F0KFtsaXN0ZW5lcl0pO1xyXG4gIH1cclxuICB0aGlzLl9saXN0ZW5lcnNbZXZlbnRUeXBlXSA9IGFycjtcclxufTtcclxuXHJcbkV2ZW50VGFyZ2V0LnByb3RvdHlwZS5yZW1vdmVFdmVudExpc3RlbmVyID0gZnVuY3Rpb24oZXZlbnRUeXBlLCBsaXN0ZW5lcikge1xyXG4gIHZhciBhcnIgPSB0aGlzLl9saXN0ZW5lcnNbZXZlbnRUeXBlXTtcclxuICBpZiAoIWFycikge1xyXG4gICAgcmV0dXJuO1xyXG4gIH1cclxuICB2YXIgaWR4ID0gYXJyLmluZGV4T2YobGlzdGVuZXIpO1xyXG4gIGlmIChpZHggIT09IC0xKSB7XHJcbiAgICBpZiAoYXJyLmxlbmd0aCA+IDEpIHtcclxuICAgICAgLy8gTWFrZSBhIGNvcHkgc28gYXMgbm90IHRvIGludGVyZmVyZSB3aXRoIGEgY3VycmVudCBkaXNwYXRjaEV2ZW50LlxyXG4gICAgICB0aGlzLl9saXN0ZW5lcnNbZXZlbnRUeXBlXSA9IGFyci5zbGljZSgwLCBpZHgpLmNvbmNhdChhcnIuc2xpY2UoaWR4ICsgMSkpO1xyXG4gICAgfSBlbHNlIHtcclxuICAgICAgZGVsZXRlIHRoaXMuX2xpc3RlbmVyc1tldmVudFR5cGVdO1xyXG4gICAgfVxyXG4gICAgcmV0dXJuO1xyXG4gIH1cclxufTtcclxuXHJcbkV2ZW50VGFyZ2V0LnByb3RvdHlwZS5kaXNwYXRjaEV2ZW50ID0gZnVuY3Rpb24oKSB7XHJcbiAgdmFyIGV2ZW50ID0gYXJndW1lbnRzWzBdO1xyXG4gIHZhciB0ID0gZXZlbnQudHlwZTtcclxuICAvLyBlcXVpdmFsZW50IG9mIEFycmF5LnByb3RvdHlwZS5zbGljZS5jYWxsKGFyZ3VtZW50cywgMCk7XHJcbiAgdmFyIGFyZ3MgPSBhcmd1bWVudHMubGVuZ3RoID09PSAxID8gW2V2ZW50XSA6IEFycmF5LmFwcGx5KG51bGwsIGFyZ3VtZW50cyk7XHJcbiAgLy8gVE9ETzogVGhpcyBkb2Vzbid0IG1hdGNoIHRoZSByZWFsIGJlaGF2aW9yOyBwZXIgc3BlYywgb25mb28gZ2V0XHJcbiAgLy8gdGhlaXIgcGxhY2UgaW4gbGluZSBmcm9tIHRoZSAvZmlyc3QvIHRpbWUgdGhleSdyZSBzZXQgZnJvbVxyXG4gIC8vIG5vbi1udWxsLiBBbHRob3VnaCBXZWJLaXQgYnVtcHMgaXQgdG8gdGhlIGVuZCBldmVyeSB0aW1lIGl0J3NcclxuICAvLyBzZXQuXHJcbiAgaWYgKHRoaXNbJ29uJyArIHRdKSB7XHJcbiAgICB0aGlzWydvbicgKyB0XS5hcHBseSh0aGlzLCBhcmdzKTtcclxuICB9XHJcbiAgaWYgKHQgaW4gdGhpcy5fbGlzdGVuZXJzKSB7XHJcbiAgICAvLyBHcmFiIGEgcmVmZXJlbmNlIHRvIHRoZSBsaXN0ZW5lcnMgbGlzdC4gcmVtb3ZlRXZlbnRMaXN0ZW5lciBtYXkgYWx0ZXIgdGhlIGxpc3QuXHJcbiAgICB2YXIgbGlzdGVuZXJzID0gdGhpcy5fbGlzdGVuZXJzW3RdO1xyXG4gICAgZm9yICh2YXIgaSA9IDA7IGkgPCBsaXN0ZW5lcnMubGVuZ3RoOyBpKyspIHtcclxuICAgICAgbGlzdGVuZXJzW2ldLmFwcGx5KHRoaXMsIGFyZ3MpO1xyXG4gICAgfVxyXG4gIH1cclxufTtcclxuXHJcbm1vZHVsZS5leHBvcnRzID0gRXZlbnRUYXJnZXQ7XHJcblxyXG59LHt9XSw2OltmdW5jdGlvbihyZXF1aXJlLG1vZHVsZSxleHBvcnRzKXtcclxuJ3VzZSBzdHJpY3QnO1xyXG5cclxudmFyIGluaGVyaXRzID0gcmVxdWlyZSgnaW5oZXJpdHMnKVxyXG4gICwgRXZlbnQgPSByZXF1aXJlKCcuL2V2ZW50JylcclxuICA7XHJcblxyXG5mdW5jdGlvbiBUcmFuc3BvcnRNZXNzYWdlRXZlbnQoZGF0YSkge1xyXG4gIEV2ZW50LmNhbGwodGhpcyk7XHJcbiAgdGhpcy5pbml0RXZlbnQoJ21lc3NhZ2UnLCBmYWxzZSwgZmFsc2UpO1xyXG4gIHRoaXMuZGF0YSA9IGRhdGE7XHJcbn1cclxuXHJcbmluaGVyaXRzKFRyYW5zcG9ydE1lc3NhZ2VFdmVudCwgRXZlbnQpO1xyXG5cclxubW9kdWxlLmV4cG9ydHMgPSBUcmFuc3BvcnRNZXNzYWdlRXZlbnQ7XHJcblxyXG59LHtcIi4vZXZlbnRcIjo0LFwiaW5oZXJpdHNcIjo1N31dLDc6W2Z1bmN0aW9uKHJlcXVpcmUsbW9kdWxlLGV4cG9ydHMpe1xyXG4ndXNlIHN0cmljdCc7XHJcblxyXG52YXIgSlNPTjMgPSByZXF1aXJlKCdqc29uMycpXHJcbiAgLCBpZnJhbWVVdGlscyA9IHJlcXVpcmUoJy4vdXRpbHMvaWZyYW1lJylcclxuICA7XHJcblxyXG5mdW5jdGlvbiBGYWNhZGVKUyh0cmFuc3BvcnQpIHtcclxuICB0aGlzLl90cmFuc3BvcnQgPSB0cmFuc3BvcnQ7XHJcbiAgdHJhbnNwb3J0Lm9uKCdtZXNzYWdlJywgdGhpcy5fdHJhbnNwb3J0TWVzc2FnZS5iaW5kKHRoaXMpKTtcclxuICB0cmFuc3BvcnQub24oJ2Nsb3NlJywgdGhpcy5fdHJhbnNwb3J0Q2xvc2UuYmluZCh0aGlzKSk7XHJcbn1cclxuXHJcbkZhY2FkZUpTLnByb3RvdHlwZS5fdHJhbnNwb3J0Q2xvc2UgPSBmdW5jdGlvbihjb2RlLCByZWFzb24pIHtcclxuICBpZnJhbWVVdGlscy5wb3N0TWVzc2FnZSgnYycsIEpTT04zLnN0cmluZ2lmeShbY29kZSwgcmVhc29uXSkpO1xyXG59O1xyXG5GYWNhZGVKUy5wcm90b3R5cGUuX3RyYW5zcG9ydE1lc3NhZ2UgPSBmdW5jdGlvbihmcmFtZSkge1xyXG4gIGlmcmFtZVV0aWxzLnBvc3RNZXNzYWdlKCd0JywgZnJhbWUpO1xyXG59O1xyXG5GYWNhZGVKUy5wcm90b3R5cGUuX3NlbmQgPSBmdW5jdGlvbihkYXRhKSB7XHJcbiAgdGhpcy5fdHJhbnNwb3J0LnNlbmQoZGF0YSk7XHJcbn07XHJcbkZhY2FkZUpTLnByb3RvdHlwZS5fY2xvc2UgPSBmdW5jdGlvbigpIHtcclxuICB0aGlzLl90cmFuc3BvcnQuY2xvc2UoKTtcclxuICB0aGlzLl90cmFuc3BvcnQucmVtb3ZlQWxsTGlzdGVuZXJzKCk7XHJcbn07XHJcblxyXG5tb2R1bGUuZXhwb3J0cyA9IEZhY2FkZUpTO1xyXG5cclxufSx7XCIuL3V0aWxzL2lmcmFtZVwiOjQ3LFwianNvbjNcIjo1OH1dLDg6W2Z1bmN0aW9uKHJlcXVpcmUsbW9kdWxlLGV4cG9ydHMpe1xyXG4oZnVuY3Rpb24gKHByb2Nlc3Mpe1xyXG4ndXNlIHN0cmljdCc7XHJcblxyXG52YXIgdXJsVXRpbHMgPSByZXF1aXJlKCcuL3V0aWxzL3VybCcpXHJcbiAgLCBldmVudFV0aWxzID0gcmVxdWlyZSgnLi91dGlscy9ldmVudCcpXHJcbiAgLCBKU09OMyA9IHJlcXVpcmUoJ2pzb24zJylcclxuICAsIEZhY2FkZUpTID0gcmVxdWlyZSgnLi9mYWNhZGUnKVxyXG4gICwgSW5mb0lmcmFtZVJlY2VpdmVyID0gcmVxdWlyZSgnLi9pbmZvLWlmcmFtZS1yZWNlaXZlcicpXHJcbiAgLCBpZnJhbWVVdGlscyA9IHJlcXVpcmUoJy4vdXRpbHMvaWZyYW1lJylcclxuICAsIGxvYyA9IHJlcXVpcmUoJy4vbG9jYXRpb24nKVxyXG4gIDtcclxuXHJcbnZhciBkZWJ1ZyA9IGZ1bmN0aW9uKCkge307XHJcbmlmIChwcm9jZXNzLmVudi5OT0RFX0VOViAhPT0gJ3Byb2R1Y3Rpb24nKSB7XHJcbiAgZGVidWcgPSByZXF1aXJlKCdkZWJ1ZycpKCdzb2NranMtY2xpZW50OmlmcmFtZS1ib290c3RyYXAnKTtcclxufVxyXG5cclxubW9kdWxlLmV4cG9ydHMgPSBmdW5jdGlvbihTb2NrSlMsIGF2YWlsYWJsZVRyYW5zcG9ydHMpIHtcclxuICB2YXIgdHJhbnNwb3J0TWFwID0ge307XHJcbiAgYXZhaWxhYmxlVHJhbnNwb3J0cy5mb3JFYWNoKGZ1bmN0aW9uKGF0KSB7XHJcbiAgICBpZiAoYXQuZmFjYWRlVHJhbnNwb3J0KSB7XHJcbiAgICAgIHRyYW5zcG9ydE1hcFthdC5mYWNhZGVUcmFuc3BvcnQudHJhbnNwb3J0TmFtZV0gPSBhdC5mYWNhZGVUcmFuc3BvcnQ7XHJcbiAgICB9XHJcbiAgfSk7XHJcblxyXG4gIC8vIGhhcmQtY29kZWQgZm9yIHRoZSBpbmZvIGlmcmFtZVxyXG4gIC8vIFRPRE8gc2VlIGlmIHdlIGNhbiBtYWtlIHRoaXMgbW9yZSBkeW5hbWljXHJcbiAgdHJhbnNwb3J0TWFwW0luZm9JZnJhbWVSZWNlaXZlci50cmFuc3BvcnROYW1lXSA9IEluZm9JZnJhbWVSZWNlaXZlcjtcclxuICB2YXIgcGFyZW50T3JpZ2luO1xyXG5cclxuICAvKiBlc2xpbnQtZGlzYWJsZSBjYW1lbGNhc2UgKi9cclxuICBTb2NrSlMuYm9vdHN0cmFwX2lmcmFtZSA9IGZ1bmN0aW9uKCkge1xyXG4gICAgLyogZXNsaW50LWVuYWJsZSBjYW1lbGNhc2UgKi9cclxuICAgIHZhciBmYWNhZGU7XHJcbiAgICBpZnJhbWVVdGlscy5jdXJyZW50V2luZG93SWQgPSBsb2MuaGFzaC5zbGljZSgxKTtcclxuICAgIHZhciBvbk1lc3NhZ2UgPSBmdW5jdGlvbihlKSB7XHJcbiAgICAgIGlmIChlLnNvdXJjZSAhPT0gcGFyZW50KSB7XHJcbiAgICAgICAgcmV0dXJuO1xyXG4gICAgICB9XHJcbiAgICAgIGlmICh0eXBlb2YgcGFyZW50T3JpZ2luID09PSAndW5kZWZpbmVkJykge1xyXG4gICAgICAgIHBhcmVudE9yaWdpbiA9IGUub3JpZ2luO1xyXG4gICAgICB9XHJcbiAgICAgIGlmIChlLm9yaWdpbiAhPT0gcGFyZW50T3JpZ2luKSB7XHJcbiAgICAgICAgcmV0dXJuO1xyXG4gICAgICB9XHJcblxyXG4gICAgICB2YXIgaWZyYW1lTWVzc2FnZTtcclxuICAgICAgdHJ5IHtcclxuICAgICAgICBpZnJhbWVNZXNzYWdlID0gSlNPTjMucGFyc2UoZS5kYXRhKTtcclxuICAgICAgfSBjYXRjaCAoaWdub3JlZCkge1xyXG4gICAgICAgIGRlYnVnKCdiYWQganNvbicsIGUuZGF0YSk7XHJcbiAgICAgICAgcmV0dXJuO1xyXG4gICAgICB9XHJcblxyXG4gICAgICBpZiAoaWZyYW1lTWVzc2FnZS53aW5kb3dJZCAhPT0gaWZyYW1lVXRpbHMuY3VycmVudFdpbmRvd0lkKSB7XHJcbiAgICAgICAgcmV0dXJuO1xyXG4gICAgICB9XHJcbiAgICAgIHN3aXRjaCAoaWZyYW1lTWVzc2FnZS50eXBlKSB7XHJcbiAgICAgIGNhc2UgJ3MnOlxyXG4gICAgICAgIHZhciBwO1xyXG4gICAgICAgIHRyeSB7XHJcbiAgICAgICAgICBwID0gSlNPTjMucGFyc2UoaWZyYW1lTWVzc2FnZS5kYXRhKTtcclxuICAgICAgICB9IGNhdGNoIChpZ25vcmVkKSB7XHJcbiAgICAgICAgICBkZWJ1ZygnYmFkIGpzb24nLCBpZnJhbWVNZXNzYWdlLmRhdGEpO1xyXG4gICAgICAgICAgYnJlYWs7XHJcbiAgICAgICAgfVxyXG4gICAgICAgIHZhciB2ZXJzaW9uID0gcFswXTtcclxuICAgICAgICB2YXIgdHJhbnNwb3J0ID0gcFsxXTtcclxuICAgICAgICB2YXIgdHJhbnNVcmwgPSBwWzJdO1xyXG4gICAgICAgIHZhciBiYXNlVXJsID0gcFszXTtcclxuICAgICAgICBkZWJ1Zyh2ZXJzaW9uLCB0cmFuc3BvcnQsIHRyYW5zVXJsLCBiYXNlVXJsKTtcclxuICAgICAgICAvLyBjaGFuZ2UgdGhpcyB0byBzZW12ZXIgbG9naWNcclxuICAgICAgICBpZiAodmVyc2lvbiAhPT0gU29ja0pTLnZlcnNpb24pIHtcclxuICAgICAgICAgIHRocm93IG5ldyBFcnJvcignSW5jb21wYXRpYmxlIFNvY2tKUyEgTWFpbiBzaXRlIHVzZXM6JyArXHJcbiAgICAgICAgICAgICAgICAgICAgJyBcIicgKyB2ZXJzaW9uICsgJ1wiLCB0aGUgaWZyYW1lOicgK1xyXG4gICAgICAgICAgICAgICAgICAgICcgXCInICsgU29ja0pTLnZlcnNpb24gKyAnXCIuJyk7XHJcbiAgICAgICAgfVxyXG5cclxuICAgICAgICBpZiAoIXVybFV0aWxzLmlzT3JpZ2luRXF1YWwodHJhbnNVcmwsIGxvYy5ocmVmKSB8fFxyXG4gICAgICAgICAgICAhdXJsVXRpbHMuaXNPcmlnaW5FcXVhbChiYXNlVXJsLCBsb2MuaHJlZikpIHtcclxuICAgICAgICAgIHRocm93IG5ldyBFcnJvcignQ2FuXFwndCBjb25uZWN0IHRvIGRpZmZlcmVudCBkb21haW4gZnJvbSB3aXRoaW4gYW4gJyArXHJcbiAgICAgICAgICAgICAgICAgICAgJ2lmcmFtZS4gKCcgKyBsb2MuaHJlZiArICcsICcgKyB0cmFuc1VybCArICcsICcgKyBiYXNlVXJsICsgJyknKTtcclxuICAgICAgICB9XHJcbiAgICAgICAgZmFjYWRlID0gbmV3IEZhY2FkZUpTKG5ldyB0cmFuc3BvcnRNYXBbdHJhbnNwb3J0XSh0cmFuc1VybCwgYmFzZVVybCkpO1xyXG4gICAgICAgIGJyZWFrO1xyXG4gICAgICBjYXNlICdtJzpcclxuICAgICAgICBmYWNhZGUuX3NlbmQoaWZyYW1lTWVzc2FnZS5kYXRhKTtcclxuICAgICAgICBicmVhaztcclxuICAgICAgY2FzZSAnYyc6XHJcbiAgICAgICAgaWYgKGZhY2FkZSkge1xyXG4gICAgICAgICAgZmFjYWRlLl9jbG9zZSgpO1xyXG4gICAgICAgIH1cclxuICAgICAgICBmYWNhZGUgPSBudWxsO1xyXG4gICAgICAgIGJyZWFrO1xyXG4gICAgICB9XHJcbiAgICB9O1xyXG5cclxuICAgIGV2ZW50VXRpbHMuYXR0YWNoRXZlbnQoJ21lc3NhZ2UnLCBvbk1lc3NhZ2UpO1xyXG5cclxuICAgIC8vIFN0YXJ0XHJcbiAgICBpZnJhbWVVdGlscy5wb3N0TWVzc2FnZSgncycpO1xyXG4gIH07XHJcbn07XHJcblxyXG59KS5jYWxsKHRoaXMseyBlbnY6IHt9IH0pXHJcblxyXG59LHtcIi4vZmFjYWRlXCI6NyxcIi4vaW5mby1pZnJhbWUtcmVjZWl2ZXJcIjoxMCxcIi4vbG9jYXRpb25cIjoxMyxcIi4vdXRpbHMvZXZlbnRcIjo0NixcIi4vdXRpbHMvaWZyYW1lXCI6NDcsXCIuL3V0aWxzL3VybFwiOjUyLFwiZGVidWdcIjo1NSxcImpzb24zXCI6NTh9XSw5OltmdW5jdGlvbihyZXF1aXJlLG1vZHVsZSxleHBvcnRzKXtcclxuKGZ1bmN0aW9uIChwcm9jZXNzKXtcclxuJ3VzZSBzdHJpY3QnO1xyXG5cclxudmFyIEV2ZW50RW1pdHRlciA9IHJlcXVpcmUoJ2V2ZW50cycpLkV2ZW50RW1pdHRlclxyXG4gICwgaW5oZXJpdHMgPSByZXF1aXJlKCdpbmhlcml0cycpXHJcbiAgLCBKU09OMyA9IHJlcXVpcmUoJ2pzb24zJylcclxuICAsIG9iamVjdFV0aWxzID0gcmVxdWlyZSgnLi91dGlscy9vYmplY3QnKVxyXG4gIDtcclxuXHJcbnZhciBkZWJ1ZyA9IGZ1bmN0aW9uKCkge307XHJcbmlmIChwcm9jZXNzLmVudi5OT0RFX0VOViAhPT0gJ3Byb2R1Y3Rpb24nKSB7XHJcbiAgZGVidWcgPSByZXF1aXJlKCdkZWJ1ZycpKCdzb2NranMtY2xpZW50OmluZm8tYWpheCcpO1xyXG59XHJcblxyXG5mdW5jdGlvbiBJbmZvQWpheCh1cmwsIEFqYXhPYmplY3QpIHtcclxuICBFdmVudEVtaXR0ZXIuY2FsbCh0aGlzKTtcclxuXHJcbiAgdmFyIHNlbGYgPSB0aGlzO1xyXG4gIHZhciB0MCA9ICtuZXcgRGF0ZSgpO1xyXG4gIHRoaXMueG8gPSBuZXcgQWpheE9iamVjdCgnR0VUJywgdXJsKTtcclxuXHJcbiAgdGhpcy54by5vbmNlKCdmaW5pc2gnLCBmdW5jdGlvbihzdGF0dXMsIHRleHQpIHtcclxuICAgIHZhciBpbmZvLCBydHQ7XHJcbiAgICBpZiAoc3RhdHVzID09PSAyMDApIHtcclxuICAgICAgcnR0ID0gKCtuZXcgRGF0ZSgpKSAtIHQwO1xyXG4gICAgICBpZiAodGV4dCkge1xyXG4gICAgICAgIHRyeSB7XHJcbiAgICAgICAgICBpbmZvID0gSlNPTjMucGFyc2UodGV4dCk7XHJcbiAgICAgICAgfSBjYXRjaCAoZSkge1xyXG4gICAgICAgICAgZGVidWcoJ2JhZCBqc29uJywgdGV4dCk7XHJcbiAgICAgICAgfVxyXG4gICAgICB9XHJcblxyXG4gICAgICBpZiAoIW9iamVjdFV0aWxzLmlzT2JqZWN0KGluZm8pKSB7XHJcbiAgICAgICAgaW5mbyA9IHt9O1xyXG4gICAgICB9XHJcbiAgICB9XHJcbiAgICBzZWxmLmVtaXQoJ2ZpbmlzaCcsIGluZm8sIHJ0dCk7XHJcbiAgICBzZWxmLnJlbW92ZUFsbExpc3RlbmVycygpO1xyXG4gIH0pO1xyXG59XHJcblxyXG5pbmhlcml0cyhJbmZvQWpheCwgRXZlbnRFbWl0dGVyKTtcclxuXHJcbkluZm9BamF4LnByb3RvdHlwZS5jbG9zZSA9IGZ1bmN0aW9uKCkge1xyXG4gIHRoaXMucmVtb3ZlQWxsTGlzdGVuZXJzKCk7XHJcbiAgdGhpcy54by5jbG9zZSgpO1xyXG59O1xyXG5cclxubW9kdWxlLmV4cG9ydHMgPSBJbmZvQWpheDtcclxuXHJcbn0pLmNhbGwodGhpcyx7IGVudjoge30gfSlcclxuXHJcbn0se1wiLi91dGlscy9vYmplY3RcIjo0OSxcImRlYnVnXCI6NTUsXCJldmVudHNcIjozLFwiaW5oZXJpdHNcIjo1NyxcImpzb24zXCI6NTh9XSwxMDpbZnVuY3Rpb24ocmVxdWlyZSxtb2R1bGUsZXhwb3J0cyl7XHJcbid1c2Ugc3RyaWN0JztcclxuXHJcbnZhciBpbmhlcml0cyA9IHJlcXVpcmUoJ2luaGVyaXRzJylcclxuICAsIEV2ZW50RW1pdHRlciA9IHJlcXVpcmUoJ2V2ZW50cycpLkV2ZW50RW1pdHRlclxyXG4gICwgSlNPTjMgPSByZXF1aXJlKCdqc29uMycpXHJcbiAgLCBYSFJMb2NhbE9iamVjdCA9IHJlcXVpcmUoJy4vdHJhbnNwb3J0L3NlbmRlci94aHItbG9jYWwnKVxyXG4gICwgSW5mb0FqYXggPSByZXF1aXJlKCcuL2luZm8tYWpheCcpXHJcbiAgO1xyXG5cclxuZnVuY3Rpb24gSW5mb1JlY2VpdmVySWZyYW1lKHRyYW5zVXJsKSB7XHJcbiAgdmFyIHNlbGYgPSB0aGlzO1xyXG4gIEV2ZW50RW1pdHRlci5jYWxsKHRoaXMpO1xyXG5cclxuICB0aGlzLmlyID0gbmV3IEluZm9BamF4KHRyYW5zVXJsLCBYSFJMb2NhbE9iamVjdCk7XHJcbiAgdGhpcy5pci5vbmNlKCdmaW5pc2gnLCBmdW5jdGlvbihpbmZvLCBydHQpIHtcclxuICAgIHNlbGYuaXIgPSBudWxsO1xyXG4gICAgc2VsZi5lbWl0KCdtZXNzYWdlJywgSlNPTjMuc3RyaW5naWZ5KFtpbmZvLCBydHRdKSk7XHJcbiAgfSk7XHJcbn1cclxuXHJcbmluaGVyaXRzKEluZm9SZWNlaXZlcklmcmFtZSwgRXZlbnRFbWl0dGVyKTtcclxuXHJcbkluZm9SZWNlaXZlcklmcmFtZS50cmFuc3BvcnROYW1lID0gJ2lmcmFtZS1pbmZvLXJlY2VpdmVyJztcclxuXHJcbkluZm9SZWNlaXZlcklmcmFtZS5wcm90b3R5cGUuY2xvc2UgPSBmdW5jdGlvbigpIHtcclxuICBpZiAodGhpcy5pcikge1xyXG4gICAgdGhpcy5pci5jbG9zZSgpO1xyXG4gICAgdGhpcy5pciA9IG51bGw7XHJcbiAgfVxyXG4gIHRoaXMucmVtb3ZlQWxsTGlzdGVuZXJzKCk7XHJcbn07XHJcblxyXG5tb2R1bGUuZXhwb3J0cyA9IEluZm9SZWNlaXZlcklmcmFtZTtcclxuXHJcbn0se1wiLi9pbmZvLWFqYXhcIjo5LFwiLi90cmFuc3BvcnQvc2VuZGVyL3hoci1sb2NhbFwiOjM3LFwiZXZlbnRzXCI6MyxcImluaGVyaXRzXCI6NTcsXCJqc29uM1wiOjU4fV0sMTE6W2Z1bmN0aW9uKHJlcXVpcmUsbW9kdWxlLGV4cG9ydHMpe1xyXG4oZnVuY3Rpb24gKHByb2Nlc3MsZ2xvYmFsKXtcclxuJ3VzZSBzdHJpY3QnO1xyXG5cclxudmFyIEV2ZW50RW1pdHRlciA9IHJlcXVpcmUoJ2V2ZW50cycpLkV2ZW50RW1pdHRlclxyXG4gICwgaW5oZXJpdHMgPSByZXF1aXJlKCdpbmhlcml0cycpXHJcbiAgLCBKU09OMyA9IHJlcXVpcmUoJ2pzb24zJylcclxuICAsIHV0aWxzID0gcmVxdWlyZSgnLi91dGlscy9ldmVudCcpXHJcbiAgLCBJZnJhbWVUcmFuc3BvcnQgPSByZXF1aXJlKCcuL3RyYW5zcG9ydC9pZnJhbWUnKVxyXG4gICwgSW5mb1JlY2VpdmVySWZyYW1lID0gcmVxdWlyZSgnLi9pbmZvLWlmcmFtZS1yZWNlaXZlcicpXHJcbiAgO1xyXG5cclxudmFyIGRlYnVnID0gZnVuY3Rpb24oKSB7fTtcclxuaWYgKHByb2Nlc3MuZW52Lk5PREVfRU5WICE9PSAncHJvZHVjdGlvbicpIHtcclxuICBkZWJ1ZyA9IHJlcXVpcmUoJ2RlYnVnJykoJ3NvY2tqcy1jbGllbnQ6aW5mby1pZnJhbWUnKTtcclxufVxyXG5cclxuZnVuY3Rpb24gSW5mb0lmcmFtZShiYXNlVXJsLCB1cmwpIHtcclxuICB2YXIgc2VsZiA9IHRoaXM7XHJcbiAgRXZlbnRFbWl0dGVyLmNhbGwodGhpcyk7XHJcblxyXG4gIHZhciBnbyA9IGZ1bmN0aW9uKCkge1xyXG4gICAgdmFyIGlmciA9IHNlbGYuaWZyID0gbmV3IElmcmFtZVRyYW5zcG9ydChJbmZvUmVjZWl2ZXJJZnJhbWUudHJhbnNwb3J0TmFtZSwgdXJsLCBiYXNlVXJsKTtcclxuXHJcbiAgICBpZnIub25jZSgnbWVzc2FnZScsIGZ1bmN0aW9uKG1zZykge1xyXG4gICAgICBpZiAobXNnKSB7XHJcbiAgICAgICAgdmFyIGQ7XHJcbiAgICAgICAgdHJ5IHtcclxuICAgICAgICAgIGQgPSBKU09OMy5wYXJzZShtc2cpO1xyXG4gICAgICAgIH0gY2F0Y2ggKGUpIHtcclxuICAgICAgICAgIGRlYnVnKCdiYWQganNvbicsIG1zZyk7XHJcbiAgICAgICAgICBzZWxmLmVtaXQoJ2ZpbmlzaCcpO1xyXG4gICAgICAgICAgc2VsZi5jbG9zZSgpO1xyXG4gICAgICAgICAgcmV0dXJuO1xyXG4gICAgICAgIH1cclxuXHJcbiAgICAgICAgdmFyIGluZm8gPSBkWzBdLCBydHQgPSBkWzFdO1xyXG4gICAgICAgIHNlbGYuZW1pdCgnZmluaXNoJywgaW5mbywgcnR0KTtcclxuICAgICAgfVxyXG4gICAgICBzZWxmLmNsb3NlKCk7XHJcbiAgICB9KTtcclxuXHJcbiAgICBpZnIub25jZSgnY2xvc2UnLCBmdW5jdGlvbigpIHtcclxuICAgICAgc2VsZi5lbWl0KCdmaW5pc2gnKTtcclxuICAgICAgc2VsZi5jbG9zZSgpO1xyXG4gICAgfSk7XHJcbiAgfTtcclxuXHJcbiAgLy8gVE9ETyB0aGlzIHNlZW1zIHRoZSBzYW1lIGFzIHRoZSAnbmVlZEJvZHknIGZyb20gdHJhbnNwb3J0c1xyXG4gIGlmICghZ2xvYmFsLmRvY3VtZW50LmJvZHkpIHtcclxuICAgIHV0aWxzLmF0dGFjaEV2ZW50KCdsb2FkJywgZ28pO1xyXG4gIH0gZWxzZSB7XHJcbiAgICBnbygpO1xyXG4gIH1cclxufVxyXG5cclxuaW5oZXJpdHMoSW5mb0lmcmFtZSwgRXZlbnRFbWl0dGVyKTtcclxuXHJcbkluZm9JZnJhbWUuZW5hYmxlZCA9IGZ1bmN0aW9uKCkge1xyXG4gIHJldHVybiBJZnJhbWVUcmFuc3BvcnQuZW5hYmxlZCgpO1xyXG59O1xyXG5cclxuSW5mb0lmcmFtZS5wcm90b3R5cGUuY2xvc2UgPSBmdW5jdGlvbigpIHtcclxuICBpZiAodGhpcy5pZnIpIHtcclxuICAgIHRoaXMuaWZyLmNsb3NlKCk7XHJcbiAgfVxyXG4gIHRoaXMucmVtb3ZlQWxsTGlzdGVuZXJzKCk7XHJcbiAgdGhpcy5pZnIgPSBudWxsO1xyXG59O1xyXG5cclxubW9kdWxlLmV4cG9ydHMgPSBJbmZvSWZyYW1lO1xyXG5cclxufSkuY2FsbCh0aGlzLHsgZW52OiB7fSB9LHR5cGVvZiBnbG9iYWwgIT09IFwidW5kZWZpbmVkXCIgPyBnbG9iYWwgOiB0eXBlb2Ygc2VsZiAhPT0gXCJ1bmRlZmluZWRcIiA/IHNlbGYgOiB0eXBlb2Ygd2luZG93ICE9PSBcInVuZGVmaW5lZFwiID8gd2luZG93IDoge30pXHJcblxyXG59LHtcIi4vaW5mby1pZnJhbWUtcmVjZWl2ZXJcIjoxMCxcIi4vdHJhbnNwb3J0L2lmcmFtZVwiOjIyLFwiLi91dGlscy9ldmVudFwiOjQ2LFwiZGVidWdcIjo1NSxcImV2ZW50c1wiOjMsXCJpbmhlcml0c1wiOjU3LFwianNvbjNcIjo1OH1dLDEyOltmdW5jdGlvbihyZXF1aXJlLG1vZHVsZSxleHBvcnRzKXtcclxuKGZ1bmN0aW9uIChwcm9jZXNzKXtcclxuJ3VzZSBzdHJpY3QnO1xyXG5cclxudmFyIEV2ZW50RW1pdHRlciA9IHJlcXVpcmUoJ2V2ZW50cycpLkV2ZW50RW1pdHRlclxyXG4gICwgaW5oZXJpdHMgPSByZXF1aXJlKCdpbmhlcml0cycpXHJcbiAgLCB1cmxVdGlscyA9IHJlcXVpcmUoJy4vdXRpbHMvdXJsJylcclxuICAsIFhEUiA9IHJlcXVpcmUoJy4vdHJhbnNwb3J0L3NlbmRlci94ZHInKVxyXG4gICwgWEhSQ29ycyA9IHJlcXVpcmUoJy4vdHJhbnNwb3J0L3NlbmRlci94aHItY29ycycpXHJcbiAgLCBYSFJMb2NhbCA9IHJlcXVpcmUoJy4vdHJhbnNwb3J0L3NlbmRlci94aHItbG9jYWwnKVxyXG4gICwgWEhSRmFrZSA9IHJlcXVpcmUoJy4vdHJhbnNwb3J0L3NlbmRlci94aHItZmFrZScpXHJcbiAgLCBJbmZvSWZyYW1lID0gcmVxdWlyZSgnLi9pbmZvLWlmcmFtZScpXHJcbiAgLCBJbmZvQWpheCA9IHJlcXVpcmUoJy4vaW5mby1hamF4JylcclxuICA7XHJcblxyXG52YXIgZGVidWcgPSBmdW5jdGlvbigpIHt9O1xyXG5pZiAocHJvY2Vzcy5lbnYuTk9ERV9FTlYgIT09ICdwcm9kdWN0aW9uJykge1xyXG4gIGRlYnVnID0gcmVxdWlyZSgnZGVidWcnKSgnc29ja2pzLWNsaWVudDppbmZvLXJlY2VpdmVyJyk7XHJcbn1cclxuXHJcbmZ1bmN0aW9uIEluZm9SZWNlaXZlcihiYXNlVXJsLCB1cmxJbmZvKSB7XHJcbiAgZGVidWcoYmFzZVVybCk7XHJcbiAgdmFyIHNlbGYgPSB0aGlzO1xyXG4gIEV2ZW50RW1pdHRlci5jYWxsKHRoaXMpO1xyXG5cclxuICBzZXRUaW1lb3V0KGZ1bmN0aW9uKCkge1xyXG4gICAgc2VsZi5kb1hocihiYXNlVXJsLCB1cmxJbmZvKTtcclxuICB9LCAwKTtcclxufVxyXG5cclxuaW5oZXJpdHMoSW5mb1JlY2VpdmVyLCBFdmVudEVtaXR0ZXIpO1xyXG5cclxuLy8gVE9ETyB0aGlzIGlzIGN1cnJlbnRseSBpZ25vcmluZyB0aGUgbGlzdCBvZiBhdmFpbGFibGUgdHJhbnNwb3J0cyBhbmQgdGhlIHdoaXRlbGlzdFxyXG5cclxuSW5mb1JlY2VpdmVyLl9nZXRSZWNlaXZlciA9IGZ1bmN0aW9uKGJhc2VVcmwsIHVybCwgdXJsSW5mbykge1xyXG4gIC8vIGRldGVybWluZSBtZXRob2Qgb2YgQ09SUyBzdXBwb3J0IChpZiBuZWVkZWQpXHJcbiAgaWYgKHVybEluZm8uc2FtZU9yaWdpbikge1xyXG4gICAgcmV0dXJuIG5ldyBJbmZvQWpheCh1cmwsIFhIUkxvY2FsKTtcclxuICB9XHJcbiAgaWYgKFhIUkNvcnMuZW5hYmxlZCkge1xyXG4gICAgcmV0dXJuIG5ldyBJbmZvQWpheCh1cmwsIFhIUkNvcnMpO1xyXG4gIH1cclxuICBpZiAoWERSLmVuYWJsZWQgJiYgdXJsSW5mby5zYW1lU2NoZW1lKSB7XHJcbiAgICByZXR1cm4gbmV3IEluZm9BamF4KHVybCwgWERSKTtcclxuICB9XHJcbiAgaWYgKEluZm9JZnJhbWUuZW5hYmxlZCgpKSB7XHJcbiAgICByZXR1cm4gbmV3IEluZm9JZnJhbWUoYmFzZVVybCwgdXJsKTtcclxuICB9XHJcbiAgcmV0dXJuIG5ldyBJbmZvQWpheCh1cmwsIFhIUkZha2UpO1xyXG59O1xyXG5cclxuSW5mb1JlY2VpdmVyLnByb3RvdHlwZS5kb1hociA9IGZ1bmN0aW9uKGJhc2VVcmwsIHVybEluZm8pIHtcclxuICB2YXIgc2VsZiA9IHRoaXNcclxuICAgICwgdXJsID0gdXJsVXRpbHMuYWRkUGF0aChiYXNlVXJsLCAnL2luZm8nKVxyXG4gICAgO1xyXG4gIGRlYnVnKCdkb1hocicsIHVybCk7XHJcblxyXG4gIHRoaXMueG8gPSBJbmZvUmVjZWl2ZXIuX2dldFJlY2VpdmVyKGJhc2VVcmwsIHVybCwgdXJsSW5mbyk7XHJcblxyXG4gIHRoaXMudGltZW91dFJlZiA9IHNldFRpbWVvdXQoZnVuY3Rpb24oKSB7XHJcbiAgICBkZWJ1ZygndGltZW91dCcpO1xyXG4gICAgc2VsZi5fY2xlYW51cChmYWxzZSk7XHJcbiAgICBzZWxmLmVtaXQoJ2ZpbmlzaCcpO1xyXG4gIH0sIEluZm9SZWNlaXZlci50aW1lb3V0KTtcclxuXHJcbiAgdGhpcy54by5vbmNlKCdmaW5pc2gnLCBmdW5jdGlvbihpbmZvLCBydHQpIHtcclxuICAgIGRlYnVnKCdmaW5pc2gnLCBpbmZvLCBydHQpO1xyXG4gICAgc2VsZi5fY2xlYW51cCh0cnVlKTtcclxuICAgIHNlbGYuZW1pdCgnZmluaXNoJywgaW5mbywgcnR0KTtcclxuICB9KTtcclxufTtcclxuXHJcbkluZm9SZWNlaXZlci5wcm90b3R5cGUuX2NsZWFudXAgPSBmdW5jdGlvbih3YXNDbGVhbikge1xyXG4gIGRlYnVnKCdfY2xlYW51cCcpO1xyXG4gIGNsZWFyVGltZW91dCh0aGlzLnRpbWVvdXRSZWYpO1xyXG4gIHRoaXMudGltZW91dFJlZiA9IG51bGw7XHJcbiAgaWYgKCF3YXNDbGVhbiAmJiB0aGlzLnhvKSB7XHJcbiAgICB0aGlzLnhvLmNsb3NlKCk7XHJcbiAgfVxyXG4gIHRoaXMueG8gPSBudWxsO1xyXG59O1xyXG5cclxuSW5mb1JlY2VpdmVyLnByb3RvdHlwZS5jbG9zZSA9IGZ1bmN0aW9uKCkge1xyXG4gIGRlYnVnKCdjbG9zZScpO1xyXG4gIHRoaXMucmVtb3ZlQWxsTGlzdGVuZXJzKCk7XHJcbiAgdGhpcy5fY2xlYW51cChmYWxzZSk7XHJcbn07XHJcblxyXG5JbmZvUmVjZWl2ZXIudGltZW91dCA9IDgwMDA7XHJcblxyXG5tb2R1bGUuZXhwb3J0cyA9IEluZm9SZWNlaXZlcjtcclxuXHJcbn0pLmNhbGwodGhpcyx7IGVudjoge30gfSlcclxuXHJcbn0se1wiLi9pbmZvLWFqYXhcIjo5LFwiLi9pbmZvLWlmcmFtZVwiOjExLFwiLi90cmFuc3BvcnQvc2VuZGVyL3hkclwiOjM0LFwiLi90cmFuc3BvcnQvc2VuZGVyL3hoci1jb3JzXCI6MzUsXCIuL3RyYW5zcG9ydC9zZW5kZXIveGhyLWZha2VcIjozNixcIi4vdHJhbnNwb3J0L3NlbmRlci94aHItbG9jYWxcIjozNyxcIi4vdXRpbHMvdXJsXCI6NTIsXCJkZWJ1Z1wiOjU1LFwiZXZlbnRzXCI6MyxcImluaGVyaXRzXCI6NTd9XSwxMzpbZnVuY3Rpb24ocmVxdWlyZSxtb2R1bGUsZXhwb3J0cyl7XHJcbihmdW5jdGlvbiAoZ2xvYmFsKXtcclxuJ3VzZSBzdHJpY3QnO1xyXG5cclxubW9kdWxlLmV4cG9ydHMgPSBnbG9iYWwubG9jYXRpb24gfHwge1xyXG4gIG9yaWdpbjogJ2h0dHA6Ly9sb2NhbGhvc3Q6ODAnXHJcbiwgcHJvdG9jb2w6ICdodHRwJ1xyXG4sIGhvc3Q6ICdsb2NhbGhvc3QnXHJcbiwgcG9ydDogODBcclxuLCBocmVmOiAnaHR0cDovL2xvY2FsaG9zdC8nXHJcbiwgaGFzaDogJydcclxufTtcclxuXHJcbn0pLmNhbGwodGhpcyx0eXBlb2YgZ2xvYmFsICE9PSBcInVuZGVmaW5lZFwiID8gZ2xvYmFsIDogdHlwZW9mIHNlbGYgIT09IFwidW5kZWZpbmVkXCIgPyBzZWxmIDogdHlwZW9mIHdpbmRvdyAhPT0gXCJ1bmRlZmluZWRcIiA/IHdpbmRvdyA6IHt9KVxyXG5cclxufSx7fV0sMTQ6W2Z1bmN0aW9uKHJlcXVpcmUsbW9kdWxlLGV4cG9ydHMpe1xyXG4oZnVuY3Rpb24gKHByb2Nlc3MsZ2xvYmFsKXtcclxuJ3VzZSBzdHJpY3QnO1xyXG5cclxucmVxdWlyZSgnLi9zaGltcycpO1xyXG5cclxudmFyIFVSTCA9IHJlcXVpcmUoJ3VybC1wYXJzZScpXHJcbiAgLCBpbmhlcml0cyA9IHJlcXVpcmUoJ2luaGVyaXRzJylcclxuICAsIEpTT04zID0gcmVxdWlyZSgnanNvbjMnKVxyXG4gICwgcmFuZG9tID0gcmVxdWlyZSgnLi91dGlscy9yYW5kb20nKVxyXG4gICwgZXNjYXBlID0gcmVxdWlyZSgnLi91dGlscy9lc2NhcGUnKVxyXG4gICwgdXJsVXRpbHMgPSByZXF1aXJlKCcuL3V0aWxzL3VybCcpXHJcbiAgLCBldmVudFV0aWxzID0gcmVxdWlyZSgnLi91dGlscy9ldmVudCcpXHJcbiAgLCB0cmFuc3BvcnQgPSByZXF1aXJlKCcuL3V0aWxzL3RyYW5zcG9ydCcpXHJcbiAgLCBvYmplY3RVdGlscyA9IHJlcXVpcmUoJy4vdXRpbHMvb2JqZWN0JylcclxuICAsIGJyb3dzZXIgPSByZXF1aXJlKCcuL3V0aWxzL2Jyb3dzZXInKVxyXG4gICwgbG9nID0gcmVxdWlyZSgnLi91dGlscy9sb2cnKVxyXG4gICwgRXZlbnQgPSByZXF1aXJlKCcuL2V2ZW50L2V2ZW50JylcclxuICAsIEV2ZW50VGFyZ2V0ID0gcmVxdWlyZSgnLi9ldmVudC9ldmVudHRhcmdldCcpXHJcbiAgLCBsb2MgPSByZXF1aXJlKCcuL2xvY2F0aW9uJylcclxuICAsIENsb3NlRXZlbnQgPSByZXF1aXJlKCcuL2V2ZW50L2Nsb3NlJylcclxuICAsIFRyYW5zcG9ydE1lc3NhZ2VFdmVudCA9IHJlcXVpcmUoJy4vZXZlbnQvdHJhbnMtbWVzc2FnZScpXHJcbiAgLCBJbmZvUmVjZWl2ZXIgPSByZXF1aXJlKCcuL2luZm8tcmVjZWl2ZXInKVxyXG4gIDtcclxuXHJcbnZhciBkZWJ1ZyA9IGZ1bmN0aW9uKCkge307XHJcbmlmIChwcm9jZXNzLmVudi5OT0RFX0VOViAhPT0gJ3Byb2R1Y3Rpb24nKSB7XHJcbiAgZGVidWcgPSByZXF1aXJlKCdkZWJ1ZycpKCdzb2NranMtY2xpZW50Om1haW4nKTtcclxufVxyXG5cclxudmFyIHRyYW5zcG9ydHM7XHJcblxyXG4vLyBmb2xsb3cgY29uc3RydWN0b3Igc3RlcHMgZGVmaW5lZCBhdCBodHRwOi8vZGV2LnczLm9yZy9odG1sNS93ZWJzb2NrZXRzLyN0aGUtd2Vic29ja2V0LWludGVyZmFjZVxyXG5mdW5jdGlvbiBTb2NrSlModXJsLCBwcm90b2NvbHMsIG9wdGlvbnMpIHtcclxuICBpZiAoISh0aGlzIGluc3RhbmNlb2YgU29ja0pTKSkge1xyXG4gICAgcmV0dXJuIG5ldyBTb2NrSlModXJsLCBwcm90b2NvbHMsIG9wdGlvbnMpO1xyXG4gIH1cclxuICBpZiAoYXJndW1lbnRzLmxlbmd0aCA8IDEpIHtcclxuICAgIHRocm93IG5ldyBUeXBlRXJyb3IoXCJGYWlsZWQgdG8gY29uc3RydWN0ICdTb2NrSlM6IDEgYXJndW1lbnQgcmVxdWlyZWQsIGJ1dCBvbmx5IDAgcHJlc2VudFwiKTtcclxuICB9XHJcbiAgRXZlbnRUYXJnZXQuY2FsbCh0aGlzKTtcclxuXHJcbiAgdGhpcy5yZWFkeVN0YXRlID0gU29ja0pTLkNPTk5FQ1RJTkc7XHJcbiAgdGhpcy5leHRlbnNpb25zID0gJyc7XHJcbiAgdGhpcy5wcm90b2NvbCA9ICcnO1xyXG5cclxuICAvLyBub24tc3RhbmRhcmQgZXh0ZW5zaW9uXHJcbiAgb3B0aW9ucyA9IG9wdGlvbnMgfHwge307XHJcbiAgaWYgKG9wdGlvbnMucHJvdG9jb2xzX3doaXRlbGlzdCkge1xyXG4gICAgbG9nLndhcm4oXCIncHJvdG9jb2xzX3doaXRlbGlzdCcgaXMgREVQUkVDQVRFRC4gVXNlICd0cmFuc3BvcnRzJyBpbnN0ZWFkLlwiKTtcclxuICB9XHJcbiAgdGhpcy5fdHJhbnNwb3J0c1doaXRlbGlzdCA9IG9wdGlvbnMudHJhbnNwb3J0cztcclxuICB0aGlzLl90cmFuc3BvcnRPcHRpb25zID0gb3B0aW9ucy50cmFuc3BvcnRPcHRpb25zIHx8IHt9O1xyXG5cclxuICB2YXIgc2Vzc2lvbklkID0gb3B0aW9ucy5zZXNzaW9uSWQgfHwgODtcclxuICBpZiAodHlwZW9mIHNlc3Npb25JZCA9PT0gJ2Z1bmN0aW9uJykge1xyXG4gICAgdGhpcy5fZ2VuZXJhdGVTZXNzaW9uSWQgPSBzZXNzaW9uSWQ7XHJcbiAgfSBlbHNlIGlmICh0eXBlb2Ygc2Vzc2lvbklkID09PSAnbnVtYmVyJykge1xyXG4gICAgdGhpcy5fZ2VuZXJhdGVTZXNzaW9uSWQgPSBmdW5jdGlvbigpIHtcclxuICAgICAgcmV0dXJuIHJhbmRvbS5zdHJpbmcoc2Vzc2lvbklkKTtcclxuICAgIH07XHJcbiAgfSBlbHNlIHtcclxuICAgIHRocm93IG5ldyBUeXBlRXJyb3IoJ0lmIHNlc3Npb25JZCBpcyB1c2VkIGluIHRoZSBvcHRpb25zLCBpdCBuZWVkcyB0byBiZSBhIG51bWJlciBvciBhIGZ1bmN0aW9uLicpO1xyXG4gIH1cclxuXHJcbiAgdGhpcy5fc2VydmVyID0gb3B0aW9ucy5zZXJ2ZXIgfHwgcmFuZG9tLm51bWJlclN0cmluZygxMDAwKTtcclxuXHJcbiAgLy8gU3RlcCAxIG9mIFdTIHNwZWMgLSBwYXJzZSBhbmQgdmFsaWRhdGUgdGhlIHVybC4gSXNzdWUgIzhcclxuICB2YXIgcGFyc2VkVXJsID0gbmV3IFVSTCh1cmwpO1xyXG4gIGlmICghcGFyc2VkVXJsLmhvc3QgfHwgIXBhcnNlZFVybC5wcm90b2NvbCkge1xyXG4gICAgdGhyb3cgbmV3IFN5bnRheEVycm9yKFwiVGhlIFVSTCAnXCIgKyB1cmwgKyBcIicgaXMgaW52YWxpZFwiKTtcclxuICB9IGVsc2UgaWYgKHBhcnNlZFVybC5oYXNoKSB7XHJcbiAgICB0aHJvdyBuZXcgU3ludGF4RXJyb3IoJ1RoZSBVUkwgbXVzdCBub3QgY29udGFpbiBhIGZyYWdtZW50Jyk7XHJcbiAgfSBlbHNlIGlmIChwYXJzZWRVcmwucHJvdG9jb2wgIT09ICdodHRwOicgJiYgcGFyc2VkVXJsLnByb3RvY29sICE9PSAnaHR0cHM6Jykge1xyXG4gICAgdGhyb3cgbmV3IFN5bnRheEVycm9yKFwiVGhlIFVSTCdzIHNjaGVtZSBtdXN0IGJlIGVpdGhlciAnaHR0cDonIG9yICdodHRwczonLiAnXCIgKyBwYXJzZWRVcmwucHJvdG9jb2wgKyBcIicgaXMgbm90IGFsbG93ZWQuXCIpO1xyXG4gIH1cclxuXHJcbiAgdmFyIHNlY3VyZSA9IHBhcnNlZFVybC5wcm90b2NvbCA9PT0gJ2h0dHBzOic7XHJcbiAgLy8gU3RlcCAyIC0gZG9uJ3QgYWxsb3cgc2VjdXJlIG9yaWdpbiB3aXRoIGFuIGluc2VjdXJlIHByb3RvY29sXHJcbiAgaWYgKGxvYy5wcm90b2NvbCA9PT0gJ2h0dHBzJyAmJiAhc2VjdXJlKSB7XHJcbiAgICB0aHJvdyBuZXcgRXJyb3IoJ1NlY3VyaXR5RXJyb3I6IEFuIGluc2VjdXJlIFNvY2tKUyBjb25uZWN0aW9uIG1heSBub3QgYmUgaW5pdGlhdGVkIGZyb20gYSBwYWdlIGxvYWRlZCBvdmVyIEhUVFBTJyk7XHJcbiAgfVxyXG5cclxuICAvLyBTdGVwIDMgLSBjaGVjayBwb3J0IGFjY2VzcyAtIG5vIG5lZWQgaGVyZVxyXG4gIC8vIFN0ZXAgNCAtIHBhcnNlIHByb3RvY29scyBhcmd1bWVudFxyXG4gIGlmICghcHJvdG9jb2xzKSB7XHJcbiAgICBwcm90b2NvbHMgPSBbXTtcclxuICB9IGVsc2UgaWYgKCFBcnJheS5pc0FycmF5KHByb3RvY29scykpIHtcclxuICAgIHByb3RvY29scyA9IFtwcm90b2NvbHNdO1xyXG4gIH1cclxuXHJcbiAgLy8gU3RlcCA1IC0gY2hlY2sgcHJvdG9jb2xzIGFyZ3VtZW50XHJcbiAgdmFyIHNvcnRlZFByb3RvY29scyA9IHByb3RvY29scy5zb3J0KCk7XHJcbiAgc29ydGVkUHJvdG9jb2xzLmZvckVhY2goZnVuY3Rpb24ocHJvdG8sIGkpIHtcclxuICAgIGlmICghcHJvdG8pIHtcclxuICAgICAgdGhyb3cgbmV3IFN5bnRheEVycm9yKFwiVGhlIHByb3RvY29scyBlbnRyeSAnXCIgKyBwcm90byArIFwiJyBpcyBpbnZhbGlkLlwiKTtcclxuICAgIH1cclxuICAgIGlmIChpIDwgKHNvcnRlZFByb3RvY29scy5sZW5ndGggLSAxKSAmJiBwcm90byA9PT0gc29ydGVkUHJvdG9jb2xzW2kgKyAxXSkge1xyXG4gICAgICB0aHJvdyBuZXcgU3ludGF4RXJyb3IoXCJUaGUgcHJvdG9jb2xzIGVudHJ5ICdcIiArIHByb3RvICsgXCInIGlzIGR1cGxpY2F0ZWQuXCIpO1xyXG4gICAgfVxyXG4gIH0pO1xyXG5cclxuICAvLyBTdGVwIDYgLSBjb252ZXJ0IG9yaWdpblxyXG4gIHZhciBvID0gdXJsVXRpbHMuZ2V0T3JpZ2luKGxvYy5ocmVmKTtcclxuICB0aGlzLl9vcmlnaW4gPSBvID8gby50b0xvd2VyQ2FzZSgpIDogbnVsbDtcclxuXHJcbiAgLy8gcmVtb3ZlIHRoZSB0cmFpbGluZyBzbGFzaFxyXG4gIHBhcnNlZFVybC5zZXQoJ3BhdGhuYW1lJywgcGFyc2VkVXJsLnBhdGhuYW1lLnJlcGxhY2UoL1xcLyskLywgJycpKTtcclxuXHJcbiAgLy8gc3RvcmUgdGhlIHNhbml0aXplZCB1cmxcclxuICB0aGlzLnVybCA9IHBhcnNlZFVybC5ocmVmO1xyXG4gIGRlYnVnKCd1c2luZyB1cmwnLCB0aGlzLnVybCk7XHJcblxyXG4gIC8vIFN0ZXAgNyAtIHN0YXJ0IGNvbm5lY3Rpb24gaW4gYmFja2dyb3VuZFxyXG4gIC8vIG9idGFpbiBzZXJ2ZXIgaW5mb1xyXG4gIC8vIGh0dHA6Ly9zb2NranMuZ2l0aHViLmlvL3NvY2tqcy1wcm90b2NvbC9zb2NranMtcHJvdG9jb2wtMC4zLjMuaHRtbCNzZWN0aW9uLTI2XHJcbiAgdGhpcy5fdXJsSW5mbyA9IHtcclxuICAgIG51bGxPcmlnaW46ICFicm93c2VyLmhhc0RvbWFpbigpXHJcbiAgLCBzYW1lT3JpZ2luOiB1cmxVdGlscy5pc09yaWdpbkVxdWFsKHRoaXMudXJsLCBsb2MuaHJlZilcclxuICAsIHNhbWVTY2hlbWU6IHVybFV0aWxzLmlzU2NoZW1lRXF1YWwodGhpcy51cmwsIGxvYy5ocmVmKVxyXG4gIH07XHJcblxyXG4gIHRoaXMuX2lyID0gbmV3IEluZm9SZWNlaXZlcih0aGlzLnVybCwgdGhpcy5fdXJsSW5mbyk7XHJcbiAgdGhpcy5faXIub25jZSgnZmluaXNoJywgdGhpcy5fcmVjZWl2ZUluZm8uYmluZCh0aGlzKSk7XHJcbn1cclxuXHJcbmluaGVyaXRzKFNvY2tKUywgRXZlbnRUYXJnZXQpO1xyXG5cclxuZnVuY3Rpb24gdXNlclNldENvZGUoY29kZSkge1xyXG4gIHJldHVybiBjb2RlID09PSAxMDAwIHx8IChjb2RlID49IDMwMDAgJiYgY29kZSA8PSA0OTk5KTtcclxufVxyXG5cclxuU29ja0pTLnByb3RvdHlwZS5jbG9zZSA9IGZ1bmN0aW9uKGNvZGUsIHJlYXNvbikge1xyXG4gIC8vIFN0ZXAgMVxyXG4gIGlmIChjb2RlICYmICF1c2VyU2V0Q29kZShjb2RlKSkge1xyXG4gICAgdGhyb3cgbmV3IEVycm9yKCdJbnZhbGlkQWNjZXNzRXJyb3I6IEludmFsaWQgY29kZScpO1xyXG4gIH1cclxuICAvLyBTdGVwIDIuNCBzdGF0ZXMgdGhlIG1heCBpcyAxMjMgYnl0ZXMsIGJ1dCB3ZSBhcmUganVzdCBjaGVja2luZyBsZW5ndGhcclxuICBpZiAocmVhc29uICYmIHJlYXNvbi5sZW5ndGggPiAxMjMpIHtcclxuICAgIHRocm93IG5ldyBTeW50YXhFcnJvcigncmVhc29uIGFyZ3VtZW50IGhhcyBhbiBpbnZhbGlkIGxlbmd0aCcpO1xyXG4gIH1cclxuXHJcbiAgLy8gU3RlcCAzLjFcclxuICBpZiAodGhpcy5yZWFkeVN0YXRlID09PSBTb2NrSlMuQ0xPU0lORyB8fCB0aGlzLnJlYWR5U3RhdGUgPT09IFNvY2tKUy5DTE9TRUQpIHtcclxuICAgIHJldHVybjtcclxuICB9XHJcblxyXG4gIC8vIFRPRE8gbG9vayBhdCBkb2NzIHRvIGRldGVybWluZSBob3cgdG8gc2V0IHRoaXNcclxuICB2YXIgd2FzQ2xlYW4gPSB0cnVlO1xyXG4gIHRoaXMuX2Nsb3NlKGNvZGUgfHwgMTAwMCwgcmVhc29uIHx8ICdOb3JtYWwgY2xvc3VyZScsIHdhc0NsZWFuKTtcclxufTtcclxuXHJcblNvY2tKUy5wcm90b3R5cGUuc2VuZCA9IGZ1bmN0aW9uKGRhdGEpIHtcclxuICAvLyAjMTMgLSBjb252ZXJ0IGFueXRoaW5nIG5vbi1zdHJpbmcgdG8gc3RyaW5nXHJcbiAgLy8gVE9ETyB0aGlzIGN1cnJlbnRseSB0dXJucyBvYmplY3RzIGludG8gW29iamVjdCBPYmplY3RdXHJcbiAgaWYgKHR5cGVvZiBkYXRhICE9PSAnc3RyaW5nJykge1xyXG4gICAgZGF0YSA9ICcnICsgZGF0YTtcclxuICB9XHJcbiAgaWYgKHRoaXMucmVhZHlTdGF0ZSA9PT0gU29ja0pTLkNPTk5FQ1RJTkcpIHtcclxuICAgIHRocm93IG5ldyBFcnJvcignSW52YWxpZFN0YXRlRXJyb3I6IFRoZSBjb25uZWN0aW9uIGhhcyBub3QgYmVlbiBlc3RhYmxpc2hlZCB5ZXQnKTtcclxuICB9XHJcbiAgaWYgKHRoaXMucmVhZHlTdGF0ZSAhPT0gU29ja0pTLk9QRU4pIHtcclxuICAgIHJldHVybjtcclxuICB9XHJcbiAgdGhpcy5fdHJhbnNwb3J0LnNlbmQoZXNjYXBlLnF1b3RlKGRhdGEpKTtcclxufTtcclxuXHJcblNvY2tKUy52ZXJzaW9uID0gcmVxdWlyZSgnLi92ZXJzaW9uJyk7XHJcblxyXG5Tb2NrSlMuQ09OTkVDVElORyA9IDA7XHJcblNvY2tKUy5PUEVOID0gMTtcclxuU29ja0pTLkNMT1NJTkcgPSAyO1xyXG5Tb2NrSlMuQ0xPU0VEID0gMztcclxuXHJcblNvY2tKUy5wcm90b3R5cGUuX3JlY2VpdmVJbmZvID0gZnVuY3Rpb24oaW5mbywgcnR0KSB7XHJcbiAgZGVidWcoJ19yZWNlaXZlSW5mbycsIHJ0dCk7XHJcbiAgdGhpcy5faXIgPSBudWxsO1xyXG4gIGlmICghaW5mbykge1xyXG4gICAgdGhpcy5fY2xvc2UoMTAwMiwgJ0Nhbm5vdCBjb25uZWN0IHRvIHNlcnZlcicpO1xyXG4gICAgcmV0dXJuO1xyXG4gIH1cclxuXHJcbiAgLy8gZXN0YWJsaXNoIGEgcm91bmQtdHJpcCB0aW1lb3V0IChSVE8pIGJhc2VkIG9uIHRoZVxyXG4gIC8vIHJvdW5kLXRyaXAgdGltZSAoUlRUKVxyXG4gIHRoaXMuX3J0byA9IHRoaXMuY291bnRSVE8ocnR0KTtcclxuICAvLyBhbGxvdyBzZXJ2ZXIgdG8gb3ZlcnJpZGUgdXJsIHVzZWQgZm9yIHRoZSBhY3R1YWwgdHJhbnNwb3J0XHJcbiAgdGhpcy5fdHJhbnNVcmwgPSBpbmZvLmJhc2VfdXJsID8gaW5mby5iYXNlX3VybCA6IHRoaXMudXJsO1xyXG4gIGluZm8gPSBvYmplY3RVdGlscy5leHRlbmQoaW5mbywgdGhpcy5fdXJsSW5mbyk7XHJcbiAgZGVidWcoJ2luZm8nLCBpbmZvKTtcclxuICAvLyBkZXRlcm1pbmUgbGlzdCBvZiBkZXNpcmVkIGFuZCBzdXBwb3J0ZWQgdHJhbnNwb3J0c1xyXG4gIHZhciBlbmFibGVkVHJhbnNwb3J0cyA9IHRyYW5zcG9ydHMuZmlsdGVyVG9FbmFibGVkKHRoaXMuX3RyYW5zcG9ydHNXaGl0ZWxpc3QsIGluZm8pO1xyXG4gIHRoaXMuX3RyYW5zcG9ydHMgPSBlbmFibGVkVHJhbnNwb3J0cy5tYWluO1xyXG4gIGRlYnVnKHRoaXMuX3RyYW5zcG9ydHMubGVuZ3RoICsgJyBlbmFibGVkIHRyYW5zcG9ydHMnKTtcclxuXHJcbiAgdGhpcy5fY29ubmVjdCgpO1xyXG59O1xyXG5cclxuU29ja0pTLnByb3RvdHlwZS5fY29ubmVjdCA9IGZ1bmN0aW9uKCkge1xyXG4gIGZvciAodmFyIFRyYW5zcG9ydCA9IHRoaXMuX3RyYW5zcG9ydHMuc2hpZnQoKTsgVHJhbnNwb3J0OyBUcmFuc3BvcnQgPSB0aGlzLl90cmFuc3BvcnRzLnNoaWZ0KCkpIHtcclxuICAgIGRlYnVnKCdhdHRlbXB0JywgVHJhbnNwb3J0LnRyYW5zcG9ydE5hbWUpO1xyXG4gICAgaWYgKFRyYW5zcG9ydC5uZWVkQm9keSkge1xyXG4gICAgICBpZiAoIWdsb2JhbC5kb2N1bWVudC5ib2R5IHx8XHJcbiAgICAgICAgICAodHlwZW9mIGdsb2JhbC5kb2N1bWVudC5yZWFkeVN0YXRlICE9PSAndW5kZWZpbmVkJyAmJlxyXG4gICAgICAgICAgICBnbG9iYWwuZG9jdW1lbnQucmVhZHlTdGF0ZSAhPT0gJ2NvbXBsZXRlJyAmJlxyXG4gICAgICAgICAgICBnbG9iYWwuZG9jdW1lbnQucmVhZHlTdGF0ZSAhPT0gJ2ludGVyYWN0aXZlJykpIHtcclxuICAgICAgICBkZWJ1Zygnd2FpdGluZyBmb3IgYm9keScpO1xyXG4gICAgICAgIHRoaXMuX3RyYW5zcG9ydHMudW5zaGlmdChUcmFuc3BvcnQpO1xyXG4gICAgICAgIGV2ZW50VXRpbHMuYXR0YWNoRXZlbnQoJ2xvYWQnLCB0aGlzLl9jb25uZWN0LmJpbmQodGhpcykpO1xyXG4gICAgICAgIHJldHVybjtcclxuICAgICAgfVxyXG4gICAgfVxyXG5cclxuICAgIC8vIGNhbGN1bGF0ZSB0aW1lb3V0IGJhc2VkIG9uIFJUTyBhbmQgcm91bmQgdHJpcHMuIERlZmF1bHQgdG8gNXNcclxuICAgIHZhciB0aW1lb3V0TXMgPSAodGhpcy5fcnRvICogVHJhbnNwb3J0LnJvdW5kVHJpcHMpIHx8IDUwMDA7XHJcbiAgICB0aGlzLl90cmFuc3BvcnRUaW1lb3V0SWQgPSBzZXRUaW1lb3V0KHRoaXMuX3RyYW5zcG9ydFRpbWVvdXQuYmluZCh0aGlzKSwgdGltZW91dE1zKTtcclxuICAgIGRlYnVnKCd1c2luZyB0aW1lb3V0JywgdGltZW91dE1zKTtcclxuXHJcbiAgICB2YXIgdHJhbnNwb3J0VXJsID0gdXJsVXRpbHMuYWRkUGF0aCh0aGlzLl90cmFuc1VybCwgJy8nICsgdGhpcy5fc2VydmVyICsgJy8nICsgdGhpcy5fZ2VuZXJhdGVTZXNzaW9uSWQoKSk7XHJcbiAgICB2YXIgb3B0aW9ucyA9IHRoaXMuX3RyYW5zcG9ydE9wdGlvbnNbVHJhbnNwb3J0LnRyYW5zcG9ydE5hbWVdO1xyXG4gICAgZGVidWcoJ3RyYW5zcG9ydCB1cmwnLCB0cmFuc3BvcnRVcmwpO1xyXG4gICAgdmFyIHRyYW5zcG9ydE9iaiA9IG5ldyBUcmFuc3BvcnQodHJhbnNwb3J0VXJsLCB0aGlzLl90cmFuc1VybCwgb3B0aW9ucyk7XHJcbiAgICB0cmFuc3BvcnRPYmoub24oJ21lc3NhZ2UnLCB0aGlzLl90cmFuc3BvcnRNZXNzYWdlLmJpbmQodGhpcykpO1xyXG4gICAgdHJhbnNwb3J0T2JqLm9uY2UoJ2Nsb3NlJywgdGhpcy5fdHJhbnNwb3J0Q2xvc2UuYmluZCh0aGlzKSk7XHJcbiAgICB0cmFuc3BvcnRPYmoudHJhbnNwb3J0TmFtZSA9IFRyYW5zcG9ydC50cmFuc3BvcnROYW1lO1xyXG4gICAgdGhpcy5fdHJhbnNwb3J0ID0gdHJhbnNwb3J0T2JqO1xyXG5cclxuICAgIHJldHVybjtcclxuICB9XHJcbiAgdGhpcy5fY2xvc2UoMjAwMCwgJ0FsbCB0cmFuc3BvcnRzIGZhaWxlZCcsIGZhbHNlKTtcclxufTtcclxuXHJcblNvY2tKUy5wcm90b3R5cGUuX3RyYW5zcG9ydFRpbWVvdXQgPSBmdW5jdGlvbigpIHtcclxuICBkZWJ1ZygnX3RyYW5zcG9ydFRpbWVvdXQnKTtcclxuICBpZiAodGhpcy5yZWFkeVN0YXRlID09PSBTb2NrSlMuQ09OTkVDVElORykge1xyXG4gICAgdGhpcy5fdHJhbnNwb3J0Q2xvc2UoMjAwNywgJ1RyYW5zcG9ydCB0aW1lZCBvdXQnKTtcclxuICB9XHJcbn07XHJcblxyXG5Tb2NrSlMucHJvdG90eXBlLl90cmFuc3BvcnRNZXNzYWdlID0gZnVuY3Rpb24obXNnKSB7XHJcbiAgZGVidWcoJ190cmFuc3BvcnRNZXNzYWdlJywgbXNnKTtcclxuICB2YXIgc2VsZiA9IHRoaXNcclxuICAgICwgdHlwZSA9IG1zZy5zbGljZSgwLCAxKVxyXG4gICAgLCBjb250ZW50ID0gbXNnLnNsaWNlKDEpXHJcbiAgICAsIHBheWxvYWRcclxuICAgIDtcclxuXHJcbiAgLy8gZmlyc3QgY2hlY2sgZm9yIG1lc3NhZ2VzIHRoYXQgZG9uJ3QgbmVlZCBhIHBheWxvYWRcclxuICBzd2l0Y2ggKHR5cGUpIHtcclxuICAgIGNhc2UgJ28nOlxyXG4gICAgICB0aGlzLl9vcGVuKCk7XHJcbiAgICAgIHJldHVybjtcclxuICAgIGNhc2UgJ2gnOlxyXG4gICAgICB0aGlzLmRpc3BhdGNoRXZlbnQobmV3IEV2ZW50KCdoZWFydGJlYXQnKSk7XHJcbiAgICAgIGRlYnVnKCdoZWFydGJlYXQnLCB0aGlzLnRyYW5zcG9ydCk7XHJcbiAgICAgIHJldHVybjtcclxuICB9XHJcblxyXG4gIGlmIChjb250ZW50KSB7XHJcbiAgICB0cnkge1xyXG4gICAgICBwYXlsb2FkID0gSlNPTjMucGFyc2UoY29udGVudCk7XHJcbiAgICB9IGNhdGNoIChlKSB7XHJcbiAgICAgIGRlYnVnKCdiYWQganNvbicsIGNvbnRlbnQpO1xyXG4gICAgfVxyXG4gIH1cclxuXHJcbiAgaWYgKHR5cGVvZiBwYXlsb2FkID09PSAndW5kZWZpbmVkJykge1xyXG4gICAgZGVidWcoJ2VtcHR5IHBheWxvYWQnLCBjb250ZW50KTtcclxuICAgIHJldHVybjtcclxuICB9XHJcblxyXG4gIHN3aXRjaCAodHlwZSkge1xyXG4gICAgY2FzZSAnYSc6XHJcbiAgICAgIGlmIChBcnJheS5pc0FycmF5KHBheWxvYWQpKSB7XHJcbiAgICAgICAgcGF5bG9hZC5mb3JFYWNoKGZ1bmN0aW9uKHApIHtcclxuICAgICAgICAgIGRlYnVnKCdtZXNzYWdlJywgc2VsZi50cmFuc3BvcnQsIHApO1xyXG4gICAgICAgICAgc2VsZi5kaXNwYXRjaEV2ZW50KG5ldyBUcmFuc3BvcnRNZXNzYWdlRXZlbnQocCkpO1xyXG4gICAgICAgIH0pO1xyXG4gICAgICB9XHJcbiAgICAgIGJyZWFrO1xyXG4gICAgY2FzZSAnbSc6XHJcbiAgICAgIGRlYnVnKCdtZXNzYWdlJywgdGhpcy50cmFuc3BvcnQsIHBheWxvYWQpO1xyXG4gICAgICB0aGlzLmRpc3BhdGNoRXZlbnQobmV3IFRyYW5zcG9ydE1lc3NhZ2VFdmVudChwYXlsb2FkKSk7XHJcbiAgICAgIGJyZWFrO1xyXG4gICAgY2FzZSAnYyc6XHJcbiAgICAgIGlmIChBcnJheS5pc0FycmF5KHBheWxvYWQpICYmIHBheWxvYWQubGVuZ3RoID09PSAyKSB7XHJcbiAgICAgICAgdGhpcy5fY2xvc2UocGF5bG9hZFswXSwgcGF5bG9hZFsxXSwgdHJ1ZSk7XHJcbiAgICAgIH1cclxuICAgICAgYnJlYWs7XHJcbiAgfVxyXG59O1xyXG5cclxuU29ja0pTLnByb3RvdHlwZS5fdHJhbnNwb3J0Q2xvc2UgPSBmdW5jdGlvbihjb2RlLCByZWFzb24pIHtcclxuICBkZWJ1ZygnX3RyYW5zcG9ydENsb3NlJywgdGhpcy50cmFuc3BvcnQsIGNvZGUsIHJlYXNvbik7XHJcbiAgaWYgKHRoaXMuX3RyYW5zcG9ydCkge1xyXG4gICAgdGhpcy5fdHJhbnNwb3J0LnJlbW92ZUFsbExpc3RlbmVycygpO1xyXG4gICAgdGhpcy5fdHJhbnNwb3J0ID0gbnVsbDtcclxuICAgIHRoaXMudHJhbnNwb3J0ID0gbnVsbDtcclxuICB9XHJcblxyXG4gIGlmICghdXNlclNldENvZGUoY29kZSkgJiYgY29kZSAhPT0gMjAwMCAmJiB0aGlzLnJlYWR5U3RhdGUgPT09IFNvY2tKUy5DT05ORUNUSU5HKSB7XHJcbiAgICB0aGlzLl9jb25uZWN0KCk7XHJcbiAgICByZXR1cm47XHJcbiAgfVxyXG5cclxuICB0aGlzLl9jbG9zZShjb2RlLCByZWFzb24pO1xyXG59O1xyXG5cclxuU29ja0pTLnByb3RvdHlwZS5fb3BlbiA9IGZ1bmN0aW9uKCkge1xyXG4gIGRlYnVnKCdfb3BlbicsIHRoaXMuX3RyYW5zcG9ydC50cmFuc3BvcnROYW1lLCB0aGlzLnJlYWR5U3RhdGUpO1xyXG4gIGlmICh0aGlzLnJlYWR5U3RhdGUgPT09IFNvY2tKUy5DT05ORUNUSU5HKSB7XHJcbiAgICBpZiAodGhpcy5fdHJhbnNwb3J0VGltZW91dElkKSB7XHJcbiAgICAgIGNsZWFyVGltZW91dCh0aGlzLl90cmFuc3BvcnRUaW1lb3V0SWQpO1xyXG4gICAgICB0aGlzLl90cmFuc3BvcnRUaW1lb3V0SWQgPSBudWxsO1xyXG4gICAgfVxyXG4gICAgdGhpcy5yZWFkeVN0YXRlID0gU29ja0pTLk9QRU47XHJcbiAgICB0aGlzLnRyYW5zcG9ydCA9IHRoaXMuX3RyYW5zcG9ydC50cmFuc3BvcnROYW1lO1xyXG4gICAgdGhpcy5kaXNwYXRjaEV2ZW50KG5ldyBFdmVudCgnb3BlbicpKTtcclxuICAgIGRlYnVnKCdjb25uZWN0ZWQnLCB0aGlzLnRyYW5zcG9ydCk7XHJcbiAgfSBlbHNlIHtcclxuICAgIC8vIFRoZSBzZXJ2ZXIgbWlnaHQgaGF2ZSBiZWVuIHJlc3RhcnRlZCwgYW5kIGxvc3QgdHJhY2sgb2Ygb3VyXHJcbiAgICAvLyBjb25uZWN0aW9uLlxyXG4gICAgdGhpcy5fY2xvc2UoMTAwNiwgJ1NlcnZlciBsb3N0IHNlc3Npb24nKTtcclxuICB9XHJcbn07XHJcblxyXG5Tb2NrSlMucHJvdG90eXBlLl9jbG9zZSA9IGZ1bmN0aW9uKGNvZGUsIHJlYXNvbiwgd2FzQ2xlYW4pIHtcclxuICBkZWJ1ZygnX2Nsb3NlJywgdGhpcy50cmFuc3BvcnQsIGNvZGUsIHJlYXNvbiwgd2FzQ2xlYW4sIHRoaXMucmVhZHlTdGF0ZSk7XHJcbiAgdmFyIGZvcmNlRmFpbCA9IGZhbHNlO1xyXG5cclxuICBpZiAodGhpcy5faXIpIHtcclxuICAgIGZvcmNlRmFpbCA9IHRydWU7XHJcbiAgICB0aGlzLl9pci5jbG9zZSgpO1xyXG4gICAgdGhpcy5faXIgPSBudWxsO1xyXG4gIH1cclxuICBpZiAodGhpcy5fdHJhbnNwb3J0KSB7XHJcbiAgICB0aGlzLl90cmFuc3BvcnQuY2xvc2UoKTtcclxuICAgIHRoaXMuX3RyYW5zcG9ydCA9IG51bGw7XHJcbiAgICB0aGlzLnRyYW5zcG9ydCA9IG51bGw7XHJcbiAgfVxyXG5cclxuICBpZiAodGhpcy5yZWFkeVN0YXRlID09PSBTb2NrSlMuQ0xPU0VEKSB7XHJcbiAgICB0aHJvdyBuZXcgRXJyb3IoJ0ludmFsaWRTdGF0ZUVycm9yOiBTb2NrSlMgaGFzIGFscmVhZHkgYmVlbiBjbG9zZWQnKTtcclxuICB9XHJcblxyXG4gIHRoaXMucmVhZHlTdGF0ZSA9IFNvY2tKUy5DTE9TSU5HO1xyXG4gIHNldFRpbWVvdXQoZnVuY3Rpb24oKSB7XHJcbiAgICB0aGlzLnJlYWR5U3RhdGUgPSBTb2NrSlMuQ0xPU0VEO1xyXG5cclxuICAgIGlmIChmb3JjZUZhaWwpIHtcclxuICAgICAgdGhpcy5kaXNwYXRjaEV2ZW50KG5ldyBFdmVudCgnZXJyb3InKSk7XHJcbiAgICB9XHJcblxyXG4gICAgdmFyIGUgPSBuZXcgQ2xvc2VFdmVudCgnY2xvc2UnKTtcclxuICAgIGUud2FzQ2xlYW4gPSB3YXNDbGVhbiB8fCBmYWxzZTtcclxuICAgIGUuY29kZSA9IGNvZGUgfHwgMTAwMDtcclxuICAgIGUucmVhc29uID0gcmVhc29uO1xyXG5cclxuICAgIHRoaXMuZGlzcGF0Y2hFdmVudChlKTtcclxuICAgIHRoaXMub25tZXNzYWdlID0gdGhpcy5vbmNsb3NlID0gdGhpcy5vbmVycm9yID0gbnVsbDtcclxuICAgIGRlYnVnKCdkaXNjb25uZWN0ZWQnKTtcclxuICB9LmJpbmQodGhpcyksIDApO1xyXG59O1xyXG5cclxuLy8gU2VlOiBodHRwOi8vd3d3LmVyZy5hYmRuLmFjLnVrL35nZXJyaXQvZGNjcC9ub3Rlcy9jY2lkMi9ydG9fZXN0aW1hdG9yL1xyXG4vLyBhbmQgUkZDIDI5ODguXHJcblNvY2tKUy5wcm90b3R5cGUuY291bnRSVE8gPSBmdW5jdGlvbihydHQpIHtcclxuICAvLyBJbiBhIGxvY2FsIGVudmlyb25tZW50LCB3aGVuIHVzaW5nIElFOC85IGFuZCB0aGUgYGpzb25wLXBvbGxpbmdgXHJcbiAgLy8gdHJhbnNwb3J0IHRoZSB0aW1lIG5lZWRlZCB0byBlc3RhYmxpc2ggYSBjb25uZWN0aW9uICh0aGUgdGltZSB0aGF0IHBhc3NcclxuICAvLyBmcm9tIHRoZSBvcGVuaW5nIG9mIHRoZSB0cmFuc3BvcnQgdG8gdGhlIGNhbGwgb2YgYF9kaXNwYXRjaE9wZW5gKSBpc1xyXG4gIC8vIGFyb3VuZCAyMDBtc2VjICh0aGUgbG93ZXIgYm91bmQgdXNlZCBpbiB0aGUgYXJ0aWNsZSBhYm92ZSkgYW5kIHRoaXNcclxuICAvLyBjYXVzZXMgc3B1cmlvdXMgdGltZW91dHMuIEZvciB0aGlzIHJlYXNvbiB3ZSBjYWxjdWxhdGUgYSB2YWx1ZSBzbGlnaHRseVxyXG4gIC8vIGxhcmdlciB0aGFuIHRoYXQgdXNlZCBpbiB0aGUgYXJ0aWNsZS5cclxuICBpZiAocnR0ID4gMTAwKSB7XHJcbiAgICByZXR1cm4gNCAqIHJ0dDsgLy8gcnRvID4gNDAwbXNlY1xyXG4gIH1cclxuICByZXR1cm4gMzAwICsgcnR0OyAvLyAzMDBtc2VjIDwgcnRvIDw9IDQwMG1zZWNcclxufTtcclxuXHJcbm1vZHVsZS5leHBvcnRzID0gZnVuY3Rpb24oYXZhaWxhYmxlVHJhbnNwb3J0cykge1xyXG4gIHRyYW5zcG9ydHMgPSB0cmFuc3BvcnQoYXZhaWxhYmxlVHJhbnNwb3J0cyk7XHJcbiAgcmVxdWlyZSgnLi9pZnJhbWUtYm9vdHN0cmFwJykoU29ja0pTLCBhdmFpbGFibGVUcmFuc3BvcnRzKTtcclxuICByZXR1cm4gU29ja0pTO1xyXG59O1xyXG5cclxufSkuY2FsbCh0aGlzLHsgZW52OiB7fSB9LHR5cGVvZiBnbG9iYWwgIT09IFwidW5kZWZpbmVkXCIgPyBnbG9iYWwgOiB0eXBlb2Ygc2VsZiAhPT0gXCJ1bmRlZmluZWRcIiA/IHNlbGYgOiB0eXBlb2Ygd2luZG93ICE9PSBcInVuZGVmaW5lZFwiID8gd2luZG93IDoge30pXHJcblxyXG59LHtcIi4vZXZlbnQvY2xvc2VcIjoyLFwiLi9ldmVudC9ldmVudFwiOjQsXCIuL2V2ZW50L2V2ZW50dGFyZ2V0XCI6NSxcIi4vZXZlbnQvdHJhbnMtbWVzc2FnZVwiOjYsXCIuL2lmcmFtZS1ib290c3RyYXBcIjo4LFwiLi9pbmZvLXJlY2VpdmVyXCI6MTIsXCIuL2xvY2F0aW9uXCI6MTMsXCIuL3NoaW1zXCI6MTUsXCIuL3V0aWxzL2Jyb3dzZXJcIjo0NCxcIi4vdXRpbHMvZXNjYXBlXCI6NDUsXCIuL3V0aWxzL2V2ZW50XCI6NDYsXCIuL3V0aWxzL2xvZ1wiOjQ4LFwiLi91dGlscy9vYmplY3RcIjo0OSxcIi4vdXRpbHMvcmFuZG9tXCI6NTAsXCIuL3V0aWxzL3RyYW5zcG9ydFwiOjUxLFwiLi91dGlscy91cmxcIjo1MixcIi4vdmVyc2lvblwiOjUzLFwiZGVidWdcIjo1NSxcImluaGVyaXRzXCI6NTcsXCJqc29uM1wiOjU4LFwidXJsLXBhcnNlXCI6NjF9XSwxNTpbZnVuY3Rpb24ocmVxdWlyZSxtb2R1bGUsZXhwb3J0cyl7XHJcbi8qIGVzbGludC1kaXNhYmxlICovXHJcbi8qIGpzY3M6IGRpc2FibGUgKi9cclxuJ3VzZSBzdHJpY3QnO1xyXG5cclxuLy8gcHVsbGVkIHNwZWNpZmljIHNoaW1zIGZyb20gaHR0cHM6Ly9naXRodWIuY29tL2VzLXNoaW1zL2VzNS1zaGltXHJcblxyXG52YXIgQXJyYXlQcm90b3R5cGUgPSBBcnJheS5wcm90b3R5cGU7XHJcbnZhciBPYmplY3RQcm90b3R5cGUgPSBPYmplY3QucHJvdG90eXBlO1xyXG52YXIgRnVuY3Rpb25Qcm90b3R5cGUgPSBGdW5jdGlvbi5wcm90b3R5cGU7XHJcbnZhciBTdHJpbmdQcm90b3R5cGUgPSBTdHJpbmcucHJvdG90eXBlO1xyXG52YXIgYXJyYXlfc2xpY2UgPSBBcnJheVByb3RvdHlwZS5zbGljZTtcclxuXHJcbnZhciBfdG9TdHJpbmcgPSBPYmplY3RQcm90b3R5cGUudG9TdHJpbmc7XHJcbnZhciBpc0Z1bmN0aW9uID0gZnVuY3Rpb24gKHZhbCkge1xyXG4gICAgcmV0dXJuIE9iamVjdFByb3RvdHlwZS50b1N0cmluZy5jYWxsKHZhbCkgPT09ICdbb2JqZWN0IEZ1bmN0aW9uXSc7XHJcbn07XHJcbnZhciBpc0FycmF5ID0gZnVuY3Rpb24gaXNBcnJheShvYmopIHtcclxuICAgIHJldHVybiBfdG9TdHJpbmcuY2FsbChvYmopID09PSAnW29iamVjdCBBcnJheV0nO1xyXG59O1xyXG52YXIgaXNTdHJpbmcgPSBmdW5jdGlvbiBpc1N0cmluZyhvYmopIHtcclxuICAgIHJldHVybiBfdG9TdHJpbmcuY2FsbChvYmopID09PSAnW29iamVjdCBTdHJpbmddJztcclxufTtcclxuXHJcbnZhciBzdXBwb3J0c0Rlc2NyaXB0b3JzID0gT2JqZWN0LmRlZmluZVByb3BlcnR5ICYmIChmdW5jdGlvbiAoKSB7XHJcbiAgICB0cnkge1xyXG4gICAgICAgIE9iamVjdC5kZWZpbmVQcm9wZXJ0eSh7fSwgJ3gnLCB7fSk7XHJcbiAgICAgICAgcmV0dXJuIHRydWU7XHJcbiAgICB9IGNhdGNoIChlKSB7IC8qIHRoaXMgaXMgRVMzICovXHJcbiAgICAgICAgcmV0dXJuIGZhbHNlO1xyXG4gICAgfVxyXG59KCkpO1xyXG5cclxuLy8gRGVmaW5lIGNvbmZpZ3VyYWJsZSwgd3JpdGFibGUgYW5kIG5vbi1lbnVtZXJhYmxlIHByb3BzXHJcbi8vIGlmIHRoZXkgZG9uJ3QgZXhpc3QuXHJcbnZhciBkZWZpbmVQcm9wZXJ0eTtcclxuaWYgKHN1cHBvcnRzRGVzY3JpcHRvcnMpIHtcclxuICAgIGRlZmluZVByb3BlcnR5ID0gZnVuY3Rpb24gKG9iamVjdCwgbmFtZSwgbWV0aG9kLCBmb3JjZUFzc2lnbikge1xyXG4gICAgICAgIGlmICghZm9yY2VBc3NpZ24gJiYgKG5hbWUgaW4gb2JqZWN0KSkgeyByZXR1cm47IH1cclxuICAgICAgICBPYmplY3QuZGVmaW5lUHJvcGVydHkob2JqZWN0LCBuYW1lLCB7XHJcbiAgICAgICAgICAgIGNvbmZpZ3VyYWJsZTogdHJ1ZSxcclxuICAgICAgICAgICAgZW51bWVyYWJsZTogZmFsc2UsXHJcbiAgICAgICAgICAgIHdyaXRhYmxlOiB0cnVlLFxyXG4gICAgICAgICAgICB2YWx1ZTogbWV0aG9kXHJcbiAgICAgICAgfSk7XHJcbiAgICB9O1xyXG59IGVsc2Uge1xyXG4gICAgZGVmaW5lUHJvcGVydHkgPSBmdW5jdGlvbiAob2JqZWN0LCBuYW1lLCBtZXRob2QsIGZvcmNlQXNzaWduKSB7XHJcbiAgICAgICAgaWYgKCFmb3JjZUFzc2lnbiAmJiAobmFtZSBpbiBvYmplY3QpKSB7IHJldHVybjsgfVxyXG4gICAgICAgIG9iamVjdFtuYW1lXSA9IG1ldGhvZDtcclxuICAgIH07XHJcbn1cclxudmFyIGRlZmluZVByb3BlcnRpZXMgPSBmdW5jdGlvbiAob2JqZWN0LCBtYXAsIGZvcmNlQXNzaWduKSB7XHJcbiAgICBmb3IgKHZhciBuYW1lIGluIG1hcCkge1xyXG4gICAgICAgIGlmIChPYmplY3RQcm90b3R5cGUuaGFzT3duUHJvcGVydHkuY2FsbChtYXAsIG5hbWUpKSB7XHJcbiAgICAgICAgICBkZWZpbmVQcm9wZXJ0eShvYmplY3QsIG5hbWUsIG1hcFtuYW1lXSwgZm9yY2VBc3NpZ24pO1xyXG4gICAgICAgIH1cclxuICAgIH1cclxufTtcclxuXHJcbnZhciB0b09iamVjdCA9IGZ1bmN0aW9uIChvKSB7XHJcbiAgICBpZiAobyA9PSBudWxsKSB7IC8vIHRoaXMgbWF0Y2hlcyBib3RoIG51bGwgYW5kIHVuZGVmaW5lZFxyXG4gICAgICAgIHRocm93IG5ldyBUeXBlRXJyb3IoXCJjYW4ndCBjb252ZXJ0IFwiICsgbyArICcgdG8gb2JqZWN0Jyk7XHJcbiAgICB9XHJcbiAgICByZXR1cm4gT2JqZWN0KG8pO1xyXG59O1xyXG5cclxuLy9cclxuLy8gVXRpbFxyXG4vLyA9PT09PT1cclxuLy9cclxuXHJcbi8vIEVTNSA5LjRcclxuLy8gaHR0cDovL2VzNS5naXRodWIuY29tLyN4OS40XHJcbi8vIGh0dHA6Ly9qc3BlcmYuY29tL3RvLWludGVnZXJcclxuXHJcbmZ1bmN0aW9uIHRvSW50ZWdlcihudW0pIHtcclxuICAgIHZhciBuID0gK251bTtcclxuICAgIGlmIChuICE9PSBuKSB7IC8vIGlzTmFOXHJcbiAgICAgICAgbiA9IDA7XHJcbiAgICB9IGVsc2UgaWYgKG4gIT09IDAgJiYgbiAhPT0gKDEgLyAwKSAmJiBuICE9PSAtKDEgLyAwKSkge1xyXG4gICAgICAgIG4gPSAobiA+IDAgfHwgLTEpICogTWF0aC5mbG9vcihNYXRoLmFicyhuKSk7XHJcbiAgICB9XHJcbiAgICByZXR1cm4gbjtcclxufVxyXG5cclxuZnVuY3Rpb24gVG9VaW50MzIoeCkge1xyXG4gICAgcmV0dXJuIHggPj4+IDA7XHJcbn1cclxuXHJcbi8vXHJcbi8vIEZ1bmN0aW9uXHJcbi8vID09PT09PT09XHJcbi8vXHJcblxyXG4vLyBFUy01IDE1LjMuNC41XHJcbi8vIGh0dHA6Ly9lczUuZ2l0aHViLmNvbS8jeDE1LjMuNC41XHJcblxyXG5mdW5jdGlvbiBFbXB0eSgpIHt9XHJcblxyXG5kZWZpbmVQcm9wZXJ0aWVzKEZ1bmN0aW9uUHJvdG90eXBlLCB7XHJcbiAgICBiaW5kOiBmdW5jdGlvbiBiaW5kKHRoYXQpIHsgLy8gLmxlbmd0aCBpcyAxXHJcbiAgICAgICAgLy8gMS4gTGV0IFRhcmdldCBiZSB0aGUgdGhpcyB2YWx1ZS5cclxuICAgICAgICB2YXIgdGFyZ2V0ID0gdGhpcztcclxuICAgICAgICAvLyAyLiBJZiBJc0NhbGxhYmxlKFRhcmdldCkgaXMgZmFsc2UsIHRocm93IGEgVHlwZUVycm9yIGV4Y2VwdGlvbi5cclxuICAgICAgICBpZiAoIWlzRnVuY3Rpb24odGFyZ2V0KSkge1xyXG4gICAgICAgICAgICB0aHJvdyBuZXcgVHlwZUVycm9yKCdGdW5jdGlvbi5wcm90b3R5cGUuYmluZCBjYWxsZWQgb24gaW5jb21wYXRpYmxlICcgKyB0YXJnZXQpO1xyXG4gICAgICAgIH1cclxuICAgICAgICAvLyAzLiBMZXQgQSBiZSBhIG5ldyAocG9zc2libHkgZW1wdHkpIGludGVybmFsIGxpc3Qgb2YgYWxsIG9mIHRoZVxyXG4gICAgICAgIC8vICAgYXJndW1lbnQgdmFsdWVzIHByb3ZpZGVkIGFmdGVyIHRoaXNBcmcgKGFyZzEsIGFyZzIgZXRjKSwgaW4gb3JkZXIuXHJcbiAgICAgICAgLy8gWFhYIHNsaWNlZEFyZ3Mgd2lsbCBzdGFuZCBpbiBmb3IgXCJBXCIgaWYgdXNlZFxyXG4gICAgICAgIHZhciBhcmdzID0gYXJyYXlfc2xpY2UuY2FsbChhcmd1bWVudHMsIDEpOyAvLyBmb3Igbm9ybWFsIGNhbGxcclxuICAgICAgICAvLyA0LiBMZXQgRiBiZSBhIG5ldyBuYXRpdmUgRUNNQVNjcmlwdCBvYmplY3QuXHJcbiAgICAgICAgLy8gMTEuIFNldCB0aGUgW1tQcm90b3R5cGVdXSBpbnRlcm5hbCBwcm9wZXJ0eSBvZiBGIHRvIHRoZSBzdGFuZGFyZFxyXG4gICAgICAgIC8vICAgYnVpbHQtaW4gRnVuY3Rpb24gcHJvdG90eXBlIG9iamVjdCBhcyBzcGVjaWZpZWQgaW4gMTUuMy4zLjEuXHJcbiAgICAgICAgLy8gMTIuIFNldCB0aGUgW1tDYWxsXV0gaW50ZXJuYWwgcHJvcGVydHkgb2YgRiBhcyBkZXNjcmliZWQgaW5cclxuICAgICAgICAvLyAgIDE1LjMuNC41LjEuXHJcbiAgICAgICAgLy8gMTMuIFNldCB0aGUgW1tDb25zdHJ1Y3RdXSBpbnRlcm5hbCBwcm9wZXJ0eSBvZiBGIGFzIGRlc2NyaWJlZCBpblxyXG4gICAgICAgIC8vICAgMTUuMy40LjUuMi5cclxuICAgICAgICAvLyAxNC4gU2V0IHRoZSBbW0hhc0luc3RhbmNlXV0gaW50ZXJuYWwgcHJvcGVydHkgb2YgRiBhcyBkZXNjcmliZWQgaW5cclxuICAgICAgICAvLyAgIDE1LjMuNC41LjMuXHJcbiAgICAgICAgdmFyIGJpbmRlciA9IGZ1bmN0aW9uICgpIHtcclxuXHJcbiAgICAgICAgICAgIGlmICh0aGlzIGluc3RhbmNlb2YgYm91bmQpIHtcclxuICAgICAgICAgICAgICAgIC8vIDE1LjMuNC41LjIgW1tDb25zdHJ1Y3RdXVxyXG4gICAgICAgICAgICAgICAgLy8gV2hlbiB0aGUgW1tDb25zdHJ1Y3RdXSBpbnRlcm5hbCBtZXRob2Qgb2YgYSBmdW5jdGlvbiBvYmplY3QsXHJcbiAgICAgICAgICAgICAgICAvLyBGIHRoYXQgd2FzIGNyZWF0ZWQgdXNpbmcgdGhlIGJpbmQgZnVuY3Rpb24gaXMgY2FsbGVkIHdpdGggYVxyXG4gICAgICAgICAgICAgICAgLy8gbGlzdCBvZiBhcmd1bWVudHMgRXh0cmFBcmdzLCB0aGUgZm9sbG93aW5nIHN0ZXBzIGFyZSB0YWtlbjpcclxuICAgICAgICAgICAgICAgIC8vIDEuIExldCB0YXJnZXQgYmUgdGhlIHZhbHVlIG9mIEYncyBbW1RhcmdldEZ1bmN0aW9uXV1cclxuICAgICAgICAgICAgICAgIC8vICAgaW50ZXJuYWwgcHJvcGVydHkuXHJcbiAgICAgICAgICAgICAgICAvLyAyLiBJZiB0YXJnZXQgaGFzIG5vIFtbQ29uc3RydWN0XV0gaW50ZXJuYWwgbWV0aG9kLCBhXHJcbiAgICAgICAgICAgICAgICAvLyAgIFR5cGVFcnJvciBleGNlcHRpb24gaXMgdGhyb3duLlxyXG4gICAgICAgICAgICAgICAgLy8gMy4gTGV0IGJvdW5kQXJncyBiZSB0aGUgdmFsdWUgb2YgRidzIFtbQm91bmRBcmdzXV0gaW50ZXJuYWxcclxuICAgICAgICAgICAgICAgIC8vICAgcHJvcGVydHkuXHJcbiAgICAgICAgICAgICAgICAvLyA0LiBMZXQgYXJncyBiZSBhIG5ldyBsaXN0IGNvbnRhaW5pbmcgdGhlIHNhbWUgdmFsdWVzIGFzIHRoZVxyXG4gICAgICAgICAgICAgICAgLy8gICBsaXN0IGJvdW5kQXJncyBpbiB0aGUgc2FtZSBvcmRlciBmb2xsb3dlZCBieSB0aGUgc2FtZVxyXG4gICAgICAgICAgICAgICAgLy8gICB2YWx1ZXMgYXMgdGhlIGxpc3QgRXh0cmFBcmdzIGluIHRoZSBzYW1lIG9yZGVyLlxyXG4gICAgICAgICAgICAgICAgLy8gNS4gUmV0dXJuIHRoZSByZXN1bHQgb2YgY2FsbGluZyB0aGUgW1tDb25zdHJ1Y3RdXSBpbnRlcm5hbFxyXG4gICAgICAgICAgICAgICAgLy8gICBtZXRob2Qgb2YgdGFyZ2V0IHByb3ZpZGluZyBhcmdzIGFzIHRoZSBhcmd1bWVudHMuXHJcblxyXG4gICAgICAgICAgICAgICAgdmFyIHJlc3VsdCA9IHRhcmdldC5hcHBseShcclxuICAgICAgICAgICAgICAgICAgICB0aGlzLFxyXG4gICAgICAgICAgICAgICAgICAgIGFyZ3MuY29uY2F0KGFycmF5X3NsaWNlLmNhbGwoYXJndW1lbnRzKSlcclxuICAgICAgICAgICAgICAgICk7XHJcbiAgICAgICAgICAgICAgICBpZiAoT2JqZWN0KHJlc3VsdCkgPT09IHJlc3VsdCkge1xyXG4gICAgICAgICAgICAgICAgICAgIHJldHVybiByZXN1bHQ7XHJcbiAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgICAgICByZXR1cm4gdGhpcztcclxuXHJcbiAgICAgICAgICAgIH0gZWxzZSB7XHJcbiAgICAgICAgICAgICAgICAvLyAxNS4zLjQuNS4xIFtbQ2FsbF1dXHJcbiAgICAgICAgICAgICAgICAvLyBXaGVuIHRoZSBbW0NhbGxdXSBpbnRlcm5hbCBtZXRob2Qgb2YgYSBmdW5jdGlvbiBvYmplY3QsIEYsXHJcbiAgICAgICAgICAgICAgICAvLyB3aGljaCB3YXMgY3JlYXRlZCB1c2luZyB0aGUgYmluZCBmdW5jdGlvbiBpcyBjYWxsZWQgd2l0aCBhXHJcbiAgICAgICAgICAgICAgICAvLyB0aGlzIHZhbHVlIGFuZCBhIGxpc3Qgb2YgYXJndW1lbnRzIEV4dHJhQXJncywgdGhlIGZvbGxvd2luZ1xyXG4gICAgICAgICAgICAgICAgLy8gc3RlcHMgYXJlIHRha2VuOlxyXG4gICAgICAgICAgICAgICAgLy8gMS4gTGV0IGJvdW5kQXJncyBiZSB0aGUgdmFsdWUgb2YgRidzIFtbQm91bmRBcmdzXV0gaW50ZXJuYWxcclxuICAgICAgICAgICAgICAgIC8vICAgcHJvcGVydHkuXHJcbiAgICAgICAgICAgICAgICAvLyAyLiBMZXQgYm91bmRUaGlzIGJlIHRoZSB2YWx1ZSBvZiBGJ3MgW1tCb3VuZFRoaXNdXSBpbnRlcm5hbFxyXG4gICAgICAgICAgICAgICAgLy8gICBwcm9wZXJ0eS5cclxuICAgICAgICAgICAgICAgIC8vIDMuIExldCB0YXJnZXQgYmUgdGhlIHZhbHVlIG9mIEYncyBbW1RhcmdldEZ1bmN0aW9uXV0gaW50ZXJuYWxcclxuICAgICAgICAgICAgICAgIC8vICAgcHJvcGVydHkuXHJcbiAgICAgICAgICAgICAgICAvLyA0LiBMZXQgYXJncyBiZSBhIG5ldyBsaXN0IGNvbnRhaW5pbmcgdGhlIHNhbWUgdmFsdWVzIGFzIHRoZVxyXG4gICAgICAgICAgICAgICAgLy8gICBsaXN0IGJvdW5kQXJncyBpbiB0aGUgc2FtZSBvcmRlciBmb2xsb3dlZCBieSB0aGUgc2FtZVxyXG4gICAgICAgICAgICAgICAgLy8gICB2YWx1ZXMgYXMgdGhlIGxpc3QgRXh0cmFBcmdzIGluIHRoZSBzYW1lIG9yZGVyLlxyXG4gICAgICAgICAgICAgICAgLy8gNS4gUmV0dXJuIHRoZSByZXN1bHQgb2YgY2FsbGluZyB0aGUgW1tDYWxsXV0gaW50ZXJuYWwgbWV0aG9kXHJcbiAgICAgICAgICAgICAgICAvLyAgIG9mIHRhcmdldCBwcm92aWRpbmcgYm91bmRUaGlzIGFzIHRoZSB0aGlzIHZhbHVlIGFuZFxyXG4gICAgICAgICAgICAgICAgLy8gICBwcm92aWRpbmcgYXJncyBhcyB0aGUgYXJndW1lbnRzLlxyXG5cclxuICAgICAgICAgICAgICAgIC8vIGVxdWl2OiB0YXJnZXQuY2FsbCh0aGlzLCAuLi5ib3VuZEFyZ3MsIC4uLmFyZ3MpXHJcbiAgICAgICAgICAgICAgICByZXR1cm4gdGFyZ2V0LmFwcGx5KFxyXG4gICAgICAgICAgICAgICAgICAgIHRoYXQsXHJcbiAgICAgICAgICAgICAgICAgICAgYXJncy5jb25jYXQoYXJyYXlfc2xpY2UuY2FsbChhcmd1bWVudHMpKVxyXG4gICAgICAgICAgICAgICAgKTtcclxuXHJcbiAgICAgICAgICAgIH1cclxuXHJcbiAgICAgICAgfTtcclxuXHJcbiAgICAgICAgLy8gMTUuIElmIHRoZSBbW0NsYXNzXV0gaW50ZXJuYWwgcHJvcGVydHkgb2YgVGFyZ2V0IGlzIFwiRnVuY3Rpb25cIiwgdGhlblxyXG4gICAgICAgIC8vICAgICBhLiBMZXQgTCBiZSB0aGUgbGVuZ3RoIHByb3BlcnR5IG9mIFRhcmdldCBtaW51cyB0aGUgbGVuZ3RoIG9mIEEuXHJcbiAgICAgICAgLy8gICAgIGIuIFNldCB0aGUgbGVuZ3RoIG93biBwcm9wZXJ0eSBvZiBGIHRvIGVpdGhlciAwIG9yIEwsIHdoaWNoZXZlciBpc1xyXG4gICAgICAgIC8vICAgICAgIGxhcmdlci5cclxuICAgICAgICAvLyAxNi4gRWxzZSBzZXQgdGhlIGxlbmd0aCBvd24gcHJvcGVydHkgb2YgRiB0byAwLlxyXG5cclxuICAgICAgICB2YXIgYm91bmRMZW5ndGggPSBNYXRoLm1heCgwLCB0YXJnZXQubGVuZ3RoIC0gYXJncy5sZW5ndGgpO1xyXG5cclxuICAgICAgICAvLyAxNy4gU2V0IHRoZSBhdHRyaWJ1dGVzIG9mIHRoZSBsZW5ndGggb3duIHByb3BlcnR5IG9mIEYgdG8gdGhlIHZhbHVlc1xyXG4gICAgICAgIC8vICAgc3BlY2lmaWVkIGluIDE1LjMuNS4xLlxyXG4gICAgICAgIHZhciBib3VuZEFyZ3MgPSBbXTtcclxuICAgICAgICBmb3IgKHZhciBpID0gMDsgaSA8IGJvdW5kTGVuZ3RoOyBpKyspIHtcclxuICAgICAgICAgICAgYm91bmRBcmdzLnB1c2goJyQnICsgaSk7XHJcbiAgICAgICAgfVxyXG5cclxuICAgICAgICAvLyBYWFggQnVpbGQgYSBkeW5hbWljIGZ1bmN0aW9uIHdpdGggZGVzaXJlZCBhbW91bnQgb2YgYXJndW1lbnRzIGlzIHRoZSBvbmx5XHJcbiAgICAgICAgLy8gd2F5IHRvIHNldCB0aGUgbGVuZ3RoIHByb3BlcnR5IG9mIGEgZnVuY3Rpb24uXHJcbiAgICAgICAgLy8gSW4gZW52aXJvbm1lbnRzIHdoZXJlIENvbnRlbnQgU2VjdXJpdHkgUG9saWNpZXMgZW5hYmxlZCAoQ2hyb21lIGV4dGVuc2lvbnMsXHJcbiAgICAgICAgLy8gZm9yIGV4LikgYWxsIHVzZSBvZiBldmFsIG9yIEZ1bmN0aW9uIGNvc3RydWN0b3IgdGhyb3dzIGFuIGV4Y2VwdGlvbi5cclxuICAgICAgICAvLyBIb3dldmVyIGluIGFsbCBvZiB0aGVzZSBlbnZpcm9ubWVudHMgRnVuY3Rpb24ucHJvdG90eXBlLmJpbmQgZXhpc3RzXHJcbiAgICAgICAgLy8gYW5kIHNvIHRoaXMgY29kZSB3aWxsIG5ldmVyIGJlIGV4ZWN1dGVkLlxyXG4gICAgICAgIHZhciBib3VuZCA9IEZ1bmN0aW9uKCdiaW5kZXInLCAncmV0dXJuIGZ1bmN0aW9uICgnICsgYm91bmRBcmdzLmpvaW4oJywnKSArICcpeyByZXR1cm4gYmluZGVyLmFwcGx5KHRoaXMsIGFyZ3VtZW50cyk7IH0nKShiaW5kZXIpO1xyXG5cclxuICAgICAgICBpZiAodGFyZ2V0LnByb3RvdHlwZSkge1xyXG4gICAgICAgICAgICBFbXB0eS5wcm90b3R5cGUgPSB0YXJnZXQucHJvdG90eXBlO1xyXG4gICAgICAgICAgICBib3VuZC5wcm90b3R5cGUgPSBuZXcgRW1wdHkoKTtcclxuICAgICAgICAgICAgLy8gQ2xlYW4gdXAgZGFuZ2xpbmcgcmVmZXJlbmNlcy5cclxuICAgICAgICAgICAgRW1wdHkucHJvdG90eXBlID0gbnVsbDtcclxuICAgICAgICB9XHJcblxyXG4gICAgICAgIC8vIFRPRE9cclxuICAgICAgICAvLyAxOC4gU2V0IHRoZSBbW0V4dGVuc2libGVdXSBpbnRlcm5hbCBwcm9wZXJ0eSBvZiBGIHRvIHRydWUuXHJcblxyXG4gICAgICAgIC8vIFRPRE9cclxuICAgICAgICAvLyAxOS4gTGV0IHRocm93ZXIgYmUgdGhlIFtbVGhyb3dUeXBlRXJyb3JdXSBmdW5jdGlvbiBPYmplY3QgKDEzLjIuMykuXHJcbiAgICAgICAgLy8gMjAuIENhbGwgdGhlIFtbRGVmaW5lT3duUHJvcGVydHldXSBpbnRlcm5hbCBtZXRob2Qgb2YgRiB3aXRoXHJcbiAgICAgICAgLy8gICBhcmd1bWVudHMgXCJjYWxsZXJcIiwgUHJvcGVydHlEZXNjcmlwdG9yIHtbW0dldF1dOiB0aHJvd2VyLCBbW1NldF1dOlxyXG4gICAgICAgIC8vICAgdGhyb3dlciwgW1tFbnVtZXJhYmxlXV06IGZhbHNlLCBbW0NvbmZpZ3VyYWJsZV1dOiBmYWxzZX0sIGFuZFxyXG4gICAgICAgIC8vICAgZmFsc2UuXHJcbiAgICAgICAgLy8gMjEuIENhbGwgdGhlIFtbRGVmaW5lT3duUHJvcGVydHldXSBpbnRlcm5hbCBtZXRob2Qgb2YgRiB3aXRoXHJcbiAgICAgICAgLy8gICBhcmd1bWVudHMgXCJhcmd1bWVudHNcIiwgUHJvcGVydHlEZXNjcmlwdG9yIHtbW0dldF1dOiB0aHJvd2VyLFxyXG4gICAgICAgIC8vICAgW1tTZXRdXTogdGhyb3dlciwgW1tFbnVtZXJhYmxlXV06IGZhbHNlLCBbW0NvbmZpZ3VyYWJsZV1dOiBmYWxzZX0sXHJcbiAgICAgICAgLy8gICBhbmQgZmFsc2UuXHJcblxyXG4gICAgICAgIC8vIFRPRE9cclxuICAgICAgICAvLyBOT1RFIEZ1bmN0aW9uIG9iamVjdHMgY3JlYXRlZCB1c2luZyBGdW5jdGlvbi5wcm90b3R5cGUuYmluZCBkbyBub3RcclxuICAgICAgICAvLyBoYXZlIGEgcHJvdG90eXBlIHByb3BlcnR5IG9yIHRoZSBbW0NvZGVdXSwgW1tGb3JtYWxQYXJhbWV0ZXJzXV0sIGFuZFxyXG4gICAgICAgIC8vIFtbU2NvcGVdXSBpbnRlcm5hbCBwcm9wZXJ0aWVzLlxyXG4gICAgICAgIC8vIFhYWCBjYW4ndCBkZWxldGUgcHJvdG90eXBlIGluIHB1cmUtanMuXHJcblxyXG4gICAgICAgIC8vIDIyLiBSZXR1cm4gRi5cclxuICAgICAgICByZXR1cm4gYm91bmQ7XHJcbiAgICB9XHJcbn0pO1xyXG5cclxuLy9cclxuLy8gQXJyYXlcclxuLy8gPT09PT1cclxuLy9cclxuXHJcbi8vIEVTNSAxNS40LjMuMlxyXG4vLyBodHRwOi8vZXM1LmdpdGh1Yi5jb20vI3gxNS40LjMuMlxyXG4vLyBodHRwczovL2RldmVsb3Blci5tb3ppbGxhLm9yZy9lbi9KYXZhU2NyaXB0L1JlZmVyZW5jZS9HbG9iYWxfT2JqZWN0cy9BcnJheS9pc0FycmF5XHJcbmRlZmluZVByb3BlcnRpZXMoQXJyYXksIHsgaXNBcnJheTogaXNBcnJheSB9KTtcclxuXHJcblxyXG52YXIgYm94ZWRTdHJpbmcgPSBPYmplY3QoJ2EnKTtcclxudmFyIHNwbGl0U3RyaW5nID0gYm94ZWRTdHJpbmdbMF0gIT09ICdhJyB8fCAhKDAgaW4gYm94ZWRTdHJpbmcpO1xyXG5cclxudmFyIHByb3Blcmx5Qm94ZXNDb250ZXh0ID0gZnVuY3Rpb24gcHJvcGVybHlCb3hlZChtZXRob2QpIHtcclxuICAgIC8vIENoZWNrIG5vZGUgMC42LjIxIGJ1ZyB3aGVyZSB0aGlyZCBwYXJhbWV0ZXIgaXMgbm90IGJveGVkXHJcbiAgICB2YXIgcHJvcGVybHlCb3hlc05vblN0cmljdCA9IHRydWU7XHJcbiAgICB2YXIgcHJvcGVybHlCb3hlc1N0cmljdCA9IHRydWU7XHJcbiAgICBpZiAobWV0aG9kKSB7XHJcbiAgICAgICAgbWV0aG9kLmNhbGwoJ2ZvbycsIGZ1bmN0aW9uIChfLCBfXywgY29udGV4dCkge1xyXG4gICAgICAgICAgICBpZiAodHlwZW9mIGNvbnRleHQgIT09ICdvYmplY3QnKSB7IHByb3Blcmx5Qm94ZXNOb25TdHJpY3QgPSBmYWxzZTsgfVxyXG4gICAgICAgIH0pO1xyXG5cclxuICAgICAgICBtZXRob2QuY2FsbChbMV0sIGZ1bmN0aW9uICgpIHtcclxuICAgICAgICAgICAgJ3VzZSBzdHJpY3QnO1xyXG4gICAgICAgICAgICBwcm9wZXJseUJveGVzU3RyaWN0ID0gdHlwZW9mIHRoaXMgPT09ICdzdHJpbmcnO1xyXG4gICAgICAgIH0sICd4Jyk7XHJcbiAgICB9XHJcbiAgICByZXR1cm4gISFtZXRob2QgJiYgcHJvcGVybHlCb3hlc05vblN0cmljdCAmJiBwcm9wZXJseUJveGVzU3RyaWN0O1xyXG59O1xyXG5cclxuZGVmaW5lUHJvcGVydGllcyhBcnJheVByb3RvdHlwZSwge1xyXG4gICAgZm9yRWFjaDogZnVuY3Rpb24gZm9yRWFjaChmdW4gLyosIHRoaXNwKi8pIHtcclxuICAgICAgICB2YXIgb2JqZWN0ID0gdG9PYmplY3QodGhpcyksXHJcbiAgICAgICAgICAgIHNlbGYgPSBzcGxpdFN0cmluZyAmJiBpc1N0cmluZyh0aGlzKSA/IHRoaXMuc3BsaXQoJycpIDogb2JqZWN0LFxyXG4gICAgICAgICAgICB0aGlzcCA9IGFyZ3VtZW50c1sxXSxcclxuICAgICAgICAgICAgaSA9IC0xLFxyXG4gICAgICAgICAgICBsZW5ndGggPSBzZWxmLmxlbmd0aCA+Pj4gMDtcclxuXHJcbiAgICAgICAgLy8gSWYgbm8gY2FsbGJhY2sgZnVuY3Rpb24gb3IgaWYgY2FsbGJhY2sgaXMgbm90IGEgY2FsbGFibGUgZnVuY3Rpb25cclxuICAgICAgICBpZiAoIWlzRnVuY3Rpb24oZnVuKSkge1xyXG4gICAgICAgICAgICB0aHJvdyBuZXcgVHlwZUVycm9yKCk7IC8vIFRPRE8gbWVzc2FnZVxyXG4gICAgICAgIH1cclxuXHJcbiAgICAgICAgd2hpbGUgKCsraSA8IGxlbmd0aCkge1xyXG4gICAgICAgICAgICBpZiAoaSBpbiBzZWxmKSB7XHJcbiAgICAgICAgICAgICAgICAvLyBJbnZva2UgdGhlIGNhbGxiYWNrIGZ1bmN0aW9uIHdpdGggY2FsbCwgcGFzc2luZyBhcmd1bWVudHM6XHJcbiAgICAgICAgICAgICAgICAvLyBjb250ZXh0LCBwcm9wZXJ0eSB2YWx1ZSwgcHJvcGVydHkga2V5LCB0aGlzQXJnIG9iamVjdFxyXG4gICAgICAgICAgICAgICAgLy8gY29udGV4dFxyXG4gICAgICAgICAgICAgICAgZnVuLmNhbGwodGhpc3AsIHNlbGZbaV0sIGksIG9iamVjdCk7XHJcbiAgICAgICAgICAgIH1cclxuICAgICAgICB9XHJcbiAgICB9XHJcbn0sICFwcm9wZXJseUJveGVzQ29udGV4dChBcnJheVByb3RvdHlwZS5mb3JFYWNoKSk7XHJcblxyXG4vLyBFUzUgMTUuNC40LjE0XHJcbi8vIGh0dHA6Ly9lczUuZ2l0aHViLmNvbS8jeDE1LjQuNC4xNFxyXG4vLyBodHRwczovL2RldmVsb3Blci5tb3ppbGxhLm9yZy9lbi9KYXZhU2NyaXB0L1JlZmVyZW5jZS9HbG9iYWxfT2JqZWN0cy9BcnJheS9pbmRleE9mXHJcbnZhciBoYXNGaXJlZm94MkluZGV4T2ZCdWcgPSBBcnJheS5wcm90b3R5cGUuaW5kZXhPZiAmJiBbMCwgMV0uaW5kZXhPZigxLCAyKSAhPT0gLTE7XHJcbmRlZmluZVByb3BlcnRpZXMoQXJyYXlQcm90b3R5cGUsIHtcclxuICAgIGluZGV4T2Y6IGZ1bmN0aW9uIGluZGV4T2Yoc291Z2h0IC8qLCBmcm9tSW5kZXggKi8gKSB7XHJcbiAgICAgICAgdmFyIHNlbGYgPSBzcGxpdFN0cmluZyAmJiBpc1N0cmluZyh0aGlzKSA/IHRoaXMuc3BsaXQoJycpIDogdG9PYmplY3QodGhpcyksXHJcbiAgICAgICAgICAgIGxlbmd0aCA9IHNlbGYubGVuZ3RoID4+PiAwO1xyXG5cclxuICAgICAgICBpZiAoIWxlbmd0aCkge1xyXG4gICAgICAgICAgICByZXR1cm4gLTE7XHJcbiAgICAgICAgfVxyXG5cclxuICAgICAgICB2YXIgaSA9IDA7XHJcbiAgICAgICAgaWYgKGFyZ3VtZW50cy5sZW5ndGggPiAxKSB7XHJcbiAgICAgICAgICAgIGkgPSB0b0ludGVnZXIoYXJndW1lbnRzWzFdKTtcclxuICAgICAgICB9XHJcblxyXG4gICAgICAgIC8vIGhhbmRsZSBuZWdhdGl2ZSBpbmRpY2VzXHJcbiAgICAgICAgaSA9IGkgPj0gMCA/IGkgOiBNYXRoLm1heCgwLCBsZW5ndGggKyBpKTtcclxuICAgICAgICBmb3IgKDsgaSA8IGxlbmd0aDsgaSsrKSB7XHJcbiAgICAgICAgICAgIGlmIChpIGluIHNlbGYgJiYgc2VsZltpXSA9PT0gc291Z2h0KSB7XHJcbiAgICAgICAgICAgICAgICByZXR1cm4gaTtcclxuICAgICAgICAgICAgfVxyXG4gICAgICAgIH1cclxuICAgICAgICByZXR1cm4gLTE7XHJcbiAgICB9XHJcbn0sIGhhc0ZpcmVmb3gySW5kZXhPZkJ1Zyk7XHJcblxyXG4vL1xyXG4vLyBTdHJpbmdcclxuLy8gPT09PT09XHJcbi8vXHJcblxyXG4vLyBFUzUgMTUuNS40LjE0XHJcbi8vIGh0dHA6Ly9lczUuZ2l0aHViLmNvbS8jeDE1LjUuNC4xNFxyXG5cclxuLy8gW2J1Z2ZpeCwgSUUgbHQgOSwgZmlyZWZveCA0LCBLb25xdWVyb3IsIE9wZXJhLCBvYnNjdXJlIGJyb3dzZXJzXVxyXG4vLyBNYW55IGJyb3dzZXJzIGRvIG5vdCBzcGxpdCBwcm9wZXJseSB3aXRoIHJlZ3VsYXIgZXhwcmVzc2lvbnMgb3IgdGhleVxyXG4vLyBkbyBub3QgcGVyZm9ybSB0aGUgc3BsaXQgY29ycmVjdGx5IHVuZGVyIG9ic2N1cmUgY29uZGl0aW9ucy5cclxuLy8gU2VlIGh0dHA6Ly9ibG9nLnN0ZXZlbmxldml0aGFuLmNvbS9hcmNoaXZlcy9jcm9zcy1icm93c2VyLXNwbGl0XHJcbi8vIEkndmUgdGVzdGVkIGluIG1hbnkgYnJvd3NlcnMgYW5kIHRoaXMgc2VlbXMgdG8gY292ZXIgdGhlIGRldmlhbnQgb25lczpcclxuLy8gICAgJ2FiJy5zcGxpdCgvKD86YWIpKi8pIHNob3VsZCBiZSBbXCJcIiwgXCJcIl0sIG5vdCBbXCJcIl1cclxuLy8gICAgJy4nLnNwbGl0KC8oLj8pKC4/KS8pIHNob3VsZCBiZSBbXCJcIiwgXCIuXCIsIFwiXCIsIFwiXCJdLCBub3QgW1wiXCIsIFwiXCJdXHJcbi8vICAgICd0ZXNzdCcuc3BsaXQoLyhzKSovKSBzaG91bGQgYmUgW1widFwiLCB1bmRlZmluZWQsIFwiZVwiLCBcInNcIiwgXCJ0XCJdLCBub3RcclxuLy8gICAgICAgW3VuZGVmaW5lZCwgXCJ0XCIsIHVuZGVmaW5lZCwgXCJlXCIsIC4uLl1cclxuLy8gICAgJycuc3BsaXQoLy4/Lykgc2hvdWxkIGJlIFtdLCBub3QgW1wiXCJdXHJcbi8vICAgICcuJy5zcGxpdCgvKCkoKS8pIHNob3VsZCBiZSBbXCIuXCJdLCBub3QgW1wiXCIsIFwiXCIsIFwiLlwiXVxyXG5cclxudmFyIHN0cmluZ19zcGxpdCA9IFN0cmluZ1Byb3RvdHlwZS5zcGxpdDtcclxuaWYgKFxyXG4gICAgJ2FiJy5zcGxpdCgvKD86YWIpKi8pLmxlbmd0aCAhPT0gMiB8fFxyXG4gICAgJy4nLnNwbGl0KC8oLj8pKC4/KS8pLmxlbmd0aCAhPT0gNCB8fFxyXG4gICAgJ3Rlc3N0Jy5zcGxpdCgvKHMpKi8pWzFdID09PSAndCcgfHxcclxuICAgICd0ZXN0Jy5zcGxpdCgvKD86KS8sIC0xKS5sZW5ndGggIT09IDQgfHxcclxuICAgICcnLnNwbGl0KC8uPy8pLmxlbmd0aCB8fFxyXG4gICAgJy4nLnNwbGl0KC8oKSgpLykubGVuZ3RoID4gMVxyXG4pIHtcclxuICAgIChmdW5jdGlvbiAoKSB7XHJcbiAgICAgICAgdmFyIGNvbXBsaWFudEV4ZWNOcGNnID0gLygpPz8vLmV4ZWMoJycpWzFdID09PSB2b2lkIDA7IC8vIE5QQ0c6IG5vbnBhcnRpY2lwYXRpbmcgY2FwdHVyaW5nIGdyb3VwXHJcblxyXG4gICAgICAgIFN0cmluZ1Byb3RvdHlwZS5zcGxpdCA9IGZ1bmN0aW9uIChzZXBhcmF0b3IsIGxpbWl0KSB7XHJcbiAgICAgICAgICAgIHZhciBzdHJpbmcgPSB0aGlzO1xyXG4gICAgICAgICAgICBpZiAoc2VwYXJhdG9yID09PSB2b2lkIDAgJiYgbGltaXQgPT09IDApIHtcclxuICAgICAgICAgICAgICAgIHJldHVybiBbXTtcclxuICAgICAgICAgICAgfVxyXG5cclxuICAgICAgICAgICAgLy8gSWYgYHNlcGFyYXRvcmAgaXMgbm90IGEgcmVnZXgsIHVzZSBuYXRpdmUgc3BsaXRcclxuICAgICAgICAgICAgaWYgKF90b1N0cmluZy5jYWxsKHNlcGFyYXRvcikgIT09ICdbb2JqZWN0IFJlZ0V4cF0nKSB7XHJcbiAgICAgICAgICAgICAgICByZXR1cm4gc3RyaW5nX3NwbGl0LmNhbGwodGhpcywgc2VwYXJhdG9yLCBsaW1pdCk7XHJcbiAgICAgICAgICAgIH1cclxuXHJcbiAgICAgICAgICAgIHZhciBvdXRwdXQgPSBbXSxcclxuICAgICAgICAgICAgICAgIGZsYWdzID0gKHNlcGFyYXRvci5pZ25vcmVDYXNlID8gJ2knIDogJycpICtcclxuICAgICAgICAgICAgICAgICAgICAgICAgKHNlcGFyYXRvci5tdWx0aWxpbmUgID8gJ20nIDogJycpICtcclxuICAgICAgICAgICAgICAgICAgICAgICAgKHNlcGFyYXRvci5leHRlbmRlZCAgID8gJ3gnIDogJycpICsgLy8gUHJvcG9zZWQgZm9yIEVTNlxyXG4gICAgICAgICAgICAgICAgICAgICAgICAoc2VwYXJhdG9yLnN0aWNreSAgICAgPyAneScgOiAnJyksIC8vIEZpcmVmb3ggMytcclxuICAgICAgICAgICAgICAgIGxhc3RMYXN0SW5kZXggPSAwLFxyXG4gICAgICAgICAgICAgICAgLy8gTWFrZSBgZ2xvYmFsYCBhbmQgYXZvaWQgYGxhc3RJbmRleGAgaXNzdWVzIGJ5IHdvcmtpbmcgd2l0aCBhIGNvcHlcclxuICAgICAgICAgICAgICAgIHNlcGFyYXRvcjIsIG1hdGNoLCBsYXN0SW5kZXgsIGxhc3RMZW5ndGg7XHJcbiAgICAgICAgICAgIHNlcGFyYXRvciA9IG5ldyBSZWdFeHAoc2VwYXJhdG9yLnNvdXJjZSwgZmxhZ3MgKyAnZycpO1xyXG4gICAgICAgICAgICBzdHJpbmcgKz0gJyc7IC8vIFR5cGUtY29udmVydFxyXG4gICAgICAgICAgICBpZiAoIWNvbXBsaWFudEV4ZWNOcGNnKSB7XHJcbiAgICAgICAgICAgICAgICAvLyBEb2Vzbid0IG5lZWQgZmxhZ3MgZ3ksIGJ1dCB0aGV5IGRvbid0IGh1cnRcclxuICAgICAgICAgICAgICAgIHNlcGFyYXRvcjIgPSBuZXcgUmVnRXhwKCdeJyArIHNlcGFyYXRvci5zb3VyY2UgKyAnJCg/IVxcXFxzKScsIGZsYWdzKTtcclxuICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICAvKiBWYWx1ZXMgZm9yIGBsaW1pdGAsIHBlciB0aGUgc3BlYzpcclxuICAgICAgICAgICAgICogSWYgdW5kZWZpbmVkOiA0Mjk0OTY3Mjk1IC8vIE1hdGgucG93KDIsIDMyKSAtIDFcclxuICAgICAgICAgICAgICogSWYgMCwgSW5maW5pdHksIG9yIE5hTjogMFxyXG4gICAgICAgICAgICAgKiBJZiBwb3NpdGl2ZSBudW1iZXI6IGxpbWl0ID0gTWF0aC5mbG9vcihsaW1pdCk7IGlmIChsaW1pdCA+IDQyOTQ5NjcyOTUpIGxpbWl0IC09IDQyOTQ5NjcyOTY7XHJcbiAgICAgICAgICAgICAqIElmIG5lZ2F0aXZlIG51bWJlcjogNDI5NDk2NzI5NiAtIE1hdGguZmxvb3IoTWF0aC5hYnMobGltaXQpKVxyXG4gICAgICAgICAgICAgKiBJZiBvdGhlcjogVHlwZS1jb252ZXJ0LCB0aGVuIHVzZSB0aGUgYWJvdmUgcnVsZXNcclxuICAgICAgICAgICAgICovXHJcbiAgICAgICAgICAgIGxpbWl0ID0gbGltaXQgPT09IHZvaWQgMCA/XHJcbiAgICAgICAgICAgICAgICAtMSA+Pj4gMCA6IC8vIE1hdGgucG93KDIsIDMyKSAtIDFcclxuICAgICAgICAgICAgICAgIFRvVWludDMyKGxpbWl0KTtcclxuICAgICAgICAgICAgd2hpbGUgKG1hdGNoID0gc2VwYXJhdG9yLmV4ZWMoc3RyaW5nKSkge1xyXG4gICAgICAgICAgICAgICAgLy8gYHNlcGFyYXRvci5sYXN0SW5kZXhgIGlzIG5vdCByZWxpYWJsZSBjcm9zcy1icm93c2VyXHJcbiAgICAgICAgICAgICAgICBsYXN0SW5kZXggPSBtYXRjaC5pbmRleCArIG1hdGNoWzBdLmxlbmd0aDtcclxuICAgICAgICAgICAgICAgIGlmIChsYXN0SW5kZXggPiBsYXN0TGFzdEluZGV4KSB7XHJcbiAgICAgICAgICAgICAgICAgICAgb3V0cHV0LnB1c2goc3RyaW5nLnNsaWNlKGxhc3RMYXN0SW5kZXgsIG1hdGNoLmluZGV4KSk7XHJcbiAgICAgICAgICAgICAgICAgICAgLy8gRml4IGJyb3dzZXJzIHdob3NlIGBleGVjYCBtZXRob2RzIGRvbid0IGNvbnNpc3RlbnRseSByZXR1cm4gYHVuZGVmaW5lZGAgZm9yXHJcbiAgICAgICAgICAgICAgICAgICAgLy8gbm9ucGFydGljaXBhdGluZyBjYXB0dXJpbmcgZ3JvdXBzXHJcbiAgICAgICAgICAgICAgICAgICAgaWYgKCFjb21wbGlhbnRFeGVjTnBjZyAmJiBtYXRjaC5sZW5ndGggPiAxKSB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIG1hdGNoWzBdLnJlcGxhY2Uoc2VwYXJhdG9yMiwgZnVuY3Rpb24gKCkge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgZm9yICh2YXIgaSA9IDE7IGkgPCBhcmd1bWVudHMubGVuZ3RoIC0gMjsgaSsrKSB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgaWYgKGFyZ3VtZW50c1tpXSA9PT0gdm9pZCAwKSB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIG1hdGNoW2ldID0gdm9pZCAwO1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgICAgICAgICAgICAgfSk7XHJcbiAgICAgICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICAgICAgICAgIGlmIChtYXRjaC5sZW5ndGggPiAxICYmIG1hdGNoLmluZGV4IDwgc3RyaW5nLmxlbmd0aCkge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICBBcnJheVByb3RvdHlwZS5wdXNoLmFwcGx5KG91dHB1dCwgbWF0Y2guc2xpY2UoMSkpO1xyXG4gICAgICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgICAgICAgICBsYXN0TGVuZ3RoID0gbWF0Y2hbMF0ubGVuZ3RoO1xyXG4gICAgICAgICAgICAgICAgICAgIGxhc3RMYXN0SW5kZXggPSBsYXN0SW5kZXg7XHJcbiAgICAgICAgICAgICAgICAgICAgaWYgKG91dHB1dC5sZW5ndGggPj0gbGltaXQpIHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgYnJlYWs7XHJcbiAgICAgICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICAgICAgaWYgKHNlcGFyYXRvci5sYXN0SW5kZXggPT09IG1hdGNoLmluZGV4KSB7XHJcbiAgICAgICAgICAgICAgICAgICAgc2VwYXJhdG9yLmxhc3RJbmRleCsrOyAvLyBBdm9pZCBhbiBpbmZpbml0ZSBsb29wXHJcbiAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgaWYgKGxhc3RMYXN0SW5kZXggPT09IHN0cmluZy5sZW5ndGgpIHtcclxuICAgICAgICAgICAgICAgIGlmIChsYXN0TGVuZ3RoIHx8ICFzZXBhcmF0b3IudGVzdCgnJykpIHtcclxuICAgICAgICAgICAgICAgICAgICBvdXRwdXQucHVzaCgnJyk7XHJcbiAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgIH0gZWxzZSB7XHJcbiAgICAgICAgICAgICAgICBvdXRwdXQucHVzaChzdHJpbmcuc2xpY2UobGFzdExhc3RJbmRleCkpO1xyXG4gICAgICAgICAgICB9XHJcbiAgICAgICAgICAgIHJldHVybiBvdXRwdXQubGVuZ3RoID4gbGltaXQgPyBvdXRwdXQuc2xpY2UoMCwgbGltaXQpIDogb3V0cHV0O1xyXG4gICAgICAgIH07XHJcbiAgICB9KCkpO1xyXG5cclxuLy8gW2J1Z2ZpeCwgY2hyb21lXVxyXG4vLyBJZiBzZXBhcmF0b3IgaXMgdW5kZWZpbmVkLCB0aGVuIHRoZSByZXN1bHQgYXJyYXkgY29udGFpbnMganVzdCBvbmUgU3RyaW5nLFxyXG4vLyB3aGljaCBpcyB0aGUgdGhpcyB2YWx1ZSAoY29udmVydGVkIHRvIGEgU3RyaW5nKS4gSWYgbGltaXQgaXMgbm90IHVuZGVmaW5lZCxcclxuLy8gdGhlbiB0aGUgb3V0cHV0IGFycmF5IGlzIHRydW5jYXRlZCBzbyB0aGF0IGl0IGNvbnRhaW5zIG5vIG1vcmUgdGhhbiBsaW1pdFxyXG4vLyBlbGVtZW50cy5cclxuLy8gXCIwXCIuc3BsaXQodW5kZWZpbmVkLCAwKSAtPiBbXVxyXG59IGVsc2UgaWYgKCcwJy5zcGxpdCh2b2lkIDAsIDApLmxlbmd0aCkge1xyXG4gICAgU3RyaW5nUHJvdG90eXBlLnNwbGl0ID0gZnVuY3Rpb24gc3BsaXQoc2VwYXJhdG9yLCBsaW1pdCkge1xyXG4gICAgICAgIGlmIChzZXBhcmF0b3IgPT09IHZvaWQgMCAmJiBsaW1pdCA9PT0gMCkgeyByZXR1cm4gW107IH1cclxuICAgICAgICByZXR1cm4gc3RyaW5nX3NwbGl0LmNhbGwodGhpcywgc2VwYXJhdG9yLCBsaW1pdCk7XHJcbiAgICB9O1xyXG59XHJcblxyXG4vLyBFQ01BLTI2MiwgM3JkIEIuMi4zXHJcbi8vIE5vdCBhbiBFQ01BU2NyaXB0IHN0YW5kYXJkLCBhbHRob3VnaCBFQ01BU2NyaXB0IDNyZCBFZGl0aW9uIGhhcyBhXHJcbi8vIG5vbi1ub3JtYXRpdmUgc2VjdGlvbiBzdWdnZXN0aW5nIHVuaWZvcm0gc2VtYW50aWNzIGFuZCBpdCBzaG91bGQgYmVcclxuLy8gbm9ybWFsaXplZCBhY3Jvc3MgYWxsIGJyb3dzZXJzXHJcbi8vIFtidWdmaXgsIElFIGx0IDldIElFIDwgOSBzdWJzdHIoKSB3aXRoIG5lZ2F0aXZlIHZhbHVlIG5vdCB3b3JraW5nIGluIElFXHJcbnZhciBzdHJpbmdfc3Vic3RyID0gU3RyaW5nUHJvdG90eXBlLnN1YnN0cjtcclxudmFyIGhhc05lZ2F0aXZlU3Vic3RyQnVnID0gJycuc3Vic3RyICYmICcwYicuc3Vic3RyKC0xKSAhPT0gJ2InO1xyXG5kZWZpbmVQcm9wZXJ0aWVzKFN0cmluZ1Byb3RvdHlwZSwge1xyXG4gICAgc3Vic3RyOiBmdW5jdGlvbiBzdWJzdHIoc3RhcnQsIGxlbmd0aCkge1xyXG4gICAgICAgIHJldHVybiBzdHJpbmdfc3Vic3RyLmNhbGwoXHJcbiAgICAgICAgICAgIHRoaXMsXHJcbiAgICAgICAgICAgIHN0YXJ0IDwgMCA/ICgoc3RhcnQgPSB0aGlzLmxlbmd0aCArIHN0YXJ0KSA8IDAgPyAwIDogc3RhcnQpIDogc3RhcnQsXHJcbiAgICAgICAgICAgIGxlbmd0aFxyXG4gICAgICAgICk7XHJcbiAgICB9XHJcbn0sIGhhc05lZ2F0aXZlU3Vic3RyQnVnKTtcclxuXHJcbn0se31dLDE2OltmdW5jdGlvbihyZXF1aXJlLG1vZHVsZSxleHBvcnRzKXtcclxuJ3VzZSBzdHJpY3QnO1xyXG5cclxubW9kdWxlLmV4cG9ydHMgPSBbXHJcbiAgLy8gc3RyZWFtaW5nIHRyYW5zcG9ydHNcclxuICByZXF1aXJlKCcuL3RyYW5zcG9ydC93ZWJzb2NrZXQnKVxyXG4sIHJlcXVpcmUoJy4vdHJhbnNwb3J0L3hoci1zdHJlYW1pbmcnKVxyXG4sIHJlcXVpcmUoJy4vdHJhbnNwb3J0L3hkci1zdHJlYW1pbmcnKVxyXG4sIHJlcXVpcmUoJy4vdHJhbnNwb3J0L2V2ZW50c291cmNlJylcclxuLCByZXF1aXJlKCcuL3RyYW5zcG9ydC9saWIvaWZyYW1lLXdyYXAnKShyZXF1aXJlKCcuL3RyYW5zcG9ydC9ldmVudHNvdXJjZScpKVxyXG5cclxuICAvLyBwb2xsaW5nIHRyYW5zcG9ydHNcclxuLCByZXF1aXJlKCcuL3RyYW5zcG9ydC9odG1sZmlsZScpXHJcbiwgcmVxdWlyZSgnLi90cmFuc3BvcnQvbGliL2lmcmFtZS13cmFwJykocmVxdWlyZSgnLi90cmFuc3BvcnQvaHRtbGZpbGUnKSlcclxuLCByZXF1aXJlKCcuL3RyYW5zcG9ydC94aHItcG9sbGluZycpXHJcbiwgcmVxdWlyZSgnLi90cmFuc3BvcnQveGRyLXBvbGxpbmcnKVxyXG4sIHJlcXVpcmUoJy4vdHJhbnNwb3J0L2xpYi9pZnJhbWUtd3JhcCcpKHJlcXVpcmUoJy4vdHJhbnNwb3J0L3hoci1wb2xsaW5nJykpXHJcbiwgcmVxdWlyZSgnLi90cmFuc3BvcnQvanNvbnAtcG9sbGluZycpXHJcbl07XHJcblxyXG59LHtcIi4vdHJhbnNwb3J0L2V2ZW50c291cmNlXCI6MjAsXCIuL3RyYW5zcG9ydC9odG1sZmlsZVwiOjIxLFwiLi90cmFuc3BvcnQvanNvbnAtcG9sbGluZ1wiOjIzLFwiLi90cmFuc3BvcnQvbGliL2lmcmFtZS13cmFwXCI6MjYsXCIuL3RyYW5zcG9ydC93ZWJzb2NrZXRcIjozOCxcIi4vdHJhbnNwb3J0L3hkci1wb2xsaW5nXCI6MzksXCIuL3RyYW5zcG9ydC94ZHItc3RyZWFtaW5nXCI6NDAsXCIuL3RyYW5zcG9ydC94aHItcG9sbGluZ1wiOjQxLFwiLi90cmFuc3BvcnQveGhyLXN0cmVhbWluZ1wiOjQyfV0sMTc6W2Z1bmN0aW9uKHJlcXVpcmUsbW9kdWxlLGV4cG9ydHMpe1xyXG4oZnVuY3Rpb24gKHByb2Nlc3MsZ2xvYmFsKXtcclxuJ3VzZSBzdHJpY3QnO1xyXG5cclxudmFyIEV2ZW50RW1pdHRlciA9IHJlcXVpcmUoJ2V2ZW50cycpLkV2ZW50RW1pdHRlclxyXG4gICwgaW5oZXJpdHMgPSByZXF1aXJlKCdpbmhlcml0cycpXHJcbiAgLCB1dGlscyA9IHJlcXVpcmUoJy4uLy4uL3V0aWxzL2V2ZW50JylcclxuICAsIHVybFV0aWxzID0gcmVxdWlyZSgnLi4vLi4vdXRpbHMvdXJsJylcclxuICAsIFhIUiA9IGdsb2JhbC5YTUxIdHRwUmVxdWVzdFxyXG4gIDtcclxuXHJcbnZhciBkZWJ1ZyA9IGZ1bmN0aW9uKCkge307XHJcbmlmIChwcm9jZXNzLmVudi5OT0RFX0VOViAhPT0gJ3Byb2R1Y3Rpb24nKSB7XHJcbiAgZGVidWcgPSByZXF1aXJlKCdkZWJ1ZycpKCdzb2NranMtY2xpZW50OmJyb3dzZXI6eGhyJyk7XHJcbn1cclxuXHJcbmZ1bmN0aW9uIEFic3RyYWN0WEhST2JqZWN0KG1ldGhvZCwgdXJsLCBwYXlsb2FkLCBvcHRzKSB7XHJcbiAgZGVidWcobWV0aG9kLCB1cmwpO1xyXG4gIHZhciBzZWxmID0gdGhpcztcclxuICBFdmVudEVtaXR0ZXIuY2FsbCh0aGlzKTtcclxuXHJcbiAgc2V0VGltZW91dChmdW5jdGlvbiAoKSB7XHJcbiAgICBzZWxmLl9zdGFydChtZXRob2QsIHVybCwgcGF5bG9hZCwgb3B0cyk7XHJcbiAgfSwgMCk7XHJcbn1cclxuXHJcbmluaGVyaXRzKEFic3RyYWN0WEhST2JqZWN0LCBFdmVudEVtaXR0ZXIpO1xyXG5cclxuQWJzdHJhY3RYSFJPYmplY3QucHJvdG90eXBlLl9zdGFydCA9IGZ1bmN0aW9uKG1ldGhvZCwgdXJsLCBwYXlsb2FkLCBvcHRzKSB7XHJcbiAgdmFyIHNlbGYgPSB0aGlzO1xyXG5cclxuICB0cnkge1xyXG4gICAgdGhpcy54aHIgPSBuZXcgWEhSKCk7XHJcbiAgfSBjYXRjaCAoeCkge1xyXG4gICAgLy8gaW50ZW50aW9uYWxseSBlbXB0eVxyXG4gIH1cclxuXHJcbiAgaWYgKCF0aGlzLnhocikge1xyXG4gICAgZGVidWcoJ25vIHhocicpO1xyXG4gICAgdGhpcy5lbWl0KCdmaW5pc2gnLCAwLCAnbm8geGhyIHN1cHBvcnQnKTtcclxuICAgIHRoaXMuX2NsZWFudXAoKTtcclxuICAgIHJldHVybjtcclxuICB9XHJcblxyXG4gIC8vIHNldmVyYWwgYnJvd3NlcnMgY2FjaGUgUE9TVHNcclxuICB1cmwgPSB1cmxVdGlscy5hZGRRdWVyeSh1cmwsICd0PScgKyAoK25ldyBEYXRlKCkpKTtcclxuXHJcbiAgLy8gRXhwbG9yZXIgdGVuZHMgdG8ga2VlcCBjb25uZWN0aW9uIG9wZW4sIGV2ZW4gYWZ0ZXIgdGhlXHJcbiAgLy8gdGFiIGdldHMgY2xvc2VkOiBodHRwOi8vYnVncy5qcXVlcnkuY29tL3RpY2tldC81MjgwXHJcbiAgdGhpcy51bmxvYWRSZWYgPSB1dGlscy51bmxvYWRBZGQoZnVuY3Rpb24oKSB7XHJcbiAgICBkZWJ1ZygndW5sb2FkIGNsZWFudXAnKTtcclxuICAgIHNlbGYuX2NsZWFudXAodHJ1ZSk7XHJcbiAgfSk7XHJcbiAgdHJ5IHtcclxuICAgIHRoaXMueGhyLm9wZW4obWV0aG9kLCB1cmwsIHRydWUpO1xyXG4gICAgaWYgKHRoaXMudGltZW91dCAmJiAndGltZW91dCcgaW4gdGhpcy54aHIpIHtcclxuICAgICAgdGhpcy54aHIudGltZW91dCA9IHRoaXMudGltZW91dDtcclxuICAgICAgdGhpcy54aHIub250aW1lb3V0ID0gZnVuY3Rpb24oKSB7XHJcbiAgICAgICAgZGVidWcoJ3hociB0aW1lb3V0Jyk7XHJcbiAgICAgICAgc2VsZi5lbWl0KCdmaW5pc2gnLCAwLCAnJyk7XHJcbiAgICAgICAgc2VsZi5fY2xlYW51cChmYWxzZSk7XHJcbiAgICAgIH07XHJcbiAgICB9XHJcbiAgfSBjYXRjaCAoZSkge1xyXG4gICAgZGVidWcoJ2V4Y2VwdGlvbicsIGUpO1xyXG4gICAgLy8gSUUgcmFpc2VzIGFuIGV4Y2VwdGlvbiBvbiB3cm9uZyBwb3J0LlxyXG4gICAgdGhpcy5lbWl0KCdmaW5pc2gnLCAwLCAnJyk7XHJcbiAgICB0aGlzLl9jbGVhbnVwKGZhbHNlKTtcclxuICAgIHJldHVybjtcclxuICB9XHJcblxyXG4gIGlmICgoIW9wdHMgfHwgIW9wdHMubm9DcmVkZW50aWFscykgJiYgQWJzdHJhY3RYSFJPYmplY3Quc3VwcG9ydHNDT1JTKSB7XHJcbiAgICBkZWJ1Zygnd2l0aENyZWRlbnRpYWxzJyk7XHJcbiAgICAvLyBNb3ppbGxhIGRvY3Mgc2F5cyBodHRwczovL2RldmVsb3Blci5tb3ppbGxhLm9yZy9lbi9YTUxIdHRwUmVxdWVzdCA6XHJcbiAgICAvLyBcIlRoaXMgbmV2ZXIgYWZmZWN0cyBzYW1lLXNpdGUgcmVxdWVzdHMuXCJcclxuXHJcbiAgICB0aGlzLnhoci53aXRoQ3JlZGVudGlhbHMgPSAndHJ1ZSc7XHJcbiAgfVxyXG4gIGlmIChvcHRzICYmIG9wdHMuaGVhZGVycykge1xyXG4gICAgZm9yICh2YXIga2V5IGluIG9wdHMuaGVhZGVycykge1xyXG4gICAgICB0aGlzLnhoci5zZXRSZXF1ZXN0SGVhZGVyKGtleSwgb3B0cy5oZWFkZXJzW2tleV0pO1xyXG4gICAgfVxyXG4gIH1cclxuXHJcbiAgdGhpcy54aHIub25yZWFkeXN0YXRlY2hhbmdlID0gZnVuY3Rpb24oKSB7XHJcbiAgICBpZiAoc2VsZi54aHIpIHtcclxuICAgICAgdmFyIHggPSBzZWxmLnhocjtcclxuICAgICAgdmFyIHRleHQsIHN0YXR1cztcclxuICAgICAgZGVidWcoJ3JlYWR5U3RhdGUnLCB4LnJlYWR5U3RhdGUpO1xyXG4gICAgICBzd2l0Y2ggKHgucmVhZHlTdGF0ZSkge1xyXG4gICAgICBjYXNlIDM6XHJcbiAgICAgICAgLy8gSUUgZG9lc24ndCBsaWtlIHBlZWtpbmcgaW50byByZXNwb25zZVRleHQgb3Igc3RhdHVzXHJcbiAgICAgICAgLy8gb24gTWljcm9zb2Z0LlhNTEhUVFAgYW5kIHJlYWR5c3RhdGU9M1xyXG4gICAgICAgIHRyeSB7XHJcbiAgICAgICAgICBzdGF0dXMgPSB4LnN0YXR1cztcclxuICAgICAgICAgIHRleHQgPSB4LnJlc3BvbnNlVGV4dDtcclxuICAgICAgICB9IGNhdGNoIChlKSB7XHJcbiAgICAgICAgICAvLyBpbnRlbnRpb25hbGx5IGVtcHR5XHJcbiAgICAgICAgfVxyXG4gICAgICAgIGRlYnVnKCdzdGF0dXMnLCBzdGF0dXMpO1xyXG4gICAgICAgIC8vIElFIHJldHVybnMgMTIyMyBmb3IgMjA0OiBodHRwOi8vYnVncy5qcXVlcnkuY29tL3RpY2tldC8xNDUwXHJcbiAgICAgICAgaWYgKHN0YXR1cyA9PT0gMTIyMykge1xyXG4gICAgICAgICAgc3RhdHVzID0gMjA0O1xyXG4gICAgICAgIH1cclxuXHJcbiAgICAgICAgLy8gSUUgZG9lcyByZXR1cm4gcmVhZHlzdGF0ZSA9PSAzIGZvciA0MDQgYW5zd2Vycy5cclxuICAgICAgICBpZiAoc3RhdHVzID09PSAyMDAgJiYgdGV4dCAmJiB0ZXh0Lmxlbmd0aCA+IDApIHtcclxuICAgICAgICAgIGRlYnVnKCdjaHVuaycpO1xyXG4gICAgICAgICAgc2VsZi5lbWl0KCdjaHVuaycsIHN0YXR1cywgdGV4dCk7XHJcbiAgICAgICAgfVxyXG4gICAgICAgIGJyZWFrO1xyXG4gICAgICBjYXNlIDQ6XHJcbiAgICAgICAgc3RhdHVzID0geC5zdGF0dXM7XHJcbiAgICAgICAgZGVidWcoJ3N0YXR1cycsIHN0YXR1cyk7XHJcbiAgICAgICAgLy8gSUUgcmV0dXJucyAxMjIzIGZvciAyMDQ6IGh0dHA6Ly9idWdzLmpxdWVyeS5jb20vdGlja2V0LzE0NTBcclxuICAgICAgICBpZiAoc3RhdHVzID09PSAxMjIzKSB7XHJcbiAgICAgICAgICBzdGF0dXMgPSAyMDQ7XHJcbiAgICAgICAgfVxyXG4gICAgICAgIC8vIElFIHJldHVybnMgdGhpcyBmb3IgYSBiYWQgcG9ydFxyXG4gICAgICAgIC8vIGh0dHA6Ly9tc2RuLm1pY3Jvc29mdC5jb20vZW4tdXMvbGlicmFyeS93aW5kb3dzL2Rlc2t0b3AvYWEzODM3NzAodj12cy44NSkuYXNweFxyXG4gICAgICAgIGlmIChzdGF0dXMgPT09IDEyMDA1IHx8IHN0YXR1cyA9PT0gMTIwMjkpIHtcclxuICAgICAgICAgIHN0YXR1cyA9IDA7XHJcbiAgICAgICAgfVxyXG5cclxuICAgICAgICBkZWJ1ZygnZmluaXNoJywgc3RhdHVzLCB4LnJlc3BvbnNlVGV4dCk7XHJcbiAgICAgICAgc2VsZi5lbWl0KCdmaW5pc2gnLCBzdGF0dXMsIHgucmVzcG9uc2VUZXh0KTtcclxuICAgICAgICBzZWxmLl9jbGVhbnVwKGZhbHNlKTtcclxuICAgICAgICBicmVhaztcclxuICAgICAgfVxyXG4gICAgfVxyXG4gIH07XHJcblxyXG4gIHRyeSB7XHJcbiAgICBzZWxmLnhoci5zZW5kKHBheWxvYWQpO1xyXG4gIH0gY2F0Y2ggKGUpIHtcclxuICAgIHNlbGYuZW1pdCgnZmluaXNoJywgMCwgJycpO1xyXG4gICAgc2VsZi5fY2xlYW51cChmYWxzZSk7XHJcbiAgfVxyXG59O1xyXG5cclxuQWJzdHJhY3RYSFJPYmplY3QucHJvdG90eXBlLl9jbGVhbnVwID0gZnVuY3Rpb24oYWJvcnQpIHtcclxuICBkZWJ1ZygnY2xlYW51cCcpO1xyXG4gIGlmICghdGhpcy54aHIpIHtcclxuICAgIHJldHVybjtcclxuICB9XHJcbiAgdGhpcy5yZW1vdmVBbGxMaXN0ZW5lcnMoKTtcclxuICB1dGlscy51bmxvYWREZWwodGhpcy51bmxvYWRSZWYpO1xyXG5cclxuICAvLyBJRSBuZWVkcyB0aGlzIGZpZWxkIHRvIGJlIGEgZnVuY3Rpb25cclxuICB0aGlzLnhoci5vbnJlYWR5c3RhdGVjaGFuZ2UgPSBmdW5jdGlvbigpIHt9O1xyXG4gIGlmICh0aGlzLnhoci5vbnRpbWVvdXQpIHtcclxuICAgIHRoaXMueGhyLm9udGltZW91dCA9IG51bGw7XHJcbiAgfVxyXG5cclxuICBpZiAoYWJvcnQpIHtcclxuICAgIHRyeSB7XHJcbiAgICAgIHRoaXMueGhyLmFib3J0KCk7XHJcbiAgICB9IGNhdGNoICh4KSB7XHJcbiAgICAgIC8vIGludGVudGlvbmFsbHkgZW1wdHlcclxuICAgIH1cclxuICB9XHJcbiAgdGhpcy51bmxvYWRSZWYgPSB0aGlzLnhociA9IG51bGw7XHJcbn07XHJcblxyXG5BYnN0cmFjdFhIUk9iamVjdC5wcm90b3R5cGUuY2xvc2UgPSBmdW5jdGlvbigpIHtcclxuICBkZWJ1ZygnY2xvc2UnKTtcclxuICB0aGlzLl9jbGVhbnVwKHRydWUpO1xyXG59O1xyXG5cclxuQWJzdHJhY3RYSFJPYmplY3QuZW5hYmxlZCA9ICEhWEhSO1xyXG4vLyBvdmVycmlkZSBYTUxIdHRwUmVxdWVzdCBmb3IgSUU2LzdcclxuLy8gb2JmdXNjYXRlIHRvIGF2b2lkIGZpcmV3YWxsc1xyXG52YXIgYXhvID0gWydBY3RpdmUnXS5jb25jYXQoJ09iamVjdCcpLmpvaW4oJ1gnKTtcclxuaWYgKCFBYnN0cmFjdFhIUk9iamVjdC5lbmFibGVkICYmIChheG8gaW4gZ2xvYmFsKSkge1xyXG4gIGRlYnVnKCdvdmVycmlkaW5nIHhtbGh0dHByZXF1ZXN0Jyk7XHJcbiAgWEhSID0gZnVuY3Rpb24oKSB7XHJcbiAgICB0cnkge1xyXG4gICAgICByZXR1cm4gbmV3IGdsb2JhbFtheG9dKCdNaWNyb3NvZnQuWE1MSFRUUCcpO1xyXG4gICAgfSBjYXRjaCAoZSkge1xyXG4gICAgICByZXR1cm4gbnVsbDtcclxuICAgIH1cclxuICB9O1xyXG4gIEFic3RyYWN0WEhST2JqZWN0LmVuYWJsZWQgPSAhIW5ldyBYSFIoKTtcclxufVxyXG5cclxudmFyIGNvcnMgPSBmYWxzZTtcclxudHJ5IHtcclxuICBjb3JzID0gJ3dpdGhDcmVkZW50aWFscycgaW4gbmV3IFhIUigpO1xyXG59IGNhdGNoIChpZ25vcmVkKSB7XHJcbiAgLy8gaW50ZW50aW9uYWxseSBlbXB0eVxyXG59XHJcblxyXG5BYnN0cmFjdFhIUk9iamVjdC5zdXBwb3J0c0NPUlMgPSBjb3JzO1xyXG5cclxubW9kdWxlLmV4cG9ydHMgPSBBYnN0cmFjdFhIUk9iamVjdDtcclxuXHJcbn0pLmNhbGwodGhpcyx7IGVudjoge30gfSx0eXBlb2YgZ2xvYmFsICE9PSBcInVuZGVmaW5lZFwiID8gZ2xvYmFsIDogdHlwZW9mIHNlbGYgIT09IFwidW5kZWZpbmVkXCIgPyBzZWxmIDogdHlwZW9mIHdpbmRvdyAhPT0gXCJ1bmRlZmluZWRcIiA/IHdpbmRvdyA6IHt9KVxyXG5cclxufSx7XCIuLi8uLi91dGlscy9ldmVudFwiOjQ2LFwiLi4vLi4vdXRpbHMvdXJsXCI6NTIsXCJkZWJ1Z1wiOjU1LFwiZXZlbnRzXCI6MyxcImluaGVyaXRzXCI6NTd9XSwxODpbZnVuY3Rpb24ocmVxdWlyZSxtb2R1bGUsZXhwb3J0cyl7XHJcbihmdW5jdGlvbiAoZ2xvYmFsKXtcclxubW9kdWxlLmV4cG9ydHMgPSBnbG9iYWwuRXZlbnRTb3VyY2U7XHJcblxyXG59KS5jYWxsKHRoaXMsdHlwZW9mIGdsb2JhbCAhPT0gXCJ1bmRlZmluZWRcIiA/IGdsb2JhbCA6IHR5cGVvZiBzZWxmICE9PSBcInVuZGVmaW5lZFwiID8gc2VsZiA6IHR5cGVvZiB3aW5kb3cgIT09IFwidW5kZWZpbmVkXCIgPyB3aW5kb3cgOiB7fSlcclxuXHJcbn0se31dLDE5OltmdW5jdGlvbihyZXF1aXJlLG1vZHVsZSxleHBvcnRzKXtcclxuKGZ1bmN0aW9uIChnbG9iYWwpe1xyXG4ndXNlIHN0cmljdCc7XHJcblxyXG52YXIgRHJpdmVyID0gZ2xvYmFsLldlYlNvY2tldCB8fCBnbG9iYWwuTW96V2ViU29ja2V0O1xyXG5pZiAoRHJpdmVyKSB7XHJcblx0bW9kdWxlLmV4cG9ydHMgPSBmdW5jdGlvbiBXZWJTb2NrZXRCcm93c2VyRHJpdmVyKHVybCkge1xyXG5cdFx0cmV0dXJuIG5ldyBEcml2ZXIodXJsKTtcclxuXHR9O1xyXG59IGVsc2Uge1xyXG5cdG1vZHVsZS5leHBvcnRzID0gdW5kZWZpbmVkO1xyXG59XHJcblxyXG59KS5jYWxsKHRoaXMsdHlwZW9mIGdsb2JhbCAhPT0gXCJ1bmRlZmluZWRcIiA/IGdsb2JhbCA6IHR5cGVvZiBzZWxmICE9PSBcInVuZGVmaW5lZFwiID8gc2VsZiA6IHR5cGVvZiB3aW5kb3cgIT09IFwidW5kZWZpbmVkXCIgPyB3aW5kb3cgOiB7fSlcclxuXHJcbn0se31dLDIwOltmdW5jdGlvbihyZXF1aXJlLG1vZHVsZSxleHBvcnRzKXtcclxuJ3VzZSBzdHJpY3QnO1xyXG5cclxudmFyIGluaGVyaXRzID0gcmVxdWlyZSgnaW5oZXJpdHMnKVxyXG4gICwgQWpheEJhc2VkVHJhbnNwb3J0ID0gcmVxdWlyZSgnLi9saWIvYWpheC1iYXNlZCcpXHJcbiAgLCBFdmVudFNvdXJjZVJlY2VpdmVyID0gcmVxdWlyZSgnLi9yZWNlaXZlci9ldmVudHNvdXJjZScpXHJcbiAgLCBYSFJDb3JzT2JqZWN0ID0gcmVxdWlyZSgnLi9zZW5kZXIveGhyLWNvcnMnKVxyXG4gICwgRXZlbnRTb3VyY2VEcml2ZXIgPSByZXF1aXJlKCdldmVudHNvdXJjZScpXHJcbiAgO1xyXG5cclxuZnVuY3Rpb24gRXZlbnRTb3VyY2VUcmFuc3BvcnQodHJhbnNVcmwpIHtcclxuICBpZiAoIUV2ZW50U291cmNlVHJhbnNwb3J0LmVuYWJsZWQoKSkge1xyXG4gICAgdGhyb3cgbmV3IEVycm9yKCdUcmFuc3BvcnQgY3JlYXRlZCB3aGVuIGRpc2FibGVkJyk7XHJcbiAgfVxyXG5cclxuICBBamF4QmFzZWRUcmFuc3BvcnQuY2FsbCh0aGlzLCB0cmFuc1VybCwgJy9ldmVudHNvdXJjZScsIEV2ZW50U291cmNlUmVjZWl2ZXIsIFhIUkNvcnNPYmplY3QpO1xyXG59XHJcblxyXG5pbmhlcml0cyhFdmVudFNvdXJjZVRyYW5zcG9ydCwgQWpheEJhc2VkVHJhbnNwb3J0KTtcclxuXHJcbkV2ZW50U291cmNlVHJhbnNwb3J0LmVuYWJsZWQgPSBmdW5jdGlvbigpIHtcclxuICByZXR1cm4gISFFdmVudFNvdXJjZURyaXZlcjtcclxufTtcclxuXHJcbkV2ZW50U291cmNlVHJhbnNwb3J0LnRyYW5zcG9ydE5hbWUgPSAnZXZlbnRzb3VyY2UnO1xyXG5FdmVudFNvdXJjZVRyYW5zcG9ydC5yb3VuZFRyaXBzID0gMjtcclxuXHJcbm1vZHVsZS5leHBvcnRzID0gRXZlbnRTb3VyY2VUcmFuc3BvcnQ7XHJcblxyXG59LHtcIi4vbGliL2FqYXgtYmFzZWRcIjoyNCxcIi4vcmVjZWl2ZXIvZXZlbnRzb3VyY2VcIjoyOSxcIi4vc2VuZGVyL3hoci1jb3JzXCI6MzUsXCJldmVudHNvdXJjZVwiOjE4LFwiaW5oZXJpdHNcIjo1N31dLDIxOltmdW5jdGlvbihyZXF1aXJlLG1vZHVsZSxleHBvcnRzKXtcclxuJ3VzZSBzdHJpY3QnO1xyXG5cclxudmFyIGluaGVyaXRzID0gcmVxdWlyZSgnaW5oZXJpdHMnKVxyXG4gICwgSHRtbGZpbGVSZWNlaXZlciA9IHJlcXVpcmUoJy4vcmVjZWl2ZXIvaHRtbGZpbGUnKVxyXG4gICwgWEhSTG9jYWxPYmplY3QgPSByZXF1aXJlKCcuL3NlbmRlci94aHItbG9jYWwnKVxyXG4gICwgQWpheEJhc2VkVHJhbnNwb3J0ID0gcmVxdWlyZSgnLi9saWIvYWpheC1iYXNlZCcpXHJcbiAgO1xyXG5cclxuZnVuY3Rpb24gSHRtbEZpbGVUcmFuc3BvcnQodHJhbnNVcmwpIHtcclxuICBpZiAoIUh0bWxmaWxlUmVjZWl2ZXIuZW5hYmxlZCkge1xyXG4gICAgdGhyb3cgbmV3IEVycm9yKCdUcmFuc3BvcnQgY3JlYXRlZCB3aGVuIGRpc2FibGVkJyk7XHJcbiAgfVxyXG4gIEFqYXhCYXNlZFRyYW5zcG9ydC5jYWxsKHRoaXMsIHRyYW5zVXJsLCAnL2h0bWxmaWxlJywgSHRtbGZpbGVSZWNlaXZlciwgWEhSTG9jYWxPYmplY3QpO1xyXG59XHJcblxyXG5pbmhlcml0cyhIdG1sRmlsZVRyYW5zcG9ydCwgQWpheEJhc2VkVHJhbnNwb3J0KTtcclxuXHJcbkh0bWxGaWxlVHJhbnNwb3J0LmVuYWJsZWQgPSBmdW5jdGlvbihpbmZvKSB7XHJcbiAgcmV0dXJuIEh0bWxmaWxlUmVjZWl2ZXIuZW5hYmxlZCAmJiBpbmZvLnNhbWVPcmlnaW47XHJcbn07XHJcblxyXG5IdG1sRmlsZVRyYW5zcG9ydC50cmFuc3BvcnROYW1lID0gJ2h0bWxmaWxlJztcclxuSHRtbEZpbGVUcmFuc3BvcnQucm91bmRUcmlwcyA9IDI7XHJcblxyXG5tb2R1bGUuZXhwb3J0cyA9IEh0bWxGaWxlVHJhbnNwb3J0O1xyXG5cclxufSx7XCIuL2xpYi9hamF4LWJhc2VkXCI6MjQsXCIuL3JlY2VpdmVyL2h0bWxmaWxlXCI6MzAsXCIuL3NlbmRlci94aHItbG9jYWxcIjozNyxcImluaGVyaXRzXCI6NTd9XSwyMjpbZnVuY3Rpb24ocmVxdWlyZSxtb2R1bGUsZXhwb3J0cyl7XHJcbihmdW5jdGlvbiAocHJvY2Vzcyl7XHJcbid1c2Ugc3RyaWN0JztcclxuXHJcbi8vIEZldyBjb29sIHRyYW5zcG9ydHMgZG8gd29yayBvbmx5IGZvciBzYW1lLW9yaWdpbi4gSW4gb3JkZXIgdG8gbWFrZVxyXG4vLyB0aGVtIHdvcmsgY3Jvc3MtZG9tYWluIHdlIHNoYWxsIHVzZSBpZnJhbWUsIHNlcnZlZCBmcm9tIHRoZVxyXG4vLyByZW1vdGUgZG9tYWluLiBOZXcgYnJvd3NlcnMgaGF2ZSBjYXBhYmlsaXRpZXMgdG8gY29tbXVuaWNhdGUgd2l0aFxyXG4vLyBjcm9zcyBkb21haW4gaWZyYW1lIHVzaW5nIHBvc3RNZXNzYWdlKCkuIEluIElFIGl0IHdhcyBpbXBsZW1lbnRlZFxyXG4vLyBmcm9tIElFIDgrLCBidXQgb2YgY291cnNlLCBJRSBnb3Qgc29tZSBkZXRhaWxzIHdyb25nOlxyXG4vLyAgICBodHRwOi8vbXNkbi5taWNyb3NvZnQuY29tL2VuLXVzL2xpYnJhcnkvY2MxOTcwMTUodj1WUy44NSkuYXNweFxyXG4vLyAgICBodHRwOi8vc3RldmVzb3VkZXJzLmNvbS9taXNjL3Rlc3QtcG9zdG1lc3NhZ2UucGhwXHJcblxyXG52YXIgaW5oZXJpdHMgPSByZXF1aXJlKCdpbmhlcml0cycpXHJcbiAgLCBKU09OMyA9IHJlcXVpcmUoJ2pzb24zJylcclxuICAsIEV2ZW50RW1pdHRlciA9IHJlcXVpcmUoJ2V2ZW50cycpLkV2ZW50RW1pdHRlclxyXG4gICwgdmVyc2lvbiA9IHJlcXVpcmUoJy4uL3ZlcnNpb24nKVxyXG4gICwgdXJsVXRpbHMgPSByZXF1aXJlKCcuLi91dGlscy91cmwnKVxyXG4gICwgaWZyYW1lVXRpbHMgPSByZXF1aXJlKCcuLi91dGlscy9pZnJhbWUnKVxyXG4gICwgZXZlbnRVdGlscyA9IHJlcXVpcmUoJy4uL3V0aWxzL2V2ZW50JylcclxuICAsIHJhbmRvbSA9IHJlcXVpcmUoJy4uL3V0aWxzL3JhbmRvbScpXHJcbiAgO1xyXG5cclxudmFyIGRlYnVnID0gZnVuY3Rpb24oKSB7fTtcclxuaWYgKHByb2Nlc3MuZW52Lk5PREVfRU5WICE9PSAncHJvZHVjdGlvbicpIHtcclxuICBkZWJ1ZyA9IHJlcXVpcmUoJ2RlYnVnJykoJ3NvY2tqcy1jbGllbnQ6dHJhbnNwb3J0OmlmcmFtZScpO1xyXG59XHJcblxyXG5mdW5jdGlvbiBJZnJhbWVUcmFuc3BvcnQodHJhbnNwb3J0LCB0cmFuc1VybCwgYmFzZVVybCkge1xyXG4gIGlmICghSWZyYW1lVHJhbnNwb3J0LmVuYWJsZWQoKSkge1xyXG4gICAgdGhyb3cgbmV3IEVycm9yKCdUcmFuc3BvcnQgY3JlYXRlZCB3aGVuIGRpc2FibGVkJyk7XHJcbiAgfVxyXG4gIEV2ZW50RW1pdHRlci5jYWxsKHRoaXMpO1xyXG5cclxuICB2YXIgc2VsZiA9IHRoaXM7XHJcbiAgdGhpcy5vcmlnaW4gPSB1cmxVdGlscy5nZXRPcmlnaW4oYmFzZVVybCk7XHJcbiAgdGhpcy5iYXNlVXJsID0gYmFzZVVybDtcclxuICB0aGlzLnRyYW5zVXJsID0gdHJhbnNVcmw7XHJcbiAgdGhpcy50cmFuc3BvcnQgPSB0cmFuc3BvcnQ7XHJcbiAgdGhpcy53aW5kb3dJZCA9IHJhbmRvbS5zdHJpbmcoOCk7XHJcblxyXG4gIHZhciBpZnJhbWVVcmwgPSB1cmxVdGlscy5hZGRQYXRoKGJhc2VVcmwsICcvaWZyYW1lLmh0bWwnKSArICcjJyArIHRoaXMud2luZG93SWQ7XHJcbiAgZGVidWcodHJhbnNwb3J0LCB0cmFuc1VybCwgaWZyYW1lVXJsKTtcclxuXHJcbiAgdGhpcy5pZnJhbWVPYmogPSBpZnJhbWVVdGlscy5jcmVhdGVJZnJhbWUoaWZyYW1lVXJsLCBmdW5jdGlvbihyKSB7XHJcbiAgICBkZWJ1ZygnZXJyIGNhbGxiYWNrJyk7XHJcbiAgICBzZWxmLmVtaXQoJ2Nsb3NlJywgMTAwNiwgJ1VuYWJsZSB0byBsb2FkIGFuIGlmcmFtZSAoJyArIHIgKyAnKScpO1xyXG4gICAgc2VsZi5jbG9zZSgpO1xyXG4gIH0pO1xyXG5cclxuICB0aGlzLm9ubWVzc2FnZUNhbGxiYWNrID0gdGhpcy5fbWVzc2FnZS5iaW5kKHRoaXMpO1xyXG4gIGV2ZW50VXRpbHMuYXR0YWNoRXZlbnQoJ21lc3NhZ2UnLCB0aGlzLm9ubWVzc2FnZUNhbGxiYWNrKTtcclxufVxyXG5cclxuaW5oZXJpdHMoSWZyYW1lVHJhbnNwb3J0LCBFdmVudEVtaXR0ZXIpO1xyXG5cclxuSWZyYW1lVHJhbnNwb3J0LnByb3RvdHlwZS5jbG9zZSA9IGZ1bmN0aW9uKCkge1xyXG4gIGRlYnVnKCdjbG9zZScpO1xyXG4gIHRoaXMucmVtb3ZlQWxsTGlzdGVuZXJzKCk7XHJcbiAgaWYgKHRoaXMuaWZyYW1lT2JqKSB7XHJcbiAgICBldmVudFV0aWxzLmRldGFjaEV2ZW50KCdtZXNzYWdlJywgdGhpcy5vbm1lc3NhZ2VDYWxsYmFjayk7XHJcbiAgICB0cnkge1xyXG4gICAgICAvLyBXaGVuIHRoZSBpZnJhbWUgaXMgbm90IGxvYWRlZCwgSUUgcmFpc2VzIGFuIGV4Y2VwdGlvblxyXG4gICAgICAvLyBvbiAnY29udGVudFdpbmRvdycuXHJcbiAgICAgIHRoaXMucG9zdE1lc3NhZ2UoJ2MnKTtcclxuICAgIH0gY2F0Y2ggKHgpIHtcclxuICAgICAgLy8gaW50ZW50aW9uYWxseSBlbXB0eVxyXG4gICAgfVxyXG4gICAgdGhpcy5pZnJhbWVPYmouY2xlYW51cCgpO1xyXG4gICAgdGhpcy5pZnJhbWVPYmogPSBudWxsO1xyXG4gICAgdGhpcy5vbm1lc3NhZ2VDYWxsYmFjayA9IHRoaXMuaWZyYW1lT2JqID0gbnVsbDtcclxuICB9XHJcbn07XHJcblxyXG5JZnJhbWVUcmFuc3BvcnQucHJvdG90eXBlLl9tZXNzYWdlID0gZnVuY3Rpb24oZSkge1xyXG4gIGRlYnVnKCdtZXNzYWdlJywgZS5kYXRhKTtcclxuICBpZiAoIXVybFV0aWxzLmlzT3JpZ2luRXF1YWwoZS5vcmlnaW4sIHRoaXMub3JpZ2luKSkge1xyXG4gICAgZGVidWcoJ25vdCBzYW1lIG9yaWdpbicsIGUub3JpZ2luLCB0aGlzLm9yaWdpbik7XHJcbiAgICByZXR1cm47XHJcbiAgfVxyXG5cclxuICB2YXIgaWZyYW1lTWVzc2FnZTtcclxuICB0cnkge1xyXG4gICAgaWZyYW1lTWVzc2FnZSA9IEpTT04zLnBhcnNlKGUuZGF0YSk7XHJcbiAgfSBjYXRjaCAoaWdub3JlZCkge1xyXG4gICAgZGVidWcoJ2JhZCBqc29uJywgZS5kYXRhKTtcclxuICAgIHJldHVybjtcclxuICB9XHJcblxyXG4gIGlmIChpZnJhbWVNZXNzYWdlLndpbmRvd0lkICE9PSB0aGlzLndpbmRvd0lkKSB7XHJcbiAgICBkZWJ1ZygnbWlzbWF0Y2hlZCB3aW5kb3cgaWQnLCBpZnJhbWVNZXNzYWdlLndpbmRvd0lkLCB0aGlzLndpbmRvd0lkKTtcclxuICAgIHJldHVybjtcclxuICB9XHJcblxyXG4gIHN3aXRjaCAoaWZyYW1lTWVzc2FnZS50eXBlKSB7XHJcbiAgY2FzZSAncyc6XHJcbiAgICB0aGlzLmlmcmFtZU9iai5sb2FkZWQoKTtcclxuICAgIC8vIHdpbmRvdyBnbG9iYWwgZGVwZW5kZW5jeVxyXG4gICAgdGhpcy5wb3N0TWVzc2FnZSgncycsIEpTT04zLnN0cmluZ2lmeShbXHJcbiAgICAgIHZlcnNpb25cclxuICAgICwgdGhpcy50cmFuc3BvcnRcclxuICAgICwgdGhpcy50cmFuc1VybFxyXG4gICAgLCB0aGlzLmJhc2VVcmxcclxuICAgIF0pKTtcclxuICAgIGJyZWFrO1xyXG4gIGNhc2UgJ3QnOlxyXG4gICAgdGhpcy5lbWl0KCdtZXNzYWdlJywgaWZyYW1lTWVzc2FnZS5kYXRhKTtcclxuICAgIGJyZWFrO1xyXG4gIGNhc2UgJ2MnOlxyXG4gICAgdmFyIGNkYXRhO1xyXG4gICAgdHJ5IHtcclxuICAgICAgY2RhdGEgPSBKU09OMy5wYXJzZShpZnJhbWVNZXNzYWdlLmRhdGEpO1xyXG4gICAgfSBjYXRjaCAoaWdub3JlZCkge1xyXG4gICAgICBkZWJ1ZygnYmFkIGpzb24nLCBpZnJhbWVNZXNzYWdlLmRhdGEpO1xyXG4gICAgICByZXR1cm47XHJcbiAgICB9XHJcbiAgICB0aGlzLmVtaXQoJ2Nsb3NlJywgY2RhdGFbMF0sIGNkYXRhWzFdKTtcclxuICAgIHRoaXMuY2xvc2UoKTtcclxuICAgIGJyZWFrO1xyXG4gIH1cclxufTtcclxuXHJcbklmcmFtZVRyYW5zcG9ydC5wcm90b3R5cGUucG9zdE1lc3NhZ2UgPSBmdW5jdGlvbih0eXBlLCBkYXRhKSB7XHJcbiAgZGVidWcoJ3Bvc3RNZXNzYWdlJywgdHlwZSwgZGF0YSk7XHJcbiAgdGhpcy5pZnJhbWVPYmoucG9zdChKU09OMy5zdHJpbmdpZnkoe1xyXG4gICAgd2luZG93SWQ6IHRoaXMud2luZG93SWRcclxuICAsIHR5cGU6IHR5cGVcclxuICAsIGRhdGE6IGRhdGEgfHwgJydcclxuICB9KSwgdGhpcy5vcmlnaW4pO1xyXG59O1xyXG5cclxuSWZyYW1lVHJhbnNwb3J0LnByb3RvdHlwZS5zZW5kID0gZnVuY3Rpb24obWVzc2FnZSkge1xyXG4gIGRlYnVnKCdzZW5kJywgbWVzc2FnZSk7XHJcbiAgdGhpcy5wb3N0TWVzc2FnZSgnbScsIG1lc3NhZ2UpO1xyXG59O1xyXG5cclxuSWZyYW1lVHJhbnNwb3J0LmVuYWJsZWQgPSBmdW5jdGlvbigpIHtcclxuICByZXR1cm4gaWZyYW1lVXRpbHMuaWZyYW1lRW5hYmxlZDtcclxufTtcclxuXHJcbklmcmFtZVRyYW5zcG9ydC50cmFuc3BvcnROYW1lID0gJ2lmcmFtZSc7XHJcbklmcmFtZVRyYW5zcG9ydC5yb3VuZFRyaXBzID0gMjtcclxuXHJcbm1vZHVsZS5leHBvcnRzID0gSWZyYW1lVHJhbnNwb3J0O1xyXG5cclxufSkuY2FsbCh0aGlzLHsgZW52OiB7fSB9KVxyXG5cclxufSx7XCIuLi91dGlscy9ldmVudFwiOjQ2LFwiLi4vdXRpbHMvaWZyYW1lXCI6NDcsXCIuLi91dGlscy9yYW5kb21cIjo1MCxcIi4uL3V0aWxzL3VybFwiOjUyLFwiLi4vdmVyc2lvblwiOjUzLFwiZGVidWdcIjo1NSxcImV2ZW50c1wiOjMsXCJpbmhlcml0c1wiOjU3LFwianNvbjNcIjo1OH1dLDIzOltmdW5jdGlvbihyZXF1aXJlLG1vZHVsZSxleHBvcnRzKXtcclxuKGZ1bmN0aW9uIChnbG9iYWwpe1xyXG4ndXNlIHN0cmljdCc7XHJcblxyXG4vLyBUaGUgc2ltcGxlc3QgYW5kIG1vc3Qgcm9idXN0IHRyYW5zcG9ydCwgdXNpbmcgdGhlIHdlbGwta25vdyBjcm9zc1xyXG4vLyBkb21haW4gaGFjayAtIEpTT05QLiBUaGlzIHRyYW5zcG9ydCBpcyBxdWl0ZSBpbmVmZmljaWVudCAtIG9uZVxyXG4vLyBtZXNzYWdlIGNvdWxkIHVzZSB1cCB0byBvbmUgaHR0cCByZXF1ZXN0LiBCdXQgYXQgbGVhc3QgaXQgd29ya3MgYWxtb3N0XHJcbi8vIGV2ZXJ5d2hlcmUuXHJcbi8vIEtub3duIGxpbWl0YXRpb25zOlxyXG4vLyAgIG8geW91IHdpbGwgZ2V0IGEgc3Bpbm5pbmcgY3Vyc29yXHJcbi8vICAgbyBmb3IgS29ucXVlcm9yIGEgZHVtYiB0aW1lciBpcyBuZWVkZWQgdG8gZGV0ZWN0IGVycm9yc1xyXG5cclxudmFyIGluaGVyaXRzID0gcmVxdWlyZSgnaW5oZXJpdHMnKVxyXG4gICwgU2VuZGVyUmVjZWl2ZXIgPSByZXF1aXJlKCcuL2xpYi9zZW5kZXItcmVjZWl2ZXInKVxyXG4gICwgSnNvbnBSZWNlaXZlciA9IHJlcXVpcmUoJy4vcmVjZWl2ZXIvanNvbnAnKVxyXG4gICwganNvbnBTZW5kZXIgPSByZXF1aXJlKCcuL3NlbmRlci9qc29ucCcpXHJcbiAgO1xyXG5cclxuZnVuY3Rpb24gSnNvblBUcmFuc3BvcnQodHJhbnNVcmwpIHtcclxuICBpZiAoIUpzb25QVHJhbnNwb3J0LmVuYWJsZWQoKSkge1xyXG4gICAgdGhyb3cgbmV3IEVycm9yKCdUcmFuc3BvcnQgY3JlYXRlZCB3aGVuIGRpc2FibGVkJyk7XHJcbiAgfVxyXG4gIFNlbmRlclJlY2VpdmVyLmNhbGwodGhpcywgdHJhbnNVcmwsICcvanNvbnAnLCBqc29ucFNlbmRlciwgSnNvbnBSZWNlaXZlcik7XHJcbn1cclxuXHJcbmluaGVyaXRzKEpzb25QVHJhbnNwb3J0LCBTZW5kZXJSZWNlaXZlcik7XHJcblxyXG5Kc29uUFRyYW5zcG9ydC5lbmFibGVkID0gZnVuY3Rpb24oKSB7XHJcbiAgcmV0dXJuICEhZ2xvYmFsLmRvY3VtZW50O1xyXG59O1xyXG5cclxuSnNvblBUcmFuc3BvcnQudHJhbnNwb3J0TmFtZSA9ICdqc29ucC1wb2xsaW5nJztcclxuSnNvblBUcmFuc3BvcnQucm91bmRUcmlwcyA9IDE7XHJcbkpzb25QVHJhbnNwb3J0Lm5lZWRCb2R5ID0gdHJ1ZTtcclxuXHJcbm1vZHVsZS5leHBvcnRzID0gSnNvblBUcmFuc3BvcnQ7XHJcblxyXG59KS5jYWxsKHRoaXMsdHlwZW9mIGdsb2JhbCAhPT0gXCJ1bmRlZmluZWRcIiA/IGdsb2JhbCA6IHR5cGVvZiBzZWxmICE9PSBcInVuZGVmaW5lZFwiID8gc2VsZiA6IHR5cGVvZiB3aW5kb3cgIT09IFwidW5kZWZpbmVkXCIgPyB3aW5kb3cgOiB7fSlcclxuXHJcbn0se1wiLi9saWIvc2VuZGVyLXJlY2VpdmVyXCI6MjgsXCIuL3JlY2VpdmVyL2pzb25wXCI6MzEsXCIuL3NlbmRlci9qc29ucFwiOjMzLFwiaW5oZXJpdHNcIjo1N31dLDI0OltmdW5jdGlvbihyZXF1aXJlLG1vZHVsZSxleHBvcnRzKXtcclxuKGZ1bmN0aW9uIChwcm9jZXNzKXtcclxuJ3VzZSBzdHJpY3QnO1xyXG5cclxudmFyIGluaGVyaXRzID0gcmVxdWlyZSgnaW5oZXJpdHMnKVxyXG4gICwgdXJsVXRpbHMgPSByZXF1aXJlKCcuLi8uLi91dGlscy91cmwnKVxyXG4gICwgU2VuZGVyUmVjZWl2ZXIgPSByZXF1aXJlKCcuL3NlbmRlci1yZWNlaXZlcicpXHJcbiAgO1xyXG5cclxudmFyIGRlYnVnID0gZnVuY3Rpb24oKSB7fTtcclxuaWYgKHByb2Nlc3MuZW52Lk5PREVfRU5WICE9PSAncHJvZHVjdGlvbicpIHtcclxuICBkZWJ1ZyA9IHJlcXVpcmUoJ2RlYnVnJykoJ3NvY2tqcy1jbGllbnQ6YWpheC1iYXNlZCcpO1xyXG59XHJcblxyXG5mdW5jdGlvbiBjcmVhdGVBamF4U2VuZGVyKEFqYXhPYmplY3QpIHtcclxuICByZXR1cm4gZnVuY3Rpb24odXJsLCBwYXlsb2FkLCBjYWxsYmFjaykge1xyXG4gICAgZGVidWcoJ2NyZWF0ZSBhamF4IHNlbmRlcicsIHVybCwgcGF5bG9hZCk7XHJcbiAgICB2YXIgb3B0ID0ge307XHJcbiAgICBpZiAodHlwZW9mIHBheWxvYWQgPT09ICdzdHJpbmcnKSB7XHJcbiAgICAgIG9wdC5oZWFkZXJzID0geydDb250ZW50LXR5cGUnOiAndGV4dC9wbGFpbid9O1xyXG4gICAgfVxyXG4gICAgdmFyIGFqYXhVcmwgPSB1cmxVdGlscy5hZGRQYXRoKHVybCwgJy94aHJfc2VuZCcpO1xyXG4gICAgdmFyIHhvID0gbmV3IEFqYXhPYmplY3QoJ1BPU1QnLCBhamF4VXJsLCBwYXlsb2FkLCBvcHQpO1xyXG4gICAgeG8ub25jZSgnZmluaXNoJywgZnVuY3Rpb24oc3RhdHVzKSB7XHJcbiAgICAgIGRlYnVnKCdmaW5pc2gnLCBzdGF0dXMpO1xyXG4gICAgICB4byA9IG51bGw7XHJcblxyXG4gICAgICBpZiAoc3RhdHVzICE9PSAyMDAgJiYgc3RhdHVzICE9PSAyMDQpIHtcclxuICAgICAgICByZXR1cm4gY2FsbGJhY2sobmV3IEVycm9yKCdodHRwIHN0YXR1cyAnICsgc3RhdHVzKSk7XHJcbiAgICAgIH1cclxuICAgICAgY2FsbGJhY2soKTtcclxuICAgIH0pO1xyXG4gICAgcmV0dXJuIGZ1bmN0aW9uKCkge1xyXG4gICAgICBkZWJ1ZygnYWJvcnQnKTtcclxuICAgICAgeG8uY2xvc2UoKTtcclxuICAgICAgeG8gPSBudWxsO1xyXG5cclxuICAgICAgdmFyIGVyciA9IG5ldyBFcnJvcignQWJvcnRlZCcpO1xyXG4gICAgICBlcnIuY29kZSA9IDEwMDA7XHJcbiAgICAgIGNhbGxiYWNrKGVycik7XHJcbiAgICB9O1xyXG4gIH07XHJcbn1cclxuXHJcbmZ1bmN0aW9uIEFqYXhCYXNlZFRyYW5zcG9ydCh0cmFuc1VybCwgdXJsU3VmZml4LCBSZWNlaXZlciwgQWpheE9iamVjdCkge1xyXG4gIFNlbmRlclJlY2VpdmVyLmNhbGwodGhpcywgdHJhbnNVcmwsIHVybFN1ZmZpeCwgY3JlYXRlQWpheFNlbmRlcihBamF4T2JqZWN0KSwgUmVjZWl2ZXIsIEFqYXhPYmplY3QpO1xyXG59XHJcblxyXG5pbmhlcml0cyhBamF4QmFzZWRUcmFuc3BvcnQsIFNlbmRlclJlY2VpdmVyKTtcclxuXHJcbm1vZHVsZS5leHBvcnRzID0gQWpheEJhc2VkVHJhbnNwb3J0O1xyXG5cclxufSkuY2FsbCh0aGlzLHsgZW52OiB7fSB9KVxyXG5cclxufSx7XCIuLi8uLi91dGlscy91cmxcIjo1MixcIi4vc2VuZGVyLXJlY2VpdmVyXCI6MjgsXCJkZWJ1Z1wiOjU1LFwiaW5oZXJpdHNcIjo1N31dLDI1OltmdW5jdGlvbihyZXF1aXJlLG1vZHVsZSxleHBvcnRzKXtcclxuKGZ1bmN0aW9uIChwcm9jZXNzKXtcclxuJ3VzZSBzdHJpY3QnO1xyXG5cclxudmFyIGluaGVyaXRzID0gcmVxdWlyZSgnaW5oZXJpdHMnKVxyXG4gICwgRXZlbnRFbWl0dGVyID0gcmVxdWlyZSgnZXZlbnRzJykuRXZlbnRFbWl0dGVyXHJcbiAgO1xyXG5cclxudmFyIGRlYnVnID0gZnVuY3Rpb24oKSB7fTtcclxuaWYgKHByb2Nlc3MuZW52Lk5PREVfRU5WICE9PSAncHJvZHVjdGlvbicpIHtcclxuICBkZWJ1ZyA9IHJlcXVpcmUoJ2RlYnVnJykoJ3NvY2tqcy1jbGllbnQ6YnVmZmVyZWQtc2VuZGVyJyk7XHJcbn1cclxuXHJcbmZ1bmN0aW9uIEJ1ZmZlcmVkU2VuZGVyKHVybCwgc2VuZGVyKSB7XHJcbiAgZGVidWcodXJsKTtcclxuICBFdmVudEVtaXR0ZXIuY2FsbCh0aGlzKTtcclxuICB0aGlzLnNlbmRCdWZmZXIgPSBbXTtcclxuICB0aGlzLnNlbmRlciA9IHNlbmRlcjtcclxuICB0aGlzLnVybCA9IHVybDtcclxufVxyXG5cclxuaW5oZXJpdHMoQnVmZmVyZWRTZW5kZXIsIEV2ZW50RW1pdHRlcik7XHJcblxyXG5CdWZmZXJlZFNlbmRlci5wcm90b3R5cGUuc2VuZCA9IGZ1bmN0aW9uKG1lc3NhZ2UpIHtcclxuICBkZWJ1Zygnc2VuZCcsIG1lc3NhZ2UpO1xyXG4gIHRoaXMuc2VuZEJ1ZmZlci5wdXNoKG1lc3NhZ2UpO1xyXG4gIGlmICghdGhpcy5zZW5kU3RvcCkge1xyXG4gICAgdGhpcy5zZW5kU2NoZWR1bGUoKTtcclxuICB9XHJcbn07XHJcblxyXG4vLyBGb3IgcG9sbGluZyB0cmFuc3BvcnRzIGluIGEgc2l0dWF0aW9uIHdoZW4gaW4gdGhlIG1lc3NhZ2UgY2FsbGJhY2ssXHJcbi8vIG5ldyBtZXNzYWdlIGlzIGJlaW5nIHNlbmQuIElmIHRoZSBzZW5kaW5nIGNvbm5lY3Rpb24gd2FzIHN0YXJ0ZWRcclxuLy8gYmVmb3JlIHJlY2VpdmluZyBvbmUsIGl0IGlzIHBvc3NpYmxlIHRvIHNhdHVyYXRlIHRoZSBuZXR3b3JrIGFuZFxyXG4vLyB0aW1lb3V0IGR1ZSB0byB0aGUgbGFjayBvZiByZWNlaXZpbmcgc29ja2V0LiBUbyBhdm9pZCB0aGF0IHdlIGRlbGF5XHJcbi8vIHNlbmRpbmcgbWVzc2FnZXMgYnkgc29tZSBzbWFsbCB0aW1lLCBpbiBvcmRlciB0byBsZXQgcmVjZWl2aW5nXHJcbi8vIGNvbm5lY3Rpb24gYmUgc3RhcnRlZCBiZWZvcmVoYW5kLiBUaGlzIGlzIG9ubHkgYSBoYWxmbWVhc3VyZSBhbmRcclxuLy8gZG9lcyBub3QgZml4IHRoZSBiaWcgcHJvYmxlbSwgYnV0IGl0IGRvZXMgbWFrZSB0aGUgdGVzdHMgZ28gbW9yZVxyXG4vLyBzdGFibGUgb24gc2xvdyBuZXR3b3Jrcy5cclxuQnVmZmVyZWRTZW5kZXIucHJvdG90eXBlLnNlbmRTY2hlZHVsZVdhaXQgPSBmdW5jdGlvbigpIHtcclxuICBkZWJ1Zygnc2VuZFNjaGVkdWxlV2FpdCcpO1xyXG4gIHZhciBzZWxmID0gdGhpcztcclxuICB2YXIgdHJlZjtcclxuICB0aGlzLnNlbmRTdG9wID0gZnVuY3Rpb24oKSB7XHJcbiAgICBkZWJ1Zygnc2VuZFN0b3AnKTtcclxuICAgIHNlbGYuc2VuZFN0b3AgPSBudWxsO1xyXG4gICAgY2xlYXJUaW1lb3V0KHRyZWYpO1xyXG4gIH07XHJcbiAgdHJlZiA9IHNldFRpbWVvdXQoZnVuY3Rpb24oKSB7XHJcbiAgICBkZWJ1ZygndGltZW91dCcpO1xyXG4gICAgc2VsZi5zZW5kU3RvcCA9IG51bGw7XHJcbiAgICBzZWxmLnNlbmRTY2hlZHVsZSgpO1xyXG4gIH0sIDI1KTtcclxufTtcclxuXHJcbkJ1ZmZlcmVkU2VuZGVyLnByb3RvdHlwZS5zZW5kU2NoZWR1bGUgPSBmdW5jdGlvbigpIHtcclxuICBkZWJ1Zygnc2VuZFNjaGVkdWxlJywgdGhpcy5zZW5kQnVmZmVyLmxlbmd0aCk7XHJcbiAgdmFyIHNlbGYgPSB0aGlzO1xyXG4gIGlmICh0aGlzLnNlbmRCdWZmZXIubGVuZ3RoID4gMCkge1xyXG4gICAgdmFyIHBheWxvYWQgPSAnWycgKyB0aGlzLnNlbmRCdWZmZXIuam9pbignLCcpICsgJ10nO1xyXG4gICAgdGhpcy5zZW5kU3RvcCA9IHRoaXMuc2VuZGVyKHRoaXMudXJsLCBwYXlsb2FkLCBmdW5jdGlvbihlcnIpIHtcclxuICAgICAgc2VsZi5zZW5kU3RvcCA9IG51bGw7XHJcbiAgICAgIGlmIChlcnIpIHtcclxuICAgICAgICBkZWJ1ZygnZXJyb3InLCBlcnIpO1xyXG4gICAgICAgIHNlbGYuZW1pdCgnY2xvc2UnLCBlcnIuY29kZSB8fCAxMDA2LCAnU2VuZGluZyBlcnJvcjogJyArIGVycik7XHJcbiAgICAgICAgc2VsZi5jbG9zZSgpO1xyXG4gICAgICB9IGVsc2Uge1xyXG4gICAgICAgIHNlbGYuc2VuZFNjaGVkdWxlV2FpdCgpO1xyXG4gICAgICB9XHJcbiAgICB9KTtcclxuICAgIHRoaXMuc2VuZEJ1ZmZlciA9IFtdO1xyXG4gIH1cclxufTtcclxuXHJcbkJ1ZmZlcmVkU2VuZGVyLnByb3RvdHlwZS5fY2xlYW51cCA9IGZ1bmN0aW9uKCkge1xyXG4gIGRlYnVnKCdfY2xlYW51cCcpO1xyXG4gIHRoaXMucmVtb3ZlQWxsTGlzdGVuZXJzKCk7XHJcbn07XHJcblxyXG5CdWZmZXJlZFNlbmRlci5wcm90b3R5cGUuY2xvc2UgPSBmdW5jdGlvbigpIHtcclxuICBkZWJ1ZygnY2xvc2UnKTtcclxuICB0aGlzLl9jbGVhbnVwKCk7XHJcbiAgaWYgKHRoaXMuc2VuZFN0b3ApIHtcclxuICAgIHRoaXMuc2VuZFN0b3AoKTtcclxuICAgIHRoaXMuc2VuZFN0b3AgPSBudWxsO1xyXG4gIH1cclxufTtcclxuXHJcbm1vZHVsZS5leHBvcnRzID0gQnVmZmVyZWRTZW5kZXI7XHJcblxyXG59KS5jYWxsKHRoaXMseyBlbnY6IHt9IH0pXHJcblxyXG59LHtcImRlYnVnXCI6NTUsXCJldmVudHNcIjozLFwiaW5oZXJpdHNcIjo1N31dLDI2OltmdW5jdGlvbihyZXF1aXJlLG1vZHVsZSxleHBvcnRzKXtcclxuKGZ1bmN0aW9uIChnbG9iYWwpe1xyXG4ndXNlIHN0cmljdCc7XHJcblxyXG52YXIgaW5oZXJpdHMgPSByZXF1aXJlKCdpbmhlcml0cycpXHJcbiAgLCBJZnJhbWVUcmFuc3BvcnQgPSByZXF1aXJlKCcuLi9pZnJhbWUnKVxyXG4gICwgb2JqZWN0VXRpbHMgPSByZXF1aXJlKCcuLi8uLi91dGlscy9vYmplY3QnKVxyXG4gIDtcclxuXHJcbm1vZHVsZS5leHBvcnRzID0gZnVuY3Rpb24odHJhbnNwb3J0KSB7XHJcblxyXG4gIGZ1bmN0aW9uIElmcmFtZVdyYXBUcmFuc3BvcnQodHJhbnNVcmwsIGJhc2VVcmwpIHtcclxuICAgIElmcmFtZVRyYW5zcG9ydC5jYWxsKHRoaXMsIHRyYW5zcG9ydC50cmFuc3BvcnROYW1lLCB0cmFuc1VybCwgYmFzZVVybCk7XHJcbiAgfVxyXG5cclxuICBpbmhlcml0cyhJZnJhbWVXcmFwVHJhbnNwb3J0LCBJZnJhbWVUcmFuc3BvcnQpO1xyXG5cclxuICBJZnJhbWVXcmFwVHJhbnNwb3J0LmVuYWJsZWQgPSBmdW5jdGlvbih1cmwsIGluZm8pIHtcclxuICAgIGlmICghZ2xvYmFsLmRvY3VtZW50KSB7XHJcbiAgICAgIHJldHVybiBmYWxzZTtcclxuICAgIH1cclxuXHJcbiAgICB2YXIgaWZyYW1lSW5mbyA9IG9iamVjdFV0aWxzLmV4dGVuZCh7fSwgaW5mbyk7XHJcbiAgICBpZnJhbWVJbmZvLnNhbWVPcmlnaW4gPSB0cnVlO1xyXG4gICAgcmV0dXJuIHRyYW5zcG9ydC5lbmFibGVkKGlmcmFtZUluZm8pICYmIElmcmFtZVRyYW5zcG9ydC5lbmFibGVkKCk7XHJcbiAgfTtcclxuXHJcbiAgSWZyYW1lV3JhcFRyYW5zcG9ydC50cmFuc3BvcnROYW1lID0gJ2lmcmFtZS0nICsgdHJhbnNwb3J0LnRyYW5zcG9ydE5hbWU7XHJcbiAgSWZyYW1lV3JhcFRyYW5zcG9ydC5uZWVkQm9keSA9IHRydWU7XHJcbiAgSWZyYW1lV3JhcFRyYW5zcG9ydC5yb3VuZFRyaXBzID0gSWZyYW1lVHJhbnNwb3J0LnJvdW5kVHJpcHMgKyB0cmFuc3BvcnQucm91bmRUcmlwcyAtIDE7IC8vIGh0bWwsIGphdmFzY3JpcHQgKDIpICsgdHJhbnNwb3J0IC0gbm8gQ09SUyAoMSlcclxuXHJcbiAgSWZyYW1lV3JhcFRyYW5zcG9ydC5mYWNhZGVUcmFuc3BvcnQgPSB0cmFuc3BvcnQ7XHJcblxyXG4gIHJldHVybiBJZnJhbWVXcmFwVHJhbnNwb3J0O1xyXG59O1xyXG5cclxufSkuY2FsbCh0aGlzLHR5cGVvZiBnbG9iYWwgIT09IFwidW5kZWZpbmVkXCIgPyBnbG9iYWwgOiB0eXBlb2Ygc2VsZiAhPT0gXCJ1bmRlZmluZWRcIiA/IHNlbGYgOiB0eXBlb2Ygd2luZG93ICE9PSBcInVuZGVmaW5lZFwiID8gd2luZG93IDoge30pXHJcblxyXG59LHtcIi4uLy4uL3V0aWxzL29iamVjdFwiOjQ5LFwiLi4vaWZyYW1lXCI6MjIsXCJpbmhlcml0c1wiOjU3fV0sMjc6W2Z1bmN0aW9uKHJlcXVpcmUsbW9kdWxlLGV4cG9ydHMpe1xyXG4oZnVuY3Rpb24gKHByb2Nlc3Mpe1xyXG4ndXNlIHN0cmljdCc7XHJcblxyXG52YXIgaW5oZXJpdHMgPSByZXF1aXJlKCdpbmhlcml0cycpXHJcbiAgLCBFdmVudEVtaXR0ZXIgPSByZXF1aXJlKCdldmVudHMnKS5FdmVudEVtaXR0ZXJcclxuICA7XHJcblxyXG52YXIgZGVidWcgPSBmdW5jdGlvbigpIHt9O1xyXG5pZiAocHJvY2Vzcy5lbnYuTk9ERV9FTlYgIT09ICdwcm9kdWN0aW9uJykge1xyXG4gIGRlYnVnID0gcmVxdWlyZSgnZGVidWcnKSgnc29ja2pzLWNsaWVudDpwb2xsaW5nJyk7XHJcbn1cclxuXHJcbmZ1bmN0aW9uIFBvbGxpbmcoUmVjZWl2ZXIsIHJlY2VpdmVVcmwsIEFqYXhPYmplY3QpIHtcclxuICBkZWJ1ZyhyZWNlaXZlVXJsKTtcclxuICBFdmVudEVtaXR0ZXIuY2FsbCh0aGlzKTtcclxuICB0aGlzLlJlY2VpdmVyID0gUmVjZWl2ZXI7XHJcbiAgdGhpcy5yZWNlaXZlVXJsID0gcmVjZWl2ZVVybDtcclxuICB0aGlzLkFqYXhPYmplY3QgPSBBamF4T2JqZWN0O1xyXG4gIHRoaXMuX3NjaGVkdWxlUmVjZWl2ZXIoKTtcclxufVxyXG5cclxuaW5oZXJpdHMoUG9sbGluZywgRXZlbnRFbWl0dGVyKTtcclxuXHJcblBvbGxpbmcucHJvdG90eXBlLl9zY2hlZHVsZVJlY2VpdmVyID0gZnVuY3Rpb24oKSB7XHJcbiAgZGVidWcoJ19zY2hlZHVsZVJlY2VpdmVyJyk7XHJcbiAgdmFyIHNlbGYgPSB0aGlzO1xyXG4gIHZhciBwb2xsID0gdGhpcy5wb2xsID0gbmV3IHRoaXMuUmVjZWl2ZXIodGhpcy5yZWNlaXZlVXJsLCB0aGlzLkFqYXhPYmplY3QpO1xyXG5cclxuICBwb2xsLm9uKCdtZXNzYWdlJywgZnVuY3Rpb24obXNnKSB7XHJcbiAgICBkZWJ1ZygnbWVzc2FnZScsIG1zZyk7XHJcbiAgICBzZWxmLmVtaXQoJ21lc3NhZ2UnLCBtc2cpO1xyXG4gIH0pO1xyXG5cclxuICBwb2xsLm9uY2UoJ2Nsb3NlJywgZnVuY3Rpb24oY29kZSwgcmVhc29uKSB7XHJcbiAgICBkZWJ1ZygnY2xvc2UnLCBjb2RlLCByZWFzb24sIHNlbGYucG9sbElzQ2xvc2luZyk7XHJcbiAgICBzZWxmLnBvbGwgPSBwb2xsID0gbnVsbDtcclxuXHJcbiAgICBpZiAoIXNlbGYucG9sbElzQ2xvc2luZykge1xyXG4gICAgICBpZiAocmVhc29uID09PSAnbmV0d29yaycpIHtcclxuICAgICAgICBzZWxmLl9zY2hlZHVsZVJlY2VpdmVyKCk7XHJcbiAgICAgIH0gZWxzZSB7XHJcbiAgICAgICAgc2VsZi5lbWl0KCdjbG9zZScsIGNvZGUgfHwgMTAwNiwgcmVhc29uKTtcclxuICAgICAgICBzZWxmLnJlbW92ZUFsbExpc3RlbmVycygpO1xyXG4gICAgICB9XHJcbiAgICB9XHJcbiAgfSk7XHJcbn07XHJcblxyXG5Qb2xsaW5nLnByb3RvdHlwZS5hYm9ydCA9IGZ1bmN0aW9uKCkge1xyXG4gIGRlYnVnKCdhYm9ydCcpO1xyXG4gIHRoaXMucmVtb3ZlQWxsTGlzdGVuZXJzKCk7XHJcbiAgdGhpcy5wb2xsSXNDbG9zaW5nID0gdHJ1ZTtcclxuICBpZiAodGhpcy5wb2xsKSB7XHJcbiAgICB0aGlzLnBvbGwuYWJvcnQoKTtcclxuICB9XHJcbn07XHJcblxyXG5tb2R1bGUuZXhwb3J0cyA9IFBvbGxpbmc7XHJcblxyXG59KS5jYWxsKHRoaXMseyBlbnY6IHt9IH0pXHJcblxyXG59LHtcImRlYnVnXCI6NTUsXCJldmVudHNcIjozLFwiaW5oZXJpdHNcIjo1N31dLDI4OltmdW5jdGlvbihyZXF1aXJlLG1vZHVsZSxleHBvcnRzKXtcclxuKGZ1bmN0aW9uIChwcm9jZXNzKXtcclxuJ3VzZSBzdHJpY3QnO1xyXG5cclxudmFyIGluaGVyaXRzID0gcmVxdWlyZSgnaW5oZXJpdHMnKVxyXG4gICwgdXJsVXRpbHMgPSByZXF1aXJlKCcuLi8uLi91dGlscy91cmwnKVxyXG4gICwgQnVmZmVyZWRTZW5kZXIgPSByZXF1aXJlKCcuL2J1ZmZlcmVkLXNlbmRlcicpXHJcbiAgLCBQb2xsaW5nID0gcmVxdWlyZSgnLi9wb2xsaW5nJylcclxuICA7XHJcblxyXG52YXIgZGVidWcgPSBmdW5jdGlvbigpIHt9O1xyXG5pZiAocHJvY2Vzcy5lbnYuTk9ERV9FTlYgIT09ICdwcm9kdWN0aW9uJykge1xyXG4gIGRlYnVnID0gcmVxdWlyZSgnZGVidWcnKSgnc29ja2pzLWNsaWVudDpzZW5kZXItcmVjZWl2ZXInKTtcclxufVxyXG5cclxuZnVuY3Rpb24gU2VuZGVyUmVjZWl2ZXIodHJhbnNVcmwsIHVybFN1ZmZpeCwgc2VuZGVyRnVuYywgUmVjZWl2ZXIsIEFqYXhPYmplY3QpIHtcclxuICB2YXIgcG9sbFVybCA9IHVybFV0aWxzLmFkZFBhdGgodHJhbnNVcmwsIHVybFN1ZmZpeCk7XHJcbiAgZGVidWcocG9sbFVybCk7XHJcbiAgdmFyIHNlbGYgPSB0aGlzO1xyXG4gIEJ1ZmZlcmVkU2VuZGVyLmNhbGwodGhpcywgdHJhbnNVcmwsIHNlbmRlckZ1bmMpO1xyXG5cclxuICB0aGlzLnBvbGwgPSBuZXcgUG9sbGluZyhSZWNlaXZlciwgcG9sbFVybCwgQWpheE9iamVjdCk7XHJcbiAgdGhpcy5wb2xsLm9uKCdtZXNzYWdlJywgZnVuY3Rpb24obXNnKSB7XHJcbiAgICBkZWJ1ZygncG9sbCBtZXNzYWdlJywgbXNnKTtcclxuICAgIHNlbGYuZW1pdCgnbWVzc2FnZScsIG1zZyk7XHJcbiAgfSk7XHJcbiAgdGhpcy5wb2xsLm9uY2UoJ2Nsb3NlJywgZnVuY3Rpb24oY29kZSwgcmVhc29uKSB7XHJcbiAgICBkZWJ1ZygncG9sbCBjbG9zZScsIGNvZGUsIHJlYXNvbik7XHJcbiAgICBzZWxmLnBvbGwgPSBudWxsO1xyXG4gICAgc2VsZi5lbWl0KCdjbG9zZScsIGNvZGUsIHJlYXNvbik7XHJcbiAgICBzZWxmLmNsb3NlKCk7XHJcbiAgfSk7XHJcbn1cclxuXHJcbmluaGVyaXRzKFNlbmRlclJlY2VpdmVyLCBCdWZmZXJlZFNlbmRlcik7XHJcblxyXG5TZW5kZXJSZWNlaXZlci5wcm90b3R5cGUuY2xvc2UgPSBmdW5jdGlvbigpIHtcclxuICBCdWZmZXJlZFNlbmRlci5wcm90b3R5cGUuY2xvc2UuY2FsbCh0aGlzKTtcclxuICBkZWJ1ZygnY2xvc2UnKTtcclxuICB0aGlzLnJlbW92ZUFsbExpc3RlbmVycygpO1xyXG4gIGlmICh0aGlzLnBvbGwpIHtcclxuICAgIHRoaXMucG9sbC5hYm9ydCgpO1xyXG4gICAgdGhpcy5wb2xsID0gbnVsbDtcclxuICB9XHJcbn07XHJcblxyXG5tb2R1bGUuZXhwb3J0cyA9IFNlbmRlclJlY2VpdmVyO1xyXG5cclxufSkuY2FsbCh0aGlzLHsgZW52OiB7fSB9KVxyXG5cclxufSx7XCIuLi8uLi91dGlscy91cmxcIjo1MixcIi4vYnVmZmVyZWQtc2VuZGVyXCI6MjUsXCIuL3BvbGxpbmdcIjoyNyxcImRlYnVnXCI6NTUsXCJpbmhlcml0c1wiOjU3fV0sMjk6W2Z1bmN0aW9uKHJlcXVpcmUsbW9kdWxlLGV4cG9ydHMpe1xyXG4oZnVuY3Rpb24gKHByb2Nlc3Mpe1xyXG4ndXNlIHN0cmljdCc7XHJcblxyXG52YXIgaW5oZXJpdHMgPSByZXF1aXJlKCdpbmhlcml0cycpXHJcbiAgLCBFdmVudEVtaXR0ZXIgPSByZXF1aXJlKCdldmVudHMnKS5FdmVudEVtaXR0ZXJcclxuICAsIEV2ZW50U291cmNlRHJpdmVyID0gcmVxdWlyZSgnZXZlbnRzb3VyY2UnKVxyXG4gIDtcclxuXHJcbnZhciBkZWJ1ZyA9IGZ1bmN0aW9uKCkge307XHJcbmlmIChwcm9jZXNzLmVudi5OT0RFX0VOViAhPT0gJ3Byb2R1Y3Rpb24nKSB7XHJcbiAgZGVidWcgPSByZXF1aXJlKCdkZWJ1ZycpKCdzb2NranMtY2xpZW50OnJlY2VpdmVyOmV2ZW50c291cmNlJyk7XHJcbn1cclxuXHJcbmZ1bmN0aW9uIEV2ZW50U291cmNlUmVjZWl2ZXIodXJsKSB7XHJcbiAgZGVidWcodXJsKTtcclxuICBFdmVudEVtaXR0ZXIuY2FsbCh0aGlzKTtcclxuXHJcbiAgdmFyIHNlbGYgPSB0aGlzO1xyXG4gIHZhciBlcyA9IHRoaXMuZXMgPSBuZXcgRXZlbnRTb3VyY2VEcml2ZXIodXJsKTtcclxuICBlcy5vbm1lc3NhZ2UgPSBmdW5jdGlvbihlKSB7XHJcbiAgICBkZWJ1ZygnbWVzc2FnZScsIGUuZGF0YSk7XHJcbiAgICBzZWxmLmVtaXQoJ21lc3NhZ2UnLCBkZWNvZGVVUkkoZS5kYXRhKSk7XHJcbiAgfTtcclxuICBlcy5vbmVycm9yID0gZnVuY3Rpb24oZSkge1xyXG4gICAgZGVidWcoJ2Vycm9yJywgZXMucmVhZHlTdGF0ZSwgZSk7XHJcbiAgICAvLyBFUyBvbiByZWNvbm5lY3Rpb24gaGFzIHJlYWR5U3RhdGUgPSAwIG9yIDEuXHJcbiAgICAvLyBvbiBuZXR3b3JrIGVycm9yIGl0J3MgQ0xPU0VEID0gMlxyXG4gICAgdmFyIHJlYXNvbiA9IChlcy5yZWFkeVN0YXRlICE9PSAyID8gJ25ldHdvcmsnIDogJ3Blcm1hbmVudCcpO1xyXG4gICAgc2VsZi5fY2xlYW51cCgpO1xyXG4gICAgc2VsZi5fY2xvc2UocmVhc29uKTtcclxuICB9O1xyXG59XHJcblxyXG5pbmhlcml0cyhFdmVudFNvdXJjZVJlY2VpdmVyLCBFdmVudEVtaXR0ZXIpO1xyXG5cclxuRXZlbnRTb3VyY2VSZWNlaXZlci5wcm90b3R5cGUuYWJvcnQgPSBmdW5jdGlvbigpIHtcclxuICBkZWJ1ZygnYWJvcnQnKTtcclxuICB0aGlzLl9jbGVhbnVwKCk7XHJcbiAgdGhpcy5fY2xvc2UoJ3VzZXInKTtcclxufTtcclxuXHJcbkV2ZW50U291cmNlUmVjZWl2ZXIucHJvdG90eXBlLl9jbGVhbnVwID0gZnVuY3Rpb24oKSB7XHJcbiAgZGVidWcoJ2NsZWFudXAnKTtcclxuICB2YXIgZXMgPSB0aGlzLmVzO1xyXG4gIGlmIChlcykge1xyXG4gICAgZXMub25tZXNzYWdlID0gZXMub25lcnJvciA9IG51bGw7XHJcbiAgICBlcy5jbG9zZSgpO1xyXG4gICAgdGhpcy5lcyA9IG51bGw7XHJcbiAgfVxyXG59O1xyXG5cclxuRXZlbnRTb3VyY2VSZWNlaXZlci5wcm90b3R5cGUuX2Nsb3NlID0gZnVuY3Rpb24ocmVhc29uKSB7XHJcbiAgZGVidWcoJ2Nsb3NlJywgcmVhc29uKTtcclxuICB2YXIgc2VsZiA9IHRoaXM7XHJcbiAgLy8gU2FmYXJpIGFuZCBjaHJvbWUgPCAxNSBjcmFzaCBpZiB3ZSBjbG9zZSB3aW5kb3cgYmVmb3JlXHJcbiAgLy8gd2FpdGluZyBmb3IgRVMgY2xlYW51cC4gU2VlOlxyXG4gIC8vIGh0dHBzOi8vY29kZS5nb29nbGUuY29tL3AvY2hyb21pdW0vaXNzdWVzL2RldGFpbD9pZD04OTE1NVxyXG4gIHNldFRpbWVvdXQoZnVuY3Rpb24oKSB7XHJcbiAgICBzZWxmLmVtaXQoJ2Nsb3NlJywgbnVsbCwgcmVhc29uKTtcclxuICAgIHNlbGYucmVtb3ZlQWxsTGlzdGVuZXJzKCk7XHJcbiAgfSwgMjAwKTtcclxufTtcclxuXHJcbm1vZHVsZS5leHBvcnRzID0gRXZlbnRTb3VyY2VSZWNlaXZlcjtcclxuXHJcbn0pLmNhbGwodGhpcyx7IGVudjoge30gfSlcclxuXHJcbn0se1wiZGVidWdcIjo1NSxcImV2ZW50c1wiOjMsXCJldmVudHNvdXJjZVwiOjE4LFwiaW5oZXJpdHNcIjo1N31dLDMwOltmdW5jdGlvbihyZXF1aXJlLG1vZHVsZSxleHBvcnRzKXtcclxuKGZ1bmN0aW9uIChwcm9jZXNzLGdsb2JhbCl7XHJcbid1c2Ugc3RyaWN0JztcclxuXHJcbnZhciBpbmhlcml0cyA9IHJlcXVpcmUoJ2luaGVyaXRzJylcclxuICAsIGlmcmFtZVV0aWxzID0gcmVxdWlyZSgnLi4vLi4vdXRpbHMvaWZyYW1lJylcclxuICAsIHVybFV0aWxzID0gcmVxdWlyZSgnLi4vLi4vdXRpbHMvdXJsJylcclxuICAsIEV2ZW50RW1pdHRlciA9IHJlcXVpcmUoJ2V2ZW50cycpLkV2ZW50RW1pdHRlclxyXG4gICwgcmFuZG9tID0gcmVxdWlyZSgnLi4vLi4vdXRpbHMvcmFuZG9tJylcclxuICA7XHJcblxyXG52YXIgZGVidWcgPSBmdW5jdGlvbigpIHt9O1xyXG5pZiAocHJvY2Vzcy5lbnYuTk9ERV9FTlYgIT09ICdwcm9kdWN0aW9uJykge1xyXG4gIGRlYnVnID0gcmVxdWlyZSgnZGVidWcnKSgnc29ja2pzLWNsaWVudDpyZWNlaXZlcjpodG1sZmlsZScpO1xyXG59XHJcblxyXG5mdW5jdGlvbiBIdG1sZmlsZVJlY2VpdmVyKHVybCkge1xyXG4gIGRlYnVnKHVybCk7XHJcbiAgRXZlbnRFbWl0dGVyLmNhbGwodGhpcyk7XHJcbiAgdmFyIHNlbGYgPSB0aGlzO1xyXG4gIGlmcmFtZVV0aWxzLnBvbGx1dGVHbG9iYWxOYW1lc3BhY2UoKTtcclxuXHJcbiAgdGhpcy5pZCA9ICdhJyArIHJhbmRvbS5zdHJpbmcoNik7XHJcbiAgdXJsID0gdXJsVXRpbHMuYWRkUXVlcnkodXJsLCAnYz0nICsgZGVjb2RlVVJJQ29tcG9uZW50KGlmcmFtZVV0aWxzLldQcmVmaXggKyAnLicgKyB0aGlzLmlkKSk7XHJcblxyXG4gIGRlYnVnKCd1c2luZyBodG1sZmlsZScsIEh0bWxmaWxlUmVjZWl2ZXIuaHRtbGZpbGVFbmFibGVkKTtcclxuICB2YXIgY29uc3RydWN0RnVuYyA9IEh0bWxmaWxlUmVjZWl2ZXIuaHRtbGZpbGVFbmFibGVkID9cclxuICAgICAgaWZyYW1lVXRpbHMuY3JlYXRlSHRtbGZpbGUgOiBpZnJhbWVVdGlscy5jcmVhdGVJZnJhbWU7XHJcblxyXG4gIGdsb2JhbFtpZnJhbWVVdGlscy5XUHJlZml4XVt0aGlzLmlkXSA9IHtcclxuICAgIHN0YXJ0OiBmdW5jdGlvbigpIHtcclxuICAgICAgZGVidWcoJ3N0YXJ0Jyk7XHJcbiAgICAgIHNlbGYuaWZyYW1lT2JqLmxvYWRlZCgpO1xyXG4gICAgfVxyXG4gICwgbWVzc2FnZTogZnVuY3Rpb24oZGF0YSkge1xyXG4gICAgICBkZWJ1ZygnbWVzc2FnZScsIGRhdGEpO1xyXG4gICAgICBzZWxmLmVtaXQoJ21lc3NhZ2UnLCBkYXRhKTtcclxuICAgIH1cclxuICAsIHN0b3A6IGZ1bmN0aW9uKCkge1xyXG4gICAgICBkZWJ1Zygnc3RvcCcpO1xyXG4gICAgICBzZWxmLl9jbGVhbnVwKCk7XHJcbiAgICAgIHNlbGYuX2Nsb3NlKCduZXR3b3JrJyk7XHJcbiAgICB9XHJcbiAgfTtcclxuICB0aGlzLmlmcmFtZU9iaiA9IGNvbnN0cnVjdEZ1bmModXJsLCBmdW5jdGlvbigpIHtcclxuICAgIGRlYnVnKCdjYWxsYmFjaycpO1xyXG4gICAgc2VsZi5fY2xlYW51cCgpO1xyXG4gICAgc2VsZi5fY2xvc2UoJ3Blcm1hbmVudCcpO1xyXG4gIH0pO1xyXG59XHJcblxyXG5pbmhlcml0cyhIdG1sZmlsZVJlY2VpdmVyLCBFdmVudEVtaXR0ZXIpO1xyXG5cclxuSHRtbGZpbGVSZWNlaXZlci5wcm90b3R5cGUuYWJvcnQgPSBmdW5jdGlvbigpIHtcclxuICBkZWJ1ZygnYWJvcnQnKTtcclxuICB0aGlzLl9jbGVhbnVwKCk7XHJcbiAgdGhpcy5fY2xvc2UoJ3VzZXInKTtcclxufTtcclxuXHJcbkh0bWxmaWxlUmVjZWl2ZXIucHJvdG90eXBlLl9jbGVhbnVwID0gZnVuY3Rpb24oKSB7XHJcbiAgZGVidWcoJ19jbGVhbnVwJyk7XHJcbiAgaWYgKHRoaXMuaWZyYW1lT2JqKSB7XHJcbiAgICB0aGlzLmlmcmFtZU9iai5jbGVhbnVwKCk7XHJcbiAgICB0aGlzLmlmcmFtZU9iaiA9IG51bGw7XHJcbiAgfVxyXG4gIGRlbGV0ZSBnbG9iYWxbaWZyYW1lVXRpbHMuV1ByZWZpeF1bdGhpcy5pZF07XHJcbn07XHJcblxyXG5IdG1sZmlsZVJlY2VpdmVyLnByb3RvdHlwZS5fY2xvc2UgPSBmdW5jdGlvbihyZWFzb24pIHtcclxuICBkZWJ1ZygnX2Nsb3NlJywgcmVhc29uKTtcclxuICB0aGlzLmVtaXQoJ2Nsb3NlJywgbnVsbCwgcmVhc29uKTtcclxuICB0aGlzLnJlbW92ZUFsbExpc3RlbmVycygpO1xyXG59O1xyXG5cclxuSHRtbGZpbGVSZWNlaXZlci5odG1sZmlsZUVuYWJsZWQgPSBmYWxzZTtcclxuXHJcbi8vIG9iZnVzY2F0ZSB0byBhdm9pZCBmaXJld2FsbHNcclxudmFyIGF4byA9IFsnQWN0aXZlJ10uY29uY2F0KCdPYmplY3QnKS5qb2luKCdYJyk7XHJcbmlmIChheG8gaW4gZ2xvYmFsKSB7XHJcbiAgdHJ5IHtcclxuICAgIEh0bWxmaWxlUmVjZWl2ZXIuaHRtbGZpbGVFbmFibGVkID0gISFuZXcgZ2xvYmFsW2F4b10oJ2h0bWxmaWxlJyk7XHJcbiAgfSBjYXRjaCAoeCkge1xyXG4gICAgLy8gaW50ZW50aW9uYWxseSBlbXB0eVxyXG4gIH1cclxufVxyXG5cclxuSHRtbGZpbGVSZWNlaXZlci5lbmFibGVkID0gSHRtbGZpbGVSZWNlaXZlci5odG1sZmlsZUVuYWJsZWQgfHwgaWZyYW1lVXRpbHMuaWZyYW1lRW5hYmxlZDtcclxuXHJcbm1vZHVsZS5leHBvcnRzID0gSHRtbGZpbGVSZWNlaXZlcjtcclxuXHJcbn0pLmNhbGwodGhpcyx7IGVudjoge30gfSx0eXBlb2YgZ2xvYmFsICE9PSBcInVuZGVmaW5lZFwiID8gZ2xvYmFsIDogdHlwZW9mIHNlbGYgIT09IFwidW5kZWZpbmVkXCIgPyBzZWxmIDogdHlwZW9mIHdpbmRvdyAhPT0gXCJ1bmRlZmluZWRcIiA/IHdpbmRvdyA6IHt9KVxyXG5cclxufSx7XCIuLi8uLi91dGlscy9pZnJhbWVcIjo0NyxcIi4uLy4uL3V0aWxzL3JhbmRvbVwiOjUwLFwiLi4vLi4vdXRpbHMvdXJsXCI6NTIsXCJkZWJ1Z1wiOjU1LFwiZXZlbnRzXCI6MyxcImluaGVyaXRzXCI6NTd9XSwzMTpbZnVuY3Rpb24ocmVxdWlyZSxtb2R1bGUsZXhwb3J0cyl7XHJcbihmdW5jdGlvbiAocHJvY2VzcyxnbG9iYWwpe1xyXG4ndXNlIHN0cmljdCc7XHJcblxyXG52YXIgdXRpbHMgPSByZXF1aXJlKCcuLi8uLi91dGlscy9pZnJhbWUnKVxyXG4gICwgcmFuZG9tID0gcmVxdWlyZSgnLi4vLi4vdXRpbHMvcmFuZG9tJylcclxuICAsIGJyb3dzZXIgPSByZXF1aXJlKCcuLi8uLi91dGlscy9icm93c2VyJylcclxuICAsIHVybFV0aWxzID0gcmVxdWlyZSgnLi4vLi4vdXRpbHMvdXJsJylcclxuICAsIGluaGVyaXRzID0gcmVxdWlyZSgnaW5oZXJpdHMnKVxyXG4gICwgRXZlbnRFbWl0dGVyID0gcmVxdWlyZSgnZXZlbnRzJykuRXZlbnRFbWl0dGVyXHJcbiAgO1xyXG5cclxudmFyIGRlYnVnID0gZnVuY3Rpb24oKSB7fTtcclxuaWYgKHByb2Nlc3MuZW52Lk5PREVfRU5WICE9PSAncHJvZHVjdGlvbicpIHtcclxuICBkZWJ1ZyA9IHJlcXVpcmUoJ2RlYnVnJykoJ3NvY2tqcy1jbGllbnQ6cmVjZWl2ZXI6anNvbnAnKTtcclxufVxyXG5cclxuZnVuY3Rpb24gSnNvbnBSZWNlaXZlcih1cmwpIHtcclxuICBkZWJ1Zyh1cmwpO1xyXG4gIHZhciBzZWxmID0gdGhpcztcclxuICBFdmVudEVtaXR0ZXIuY2FsbCh0aGlzKTtcclxuXHJcbiAgdXRpbHMucG9sbHV0ZUdsb2JhbE5hbWVzcGFjZSgpO1xyXG5cclxuICB0aGlzLmlkID0gJ2EnICsgcmFuZG9tLnN0cmluZyg2KTtcclxuICB2YXIgdXJsV2l0aElkID0gdXJsVXRpbHMuYWRkUXVlcnkodXJsLCAnYz0nICsgZW5jb2RlVVJJQ29tcG9uZW50KHV0aWxzLldQcmVmaXggKyAnLicgKyB0aGlzLmlkKSk7XHJcblxyXG4gIGdsb2JhbFt1dGlscy5XUHJlZml4XVt0aGlzLmlkXSA9IHRoaXMuX2NhbGxiYWNrLmJpbmQodGhpcyk7XHJcbiAgdGhpcy5fY3JlYXRlU2NyaXB0KHVybFdpdGhJZCk7XHJcblxyXG4gIC8vIEZhbGxiYWNrIG1vc3RseSBmb3IgS29ucXVlcm9yIC0gc3R1cGlkIHRpbWVyLCAzNSBzZWNvbmRzIHNoYWxsIGJlIHBsZW50eS5cclxuICB0aGlzLnRpbWVvdXRJZCA9IHNldFRpbWVvdXQoZnVuY3Rpb24oKSB7XHJcbiAgICBkZWJ1ZygndGltZW91dCcpO1xyXG4gICAgc2VsZi5fYWJvcnQobmV3IEVycm9yKCdKU09OUCBzY3JpcHQgbG9hZGVkIGFibm9ybWFsbHkgKHRpbWVvdXQpJykpO1xyXG4gIH0sIEpzb25wUmVjZWl2ZXIudGltZW91dCk7XHJcbn1cclxuXHJcbmluaGVyaXRzKEpzb25wUmVjZWl2ZXIsIEV2ZW50RW1pdHRlcik7XHJcblxyXG5Kc29ucFJlY2VpdmVyLnByb3RvdHlwZS5hYm9ydCA9IGZ1bmN0aW9uKCkge1xyXG4gIGRlYnVnKCdhYm9ydCcpO1xyXG4gIGlmIChnbG9iYWxbdXRpbHMuV1ByZWZpeF1bdGhpcy5pZF0pIHtcclxuICAgIHZhciBlcnIgPSBuZXcgRXJyb3IoJ0pTT05QIHVzZXIgYWJvcnRlZCByZWFkJyk7XHJcbiAgICBlcnIuY29kZSA9IDEwMDA7XHJcbiAgICB0aGlzLl9hYm9ydChlcnIpO1xyXG4gIH1cclxufTtcclxuXHJcbkpzb25wUmVjZWl2ZXIudGltZW91dCA9IDM1MDAwO1xyXG5Kc29ucFJlY2VpdmVyLnNjcmlwdEVycm9yVGltZW91dCA9IDEwMDA7XHJcblxyXG5Kc29ucFJlY2VpdmVyLnByb3RvdHlwZS5fY2FsbGJhY2sgPSBmdW5jdGlvbihkYXRhKSB7XHJcbiAgZGVidWcoJ19jYWxsYmFjaycsIGRhdGEpO1xyXG4gIHRoaXMuX2NsZWFudXAoKTtcclxuXHJcbiAgaWYgKHRoaXMuYWJvcnRpbmcpIHtcclxuICAgIHJldHVybjtcclxuICB9XHJcblxyXG4gIGlmIChkYXRhKSB7XHJcbiAgICBkZWJ1ZygnbWVzc2FnZScsIGRhdGEpO1xyXG4gICAgdGhpcy5lbWl0KCdtZXNzYWdlJywgZGF0YSk7XHJcbiAgfVxyXG4gIHRoaXMuZW1pdCgnY2xvc2UnLCBudWxsLCAnbmV0d29yaycpO1xyXG4gIHRoaXMucmVtb3ZlQWxsTGlzdGVuZXJzKCk7XHJcbn07XHJcblxyXG5Kc29ucFJlY2VpdmVyLnByb3RvdHlwZS5fYWJvcnQgPSBmdW5jdGlvbihlcnIpIHtcclxuICBkZWJ1ZygnX2Fib3J0JywgZXJyKTtcclxuICB0aGlzLl9jbGVhbnVwKCk7XHJcbiAgdGhpcy5hYm9ydGluZyA9IHRydWU7XHJcbiAgdGhpcy5lbWl0KCdjbG9zZScsIGVyci5jb2RlLCBlcnIubWVzc2FnZSk7XHJcbiAgdGhpcy5yZW1vdmVBbGxMaXN0ZW5lcnMoKTtcclxufTtcclxuXHJcbkpzb25wUmVjZWl2ZXIucHJvdG90eXBlLl9jbGVhbnVwID0gZnVuY3Rpb24oKSB7XHJcbiAgZGVidWcoJ19jbGVhbnVwJyk7XHJcbiAgY2xlYXJUaW1lb3V0KHRoaXMudGltZW91dElkKTtcclxuICBpZiAodGhpcy5zY3JpcHQyKSB7XHJcbiAgICB0aGlzLnNjcmlwdDIucGFyZW50Tm9kZS5yZW1vdmVDaGlsZCh0aGlzLnNjcmlwdDIpO1xyXG4gICAgdGhpcy5zY3JpcHQyID0gbnVsbDtcclxuICB9XHJcbiAgaWYgKHRoaXMuc2NyaXB0KSB7XHJcbiAgICB2YXIgc2NyaXB0ID0gdGhpcy5zY3JpcHQ7XHJcbiAgICAvLyBVbmZvcnR1bmF0ZWx5LCB5b3UgY2FuJ3QgcmVhbGx5IGFib3J0IHNjcmlwdCBsb2FkaW5nIG9mXHJcbiAgICAvLyB0aGUgc2NyaXB0LlxyXG4gICAgc2NyaXB0LnBhcmVudE5vZGUucmVtb3ZlQ2hpbGQoc2NyaXB0KTtcclxuICAgIHNjcmlwdC5vbnJlYWR5c3RhdGVjaGFuZ2UgPSBzY3JpcHQub25lcnJvciA9XHJcbiAgICAgICAgc2NyaXB0Lm9ubG9hZCA9IHNjcmlwdC5vbmNsaWNrID0gbnVsbDtcclxuICAgIHRoaXMuc2NyaXB0ID0gbnVsbDtcclxuICB9XHJcbiAgZGVsZXRlIGdsb2JhbFt1dGlscy5XUHJlZml4XVt0aGlzLmlkXTtcclxufTtcclxuXHJcbkpzb25wUmVjZWl2ZXIucHJvdG90eXBlLl9zY3JpcHRFcnJvciA9IGZ1bmN0aW9uKCkge1xyXG4gIGRlYnVnKCdfc2NyaXB0RXJyb3InKTtcclxuICB2YXIgc2VsZiA9IHRoaXM7XHJcbiAgaWYgKHRoaXMuZXJyb3JUaW1lcikge1xyXG4gICAgcmV0dXJuO1xyXG4gIH1cclxuXHJcbiAgdGhpcy5lcnJvclRpbWVyID0gc2V0VGltZW91dChmdW5jdGlvbigpIHtcclxuICAgIGlmICghc2VsZi5sb2FkZWRPa2F5KSB7XHJcbiAgICAgIHNlbGYuX2Fib3J0KG5ldyBFcnJvcignSlNPTlAgc2NyaXB0IGxvYWRlZCBhYm5vcm1hbGx5IChvbmVycm9yKScpKTtcclxuICAgIH1cclxuICB9LCBKc29ucFJlY2VpdmVyLnNjcmlwdEVycm9yVGltZW91dCk7XHJcbn07XHJcblxyXG5Kc29ucFJlY2VpdmVyLnByb3RvdHlwZS5fY3JlYXRlU2NyaXB0ID0gZnVuY3Rpb24odXJsKSB7XHJcbiAgZGVidWcoJ19jcmVhdGVTY3JpcHQnLCB1cmwpO1xyXG4gIHZhciBzZWxmID0gdGhpcztcclxuICB2YXIgc2NyaXB0ID0gdGhpcy5zY3JpcHQgPSBnbG9iYWwuZG9jdW1lbnQuY3JlYXRlRWxlbWVudCgnc2NyaXB0Jyk7XHJcbiAgdmFyIHNjcmlwdDI7ICAvLyBPcGVyYSBzeW5jaHJvbm91cyBsb2FkIHRyaWNrLlxyXG5cclxuICBzY3JpcHQuaWQgPSAnYScgKyByYW5kb20uc3RyaW5nKDgpO1xyXG4gIHNjcmlwdC5zcmMgPSB1cmw7XHJcbiAgc2NyaXB0LnR5cGUgPSAndGV4dC9qYXZhc2NyaXB0JztcclxuICBzY3JpcHQuY2hhcnNldCA9ICdVVEYtOCc7XHJcbiAgc2NyaXB0Lm9uZXJyb3IgPSB0aGlzLl9zY3JpcHRFcnJvci5iaW5kKHRoaXMpO1xyXG4gIHNjcmlwdC5vbmxvYWQgPSBmdW5jdGlvbigpIHtcclxuICAgIGRlYnVnKCdvbmxvYWQnKTtcclxuICAgIHNlbGYuX2Fib3J0KG5ldyBFcnJvcignSlNPTlAgc2NyaXB0IGxvYWRlZCBhYm5vcm1hbGx5IChvbmxvYWQpJykpO1xyXG4gIH07XHJcblxyXG4gIC8vIElFOSBmaXJlcyAnZXJyb3InIGV2ZW50IGFmdGVyIG9ucmVhZHlzdGF0ZWNoYW5nZSBvciBiZWZvcmUsIGluIHJhbmRvbSBvcmRlci5cclxuICAvLyBVc2UgbG9hZGVkT2theSB0byBkZXRlcm1pbmUgaWYgYWN0dWFsbHkgZXJyb3JlZFxyXG4gIHNjcmlwdC5vbnJlYWR5c3RhdGVjaGFuZ2UgPSBmdW5jdGlvbigpIHtcclxuICAgIGRlYnVnKCdvbnJlYWR5c3RhdGVjaGFuZ2UnLCBzY3JpcHQucmVhZHlTdGF0ZSk7XHJcbiAgICBpZiAoL2xvYWRlZHxjbG9zZWQvLnRlc3Qoc2NyaXB0LnJlYWR5U3RhdGUpKSB7XHJcbiAgICAgIGlmIChzY3JpcHQgJiYgc2NyaXB0Lmh0bWxGb3IgJiYgc2NyaXB0Lm9uY2xpY2spIHtcclxuICAgICAgICBzZWxmLmxvYWRlZE9rYXkgPSB0cnVlO1xyXG4gICAgICAgIHRyeSB7XHJcbiAgICAgICAgICAvLyBJbiBJRSwgYWN0dWFsbHkgZXhlY3V0ZSB0aGUgc2NyaXB0LlxyXG4gICAgICAgICAgc2NyaXB0Lm9uY2xpY2soKTtcclxuICAgICAgICB9IGNhdGNoICh4KSB7XHJcbiAgICAgICAgICAvLyBpbnRlbnRpb25hbGx5IGVtcHR5XHJcbiAgICAgICAgfVxyXG4gICAgICB9XHJcbiAgICAgIGlmIChzY3JpcHQpIHtcclxuICAgICAgICBzZWxmLl9hYm9ydChuZXcgRXJyb3IoJ0pTT05QIHNjcmlwdCBsb2FkZWQgYWJub3JtYWxseSAob25yZWFkeXN0YXRlY2hhbmdlKScpKTtcclxuICAgICAgfVxyXG4gICAgfVxyXG4gIH07XHJcbiAgLy8gSUU6IGV2ZW50L2h0bWxGb3Ivb25jbGljayB0cmljay5cclxuICAvLyBPbmUgY2FuJ3QgcmVseSBvbiBwcm9wZXIgb3JkZXIgZm9yIG9ucmVhZHlzdGF0ZWNoYW5nZS4gSW4gb3JkZXIgdG9cclxuICAvLyBtYWtlIHN1cmUsIHNldCBhICdodG1sRm9yJyBhbmQgJ2V2ZW50JyBwcm9wZXJ0aWVzLCBzbyB0aGF0XHJcbiAgLy8gc2NyaXB0IGNvZGUgd2lsbCBiZSBpbnN0YWxsZWQgYXMgJ29uY2xpY2snIGhhbmRsZXIgZm9yIHRoZVxyXG4gIC8vIHNjcmlwdCBvYmplY3QuIExhdGVyLCBvbnJlYWR5c3RhdGVjaGFuZ2UsIG1hbnVhbGx5IGV4ZWN1dGUgdGhpc1xyXG4gIC8vIGNvZGUuIEZGIGFuZCBDaHJvbWUgZG9lc24ndCB3b3JrIHdpdGggJ2V2ZW50JyBhbmQgJ2h0bWxGb3InXHJcbiAgLy8gc2V0LiBGb3IgcmVmZXJlbmNlIHNlZTpcclxuICAvLyAgIGh0dHA6Ly9qYXVib3VyZy5uZXQvMjAxMC8wNy9sb2FkaW5nLXNjcmlwdC1hcy1vbmNsaWNrLWhhbmRsZXItb2YuaHRtbFxyXG4gIC8vIEFsc28sIHJlYWQgb24gdGhhdCBhYm91dCBzY3JpcHQgb3JkZXJpbmc6XHJcbiAgLy8gICBodHRwOi8vd2lraS53aGF0d2cub3JnL3dpa2kvRHluYW1pY19TY3JpcHRfRXhlY3V0aW9uX09yZGVyXHJcbiAgaWYgKHR5cGVvZiBzY3JpcHQuYXN5bmMgPT09ICd1bmRlZmluZWQnICYmIGdsb2JhbC5kb2N1bWVudC5hdHRhY2hFdmVudCkge1xyXG4gICAgLy8gQWNjb3JkaW5nIHRvIG1vemlsbGEgZG9jcywgaW4gcmVjZW50IGJyb3dzZXJzIHNjcmlwdC5hc3luYyBkZWZhdWx0c1xyXG4gICAgLy8gdG8gJ3RydWUnLCBzbyB3ZSBtYXkgdXNlIGl0IHRvIGRldGVjdCBhIGdvb2QgYnJvd3NlcjpcclxuICAgIC8vIGh0dHBzOi8vZGV2ZWxvcGVyLm1vemlsbGEub3JnL2VuL0hUTUwvRWxlbWVudC9zY3JpcHRcclxuICAgIGlmICghYnJvd3Nlci5pc09wZXJhKCkpIHtcclxuICAgICAgLy8gTmFpdmVseSBhc3N1bWUgd2UncmUgaW4gSUVcclxuICAgICAgdHJ5IHtcclxuICAgICAgICBzY3JpcHQuaHRtbEZvciA9IHNjcmlwdC5pZDtcclxuICAgICAgICBzY3JpcHQuZXZlbnQgPSAnb25jbGljayc7XHJcbiAgICAgIH0gY2F0Y2ggKHgpIHtcclxuICAgICAgICAvLyBpbnRlbnRpb25hbGx5IGVtcHR5XHJcbiAgICAgIH1cclxuICAgICAgc2NyaXB0LmFzeW5jID0gdHJ1ZTtcclxuICAgIH0gZWxzZSB7XHJcbiAgICAgIC8vIE9wZXJhLCBzZWNvbmQgc3luYyBzY3JpcHQgaGFja1xyXG4gICAgICBzY3JpcHQyID0gdGhpcy5zY3JpcHQyID0gZ2xvYmFsLmRvY3VtZW50LmNyZWF0ZUVsZW1lbnQoJ3NjcmlwdCcpO1xyXG4gICAgICBzY3JpcHQyLnRleHQgPSBcInRyeXt2YXIgYSA9IGRvY3VtZW50LmdldEVsZW1lbnRCeUlkKCdcIiArIHNjcmlwdC5pZCArIFwiJyk7IGlmKGEpYS5vbmVycm9yKCk7fWNhdGNoKHgpe307XCI7XHJcbiAgICAgIHNjcmlwdC5hc3luYyA9IHNjcmlwdDIuYXN5bmMgPSBmYWxzZTtcclxuICAgIH1cclxuICB9XHJcbiAgaWYgKHR5cGVvZiBzY3JpcHQuYXN5bmMgIT09ICd1bmRlZmluZWQnKSB7XHJcbiAgICBzY3JpcHQuYXN5bmMgPSB0cnVlO1xyXG4gIH1cclxuXHJcbiAgdmFyIGhlYWQgPSBnbG9iYWwuZG9jdW1lbnQuZ2V0RWxlbWVudHNCeVRhZ05hbWUoJ2hlYWQnKVswXTtcclxuICBoZWFkLmluc2VydEJlZm9yZShzY3JpcHQsIGhlYWQuZmlyc3RDaGlsZCk7XHJcbiAgaWYgKHNjcmlwdDIpIHtcclxuICAgIGhlYWQuaW5zZXJ0QmVmb3JlKHNjcmlwdDIsIGhlYWQuZmlyc3RDaGlsZCk7XHJcbiAgfVxyXG59O1xyXG5cclxubW9kdWxlLmV4cG9ydHMgPSBKc29ucFJlY2VpdmVyO1xyXG5cclxufSkuY2FsbCh0aGlzLHsgZW52OiB7fSB9LHR5cGVvZiBnbG9iYWwgIT09IFwidW5kZWZpbmVkXCIgPyBnbG9iYWwgOiB0eXBlb2Ygc2VsZiAhPT0gXCJ1bmRlZmluZWRcIiA/IHNlbGYgOiB0eXBlb2Ygd2luZG93ICE9PSBcInVuZGVmaW5lZFwiID8gd2luZG93IDoge30pXHJcblxyXG59LHtcIi4uLy4uL3V0aWxzL2Jyb3dzZXJcIjo0NCxcIi4uLy4uL3V0aWxzL2lmcmFtZVwiOjQ3LFwiLi4vLi4vdXRpbHMvcmFuZG9tXCI6NTAsXCIuLi8uLi91dGlscy91cmxcIjo1MixcImRlYnVnXCI6NTUsXCJldmVudHNcIjozLFwiaW5oZXJpdHNcIjo1N31dLDMyOltmdW5jdGlvbihyZXF1aXJlLG1vZHVsZSxleHBvcnRzKXtcclxuKGZ1bmN0aW9uIChwcm9jZXNzKXtcclxuJ3VzZSBzdHJpY3QnO1xyXG5cclxudmFyIGluaGVyaXRzID0gcmVxdWlyZSgnaW5oZXJpdHMnKVxyXG4gICwgRXZlbnRFbWl0dGVyID0gcmVxdWlyZSgnZXZlbnRzJykuRXZlbnRFbWl0dGVyXHJcbiAgO1xyXG5cclxudmFyIGRlYnVnID0gZnVuY3Rpb24oKSB7fTtcclxuaWYgKHByb2Nlc3MuZW52Lk5PREVfRU5WICE9PSAncHJvZHVjdGlvbicpIHtcclxuICBkZWJ1ZyA9IHJlcXVpcmUoJ2RlYnVnJykoJ3NvY2tqcy1jbGllbnQ6cmVjZWl2ZXI6eGhyJyk7XHJcbn1cclxuXHJcbmZ1bmN0aW9uIFhoclJlY2VpdmVyKHVybCwgQWpheE9iamVjdCkge1xyXG4gIGRlYnVnKHVybCk7XHJcbiAgRXZlbnRFbWl0dGVyLmNhbGwodGhpcyk7XHJcbiAgdmFyIHNlbGYgPSB0aGlzO1xyXG5cclxuICB0aGlzLmJ1ZmZlclBvc2l0aW9uID0gMDtcclxuXHJcbiAgdGhpcy54byA9IG5ldyBBamF4T2JqZWN0KCdQT1NUJywgdXJsLCBudWxsKTtcclxuICB0aGlzLnhvLm9uKCdjaHVuaycsIHRoaXMuX2NodW5rSGFuZGxlci5iaW5kKHRoaXMpKTtcclxuICB0aGlzLnhvLm9uY2UoJ2ZpbmlzaCcsIGZ1bmN0aW9uKHN0YXR1cywgdGV4dCkge1xyXG4gICAgZGVidWcoJ2ZpbmlzaCcsIHN0YXR1cywgdGV4dCk7XHJcbiAgICBzZWxmLl9jaHVua0hhbmRsZXIoc3RhdHVzLCB0ZXh0KTtcclxuICAgIHNlbGYueG8gPSBudWxsO1xyXG4gICAgdmFyIHJlYXNvbiA9IHN0YXR1cyA9PT0gMjAwID8gJ25ldHdvcmsnIDogJ3Blcm1hbmVudCc7XHJcbiAgICBkZWJ1ZygnY2xvc2UnLCByZWFzb24pO1xyXG4gICAgc2VsZi5lbWl0KCdjbG9zZScsIG51bGwsIHJlYXNvbik7XHJcbiAgICBzZWxmLl9jbGVhbnVwKCk7XHJcbiAgfSk7XHJcbn1cclxuXHJcbmluaGVyaXRzKFhoclJlY2VpdmVyLCBFdmVudEVtaXR0ZXIpO1xyXG5cclxuWGhyUmVjZWl2ZXIucHJvdG90eXBlLl9jaHVua0hhbmRsZXIgPSBmdW5jdGlvbihzdGF0dXMsIHRleHQpIHtcclxuICBkZWJ1ZygnX2NodW5rSGFuZGxlcicsIHN0YXR1cyk7XHJcbiAgaWYgKHN0YXR1cyAhPT0gMjAwIHx8ICF0ZXh0KSB7XHJcbiAgICByZXR1cm47XHJcbiAgfVxyXG5cclxuICBmb3IgKHZhciBpZHggPSAtMTsgOyB0aGlzLmJ1ZmZlclBvc2l0aW9uICs9IGlkeCArIDEpIHtcclxuICAgIHZhciBidWYgPSB0ZXh0LnNsaWNlKHRoaXMuYnVmZmVyUG9zaXRpb24pO1xyXG4gICAgaWR4ID0gYnVmLmluZGV4T2YoJ1xcbicpO1xyXG4gICAgaWYgKGlkeCA9PT0gLTEpIHtcclxuICAgICAgYnJlYWs7XHJcbiAgICB9XHJcbiAgICB2YXIgbXNnID0gYnVmLnNsaWNlKDAsIGlkeCk7XHJcbiAgICBpZiAobXNnKSB7XHJcbiAgICAgIGRlYnVnKCdtZXNzYWdlJywgbXNnKTtcclxuICAgICAgdGhpcy5lbWl0KCdtZXNzYWdlJywgbXNnKTtcclxuICAgIH1cclxuICB9XHJcbn07XHJcblxyXG5YaHJSZWNlaXZlci5wcm90b3R5cGUuX2NsZWFudXAgPSBmdW5jdGlvbigpIHtcclxuICBkZWJ1ZygnX2NsZWFudXAnKTtcclxuICB0aGlzLnJlbW92ZUFsbExpc3RlbmVycygpO1xyXG59O1xyXG5cclxuWGhyUmVjZWl2ZXIucHJvdG90eXBlLmFib3J0ID0gZnVuY3Rpb24oKSB7XHJcbiAgZGVidWcoJ2Fib3J0Jyk7XHJcbiAgaWYgKHRoaXMueG8pIHtcclxuICAgIHRoaXMueG8uY2xvc2UoKTtcclxuICAgIGRlYnVnKCdjbG9zZScpO1xyXG4gICAgdGhpcy5lbWl0KCdjbG9zZScsIG51bGwsICd1c2VyJyk7XHJcbiAgICB0aGlzLnhvID0gbnVsbDtcclxuICB9XHJcbiAgdGhpcy5fY2xlYW51cCgpO1xyXG59O1xyXG5cclxubW9kdWxlLmV4cG9ydHMgPSBYaHJSZWNlaXZlcjtcclxuXHJcbn0pLmNhbGwodGhpcyx7IGVudjoge30gfSlcclxuXHJcbn0se1wiZGVidWdcIjo1NSxcImV2ZW50c1wiOjMsXCJpbmhlcml0c1wiOjU3fV0sMzM6W2Z1bmN0aW9uKHJlcXVpcmUsbW9kdWxlLGV4cG9ydHMpe1xyXG4oZnVuY3Rpb24gKHByb2Nlc3MsZ2xvYmFsKXtcclxuJ3VzZSBzdHJpY3QnO1xyXG5cclxudmFyIHJhbmRvbSA9IHJlcXVpcmUoJy4uLy4uL3V0aWxzL3JhbmRvbScpXHJcbiAgLCB1cmxVdGlscyA9IHJlcXVpcmUoJy4uLy4uL3V0aWxzL3VybCcpXHJcbiAgO1xyXG5cclxudmFyIGRlYnVnID0gZnVuY3Rpb24oKSB7fTtcclxuaWYgKHByb2Nlc3MuZW52Lk5PREVfRU5WICE9PSAncHJvZHVjdGlvbicpIHtcclxuICBkZWJ1ZyA9IHJlcXVpcmUoJ2RlYnVnJykoJ3NvY2tqcy1jbGllbnQ6c2VuZGVyOmpzb25wJyk7XHJcbn1cclxuXHJcbnZhciBmb3JtLCBhcmVhO1xyXG5cclxuZnVuY3Rpb24gY3JlYXRlSWZyYW1lKGlkKSB7XHJcbiAgZGVidWcoJ2NyZWF0ZUlmcmFtZScsIGlkKTtcclxuICB0cnkge1xyXG4gICAgLy8gaWU2IGR5bmFtaWMgaWZyYW1lcyB3aXRoIHRhcmdldD1cIlwiIHN1cHBvcnQgKHRoYW5rcyBDaHJpcyBMYW1iYWNoZXIpXHJcbiAgICByZXR1cm4gZ2xvYmFsLmRvY3VtZW50LmNyZWF0ZUVsZW1lbnQoJzxpZnJhbWUgbmFtZT1cIicgKyBpZCArICdcIj4nKTtcclxuICB9IGNhdGNoICh4KSB7XHJcbiAgICB2YXIgaWZyYW1lID0gZ2xvYmFsLmRvY3VtZW50LmNyZWF0ZUVsZW1lbnQoJ2lmcmFtZScpO1xyXG4gICAgaWZyYW1lLm5hbWUgPSBpZDtcclxuICAgIHJldHVybiBpZnJhbWU7XHJcbiAgfVxyXG59XHJcblxyXG5mdW5jdGlvbiBjcmVhdGVGb3JtKCkge1xyXG4gIGRlYnVnKCdjcmVhdGVGb3JtJyk7XHJcbiAgZm9ybSA9IGdsb2JhbC5kb2N1bWVudC5jcmVhdGVFbGVtZW50KCdmb3JtJyk7XHJcbiAgZm9ybS5zdHlsZS5kaXNwbGF5ID0gJ25vbmUnO1xyXG4gIGZvcm0uc3R5bGUucG9zaXRpb24gPSAnYWJzb2x1dGUnO1xyXG4gIGZvcm0ubWV0aG9kID0gJ1BPU1QnO1xyXG4gIGZvcm0uZW5jdHlwZSA9ICdhcHBsaWNhdGlvbi94LXd3dy1mb3JtLXVybGVuY29kZWQnO1xyXG4gIGZvcm0uYWNjZXB0Q2hhcnNldCA9ICdVVEYtOCc7XHJcblxyXG4gIGFyZWEgPSBnbG9iYWwuZG9jdW1lbnQuY3JlYXRlRWxlbWVudCgndGV4dGFyZWEnKTtcclxuICBhcmVhLm5hbWUgPSAnZCc7XHJcbiAgZm9ybS5hcHBlbmRDaGlsZChhcmVhKTtcclxuXHJcbiAgZ2xvYmFsLmRvY3VtZW50LmJvZHkuYXBwZW5kQ2hpbGQoZm9ybSk7XHJcbn1cclxuXHJcbm1vZHVsZS5leHBvcnRzID0gZnVuY3Rpb24odXJsLCBwYXlsb2FkLCBjYWxsYmFjaykge1xyXG4gIGRlYnVnKHVybCwgcGF5bG9hZCk7XHJcbiAgaWYgKCFmb3JtKSB7XHJcbiAgICBjcmVhdGVGb3JtKCk7XHJcbiAgfVxyXG4gIHZhciBpZCA9ICdhJyArIHJhbmRvbS5zdHJpbmcoOCk7XHJcbiAgZm9ybS50YXJnZXQgPSBpZDtcclxuICBmb3JtLmFjdGlvbiA9IHVybFV0aWxzLmFkZFF1ZXJ5KHVybFV0aWxzLmFkZFBhdGgodXJsLCAnL2pzb25wX3NlbmQnKSwgJ2k9JyArIGlkKTtcclxuXHJcbiAgdmFyIGlmcmFtZSA9IGNyZWF0ZUlmcmFtZShpZCk7XHJcbiAgaWZyYW1lLmlkID0gaWQ7XHJcbiAgaWZyYW1lLnN0eWxlLmRpc3BsYXkgPSAnbm9uZSc7XHJcbiAgZm9ybS5hcHBlbmRDaGlsZChpZnJhbWUpO1xyXG5cclxuICB0cnkge1xyXG4gICAgYXJlYS52YWx1ZSA9IHBheWxvYWQ7XHJcbiAgfSBjYXRjaCAoZSkge1xyXG4gICAgLy8gc2VyaW91c2x5IGJyb2tlbiBicm93c2VycyBnZXQgaGVyZVxyXG4gIH1cclxuICBmb3JtLnN1Ym1pdCgpO1xyXG5cclxuICB2YXIgY29tcGxldGVkID0gZnVuY3Rpb24oZXJyKSB7XHJcbiAgICBkZWJ1ZygnY29tcGxldGVkJywgaWQsIGVycik7XHJcbiAgICBpZiAoIWlmcmFtZS5vbmVycm9yKSB7XHJcbiAgICAgIHJldHVybjtcclxuICAgIH1cclxuICAgIGlmcmFtZS5vbnJlYWR5c3RhdGVjaGFuZ2UgPSBpZnJhbWUub25lcnJvciA9IGlmcmFtZS5vbmxvYWQgPSBudWxsO1xyXG4gICAgLy8gT3BlcmEgbWluaSBkb2Vzbid0IGxpa2UgaWYgd2UgR0MgaWZyYW1lXHJcbiAgICAvLyBpbW1lZGlhdGVseSwgdGh1cyB0aGlzIHRpbWVvdXQuXHJcbiAgICBzZXRUaW1lb3V0KGZ1bmN0aW9uKCkge1xyXG4gICAgICBkZWJ1ZygnY2xlYW5pbmcgdXAnLCBpZCk7XHJcbiAgICAgIGlmcmFtZS5wYXJlbnROb2RlLnJlbW92ZUNoaWxkKGlmcmFtZSk7XHJcbiAgICAgIGlmcmFtZSA9IG51bGw7XHJcbiAgICB9LCA1MDApO1xyXG4gICAgYXJlYS52YWx1ZSA9ICcnO1xyXG4gICAgLy8gSXQgaXMgbm90IHBvc3NpYmxlIHRvIGRldGVjdCBpZiB0aGUgaWZyYW1lIHN1Y2NlZWRlZCBvclxyXG4gICAgLy8gZmFpbGVkIHRvIHN1Ym1pdCBvdXIgZm9ybS5cclxuICAgIGNhbGxiYWNrKGVycik7XHJcbiAgfTtcclxuICBpZnJhbWUub25lcnJvciA9IGZ1bmN0aW9uKCkge1xyXG4gICAgZGVidWcoJ29uZXJyb3InLCBpZCk7XHJcbiAgICBjb21wbGV0ZWQoKTtcclxuICB9O1xyXG4gIGlmcmFtZS5vbmxvYWQgPSBmdW5jdGlvbigpIHtcclxuICAgIGRlYnVnKCdvbmxvYWQnLCBpZCk7XHJcbiAgICBjb21wbGV0ZWQoKTtcclxuICB9O1xyXG4gIGlmcmFtZS5vbnJlYWR5c3RhdGVjaGFuZ2UgPSBmdW5jdGlvbihlKSB7XHJcbiAgICBkZWJ1Zygnb25yZWFkeXN0YXRlY2hhbmdlJywgaWQsIGlmcmFtZS5yZWFkeVN0YXRlLCBlKTtcclxuICAgIGlmIChpZnJhbWUucmVhZHlTdGF0ZSA9PT0gJ2NvbXBsZXRlJykge1xyXG4gICAgICBjb21wbGV0ZWQoKTtcclxuICAgIH1cclxuICB9O1xyXG4gIHJldHVybiBmdW5jdGlvbigpIHtcclxuICAgIGRlYnVnKCdhYm9ydGVkJywgaWQpO1xyXG4gICAgY29tcGxldGVkKG5ldyBFcnJvcignQWJvcnRlZCcpKTtcclxuICB9O1xyXG59O1xyXG5cclxufSkuY2FsbCh0aGlzLHsgZW52OiB7fSB9LHR5cGVvZiBnbG9iYWwgIT09IFwidW5kZWZpbmVkXCIgPyBnbG9iYWwgOiB0eXBlb2Ygc2VsZiAhPT0gXCJ1bmRlZmluZWRcIiA/IHNlbGYgOiB0eXBlb2Ygd2luZG93ICE9PSBcInVuZGVmaW5lZFwiID8gd2luZG93IDoge30pXHJcblxyXG59LHtcIi4uLy4uL3V0aWxzL3JhbmRvbVwiOjUwLFwiLi4vLi4vdXRpbHMvdXJsXCI6NTIsXCJkZWJ1Z1wiOjU1fV0sMzQ6W2Z1bmN0aW9uKHJlcXVpcmUsbW9kdWxlLGV4cG9ydHMpe1xyXG4oZnVuY3Rpb24gKHByb2Nlc3MsZ2xvYmFsKXtcclxuJ3VzZSBzdHJpY3QnO1xyXG5cclxudmFyIEV2ZW50RW1pdHRlciA9IHJlcXVpcmUoJ2V2ZW50cycpLkV2ZW50RW1pdHRlclxyXG4gICwgaW5oZXJpdHMgPSByZXF1aXJlKCdpbmhlcml0cycpXHJcbiAgLCBldmVudFV0aWxzID0gcmVxdWlyZSgnLi4vLi4vdXRpbHMvZXZlbnQnKVxyXG4gICwgYnJvd3NlciA9IHJlcXVpcmUoJy4uLy4uL3V0aWxzL2Jyb3dzZXInKVxyXG4gICwgdXJsVXRpbHMgPSByZXF1aXJlKCcuLi8uLi91dGlscy91cmwnKVxyXG4gIDtcclxuXHJcbnZhciBkZWJ1ZyA9IGZ1bmN0aW9uKCkge307XHJcbmlmIChwcm9jZXNzLmVudi5OT0RFX0VOViAhPT0gJ3Byb2R1Y3Rpb24nKSB7XHJcbiAgZGVidWcgPSByZXF1aXJlKCdkZWJ1ZycpKCdzb2NranMtY2xpZW50OnNlbmRlcjp4ZHInKTtcclxufVxyXG5cclxuLy8gUmVmZXJlbmNlczpcclxuLy8gICBodHRwOi8vYWpheGlhbi5jb20vYXJjaGl2ZXMvMTAwLWxpbmUtYWpheC13cmFwcGVyXHJcbi8vICAgaHR0cDovL21zZG4ubWljcm9zb2Z0LmNvbS9lbi11cy9saWJyYXJ5L2NjMjg4MDYwKHY9VlMuODUpLmFzcHhcclxuXHJcbmZ1bmN0aW9uIFhEUk9iamVjdChtZXRob2QsIHVybCwgcGF5bG9hZCkge1xyXG4gIGRlYnVnKG1ldGhvZCwgdXJsKTtcclxuICB2YXIgc2VsZiA9IHRoaXM7XHJcbiAgRXZlbnRFbWl0dGVyLmNhbGwodGhpcyk7XHJcblxyXG4gIHNldFRpbWVvdXQoZnVuY3Rpb24oKSB7XHJcbiAgICBzZWxmLl9zdGFydChtZXRob2QsIHVybCwgcGF5bG9hZCk7XHJcbiAgfSwgMCk7XHJcbn1cclxuXHJcbmluaGVyaXRzKFhEUk9iamVjdCwgRXZlbnRFbWl0dGVyKTtcclxuXHJcblhEUk9iamVjdC5wcm90b3R5cGUuX3N0YXJ0ID0gZnVuY3Rpb24obWV0aG9kLCB1cmwsIHBheWxvYWQpIHtcclxuICBkZWJ1ZygnX3N0YXJ0Jyk7XHJcbiAgdmFyIHNlbGYgPSB0aGlzO1xyXG4gIHZhciB4ZHIgPSBuZXcgZ2xvYmFsLlhEb21haW5SZXF1ZXN0KCk7XHJcbiAgLy8gSUUgY2FjaGVzIGV2ZW4gUE9TVHNcclxuICB1cmwgPSB1cmxVdGlscy5hZGRRdWVyeSh1cmwsICd0PScgKyAoK25ldyBEYXRlKCkpKTtcclxuXHJcbiAgeGRyLm9uZXJyb3IgPSBmdW5jdGlvbigpIHtcclxuICAgIGRlYnVnKCdvbmVycm9yJyk7XHJcbiAgICBzZWxmLl9lcnJvcigpO1xyXG4gIH07XHJcbiAgeGRyLm9udGltZW91dCA9IGZ1bmN0aW9uKCkge1xyXG4gICAgZGVidWcoJ29udGltZW91dCcpO1xyXG4gICAgc2VsZi5fZXJyb3IoKTtcclxuICB9O1xyXG4gIHhkci5vbnByb2dyZXNzID0gZnVuY3Rpb24oKSB7XHJcbiAgICBkZWJ1ZygncHJvZ3Jlc3MnLCB4ZHIucmVzcG9uc2VUZXh0KTtcclxuICAgIHNlbGYuZW1pdCgnY2h1bmsnLCAyMDAsIHhkci5yZXNwb25zZVRleHQpO1xyXG4gIH07XHJcbiAgeGRyLm9ubG9hZCA9IGZ1bmN0aW9uKCkge1xyXG4gICAgZGVidWcoJ2xvYWQnKTtcclxuICAgIHNlbGYuZW1pdCgnZmluaXNoJywgMjAwLCB4ZHIucmVzcG9uc2VUZXh0KTtcclxuICAgIHNlbGYuX2NsZWFudXAoZmFsc2UpO1xyXG4gIH07XHJcbiAgdGhpcy54ZHIgPSB4ZHI7XHJcbiAgdGhpcy51bmxvYWRSZWYgPSBldmVudFV0aWxzLnVubG9hZEFkZChmdW5jdGlvbigpIHtcclxuICAgIHNlbGYuX2NsZWFudXAodHJ1ZSk7XHJcbiAgfSk7XHJcbiAgdHJ5IHtcclxuICAgIC8vIEZhaWxzIHdpdGggQWNjZXNzRGVuaWVkIGlmIHBvcnQgbnVtYmVyIGlzIGJvZ3VzXHJcbiAgICB0aGlzLnhkci5vcGVuKG1ldGhvZCwgdXJsKTtcclxuICAgIGlmICh0aGlzLnRpbWVvdXQpIHtcclxuICAgICAgdGhpcy54ZHIudGltZW91dCA9IHRoaXMudGltZW91dDtcclxuICAgIH1cclxuICAgIHRoaXMueGRyLnNlbmQocGF5bG9hZCk7XHJcbiAgfSBjYXRjaCAoeCkge1xyXG4gICAgdGhpcy5fZXJyb3IoKTtcclxuICB9XHJcbn07XHJcblxyXG5YRFJPYmplY3QucHJvdG90eXBlLl9lcnJvciA9IGZ1bmN0aW9uKCkge1xyXG4gIHRoaXMuZW1pdCgnZmluaXNoJywgMCwgJycpO1xyXG4gIHRoaXMuX2NsZWFudXAoZmFsc2UpO1xyXG59O1xyXG5cclxuWERST2JqZWN0LnByb3RvdHlwZS5fY2xlYW51cCA9IGZ1bmN0aW9uKGFib3J0KSB7XHJcbiAgZGVidWcoJ2NsZWFudXAnLCBhYm9ydCk7XHJcbiAgaWYgKCF0aGlzLnhkcikge1xyXG4gICAgcmV0dXJuO1xyXG4gIH1cclxuICB0aGlzLnJlbW92ZUFsbExpc3RlbmVycygpO1xyXG4gIGV2ZW50VXRpbHMudW5sb2FkRGVsKHRoaXMudW5sb2FkUmVmKTtcclxuXHJcbiAgdGhpcy54ZHIub250aW1lb3V0ID0gdGhpcy54ZHIub25lcnJvciA9IHRoaXMueGRyLm9ucHJvZ3Jlc3MgPSB0aGlzLnhkci5vbmxvYWQgPSBudWxsO1xyXG4gIGlmIChhYm9ydCkge1xyXG4gICAgdHJ5IHtcclxuICAgICAgdGhpcy54ZHIuYWJvcnQoKTtcclxuICAgIH0gY2F0Y2ggKHgpIHtcclxuICAgICAgLy8gaW50ZW50aW9uYWxseSBlbXB0eVxyXG4gICAgfVxyXG4gIH1cclxuICB0aGlzLnVubG9hZFJlZiA9IHRoaXMueGRyID0gbnVsbDtcclxufTtcclxuXHJcblhEUk9iamVjdC5wcm90b3R5cGUuY2xvc2UgPSBmdW5jdGlvbigpIHtcclxuICBkZWJ1ZygnY2xvc2UnKTtcclxuICB0aGlzLl9jbGVhbnVwKHRydWUpO1xyXG59O1xyXG5cclxuLy8gSUUgOC85IGlmIHRoZSByZXF1ZXN0IHRhcmdldCB1c2VzIHRoZSBzYW1lIHNjaGVtZSAtICM3OVxyXG5YRFJPYmplY3QuZW5hYmxlZCA9ICEhKGdsb2JhbC5YRG9tYWluUmVxdWVzdCAmJiBicm93c2VyLmhhc0RvbWFpbigpKTtcclxuXHJcbm1vZHVsZS5leHBvcnRzID0gWERST2JqZWN0O1xyXG5cclxufSkuY2FsbCh0aGlzLHsgZW52OiB7fSB9LHR5cGVvZiBnbG9iYWwgIT09IFwidW5kZWZpbmVkXCIgPyBnbG9iYWwgOiB0eXBlb2Ygc2VsZiAhPT0gXCJ1bmRlZmluZWRcIiA/IHNlbGYgOiB0eXBlb2Ygd2luZG93ICE9PSBcInVuZGVmaW5lZFwiID8gd2luZG93IDoge30pXHJcblxyXG59LHtcIi4uLy4uL3V0aWxzL2Jyb3dzZXJcIjo0NCxcIi4uLy4uL3V0aWxzL2V2ZW50XCI6NDYsXCIuLi8uLi91dGlscy91cmxcIjo1MixcImRlYnVnXCI6NTUsXCJldmVudHNcIjozLFwiaW5oZXJpdHNcIjo1N31dLDM1OltmdW5jdGlvbihyZXF1aXJlLG1vZHVsZSxleHBvcnRzKXtcclxuJ3VzZSBzdHJpY3QnO1xyXG5cclxudmFyIGluaGVyaXRzID0gcmVxdWlyZSgnaW5oZXJpdHMnKVxyXG4gICwgWGhyRHJpdmVyID0gcmVxdWlyZSgnLi4vZHJpdmVyL3hocicpXHJcbiAgO1xyXG5cclxuZnVuY3Rpb24gWEhSQ29yc09iamVjdChtZXRob2QsIHVybCwgcGF5bG9hZCwgb3B0cykge1xyXG4gIFhockRyaXZlci5jYWxsKHRoaXMsIG1ldGhvZCwgdXJsLCBwYXlsb2FkLCBvcHRzKTtcclxufVxyXG5cclxuaW5oZXJpdHMoWEhSQ29yc09iamVjdCwgWGhyRHJpdmVyKTtcclxuXHJcblhIUkNvcnNPYmplY3QuZW5hYmxlZCA9IFhockRyaXZlci5lbmFibGVkICYmIFhockRyaXZlci5zdXBwb3J0c0NPUlM7XHJcblxyXG5tb2R1bGUuZXhwb3J0cyA9IFhIUkNvcnNPYmplY3Q7XHJcblxyXG59LHtcIi4uL2RyaXZlci94aHJcIjoxNyxcImluaGVyaXRzXCI6NTd9XSwzNjpbZnVuY3Rpb24ocmVxdWlyZSxtb2R1bGUsZXhwb3J0cyl7XHJcbid1c2Ugc3RyaWN0JztcclxuXHJcbnZhciBFdmVudEVtaXR0ZXIgPSByZXF1aXJlKCdldmVudHMnKS5FdmVudEVtaXR0ZXJcclxuICAsIGluaGVyaXRzID0gcmVxdWlyZSgnaW5oZXJpdHMnKVxyXG4gIDtcclxuXHJcbmZ1bmN0aW9uIFhIUkZha2UoLyogbWV0aG9kLCB1cmwsIHBheWxvYWQsIG9wdHMgKi8pIHtcclxuICB2YXIgc2VsZiA9IHRoaXM7XHJcbiAgRXZlbnRFbWl0dGVyLmNhbGwodGhpcyk7XHJcblxyXG4gIHRoaXMudG8gPSBzZXRUaW1lb3V0KGZ1bmN0aW9uKCkge1xyXG4gICAgc2VsZi5lbWl0KCdmaW5pc2gnLCAyMDAsICd7fScpO1xyXG4gIH0sIFhIUkZha2UudGltZW91dCk7XHJcbn1cclxuXHJcbmluaGVyaXRzKFhIUkZha2UsIEV2ZW50RW1pdHRlcik7XHJcblxyXG5YSFJGYWtlLnByb3RvdHlwZS5jbG9zZSA9IGZ1bmN0aW9uKCkge1xyXG4gIGNsZWFyVGltZW91dCh0aGlzLnRvKTtcclxufTtcclxuXHJcblhIUkZha2UudGltZW91dCA9IDIwMDA7XHJcblxyXG5tb2R1bGUuZXhwb3J0cyA9IFhIUkZha2U7XHJcblxyXG59LHtcImV2ZW50c1wiOjMsXCJpbmhlcml0c1wiOjU3fV0sMzc6W2Z1bmN0aW9uKHJlcXVpcmUsbW9kdWxlLGV4cG9ydHMpe1xyXG4ndXNlIHN0cmljdCc7XHJcblxyXG52YXIgaW5oZXJpdHMgPSByZXF1aXJlKCdpbmhlcml0cycpXHJcbiAgLCBYaHJEcml2ZXIgPSByZXF1aXJlKCcuLi9kcml2ZXIveGhyJylcclxuICA7XHJcblxyXG5mdW5jdGlvbiBYSFJMb2NhbE9iamVjdChtZXRob2QsIHVybCwgcGF5bG9hZCAvKiwgb3B0cyAqLykge1xyXG4gIFhockRyaXZlci5jYWxsKHRoaXMsIG1ldGhvZCwgdXJsLCBwYXlsb2FkLCB7XHJcbiAgICBub0NyZWRlbnRpYWxzOiB0cnVlXHJcbiAgfSk7XHJcbn1cclxuXHJcbmluaGVyaXRzKFhIUkxvY2FsT2JqZWN0LCBYaHJEcml2ZXIpO1xyXG5cclxuWEhSTG9jYWxPYmplY3QuZW5hYmxlZCA9IFhockRyaXZlci5lbmFibGVkO1xyXG5cclxubW9kdWxlLmV4cG9ydHMgPSBYSFJMb2NhbE9iamVjdDtcclxuXHJcbn0se1wiLi4vZHJpdmVyL3hoclwiOjE3LFwiaW5oZXJpdHNcIjo1N31dLDM4OltmdW5jdGlvbihyZXF1aXJlLG1vZHVsZSxleHBvcnRzKXtcclxuKGZ1bmN0aW9uIChwcm9jZXNzKXtcclxuJ3VzZSBzdHJpY3QnO1xyXG5cclxudmFyIHV0aWxzID0gcmVxdWlyZSgnLi4vdXRpbHMvZXZlbnQnKVxyXG4gICwgdXJsVXRpbHMgPSByZXF1aXJlKCcuLi91dGlscy91cmwnKVxyXG4gICwgaW5oZXJpdHMgPSByZXF1aXJlKCdpbmhlcml0cycpXHJcbiAgLCBFdmVudEVtaXR0ZXIgPSByZXF1aXJlKCdldmVudHMnKS5FdmVudEVtaXR0ZXJcclxuICAsIFdlYnNvY2tldERyaXZlciA9IHJlcXVpcmUoJy4vZHJpdmVyL3dlYnNvY2tldCcpXHJcbiAgO1xyXG5cclxudmFyIGRlYnVnID0gZnVuY3Rpb24oKSB7fTtcclxuaWYgKHByb2Nlc3MuZW52Lk5PREVfRU5WICE9PSAncHJvZHVjdGlvbicpIHtcclxuICBkZWJ1ZyA9IHJlcXVpcmUoJ2RlYnVnJykoJ3NvY2tqcy1jbGllbnQ6d2Vic29ja2V0Jyk7XHJcbn1cclxuXHJcbmZ1bmN0aW9uIFdlYlNvY2tldFRyYW5zcG9ydCh0cmFuc1VybCwgaWdub3JlLCBvcHRpb25zKSB7XHJcbiAgaWYgKCFXZWJTb2NrZXRUcmFuc3BvcnQuZW5hYmxlZCgpKSB7XHJcbiAgICB0aHJvdyBuZXcgRXJyb3IoJ1RyYW5zcG9ydCBjcmVhdGVkIHdoZW4gZGlzYWJsZWQnKTtcclxuICB9XHJcblxyXG4gIEV2ZW50RW1pdHRlci5jYWxsKHRoaXMpO1xyXG4gIGRlYnVnKCdjb25zdHJ1Y3RvcicsIHRyYW5zVXJsKTtcclxuXHJcbiAgdmFyIHNlbGYgPSB0aGlzO1xyXG4gIHZhciB1cmwgPSB1cmxVdGlscy5hZGRQYXRoKHRyYW5zVXJsLCAnL3dlYnNvY2tldCcpO1xyXG4gIGlmICh1cmwuc2xpY2UoMCwgNSkgPT09ICdodHRwcycpIHtcclxuICAgIHVybCA9ICd3c3MnICsgdXJsLnNsaWNlKDUpO1xyXG4gIH0gZWxzZSB7XHJcbiAgICB1cmwgPSAnd3MnICsgdXJsLnNsaWNlKDQpO1xyXG4gIH1cclxuICB0aGlzLnVybCA9IHVybDtcclxuXHJcbiAgdGhpcy53cyA9IG5ldyBXZWJzb2NrZXREcml2ZXIodGhpcy51cmwsIFtdLCBvcHRpb25zKTtcclxuICB0aGlzLndzLm9ubWVzc2FnZSA9IGZ1bmN0aW9uKGUpIHtcclxuICAgIGRlYnVnKCdtZXNzYWdlIGV2ZW50JywgZS5kYXRhKTtcclxuICAgIHNlbGYuZW1pdCgnbWVzc2FnZScsIGUuZGF0YSk7XHJcbiAgfTtcclxuICAvLyBGaXJlZm94IGhhcyBhbiBpbnRlcmVzdGluZyBidWcuIElmIGEgd2Vic29ja2V0IGNvbm5lY3Rpb24gaXNcclxuICAvLyBjcmVhdGVkIGFmdGVyIG9udW5sb2FkLCBpdCBzdGF5cyBhbGl2ZSBldmVuIHdoZW4gdXNlclxyXG4gIC8vIG5hdmlnYXRlcyBhd2F5IGZyb20gdGhlIHBhZ2UuIEluIHN1Y2ggc2l0dWF0aW9uIGxldCdzIGxpZSAtXHJcbiAgLy8gbGV0J3Mgbm90IG9wZW4gdGhlIHdzIGNvbm5lY3Rpb24gYXQgYWxsLiBTZWU6XHJcbiAgLy8gaHR0cHM6Ly9naXRodWIuY29tL3NvY2tqcy9zb2NranMtY2xpZW50L2lzc3Vlcy8yOFxyXG4gIC8vIGh0dHBzOi8vYnVnemlsbGEubW96aWxsYS5vcmcvc2hvd19idWcuY2dpP2lkPTY5NjA4NVxyXG4gIHRoaXMudW5sb2FkUmVmID0gdXRpbHMudW5sb2FkQWRkKGZ1bmN0aW9uKCkge1xyXG4gICAgZGVidWcoJ3VubG9hZCcpO1xyXG4gICAgc2VsZi53cy5jbG9zZSgpO1xyXG4gIH0pO1xyXG4gIHRoaXMud3Mub25jbG9zZSA9IGZ1bmN0aW9uKGUpIHtcclxuICAgIGRlYnVnKCdjbG9zZSBldmVudCcsIGUuY29kZSwgZS5yZWFzb24pO1xyXG4gICAgc2VsZi5lbWl0KCdjbG9zZScsIGUuY29kZSwgZS5yZWFzb24pO1xyXG4gICAgc2VsZi5fY2xlYW51cCgpO1xyXG4gIH07XHJcbiAgdGhpcy53cy5vbmVycm9yID0gZnVuY3Rpb24oZSkge1xyXG4gICAgZGVidWcoJ2Vycm9yIGV2ZW50JywgZSk7XHJcbiAgICBzZWxmLmVtaXQoJ2Nsb3NlJywgMTAwNiwgJ1dlYlNvY2tldCBjb25uZWN0aW9uIGJyb2tlbicpO1xyXG4gICAgc2VsZi5fY2xlYW51cCgpO1xyXG4gIH07XHJcbn1cclxuXHJcbmluaGVyaXRzKFdlYlNvY2tldFRyYW5zcG9ydCwgRXZlbnRFbWl0dGVyKTtcclxuXHJcbldlYlNvY2tldFRyYW5zcG9ydC5wcm90b3R5cGUuc2VuZCA9IGZ1bmN0aW9uKGRhdGEpIHtcclxuICB2YXIgbXNnID0gJ1snICsgZGF0YSArICddJztcclxuICBkZWJ1Zygnc2VuZCcsIG1zZyk7XHJcbiAgdGhpcy53cy5zZW5kKG1zZyk7XHJcbn07XHJcblxyXG5XZWJTb2NrZXRUcmFuc3BvcnQucHJvdG90eXBlLmNsb3NlID0gZnVuY3Rpb24oKSB7XHJcbiAgZGVidWcoJ2Nsb3NlJyk7XHJcbiAgdmFyIHdzID0gdGhpcy53cztcclxuICB0aGlzLl9jbGVhbnVwKCk7XHJcbiAgaWYgKHdzKSB7XHJcbiAgICB3cy5jbG9zZSgpO1xyXG4gIH1cclxufTtcclxuXHJcbldlYlNvY2tldFRyYW5zcG9ydC5wcm90b3R5cGUuX2NsZWFudXAgPSBmdW5jdGlvbigpIHtcclxuICBkZWJ1ZygnX2NsZWFudXAnKTtcclxuICB2YXIgd3MgPSB0aGlzLndzO1xyXG4gIGlmICh3cykge1xyXG4gICAgd3Mub25tZXNzYWdlID0gd3Mub25jbG9zZSA9IHdzLm9uZXJyb3IgPSBudWxsO1xyXG4gIH1cclxuICB1dGlscy51bmxvYWREZWwodGhpcy51bmxvYWRSZWYpO1xyXG4gIHRoaXMudW5sb2FkUmVmID0gdGhpcy53cyA9IG51bGw7XHJcbiAgdGhpcy5yZW1vdmVBbGxMaXN0ZW5lcnMoKTtcclxufTtcclxuXHJcbldlYlNvY2tldFRyYW5zcG9ydC5lbmFibGVkID0gZnVuY3Rpb24oKSB7XHJcbiAgZGVidWcoJ2VuYWJsZWQnKTtcclxuICByZXR1cm4gISFXZWJzb2NrZXREcml2ZXI7XHJcbn07XHJcbldlYlNvY2tldFRyYW5zcG9ydC50cmFuc3BvcnROYW1lID0gJ3dlYnNvY2tldCc7XHJcblxyXG4vLyBJbiB0aGVvcnksIHdzIHNob3VsZCByZXF1aXJlIDEgcm91bmQgdHJpcC4gQnV0IGluIGNocm9tZSwgdGhpcyBpc1xyXG4vLyBub3QgdmVyeSBzdGFibGUgb3ZlciBTU0wuIE1vc3QgbGlrZWx5IGEgd3MgY29ubmVjdGlvbiByZXF1aXJlcyBhXHJcbi8vIHNlcGFyYXRlIFNTTCBjb25uZWN0aW9uLCBpbiB3aGljaCBjYXNlIDIgcm91bmQgdHJpcHMgYXJlIGFuXHJcbi8vIGFic29sdXRlIG1pbnVtdW0uXHJcbldlYlNvY2tldFRyYW5zcG9ydC5yb3VuZFRyaXBzID0gMjtcclxuXHJcbm1vZHVsZS5leHBvcnRzID0gV2ViU29ja2V0VHJhbnNwb3J0O1xyXG5cclxufSkuY2FsbCh0aGlzLHsgZW52OiB7fSB9KVxyXG5cclxufSx7XCIuLi91dGlscy9ldmVudFwiOjQ2LFwiLi4vdXRpbHMvdXJsXCI6NTIsXCIuL2RyaXZlci93ZWJzb2NrZXRcIjoxOSxcImRlYnVnXCI6NTUsXCJldmVudHNcIjozLFwiaW5oZXJpdHNcIjo1N31dLDM5OltmdW5jdGlvbihyZXF1aXJlLG1vZHVsZSxleHBvcnRzKXtcclxuJ3VzZSBzdHJpY3QnO1xyXG5cclxudmFyIGluaGVyaXRzID0gcmVxdWlyZSgnaW5oZXJpdHMnKVxyXG4gICwgQWpheEJhc2VkVHJhbnNwb3J0ID0gcmVxdWlyZSgnLi9saWIvYWpheC1iYXNlZCcpXHJcbiAgLCBYZHJTdHJlYW1pbmdUcmFuc3BvcnQgPSByZXF1aXJlKCcuL3hkci1zdHJlYW1pbmcnKVxyXG4gICwgWGhyUmVjZWl2ZXIgPSByZXF1aXJlKCcuL3JlY2VpdmVyL3hocicpXHJcbiAgLCBYRFJPYmplY3QgPSByZXF1aXJlKCcuL3NlbmRlci94ZHInKVxyXG4gIDtcclxuXHJcbmZ1bmN0aW9uIFhkclBvbGxpbmdUcmFuc3BvcnQodHJhbnNVcmwpIHtcclxuICBpZiAoIVhEUk9iamVjdC5lbmFibGVkKSB7XHJcbiAgICB0aHJvdyBuZXcgRXJyb3IoJ1RyYW5zcG9ydCBjcmVhdGVkIHdoZW4gZGlzYWJsZWQnKTtcclxuICB9XHJcbiAgQWpheEJhc2VkVHJhbnNwb3J0LmNhbGwodGhpcywgdHJhbnNVcmwsICcveGhyJywgWGhyUmVjZWl2ZXIsIFhEUk9iamVjdCk7XHJcbn1cclxuXHJcbmluaGVyaXRzKFhkclBvbGxpbmdUcmFuc3BvcnQsIEFqYXhCYXNlZFRyYW5zcG9ydCk7XHJcblxyXG5YZHJQb2xsaW5nVHJhbnNwb3J0LmVuYWJsZWQgPSBYZHJTdHJlYW1pbmdUcmFuc3BvcnQuZW5hYmxlZDtcclxuWGRyUG9sbGluZ1RyYW5zcG9ydC50cmFuc3BvcnROYW1lID0gJ3hkci1wb2xsaW5nJztcclxuWGRyUG9sbGluZ1RyYW5zcG9ydC5yb3VuZFRyaXBzID0gMjsgLy8gcHJlZmxpZ2h0LCBhamF4XHJcblxyXG5tb2R1bGUuZXhwb3J0cyA9IFhkclBvbGxpbmdUcmFuc3BvcnQ7XHJcblxyXG59LHtcIi4vbGliL2FqYXgtYmFzZWRcIjoyNCxcIi4vcmVjZWl2ZXIveGhyXCI6MzIsXCIuL3NlbmRlci94ZHJcIjozNCxcIi4veGRyLXN0cmVhbWluZ1wiOjQwLFwiaW5oZXJpdHNcIjo1N31dLDQwOltmdW5jdGlvbihyZXF1aXJlLG1vZHVsZSxleHBvcnRzKXtcclxuJ3VzZSBzdHJpY3QnO1xyXG5cclxudmFyIGluaGVyaXRzID0gcmVxdWlyZSgnaW5oZXJpdHMnKVxyXG4gICwgQWpheEJhc2VkVHJhbnNwb3J0ID0gcmVxdWlyZSgnLi9saWIvYWpheC1iYXNlZCcpXHJcbiAgLCBYaHJSZWNlaXZlciA9IHJlcXVpcmUoJy4vcmVjZWl2ZXIveGhyJylcclxuICAsIFhEUk9iamVjdCA9IHJlcXVpcmUoJy4vc2VuZGVyL3hkcicpXHJcbiAgO1xyXG5cclxuLy8gQWNjb3JkaW5nIHRvOlxyXG4vLyAgIGh0dHA6Ly9zdGFja292ZXJmbG93LmNvbS9xdWVzdGlvbnMvMTY0MTUwNy9kZXRlY3QtYnJvd3Nlci1zdXBwb3J0LWZvci1jcm9zcy1kb21haW4teG1saHR0cHJlcXVlc3RzXHJcbi8vICAgaHR0cDovL2hhY2tzLm1vemlsbGEub3JnLzIwMDkvMDcvY3Jvc3Mtc2l0ZS14bWxodHRwcmVxdWVzdC13aXRoLWNvcnMvXHJcblxyXG5mdW5jdGlvbiBYZHJTdHJlYW1pbmdUcmFuc3BvcnQodHJhbnNVcmwpIHtcclxuICBpZiAoIVhEUk9iamVjdC5lbmFibGVkKSB7XHJcbiAgICB0aHJvdyBuZXcgRXJyb3IoJ1RyYW5zcG9ydCBjcmVhdGVkIHdoZW4gZGlzYWJsZWQnKTtcclxuICB9XHJcbiAgQWpheEJhc2VkVHJhbnNwb3J0LmNhbGwodGhpcywgdHJhbnNVcmwsICcveGhyX3N0cmVhbWluZycsIFhoclJlY2VpdmVyLCBYRFJPYmplY3QpO1xyXG59XHJcblxyXG5pbmhlcml0cyhYZHJTdHJlYW1pbmdUcmFuc3BvcnQsIEFqYXhCYXNlZFRyYW5zcG9ydCk7XHJcblxyXG5YZHJTdHJlYW1pbmdUcmFuc3BvcnQuZW5hYmxlZCA9IGZ1bmN0aW9uKGluZm8pIHtcclxuICBpZiAoaW5mby5jb29raWVfbmVlZGVkIHx8IGluZm8ubnVsbE9yaWdpbikge1xyXG4gICAgcmV0dXJuIGZhbHNlO1xyXG4gIH1cclxuICByZXR1cm4gWERST2JqZWN0LmVuYWJsZWQgJiYgaW5mby5zYW1lU2NoZW1lO1xyXG59O1xyXG5cclxuWGRyU3RyZWFtaW5nVHJhbnNwb3J0LnRyYW5zcG9ydE5hbWUgPSAneGRyLXN0cmVhbWluZyc7XHJcblhkclN0cmVhbWluZ1RyYW5zcG9ydC5yb3VuZFRyaXBzID0gMjsgLy8gcHJlZmxpZ2h0LCBhamF4XHJcblxyXG5tb2R1bGUuZXhwb3J0cyA9IFhkclN0cmVhbWluZ1RyYW5zcG9ydDtcclxuXHJcbn0se1wiLi9saWIvYWpheC1iYXNlZFwiOjI0LFwiLi9yZWNlaXZlci94aHJcIjozMixcIi4vc2VuZGVyL3hkclwiOjM0LFwiaW5oZXJpdHNcIjo1N31dLDQxOltmdW5jdGlvbihyZXF1aXJlLG1vZHVsZSxleHBvcnRzKXtcclxuJ3VzZSBzdHJpY3QnO1xyXG5cclxudmFyIGluaGVyaXRzID0gcmVxdWlyZSgnaW5oZXJpdHMnKVxyXG4gICwgQWpheEJhc2VkVHJhbnNwb3J0ID0gcmVxdWlyZSgnLi9saWIvYWpheC1iYXNlZCcpXHJcbiAgLCBYaHJSZWNlaXZlciA9IHJlcXVpcmUoJy4vcmVjZWl2ZXIveGhyJylcclxuICAsIFhIUkNvcnNPYmplY3QgPSByZXF1aXJlKCcuL3NlbmRlci94aHItY29ycycpXHJcbiAgLCBYSFJMb2NhbE9iamVjdCA9IHJlcXVpcmUoJy4vc2VuZGVyL3hoci1sb2NhbCcpXHJcbiAgO1xyXG5cclxuZnVuY3Rpb24gWGhyUG9sbGluZ1RyYW5zcG9ydCh0cmFuc1VybCkge1xyXG4gIGlmICghWEhSTG9jYWxPYmplY3QuZW5hYmxlZCAmJiAhWEhSQ29yc09iamVjdC5lbmFibGVkKSB7XHJcbiAgICB0aHJvdyBuZXcgRXJyb3IoJ1RyYW5zcG9ydCBjcmVhdGVkIHdoZW4gZGlzYWJsZWQnKTtcclxuICB9XHJcbiAgQWpheEJhc2VkVHJhbnNwb3J0LmNhbGwodGhpcywgdHJhbnNVcmwsICcveGhyJywgWGhyUmVjZWl2ZXIsIFhIUkNvcnNPYmplY3QpO1xyXG59XHJcblxyXG5pbmhlcml0cyhYaHJQb2xsaW5nVHJhbnNwb3J0LCBBamF4QmFzZWRUcmFuc3BvcnQpO1xyXG5cclxuWGhyUG9sbGluZ1RyYW5zcG9ydC5lbmFibGVkID0gZnVuY3Rpb24oaW5mbykge1xyXG4gIGlmIChpbmZvLm51bGxPcmlnaW4pIHtcclxuICAgIHJldHVybiBmYWxzZTtcclxuICB9XHJcblxyXG4gIGlmIChYSFJMb2NhbE9iamVjdC5lbmFibGVkICYmIGluZm8uc2FtZU9yaWdpbikge1xyXG4gICAgcmV0dXJuIHRydWU7XHJcbiAgfVxyXG4gIHJldHVybiBYSFJDb3JzT2JqZWN0LmVuYWJsZWQ7XHJcbn07XHJcblxyXG5YaHJQb2xsaW5nVHJhbnNwb3J0LnRyYW5zcG9ydE5hbWUgPSAneGhyLXBvbGxpbmcnO1xyXG5YaHJQb2xsaW5nVHJhbnNwb3J0LnJvdW5kVHJpcHMgPSAyOyAvLyBwcmVmbGlnaHQsIGFqYXhcclxuXHJcbm1vZHVsZS5leHBvcnRzID0gWGhyUG9sbGluZ1RyYW5zcG9ydDtcclxuXHJcbn0se1wiLi9saWIvYWpheC1iYXNlZFwiOjI0LFwiLi9yZWNlaXZlci94aHJcIjozMixcIi4vc2VuZGVyL3hoci1jb3JzXCI6MzUsXCIuL3NlbmRlci94aHItbG9jYWxcIjozNyxcImluaGVyaXRzXCI6NTd9XSw0MjpbZnVuY3Rpb24ocmVxdWlyZSxtb2R1bGUsZXhwb3J0cyl7XHJcbihmdW5jdGlvbiAoZ2xvYmFsKXtcclxuJ3VzZSBzdHJpY3QnO1xyXG5cclxudmFyIGluaGVyaXRzID0gcmVxdWlyZSgnaW5oZXJpdHMnKVxyXG4gICwgQWpheEJhc2VkVHJhbnNwb3J0ID0gcmVxdWlyZSgnLi9saWIvYWpheC1iYXNlZCcpXHJcbiAgLCBYaHJSZWNlaXZlciA9IHJlcXVpcmUoJy4vcmVjZWl2ZXIveGhyJylcclxuICAsIFhIUkNvcnNPYmplY3QgPSByZXF1aXJlKCcuL3NlbmRlci94aHItY29ycycpXHJcbiAgLCBYSFJMb2NhbE9iamVjdCA9IHJlcXVpcmUoJy4vc2VuZGVyL3hoci1sb2NhbCcpXHJcbiAgLCBicm93c2VyID0gcmVxdWlyZSgnLi4vdXRpbHMvYnJvd3NlcicpXHJcbiAgO1xyXG5cclxuZnVuY3Rpb24gWGhyU3RyZWFtaW5nVHJhbnNwb3J0KHRyYW5zVXJsKSB7XHJcbiAgaWYgKCFYSFJMb2NhbE9iamVjdC5lbmFibGVkICYmICFYSFJDb3JzT2JqZWN0LmVuYWJsZWQpIHtcclxuICAgIHRocm93IG5ldyBFcnJvcignVHJhbnNwb3J0IGNyZWF0ZWQgd2hlbiBkaXNhYmxlZCcpO1xyXG4gIH1cclxuICBBamF4QmFzZWRUcmFuc3BvcnQuY2FsbCh0aGlzLCB0cmFuc1VybCwgJy94aHJfc3RyZWFtaW5nJywgWGhyUmVjZWl2ZXIsIFhIUkNvcnNPYmplY3QpO1xyXG59XHJcblxyXG5pbmhlcml0cyhYaHJTdHJlYW1pbmdUcmFuc3BvcnQsIEFqYXhCYXNlZFRyYW5zcG9ydCk7XHJcblxyXG5YaHJTdHJlYW1pbmdUcmFuc3BvcnQuZW5hYmxlZCA9IGZ1bmN0aW9uKGluZm8pIHtcclxuICBpZiAoaW5mby5udWxsT3JpZ2luKSB7XHJcbiAgICByZXR1cm4gZmFsc2U7XHJcbiAgfVxyXG4gIC8vIE9wZXJhIGRvZXNuJ3Qgc3VwcG9ydCB4aHItc3RyZWFtaW5nICM2MFxyXG4gIC8vIEJ1dCBpdCBtaWdodCBiZSBhYmxlIHRvICM5MlxyXG4gIGlmIChicm93c2VyLmlzT3BlcmEoKSkge1xyXG4gICAgcmV0dXJuIGZhbHNlO1xyXG4gIH1cclxuXHJcbiAgcmV0dXJuIFhIUkNvcnNPYmplY3QuZW5hYmxlZDtcclxufTtcclxuXHJcblhoclN0cmVhbWluZ1RyYW5zcG9ydC50cmFuc3BvcnROYW1lID0gJ3hoci1zdHJlYW1pbmcnO1xyXG5YaHJTdHJlYW1pbmdUcmFuc3BvcnQucm91bmRUcmlwcyA9IDI7IC8vIHByZWZsaWdodCwgYWpheFxyXG5cclxuLy8gU2FmYXJpIGdldHMgY29uZnVzZWQgd2hlbiBhIHN0cmVhbWluZyBhamF4IHJlcXVlc3QgaXMgc3RhcnRlZFxyXG4vLyBiZWZvcmUgb25sb2FkLiBUaGlzIGNhdXNlcyB0aGUgbG9hZCBpbmRpY2F0b3IgdG8gc3BpbiBpbmRlZmluZXRlbHkuXHJcbi8vIE9ubHkgcmVxdWlyZSBib2R5IHdoZW4gdXNlZCBpbiBhIGJyb3dzZXJcclxuWGhyU3RyZWFtaW5nVHJhbnNwb3J0Lm5lZWRCb2R5ID0gISFnbG9iYWwuZG9jdW1lbnQ7XHJcblxyXG5tb2R1bGUuZXhwb3J0cyA9IFhoclN0cmVhbWluZ1RyYW5zcG9ydDtcclxuXHJcbn0pLmNhbGwodGhpcyx0eXBlb2YgZ2xvYmFsICE9PSBcInVuZGVmaW5lZFwiID8gZ2xvYmFsIDogdHlwZW9mIHNlbGYgIT09IFwidW5kZWZpbmVkXCIgPyBzZWxmIDogdHlwZW9mIHdpbmRvdyAhPT0gXCJ1bmRlZmluZWRcIiA/IHdpbmRvdyA6IHt9KVxyXG5cclxufSx7XCIuLi91dGlscy9icm93c2VyXCI6NDQsXCIuL2xpYi9hamF4LWJhc2VkXCI6MjQsXCIuL3JlY2VpdmVyL3hoclwiOjMyLFwiLi9zZW5kZXIveGhyLWNvcnNcIjozNSxcIi4vc2VuZGVyL3hoci1sb2NhbFwiOjM3LFwiaW5oZXJpdHNcIjo1N31dLDQzOltmdW5jdGlvbihyZXF1aXJlLG1vZHVsZSxleHBvcnRzKXtcclxuKGZ1bmN0aW9uIChnbG9iYWwpe1xyXG4ndXNlIHN0cmljdCc7XHJcblxyXG5pZiAoZ2xvYmFsLmNyeXB0byAmJiBnbG9iYWwuY3J5cHRvLmdldFJhbmRvbVZhbHVlcykge1xyXG4gIG1vZHVsZS5leHBvcnRzLnJhbmRvbUJ5dGVzID0gZnVuY3Rpb24obGVuZ3RoKSB7XHJcbiAgICB2YXIgYnl0ZXMgPSBuZXcgVWludDhBcnJheShsZW5ndGgpO1xyXG4gICAgZ2xvYmFsLmNyeXB0by5nZXRSYW5kb21WYWx1ZXMoYnl0ZXMpO1xyXG4gICAgcmV0dXJuIGJ5dGVzO1xyXG4gIH07XHJcbn0gZWxzZSB7XHJcbiAgbW9kdWxlLmV4cG9ydHMucmFuZG9tQnl0ZXMgPSBmdW5jdGlvbihsZW5ndGgpIHtcclxuICAgIHZhciBieXRlcyA9IG5ldyBBcnJheShsZW5ndGgpO1xyXG4gICAgZm9yICh2YXIgaSA9IDA7IGkgPCBsZW5ndGg7IGkrKykge1xyXG4gICAgICBieXRlc1tpXSA9IE1hdGguZmxvb3IoTWF0aC5yYW5kb20oKSAqIDI1Nik7XHJcbiAgICB9XHJcbiAgICByZXR1cm4gYnl0ZXM7XHJcbiAgfTtcclxufVxyXG5cclxufSkuY2FsbCh0aGlzLHR5cGVvZiBnbG9iYWwgIT09IFwidW5kZWZpbmVkXCIgPyBnbG9iYWwgOiB0eXBlb2Ygc2VsZiAhPT0gXCJ1bmRlZmluZWRcIiA/IHNlbGYgOiB0eXBlb2Ygd2luZG93ICE9PSBcInVuZGVmaW5lZFwiID8gd2luZG93IDoge30pXHJcblxyXG59LHt9XSw0NDpbZnVuY3Rpb24ocmVxdWlyZSxtb2R1bGUsZXhwb3J0cyl7XHJcbihmdW5jdGlvbiAoZ2xvYmFsKXtcclxuJ3VzZSBzdHJpY3QnO1xyXG5cclxubW9kdWxlLmV4cG9ydHMgPSB7XHJcbiAgaXNPcGVyYTogZnVuY3Rpb24oKSB7XHJcbiAgICByZXR1cm4gZ2xvYmFsLm5hdmlnYXRvciAmJlxyXG4gICAgICAvb3BlcmEvaS50ZXN0KGdsb2JhbC5uYXZpZ2F0b3IudXNlckFnZW50KTtcclxuICB9XHJcblxyXG4sIGlzS29ucXVlcm9yOiBmdW5jdGlvbigpIHtcclxuICAgIHJldHVybiBnbG9iYWwubmF2aWdhdG9yICYmXHJcbiAgICAgIC9rb25xdWVyb3IvaS50ZXN0KGdsb2JhbC5uYXZpZ2F0b3IudXNlckFnZW50KTtcclxuICB9XHJcblxyXG4gIC8vICMxODcgd3JhcCBkb2N1bWVudC5kb21haW4gaW4gdHJ5L2NhdGNoIGJlY2F1c2Ugb2YgV1A4IGZyb20gZmlsZTovLy9cclxuLCBoYXNEb21haW46IGZ1bmN0aW9uICgpIHtcclxuICAgIC8vIG5vbi1icm93c2VyIGNsaWVudCBhbHdheXMgaGFzIGEgZG9tYWluXHJcbiAgICBpZiAoIWdsb2JhbC5kb2N1bWVudCkge1xyXG4gICAgICByZXR1cm4gdHJ1ZTtcclxuICAgIH1cclxuXHJcbiAgICB0cnkge1xyXG4gICAgICByZXR1cm4gISFnbG9iYWwuZG9jdW1lbnQuZG9tYWluO1xyXG4gICAgfSBjYXRjaCAoZSkge1xyXG4gICAgICByZXR1cm4gZmFsc2U7XHJcbiAgICB9XHJcbiAgfVxyXG59O1xyXG5cclxufSkuY2FsbCh0aGlzLHR5cGVvZiBnbG9iYWwgIT09IFwidW5kZWZpbmVkXCIgPyBnbG9iYWwgOiB0eXBlb2Ygc2VsZiAhPT0gXCJ1bmRlZmluZWRcIiA/IHNlbGYgOiB0eXBlb2Ygd2luZG93ICE9PSBcInVuZGVmaW5lZFwiID8gd2luZG93IDoge30pXHJcblxyXG59LHt9XSw0NTpbZnVuY3Rpb24ocmVxdWlyZSxtb2R1bGUsZXhwb3J0cyl7XHJcbid1c2Ugc3RyaWN0JztcclxuXHJcbnZhciBKU09OMyA9IHJlcXVpcmUoJ2pzb24zJyk7XHJcblxyXG4vLyBTb21lIGV4dHJhIGNoYXJhY3RlcnMgdGhhdCBDaHJvbWUgZ2V0cyB3cm9uZywgYW5kIHN1YnN0aXR1dGVzIHdpdGhcclxuLy8gc29tZXRoaW5nIGVsc2Ugb24gdGhlIHdpcmUuXHJcbi8vIGVzbGludC1kaXNhYmxlLW5leHQtbGluZSBuby1jb250cm9sLXJlZ2V4XHJcbnZhciBleHRyYUVzY2FwYWJsZSA9IC9bXFx4MDAtXFx4MWZcXHVkODAwLVxcdWRmZmZcXHVmZmZlXFx1ZmZmZlxcdTAzMDAtXFx1MDMzM1xcdTAzM2QtXFx1MDM0NlxcdTAzNGEtXFx1MDM0Y1xcdTAzNTAtXFx1MDM1MlxcdTAzNTctXFx1MDM1OFxcdTAzNWMtXFx1MDM2MlxcdTAzNzRcXHUwMzdlXFx1MDM4N1xcdTA1OTEtXFx1MDVhZlxcdTA1YzRcXHUwNjEwLVxcdTA2MTdcXHUwNjUzLVxcdTA2NTRcXHUwNjU3LVxcdTA2NWJcXHUwNjVkLVxcdTA2NWVcXHUwNmRmLVxcdTA2ZTJcXHUwNmViLVxcdTA2ZWNcXHUwNzMwXFx1MDczMi1cXHUwNzMzXFx1MDczNS1cXHUwNzM2XFx1MDczYVxcdTA3M2RcXHUwNzNmLVxcdTA3NDFcXHUwNzQzXFx1MDc0NVxcdTA3NDdcXHUwN2ViLVxcdTA3ZjFcXHUwOTUxXFx1MDk1OC1cXHUwOTVmXFx1MDlkYy1cXHUwOWRkXFx1MDlkZlxcdTBhMzNcXHUwYTM2XFx1MGE1OS1cXHUwYTViXFx1MGE1ZVxcdTBiNWMtXFx1MGI1ZFxcdTBlMzgtXFx1MGUzOVxcdTBmNDNcXHUwZjRkXFx1MGY1MlxcdTBmNTdcXHUwZjVjXFx1MGY2OVxcdTBmNzItXFx1MGY3NlxcdTBmNzhcXHUwZjgwLVxcdTBmODNcXHUwZjkzXFx1MGY5ZFxcdTBmYTJcXHUwZmE3XFx1MGZhY1xcdTBmYjlcXHUxOTM5LVxcdTE5M2FcXHUxYTE3XFx1MWI2YlxcdTFjZGEtXFx1MWNkYlxcdTFkYzAtXFx1MWRjZlxcdTFkZmNcXHUxZGZlXFx1MWY3MVxcdTFmNzNcXHUxZjc1XFx1MWY3N1xcdTFmNzlcXHUxZjdiXFx1MWY3ZFxcdTFmYmJcXHUxZmJlXFx1MWZjOVxcdTFmY2JcXHUxZmQzXFx1MWZkYlxcdTFmZTNcXHUxZmViXFx1MWZlZS1cXHUxZmVmXFx1MWZmOVxcdTFmZmJcXHUxZmZkXFx1MjAwMC1cXHUyMDAxXFx1MjBkMC1cXHUyMGQxXFx1MjBkNC1cXHUyMGQ3XFx1MjBlNy1cXHUyMGU5XFx1MjEyNlxcdTIxMmEtXFx1MjEyYlxcdTIzMjktXFx1MjMyYVxcdTJhZGNcXHUzMDJiLVxcdTMwMmNcXHVhYWIyLVxcdWFhYjNcXHVmOTAwLVxcdWZhMGRcXHVmYTEwXFx1ZmExMlxcdWZhMTUtXFx1ZmExZVxcdWZhMjBcXHVmYTIyXFx1ZmEyNS1cXHVmYTI2XFx1ZmEyYS1cXHVmYTJkXFx1ZmEzMC1cXHVmYTZkXFx1ZmE3MC1cXHVmYWQ5XFx1ZmIxZFxcdWZiMWZcXHVmYjJhLVxcdWZiMzZcXHVmYjM4LVxcdWZiM2NcXHVmYjNlXFx1ZmI0MC1cXHVmYjQxXFx1ZmI0My1cXHVmYjQ0XFx1ZmI0Ni1cXHVmYjRlXFx1ZmZmMC1cXHVmZmZmXS9nXHJcbiAgLCBleHRyYUxvb2t1cDtcclxuXHJcbi8vIFRoaXMgbWF5IGJlIHF1aXRlIHNsb3csIHNvIGxldCdzIGRlbGF5IHVudGlsIHVzZXIgYWN0dWFsbHkgdXNlcyBiYWRcclxuLy8gY2hhcmFjdGVycy5cclxudmFyIHVucm9sbExvb2t1cCA9IGZ1bmN0aW9uKGVzY2FwYWJsZSkge1xyXG4gIHZhciBpO1xyXG4gIHZhciB1bnJvbGxlZCA9IHt9O1xyXG4gIHZhciBjID0gW107XHJcbiAgZm9yIChpID0gMDsgaSA8IDY1NTM2OyBpKyspIHtcclxuICAgIGMucHVzaCggU3RyaW5nLmZyb21DaGFyQ29kZShpKSApO1xyXG4gIH1cclxuICBlc2NhcGFibGUubGFzdEluZGV4ID0gMDtcclxuICBjLmpvaW4oJycpLnJlcGxhY2UoZXNjYXBhYmxlLCBmdW5jdGlvbihhKSB7XHJcbiAgICB1bnJvbGxlZFsgYSBdID0gJ1xcXFx1JyArICgnMDAwMCcgKyBhLmNoYXJDb2RlQXQoMCkudG9TdHJpbmcoMTYpKS5zbGljZSgtNCk7XHJcbiAgICByZXR1cm4gJyc7XHJcbiAgfSk7XHJcbiAgZXNjYXBhYmxlLmxhc3RJbmRleCA9IDA7XHJcbiAgcmV0dXJuIHVucm9sbGVkO1xyXG59O1xyXG5cclxuLy8gUXVvdGUgc3RyaW5nLCBhbHNvIHRha2luZyBjYXJlIG9mIHVuaWNvZGUgY2hhcmFjdGVycyB0aGF0IGJyb3dzZXJzXHJcbi8vIG9mdGVuIGJyZWFrLiBFc3BlY2lhbGx5LCB0YWtlIGNhcmUgb2YgdW5pY29kZSBzdXJyb2dhdGVzOlxyXG4vLyBodHRwOi8vZW4ud2lraXBlZGlhLm9yZy93aWtpL01hcHBpbmdfb2ZfVW5pY29kZV9jaGFyYWN0ZXJzI1N1cnJvZ2F0ZXNcclxubW9kdWxlLmV4cG9ydHMgPSB7XHJcbiAgcXVvdGU6IGZ1bmN0aW9uKHN0cmluZykge1xyXG4gICAgdmFyIHF1b3RlZCA9IEpTT04zLnN0cmluZ2lmeShzdHJpbmcpO1xyXG5cclxuICAgIC8vIEluIG1vc3QgY2FzZXMgdGhpcyBzaG91bGQgYmUgdmVyeSBmYXN0IGFuZCBnb29kIGVub3VnaC5cclxuICAgIGV4dHJhRXNjYXBhYmxlLmxhc3RJbmRleCA9IDA7XHJcbiAgICBpZiAoIWV4dHJhRXNjYXBhYmxlLnRlc3QocXVvdGVkKSkge1xyXG4gICAgICByZXR1cm4gcXVvdGVkO1xyXG4gICAgfVxyXG5cclxuICAgIGlmICghZXh0cmFMb29rdXApIHtcclxuICAgICAgZXh0cmFMb29rdXAgPSB1bnJvbGxMb29rdXAoZXh0cmFFc2NhcGFibGUpO1xyXG4gICAgfVxyXG5cclxuICAgIHJldHVybiBxdW90ZWQucmVwbGFjZShleHRyYUVzY2FwYWJsZSwgZnVuY3Rpb24oYSkge1xyXG4gICAgICByZXR1cm4gZXh0cmFMb29rdXBbYV07XHJcbiAgICB9KTtcclxuICB9XHJcbn07XHJcblxyXG59LHtcImpzb24zXCI6NTh9XSw0NjpbZnVuY3Rpb24ocmVxdWlyZSxtb2R1bGUsZXhwb3J0cyl7XHJcbihmdW5jdGlvbiAoZ2xvYmFsKXtcclxuJ3VzZSBzdHJpY3QnO1xyXG5cclxudmFyIHJhbmRvbSA9IHJlcXVpcmUoJy4vcmFuZG9tJyk7XHJcblxyXG52YXIgb25VbmxvYWQgPSB7fVxyXG4gICwgYWZ0ZXJVbmxvYWQgPSBmYWxzZVxyXG4gICAgLy8gZGV0ZWN0IGdvb2dsZSBjaHJvbWUgcGFja2FnZWQgYXBwcyBiZWNhdXNlIHRoZXkgZG9uJ3QgYWxsb3cgdGhlICd1bmxvYWQnIGV2ZW50XHJcbiAgLCBpc0Nocm9tZVBhY2thZ2VkQXBwID0gZ2xvYmFsLmNocm9tZSAmJiBnbG9iYWwuY2hyb21lLmFwcCAmJiBnbG9iYWwuY2hyb21lLmFwcC5ydW50aW1lXHJcbiAgO1xyXG5cclxubW9kdWxlLmV4cG9ydHMgPSB7XHJcbiAgYXR0YWNoRXZlbnQ6IGZ1bmN0aW9uKGV2ZW50LCBsaXN0ZW5lcikge1xyXG4gICAgaWYgKHR5cGVvZiBnbG9iYWwuYWRkRXZlbnRMaXN0ZW5lciAhPT0gJ3VuZGVmaW5lZCcpIHtcclxuICAgICAgZ2xvYmFsLmFkZEV2ZW50TGlzdGVuZXIoZXZlbnQsIGxpc3RlbmVyLCBmYWxzZSk7XHJcbiAgICB9IGVsc2UgaWYgKGdsb2JhbC5kb2N1bWVudCAmJiBnbG9iYWwuYXR0YWNoRXZlbnQpIHtcclxuICAgICAgLy8gSUUgcXVpcmtzLlxyXG4gICAgICAvLyBBY2NvcmRpbmcgdG86IGh0dHA6Ly9zdGV2ZXNvdWRlcnMuY29tL21pc2MvdGVzdC1wb3N0bWVzc2FnZS5waHBcclxuICAgICAgLy8gdGhlIG1lc3NhZ2UgZ2V0cyBkZWxpdmVyZWQgb25seSB0byAnZG9jdW1lbnQnLCBub3QgJ3dpbmRvdycuXHJcbiAgICAgIGdsb2JhbC5kb2N1bWVudC5hdHRhY2hFdmVudCgnb24nICsgZXZlbnQsIGxpc3RlbmVyKTtcclxuICAgICAgLy8gSSBnZXQgJ3dpbmRvdycgZm9yIGllOC5cclxuICAgICAgZ2xvYmFsLmF0dGFjaEV2ZW50KCdvbicgKyBldmVudCwgbGlzdGVuZXIpO1xyXG4gICAgfVxyXG4gIH1cclxuXHJcbiwgZGV0YWNoRXZlbnQ6IGZ1bmN0aW9uKGV2ZW50LCBsaXN0ZW5lcikge1xyXG4gICAgaWYgKHR5cGVvZiBnbG9iYWwuYWRkRXZlbnRMaXN0ZW5lciAhPT0gJ3VuZGVmaW5lZCcpIHtcclxuICAgICAgZ2xvYmFsLnJlbW92ZUV2ZW50TGlzdGVuZXIoZXZlbnQsIGxpc3RlbmVyLCBmYWxzZSk7XHJcbiAgICB9IGVsc2UgaWYgKGdsb2JhbC5kb2N1bWVudCAmJiBnbG9iYWwuZGV0YWNoRXZlbnQpIHtcclxuICAgICAgZ2xvYmFsLmRvY3VtZW50LmRldGFjaEV2ZW50KCdvbicgKyBldmVudCwgbGlzdGVuZXIpO1xyXG4gICAgICBnbG9iYWwuZGV0YWNoRXZlbnQoJ29uJyArIGV2ZW50LCBsaXN0ZW5lcik7XHJcbiAgICB9XHJcbiAgfVxyXG5cclxuLCB1bmxvYWRBZGQ6IGZ1bmN0aW9uKGxpc3RlbmVyKSB7XHJcbiAgICBpZiAoaXNDaHJvbWVQYWNrYWdlZEFwcCkge1xyXG4gICAgICByZXR1cm4gbnVsbDtcclxuICAgIH1cclxuXHJcbiAgICB2YXIgcmVmID0gcmFuZG9tLnN0cmluZyg4KTtcclxuICAgIG9uVW5sb2FkW3JlZl0gPSBsaXN0ZW5lcjtcclxuICAgIGlmIChhZnRlclVubG9hZCkge1xyXG4gICAgICBzZXRUaW1lb3V0KHRoaXMudHJpZ2dlclVubG9hZENhbGxiYWNrcywgMCk7XHJcbiAgICB9XHJcbiAgICByZXR1cm4gcmVmO1xyXG4gIH1cclxuXHJcbiwgdW5sb2FkRGVsOiBmdW5jdGlvbihyZWYpIHtcclxuICAgIGlmIChyZWYgaW4gb25VbmxvYWQpIHtcclxuICAgICAgZGVsZXRlIG9uVW5sb2FkW3JlZl07XHJcbiAgICB9XHJcbiAgfVxyXG5cclxuLCB0cmlnZ2VyVW5sb2FkQ2FsbGJhY2tzOiBmdW5jdGlvbigpIHtcclxuICAgIGZvciAodmFyIHJlZiBpbiBvblVubG9hZCkge1xyXG4gICAgICBvblVubG9hZFtyZWZdKCk7XHJcbiAgICAgIGRlbGV0ZSBvblVubG9hZFtyZWZdO1xyXG4gICAgfVxyXG4gIH1cclxufTtcclxuXHJcbnZhciB1bmxvYWRUcmlnZ2VyZWQgPSBmdW5jdGlvbigpIHtcclxuICBpZiAoYWZ0ZXJVbmxvYWQpIHtcclxuICAgIHJldHVybjtcclxuICB9XHJcbiAgYWZ0ZXJVbmxvYWQgPSB0cnVlO1xyXG4gIG1vZHVsZS5leHBvcnRzLnRyaWdnZXJVbmxvYWRDYWxsYmFja3MoKTtcclxufTtcclxuXHJcbi8vICd1bmxvYWQnIGFsb25lIGlzIG5vdCByZWxpYWJsZSBpbiBvcGVyYSB3aXRoaW4gYW4gaWZyYW1lLCBidXQgd2VcclxuLy8gY2FuJ3QgdXNlIGBiZWZvcmV1bmxvYWRgIGFzIElFIGZpcmVzIGl0IG9uIGphdmFzY3JpcHQ6IGxpbmtzLlxyXG5pZiAoIWlzQ2hyb21lUGFja2FnZWRBcHApIHtcclxuICBtb2R1bGUuZXhwb3J0cy5hdHRhY2hFdmVudCgndW5sb2FkJywgdW5sb2FkVHJpZ2dlcmVkKTtcclxufVxyXG5cclxufSkuY2FsbCh0aGlzLHR5cGVvZiBnbG9iYWwgIT09IFwidW5kZWZpbmVkXCIgPyBnbG9iYWwgOiB0eXBlb2Ygc2VsZiAhPT0gXCJ1bmRlZmluZWRcIiA/IHNlbGYgOiB0eXBlb2Ygd2luZG93ICE9PSBcInVuZGVmaW5lZFwiID8gd2luZG93IDoge30pXHJcblxyXG59LHtcIi4vcmFuZG9tXCI6NTB9XSw0NzpbZnVuY3Rpb24ocmVxdWlyZSxtb2R1bGUsZXhwb3J0cyl7XHJcbihmdW5jdGlvbiAocHJvY2VzcyxnbG9iYWwpe1xyXG4ndXNlIHN0cmljdCc7XHJcblxyXG52YXIgZXZlbnRVdGlscyA9IHJlcXVpcmUoJy4vZXZlbnQnKVxyXG4gICwgSlNPTjMgPSByZXF1aXJlKCdqc29uMycpXHJcbiAgLCBicm93c2VyID0gcmVxdWlyZSgnLi9icm93c2VyJylcclxuICA7XHJcblxyXG52YXIgZGVidWcgPSBmdW5jdGlvbigpIHt9O1xyXG5pZiAocHJvY2Vzcy5lbnYuTk9ERV9FTlYgIT09ICdwcm9kdWN0aW9uJykge1xyXG4gIGRlYnVnID0gcmVxdWlyZSgnZGVidWcnKSgnc29ja2pzLWNsaWVudDp1dGlsczppZnJhbWUnKTtcclxufVxyXG5cclxubW9kdWxlLmV4cG9ydHMgPSB7XHJcbiAgV1ByZWZpeDogJ19qcCdcclxuLCBjdXJyZW50V2luZG93SWQ6IG51bGxcclxuXHJcbiwgcG9sbHV0ZUdsb2JhbE5hbWVzcGFjZTogZnVuY3Rpb24oKSB7XHJcbiAgICBpZiAoIShtb2R1bGUuZXhwb3J0cy5XUHJlZml4IGluIGdsb2JhbCkpIHtcclxuICAgICAgZ2xvYmFsW21vZHVsZS5leHBvcnRzLldQcmVmaXhdID0ge307XHJcbiAgICB9XHJcbiAgfVxyXG5cclxuLCBwb3N0TWVzc2FnZTogZnVuY3Rpb24odHlwZSwgZGF0YSkge1xyXG4gICAgaWYgKGdsb2JhbC5wYXJlbnQgIT09IGdsb2JhbCkge1xyXG4gICAgICBnbG9iYWwucGFyZW50LnBvc3RNZXNzYWdlKEpTT04zLnN0cmluZ2lmeSh7XHJcbiAgICAgICAgd2luZG93SWQ6IG1vZHVsZS5leHBvcnRzLmN1cnJlbnRXaW5kb3dJZFxyXG4gICAgICAsIHR5cGU6IHR5cGVcclxuICAgICAgLCBkYXRhOiBkYXRhIHx8ICcnXHJcbiAgICAgIH0pLCAnKicpO1xyXG4gICAgfSBlbHNlIHtcclxuICAgICAgZGVidWcoJ0Nhbm5vdCBwb3N0TWVzc2FnZSwgbm8gcGFyZW50IHdpbmRvdy4nLCB0eXBlLCBkYXRhKTtcclxuICAgIH1cclxuICB9XHJcblxyXG4sIGNyZWF0ZUlmcmFtZTogZnVuY3Rpb24oaWZyYW1lVXJsLCBlcnJvckNhbGxiYWNrKSB7XHJcbiAgICB2YXIgaWZyYW1lID0gZ2xvYmFsLmRvY3VtZW50LmNyZWF0ZUVsZW1lbnQoJ2lmcmFtZScpO1xyXG4gICAgdmFyIHRyZWYsIHVubG9hZFJlZjtcclxuICAgIHZhciB1bmF0dGFjaCA9IGZ1bmN0aW9uKCkge1xyXG4gICAgICBkZWJ1ZygndW5hdHRhY2gnKTtcclxuICAgICAgY2xlYXJUaW1lb3V0KHRyZWYpO1xyXG4gICAgICAvLyBFeHBsb3JlciBoYWQgcHJvYmxlbXMgd2l0aCB0aGF0LlxyXG4gICAgICB0cnkge1xyXG4gICAgICAgIGlmcmFtZS5vbmxvYWQgPSBudWxsO1xyXG4gICAgICB9IGNhdGNoICh4KSB7XHJcbiAgICAgICAgLy8gaW50ZW50aW9uYWxseSBlbXB0eVxyXG4gICAgICB9XHJcbiAgICAgIGlmcmFtZS5vbmVycm9yID0gbnVsbDtcclxuICAgIH07XHJcbiAgICB2YXIgY2xlYW51cCA9IGZ1bmN0aW9uKCkge1xyXG4gICAgICBkZWJ1ZygnY2xlYW51cCcpO1xyXG4gICAgICBpZiAoaWZyYW1lKSB7XHJcbiAgICAgICAgdW5hdHRhY2goKTtcclxuICAgICAgICAvLyBUaGlzIHRpbWVvdXQgbWFrZXMgY2hyb21lIGZpcmUgb25iZWZvcmV1bmxvYWQgZXZlbnRcclxuICAgICAgICAvLyB3aXRoaW4gaWZyYW1lLiBXaXRob3V0IHRoZSB0aW1lb3V0IGl0IGdvZXMgc3RyYWlnaHQgdG9cclxuICAgICAgICAvLyBvbnVubG9hZC5cclxuICAgICAgICBzZXRUaW1lb3V0KGZ1bmN0aW9uKCkge1xyXG4gICAgICAgICAgaWYgKGlmcmFtZSkge1xyXG4gICAgICAgICAgICBpZnJhbWUucGFyZW50Tm9kZS5yZW1vdmVDaGlsZChpZnJhbWUpO1xyXG4gICAgICAgICAgfVxyXG4gICAgICAgICAgaWZyYW1lID0gbnVsbDtcclxuICAgICAgICB9LCAwKTtcclxuICAgICAgICBldmVudFV0aWxzLnVubG9hZERlbCh1bmxvYWRSZWYpO1xyXG4gICAgICB9XHJcbiAgICB9O1xyXG4gICAgdmFyIG9uZXJyb3IgPSBmdW5jdGlvbihlcnIpIHtcclxuICAgICAgZGVidWcoJ29uZXJyb3InLCBlcnIpO1xyXG4gICAgICBpZiAoaWZyYW1lKSB7XHJcbiAgICAgICAgY2xlYW51cCgpO1xyXG4gICAgICAgIGVycm9yQ2FsbGJhY2soZXJyKTtcclxuICAgICAgfVxyXG4gICAgfTtcclxuICAgIHZhciBwb3N0ID0gZnVuY3Rpb24obXNnLCBvcmlnaW4pIHtcclxuICAgICAgZGVidWcoJ3Bvc3QnLCBtc2csIG9yaWdpbik7XHJcbiAgICAgIHRyeSB7XHJcbiAgICAgICAgLy8gV2hlbiB0aGUgaWZyYW1lIGlzIG5vdCBsb2FkZWQsIElFIHJhaXNlcyBhbiBleGNlcHRpb25cclxuICAgICAgICAvLyBvbiAnY29udGVudFdpbmRvdycuXHJcbiAgICAgICAgc2V0VGltZW91dChmdW5jdGlvbigpIHtcclxuICAgICAgICAgIGlmIChpZnJhbWUgJiYgaWZyYW1lLmNvbnRlbnRXaW5kb3cpIHtcclxuICAgICAgICAgICAgaWZyYW1lLmNvbnRlbnRXaW5kb3cucG9zdE1lc3NhZ2UobXNnLCBvcmlnaW4pO1xyXG4gICAgICAgICAgfVxyXG4gICAgICAgIH0sIDApO1xyXG4gICAgICB9IGNhdGNoICh4KSB7XHJcbiAgICAgICAgLy8gaW50ZW50aW9uYWxseSBlbXB0eVxyXG4gICAgICB9XHJcbiAgICB9O1xyXG5cclxuICAgIGlmcmFtZS5zcmMgPSBpZnJhbWVVcmw7XHJcbiAgICBpZnJhbWUuc3R5bGUuZGlzcGxheSA9ICdub25lJztcclxuICAgIGlmcmFtZS5zdHlsZS5wb3NpdGlvbiA9ICdhYnNvbHV0ZSc7XHJcbiAgICBpZnJhbWUub25lcnJvciA9IGZ1bmN0aW9uKCkge1xyXG4gICAgICBvbmVycm9yKCdvbmVycm9yJyk7XHJcbiAgICB9O1xyXG4gICAgaWZyYW1lLm9ubG9hZCA9IGZ1bmN0aW9uKCkge1xyXG4gICAgICBkZWJ1Zygnb25sb2FkJyk7XHJcbiAgICAgIC8vIGBvbmxvYWRgIGlzIHRyaWdnZXJlZCBiZWZvcmUgc2NyaXB0cyBvbiB0aGUgaWZyYW1lIGFyZVxyXG4gICAgICAvLyBleGVjdXRlZC4gR2l2ZSBpdCBmZXcgc2Vjb25kcyB0byBhY3R1YWxseSBsb2FkIHN0dWZmLlxyXG4gICAgICBjbGVhclRpbWVvdXQodHJlZik7XHJcbiAgICAgIHRyZWYgPSBzZXRUaW1lb3V0KGZ1bmN0aW9uKCkge1xyXG4gICAgICAgIG9uZXJyb3IoJ29ubG9hZCB0aW1lb3V0Jyk7XHJcbiAgICAgIH0sIDIwMDApO1xyXG4gICAgfTtcclxuICAgIGdsb2JhbC5kb2N1bWVudC5ib2R5LmFwcGVuZENoaWxkKGlmcmFtZSk7XHJcbiAgICB0cmVmID0gc2V0VGltZW91dChmdW5jdGlvbigpIHtcclxuICAgICAgb25lcnJvcigndGltZW91dCcpO1xyXG4gICAgfSwgMTUwMDApO1xyXG4gICAgdW5sb2FkUmVmID0gZXZlbnRVdGlscy51bmxvYWRBZGQoY2xlYW51cCk7XHJcbiAgICByZXR1cm4ge1xyXG4gICAgICBwb3N0OiBwb3N0XHJcbiAgICAsIGNsZWFudXA6IGNsZWFudXBcclxuICAgICwgbG9hZGVkOiB1bmF0dGFjaFxyXG4gICAgfTtcclxuICB9XHJcblxyXG4vKiBlc2xpbnQgbm8tdW5kZWY6IFwib2ZmXCIsIG5ldy1jYXA6IFwib2ZmXCIgKi9cclxuLCBjcmVhdGVIdG1sZmlsZTogZnVuY3Rpb24oaWZyYW1lVXJsLCBlcnJvckNhbGxiYWNrKSB7XHJcbiAgICB2YXIgYXhvID0gWydBY3RpdmUnXS5jb25jYXQoJ09iamVjdCcpLmpvaW4oJ1gnKTtcclxuICAgIHZhciBkb2MgPSBuZXcgZ2xvYmFsW2F4b10oJ2h0bWxmaWxlJyk7XHJcbiAgICB2YXIgdHJlZiwgdW5sb2FkUmVmO1xyXG4gICAgdmFyIGlmcmFtZTtcclxuICAgIHZhciB1bmF0dGFjaCA9IGZ1bmN0aW9uKCkge1xyXG4gICAgICBjbGVhclRpbWVvdXQodHJlZik7XHJcbiAgICAgIGlmcmFtZS5vbmVycm9yID0gbnVsbDtcclxuICAgIH07XHJcbiAgICB2YXIgY2xlYW51cCA9IGZ1bmN0aW9uKCkge1xyXG4gICAgICBpZiAoZG9jKSB7XHJcbiAgICAgICAgdW5hdHRhY2goKTtcclxuICAgICAgICBldmVudFV0aWxzLnVubG9hZERlbCh1bmxvYWRSZWYpO1xyXG4gICAgICAgIGlmcmFtZS5wYXJlbnROb2RlLnJlbW92ZUNoaWxkKGlmcmFtZSk7XHJcbiAgICAgICAgaWZyYW1lID0gZG9jID0gbnVsbDtcclxuICAgICAgICBDb2xsZWN0R2FyYmFnZSgpO1xyXG4gICAgICB9XHJcbiAgICB9O1xyXG4gICAgdmFyIG9uZXJyb3IgPSBmdW5jdGlvbihyKSB7XHJcbiAgICAgIGRlYnVnKCdvbmVycm9yJywgcik7XHJcbiAgICAgIGlmIChkb2MpIHtcclxuICAgICAgICBjbGVhbnVwKCk7XHJcbiAgICAgICAgZXJyb3JDYWxsYmFjayhyKTtcclxuICAgICAgfVxyXG4gICAgfTtcclxuICAgIHZhciBwb3N0ID0gZnVuY3Rpb24obXNnLCBvcmlnaW4pIHtcclxuICAgICAgdHJ5IHtcclxuICAgICAgICAvLyBXaGVuIHRoZSBpZnJhbWUgaXMgbm90IGxvYWRlZCwgSUUgcmFpc2VzIGFuIGV4Y2VwdGlvblxyXG4gICAgICAgIC8vIG9uICdjb250ZW50V2luZG93Jy5cclxuICAgICAgICBzZXRUaW1lb3V0KGZ1bmN0aW9uKCkge1xyXG4gICAgICAgICAgaWYgKGlmcmFtZSAmJiBpZnJhbWUuY29udGVudFdpbmRvdykge1xyXG4gICAgICAgICAgICAgIGlmcmFtZS5jb250ZW50V2luZG93LnBvc3RNZXNzYWdlKG1zZywgb3JpZ2luKTtcclxuICAgICAgICAgIH1cclxuICAgICAgICB9LCAwKTtcclxuICAgICAgfSBjYXRjaCAoeCkge1xyXG4gICAgICAgIC8vIGludGVudGlvbmFsbHkgZW1wdHlcclxuICAgICAgfVxyXG4gICAgfTtcclxuXHJcbiAgICBkb2Mub3BlbigpO1xyXG4gICAgZG9jLndyaXRlKCc8aHRtbD48cycgKyAnY3JpcHQ+JyArXHJcbiAgICAgICAgICAgICAgJ2RvY3VtZW50LmRvbWFpbj1cIicgKyBnbG9iYWwuZG9jdW1lbnQuZG9tYWluICsgJ1wiOycgK1xyXG4gICAgICAgICAgICAgICc8L3MnICsgJ2NyaXB0PjwvaHRtbD4nKTtcclxuICAgIGRvYy5jbG9zZSgpO1xyXG4gICAgZG9jLnBhcmVudFdpbmRvd1ttb2R1bGUuZXhwb3J0cy5XUHJlZml4XSA9IGdsb2JhbFttb2R1bGUuZXhwb3J0cy5XUHJlZml4XTtcclxuICAgIHZhciBjID0gZG9jLmNyZWF0ZUVsZW1lbnQoJ2RpdicpO1xyXG4gICAgZG9jLmJvZHkuYXBwZW5kQ2hpbGQoYyk7XHJcbiAgICBpZnJhbWUgPSBkb2MuY3JlYXRlRWxlbWVudCgnaWZyYW1lJyk7XHJcbiAgICBjLmFwcGVuZENoaWxkKGlmcmFtZSk7XHJcbiAgICBpZnJhbWUuc3JjID0gaWZyYW1lVXJsO1xyXG4gICAgaWZyYW1lLm9uZXJyb3IgPSBmdW5jdGlvbigpIHtcclxuICAgICAgb25lcnJvcignb25lcnJvcicpO1xyXG4gICAgfTtcclxuICAgIHRyZWYgPSBzZXRUaW1lb3V0KGZ1bmN0aW9uKCkge1xyXG4gICAgICBvbmVycm9yKCd0aW1lb3V0Jyk7XHJcbiAgICB9LCAxNTAwMCk7XHJcbiAgICB1bmxvYWRSZWYgPSBldmVudFV0aWxzLnVubG9hZEFkZChjbGVhbnVwKTtcclxuICAgIHJldHVybiB7XHJcbiAgICAgIHBvc3Q6IHBvc3RcclxuICAgICwgY2xlYW51cDogY2xlYW51cFxyXG4gICAgLCBsb2FkZWQ6IHVuYXR0YWNoXHJcbiAgICB9O1xyXG4gIH1cclxufTtcclxuXHJcbm1vZHVsZS5leHBvcnRzLmlmcmFtZUVuYWJsZWQgPSBmYWxzZTtcclxuaWYgKGdsb2JhbC5kb2N1bWVudCkge1xyXG4gIC8vIHBvc3RNZXNzYWdlIG1pc2JlaGF2ZXMgaW4ga29ucXVlcm9yIDQuNi41IC0gdGhlIG1lc3NhZ2VzIGFyZSBkZWxpdmVyZWQgd2l0aFxyXG4gIC8vIGh1Z2UgZGVsYXksIG9yIG5vdCBhdCBhbGwuXHJcbiAgbW9kdWxlLmV4cG9ydHMuaWZyYW1lRW5hYmxlZCA9ICh0eXBlb2YgZ2xvYmFsLnBvc3RNZXNzYWdlID09PSAnZnVuY3Rpb24nIHx8XHJcbiAgICB0eXBlb2YgZ2xvYmFsLnBvc3RNZXNzYWdlID09PSAnb2JqZWN0JykgJiYgKCFicm93c2VyLmlzS29ucXVlcm9yKCkpO1xyXG59XHJcblxyXG59KS5jYWxsKHRoaXMseyBlbnY6IHt9IH0sdHlwZW9mIGdsb2JhbCAhPT0gXCJ1bmRlZmluZWRcIiA/IGdsb2JhbCA6IHR5cGVvZiBzZWxmICE9PSBcInVuZGVmaW5lZFwiID8gc2VsZiA6IHR5cGVvZiB3aW5kb3cgIT09IFwidW5kZWZpbmVkXCIgPyB3aW5kb3cgOiB7fSlcclxuXHJcbn0se1wiLi9icm93c2VyXCI6NDQsXCIuL2V2ZW50XCI6NDYsXCJkZWJ1Z1wiOjU1LFwianNvbjNcIjo1OH1dLDQ4OltmdW5jdGlvbihyZXF1aXJlLG1vZHVsZSxleHBvcnRzKXtcclxuKGZ1bmN0aW9uIChnbG9iYWwpe1xyXG4ndXNlIHN0cmljdCc7XHJcblxyXG52YXIgbG9nT2JqZWN0ID0ge307XHJcblsnbG9nJywgJ2RlYnVnJywgJ3dhcm4nXS5mb3JFYWNoKGZ1bmN0aW9uIChsZXZlbCkge1xyXG4gIHZhciBsZXZlbEV4aXN0cztcclxuXHJcbiAgdHJ5IHtcclxuICAgIGxldmVsRXhpc3RzID0gZ2xvYmFsLmNvbnNvbGUgJiYgZ2xvYmFsLmNvbnNvbGVbbGV2ZWxdICYmIGdsb2JhbC5jb25zb2xlW2xldmVsXS5hcHBseTtcclxuICB9IGNhdGNoKGUpIHtcclxuICAgIC8vIGRvIG5vdGhpbmdcclxuICB9XHJcblxyXG4gIGxvZ09iamVjdFtsZXZlbF0gPSBsZXZlbEV4aXN0cyA/IGZ1bmN0aW9uICgpIHtcclxuICAgIHJldHVybiBnbG9iYWwuY29uc29sZVtsZXZlbF0uYXBwbHkoZ2xvYmFsLmNvbnNvbGUsIGFyZ3VtZW50cyk7XHJcbiAgfSA6IChsZXZlbCA9PT0gJ2xvZycgPyBmdW5jdGlvbiAoKSB7fSA6IGxvZ09iamVjdC5sb2cpO1xyXG59KTtcclxuXHJcbm1vZHVsZS5leHBvcnRzID0gbG9nT2JqZWN0O1xyXG5cclxufSkuY2FsbCh0aGlzLHR5cGVvZiBnbG9iYWwgIT09IFwidW5kZWZpbmVkXCIgPyBnbG9iYWwgOiB0eXBlb2Ygc2VsZiAhPT0gXCJ1bmRlZmluZWRcIiA/IHNlbGYgOiB0eXBlb2Ygd2luZG93ICE9PSBcInVuZGVmaW5lZFwiID8gd2luZG93IDoge30pXHJcblxyXG59LHt9XSw0OTpbZnVuY3Rpb24ocmVxdWlyZSxtb2R1bGUsZXhwb3J0cyl7XHJcbid1c2Ugc3RyaWN0JztcclxuXHJcbm1vZHVsZS5leHBvcnRzID0ge1xyXG4gIGlzT2JqZWN0OiBmdW5jdGlvbihvYmopIHtcclxuICAgIHZhciB0eXBlID0gdHlwZW9mIG9iajtcclxuICAgIHJldHVybiB0eXBlID09PSAnZnVuY3Rpb24nIHx8IHR5cGUgPT09ICdvYmplY3QnICYmICEhb2JqO1xyXG4gIH1cclxuXHJcbiwgZXh0ZW5kOiBmdW5jdGlvbihvYmopIHtcclxuICAgIGlmICghdGhpcy5pc09iamVjdChvYmopKSB7XHJcbiAgICAgIHJldHVybiBvYmo7XHJcbiAgICB9XHJcbiAgICB2YXIgc291cmNlLCBwcm9wO1xyXG4gICAgZm9yICh2YXIgaSA9IDEsIGxlbmd0aCA9IGFyZ3VtZW50cy5sZW5ndGg7IGkgPCBsZW5ndGg7IGkrKykge1xyXG4gICAgICBzb3VyY2UgPSBhcmd1bWVudHNbaV07XHJcbiAgICAgIGZvciAocHJvcCBpbiBzb3VyY2UpIHtcclxuICAgICAgICBpZiAoT2JqZWN0LnByb3RvdHlwZS5oYXNPd25Qcm9wZXJ0eS5jYWxsKHNvdXJjZSwgcHJvcCkpIHtcclxuICAgICAgICAgIG9ialtwcm9wXSA9IHNvdXJjZVtwcm9wXTtcclxuICAgICAgICB9XHJcbiAgICAgIH1cclxuICAgIH1cclxuICAgIHJldHVybiBvYmo7XHJcbiAgfVxyXG59O1xyXG5cclxufSx7fV0sNTA6W2Z1bmN0aW9uKHJlcXVpcmUsbW9kdWxlLGV4cG9ydHMpe1xyXG4ndXNlIHN0cmljdCc7XHJcblxyXG4vKiBnbG9iYWwgY3J5cHRvOnRydWUgKi9cclxudmFyIGNyeXB0byA9IHJlcXVpcmUoJ2NyeXB0bycpO1xyXG5cclxuLy8gVGhpcyBzdHJpbmcgaGFzIGxlbmd0aCAzMiwgYSBwb3dlciBvZiAyLCBzbyB0aGUgbW9kdWx1cyBkb2Vzbid0IGludHJvZHVjZSBhXHJcbi8vIGJpYXMuXHJcbnZhciBfcmFuZG9tU3RyaW5nQ2hhcnMgPSAnYWJjZGVmZ2hpamtsbW5vcHFyc3R1dnd4eXowMTIzNDUnO1xyXG5tb2R1bGUuZXhwb3J0cyA9IHtcclxuICBzdHJpbmc6IGZ1bmN0aW9uKGxlbmd0aCkge1xyXG4gICAgdmFyIG1heCA9IF9yYW5kb21TdHJpbmdDaGFycy5sZW5ndGg7XHJcbiAgICB2YXIgYnl0ZXMgPSBjcnlwdG8ucmFuZG9tQnl0ZXMobGVuZ3RoKTtcclxuICAgIHZhciByZXQgPSBbXTtcclxuICAgIGZvciAodmFyIGkgPSAwOyBpIDwgbGVuZ3RoOyBpKyspIHtcclxuICAgICAgcmV0LnB1c2goX3JhbmRvbVN0cmluZ0NoYXJzLnN1YnN0cihieXRlc1tpXSAlIG1heCwgMSkpO1xyXG4gICAgfVxyXG4gICAgcmV0dXJuIHJldC5qb2luKCcnKTtcclxuICB9XHJcblxyXG4sIG51bWJlcjogZnVuY3Rpb24obWF4KSB7XHJcbiAgICByZXR1cm4gTWF0aC5mbG9vcihNYXRoLnJhbmRvbSgpICogbWF4KTtcclxuICB9XHJcblxyXG4sIG51bWJlclN0cmluZzogZnVuY3Rpb24obWF4KSB7XHJcbiAgICB2YXIgdCA9ICgnJyArIChtYXggLSAxKSkubGVuZ3RoO1xyXG4gICAgdmFyIHAgPSBuZXcgQXJyYXkodCArIDEpLmpvaW4oJzAnKTtcclxuICAgIHJldHVybiAocCArIHRoaXMubnVtYmVyKG1heCkpLnNsaWNlKC10KTtcclxuICB9XHJcbn07XHJcblxyXG59LHtcImNyeXB0b1wiOjQzfV0sNTE6W2Z1bmN0aW9uKHJlcXVpcmUsbW9kdWxlLGV4cG9ydHMpe1xyXG4oZnVuY3Rpb24gKHByb2Nlc3Mpe1xyXG4ndXNlIHN0cmljdCc7XHJcblxyXG52YXIgZGVidWcgPSBmdW5jdGlvbigpIHt9O1xyXG5pZiAocHJvY2Vzcy5lbnYuTk9ERV9FTlYgIT09ICdwcm9kdWN0aW9uJykge1xyXG4gIGRlYnVnID0gcmVxdWlyZSgnZGVidWcnKSgnc29ja2pzLWNsaWVudDp1dGlsczp0cmFuc3BvcnQnKTtcclxufVxyXG5cclxubW9kdWxlLmV4cG9ydHMgPSBmdW5jdGlvbihhdmFpbGFibGVUcmFuc3BvcnRzKSB7XHJcbiAgcmV0dXJuIHtcclxuICAgIGZpbHRlclRvRW5hYmxlZDogZnVuY3Rpb24odHJhbnNwb3J0c1doaXRlbGlzdCwgaW5mbykge1xyXG4gICAgICB2YXIgdHJhbnNwb3J0cyA9IHtcclxuICAgICAgICBtYWluOiBbXVxyXG4gICAgICAsIGZhY2FkZTogW11cclxuICAgICAgfTtcclxuICAgICAgaWYgKCF0cmFuc3BvcnRzV2hpdGVsaXN0KSB7XHJcbiAgICAgICAgdHJhbnNwb3J0c1doaXRlbGlzdCA9IFtdO1xyXG4gICAgICB9IGVsc2UgaWYgKHR5cGVvZiB0cmFuc3BvcnRzV2hpdGVsaXN0ID09PSAnc3RyaW5nJykge1xyXG4gICAgICAgIHRyYW5zcG9ydHNXaGl0ZWxpc3QgPSBbdHJhbnNwb3J0c1doaXRlbGlzdF07XHJcbiAgICAgIH1cclxuXHJcbiAgICAgIGF2YWlsYWJsZVRyYW5zcG9ydHMuZm9yRWFjaChmdW5jdGlvbih0cmFucykge1xyXG4gICAgICAgIGlmICghdHJhbnMpIHtcclxuICAgICAgICAgIHJldHVybjtcclxuICAgICAgICB9XHJcblxyXG4gICAgICAgIGlmICh0cmFucy50cmFuc3BvcnROYW1lID09PSAnd2Vic29ja2V0JyAmJiBpbmZvLndlYnNvY2tldCA9PT0gZmFsc2UpIHtcclxuICAgICAgICAgIGRlYnVnKCdkaXNhYmxlZCBmcm9tIHNlcnZlcicsICd3ZWJzb2NrZXQnKTtcclxuICAgICAgICAgIHJldHVybjtcclxuICAgICAgICB9XHJcblxyXG4gICAgICAgIGlmICh0cmFuc3BvcnRzV2hpdGVsaXN0Lmxlbmd0aCAmJlxyXG4gICAgICAgICAgICB0cmFuc3BvcnRzV2hpdGVsaXN0LmluZGV4T2YodHJhbnMudHJhbnNwb3J0TmFtZSkgPT09IC0xKSB7XHJcbiAgICAgICAgICBkZWJ1Zygnbm90IGluIHdoaXRlbGlzdCcsIHRyYW5zLnRyYW5zcG9ydE5hbWUpO1xyXG4gICAgICAgICAgcmV0dXJuO1xyXG4gICAgICAgIH1cclxuXHJcbiAgICAgICAgaWYgKHRyYW5zLmVuYWJsZWQoaW5mbykpIHtcclxuICAgICAgICAgIGRlYnVnKCdlbmFibGVkJywgdHJhbnMudHJhbnNwb3J0TmFtZSk7XHJcbiAgICAgICAgICB0cmFuc3BvcnRzLm1haW4ucHVzaCh0cmFucyk7XHJcbiAgICAgICAgICBpZiAodHJhbnMuZmFjYWRlVHJhbnNwb3J0KSB7XHJcbiAgICAgICAgICAgIHRyYW5zcG9ydHMuZmFjYWRlLnB1c2godHJhbnMuZmFjYWRlVHJhbnNwb3J0KTtcclxuICAgICAgICAgIH1cclxuICAgICAgICB9IGVsc2Uge1xyXG4gICAgICAgICAgZGVidWcoJ2Rpc2FibGVkJywgdHJhbnMudHJhbnNwb3J0TmFtZSk7XHJcbiAgICAgICAgfVxyXG4gICAgICB9KTtcclxuICAgICAgcmV0dXJuIHRyYW5zcG9ydHM7XHJcbiAgICB9XHJcbiAgfTtcclxufTtcclxuXHJcbn0pLmNhbGwodGhpcyx7IGVudjoge30gfSlcclxuXHJcbn0se1wiZGVidWdcIjo1NX1dLDUyOltmdW5jdGlvbihyZXF1aXJlLG1vZHVsZSxleHBvcnRzKXtcclxuKGZ1bmN0aW9uIChwcm9jZXNzKXtcclxuJ3VzZSBzdHJpY3QnO1xyXG5cclxudmFyIFVSTCA9IHJlcXVpcmUoJ3VybC1wYXJzZScpO1xyXG5cclxudmFyIGRlYnVnID0gZnVuY3Rpb24oKSB7fTtcclxuaWYgKHByb2Nlc3MuZW52Lk5PREVfRU5WICE9PSAncHJvZHVjdGlvbicpIHtcclxuICBkZWJ1ZyA9IHJlcXVpcmUoJ2RlYnVnJykoJ3NvY2tqcy1jbGllbnQ6dXRpbHM6dXJsJyk7XHJcbn1cclxuXHJcbm1vZHVsZS5leHBvcnRzID0ge1xyXG4gIGdldE9yaWdpbjogZnVuY3Rpb24odXJsKSB7XHJcbiAgICBpZiAoIXVybCkge1xyXG4gICAgICByZXR1cm4gbnVsbDtcclxuICAgIH1cclxuXHJcbiAgICB2YXIgcCA9IG5ldyBVUkwodXJsKTtcclxuICAgIGlmIChwLnByb3RvY29sID09PSAnZmlsZTonKSB7XHJcbiAgICAgIHJldHVybiBudWxsO1xyXG4gICAgfVxyXG5cclxuICAgIHZhciBwb3J0ID0gcC5wb3J0O1xyXG4gICAgaWYgKCFwb3J0KSB7XHJcbiAgICAgIHBvcnQgPSAocC5wcm90b2NvbCA9PT0gJ2h0dHBzOicpID8gJzQ0MycgOiAnODAnO1xyXG4gICAgfVxyXG5cclxuICAgIHJldHVybiBwLnByb3RvY29sICsgJy8vJyArIHAuaG9zdG5hbWUgKyAnOicgKyBwb3J0O1xyXG4gIH1cclxuXHJcbiwgaXNPcmlnaW5FcXVhbDogZnVuY3Rpb24oYSwgYikge1xyXG4gICAgdmFyIHJlcyA9IHRoaXMuZ2V0T3JpZ2luKGEpID09PSB0aGlzLmdldE9yaWdpbihiKTtcclxuICAgIGRlYnVnKCdzYW1lJywgYSwgYiwgcmVzKTtcclxuICAgIHJldHVybiByZXM7XHJcbiAgfVxyXG5cclxuLCBpc1NjaGVtZUVxdWFsOiBmdW5jdGlvbihhLCBiKSB7XHJcbiAgICByZXR1cm4gKGEuc3BsaXQoJzonKVswXSA9PT0gYi5zcGxpdCgnOicpWzBdKTtcclxuICB9XHJcblxyXG4sIGFkZFBhdGg6IGZ1bmN0aW9uICh1cmwsIHBhdGgpIHtcclxuICAgIHZhciBxcyA9IHVybC5zcGxpdCgnPycpO1xyXG4gICAgcmV0dXJuIHFzWzBdICsgcGF0aCArIChxc1sxXSA/ICc/JyArIHFzWzFdIDogJycpO1xyXG4gIH1cclxuXHJcbiwgYWRkUXVlcnk6IGZ1bmN0aW9uICh1cmwsIHEpIHtcclxuICAgIHJldHVybiB1cmwgKyAodXJsLmluZGV4T2YoJz8nKSA9PT0gLTEgPyAoJz8nICsgcSkgOiAoJyYnICsgcSkpO1xyXG4gIH1cclxufTtcclxuXHJcbn0pLmNhbGwodGhpcyx7IGVudjoge30gfSlcclxuXHJcbn0se1wiZGVidWdcIjo1NSxcInVybC1wYXJzZVwiOjYxfV0sNTM6W2Z1bmN0aW9uKHJlcXVpcmUsbW9kdWxlLGV4cG9ydHMpe1xyXG5tb2R1bGUuZXhwb3J0cyA9ICcxLjEuNCc7XHJcblxyXG59LHt9XSw1NDpbZnVuY3Rpb24ocmVxdWlyZSxtb2R1bGUsZXhwb3J0cyl7XHJcbi8qKlxyXG4gKiBIZWxwZXJzLlxyXG4gKi9cclxuXHJcbnZhciBzID0gMTAwMFxyXG52YXIgbSA9IHMgKiA2MFxyXG52YXIgaCA9IG0gKiA2MFxyXG52YXIgZCA9IGggKiAyNFxyXG52YXIgeSA9IGQgKiAzNjUuMjVcclxuXHJcbi8qKlxyXG4gKiBQYXJzZSBvciBmb3JtYXQgdGhlIGdpdmVuIGB2YWxgLlxyXG4gKlxyXG4gKiBPcHRpb25zOlxyXG4gKlxyXG4gKiAgLSBgbG9uZ2AgdmVyYm9zZSBmb3JtYXR0aW5nIFtmYWxzZV1cclxuICpcclxuICogQHBhcmFtIHtTdHJpbmd8TnVtYmVyfSB2YWxcclxuICogQHBhcmFtIHtPYmplY3R9IFtvcHRpb25zXVxyXG4gKiBAdGhyb3dzIHtFcnJvcn0gdGhyb3cgYW4gZXJyb3IgaWYgdmFsIGlzIG5vdCBhIG5vbi1lbXB0eSBzdHJpbmcgb3IgYSBudW1iZXJcclxuICogQHJldHVybiB7U3RyaW5nfE51bWJlcn1cclxuICogQGFwaSBwdWJsaWNcclxuICovXHJcblxyXG5tb2R1bGUuZXhwb3J0cyA9IGZ1bmN0aW9uICh2YWwsIG9wdGlvbnMpIHtcclxuICBvcHRpb25zID0gb3B0aW9ucyB8fCB7fVxyXG4gIHZhciB0eXBlID0gdHlwZW9mIHZhbFxyXG4gIGlmICh0eXBlID09PSAnc3RyaW5nJyAmJiB2YWwubGVuZ3RoID4gMCkge1xyXG4gICAgcmV0dXJuIHBhcnNlKHZhbClcclxuICB9IGVsc2UgaWYgKHR5cGUgPT09ICdudW1iZXInICYmIGlzTmFOKHZhbCkgPT09IGZhbHNlKSB7XHJcbiAgICByZXR1cm4gb3B0aW9ucy5sb25nID9cclxuXHRcdFx0Zm10TG9uZyh2YWwpIDpcclxuXHRcdFx0Zm10U2hvcnQodmFsKVxyXG4gIH1cclxuICB0aHJvdyBuZXcgRXJyb3IoJ3ZhbCBpcyBub3QgYSBub24tZW1wdHkgc3RyaW5nIG9yIGEgdmFsaWQgbnVtYmVyLiB2YWw9JyArIEpTT04uc3RyaW5naWZ5KHZhbCkpXHJcbn1cclxuXHJcbi8qKlxyXG4gKiBQYXJzZSB0aGUgZ2l2ZW4gYHN0cmAgYW5kIHJldHVybiBtaWxsaXNlY29uZHMuXHJcbiAqXHJcbiAqIEBwYXJhbSB7U3RyaW5nfSBzdHJcclxuICogQHJldHVybiB7TnVtYmVyfVxyXG4gKiBAYXBpIHByaXZhdGVcclxuICovXHJcblxyXG5mdW5jdGlvbiBwYXJzZShzdHIpIHtcclxuICBzdHIgPSBTdHJpbmcoc3RyKVxyXG4gIGlmIChzdHIubGVuZ3RoID4gMTAwMDApIHtcclxuICAgIHJldHVyblxyXG4gIH1cclxuICB2YXIgbWF0Y2ggPSAvXigoPzpcXGQrKT9cXC4/XFxkKykgKihtaWxsaXNlY29uZHM/fG1zZWNzP3xtc3xzZWNvbmRzP3xzZWNzP3xzfG1pbnV0ZXM/fG1pbnM/fG18aG91cnM/fGhycz98aHxkYXlzP3xkfHllYXJzP3x5cnM/fHkpPyQvaS5leGVjKHN0cilcclxuICBpZiAoIW1hdGNoKSB7XHJcbiAgICByZXR1cm5cclxuICB9XHJcbiAgdmFyIG4gPSBwYXJzZUZsb2F0KG1hdGNoWzFdKVxyXG4gIHZhciB0eXBlID0gKG1hdGNoWzJdIHx8ICdtcycpLnRvTG93ZXJDYXNlKClcclxuICBzd2l0Y2ggKHR5cGUpIHtcclxuICAgIGNhc2UgJ3llYXJzJzpcclxuICAgIGNhc2UgJ3llYXInOlxyXG4gICAgY2FzZSAneXJzJzpcclxuICAgIGNhc2UgJ3lyJzpcclxuICAgIGNhc2UgJ3knOlxyXG4gICAgICByZXR1cm4gbiAqIHlcclxuICAgIGNhc2UgJ2RheXMnOlxyXG4gICAgY2FzZSAnZGF5JzpcclxuICAgIGNhc2UgJ2QnOlxyXG4gICAgICByZXR1cm4gbiAqIGRcclxuICAgIGNhc2UgJ2hvdXJzJzpcclxuICAgIGNhc2UgJ2hvdXInOlxyXG4gICAgY2FzZSAnaHJzJzpcclxuICAgIGNhc2UgJ2hyJzpcclxuICAgIGNhc2UgJ2gnOlxyXG4gICAgICByZXR1cm4gbiAqIGhcclxuICAgIGNhc2UgJ21pbnV0ZXMnOlxyXG4gICAgY2FzZSAnbWludXRlJzpcclxuICAgIGNhc2UgJ21pbnMnOlxyXG4gICAgY2FzZSAnbWluJzpcclxuICAgIGNhc2UgJ20nOlxyXG4gICAgICByZXR1cm4gbiAqIG1cclxuICAgIGNhc2UgJ3NlY29uZHMnOlxyXG4gICAgY2FzZSAnc2Vjb25kJzpcclxuICAgIGNhc2UgJ3NlY3MnOlxyXG4gICAgY2FzZSAnc2VjJzpcclxuICAgIGNhc2UgJ3MnOlxyXG4gICAgICByZXR1cm4gbiAqIHNcclxuICAgIGNhc2UgJ21pbGxpc2Vjb25kcyc6XHJcbiAgICBjYXNlICdtaWxsaXNlY29uZCc6XHJcbiAgICBjYXNlICdtc2Vjcyc6XHJcbiAgICBjYXNlICdtc2VjJzpcclxuICAgIGNhc2UgJ21zJzpcclxuICAgICAgcmV0dXJuIG5cclxuICAgIGRlZmF1bHQ6XHJcbiAgICAgIHJldHVybiB1bmRlZmluZWRcclxuICB9XHJcbn1cclxuXHJcbi8qKlxyXG4gKiBTaG9ydCBmb3JtYXQgZm9yIGBtc2AuXHJcbiAqXHJcbiAqIEBwYXJhbSB7TnVtYmVyfSBtc1xyXG4gKiBAcmV0dXJuIHtTdHJpbmd9XHJcbiAqIEBhcGkgcHJpdmF0ZVxyXG4gKi9cclxuXHJcbmZ1bmN0aW9uIGZtdFNob3J0KG1zKSB7XHJcbiAgaWYgKG1zID49IGQpIHtcclxuICAgIHJldHVybiBNYXRoLnJvdW5kKG1zIC8gZCkgKyAnZCdcclxuICB9XHJcbiAgaWYgKG1zID49IGgpIHtcclxuICAgIHJldHVybiBNYXRoLnJvdW5kKG1zIC8gaCkgKyAnaCdcclxuICB9XHJcbiAgaWYgKG1zID49IG0pIHtcclxuICAgIHJldHVybiBNYXRoLnJvdW5kKG1zIC8gbSkgKyAnbSdcclxuICB9XHJcbiAgaWYgKG1zID49IHMpIHtcclxuICAgIHJldHVybiBNYXRoLnJvdW5kKG1zIC8gcykgKyAncydcclxuICB9XHJcbiAgcmV0dXJuIG1zICsgJ21zJ1xyXG59XHJcblxyXG4vKipcclxuICogTG9uZyBmb3JtYXQgZm9yIGBtc2AuXHJcbiAqXHJcbiAqIEBwYXJhbSB7TnVtYmVyfSBtc1xyXG4gKiBAcmV0dXJuIHtTdHJpbmd9XHJcbiAqIEBhcGkgcHJpdmF0ZVxyXG4gKi9cclxuXHJcbmZ1bmN0aW9uIGZtdExvbmcobXMpIHtcclxuICByZXR1cm4gcGx1cmFsKG1zLCBkLCAnZGF5JykgfHxcclxuICAgIHBsdXJhbChtcywgaCwgJ2hvdXInKSB8fFxyXG4gICAgcGx1cmFsKG1zLCBtLCAnbWludXRlJykgfHxcclxuICAgIHBsdXJhbChtcywgcywgJ3NlY29uZCcpIHx8XHJcbiAgICBtcyArICcgbXMnXHJcbn1cclxuXHJcbi8qKlxyXG4gKiBQbHVyYWxpemF0aW9uIGhlbHBlci5cclxuICovXHJcblxyXG5mdW5jdGlvbiBwbHVyYWwobXMsIG4sIG5hbWUpIHtcclxuICBpZiAobXMgPCBuKSB7XHJcbiAgICByZXR1cm5cclxuICB9XHJcbiAgaWYgKG1zIDwgbiAqIDEuNSkge1xyXG4gICAgcmV0dXJuIE1hdGguZmxvb3IobXMgLyBuKSArICcgJyArIG5hbWVcclxuICB9XHJcbiAgcmV0dXJuIE1hdGguY2VpbChtcyAvIG4pICsgJyAnICsgbmFtZSArICdzJ1xyXG59XHJcblxyXG59LHt9XSw1NTpbZnVuY3Rpb24ocmVxdWlyZSxtb2R1bGUsZXhwb3J0cyl7XHJcbihmdW5jdGlvbiAocHJvY2Vzcyl7XHJcbi8qKlxyXG4gKiBUaGlzIGlzIHRoZSB3ZWIgYnJvd3NlciBpbXBsZW1lbnRhdGlvbiBvZiBgZGVidWcoKWAuXHJcbiAqXHJcbiAqIEV4cG9zZSBgZGVidWcoKWAgYXMgdGhlIG1vZHVsZS5cclxuICovXHJcblxyXG5leHBvcnRzID0gbW9kdWxlLmV4cG9ydHMgPSByZXF1aXJlKCcuL2RlYnVnJyk7XHJcbmV4cG9ydHMubG9nID0gbG9nO1xyXG5leHBvcnRzLmZvcm1hdEFyZ3MgPSBmb3JtYXRBcmdzO1xyXG5leHBvcnRzLnNhdmUgPSBzYXZlO1xyXG5leHBvcnRzLmxvYWQgPSBsb2FkO1xyXG5leHBvcnRzLnVzZUNvbG9ycyA9IHVzZUNvbG9ycztcclxuZXhwb3J0cy5zdG9yYWdlID0gJ3VuZGVmaW5lZCcgIT0gdHlwZW9mIGNocm9tZVxyXG4gICAgICAgICAgICAgICAmJiAndW5kZWZpbmVkJyAhPSB0eXBlb2YgY2hyb21lLnN0b3JhZ2VcclxuICAgICAgICAgICAgICAgICAgPyBjaHJvbWUuc3RvcmFnZS5sb2NhbFxyXG4gICAgICAgICAgICAgICAgICA6IGxvY2Fsc3RvcmFnZSgpO1xyXG5cclxuLyoqXHJcbiAqIENvbG9ycy5cclxuICovXHJcblxyXG5leHBvcnRzLmNvbG9ycyA9IFtcclxuICAnbGlnaHRzZWFncmVlbicsXHJcbiAgJ2ZvcmVzdGdyZWVuJyxcclxuICAnZ29sZGVucm9kJyxcclxuICAnZG9kZ2VyYmx1ZScsXHJcbiAgJ2RhcmtvcmNoaWQnLFxyXG4gICdjcmltc29uJ1xyXG5dO1xyXG5cclxuLyoqXHJcbiAqIEN1cnJlbnRseSBvbmx5IFdlYktpdC1iYXNlZCBXZWIgSW5zcGVjdG9ycywgRmlyZWZveCA+PSB2MzEsXHJcbiAqIGFuZCB0aGUgRmlyZWJ1ZyBleHRlbnNpb24gKGFueSBGaXJlZm94IHZlcnNpb24pIGFyZSBrbm93blxyXG4gKiB0byBzdXBwb3J0IFwiJWNcIiBDU1MgY3VzdG9taXphdGlvbnMuXHJcbiAqXHJcbiAqIFRPRE86IGFkZCBhIGBsb2NhbFN0b3JhZ2VgIHZhcmlhYmxlIHRvIGV4cGxpY2l0bHkgZW5hYmxlL2Rpc2FibGUgY29sb3JzXHJcbiAqL1xyXG5cclxuZnVuY3Rpb24gdXNlQ29sb3JzKCkge1xyXG4gIC8vIE5COiBJbiBhbiBFbGVjdHJvbiBwcmVsb2FkIHNjcmlwdCwgZG9jdW1lbnQgd2lsbCBiZSBkZWZpbmVkIGJ1dCBub3QgZnVsbHlcclxuICAvLyBpbml0aWFsaXplZC4gU2luY2Ugd2Uga25vdyB3ZSdyZSBpbiBDaHJvbWUsIHdlJ2xsIGp1c3QgZGV0ZWN0IHRoaXMgY2FzZVxyXG4gIC8vIGV4cGxpY2l0bHlcclxuICBpZiAodHlwZW9mIHdpbmRvdyAhPT0gJ3VuZGVmaW5lZCcgJiYgd2luZG93LnByb2Nlc3MgJiYgd2luZG93LnByb2Nlc3MudHlwZSA9PT0gJ3JlbmRlcmVyJykge1xyXG4gICAgcmV0dXJuIHRydWU7XHJcbiAgfVxyXG5cclxuICAvLyBpcyB3ZWJraXQ/IGh0dHA6Ly9zdGFja292ZXJmbG93LmNvbS9hLzE2NDU5NjA2LzM3Njc3M1xyXG4gIC8vIGRvY3VtZW50IGlzIHVuZGVmaW5lZCBpbiByZWFjdC1uYXRpdmU6IGh0dHBzOi8vZ2l0aHViLmNvbS9mYWNlYm9vay9yZWFjdC1uYXRpdmUvcHVsbC8xNjMyXHJcbiAgcmV0dXJuICh0eXBlb2YgZG9jdW1lbnQgIT09ICd1bmRlZmluZWQnICYmIGRvY3VtZW50ICYmIGRvY3VtZW50LmRvY3VtZW50RWxlbWVudCAmJiBkb2N1bWVudC5kb2N1bWVudEVsZW1lbnQuc3R5bGUgJiYgZG9jdW1lbnQuZG9jdW1lbnRFbGVtZW50LnN0eWxlLldlYmtpdEFwcGVhcmFuY2UpIHx8XHJcbiAgICAvLyBpcyBmaXJlYnVnPyBodHRwOi8vc3RhY2tvdmVyZmxvdy5jb20vYS8zOTgxMjAvMzc2NzczXHJcbiAgICAodHlwZW9mIHdpbmRvdyAhPT0gJ3VuZGVmaW5lZCcgJiYgd2luZG93ICYmIHdpbmRvdy5jb25zb2xlICYmICh3aW5kb3cuY29uc29sZS5maXJlYnVnIHx8ICh3aW5kb3cuY29uc29sZS5leGNlcHRpb24gJiYgd2luZG93LmNvbnNvbGUudGFibGUpKSkgfHxcclxuICAgIC8vIGlzIGZpcmVmb3ggPj0gdjMxP1xyXG4gICAgLy8gaHR0cHM6Ly9kZXZlbG9wZXIubW96aWxsYS5vcmcvZW4tVVMvZG9jcy9Ub29scy9XZWJfQ29uc29sZSNTdHlsaW5nX21lc3NhZ2VzXHJcbiAgICAodHlwZW9mIG5hdmlnYXRvciAhPT0gJ3VuZGVmaW5lZCcgJiYgbmF2aWdhdG9yICYmIG5hdmlnYXRvci51c2VyQWdlbnQgJiYgbmF2aWdhdG9yLnVzZXJBZ2VudC50b0xvd2VyQ2FzZSgpLm1hdGNoKC9maXJlZm94XFwvKFxcZCspLykgJiYgcGFyc2VJbnQoUmVnRXhwLiQxLCAxMCkgPj0gMzEpIHx8XHJcbiAgICAvLyBkb3VibGUgY2hlY2sgd2Via2l0IGluIHVzZXJBZ2VudCBqdXN0IGluIGNhc2Ugd2UgYXJlIGluIGEgd29ya2VyXHJcbiAgICAodHlwZW9mIG5hdmlnYXRvciAhPT0gJ3VuZGVmaW5lZCcgJiYgbmF2aWdhdG9yICYmIG5hdmlnYXRvci51c2VyQWdlbnQgJiYgbmF2aWdhdG9yLnVzZXJBZ2VudC50b0xvd2VyQ2FzZSgpLm1hdGNoKC9hcHBsZXdlYmtpdFxcLyhcXGQrKS8pKTtcclxufVxyXG5cclxuLyoqXHJcbiAqIE1hcCAlaiB0byBgSlNPTi5zdHJpbmdpZnkoKWAsIHNpbmNlIG5vIFdlYiBJbnNwZWN0b3JzIGRvIHRoYXQgYnkgZGVmYXVsdC5cclxuICovXHJcblxyXG5leHBvcnRzLmZvcm1hdHRlcnMuaiA9IGZ1bmN0aW9uKHYpIHtcclxuICB0cnkge1xyXG4gICAgcmV0dXJuIEpTT04uc3RyaW5naWZ5KHYpO1xyXG4gIH0gY2F0Y2ggKGVycikge1xyXG4gICAgcmV0dXJuICdbVW5leHBlY3RlZEpTT05QYXJzZUVycm9yXTogJyArIGVyci5tZXNzYWdlO1xyXG4gIH1cclxufTtcclxuXHJcblxyXG4vKipcclxuICogQ29sb3JpemUgbG9nIGFyZ3VtZW50cyBpZiBlbmFibGVkLlxyXG4gKlxyXG4gKiBAYXBpIHB1YmxpY1xyXG4gKi9cclxuXHJcbmZ1bmN0aW9uIGZvcm1hdEFyZ3MoYXJncykge1xyXG4gIHZhciB1c2VDb2xvcnMgPSB0aGlzLnVzZUNvbG9ycztcclxuXHJcbiAgYXJnc1swXSA9ICh1c2VDb2xvcnMgPyAnJWMnIDogJycpXHJcbiAgICArIHRoaXMubmFtZXNwYWNlXHJcbiAgICArICh1c2VDb2xvcnMgPyAnICVjJyA6ICcgJylcclxuICAgICsgYXJnc1swXVxyXG4gICAgKyAodXNlQ29sb3JzID8gJyVjICcgOiAnICcpXHJcbiAgICArICcrJyArIGV4cG9ydHMuaHVtYW5pemUodGhpcy5kaWZmKTtcclxuXHJcbiAgaWYgKCF1c2VDb2xvcnMpIHJldHVybjtcclxuXHJcbiAgdmFyIGMgPSAnY29sb3I6ICcgKyB0aGlzLmNvbG9yO1xyXG4gIGFyZ3Muc3BsaWNlKDEsIDAsIGMsICdjb2xvcjogaW5oZXJpdCcpXHJcblxyXG4gIC8vIHRoZSBmaW5hbCBcIiVjXCIgaXMgc29tZXdoYXQgdHJpY2t5LCBiZWNhdXNlIHRoZXJlIGNvdWxkIGJlIG90aGVyXHJcbiAgLy8gYXJndW1lbnRzIHBhc3NlZCBlaXRoZXIgYmVmb3JlIG9yIGFmdGVyIHRoZSAlYywgc28gd2UgbmVlZCB0b1xyXG4gIC8vIGZpZ3VyZSBvdXQgdGhlIGNvcnJlY3QgaW5kZXggdG8gaW5zZXJ0IHRoZSBDU1MgaW50b1xyXG4gIHZhciBpbmRleCA9IDA7XHJcbiAgdmFyIGxhc3RDID0gMDtcclxuICBhcmdzWzBdLnJlcGxhY2UoLyVbYS16QS1aJV0vZywgZnVuY3Rpb24obWF0Y2gpIHtcclxuICAgIGlmICgnJSUnID09PSBtYXRjaCkgcmV0dXJuO1xyXG4gICAgaW5kZXgrKztcclxuICAgIGlmICgnJWMnID09PSBtYXRjaCkge1xyXG4gICAgICAvLyB3ZSBvbmx5IGFyZSBpbnRlcmVzdGVkIGluIHRoZSAqbGFzdCogJWNcclxuICAgICAgLy8gKHRoZSB1c2VyIG1heSBoYXZlIHByb3ZpZGVkIHRoZWlyIG93bilcclxuICAgICAgbGFzdEMgPSBpbmRleDtcclxuICAgIH1cclxuICB9KTtcclxuXHJcbiAgYXJncy5zcGxpY2UobGFzdEMsIDAsIGMpO1xyXG59XHJcblxyXG4vKipcclxuICogSW52b2tlcyBgY29uc29sZS5sb2coKWAgd2hlbiBhdmFpbGFibGUuXHJcbiAqIE5vLW9wIHdoZW4gYGNvbnNvbGUubG9nYCBpcyBub3QgYSBcImZ1bmN0aW9uXCIuXHJcbiAqXHJcbiAqIEBhcGkgcHVibGljXHJcbiAqL1xyXG5cclxuZnVuY3Rpb24gbG9nKCkge1xyXG4gIC8vIHRoaXMgaGFja2VyeSBpcyByZXF1aXJlZCBmb3IgSUU4LzksIHdoZXJlXHJcbiAgLy8gdGhlIGBjb25zb2xlLmxvZ2AgZnVuY3Rpb24gZG9lc24ndCBoYXZlICdhcHBseSdcclxuICByZXR1cm4gJ29iamVjdCcgPT09IHR5cGVvZiBjb25zb2xlXHJcbiAgICAmJiBjb25zb2xlLmxvZ1xyXG4gICAgJiYgRnVuY3Rpb24ucHJvdG90eXBlLmFwcGx5LmNhbGwoY29uc29sZS5sb2csIGNvbnNvbGUsIGFyZ3VtZW50cyk7XHJcbn1cclxuXHJcbi8qKlxyXG4gKiBTYXZlIGBuYW1lc3BhY2VzYC5cclxuICpcclxuICogQHBhcmFtIHtTdHJpbmd9IG5hbWVzcGFjZXNcclxuICogQGFwaSBwcml2YXRlXHJcbiAqL1xyXG5cclxuZnVuY3Rpb24gc2F2ZShuYW1lc3BhY2VzKSB7XHJcbiAgdHJ5IHtcclxuICAgIGlmIChudWxsID09IG5hbWVzcGFjZXMpIHtcclxuICAgICAgZXhwb3J0cy5zdG9yYWdlLnJlbW92ZUl0ZW0oJ2RlYnVnJyk7XHJcbiAgICB9IGVsc2Uge1xyXG4gICAgICBleHBvcnRzLnN0b3JhZ2UuZGVidWcgPSBuYW1lc3BhY2VzO1xyXG4gICAgfVxyXG4gIH0gY2F0Y2goZSkge31cclxufVxyXG5cclxuLyoqXHJcbiAqIExvYWQgYG5hbWVzcGFjZXNgLlxyXG4gKlxyXG4gKiBAcmV0dXJuIHtTdHJpbmd9IHJldHVybnMgdGhlIHByZXZpb3VzbHkgcGVyc2lzdGVkIGRlYnVnIG1vZGVzXHJcbiAqIEBhcGkgcHJpdmF0ZVxyXG4gKi9cclxuXHJcbmZ1bmN0aW9uIGxvYWQoKSB7XHJcbiAgdmFyIHI7XHJcbiAgdHJ5IHtcclxuICAgIHIgPSBleHBvcnRzLnN0b3JhZ2UuZGVidWc7XHJcbiAgfSBjYXRjaChlKSB7fVxyXG5cclxuICAvLyBJZiBkZWJ1ZyBpc24ndCBzZXQgaW4gTFMsIGFuZCB3ZSdyZSBpbiBFbGVjdHJvbiwgdHJ5IHRvIGxvYWQgJERFQlVHXHJcbiAgaWYgKCFyICYmIHR5cGVvZiBwcm9jZXNzICE9PSAndW5kZWZpbmVkJyAmJiAnZW52JyBpbiBwcm9jZXNzKSB7XHJcbiAgICByID0gcHJvY2Vzcy5lbnYuREVCVUc7XHJcbiAgfVxyXG5cclxuICByZXR1cm4gcjtcclxufVxyXG5cclxuLyoqXHJcbiAqIEVuYWJsZSBuYW1lc3BhY2VzIGxpc3RlZCBpbiBgbG9jYWxTdG9yYWdlLmRlYnVnYCBpbml0aWFsbHkuXHJcbiAqL1xyXG5cclxuZXhwb3J0cy5lbmFibGUobG9hZCgpKTtcclxuXHJcbi8qKlxyXG4gKiBMb2NhbHN0b3JhZ2UgYXR0ZW1wdHMgdG8gcmV0dXJuIHRoZSBsb2NhbHN0b3JhZ2UuXHJcbiAqXHJcbiAqIFRoaXMgaXMgbmVjZXNzYXJ5IGJlY2F1c2Ugc2FmYXJpIHRocm93c1xyXG4gKiB3aGVuIGEgdXNlciBkaXNhYmxlcyBjb29raWVzL2xvY2Fsc3RvcmFnZVxyXG4gKiBhbmQgeW91IGF0dGVtcHQgdG8gYWNjZXNzIGl0LlxyXG4gKlxyXG4gKiBAcmV0dXJuIHtMb2NhbFN0b3JhZ2V9XHJcbiAqIEBhcGkgcHJpdmF0ZVxyXG4gKi9cclxuXHJcbmZ1bmN0aW9uIGxvY2Fsc3RvcmFnZSgpIHtcclxuICB0cnkge1xyXG4gICAgcmV0dXJuIHdpbmRvdy5sb2NhbFN0b3JhZ2U7XHJcbiAgfSBjYXRjaCAoZSkge31cclxufVxyXG5cclxufSkuY2FsbCh0aGlzLHsgZW52OiB7fSB9KVxyXG5cclxufSx7XCIuL2RlYnVnXCI6NTZ9XSw1NjpbZnVuY3Rpb24ocmVxdWlyZSxtb2R1bGUsZXhwb3J0cyl7XHJcblxyXG4vKipcclxuICogVGhpcyBpcyB0aGUgY29tbW9uIGxvZ2ljIGZvciBib3RoIHRoZSBOb2RlLmpzIGFuZCB3ZWIgYnJvd3NlclxyXG4gKiBpbXBsZW1lbnRhdGlvbnMgb2YgYGRlYnVnKClgLlxyXG4gKlxyXG4gKiBFeHBvc2UgYGRlYnVnKClgIGFzIHRoZSBtb2R1bGUuXHJcbiAqL1xyXG5cclxuZXhwb3J0cyA9IG1vZHVsZS5leHBvcnRzID0gY3JlYXRlRGVidWcuZGVidWcgPSBjcmVhdGVEZWJ1Z1snZGVmYXVsdCddID0gY3JlYXRlRGVidWc7XHJcbmV4cG9ydHMuY29lcmNlID0gY29lcmNlO1xyXG5leHBvcnRzLmRpc2FibGUgPSBkaXNhYmxlO1xyXG5leHBvcnRzLmVuYWJsZSA9IGVuYWJsZTtcclxuZXhwb3J0cy5lbmFibGVkID0gZW5hYmxlZDtcclxuZXhwb3J0cy5odW1hbml6ZSA9IHJlcXVpcmUoJ21zJyk7XHJcblxyXG4vKipcclxuICogVGhlIGN1cnJlbnRseSBhY3RpdmUgZGVidWcgbW9kZSBuYW1lcywgYW5kIG5hbWVzIHRvIHNraXAuXHJcbiAqL1xyXG5cclxuZXhwb3J0cy5uYW1lcyA9IFtdO1xyXG5leHBvcnRzLnNraXBzID0gW107XHJcblxyXG4vKipcclxuICogTWFwIG9mIHNwZWNpYWwgXCIlblwiIGhhbmRsaW5nIGZ1bmN0aW9ucywgZm9yIHRoZSBkZWJ1ZyBcImZvcm1hdFwiIGFyZ3VtZW50LlxyXG4gKlxyXG4gKiBWYWxpZCBrZXkgbmFtZXMgYXJlIGEgc2luZ2xlLCBsb3dlciBvciB1cHBlci1jYXNlIGxldHRlciwgaS5lLiBcIm5cIiBhbmQgXCJOXCIuXHJcbiAqL1xyXG5cclxuZXhwb3J0cy5mb3JtYXR0ZXJzID0ge307XHJcblxyXG4vKipcclxuICogUHJldmlvdXMgbG9nIHRpbWVzdGFtcC5cclxuICovXHJcblxyXG52YXIgcHJldlRpbWU7XHJcblxyXG4vKipcclxuICogU2VsZWN0IGEgY29sb3IuXHJcbiAqIEBwYXJhbSB7U3RyaW5nfSBuYW1lc3BhY2VcclxuICogQHJldHVybiB7TnVtYmVyfVxyXG4gKiBAYXBpIHByaXZhdGVcclxuICovXHJcblxyXG5mdW5jdGlvbiBzZWxlY3RDb2xvcihuYW1lc3BhY2UpIHtcclxuICB2YXIgaGFzaCA9IDAsIGk7XHJcblxyXG4gIGZvciAoaSBpbiBuYW1lc3BhY2UpIHtcclxuICAgIGhhc2ggID0gKChoYXNoIDw8IDUpIC0gaGFzaCkgKyBuYW1lc3BhY2UuY2hhckNvZGVBdChpKTtcclxuICAgIGhhc2ggfD0gMDsgLy8gQ29udmVydCB0byAzMmJpdCBpbnRlZ2VyXHJcbiAgfVxyXG5cclxuICByZXR1cm4gZXhwb3J0cy5jb2xvcnNbTWF0aC5hYnMoaGFzaCkgJSBleHBvcnRzLmNvbG9ycy5sZW5ndGhdO1xyXG59XHJcblxyXG4vKipcclxuICogQ3JlYXRlIGEgZGVidWdnZXIgd2l0aCB0aGUgZ2l2ZW4gYG5hbWVzcGFjZWAuXHJcbiAqXHJcbiAqIEBwYXJhbSB7U3RyaW5nfSBuYW1lc3BhY2VcclxuICogQHJldHVybiB7RnVuY3Rpb259XHJcbiAqIEBhcGkgcHVibGljXHJcbiAqL1xyXG5cclxuZnVuY3Rpb24gY3JlYXRlRGVidWcobmFtZXNwYWNlKSB7XHJcblxyXG4gIGZ1bmN0aW9uIGRlYnVnKCkge1xyXG4gICAgLy8gZGlzYWJsZWQ/XHJcbiAgICBpZiAoIWRlYnVnLmVuYWJsZWQpIHJldHVybjtcclxuXHJcbiAgICB2YXIgc2VsZiA9IGRlYnVnO1xyXG5cclxuICAgIC8vIHNldCBgZGlmZmAgdGltZXN0YW1wXHJcbiAgICB2YXIgY3VyciA9ICtuZXcgRGF0ZSgpO1xyXG4gICAgdmFyIG1zID0gY3VyciAtIChwcmV2VGltZSB8fCBjdXJyKTtcclxuICAgIHNlbGYuZGlmZiA9IG1zO1xyXG4gICAgc2VsZi5wcmV2ID0gcHJldlRpbWU7XHJcbiAgICBzZWxmLmN1cnIgPSBjdXJyO1xyXG4gICAgcHJldlRpbWUgPSBjdXJyO1xyXG5cclxuICAgIC8vIHR1cm4gdGhlIGBhcmd1bWVudHNgIGludG8gYSBwcm9wZXIgQXJyYXlcclxuICAgIHZhciBhcmdzID0gbmV3IEFycmF5KGFyZ3VtZW50cy5sZW5ndGgpO1xyXG4gICAgZm9yICh2YXIgaSA9IDA7IGkgPCBhcmdzLmxlbmd0aDsgaSsrKSB7XHJcbiAgICAgIGFyZ3NbaV0gPSBhcmd1bWVudHNbaV07XHJcbiAgICB9XHJcblxyXG4gICAgYXJnc1swXSA9IGV4cG9ydHMuY29lcmNlKGFyZ3NbMF0pO1xyXG5cclxuICAgIGlmICgnc3RyaW5nJyAhPT0gdHlwZW9mIGFyZ3NbMF0pIHtcclxuICAgICAgLy8gYW55dGhpbmcgZWxzZSBsZXQncyBpbnNwZWN0IHdpdGggJU9cclxuICAgICAgYXJncy51bnNoaWZ0KCclTycpO1xyXG4gICAgfVxyXG5cclxuICAgIC8vIGFwcGx5IGFueSBgZm9ybWF0dGVyc2AgdHJhbnNmb3JtYXRpb25zXHJcbiAgICB2YXIgaW5kZXggPSAwO1xyXG4gICAgYXJnc1swXSA9IGFyZ3NbMF0ucmVwbGFjZSgvJShbYS16QS1aJV0pL2csIGZ1bmN0aW9uKG1hdGNoLCBmb3JtYXQpIHtcclxuICAgICAgLy8gaWYgd2UgZW5jb3VudGVyIGFuIGVzY2FwZWQgJSB0aGVuIGRvbid0IGluY3JlYXNlIHRoZSBhcnJheSBpbmRleFxyXG4gICAgICBpZiAobWF0Y2ggPT09ICclJScpIHJldHVybiBtYXRjaDtcclxuICAgICAgaW5kZXgrKztcclxuICAgICAgdmFyIGZvcm1hdHRlciA9IGV4cG9ydHMuZm9ybWF0dGVyc1tmb3JtYXRdO1xyXG4gICAgICBpZiAoJ2Z1bmN0aW9uJyA9PT0gdHlwZW9mIGZvcm1hdHRlcikge1xyXG4gICAgICAgIHZhciB2YWwgPSBhcmdzW2luZGV4XTtcclxuICAgICAgICBtYXRjaCA9IGZvcm1hdHRlci5jYWxsKHNlbGYsIHZhbCk7XHJcblxyXG4gICAgICAgIC8vIG5vdyB3ZSBuZWVkIHRvIHJlbW92ZSBgYXJnc1tpbmRleF1gIHNpbmNlIGl0J3MgaW5saW5lZCBpbiB0aGUgYGZvcm1hdGBcclxuICAgICAgICBhcmdzLnNwbGljZShpbmRleCwgMSk7XHJcbiAgICAgICAgaW5kZXgtLTtcclxuICAgICAgfVxyXG4gICAgICByZXR1cm4gbWF0Y2g7XHJcbiAgICB9KTtcclxuXHJcbiAgICAvLyBhcHBseSBlbnYtc3BlY2lmaWMgZm9ybWF0dGluZyAoY29sb3JzLCBldGMuKVxyXG4gICAgZXhwb3J0cy5mb3JtYXRBcmdzLmNhbGwoc2VsZiwgYXJncyk7XHJcblxyXG4gICAgdmFyIGxvZ0ZuID0gZGVidWcubG9nIHx8IGV4cG9ydHMubG9nIHx8IGNvbnNvbGUubG9nLmJpbmQoY29uc29sZSk7XHJcbiAgICBsb2dGbi5hcHBseShzZWxmLCBhcmdzKTtcclxuICB9XHJcblxyXG4gIGRlYnVnLm5hbWVzcGFjZSA9IG5hbWVzcGFjZTtcclxuICBkZWJ1Zy5lbmFibGVkID0gZXhwb3J0cy5lbmFibGVkKG5hbWVzcGFjZSk7XHJcbiAgZGVidWcudXNlQ29sb3JzID0gZXhwb3J0cy51c2VDb2xvcnMoKTtcclxuICBkZWJ1Zy5jb2xvciA9IHNlbGVjdENvbG9yKG5hbWVzcGFjZSk7XHJcblxyXG4gIC8vIGVudi1zcGVjaWZpYyBpbml0aWFsaXphdGlvbiBsb2dpYyBmb3IgZGVidWcgaW5zdGFuY2VzXHJcbiAgaWYgKCdmdW5jdGlvbicgPT09IHR5cGVvZiBleHBvcnRzLmluaXQpIHtcclxuICAgIGV4cG9ydHMuaW5pdChkZWJ1Zyk7XHJcbiAgfVxyXG5cclxuICByZXR1cm4gZGVidWc7XHJcbn1cclxuXHJcbi8qKlxyXG4gKiBFbmFibGVzIGEgZGVidWcgbW9kZSBieSBuYW1lc3BhY2VzLiBUaGlzIGNhbiBpbmNsdWRlIG1vZGVzXHJcbiAqIHNlcGFyYXRlZCBieSBhIGNvbG9uIGFuZCB3aWxkY2FyZHMuXHJcbiAqXHJcbiAqIEBwYXJhbSB7U3RyaW5nfSBuYW1lc3BhY2VzXHJcbiAqIEBhcGkgcHVibGljXHJcbiAqL1xyXG5cclxuZnVuY3Rpb24gZW5hYmxlKG5hbWVzcGFjZXMpIHtcclxuICBleHBvcnRzLnNhdmUobmFtZXNwYWNlcyk7XHJcblxyXG4gIGV4cG9ydHMubmFtZXMgPSBbXTtcclxuICBleHBvcnRzLnNraXBzID0gW107XHJcblxyXG4gIHZhciBzcGxpdCA9ICh0eXBlb2YgbmFtZXNwYWNlcyA9PT0gJ3N0cmluZycgPyBuYW1lc3BhY2VzIDogJycpLnNwbGl0KC9bXFxzLF0rLyk7XHJcbiAgdmFyIGxlbiA9IHNwbGl0Lmxlbmd0aDtcclxuXHJcbiAgZm9yICh2YXIgaSA9IDA7IGkgPCBsZW47IGkrKykge1xyXG4gICAgaWYgKCFzcGxpdFtpXSkgY29udGludWU7IC8vIGlnbm9yZSBlbXB0eSBzdHJpbmdzXHJcbiAgICBuYW1lc3BhY2VzID0gc3BsaXRbaV0ucmVwbGFjZSgvXFwqL2csICcuKj8nKTtcclxuICAgIGlmIChuYW1lc3BhY2VzWzBdID09PSAnLScpIHtcclxuICAgICAgZXhwb3J0cy5za2lwcy5wdXNoKG5ldyBSZWdFeHAoJ14nICsgbmFtZXNwYWNlcy5zdWJzdHIoMSkgKyAnJCcpKTtcclxuICAgIH0gZWxzZSB7XHJcbiAgICAgIGV4cG9ydHMubmFtZXMucHVzaChuZXcgUmVnRXhwKCdeJyArIG5hbWVzcGFjZXMgKyAnJCcpKTtcclxuICAgIH1cclxuICB9XHJcbn1cclxuXHJcbi8qKlxyXG4gKiBEaXNhYmxlIGRlYnVnIG91dHB1dC5cclxuICpcclxuICogQGFwaSBwdWJsaWNcclxuICovXHJcblxyXG5mdW5jdGlvbiBkaXNhYmxlKCkge1xyXG4gIGV4cG9ydHMuZW5hYmxlKCcnKTtcclxufVxyXG5cclxuLyoqXHJcbiAqIFJldHVybnMgdHJ1ZSBpZiB0aGUgZ2l2ZW4gbW9kZSBuYW1lIGlzIGVuYWJsZWQsIGZhbHNlIG90aGVyd2lzZS5cclxuICpcclxuICogQHBhcmFtIHtTdHJpbmd9IG5hbWVcclxuICogQHJldHVybiB7Qm9vbGVhbn1cclxuICogQGFwaSBwdWJsaWNcclxuICovXHJcblxyXG5mdW5jdGlvbiBlbmFibGVkKG5hbWUpIHtcclxuICB2YXIgaSwgbGVuO1xyXG4gIGZvciAoaSA9IDAsIGxlbiA9IGV4cG9ydHMuc2tpcHMubGVuZ3RoOyBpIDwgbGVuOyBpKyspIHtcclxuICAgIGlmIChleHBvcnRzLnNraXBzW2ldLnRlc3QobmFtZSkpIHtcclxuICAgICAgcmV0dXJuIGZhbHNlO1xyXG4gICAgfVxyXG4gIH1cclxuICBmb3IgKGkgPSAwLCBsZW4gPSBleHBvcnRzLm5hbWVzLmxlbmd0aDsgaSA8IGxlbjsgaSsrKSB7XHJcbiAgICBpZiAoZXhwb3J0cy5uYW1lc1tpXS50ZXN0KG5hbWUpKSB7XHJcbiAgICAgIHJldHVybiB0cnVlO1xyXG4gICAgfVxyXG4gIH1cclxuICByZXR1cm4gZmFsc2U7XHJcbn1cclxuXHJcbi8qKlxyXG4gKiBDb2VyY2UgYHZhbGAuXHJcbiAqXHJcbiAqIEBwYXJhbSB7TWl4ZWR9IHZhbFxyXG4gKiBAcmV0dXJuIHtNaXhlZH1cclxuICogQGFwaSBwcml2YXRlXHJcbiAqL1xyXG5cclxuZnVuY3Rpb24gY29lcmNlKHZhbCkge1xyXG4gIGlmICh2YWwgaW5zdGFuY2VvZiBFcnJvcikgcmV0dXJuIHZhbC5zdGFjayB8fCB2YWwubWVzc2FnZTtcclxuICByZXR1cm4gdmFsO1xyXG59XHJcblxyXG59LHtcIm1zXCI6NTR9XSw1NzpbZnVuY3Rpb24ocmVxdWlyZSxtb2R1bGUsZXhwb3J0cyl7XHJcbmlmICh0eXBlb2YgT2JqZWN0LmNyZWF0ZSA9PT0gJ2Z1bmN0aW9uJykge1xyXG4gIC8vIGltcGxlbWVudGF0aW9uIGZyb20gc3RhbmRhcmQgbm9kZS5qcyAndXRpbCcgbW9kdWxlXHJcbiAgbW9kdWxlLmV4cG9ydHMgPSBmdW5jdGlvbiBpbmhlcml0cyhjdG9yLCBzdXBlckN0b3IpIHtcclxuICAgIGN0b3Iuc3VwZXJfID0gc3VwZXJDdG9yXHJcbiAgICBjdG9yLnByb3RvdHlwZSA9IE9iamVjdC5jcmVhdGUoc3VwZXJDdG9yLnByb3RvdHlwZSwge1xyXG4gICAgICBjb25zdHJ1Y3Rvcjoge1xyXG4gICAgICAgIHZhbHVlOiBjdG9yLFxyXG4gICAgICAgIGVudW1lcmFibGU6IGZhbHNlLFxyXG4gICAgICAgIHdyaXRhYmxlOiB0cnVlLFxyXG4gICAgICAgIGNvbmZpZ3VyYWJsZTogdHJ1ZVxyXG4gICAgICB9XHJcbiAgICB9KTtcclxuICB9O1xyXG59IGVsc2Uge1xyXG4gIC8vIG9sZCBzY2hvb2wgc2hpbSBmb3Igb2xkIGJyb3dzZXJzXHJcbiAgbW9kdWxlLmV4cG9ydHMgPSBmdW5jdGlvbiBpbmhlcml0cyhjdG9yLCBzdXBlckN0b3IpIHtcclxuICAgIGN0b3Iuc3VwZXJfID0gc3VwZXJDdG9yXHJcbiAgICB2YXIgVGVtcEN0b3IgPSBmdW5jdGlvbiAoKSB7fVxyXG4gICAgVGVtcEN0b3IucHJvdG90eXBlID0gc3VwZXJDdG9yLnByb3RvdHlwZVxyXG4gICAgY3Rvci5wcm90b3R5cGUgPSBuZXcgVGVtcEN0b3IoKVxyXG4gICAgY3Rvci5wcm90b3R5cGUuY29uc3RydWN0b3IgPSBjdG9yXHJcbiAgfVxyXG59XHJcblxyXG59LHt9XSw1ODpbZnVuY3Rpb24ocmVxdWlyZSxtb2R1bGUsZXhwb3J0cyl7XHJcbihmdW5jdGlvbiAoZ2xvYmFsKXtcclxuLyohIEpTT04gdjMuMy4yIHwgaHR0cDovL2Jlc3RpZWpzLmdpdGh1Yi5pby9qc29uMyB8IENvcHlyaWdodCAyMDEyLTIwMTQsIEtpdCBDYW1icmlkZ2UgfCBodHRwOi8va2l0Lm1pdC1saWNlbnNlLm9yZyAqL1xyXG47KGZ1bmN0aW9uICgpIHtcclxuICAvLyBEZXRlY3QgdGhlIGBkZWZpbmVgIGZ1bmN0aW9uIGV4cG9zZWQgYnkgYXN5bmNocm9ub3VzIG1vZHVsZSBsb2FkZXJzLiBUaGVcclxuICAvLyBzdHJpY3QgYGRlZmluZWAgY2hlY2sgaXMgbmVjZXNzYXJ5IGZvciBjb21wYXRpYmlsaXR5IHdpdGggYHIuanNgLlxyXG4gIHZhciBpc0xvYWRlciA9IHR5cGVvZiBkZWZpbmUgPT09IFwiZnVuY3Rpb25cIiAmJiBkZWZpbmUuYW1kO1xyXG5cclxuICAvLyBBIHNldCBvZiB0eXBlcyB1c2VkIHRvIGRpc3Rpbmd1aXNoIG9iamVjdHMgZnJvbSBwcmltaXRpdmVzLlxyXG4gIHZhciBvYmplY3RUeXBlcyA9IHtcclxuICAgIFwiZnVuY3Rpb25cIjogdHJ1ZSxcclxuICAgIFwib2JqZWN0XCI6IHRydWVcclxuICB9O1xyXG5cclxuICAvLyBEZXRlY3QgdGhlIGBleHBvcnRzYCBvYmplY3QgZXhwb3NlZCBieSBDb21tb25KUyBpbXBsZW1lbnRhdGlvbnMuXHJcbiAgdmFyIGZyZWVFeHBvcnRzID0gb2JqZWN0VHlwZXNbdHlwZW9mIGV4cG9ydHNdICYmIGV4cG9ydHMgJiYgIWV4cG9ydHMubm9kZVR5cGUgJiYgZXhwb3J0cztcclxuXHJcbiAgLy8gVXNlIHRoZSBgZ2xvYmFsYCBvYmplY3QgZXhwb3NlZCBieSBOb2RlIChpbmNsdWRpbmcgQnJvd3NlcmlmeSB2aWFcclxuICAvLyBgaW5zZXJ0LW1vZHVsZS1nbG9iYWxzYCksIE5hcndoYWwsIGFuZCBSaW5nbyBhcyB0aGUgZGVmYXVsdCBjb250ZXh0LFxyXG4gIC8vIGFuZCB0aGUgYHdpbmRvd2Agb2JqZWN0IGluIGJyb3dzZXJzLiBSaGlubyBleHBvcnRzIGEgYGdsb2JhbGAgZnVuY3Rpb25cclxuICAvLyBpbnN0ZWFkLlxyXG4gIHZhciByb290ID0gb2JqZWN0VHlwZXNbdHlwZW9mIHdpbmRvd10gJiYgd2luZG93IHx8IHRoaXMsXHJcbiAgICAgIGZyZWVHbG9iYWwgPSBmcmVlRXhwb3J0cyAmJiBvYmplY3RUeXBlc1t0eXBlb2YgbW9kdWxlXSAmJiBtb2R1bGUgJiYgIW1vZHVsZS5ub2RlVHlwZSAmJiB0eXBlb2YgZ2xvYmFsID09IFwib2JqZWN0XCIgJiYgZ2xvYmFsO1xyXG5cclxuICBpZiAoZnJlZUdsb2JhbCAmJiAoZnJlZUdsb2JhbFtcImdsb2JhbFwiXSA9PT0gZnJlZUdsb2JhbCB8fCBmcmVlR2xvYmFsW1wid2luZG93XCJdID09PSBmcmVlR2xvYmFsIHx8IGZyZWVHbG9iYWxbXCJzZWxmXCJdID09PSBmcmVlR2xvYmFsKSkge1xyXG4gICAgcm9vdCA9IGZyZWVHbG9iYWw7XHJcbiAgfVxyXG5cclxuICAvLyBQdWJsaWM6IEluaXRpYWxpemVzIEpTT04gMyB1c2luZyB0aGUgZ2l2ZW4gYGNvbnRleHRgIG9iamVjdCwgYXR0YWNoaW5nIHRoZVxyXG4gIC8vIGBzdHJpbmdpZnlgIGFuZCBgcGFyc2VgIGZ1bmN0aW9ucyB0byB0aGUgc3BlY2lmaWVkIGBleHBvcnRzYCBvYmplY3QuXHJcbiAgZnVuY3Rpb24gcnVuSW5Db250ZXh0KGNvbnRleHQsIGV4cG9ydHMpIHtcclxuICAgIGNvbnRleHQgfHwgKGNvbnRleHQgPSByb290W1wiT2JqZWN0XCJdKCkpO1xyXG4gICAgZXhwb3J0cyB8fCAoZXhwb3J0cyA9IHJvb3RbXCJPYmplY3RcIl0oKSk7XHJcblxyXG4gICAgLy8gTmF0aXZlIGNvbnN0cnVjdG9yIGFsaWFzZXMuXHJcbiAgICB2YXIgTnVtYmVyID0gY29udGV4dFtcIk51bWJlclwiXSB8fCByb290W1wiTnVtYmVyXCJdLFxyXG4gICAgICAgIFN0cmluZyA9IGNvbnRleHRbXCJTdHJpbmdcIl0gfHwgcm9vdFtcIlN0cmluZ1wiXSxcclxuICAgICAgICBPYmplY3QgPSBjb250ZXh0W1wiT2JqZWN0XCJdIHx8IHJvb3RbXCJPYmplY3RcIl0sXHJcbiAgICAgICAgRGF0ZSA9IGNvbnRleHRbXCJEYXRlXCJdIHx8IHJvb3RbXCJEYXRlXCJdLFxyXG4gICAgICAgIFN5bnRheEVycm9yID0gY29udGV4dFtcIlN5bnRheEVycm9yXCJdIHx8IHJvb3RbXCJTeW50YXhFcnJvclwiXSxcclxuICAgICAgICBUeXBlRXJyb3IgPSBjb250ZXh0W1wiVHlwZUVycm9yXCJdIHx8IHJvb3RbXCJUeXBlRXJyb3JcIl0sXHJcbiAgICAgICAgTWF0aCA9IGNvbnRleHRbXCJNYXRoXCJdIHx8IHJvb3RbXCJNYXRoXCJdLFxyXG4gICAgICAgIG5hdGl2ZUpTT04gPSBjb250ZXh0W1wiSlNPTlwiXSB8fCByb290W1wiSlNPTlwiXTtcclxuXHJcbiAgICAvLyBEZWxlZ2F0ZSB0byB0aGUgbmF0aXZlIGBzdHJpbmdpZnlgIGFuZCBgcGFyc2VgIGltcGxlbWVudGF0aW9ucy5cclxuICAgIGlmICh0eXBlb2YgbmF0aXZlSlNPTiA9PSBcIm9iamVjdFwiICYmIG5hdGl2ZUpTT04pIHtcclxuICAgICAgZXhwb3J0cy5zdHJpbmdpZnkgPSBuYXRpdmVKU09OLnN0cmluZ2lmeTtcclxuICAgICAgZXhwb3J0cy5wYXJzZSA9IG5hdGl2ZUpTT04ucGFyc2U7XHJcbiAgICB9XHJcblxyXG4gICAgLy8gQ29udmVuaWVuY2UgYWxpYXNlcy5cclxuICAgIHZhciBvYmplY3RQcm90byA9IE9iamVjdC5wcm90b3R5cGUsXHJcbiAgICAgICAgZ2V0Q2xhc3MgPSBvYmplY3RQcm90by50b1N0cmluZyxcclxuICAgICAgICBpc1Byb3BlcnR5LCBmb3JFYWNoLCB1bmRlZjtcclxuXHJcbiAgICAvLyBUZXN0IHRoZSBgRGF0ZSNnZXRVVEMqYCBtZXRob2RzLiBCYXNlZCBvbiB3b3JrIGJ5IEBZYWZmbGUuXHJcbiAgICB2YXIgaXNFeHRlbmRlZCA9IG5ldyBEYXRlKC0zNTA5ODI3MzM0NTczMjkyKTtcclxuICAgIHRyeSB7XHJcbiAgICAgIC8vIFRoZSBgZ2V0VVRDRnVsbFllYXJgLCBgTW9udGhgLCBhbmQgYERhdGVgIG1ldGhvZHMgcmV0dXJuIG5vbnNlbnNpY2FsXHJcbiAgICAgIC8vIHJlc3VsdHMgZm9yIGNlcnRhaW4gZGF0ZXMgaW4gT3BlcmEgPj0gMTAuNTMuXHJcbiAgICAgIGlzRXh0ZW5kZWQgPSBpc0V4dGVuZGVkLmdldFVUQ0Z1bGxZZWFyKCkgPT0gLTEwOTI1MiAmJiBpc0V4dGVuZGVkLmdldFVUQ01vbnRoKCkgPT09IDAgJiYgaXNFeHRlbmRlZC5nZXRVVENEYXRlKCkgPT09IDEgJiZcclxuICAgICAgICAvLyBTYWZhcmkgPCAyLjAuMiBzdG9yZXMgdGhlIGludGVybmFsIG1pbGxpc2Vjb25kIHRpbWUgdmFsdWUgY29ycmVjdGx5LFxyXG4gICAgICAgIC8vIGJ1dCBjbGlwcyB0aGUgdmFsdWVzIHJldHVybmVkIGJ5IHRoZSBkYXRlIG1ldGhvZHMgdG8gdGhlIHJhbmdlIG9mXHJcbiAgICAgICAgLy8gc2lnbmVkIDMyLWJpdCBpbnRlZ2VycyAoWy0yICoqIDMxLCAyICoqIDMxIC0gMV0pLlxyXG4gICAgICAgIGlzRXh0ZW5kZWQuZ2V0VVRDSG91cnMoKSA9PSAxMCAmJiBpc0V4dGVuZGVkLmdldFVUQ01pbnV0ZXMoKSA9PSAzNyAmJiBpc0V4dGVuZGVkLmdldFVUQ1NlY29uZHMoKSA9PSA2ICYmIGlzRXh0ZW5kZWQuZ2V0VVRDTWlsbGlzZWNvbmRzKCkgPT0gNzA4O1xyXG4gICAgfSBjYXRjaCAoZXhjZXB0aW9uKSB7fVxyXG5cclxuICAgIC8vIEludGVybmFsOiBEZXRlcm1pbmVzIHdoZXRoZXIgdGhlIG5hdGl2ZSBgSlNPTi5zdHJpbmdpZnlgIGFuZCBgcGFyc2VgXHJcbiAgICAvLyBpbXBsZW1lbnRhdGlvbnMgYXJlIHNwZWMtY29tcGxpYW50LiBCYXNlZCBvbiB3b3JrIGJ5IEtlbiBTbnlkZXIuXHJcbiAgICBmdW5jdGlvbiBoYXMobmFtZSkge1xyXG4gICAgICBpZiAoaGFzW25hbWVdICE9PSB1bmRlZikge1xyXG4gICAgICAgIC8vIFJldHVybiBjYWNoZWQgZmVhdHVyZSB0ZXN0IHJlc3VsdC5cclxuICAgICAgICByZXR1cm4gaGFzW25hbWVdO1xyXG4gICAgICB9XHJcbiAgICAgIHZhciBpc1N1cHBvcnRlZDtcclxuICAgICAgaWYgKG5hbWUgPT0gXCJidWctc3RyaW5nLWNoYXItaW5kZXhcIikge1xyXG4gICAgICAgIC8vIElFIDw9IDcgZG9lc24ndCBzdXBwb3J0IGFjY2Vzc2luZyBzdHJpbmcgY2hhcmFjdGVycyB1c2luZyBzcXVhcmVcclxuICAgICAgICAvLyBicmFja2V0IG5vdGF0aW9uLiBJRSA4IG9ubHkgc3VwcG9ydHMgdGhpcyBmb3IgcHJpbWl0aXZlcy5cclxuICAgICAgICBpc1N1cHBvcnRlZCA9IFwiYVwiWzBdICE9IFwiYVwiO1xyXG4gICAgICB9IGVsc2UgaWYgKG5hbWUgPT0gXCJqc29uXCIpIHtcclxuICAgICAgICAvLyBJbmRpY2F0ZXMgd2hldGhlciBib3RoIGBKU09OLnN0cmluZ2lmeWAgYW5kIGBKU09OLnBhcnNlYCBhcmVcclxuICAgICAgICAvLyBzdXBwb3J0ZWQuXHJcbiAgICAgICAgaXNTdXBwb3J0ZWQgPSBoYXMoXCJqc29uLXN0cmluZ2lmeVwiKSAmJiBoYXMoXCJqc29uLXBhcnNlXCIpO1xyXG4gICAgICB9IGVsc2Uge1xyXG4gICAgICAgIHZhciB2YWx1ZSwgc2VyaWFsaXplZCA9ICd7XCJhXCI6WzEsdHJ1ZSxmYWxzZSxudWxsLFwiXFxcXHUwMDAwXFxcXGJcXFxcblxcXFxmXFxcXHJcXFxcdFwiXX0nO1xyXG4gICAgICAgIC8vIFRlc3QgYEpTT04uc3RyaW5naWZ5YC5cclxuICAgICAgICBpZiAobmFtZSA9PSBcImpzb24tc3RyaW5naWZ5XCIpIHtcclxuICAgICAgICAgIHZhciBzdHJpbmdpZnkgPSBleHBvcnRzLnN0cmluZ2lmeSwgc3RyaW5naWZ5U3VwcG9ydGVkID0gdHlwZW9mIHN0cmluZ2lmeSA9PSBcImZ1bmN0aW9uXCIgJiYgaXNFeHRlbmRlZDtcclxuICAgICAgICAgIGlmIChzdHJpbmdpZnlTdXBwb3J0ZWQpIHtcclxuICAgICAgICAgICAgLy8gQSB0ZXN0IGZ1bmN0aW9uIG9iamVjdCB3aXRoIGEgY3VzdG9tIGB0b0pTT05gIG1ldGhvZC5cclxuICAgICAgICAgICAgKHZhbHVlID0gZnVuY3Rpb24gKCkge1xyXG4gICAgICAgICAgICAgIHJldHVybiAxO1xyXG4gICAgICAgICAgICB9KS50b0pTT04gPSB2YWx1ZTtcclxuICAgICAgICAgICAgdHJ5IHtcclxuICAgICAgICAgICAgICBzdHJpbmdpZnlTdXBwb3J0ZWQgPVxyXG4gICAgICAgICAgICAgICAgLy8gRmlyZWZveCAzLjFiMSBhbmQgYjIgc2VyaWFsaXplIHN0cmluZywgbnVtYmVyLCBhbmQgYm9vbGVhblxyXG4gICAgICAgICAgICAgICAgLy8gcHJpbWl0aXZlcyBhcyBvYmplY3QgbGl0ZXJhbHMuXHJcbiAgICAgICAgICAgICAgICBzdHJpbmdpZnkoMCkgPT09IFwiMFwiICYmXHJcbiAgICAgICAgICAgICAgICAvLyBGRiAzLjFiMSwgYjIsIGFuZCBKU09OIDIgc2VyaWFsaXplIHdyYXBwZWQgcHJpbWl0aXZlcyBhcyBvYmplY3RcclxuICAgICAgICAgICAgICAgIC8vIGxpdGVyYWxzLlxyXG4gICAgICAgICAgICAgICAgc3RyaW5naWZ5KG5ldyBOdW1iZXIoKSkgPT09IFwiMFwiICYmXHJcbiAgICAgICAgICAgICAgICBzdHJpbmdpZnkobmV3IFN0cmluZygpKSA9PSAnXCJcIicgJiZcclxuICAgICAgICAgICAgICAgIC8vIEZGIDMuMWIxLCAyIHRocm93IGFuIGVycm9yIGlmIHRoZSB2YWx1ZSBpcyBgbnVsbGAsIGB1bmRlZmluZWRgLCBvclxyXG4gICAgICAgICAgICAgICAgLy8gZG9lcyBub3QgZGVmaW5lIGEgY2Fub25pY2FsIEpTT04gcmVwcmVzZW50YXRpb24gKHRoaXMgYXBwbGllcyB0b1xyXG4gICAgICAgICAgICAgICAgLy8gb2JqZWN0cyB3aXRoIGB0b0pTT05gIHByb3BlcnRpZXMgYXMgd2VsbCwgKnVubGVzcyogdGhleSBhcmUgbmVzdGVkXHJcbiAgICAgICAgICAgICAgICAvLyB3aXRoaW4gYW4gb2JqZWN0IG9yIGFycmF5KS5cclxuICAgICAgICAgICAgICAgIHN0cmluZ2lmeShnZXRDbGFzcykgPT09IHVuZGVmICYmXHJcbiAgICAgICAgICAgICAgICAvLyBJRSA4IHNlcmlhbGl6ZXMgYHVuZGVmaW5lZGAgYXMgYFwidW5kZWZpbmVkXCJgLiBTYWZhcmkgPD0gNS4xLjcgYW5kXHJcbiAgICAgICAgICAgICAgICAvLyBGRiAzLjFiMyBwYXNzIHRoaXMgdGVzdC5cclxuICAgICAgICAgICAgICAgIHN0cmluZ2lmeSh1bmRlZikgPT09IHVuZGVmICYmXHJcbiAgICAgICAgICAgICAgICAvLyBTYWZhcmkgPD0gNS4xLjcgYW5kIEZGIDMuMWIzIHRocm93IGBFcnJvcmBzIGFuZCBgVHlwZUVycm9yYHMsXHJcbiAgICAgICAgICAgICAgICAvLyByZXNwZWN0aXZlbHksIGlmIHRoZSB2YWx1ZSBpcyBvbWl0dGVkIGVudGlyZWx5LlxyXG4gICAgICAgICAgICAgICAgc3RyaW5naWZ5KCkgPT09IHVuZGVmICYmXHJcbiAgICAgICAgICAgICAgICAvLyBGRiAzLjFiMSwgMiB0aHJvdyBhbiBlcnJvciBpZiB0aGUgZ2l2ZW4gdmFsdWUgaXMgbm90IGEgbnVtYmVyLFxyXG4gICAgICAgICAgICAgICAgLy8gc3RyaW5nLCBhcnJheSwgb2JqZWN0LCBCb29sZWFuLCBvciBgbnVsbGAgbGl0ZXJhbC4gVGhpcyBhcHBsaWVzIHRvXHJcbiAgICAgICAgICAgICAgICAvLyBvYmplY3RzIHdpdGggY3VzdG9tIGB0b0pTT05gIG1ldGhvZHMgYXMgd2VsbCwgdW5sZXNzIHRoZXkgYXJlIG5lc3RlZFxyXG4gICAgICAgICAgICAgICAgLy8gaW5zaWRlIG9iamVjdCBvciBhcnJheSBsaXRlcmFscy4gWVVJIDMuMC4wYjEgaWdub3JlcyBjdXN0b20gYHRvSlNPTmBcclxuICAgICAgICAgICAgICAgIC8vIG1ldGhvZHMgZW50aXJlbHkuXHJcbiAgICAgICAgICAgICAgICBzdHJpbmdpZnkodmFsdWUpID09PSBcIjFcIiAmJlxyXG4gICAgICAgICAgICAgICAgc3RyaW5naWZ5KFt2YWx1ZV0pID09IFwiWzFdXCIgJiZcclxuICAgICAgICAgICAgICAgIC8vIFByb3RvdHlwZSA8PSAxLjYuMSBzZXJpYWxpemVzIGBbdW5kZWZpbmVkXWAgYXMgYFwiW11cImAgaW5zdGVhZCBvZlxyXG4gICAgICAgICAgICAgICAgLy8gYFwiW251bGxdXCJgLlxyXG4gICAgICAgICAgICAgICAgc3RyaW5naWZ5KFt1bmRlZl0pID09IFwiW251bGxdXCIgJiZcclxuICAgICAgICAgICAgICAgIC8vIFlVSSAzLjAuMGIxIGZhaWxzIHRvIHNlcmlhbGl6ZSBgbnVsbGAgbGl0ZXJhbHMuXHJcbiAgICAgICAgICAgICAgICBzdHJpbmdpZnkobnVsbCkgPT0gXCJudWxsXCIgJiZcclxuICAgICAgICAgICAgICAgIC8vIEZGIDMuMWIxLCAyIGhhbHRzIHNlcmlhbGl6YXRpb24gaWYgYW4gYXJyYXkgY29udGFpbnMgYSBmdW5jdGlvbjpcclxuICAgICAgICAgICAgICAgIC8vIGBbMSwgdHJ1ZSwgZ2V0Q2xhc3MsIDFdYCBzZXJpYWxpemVzIGFzIFwiWzEsdHJ1ZSxdLFwiLiBGRiAzLjFiM1xyXG4gICAgICAgICAgICAgICAgLy8gZWxpZGVzIG5vbi1KU09OIHZhbHVlcyBmcm9tIG9iamVjdHMgYW5kIGFycmF5cywgdW5sZXNzIHRoZXlcclxuICAgICAgICAgICAgICAgIC8vIGRlZmluZSBjdXN0b20gYHRvSlNPTmAgbWV0aG9kcy5cclxuICAgICAgICAgICAgICAgIHN0cmluZ2lmeShbdW5kZWYsIGdldENsYXNzLCBudWxsXSkgPT0gXCJbbnVsbCxudWxsLG51bGxdXCIgJiZcclxuICAgICAgICAgICAgICAgIC8vIFNpbXBsZSBzZXJpYWxpemF0aW9uIHRlc3QuIEZGIDMuMWIxIHVzZXMgVW5pY29kZSBlc2NhcGUgc2VxdWVuY2VzXHJcbiAgICAgICAgICAgICAgICAvLyB3aGVyZSBjaGFyYWN0ZXIgZXNjYXBlIGNvZGVzIGFyZSBleHBlY3RlZCAoZS5nLiwgYFxcYmAgPT4gYFxcdTAwMDhgKS5cclxuICAgICAgICAgICAgICAgIHN0cmluZ2lmeSh7IFwiYVwiOiBbdmFsdWUsIHRydWUsIGZhbHNlLCBudWxsLCBcIlxceDAwXFxiXFxuXFxmXFxyXFx0XCJdIH0pID09IHNlcmlhbGl6ZWQgJiZcclxuICAgICAgICAgICAgICAgIC8vIEZGIDMuMWIxIGFuZCBiMiBpZ25vcmUgdGhlIGBmaWx0ZXJgIGFuZCBgd2lkdGhgIGFyZ3VtZW50cy5cclxuICAgICAgICAgICAgICAgIHN0cmluZ2lmeShudWxsLCB2YWx1ZSkgPT09IFwiMVwiICYmXHJcbiAgICAgICAgICAgICAgICBzdHJpbmdpZnkoWzEsIDJdLCBudWxsLCAxKSA9PSBcIltcXG4gMSxcXG4gMlxcbl1cIiAmJlxyXG4gICAgICAgICAgICAgICAgLy8gSlNPTiAyLCBQcm90b3R5cGUgPD0gMS43LCBhbmQgb2xkZXIgV2ViS2l0IGJ1aWxkcyBpbmNvcnJlY3RseVxyXG4gICAgICAgICAgICAgICAgLy8gc2VyaWFsaXplIGV4dGVuZGVkIHllYXJzLlxyXG4gICAgICAgICAgICAgICAgc3RyaW5naWZ5KG5ldyBEYXRlKC04LjY0ZTE1KSkgPT0gJ1wiLTI3MTgyMS0wNC0yMFQwMDowMDowMC4wMDBaXCInICYmXHJcbiAgICAgICAgICAgICAgICAvLyBUaGUgbWlsbGlzZWNvbmRzIGFyZSBvcHRpb25hbCBpbiBFUyA1LCBidXQgcmVxdWlyZWQgaW4gNS4xLlxyXG4gICAgICAgICAgICAgICAgc3RyaW5naWZ5KG5ldyBEYXRlKDguNjRlMTUpKSA9PSAnXCIrMjc1NzYwLTA5LTEzVDAwOjAwOjAwLjAwMFpcIicgJiZcclxuICAgICAgICAgICAgICAgIC8vIEZpcmVmb3ggPD0gMTEuMCBpbmNvcnJlY3RseSBzZXJpYWxpemVzIHllYXJzIHByaW9yIHRvIDAgYXMgbmVnYXRpdmVcclxuICAgICAgICAgICAgICAgIC8vIGZvdXItZGlnaXQgeWVhcnMgaW5zdGVhZCBvZiBzaXgtZGlnaXQgeWVhcnMuIENyZWRpdHM6IEBZYWZmbGUuXHJcbiAgICAgICAgICAgICAgICBzdHJpbmdpZnkobmV3IERhdGUoLTYyMTk4NzU1MmU1KSkgPT0gJ1wiLTAwMDAwMS0wMS0wMVQwMDowMDowMC4wMDBaXCInICYmXHJcbiAgICAgICAgICAgICAgICAvLyBTYWZhcmkgPD0gNS4xLjUgYW5kIE9wZXJhID49IDEwLjUzIGluY29ycmVjdGx5IHNlcmlhbGl6ZSBtaWxsaXNlY29uZFxyXG4gICAgICAgICAgICAgICAgLy8gdmFsdWVzIGxlc3MgdGhhbiAxMDAwLiBDcmVkaXRzOiBAWWFmZmxlLlxyXG4gICAgICAgICAgICAgICAgc3RyaW5naWZ5KG5ldyBEYXRlKC0xKSkgPT0gJ1wiMTk2OS0xMi0zMVQyMzo1OTo1OS45OTlaXCInO1xyXG4gICAgICAgICAgICB9IGNhdGNoIChleGNlcHRpb24pIHtcclxuICAgICAgICAgICAgICBzdHJpbmdpZnlTdXBwb3J0ZWQgPSBmYWxzZTtcclxuICAgICAgICAgICAgfVxyXG4gICAgICAgICAgfVxyXG4gICAgICAgICAgaXNTdXBwb3J0ZWQgPSBzdHJpbmdpZnlTdXBwb3J0ZWQ7XHJcbiAgICAgICAgfVxyXG4gICAgICAgIC8vIFRlc3QgYEpTT04ucGFyc2VgLlxyXG4gICAgICAgIGlmIChuYW1lID09IFwianNvbi1wYXJzZVwiKSB7XHJcbiAgICAgICAgICB2YXIgcGFyc2UgPSBleHBvcnRzLnBhcnNlO1xyXG4gICAgICAgICAgaWYgKHR5cGVvZiBwYXJzZSA9PSBcImZ1bmN0aW9uXCIpIHtcclxuICAgICAgICAgICAgdHJ5IHtcclxuICAgICAgICAgICAgICAvLyBGRiAzLjFiMSwgYjIgd2lsbCB0aHJvdyBhbiBleGNlcHRpb24gaWYgYSBiYXJlIGxpdGVyYWwgaXMgcHJvdmlkZWQuXHJcbiAgICAgICAgICAgICAgLy8gQ29uZm9ybWluZyBpbXBsZW1lbnRhdGlvbnMgc2hvdWxkIGFsc28gY29lcmNlIHRoZSBpbml0aWFsIGFyZ3VtZW50IHRvXHJcbiAgICAgICAgICAgICAgLy8gYSBzdHJpbmcgcHJpb3IgdG8gcGFyc2luZy5cclxuICAgICAgICAgICAgICBpZiAocGFyc2UoXCIwXCIpID09PSAwICYmICFwYXJzZShmYWxzZSkpIHtcclxuICAgICAgICAgICAgICAgIC8vIFNpbXBsZSBwYXJzaW5nIHRlc3QuXHJcbiAgICAgICAgICAgICAgICB2YWx1ZSA9IHBhcnNlKHNlcmlhbGl6ZWQpO1xyXG4gICAgICAgICAgICAgICAgdmFyIHBhcnNlU3VwcG9ydGVkID0gdmFsdWVbXCJhXCJdLmxlbmd0aCA9PSA1ICYmIHZhbHVlW1wiYVwiXVswXSA9PT0gMTtcclxuICAgICAgICAgICAgICAgIGlmIChwYXJzZVN1cHBvcnRlZCkge1xyXG4gICAgICAgICAgICAgICAgICB0cnkge1xyXG4gICAgICAgICAgICAgICAgICAgIC8vIFNhZmFyaSA8PSA1LjEuMiBhbmQgRkYgMy4xYjEgYWxsb3cgdW5lc2NhcGVkIHRhYnMgaW4gc3RyaW5ncy5cclxuICAgICAgICAgICAgICAgICAgICBwYXJzZVN1cHBvcnRlZCA9ICFwYXJzZSgnXCJcXHRcIicpO1xyXG4gICAgICAgICAgICAgICAgICB9IGNhdGNoIChleGNlcHRpb24pIHt9XHJcbiAgICAgICAgICAgICAgICAgIGlmIChwYXJzZVN1cHBvcnRlZCkge1xyXG4gICAgICAgICAgICAgICAgICAgIHRyeSB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAvLyBGRiA0LjAgYW5kIDQuMC4xIGFsbG93IGxlYWRpbmcgYCtgIHNpZ25zIGFuZCBsZWFkaW5nXHJcbiAgICAgICAgICAgICAgICAgICAgICAvLyBkZWNpbWFsIHBvaW50cy4gRkYgNC4wLCA0LjAuMSwgYW5kIElFIDktMTAgYWxzbyBhbGxvd1xyXG4gICAgICAgICAgICAgICAgICAgICAgLy8gY2VydGFpbiBvY3RhbCBsaXRlcmFscy5cclxuICAgICAgICAgICAgICAgICAgICAgIHBhcnNlU3VwcG9ydGVkID0gcGFyc2UoXCIwMVwiKSAhPT0gMTtcclxuICAgICAgICAgICAgICAgICAgICB9IGNhdGNoIChleGNlcHRpb24pIHt9XHJcbiAgICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgICAgICAgaWYgKHBhcnNlU3VwcG9ydGVkKSB7XHJcbiAgICAgICAgICAgICAgICAgICAgdHJ5IHtcclxuICAgICAgICAgICAgICAgICAgICAgIC8vIEZGIDQuMCwgNC4wLjEsIGFuZCBSaGlubyAxLjdSMy1SNCBhbGxvdyB0cmFpbGluZyBkZWNpbWFsXHJcbiAgICAgICAgICAgICAgICAgICAgICAvLyBwb2ludHMuIFRoZXNlIGVudmlyb25tZW50cywgYWxvbmcgd2l0aCBGRiAzLjFiMSBhbmQgMixcclxuICAgICAgICAgICAgICAgICAgICAgIC8vIGFsc28gYWxsb3cgdHJhaWxpbmcgY29tbWFzIGluIEpTT04gb2JqZWN0cyBhbmQgYXJyYXlzLlxyXG4gICAgICAgICAgICAgICAgICAgICAgcGFyc2VTdXBwb3J0ZWQgPSBwYXJzZShcIjEuXCIpICE9PSAxO1xyXG4gICAgICAgICAgICAgICAgICAgIH0gY2F0Y2ggKGV4Y2VwdGlvbikge31cclxuICAgICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgfSBjYXRjaCAoZXhjZXB0aW9uKSB7XHJcbiAgICAgICAgICAgICAgcGFyc2VTdXBwb3J0ZWQgPSBmYWxzZTtcclxuICAgICAgICAgICAgfVxyXG4gICAgICAgICAgfVxyXG4gICAgICAgICAgaXNTdXBwb3J0ZWQgPSBwYXJzZVN1cHBvcnRlZDtcclxuICAgICAgICB9XHJcbiAgICAgIH1cclxuICAgICAgcmV0dXJuIGhhc1tuYW1lXSA9ICEhaXNTdXBwb3J0ZWQ7XHJcbiAgICB9XHJcblxyXG4gICAgaWYgKCFoYXMoXCJqc29uXCIpKSB7XHJcbiAgICAgIC8vIENvbW1vbiBgW1tDbGFzc11dYCBuYW1lIGFsaWFzZXMuXHJcbiAgICAgIHZhciBmdW5jdGlvbkNsYXNzID0gXCJbb2JqZWN0IEZ1bmN0aW9uXVwiLFxyXG4gICAgICAgICAgZGF0ZUNsYXNzID0gXCJbb2JqZWN0IERhdGVdXCIsXHJcbiAgICAgICAgICBudW1iZXJDbGFzcyA9IFwiW29iamVjdCBOdW1iZXJdXCIsXHJcbiAgICAgICAgICBzdHJpbmdDbGFzcyA9IFwiW29iamVjdCBTdHJpbmddXCIsXHJcbiAgICAgICAgICBhcnJheUNsYXNzID0gXCJbb2JqZWN0IEFycmF5XVwiLFxyXG4gICAgICAgICAgYm9vbGVhbkNsYXNzID0gXCJbb2JqZWN0IEJvb2xlYW5dXCI7XHJcblxyXG4gICAgICAvLyBEZXRlY3QgaW5jb21wbGV0ZSBzdXBwb3J0IGZvciBhY2Nlc3Npbmcgc3RyaW5nIGNoYXJhY3RlcnMgYnkgaW5kZXguXHJcbiAgICAgIHZhciBjaGFySW5kZXhCdWdneSA9IGhhcyhcImJ1Zy1zdHJpbmctY2hhci1pbmRleFwiKTtcclxuXHJcbiAgICAgIC8vIERlZmluZSBhZGRpdGlvbmFsIHV0aWxpdHkgbWV0aG9kcyBpZiB0aGUgYERhdGVgIG1ldGhvZHMgYXJlIGJ1Z2d5LlxyXG4gICAgICBpZiAoIWlzRXh0ZW5kZWQpIHtcclxuICAgICAgICB2YXIgZmxvb3IgPSBNYXRoLmZsb29yO1xyXG4gICAgICAgIC8vIEEgbWFwcGluZyBiZXR3ZWVuIHRoZSBtb250aHMgb2YgdGhlIHllYXIgYW5kIHRoZSBudW1iZXIgb2YgZGF5cyBiZXR3ZWVuXHJcbiAgICAgICAgLy8gSmFudWFyeSAxc3QgYW5kIHRoZSBmaXJzdCBvZiB0aGUgcmVzcGVjdGl2ZSBtb250aC5cclxuICAgICAgICB2YXIgTW9udGhzID0gWzAsIDMxLCA1OSwgOTAsIDEyMCwgMTUxLCAxODEsIDIxMiwgMjQzLCAyNzMsIDMwNCwgMzM0XTtcclxuICAgICAgICAvLyBJbnRlcm5hbDogQ2FsY3VsYXRlcyB0aGUgbnVtYmVyIG9mIGRheXMgYmV0d2VlbiB0aGUgVW5peCBlcG9jaCBhbmQgdGhlXHJcbiAgICAgICAgLy8gZmlyc3QgZGF5IG9mIHRoZSBnaXZlbiBtb250aC5cclxuICAgICAgICB2YXIgZ2V0RGF5ID0gZnVuY3Rpb24gKHllYXIsIG1vbnRoKSB7XHJcbiAgICAgICAgICByZXR1cm4gTW9udGhzW21vbnRoXSArIDM2NSAqICh5ZWFyIC0gMTk3MCkgKyBmbG9vcigoeWVhciAtIDE5NjkgKyAobW9udGggPSArKG1vbnRoID4gMSkpKSAvIDQpIC0gZmxvb3IoKHllYXIgLSAxOTAxICsgbW9udGgpIC8gMTAwKSArIGZsb29yKCh5ZWFyIC0gMTYwMSArIG1vbnRoKSAvIDQwMCk7XHJcbiAgICAgICAgfTtcclxuICAgICAgfVxyXG5cclxuICAgICAgLy8gSW50ZXJuYWw6IERldGVybWluZXMgaWYgYSBwcm9wZXJ0eSBpcyBhIGRpcmVjdCBwcm9wZXJ0eSBvZiB0aGUgZ2l2ZW5cclxuICAgICAgLy8gb2JqZWN0LiBEZWxlZ2F0ZXMgdG8gdGhlIG5hdGl2ZSBgT2JqZWN0I2hhc093blByb3BlcnR5YCBtZXRob2QuXHJcbiAgICAgIGlmICghKGlzUHJvcGVydHkgPSBvYmplY3RQcm90by5oYXNPd25Qcm9wZXJ0eSkpIHtcclxuICAgICAgICBpc1Byb3BlcnR5ID0gZnVuY3Rpb24gKHByb3BlcnR5KSB7XHJcbiAgICAgICAgICB2YXIgbWVtYmVycyA9IHt9LCBjb25zdHJ1Y3RvcjtcclxuICAgICAgICAgIGlmICgobWVtYmVycy5fX3Byb3RvX18gPSBudWxsLCBtZW1iZXJzLl9fcHJvdG9fXyA9IHtcclxuICAgICAgICAgICAgLy8gVGhlICpwcm90byogcHJvcGVydHkgY2Fubm90IGJlIHNldCBtdWx0aXBsZSB0aW1lcyBpbiByZWNlbnRcclxuICAgICAgICAgICAgLy8gdmVyc2lvbnMgb2YgRmlyZWZveCBhbmQgU2VhTW9ua2V5LlxyXG4gICAgICAgICAgICBcInRvU3RyaW5nXCI6IDFcclxuICAgICAgICAgIH0sIG1lbWJlcnMpLnRvU3RyaW5nICE9IGdldENsYXNzKSB7XHJcbiAgICAgICAgICAgIC8vIFNhZmFyaSA8PSAyLjAuMyBkb2Vzbid0IGltcGxlbWVudCBgT2JqZWN0I2hhc093blByb3BlcnR5YCwgYnV0XHJcbiAgICAgICAgICAgIC8vIHN1cHBvcnRzIHRoZSBtdXRhYmxlICpwcm90byogcHJvcGVydHkuXHJcbiAgICAgICAgICAgIGlzUHJvcGVydHkgPSBmdW5jdGlvbiAocHJvcGVydHkpIHtcclxuICAgICAgICAgICAgICAvLyBDYXB0dXJlIGFuZCBicmVhayB0aGUgb2JqZWN0J3MgcHJvdG90eXBlIGNoYWluIChzZWUgc2VjdGlvbiA4LjYuMlxyXG4gICAgICAgICAgICAgIC8vIG9mIHRoZSBFUyA1LjEgc3BlYykuIFRoZSBwYXJlbnRoZXNpemVkIGV4cHJlc3Npb24gcHJldmVudHMgYW5cclxuICAgICAgICAgICAgICAvLyB1bnNhZmUgdHJhbnNmb3JtYXRpb24gYnkgdGhlIENsb3N1cmUgQ29tcGlsZXIuXHJcbiAgICAgICAgICAgICAgdmFyIG9yaWdpbmFsID0gdGhpcy5fX3Byb3RvX18sIHJlc3VsdCA9IHByb3BlcnR5IGluICh0aGlzLl9fcHJvdG9fXyA9IG51bGwsIHRoaXMpO1xyXG4gICAgICAgICAgICAgIC8vIFJlc3RvcmUgdGhlIG9yaWdpbmFsIHByb3RvdHlwZSBjaGFpbi5cclxuICAgICAgICAgICAgICB0aGlzLl9fcHJvdG9fXyA9IG9yaWdpbmFsO1xyXG4gICAgICAgICAgICAgIHJldHVybiByZXN1bHQ7XHJcbiAgICAgICAgICAgIH07XHJcbiAgICAgICAgICB9IGVsc2Uge1xyXG4gICAgICAgICAgICAvLyBDYXB0dXJlIGEgcmVmZXJlbmNlIHRvIHRoZSB0b3AtbGV2ZWwgYE9iamVjdGAgY29uc3RydWN0b3IuXHJcbiAgICAgICAgICAgIGNvbnN0cnVjdG9yID0gbWVtYmVycy5jb25zdHJ1Y3RvcjtcclxuICAgICAgICAgICAgLy8gVXNlIHRoZSBgY29uc3RydWN0b3JgIHByb3BlcnR5IHRvIHNpbXVsYXRlIGBPYmplY3QjaGFzT3duUHJvcGVydHlgIGluXHJcbiAgICAgICAgICAgIC8vIG90aGVyIGVudmlyb25tZW50cy5cclxuICAgICAgICAgICAgaXNQcm9wZXJ0eSA9IGZ1bmN0aW9uIChwcm9wZXJ0eSkge1xyXG4gICAgICAgICAgICAgIHZhciBwYXJlbnQgPSAodGhpcy5jb25zdHJ1Y3RvciB8fCBjb25zdHJ1Y3RvcikucHJvdG90eXBlO1xyXG4gICAgICAgICAgICAgIHJldHVybiBwcm9wZXJ0eSBpbiB0aGlzICYmICEocHJvcGVydHkgaW4gcGFyZW50ICYmIHRoaXNbcHJvcGVydHldID09PSBwYXJlbnRbcHJvcGVydHldKTtcclxuICAgICAgICAgICAgfTtcclxuICAgICAgICAgIH1cclxuICAgICAgICAgIG1lbWJlcnMgPSBudWxsO1xyXG4gICAgICAgICAgcmV0dXJuIGlzUHJvcGVydHkuY2FsbCh0aGlzLCBwcm9wZXJ0eSk7XHJcbiAgICAgICAgfTtcclxuICAgICAgfVxyXG5cclxuICAgICAgLy8gSW50ZXJuYWw6IE5vcm1hbGl6ZXMgdGhlIGBmb3IuLi5pbmAgaXRlcmF0aW9uIGFsZ29yaXRobSBhY3Jvc3NcclxuICAgICAgLy8gZW52aXJvbm1lbnRzLiBFYWNoIGVudW1lcmF0ZWQga2V5IGlzIHlpZWxkZWQgdG8gYSBgY2FsbGJhY2tgIGZ1bmN0aW9uLlxyXG4gICAgICBmb3JFYWNoID0gZnVuY3Rpb24gKG9iamVjdCwgY2FsbGJhY2spIHtcclxuICAgICAgICB2YXIgc2l6ZSA9IDAsIFByb3BlcnRpZXMsIG1lbWJlcnMsIHByb3BlcnR5O1xyXG5cclxuICAgICAgICAvLyBUZXN0cyBmb3IgYnVncyBpbiB0aGUgY3VycmVudCBlbnZpcm9ubWVudCdzIGBmb3IuLi5pbmAgYWxnb3JpdGhtLiBUaGVcclxuICAgICAgICAvLyBgdmFsdWVPZmAgcHJvcGVydHkgaW5oZXJpdHMgdGhlIG5vbi1lbnVtZXJhYmxlIGZsYWcgZnJvbVxyXG4gICAgICAgIC8vIGBPYmplY3QucHJvdG90eXBlYCBpbiBvbGRlciB2ZXJzaW9ucyBvZiBJRSwgTmV0c2NhcGUsIGFuZCBNb3ppbGxhLlxyXG4gICAgICAgIChQcm9wZXJ0aWVzID0gZnVuY3Rpb24gKCkge1xyXG4gICAgICAgICAgdGhpcy52YWx1ZU9mID0gMDtcclxuICAgICAgICB9KS5wcm90b3R5cGUudmFsdWVPZiA9IDA7XHJcblxyXG4gICAgICAgIC8vIEl0ZXJhdGUgb3ZlciBhIG5ldyBpbnN0YW5jZSBvZiB0aGUgYFByb3BlcnRpZXNgIGNsYXNzLlxyXG4gICAgICAgIG1lbWJlcnMgPSBuZXcgUHJvcGVydGllcygpO1xyXG4gICAgICAgIGZvciAocHJvcGVydHkgaW4gbWVtYmVycykge1xyXG4gICAgICAgICAgLy8gSWdub3JlIGFsbCBwcm9wZXJ0aWVzIGluaGVyaXRlZCBmcm9tIGBPYmplY3QucHJvdG90eXBlYC5cclxuICAgICAgICAgIGlmIChpc1Byb3BlcnR5LmNhbGwobWVtYmVycywgcHJvcGVydHkpKSB7XHJcbiAgICAgICAgICAgIHNpemUrKztcclxuICAgICAgICAgIH1cclxuICAgICAgICB9XHJcbiAgICAgICAgUHJvcGVydGllcyA9IG1lbWJlcnMgPSBudWxsO1xyXG5cclxuICAgICAgICAvLyBOb3JtYWxpemUgdGhlIGl0ZXJhdGlvbiBhbGdvcml0aG0uXHJcbiAgICAgICAgaWYgKCFzaXplKSB7XHJcbiAgICAgICAgICAvLyBBIGxpc3Qgb2Ygbm9uLWVudW1lcmFibGUgcHJvcGVydGllcyBpbmhlcml0ZWQgZnJvbSBgT2JqZWN0LnByb3RvdHlwZWAuXHJcbiAgICAgICAgICBtZW1iZXJzID0gW1widmFsdWVPZlwiLCBcInRvU3RyaW5nXCIsIFwidG9Mb2NhbGVTdHJpbmdcIiwgXCJwcm9wZXJ0eUlzRW51bWVyYWJsZVwiLCBcImlzUHJvdG90eXBlT2ZcIiwgXCJoYXNPd25Qcm9wZXJ0eVwiLCBcImNvbnN0cnVjdG9yXCJdO1xyXG4gICAgICAgICAgLy8gSUUgPD0gOCwgTW96aWxsYSAxLjAsIGFuZCBOZXRzY2FwZSA2LjIgaWdub3JlIHNoYWRvd2VkIG5vbi1lbnVtZXJhYmxlXHJcbiAgICAgICAgICAvLyBwcm9wZXJ0aWVzLlxyXG4gICAgICAgICAgZm9yRWFjaCA9IGZ1bmN0aW9uIChvYmplY3QsIGNhbGxiYWNrKSB7XHJcbiAgICAgICAgICAgIHZhciBpc0Z1bmN0aW9uID0gZ2V0Q2xhc3MuY2FsbChvYmplY3QpID09IGZ1bmN0aW9uQ2xhc3MsIHByb3BlcnR5LCBsZW5ndGg7XHJcbiAgICAgICAgICAgIHZhciBoYXNQcm9wZXJ0eSA9ICFpc0Z1bmN0aW9uICYmIHR5cGVvZiBvYmplY3QuY29uc3RydWN0b3IgIT0gXCJmdW5jdGlvblwiICYmIG9iamVjdFR5cGVzW3R5cGVvZiBvYmplY3QuaGFzT3duUHJvcGVydHldICYmIG9iamVjdC5oYXNPd25Qcm9wZXJ0eSB8fCBpc1Byb3BlcnR5O1xyXG4gICAgICAgICAgICBmb3IgKHByb3BlcnR5IGluIG9iamVjdCkge1xyXG4gICAgICAgICAgICAgIC8vIEdlY2tvIDw9IDEuMCBlbnVtZXJhdGVzIHRoZSBgcHJvdG90eXBlYCBwcm9wZXJ0eSBvZiBmdW5jdGlvbnMgdW5kZXJcclxuICAgICAgICAgICAgICAvLyBjZXJ0YWluIGNvbmRpdGlvbnM7IElFIGRvZXMgbm90LlxyXG4gICAgICAgICAgICAgIGlmICghKGlzRnVuY3Rpb24gJiYgcHJvcGVydHkgPT0gXCJwcm90b3R5cGVcIikgJiYgaGFzUHJvcGVydHkuY2FsbChvYmplY3QsIHByb3BlcnR5KSkge1xyXG4gICAgICAgICAgICAgICAgY2FsbGJhY2socHJvcGVydHkpO1xyXG4gICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICAvLyBNYW51YWxseSBpbnZva2UgdGhlIGNhbGxiYWNrIGZvciBlYWNoIG5vbi1lbnVtZXJhYmxlIHByb3BlcnR5LlxyXG4gICAgICAgICAgICBmb3IgKGxlbmd0aCA9IG1lbWJlcnMubGVuZ3RoOyBwcm9wZXJ0eSA9IG1lbWJlcnNbLS1sZW5ndGhdOyBoYXNQcm9wZXJ0eS5jYWxsKG9iamVjdCwgcHJvcGVydHkpICYmIGNhbGxiYWNrKHByb3BlcnR5KSk7XHJcbiAgICAgICAgICB9O1xyXG4gICAgICAgIH0gZWxzZSBpZiAoc2l6ZSA9PSAyKSB7XHJcbiAgICAgICAgICAvLyBTYWZhcmkgPD0gMi4wLjQgZW51bWVyYXRlcyBzaGFkb3dlZCBwcm9wZXJ0aWVzIHR3aWNlLlxyXG4gICAgICAgICAgZm9yRWFjaCA9IGZ1bmN0aW9uIChvYmplY3QsIGNhbGxiYWNrKSB7XHJcbiAgICAgICAgICAgIC8vIENyZWF0ZSBhIHNldCBvZiBpdGVyYXRlZCBwcm9wZXJ0aWVzLlxyXG4gICAgICAgICAgICB2YXIgbWVtYmVycyA9IHt9LCBpc0Z1bmN0aW9uID0gZ2V0Q2xhc3MuY2FsbChvYmplY3QpID09IGZ1bmN0aW9uQ2xhc3MsIHByb3BlcnR5O1xyXG4gICAgICAgICAgICBmb3IgKHByb3BlcnR5IGluIG9iamVjdCkge1xyXG4gICAgICAgICAgICAgIC8vIFN0b3JlIGVhY2ggcHJvcGVydHkgbmFtZSB0byBwcmV2ZW50IGRvdWJsZSBlbnVtZXJhdGlvbi4gVGhlXHJcbiAgICAgICAgICAgICAgLy8gYHByb3RvdHlwZWAgcHJvcGVydHkgb2YgZnVuY3Rpb25zIGlzIG5vdCBlbnVtZXJhdGVkIGR1ZSB0byBjcm9zcy1cclxuICAgICAgICAgICAgICAvLyBlbnZpcm9ubWVudCBpbmNvbnNpc3RlbmNpZXMuXHJcbiAgICAgICAgICAgICAgaWYgKCEoaXNGdW5jdGlvbiAmJiBwcm9wZXJ0eSA9PSBcInByb3RvdHlwZVwiKSAmJiAhaXNQcm9wZXJ0eS5jYWxsKG1lbWJlcnMsIHByb3BlcnR5KSAmJiAobWVtYmVyc1twcm9wZXJ0eV0gPSAxKSAmJiBpc1Byb3BlcnR5LmNhbGwob2JqZWN0LCBwcm9wZXJ0eSkpIHtcclxuICAgICAgICAgICAgICAgIGNhbGxiYWNrKHByb3BlcnR5KTtcclxuICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgIH1cclxuICAgICAgICAgIH07XHJcbiAgICAgICAgfSBlbHNlIHtcclxuICAgICAgICAgIC8vIE5vIGJ1Z3MgZGV0ZWN0ZWQ7IHVzZSB0aGUgc3RhbmRhcmQgYGZvci4uLmluYCBhbGdvcml0aG0uXHJcbiAgICAgICAgICBmb3JFYWNoID0gZnVuY3Rpb24gKG9iamVjdCwgY2FsbGJhY2spIHtcclxuICAgICAgICAgICAgdmFyIGlzRnVuY3Rpb24gPSBnZXRDbGFzcy5jYWxsKG9iamVjdCkgPT0gZnVuY3Rpb25DbGFzcywgcHJvcGVydHksIGlzQ29uc3RydWN0b3I7XHJcbiAgICAgICAgICAgIGZvciAocHJvcGVydHkgaW4gb2JqZWN0KSB7XHJcbiAgICAgICAgICAgICAgaWYgKCEoaXNGdW5jdGlvbiAmJiBwcm9wZXJ0eSA9PSBcInByb3RvdHlwZVwiKSAmJiBpc1Byb3BlcnR5LmNhbGwob2JqZWN0LCBwcm9wZXJ0eSkgJiYgIShpc0NvbnN0cnVjdG9yID0gcHJvcGVydHkgPT09IFwiY29uc3RydWN0b3JcIikpIHtcclxuICAgICAgICAgICAgICAgIGNhbGxiYWNrKHByb3BlcnR5KTtcclxuICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgLy8gTWFudWFsbHkgaW52b2tlIHRoZSBjYWxsYmFjayBmb3IgdGhlIGBjb25zdHJ1Y3RvcmAgcHJvcGVydHkgZHVlIHRvXHJcbiAgICAgICAgICAgIC8vIGNyb3NzLWVudmlyb25tZW50IGluY29uc2lzdGVuY2llcy5cclxuICAgICAgICAgICAgaWYgKGlzQ29uc3RydWN0b3IgfHwgaXNQcm9wZXJ0eS5jYWxsKG9iamVjdCwgKHByb3BlcnR5ID0gXCJjb25zdHJ1Y3RvclwiKSkpIHtcclxuICAgICAgICAgICAgICBjYWxsYmFjayhwcm9wZXJ0eSk7XHJcbiAgICAgICAgICAgIH1cclxuICAgICAgICAgIH07XHJcbiAgICAgICAgfVxyXG4gICAgICAgIHJldHVybiBmb3JFYWNoKG9iamVjdCwgY2FsbGJhY2spO1xyXG4gICAgICB9O1xyXG5cclxuICAgICAgLy8gUHVibGljOiBTZXJpYWxpemVzIGEgSmF2YVNjcmlwdCBgdmFsdWVgIGFzIGEgSlNPTiBzdHJpbmcuIFRoZSBvcHRpb25hbFxyXG4gICAgICAvLyBgZmlsdGVyYCBhcmd1bWVudCBtYXkgc3BlY2lmeSBlaXRoZXIgYSBmdW5jdGlvbiB0aGF0IGFsdGVycyBob3cgb2JqZWN0IGFuZFxyXG4gICAgICAvLyBhcnJheSBtZW1iZXJzIGFyZSBzZXJpYWxpemVkLCBvciBhbiBhcnJheSBvZiBzdHJpbmdzIGFuZCBudW1iZXJzIHRoYXRcclxuICAgICAgLy8gaW5kaWNhdGVzIHdoaWNoIHByb3BlcnRpZXMgc2hvdWxkIGJlIHNlcmlhbGl6ZWQuIFRoZSBvcHRpb25hbCBgd2lkdGhgXHJcbiAgICAgIC8vIGFyZ3VtZW50IG1heSBiZSBlaXRoZXIgYSBzdHJpbmcgb3IgbnVtYmVyIHRoYXQgc3BlY2lmaWVzIHRoZSBpbmRlbnRhdGlvblxyXG4gICAgICAvLyBsZXZlbCBvZiB0aGUgb3V0cHV0LlxyXG4gICAgICBpZiAoIWhhcyhcImpzb24tc3RyaW5naWZ5XCIpKSB7XHJcbiAgICAgICAgLy8gSW50ZXJuYWw6IEEgbWFwIG9mIGNvbnRyb2wgY2hhcmFjdGVycyBhbmQgdGhlaXIgZXNjYXBlZCBlcXVpdmFsZW50cy5cclxuICAgICAgICB2YXIgRXNjYXBlcyA9IHtcclxuICAgICAgICAgIDkyOiBcIlxcXFxcXFxcXCIsXHJcbiAgICAgICAgICAzNDogJ1xcXFxcIicsXHJcbiAgICAgICAgICA4OiBcIlxcXFxiXCIsXHJcbiAgICAgICAgICAxMjogXCJcXFxcZlwiLFxyXG4gICAgICAgICAgMTA6IFwiXFxcXG5cIixcclxuICAgICAgICAgIDEzOiBcIlxcXFxyXCIsXHJcbiAgICAgICAgICA5OiBcIlxcXFx0XCJcclxuICAgICAgICB9O1xyXG5cclxuICAgICAgICAvLyBJbnRlcm5hbDogQ29udmVydHMgYHZhbHVlYCBpbnRvIGEgemVyby1wYWRkZWQgc3RyaW5nIHN1Y2ggdGhhdCBpdHNcclxuICAgICAgICAvLyBsZW5ndGggaXMgYXQgbGVhc3QgZXF1YWwgdG8gYHdpZHRoYC4gVGhlIGB3aWR0aGAgbXVzdCBiZSA8PSA2LlxyXG4gICAgICAgIHZhciBsZWFkaW5nWmVyb2VzID0gXCIwMDAwMDBcIjtcclxuICAgICAgICB2YXIgdG9QYWRkZWRTdHJpbmcgPSBmdW5jdGlvbiAod2lkdGgsIHZhbHVlKSB7XHJcbiAgICAgICAgICAvLyBUaGUgYHx8IDBgIGV4cHJlc3Npb24gaXMgbmVjZXNzYXJ5IHRvIHdvcmsgYXJvdW5kIGEgYnVnIGluXHJcbiAgICAgICAgICAvLyBPcGVyYSA8PSA3LjU0dTIgd2hlcmUgYDAgPT0gLTBgLCBidXQgYFN0cmluZygtMCkgIT09IFwiMFwiYC5cclxuICAgICAgICAgIHJldHVybiAobGVhZGluZ1plcm9lcyArICh2YWx1ZSB8fCAwKSkuc2xpY2UoLXdpZHRoKTtcclxuICAgICAgICB9O1xyXG5cclxuICAgICAgICAvLyBJbnRlcm5hbDogRG91YmxlLXF1b3RlcyBhIHN0cmluZyBgdmFsdWVgLCByZXBsYWNpbmcgYWxsIEFTQ0lJIGNvbnRyb2xcclxuICAgICAgICAvLyBjaGFyYWN0ZXJzIChjaGFyYWN0ZXJzIHdpdGggY29kZSB1bml0IHZhbHVlcyBiZXR3ZWVuIDAgYW5kIDMxKSB3aXRoXHJcbiAgICAgICAgLy8gdGhlaXIgZXNjYXBlZCBlcXVpdmFsZW50cy4gVGhpcyBpcyBhbiBpbXBsZW1lbnRhdGlvbiBvZiB0aGVcclxuICAgICAgICAvLyBgUXVvdGUodmFsdWUpYCBvcGVyYXRpb24gZGVmaW5lZCBpbiBFUyA1LjEgc2VjdGlvbiAxNS4xMi4zLlxyXG4gICAgICAgIHZhciB1bmljb2RlUHJlZml4ID0gXCJcXFxcdTAwXCI7XHJcbiAgICAgICAgdmFyIHF1b3RlID0gZnVuY3Rpb24gKHZhbHVlKSB7XHJcbiAgICAgICAgICB2YXIgcmVzdWx0ID0gJ1wiJywgaW5kZXggPSAwLCBsZW5ndGggPSB2YWx1ZS5sZW5ndGgsIHVzZUNoYXJJbmRleCA9ICFjaGFySW5kZXhCdWdneSB8fCBsZW5ndGggPiAxMDtcclxuICAgICAgICAgIHZhciBzeW1ib2xzID0gdXNlQ2hhckluZGV4ICYmIChjaGFySW5kZXhCdWdneSA/IHZhbHVlLnNwbGl0KFwiXCIpIDogdmFsdWUpO1xyXG4gICAgICAgICAgZm9yICg7IGluZGV4IDwgbGVuZ3RoOyBpbmRleCsrKSB7XHJcbiAgICAgICAgICAgIHZhciBjaGFyQ29kZSA9IHZhbHVlLmNoYXJDb2RlQXQoaW5kZXgpO1xyXG4gICAgICAgICAgICAvLyBJZiB0aGUgY2hhcmFjdGVyIGlzIGEgY29udHJvbCBjaGFyYWN0ZXIsIGFwcGVuZCBpdHMgVW5pY29kZSBvclxyXG4gICAgICAgICAgICAvLyBzaG9ydGhhbmQgZXNjYXBlIHNlcXVlbmNlOyBvdGhlcndpc2UsIGFwcGVuZCB0aGUgY2hhcmFjdGVyIGFzLWlzLlxyXG4gICAgICAgICAgICBzd2l0Y2ggKGNoYXJDb2RlKSB7XHJcbiAgICAgICAgICAgICAgY2FzZSA4OiBjYXNlIDk6IGNhc2UgMTA6IGNhc2UgMTI6IGNhc2UgMTM6IGNhc2UgMzQ6IGNhc2UgOTI6XHJcbiAgICAgICAgICAgICAgICByZXN1bHQgKz0gRXNjYXBlc1tjaGFyQ29kZV07XHJcbiAgICAgICAgICAgICAgICBicmVhaztcclxuICAgICAgICAgICAgICBkZWZhdWx0OlxyXG4gICAgICAgICAgICAgICAgaWYgKGNoYXJDb2RlIDwgMzIpIHtcclxuICAgICAgICAgICAgICAgICAgcmVzdWx0ICs9IHVuaWNvZGVQcmVmaXggKyB0b1BhZGRlZFN0cmluZygyLCBjaGFyQ29kZS50b1N0cmluZygxNikpO1xyXG4gICAgICAgICAgICAgICAgICBicmVhaztcclxuICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgICAgIHJlc3VsdCArPSB1c2VDaGFySW5kZXggPyBzeW1ib2xzW2luZGV4XSA6IHZhbHVlLmNoYXJBdChpbmRleCk7XHJcbiAgICAgICAgICAgIH1cclxuICAgICAgICAgIH1cclxuICAgICAgICAgIHJldHVybiByZXN1bHQgKyAnXCInO1xyXG4gICAgICAgIH07XHJcblxyXG4gICAgICAgIC8vIEludGVybmFsOiBSZWN1cnNpdmVseSBzZXJpYWxpemVzIGFuIG9iamVjdC4gSW1wbGVtZW50cyB0aGVcclxuICAgICAgICAvLyBgU3RyKGtleSwgaG9sZGVyKWAsIGBKTyh2YWx1ZSlgLCBhbmQgYEpBKHZhbHVlKWAgb3BlcmF0aW9ucy5cclxuICAgICAgICB2YXIgc2VyaWFsaXplID0gZnVuY3Rpb24gKHByb3BlcnR5LCBvYmplY3QsIGNhbGxiYWNrLCBwcm9wZXJ0aWVzLCB3aGl0ZXNwYWNlLCBpbmRlbnRhdGlvbiwgc3RhY2spIHtcclxuICAgICAgICAgIHZhciB2YWx1ZSwgY2xhc3NOYW1lLCB5ZWFyLCBtb250aCwgZGF0ZSwgdGltZSwgaG91cnMsIG1pbnV0ZXMsIHNlY29uZHMsIG1pbGxpc2Vjb25kcywgcmVzdWx0cywgZWxlbWVudCwgaW5kZXgsIGxlbmd0aCwgcHJlZml4LCByZXN1bHQ7XHJcbiAgICAgICAgICB0cnkge1xyXG4gICAgICAgICAgICAvLyBOZWNlc3NhcnkgZm9yIGhvc3Qgb2JqZWN0IHN1cHBvcnQuXHJcbiAgICAgICAgICAgIHZhbHVlID0gb2JqZWN0W3Byb3BlcnR5XTtcclxuICAgICAgICAgIH0gY2F0Y2ggKGV4Y2VwdGlvbikge31cclxuICAgICAgICAgIGlmICh0eXBlb2YgdmFsdWUgPT0gXCJvYmplY3RcIiAmJiB2YWx1ZSkge1xyXG4gICAgICAgICAgICBjbGFzc05hbWUgPSBnZXRDbGFzcy5jYWxsKHZhbHVlKTtcclxuICAgICAgICAgICAgaWYgKGNsYXNzTmFtZSA9PSBkYXRlQ2xhc3MgJiYgIWlzUHJvcGVydHkuY2FsbCh2YWx1ZSwgXCJ0b0pTT05cIikpIHtcclxuICAgICAgICAgICAgICBpZiAodmFsdWUgPiAtMSAvIDAgJiYgdmFsdWUgPCAxIC8gMCkge1xyXG4gICAgICAgICAgICAgICAgLy8gRGF0ZXMgYXJlIHNlcmlhbGl6ZWQgYWNjb3JkaW5nIHRvIHRoZSBgRGF0ZSN0b0pTT05gIG1ldGhvZFxyXG4gICAgICAgICAgICAgICAgLy8gc3BlY2lmaWVkIGluIEVTIDUuMSBzZWN0aW9uIDE1LjkuNS40NC4gU2VlIHNlY3Rpb24gMTUuOS4xLjE1XHJcbiAgICAgICAgICAgICAgICAvLyBmb3IgdGhlIElTTyA4NjAxIGRhdGUgdGltZSBzdHJpbmcgZm9ybWF0LlxyXG4gICAgICAgICAgICAgICAgaWYgKGdldERheSkge1xyXG4gICAgICAgICAgICAgICAgICAvLyBNYW51YWxseSBjb21wdXRlIHRoZSB5ZWFyLCBtb250aCwgZGF0ZSwgaG91cnMsIG1pbnV0ZXMsXHJcbiAgICAgICAgICAgICAgICAgIC8vIHNlY29uZHMsIGFuZCBtaWxsaXNlY29uZHMgaWYgdGhlIGBnZXRVVEMqYCBtZXRob2RzIGFyZVxyXG4gICAgICAgICAgICAgICAgICAvLyBidWdneS4gQWRhcHRlZCBmcm9tIEBZYWZmbGUncyBgZGF0ZS1zaGltYCBwcm9qZWN0LlxyXG4gICAgICAgICAgICAgICAgICBkYXRlID0gZmxvb3IodmFsdWUgLyA4NjRlNSk7XHJcbiAgICAgICAgICAgICAgICAgIGZvciAoeWVhciA9IGZsb29yKGRhdGUgLyAzNjUuMjQyNSkgKyAxOTcwIC0gMTsgZ2V0RGF5KHllYXIgKyAxLCAwKSA8PSBkYXRlOyB5ZWFyKyspO1xyXG4gICAgICAgICAgICAgICAgICBmb3IgKG1vbnRoID0gZmxvb3IoKGRhdGUgLSBnZXREYXkoeWVhciwgMCkpIC8gMzAuNDIpOyBnZXREYXkoeWVhciwgbW9udGggKyAxKSA8PSBkYXRlOyBtb250aCsrKTtcclxuICAgICAgICAgICAgICAgICAgZGF0ZSA9IDEgKyBkYXRlIC0gZ2V0RGF5KHllYXIsIG1vbnRoKTtcclxuICAgICAgICAgICAgICAgICAgLy8gVGhlIGB0aW1lYCB2YWx1ZSBzcGVjaWZpZXMgdGhlIHRpbWUgd2l0aGluIHRoZSBkYXkgKHNlZSBFU1xyXG4gICAgICAgICAgICAgICAgICAvLyA1LjEgc2VjdGlvbiAxNS45LjEuMikuIFRoZSBmb3JtdWxhIGAoQSAlIEIgKyBCKSAlIEJgIGlzIHVzZWRcclxuICAgICAgICAgICAgICAgICAgLy8gdG8gY29tcHV0ZSBgQSBtb2R1bG8gQmAsIGFzIHRoZSBgJWAgb3BlcmF0b3IgZG9lcyBub3RcclxuICAgICAgICAgICAgICAgICAgLy8gY29ycmVzcG9uZCB0byB0aGUgYG1vZHVsb2Agb3BlcmF0aW9uIGZvciBuZWdhdGl2ZSBudW1iZXJzLlxyXG4gICAgICAgICAgICAgICAgICB0aW1lID0gKHZhbHVlICUgODY0ZTUgKyA4NjRlNSkgJSA4NjRlNTtcclxuICAgICAgICAgICAgICAgICAgLy8gVGhlIGhvdXJzLCBtaW51dGVzLCBzZWNvbmRzLCBhbmQgbWlsbGlzZWNvbmRzIGFyZSBvYnRhaW5lZCBieVxyXG4gICAgICAgICAgICAgICAgICAvLyBkZWNvbXBvc2luZyB0aGUgdGltZSB3aXRoaW4gdGhlIGRheS4gU2VlIHNlY3Rpb24gMTUuOS4xLjEwLlxyXG4gICAgICAgICAgICAgICAgICBob3VycyA9IGZsb29yKHRpbWUgLyAzNmU1KSAlIDI0O1xyXG4gICAgICAgICAgICAgICAgICBtaW51dGVzID0gZmxvb3IodGltZSAvIDZlNCkgJSA2MDtcclxuICAgICAgICAgICAgICAgICAgc2Vjb25kcyA9IGZsb29yKHRpbWUgLyAxZTMpICUgNjA7XHJcbiAgICAgICAgICAgICAgICAgIG1pbGxpc2Vjb25kcyA9IHRpbWUgJSAxZTM7XHJcbiAgICAgICAgICAgICAgICB9IGVsc2Uge1xyXG4gICAgICAgICAgICAgICAgICB5ZWFyID0gdmFsdWUuZ2V0VVRDRnVsbFllYXIoKTtcclxuICAgICAgICAgICAgICAgICAgbW9udGggPSB2YWx1ZS5nZXRVVENNb250aCgpO1xyXG4gICAgICAgICAgICAgICAgICBkYXRlID0gdmFsdWUuZ2V0VVRDRGF0ZSgpO1xyXG4gICAgICAgICAgICAgICAgICBob3VycyA9IHZhbHVlLmdldFVUQ0hvdXJzKCk7XHJcbiAgICAgICAgICAgICAgICAgIG1pbnV0ZXMgPSB2YWx1ZS5nZXRVVENNaW51dGVzKCk7XHJcbiAgICAgICAgICAgICAgICAgIHNlY29uZHMgPSB2YWx1ZS5nZXRVVENTZWNvbmRzKCk7XHJcbiAgICAgICAgICAgICAgICAgIG1pbGxpc2Vjb25kcyA9IHZhbHVlLmdldFVUQ01pbGxpc2Vjb25kcygpO1xyXG4gICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICAgICAgLy8gU2VyaWFsaXplIGV4dGVuZGVkIHllYXJzIGNvcnJlY3RseS5cclxuICAgICAgICAgICAgICAgIHZhbHVlID0gKHllYXIgPD0gMCB8fCB5ZWFyID49IDFlNCA/ICh5ZWFyIDwgMCA/IFwiLVwiIDogXCIrXCIpICsgdG9QYWRkZWRTdHJpbmcoNiwgeWVhciA8IDAgPyAteWVhciA6IHllYXIpIDogdG9QYWRkZWRTdHJpbmcoNCwgeWVhcikpICtcclxuICAgICAgICAgICAgICAgICAgXCItXCIgKyB0b1BhZGRlZFN0cmluZygyLCBtb250aCArIDEpICsgXCItXCIgKyB0b1BhZGRlZFN0cmluZygyLCBkYXRlKSArXHJcbiAgICAgICAgICAgICAgICAgIC8vIE1vbnRocywgZGF0ZXMsIGhvdXJzLCBtaW51dGVzLCBhbmQgc2Vjb25kcyBzaG91bGQgaGF2ZSB0d29cclxuICAgICAgICAgICAgICAgICAgLy8gZGlnaXRzOyBtaWxsaXNlY29uZHMgc2hvdWxkIGhhdmUgdGhyZWUuXHJcbiAgICAgICAgICAgICAgICAgIFwiVFwiICsgdG9QYWRkZWRTdHJpbmcoMiwgaG91cnMpICsgXCI6XCIgKyB0b1BhZGRlZFN0cmluZygyLCBtaW51dGVzKSArIFwiOlwiICsgdG9QYWRkZWRTdHJpbmcoMiwgc2Vjb25kcykgK1xyXG4gICAgICAgICAgICAgICAgICAvLyBNaWxsaXNlY29uZHMgYXJlIG9wdGlvbmFsIGluIEVTIDUuMCwgYnV0IHJlcXVpcmVkIGluIDUuMS5cclxuICAgICAgICAgICAgICAgICAgXCIuXCIgKyB0b1BhZGRlZFN0cmluZygzLCBtaWxsaXNlY29uZHMpICsgXCJaXCI7XHJcbiAgICAgICAgICAgICAgfSBlbHNlIHtcclxuICAgICAgICAgICAgICAgIHZhbHVlID0gbnVsbDtcclxuICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgIH0gZWxzZSBpZiAodHlwZW9mIHZhbHVlLnRvSlNPTiA9PSBcImZ1bmN0aW9uXCIgJiYgKChjbGFzc05hbWUgIT0gbnVtYmVyQ2xhc3MgJiYgY2xhc3NOYW1lICE9IHN0cmluZ0NsYXNzICYmIGNsYXNzTmFtZSAhPSBhcnJheUNsYXNzKSB8fCBpc1Byb3BlcnR5LmNhbGwodmFsdWUsIFwidG9KU09OXCIpKSkge1xyXG4gICAgICAgICAgICAgIC8vIFByb3RvdHlwZSA8PSAxLjYuMSBhZGRzIG5vbi1zdGFuZGFyZCBgdG9KU09OYCBtZXRob2RzIHRvIHRoZVxyXG4gICAgICAgICAgICAgIC8vIGBOdW1iZXJgLCBgU3RyaW5nYCwgYERhdGVgLCBhbmQgYEFycmF5YCBwcm90b3R5cGVzLiBKU09OIDNcclxuICAgICAgICAgICAgICAvLyBpZ25vcmVzIGFsbCBgdG9KU09OYCBtZXRob2RzIG9uIHRoZXNlIG9iamVjdHMgdW5sZXNzIHRoZXkgYXJlXHJcbiAgICAgICAgICAgICAgLy8gZGVmaW5lZCBkaXJlY3RseSBvbiBhbiBpbnN0YW5jZS5cclxuICAgICAgICAgICAgICB2YWx1ZSA9IHZhbHVlLnRvSlNPTihwcm9wZXJ0eSk7XHJcbiAgICAgICAgICAgIH1cclxuICAgICAgICAgIH1cclxuICAgICAgICAgIGlmIChjYWxsYmFjaykge1xyXG4gICAgICAgICAgICAvLyBJZiBhIHJlcGxhY2VtZW50IGZ1bmN0aW9uIHdhcyBwcm92aWRlZCwgY2FsbCBpdCB0byBvYnRhaW4gdGhlIHZhbHVlXHJcbiAgICAgICAgICAgIC8vIGZvciBzZXJpYWxpemF0aW9uLlxyXG4gICAgICAgICAgICB2YWx1ZSA9IGNhbGxiYWNrLmNhbGwob2JqZWN0LCBwcm9wZXJ0eSwgdmFsdWUpO1xyXG4gICAgICAgICAgfVxyXG4gICAgICAgICAgaWYgKHZhbHVlID09PSBudWxsKSB7XHJcbiAgICAgICAgICAgIHJldHVybiBcIm51bGxcIjtcclxuICAgICAgICAgIH1cclxuICAgICAgICAgIGNsYXNzTmFtZSA9IGdldENsYXNzLmNhbGwodmFsdWUpO1xyXG4gICAgICAgICAgaWYgKGNsYXNzTmFtZSA9PSBib29sZWFuQ2xhc3MpIHtcclxuICAgICAgICAgICAgLy8gQm9vbGVhbnMgYXJlIHJlcHJlc2VudGVkIGxpdGVyYWxseS5cclxuICAgICAgICAgICAgcmV0dXJuIFwiXCIgKyB2YWx1ZTtcclxuICAgICAgICAgIH0gZWxzZSBpZiAoY2xhc3NOYW1lID09IG51bWJlckNsYXNzKSB7XHJcbiAgICAgICAgICAgIC8vIEpTT04gbnVtYmVycyBtdXN0IGJlIGZpbml0ZS4gYEluZmluaXR5YCBhbmQgYE5hTmAgYXJlIHNlcmlhbGl6ZWQgYXNcclxuICAgICAgICAgICAgLy8gYFwibnVsbFwiYC5cclxuICAgICAgICAgICAgcmV0dXJuIHZhbHVlID4gLTEgLyAwICYmIHZhbHVlIDwgMSAvIDAgPyBcIlwiICsgdmFsdWUgOiBcIm51bGxcIjtcclxuICAgICAgICAgIH0gZWxzZSBpZiAoY2xhc3NOYW1lID09IHN0cmluZ0NsYXNzKSB7XHJcbiAgICAgICAgICAgIC8vIFN0cmluZ3MgYXJlIGRvdWJsZS1xdW90ZWQgYW5kIGVzY2FwZWQuXHJcbiAgICAgICAgICAgIHJldHVybiBxdW90ZShcIlwiICsgdmFsdWUpO1xyXG4gICAgICAgICAgfVxyXG4gICAgICAgICAgLy8gUmVjdXJzaXZlbHkgc2VyaWFsaXplIG9iamVjdHMgYW5kIGFycmF5cy5cclxuICAgICAgICAgIGlmICh0eXBlb2YgdmFsdWUgPT0gXCJvYmplY3RcIikge1xyXG4gICAgICAgICAgICAvLyBDaGVjayBmb3IgY3ljbGljIHN0cnVjdHVyZXMuIFRoaXMgaXMgYSBsaW5lYXIgc2VhcmNoOyBwZXJmb3JtYW5jZVxyXG4gICAgICAgICAgICAvLyBpcyBpbnZlcnNlbHkgcHJvcG9ydGlvbmFsIHRvIHRoZSBudW1iZXIgb2YgdW5pcXVlIG5lc3RlZCBvYmplY3RzLlxyXG4gICAgICAgICAgICBmb3IgKGxlbmd0aCA9IHN0YWNrLmxlbmd0aDsgbGVuZ3RoLS07KSB7XHJcbiAgICAgICAgICAgICAgaWYgKHN0YWNrW2xlbmd0aF0gPT09IHZhbHVlKSB7XHJcbiAgICAgICAgICAgICAgICAvLyBDeWNsaWMgc3RydWN0dXJlcyBjYW5ub3QgYmUgc2VyaWFsaXplZCBieSBgSlNPTi5zdHJpbmdpZnlgLlxyXG4gICAgICAgICAgICAgICAgdGhyb3cgVHlwZUVycm9yKCk7XHJcbiAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICB9XHJcbiAgICAgICAgICAgIC8vIEFkZCB0aGUgb2JqZWN0IHRvIHRoZSBzdGFjayBvZiB0cmF2ZXJzZWQgb2JqZWN0cy5cclxuICAgICAgICAgICAgc3RhY2sucHVzaCh2YWx1ZSk7XHJcbiAgICAgICAgICAgIHJlc3VsdHMgPSBbXTtcclxuICAgICAgICAgICAgLy8gU2F2ZSB0aGUgY3VycmVudCBpbmRlbnRhdGlvbiBsZXZlbCBhbmQgaW5kZW50IG9uZSBhZGRpdGlvbmFsIGxldmVsLlxyXG4gICAgICAgICAgICBwcmVmaXggPSBpbmRlbnRhdGlvbjtcclxuICAgICAgICAgICAgaW5kZW50YXRpb24gKz0gd2hpdGVzcGFjZTtcclxuICAgICAgICAgICAgaWYgKGNsYXNzTmFtZSA9PSBhcnJheUNsYXNzKSB7XHJcbiAgICAgICAgICAgICAgLy8gUmVjdXJzaXZlbHkgc2VyaWFsaXplIGFycmF5IGVsZW1lbnRzLlxyXG4gICAgICAgICAgICAgIGZvciAoaW5kZXggPSAwLCBsZW5ndGggPSB2YWx1ZS5sZW5ndGg7IGluZGV4IDwgbGVuZ3RoOyBpbmRleCsrKSB7XHJcbiAgICAgICAgICAgICAgICBlbGVtZW50ID0gc2VyaWFsaXplKGluZGV4LCB2YWx1ZSwgY2FsbGJhY2ssIHByb3BlcnRpZXMsIHdoaXRlc3BhY2UsIGluZGVudGF0aW9uLCBzdGFjayk7XHJcbiAgICAgICAgICAgICAgICByZXN1bHRzLnB1c2goZWxlbWVudCA9PT0gdW5kZWYgPyBcIm51bGxcIiA6IGVsZW1lbnQpO1xyXG4gICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgICByZXN1bHQgPSByZXN1bHRzLmxlbmd0aCA/ICh3aGl0ZXNwYWNlID8gXCJbXFxuXCIgKyBpbmRlbnRhdGlvbiArIHJlc3VsdHMuam9pbihcIixcXG5cIiArIGluZGVudGF0aW9uKSArIFwiXFxuXCIgKyBwcmVmaXggKyBcIl1cIiA6IChcIltcIiArIHJlc3VsdHMuam9pbihcIixcIikgKyBcIl1cIikpIDogXCJbXVwiO1xyXG4gICAgICAgICAgICB9IGVsc2Uge1xyXG4gICAgICAgICAgICAgIC8vIFJlY3Vyc2l2ZWx5IHNlcmlhbGl6ZSBvYmplY3QgbWVtYmVycy4gTWVtYmVycyBhcmUgc2VsZWN0ZWQgZnJvbVxyXG4gICAgICAgICAgICAgIC8vIGVpdGhlciBhIHVzZXItc3BlY2lmaWVkIGxpc3Qgb2YgcHJvcGVydHkgbmFtZXMsIG9yIHRoZSBvYmplY3RcclxuICAgICAgICAgICAgICAvLyBpdHNlbGYuXHJcbiAgICAgICAgICAgICAgZm9yRWFjaChwcm9wZXJ0aWVzIHx8IHZhbHVlLCBmdW5jdGlvbiAocHJvcGVydHkpIHtcclxuICAgICAgICAgICAgICAgIHZhciBlbGVtZW50ID0gc2VyaWFsaXplKHByb3BlcnR5LCB2YWx1ZSwgY2FsbGJhY2ssIHByb3BlcnRpZXMsIHdoaXRlc3BhY2UsIGluZGVudGF0aW9uLCBzdGFjayk7XHJcbiAgICAgICAgICAgICAgICBpZiAoZWxlbWVudCAhPT0gdW5kZWYpIHtcclxuICAgICAgICAgICAgICAgICAgLy8gQWNjb3JkaW5nIHRvIEVTIDUuMSBzZWN0aW9uIDE1LjEyLjM6IFwiSWYgYGdhcGAge3doaXRlc3BhY2V9XHJcbiAgICAgICAgICAgICAgICAgIC8vIGlzIG5vdCB0aGUgZW1wdHkgc3RyaW5nLCBsZXQgYG1lbWJlcmAge3F1b3RlKHByb3BlcnR5KSArIFwiOlwifVxyXG4gICAgICAgICAgICAgICAgICAvLyBiZSB0aGUgY29uY2F0ZW5hdGlvbiBvZiBgbWVtYmVyYCBhbmQgdGhlIGBzcGFjZWAgY2hhcmFjdGVyLlwiXHJcbiAgICAgICAgICAgICAgICAgIC8vIFRoZSBcImBzcGFjZWAgY2hhcmFjdGVyXCIgcmVmZXJzIHRvIHRoZSBsaXRlcmFsIHNwYWNlXHJcbiAgICAgICAgICAgICAgICAgIC8vIGNoYXJhY3Rlciwgbm90IHRoZSBgc3BhY2VgIHt3aWR0aH0gYXJndW1lbnQgcHJvdmlkZWQgdG9cclxuICAgICAgICAgICAgICAgICAgLy8gYEpTT04uc3RyaW5naWZ5YC5cclxuICAgICAgICAgICAgICAgICAgcmVzdWx0cy5wdXNoKHF1b3RlKHByb3BlcnR5KSArIFwiOlwiICsgKHdoaXRlc3BhY2UgPyBcIiBcIiA6IFwiXCIpICsgZWxlbWVudCk7XHJcbiAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgICAgfSk7XHJcbiAgICAgICAgICAgICAgcmVzdWx0ID0gcmVzdWx0cy5sZW5ndGggPyAod2hpdGVzcGFjZSA/IFwie1xcblwiICsgaW5kZW50YXRpb24gKyByZXN1bHRzLmpvaW4oXCIsXFxuXCIgKyBpbmRlbnRhdGlvbikgKyBcIlxcblwiICsgcHJlZml4ICsgXCJ9XCIgOiAoXCJ7XCIgKyByZXN1bHRzLmpvaW4oXCIsXCIpICsgXCJ9XCIpKSA6IFwie31cIjtcclxuICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICAvLyBSZW1vdmUgdGhlIG9iamVjdCBmcm9tIHRoZSB0cmF2ZXJzZWQgb2JqZWN0IHN0YWNrLlxyXG4gICAgICAgICAgICBzdGFjay5wb3AoKTtcclxuICAgICAgICAgICAgcmV0dXJuIHJlc3VsdDtcclxuICAgICAgICAgIH1cclxuICAgICAgICB9O1xyXG5cclxuICAgICAgICAvLyBQdWJsaWM6IGBKU09OLnN0cmluZ2lmeWAuIFNlZSBFUyA1LjEgc2VjdGlvbiAxNS4xMi4zLlxyXG4gICAgICAgIGV4cG9ydHMuc3RyaW5naWZ5ID0gZnVuY3Rpb24gKHNvdXJjZSwgZmlsdGVyLCB3aWR0aCkge1xyXG4gICAgICAgICAgdmFyIHdoaXRlc3BhY2UsIGNhbGxiYWNrLCBwcm9wZXJ0aWVzLCBjbGFzc05hbWU7XHJcbiAgICAgICAgICBpZiAob2JqZWN0VHlwZXNbdHlwZW9mIGZpbHRlcl0gJiYgZmlsdGVyKSB7XHJcbiAgICAgICAgICAgIGlmICgoY2xhc3NOYW1lID0gZ2V0Q2xhc3MuY2FsbChmaWx0ZXIpKSA9PSBmdW5jdGlvbkNsYXNzKSB7XHJcbiAgICAgICAgICAgICAgY2FsbGJhY2sgPSBmaWx0ZXI7XHJcbiAgICAgICAgICAgIH0gZWxzZSBpZiAoY2xhc3NOYW1lID09IGFycmF5Q2xhc3MpIHtcclxuICAgICAgICAgICAgICAvLyBDb252ZXJ0IHRoZSBwcm9wZXJ0eSBuYW1lcyBhcnJheSBpbnRvIGEgbWFrZXNoaWZ0IHNldC5cclxuICAgICAgICAgICAgICBwcm9wZXJ0aWVzID0ge307XHJcbiAgICAgICAgICAgICAgZm9yICh2YXIgaW5kZXggPSAwLCBsZW5ndGggPSBmaWx0ZXIubGVuZ3RoLCB2YWx1ZTsgaW5kZXggPCBsZW5ndGg7IHZhbHVlID0gZmlsdGVyW2luZGV4KytdLCAoKGNsYXNzTmFtZSA9IGdldENsYXNzLmNhbGwodmFsdWUpKSwgY2xhc3NOYW1lID09IHN0cmluZ0NsYXNzIHx8IGNsYXNzTmFtZSA9PSBudW1iZXJDbGFzcykgJiYgKHByb3BlcnRpZXNbdmFsdWVdID0gMSkpO1xyXG4gICAgICAgICAgICB9XHJcbiAgICAgICAgICB9XHJcbiAgICAgICAgICBpZiAod2lkdGgpIHtcclxuICAgICAgICAgICAgaWYgKChjbGFzc05hbWUgPSBnZXRDbGFzcy5jYWxsKHdpZHRoKSkgPT0gbnVtYmVyQ2xhc3MpIHtcclxuICAgICAgICAgICAgICAvLyBDb252ZXJ0IHRoZSBgd2lkdGhgIHRvIGFuIGludGVnZXIgYW5kIGNyZWF0ZSBhIHN0cmluZyBjb250YWluaW5nXHJcbiAgICAgICAgICAgICAgLy8gYHdpZHRoYCBudW1iZXIgb2Ygc3BhY2UgY2hhcmFjdGVycy5cclxuICAgICAgICAgICAgICBpZiAoKHdpZHRoIC09IHdpZHRoICUgMSkgPiAwKSB7XHJcbiAgICAgICAgICAgICAgICBmb3IgKHdoaXRlc3BhY2UgPSBcIlwiLCB3aWR0aCA+IDEwICYmICh3aWR0aCA9IDEwKTsgd2hpdGVzcGFjZS5sZW5ndGggPCB3aWR0aDsgd2hpdGVzcGFjZSArPSBcIiBcIik7XHJcbiAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICB9IGVsc2UgaWYgKGNsYXNzTmFtZSA9PSBzdHJpbmdDbGFzcykge1xyXG4gICAgICAgICAgICAgIHdoaXRlc3BhY2UgPSB3aWR0aC5sZW5ndGggPD0gMTAgPyB3aWR0aCA6IHdpZHRoLnNsaWNlKDAsIDEwKTtcclxuICAgICAgICAgICAgfVxyXG4gICAgICAgICAgfVxyXG4gICAgICAgICAgLy8gT3BlcmEgPD0gNy41NHUyIGRpc2NhcmRzIHRoZSB2YWx1ZXMgYXNzb2NpYXRlZCB3aXRoIGVtcHR5IHN0cmluZyBrZXlzXHJcbiAgICAgICAgICAvLyAoYFwiXCJgKSBvbmx5IGlmIHRoZXkgYXJlIHVzZWQgZGlyZWN0bHkgd2l0aGluIGFuIG9iamVjdCBtZW1iZXIgbGlzdFxyXG4gICAgICAgICAgLy8gKGUuZy4sIGAhKFwiXCIgaW4geyBcIlwiOiAxfSlgKS5cclxuICAgICAgICAgIHJldHVybiBzZXJpYWxpemUoXCJcIiwgKHZhbHVlID0ge30sIHZhbHVlW1wiXCJdID0gc291cmNlLCB2YWx1ZSksIGNhbGxiYWNrLCBwcm9wZXJ0aWVzLCB3aGl0ZXNwYWNlLCBcIlwiLCBbXSk7XHJcbiAgICAgICAgfTtcclxuICAgICAgfVxyXG5cclxuICAgICAgLy8gUHVibGljOiBQYXJzZXMgYSBKU09OIHNvdXJjZSBzdHJpbmcuXHJcbiAgICAgIGlmICghaGFzKFwianNvbi1wYXJzZVwiKSkge1xyXG4gICAgICAgIHZhciBmcm9tQ2hhckNvZGUgPSBTdHJpbmcuZnJvbUNoYXJDb2RlO1xyXG5cclxuICAgICAgICAvLyBJbnRlcm5hbDogQSBtYXAgb2YgZXNjYXBlZCBjb250cm9sIGNoYXJhY3RlcnMgYW5kIHRoZWlyIHVuZXNjYXBlZFxyXG4gICAgICAgIC8vIGVxdWl2YWxlbnRzLlxyXG4gICAgICAgIHZhciBVbmVzY2FwZXMgPSB7XHJcbiAgICAgICAgICA5MjogXCJcXFxcXCIsXHJcbiAgICAgICAgICAzNDogJ1wiJyxcclxuICAgICAgICAgIDQ3OiBcIi9cIixcclxuICAgICAgICAgIDk4OiBcIlxcYlwiLFxyXG4gICAgICAgICAgMTE2OiBcIlxcdFwiLFxyXG4gICAgICAgICAgMTEwOiBcIlxcblwiLFxyXG4gICAgICAgICAgMTAyOiBcIlxcZlwiLFxyXG4gICAgICAgICAgMTE0OiBcIlxcclwiXHJcbiAgICAgICAgfTtcclxuXHJcbiAgICAgICAgLy8gSW50ZXJuYWw6IFN0b3JlcyB0aGUgcGFyc2VyIHN0YXRlLlxyXG4gICAgICAgIHZhciBJbmRleCwgU291cmNlO1xyXG5cclxuICAgICAgICAvLyBJbnRlcm5hbDogUmVzZXRzIHRoZSBwYXJzZXIgc3RhdGUgYW5kIHRocm93cyBhIGBTeW50YXhFcnJvcmAuXHJcbiAgICAgICAgdmFyIGFib3J0ID0gZnVuY3Rpb24gKCkge1xyXG4gICAgICAgICAgSW5kZXggPSBTb3VyY2UgPSBudWxsO1xyXG4gICAgICAgICAgdGhyb3cgU3ludGF4RXJyb3IoKTtcclxuICAgICAgICB9O1xyXG5cclxuICAgICAgICAvLyBJbnRlcm5hbDogUmV0dXJucyB0aGUgbmV4dCB0b2tlbiwgb3IgYFwiJFwiYCBpZiB0aGUgcGFyc2VyIGhhcyByZWFjaGVkXHJcbiAgICAgICAgLy8gdGhlIGVuZCBvZiB0aGUgc291cmNlIHN0cmluZy4gQSB0b2tlbiBtYXkgYmUgYSBzdHJpbmcsIG51bWJlciwgYG51bGxgXHJcbiAgICAgICAgLy8gbGl0ZXJhbCwgb3IgQm9vbGVhbiBsaXRlcmFsLlxyXG4gICAgICAgIHZhciBsZXggPSBmdW5jdGlvbiAoKSB7XHJcbiAgICAgICAgICB2YXIgc291cmNlID0gU291cmNlLCBsZW5ndGggPSBzb3VyY2UubGVuZ3RoLCB2YWx1ZSwgYmVnaW4sIHBvc2l0aW9uLCBpc1NpZ25lZCwgY2hhckNvZGU7XHJcbiAgICAgICAgICB3aGlsZSAoSW5kZXggPCBsZW5ndGgpIHtcclxuICAgICAgICAgICAgY2hhckNvZGUgPSBzb3VyY2UuY2hhckNvZGVBdChJbmRleCk7XHJcbiAgICAgICAgICAgIHN3aXRjaCAoY2hhckNvZGUpIHtcclxuICAgICAgICAgICAgICBjYXNlIDk6IGNhc2UgMTA6IGNhc2UgMTM6IGNhc2UgMzI6XHJcbiAgICAgICAgICAgICAgICAvLyBTa2lwIHdoaXRlc3BhY2UgdG9rZW5zLCBpbmNsdWRpbmcgdGFicywgY2FycmlhZ2UgcmV0dXJucywgbGluZVxyXG4gICAgICAgICAgICAgICAgLy8gZmVlZHMsIGFuZCBzcGFjZSBjaGFyYWN0ZXJzLlxyXG4gICAgICAgICAgICAgICAgSW5kZXgrKztcclxuICAgICAgICAgICAgICAgIGJyZWFrO1xyXG4gICAgICAgICAgICAgIGNhc2UgMTIzOiBjYXNlIDEyNTogY2FzZSA5MTogY2FzZSA5MzogY2FzZSA1ODogY2FzZSA0NDpcclxuICAgICAgICAgICAgICAgIC8vIFBhcnNlIGEgcHVuY3R1YXRvciB0b2tlbiAoYHtgLCBgfWAsIGBbYCwgYF1gLCBgOmAsIG9yIGAsYCkgYXRcclxuICAgICAgICAgICAgICAgIC8vIHRoZSBjdXJyZW50IHBvc2l0aW9uLlxyXG4gICAgICAgICAgICAgICAgdmFsdWUgPSBjaGFySW5kZXhCdWdneSA/IHNvdXJjZS5jaGFyQXQoSW5kZXgpIDogc291cmNlW0luZGV4XTtcclxuICAgICAgICAgICAgICAgIEluZGV4Kys7XHJcbiAgICAgICAgICAgICAgICByZXR1cm4gdmFsdWU7XHJcbiAgICAgICAgICAgICAgY2FzZSAzNDpcclxuICAgICAgICAgICAgICAgIC8vIGBcImAgZGVsaW1pdHMgYSBKU09OIHN0cmluZzsgYWR2YW5jZSB0byB0aGUgbmV4dCBjaGFyYWN0ZXIgYW5kXHJcbiAgICAgICAgICAgICAgICAvLyBiZWdpbiBwYXJzaW5nIHRoZSBzdHJpbmcuIFN0cmluZyB0b2tlbnMgYXJlIHByZWZpeGVkIHdpdGggdGhlXHJcbiAgICAgICAgICAgICAgICAvLyBzZW50aW5lbCBgQGAgY2hhcmFjdGVyIHRvIGRpc3Rpbmd1aXNoIHRoZW0gZnJvbSBwdW5jdHVhdG9ycyBhbmRcclxuICAgICAgICAgICAgICAgIC8vIGVuZC1vZi1zdHJpbmcgdG9rZW5zLlxyXG4gICAgICAgICAgICAgICAgZm9yICh2YWx1ZSA9IFwiQFwiLCBJbmRleCsrOyBJbmRleCA8IGxlbmd0aDspIHtcclxuICAgICAgICAgICAgICAgICAgY2hhckNvZGUgPSBzb3VyY2UuY2hhckNvZGVBdChJbmRleCk7XHJcbiAgICAgICAgICAgICAgICAgIGlmIChjaGFyQ29kZSA8IDMyKSB7XHJcbiAgICAgICAgICAgICAgICAgICAgLy8gVW5lc2NhcGVkIEFTQ0lJIGNvbnRyb2wgY2hhcmFjdGVycyAodGhvc2Ugd2l0aCBhIGNvZGUgdW5pdFxyXG4gICAgICAgICAgICAgICAgICAgIC8vIGxlc3MgdGhhbiB0aGUgc3BhY2UgY2hhcmFjdGVyKSBhcmUgbm90IHBlcm1pdHRlZC5cclxuICAgICAgICAgICAgICAgICAgICBhYm9ydCgpO1xyXG4gICAgICAgICAgICAgICAgICB9IGVsc2UgaWYgKGNoYXJDb2RlID09IDkyKSB7XHJcbiAgICAgICAgICAgICAgICAgICAgLy8gQSByZXZlcnNlIHNvbGlkdXMgKGBcXGApIG1hcmtzIHRoZSBiZWdpbm5pbmcgb2YgYW4gZXNjYXBlZFxyXG4gICAgICAgICAgICAgICAgICAgIC8vIGNvbnRyb2wgY2hhcmFjdGVyIChpbmNsdWRpbmcgYFwiYCwgYFxcYCwgYW5kIGAvYCkgb3IgVW5pY29kZVxyXG4gICAgICAgICAgICAgICAgICAgIC8vIGVzY2FwZSBzZXF1ZW5jZS5cclxuICAgICAgICAgICAgICAgICAgICBjaGFyQ29kZSA9IHNvdXJjZS5jaGFyQ29kZUF0KCsrSW5kZXgpO1xyXG4gICAgICAgICAgICAgICAgICAgIHN3aXRjaCAoY2hhckNvZGUpIHtcclxuICAgICAgICAgICAgICAgICAgICAgIGNhc2UgOTI6IGNhc2UgMzQ6IGNhc2UgNDc6IGNhc2UgOTg6IGNhc2UgMTE2OiBjYXNlIDExMDogY2FzZSAxMDI6IGNhc2UgMTE0OlxyXG4gICAgICAgICAgICAgICAgICAgICAgICAvLyBSZXZpdmUgZXNjYXBlZCBjb250cm9sIGNoYXJhY3RlcnMuXHJcbiAgICAgICAgICAgICAgICAgICAgICAgIHZhbHVlICs9IFVuZXNjYXBlc1tjaGFyQ29kZV07XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIEluZGV4Kys7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIGJyZWFrO1xyXG4gICAgICAgICAgICAgICAgICAgICAgY2FzZSAxMTc6XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIC8vIGBcXHVgIG1hcmtzIHRoZSBiZWdpbm5pbmcgb2YgYSBVbmljb2RlIGVzY2FwZSBzZXF1ZW5jZS5cclxuICAgICAgICAgICAgICAgICAgICAgICAgLy8gQWR2YW5jZSB0byB0aGUgZmlyc3QgY2hhcmFjdGVyIGFuZCB2YWxpZGF0ZSB0aGVcclxuICAgICAgICAgICAgICAgICAgICAgICAgLy8gZm91ci1kaWdpdCBjb2RlIHBvaW50LlxyXG4gICAgICAgICAgICAgICAgICAgICAgICBiZWdpbiA9ICsrSW5kZXg7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIGZvciAocG9zaXRpb24gPSBJbmRleCArIDQ7IEluZGV4IDwgcG9zaXRpb247IEluZGV4KyspIHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICBjaGFyQ29kZSA9IHNvdXJjZS5jaGFyQ29kZUF0KEluZGV4KTtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAvLyBBIHZhbGlkIHNlcXVlbmNlIGNvbXByaXNlcyBmb3VyIGhleGRpZ2l0cyAoY2FzZS1cclxuICAgICAgICAgICAgICAgICAgICAgICAgICAvLyBpbnNlbnNpdGl2ZSkgdGhhdCBmb3JtIGEgc2luZ2xlIGhleGFkZWNpbWFsIHZhbHVlLlxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgIGlmICghKGNoYXJDb2RlID49IDQ4ICYmIGNoYXJDb2RlIDw9IDU3IHx8IGNoYXJDb2RlID49IDk3ICYmIGNoYXJDb2RlIDw9IDEwMiB8fCBjaGFyQ29kZSA+PSA2NSAmJiBjaGFyQ29kZSA8PSA3MCkpIHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIC8vIEludmFsaWQgVW5pY29kZSBlc2NhcGUgc2VxdWVuY2UuXHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICBhYm9ydCgpO1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICAgICAgICAgICAgICAvLyBSZXZpdmUgdGhlIGVzY2FwZWQgY2hhcmFjdGVyLlxyXG4gICAgICAgICAgICAgICAgICAgICAgICB2YWx1ZSArPSBmcm9tQ2hhckNvZGUoXCIweFwiICsgc291cmNlLnNsaWNlKGJlZ2luLCBJbmRleCkpO1xyXG4gICAgICAgICAgICAgICAgICAgICAgICBicmVhaztcclxuICAgICAgICAgICAgICAgICAgICAgIGRlZmF1bHQ6XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIC8vIEludmFsaWQgZXNjYXBlIHNlcXVlbmNlLlxyXG4gICAgICAgICAgICAgICAgICAgICAgICBhYm9ydCgpO1xyXG4gICAgICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgICAgICAgfSBlbHNlIHtcclxuICAgICAgICAgICAgICAgICAgICBpZiAoY2hhckNvZGUgPT0gMzQpIHtcclxuICAgICAgICAgICAgICAgICAgICAgIC8vIEFuIHVuZXNjYXBlZCBkb3VibGUtcXVvdGUgY2hhcmFjdGVyIG1hcmtzIHRoZSBlbmQgb2YgdGhlXHJcbiAgICAgICAgICAgICAgICAgICAgICAvLyBzdHJpbmcuXHJcbiAgICAgICAgICAgICAgICAgICAgICBicmVhaztcclxuICAgICAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgICAgICAgICAgY2hhckNvZGUgPSBzb3VyY2UuY2hhckNvZGVBdChJbmRleCk7XHJcbiAgICAgICAgICAgICAgICAgICAgYmVnaW4gPSBJbmRleDtcclxuICAgICAgICAgICAgICAgICAgICAvLyBPcHRpbWl6ZSBmb3IgdGhlIGNvbW1vbiBjYXNlIHdoZXJlIGEgc3RyaW5nIGlzIHZhbGlkLlxyXG4gICAgICAgICAgICAgICAgICAgIHdoaWxlIChjaGFyQ29kZSA+PSAzMiAmJiBjaGFyQ29kZSAhPSA5MiAmJiBjaGFyQ29kZSAhPSAzNCkge1xyXG4gICAgICAgICAgICAgICAgICAgICAgY2hhckNvZGUgPSBzb3VyY2UuY2hhckNvZGVBdCgrK0luZGV4KTtcclxuICAgICAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgICAgICAgICAgLy8gQXBwZW5kIHRoZSBzdHJpbmcgYXMtaXMuXHJcbiAgICAgICAgICAgICAgICAgICAgdmFsdWUgKz0gc291cmNlLnNsaWNlKGJlZ2luLCBJbmRleCk7XHJcbiAgICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgICAgIGlmIChzb3VyY2UuY2hhckNvZGVBdChJbmRleCkgPT0gMzQpIHtcclxuICAgICAgICAgICAgICAgICAgLy8gQWR2YW5jZSB0byB0aGUgbmV4dCBjaGFyYWN0ZXIgYW5kIHJldHVybiB0aGUgcmV2aXZlZCBzdHJpbmcuXHJcbiAgICAgICAgICAgICAgICAgIEluZGV4Kys7XHJcbiAgICAgICAgICAgICAgICAgIHJldHVybiB2YWx1ZTtcclxuICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgICAgIC8vIFVudGVybWluYXRlZCBzdHJpbmcuXHJcbiAgICAgICAgICAgICAgICBhYm9ydCgpO1xyXG4gICAgICAgICAgICAgIGRlZmF1bHQ6XHJcbiAgICAgICAgICAgICAgICAvLyBQYXJzZSBudW1iZXJzIGFuZCBsaXRlcmFscy5cclxuICAgICAgICAgICAgICAgIGJlZ2luID0gSW5kZXg7XHJcbiAgICAgICAgICAgICAgICAvLyBBZHZhbmNlIHBhc3QgdGhlIG5lZ2F0aXZlIHNpZ24sIGlmIG9uZSBpcyBzcGVjaWZpZWQuXHJcbiAgICAgICAgICAgICAgICBpZiAoY2hhckNvZGUgPT0gNDUpIHtcclxuICAgICAgICAgICAgICAgICAgaXNTaWduZWQgPSB0cnVlO1xyXG4gICAgICAgICAgICAgICAgICBjaGFyQ29kZSA9IHNvdXJjZS5jaGFyQ29kZUF0KCsrSW5kZXgpO1xyXG4gICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICAgICAgLy8gUGFyc2UgYW4gaW50ZWdlciBvciBmbG9hdGluZy1wb2ludCB2YWx1ZS5cclxuICAgICAgICAgICAgICAgIGlmIChjaGFyQ29kZSA+PSA0OCAmJiBjaGFyQ29kZSA8PSA1Nykge1xyXG4gICAgICAgICAgICAgICAgICAvLyBMZWFkaW5nIHplcm9lcyBhcmUgaW50ZXJwcmV0ZWQgYXMgb2N0YWwgbGl0ZXJhbHMuXHJcbiAgICAgICAgICAgICAgICAgIGlmIChjaGFyQ29kZSA9PSA0OCAmJiAoKGNoYXJDb2RlID0gc291cmNlLmNoYXJDb2RlQXQoSW5kZXggKyAxKSksIGNoYXJDb2RlID49IDQ4ICYmIGNoYXJDb2RlIDw9IDU3KSkge1xyXG4gICAgICAgICAgICAgICAgICAgIC8vIElsbGVnYWwgb2N0YWwgbGl0ZXJhbC5cclxuICAgICAgICAgICAgICAgICAgICBhYm9ydCgpO1xyXG4gICAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgICAgICAgIGlzU2lnbmVkID0gZmFsc2U7XHJcbiAgICAgICAgICAgICAgICAgIC8vIFBhcnNlIHRoZSBpbnRlZ2VyIGNvbXBvbmVudC5cclxuICAgICAgICAgICAgICAgICAgZm9yICg7IEluZGV4IDwgbGVuZ3RoICYmICgoY2hhckNvZGUgPSBzb3VyY2UuY2hhckNvZGVBdChJbmRleCkpLCBjaGFyQ29kZSA+PSA0OCAmJiBjaGFyQ29kZSA8PSA1Nyk7IEluZGV4KyspO1xyXG4gICAgICAgICAgICAgICAgICAvLyBGbG9hdHMgY2Fubm90IGNvbnRhaW4gYSBsZWFkaW5nIGRlY2ltYWwgcG9pbnQ7IGhvd2V2ZXIsIHRoaXNcclxuICAgICAgICAgICAgICAgICAgLy8gY2FzZSBpcyBhbHJlYWR5IGFjY291bnRlZCBmb3IgYnkgdGhlIHBhcnNlci5cclxuICAgICAgICAgICAgICAgICAgaWYgKHNvdXJjZS5jaGFyQ29kZUF0KEluZGV4KSA9PSA0Nikge1xyXG4gICAgICAgICAgICAgICAgICAgIHBvc2l0aW9uID0gKytJbmRleDtcclxuICAgICAgICAgICAgICAgICAgICAvLyBQYXJzZSB0aGUgZGVjaW1hbCBjb21wb25lbnQuXHJcbiAgICAgICAgICAgICAgICAgICAgZm9yICg7IHBvc2l0aW9uIDwgbGVuZ3RoICYmICgoY2hhckNvZGUgPSBzb3VyY2UuY2hhckNvZGVBdChwb3NpdGlvbikpLCBjaGFyQ29kZSA+PSA0OCAmJiBjaGFyQ29kZSA8PSA1Nyk7IHBvc2l0aW9uKyspO1xyXG4gICAgICAgICAgICAgICAgICAgIGlmIChwb3NpdGlvbiA9PSBJbmRleCkge1xyXG4gICAgICAgICAgICAgICAgICAgICAgLy8gSWxsZWdhbCB0cmFpbGluZyBkZWNpbWFsLlxyXG4gICAgICAgICAgICAgICAgICAgICAgYWJvcnQoKTtcclxuICAgICAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgICAgICAgICAgSW5kZXggPSBwb3NpdGlvbjtcclxuICAgICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICAgICAgICAvLyBQYXJzZSBleHBvbmVudHMuIFRoZSBgZWAgZGVub3RpbmcgdGhlIGV4cG9uZW50IGlzXHJcbiAgICAgICAgICAgICAgICAgIC8vIGNhc2UtaW5zZW5zaXRpdmUuXHJcbiAgICAgICAgICAgICAgICAgIGNoYXJDb2RlID0gc291cmNlLmNoYXJDb2RlQXQoSW5kZXgpO1xyXG4gICAgICAgICAgICAgICAgICBpZiAoY2hhckNvZGUgPT0gMTAxIHx8IGNoYXJDb2RlID09IDY5KSB7XHJcbiAgICAgICAgICAgICAgICAgICAgY2hhckNvZGUgPSBzb3VyY2UuY2hhckNvZGVBdCgrK0luZGV4KTtcclxuICAgICAgICAgICAgICAgICAgICAvLyBTa2lwIHBhc3QgdGhlIHNpZ24gZm9sbG93aW5nIHRoZSBleHBvbmVudCwgaWYgb25lIGlzXHJcbiAgICAgICAgICAgICAgICAgICAgLy8gc3BlY2lmaWVkLlxyXG4gICAgICAgICAgICAgICAgICAgIGlmIChjaGFyQ29kZSA9PSA0MyB8fCBjaGFyQ29kZSA9PSA0NSkge1xyXG4gICAgICAgICAgICAgICAgICAgICAgSW5kZXgrKztcclxuICAgICAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgICAgICAgICAgLy8gUGFyc2UgdGhlIGV4cG9uZW50aWFsIGNvbXBvbmVudC5cclxuICAgICAgICAgICAgICAgICAgICBmb3IgKHBvc2l0aW9uID0gSW5kZXg7IHBvc2l0aW9uIDwgbGVuZ3RoICYmICgoY2hhckNvZGUgPSBzb3VyY2UuY2hhckNvZGVBdChwb3NpdGlvbikpLCBjaGFyQ29kZSA+PSA0OCAmJiBjaGFyQ29kZSA8PSA1Nyk7IHBvc2l0aW9uKyspO1xyXG4gICAgICAgICAgICAgICAgICAgIGlmIChwb3NpdGlvbiA9PSBJbmRleCkge1xyXG4gICAgICAgICAgICAgICAgICAgICAgLy8gSWxsZWdhbCBlbXB0eSBleHBvbmVudC5cclxuICAgICAgICAgICAgICAgICAgICAgIGFib3J0KCk7XHJcbiAgICAgICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICAgICAgICAgIEluZGV4ID0gcG9zaXRpb247XHJcbiAgICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgICAgICAgLy8gQ29lcmNlIHRoZSBwYXJzZWQgdmFsdWUgdG8gYSBKYXZhU2NyaXB0IG51bWJlci5cclxuICAgICAgICAgICAgICAgICAgcmV0dXJuICtzb3VyY2Uuc2xpY2UoYmVnaW4sIEluZGV4KTtcclxuICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgICAgIC8vIEEgbmVnYXRpdmUgc2lnbiBtYXkgb25seSBwcmVjZWRlIG51bWJlcnMuXHJcbiAgICAgICAgICAgICAgICBpZiAoaXNTaWduZWQpIHtcclxuICAgICAgICAgICAgICAgICAgYWJvcnQoKTtcclxuICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgICAgIC8vIGB0cnVlYCwgYGZhbHNlYCwgYW5kIGBudWxsYCBsaXRlcmFscy5cclxuICAgICAgICAgICAgICAgIGlmIChzb3VyY2Uuc2xpY2UoSW5kZXgsIEluZGV4ICsgNCkgPT0gXCJ0cnVlXCIpIHtcclxuICAgICAgICAgICAgICAgICAgSW5kZXggKz0gNDtcclxuICAgICAgICAgICAgICAgICAgcmV0dXJuIHRydWU7XHJcbiAgICAgICAgICAgICAgICB9IGVsc2UgaWYgKHNvdXJjZS5zbGljZShJbmRleCwgSW5kZXggKyA1KSA9PSBcImZhbHNlXCIpIHtcclxuICAgICAgICAgICAgICAgICAgSW5kZXggKz0gNTtcclxuICAgICAgICAgICAgICAgICAgcmV0dXJuIGZhbHNlO1xyXG4gICAgICAgICAgICAgICAgfSBlbHNlIGlmIChzb3VyY2Uuc2xpY2UoSW5kZXgsIEluZGV4ICsgNCkgPT0gXCJudWxsXCIpIHtcclxuICAgICAgICAgICAgICAgICAgSW5kZXggKz0gNDtcclxuICAgICAgICAgICAgICAgICAgcmV0dXJuIG51bGw7XHJcbiAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgICAgICAvLyBVbnJlY29nbml6ZWQgdG9rZW4uXHJcbiAgICAgICAgICAgICAgICBhYm9ydCgpO1xyXG4gICAgICAgICAgICB9XHJcbiAgICAgICAgICB9XHJcbiAgICAgICAgICAvLyBSZXR1cm4gdGhlIHNlbnRpbmVsIGAkYCBjaGFyYWN0ZXIgaWYgdGhlIHBhcnNlciBoYXMgcmVhY2hlZCB0aGUgZW5kXHJcbiAgICAgICAgICAvLyBvZiB0aGUgc291cmNlIHN0cmluZy5cclxuICAgICAgICAgIHJldHVybiBcIiRcIjtcclxuICAgICAgICB9O1xyXG5cclxuICAgICAgICAvLyBJbnRlcm5hbDogUGFyc2VzIGEgSlNPTiBgdmFsdWVgIHRva2VuLlxyXG4gICAgICAgIHZhciBnZXQgPSBmdW5jdGlvbiAodmFsdWUpIHtcclxuICAgICAgICAgIHZhciByZXN1bHRzLCBoYXNNZW1iZXJzO1xyXG4gICAgICAgICAgaWYgKHZhbHVlID09IFwiJFwiKSB7XHJcbiAgICAgICAgICAgIC8vIFVuZXhwZWN0ZWQgZW5kIG9mIGlucHV0LlxyXG4gICAgICAgICAgICBhYm9ydCgpO1xyXG4gICAgICAgICAgfVxyXG4gICAgICAgICAgaWYgKHR5cGVvZiB2YWx1ZSA9PSBcInN0cmluZ1wiKSB7XHJcbiAgICAgICAgICAgIGlmICgoY2hhckluZGV4QnVnZ3kgPyB2YWx1ZS5jaGFyQXQoMCkgOiB2YWx1ZVswXSkgPT0gXCJAXCIpIHtcclxuICAgICAgICAgICAgICAvLyBSZW1vdmUgdGhlIHNlbnRpbmVsIGBAYCBjaGFyYWN0ZXIuXHJcbiAgICAgICAgICAgICAgcmV0dXJuIHZhbHVlLnNsaWNlKDEpO1xyXG4gICAgICAgICAgICB9XHJcbiAgICAgICAgICAgIC8vIFBhcnNlIG9iamVjdCBhbmQgYXJyYXkgbGl0ZXJhbHMuXHJcbiAgICAgICAgICAgIGlmICh2YWx1ZSA9PSBcIltcIikge1xyXG4gICAgICAgICAgICAgIC8vIFBhcnNlcyBhIEpTT04gYXJyYXksIHJldHVybmluZyBhIG5ldyBKYXZhU2NyaXB0IGFycmF5LlxyXG4gICAgICAgICAgICAgIHJlc3VsdHMgPSBbXTtcclxuICAgICAgICAgICAgICBmb3IgKDs7IGhhc01lbWJlcnMgfHwgKGhhc01lbWJlcnMgPSB0cnVlKSkge1xyXG4gICAgICAgICAgICAgICAgdmFsdWUgPSBsZXgoKTtcclxuICAgICAgICAgICAgICAgIC8vIEEgY2xvc2luZyBzcXVhcmUgYnJhY2tldCBtYXJrcyB0aGUgZW5kIG9mIHRoZSBhcnJheSBsaXRlcmFsLlxyXG4gICAgICAgICAgICAgICAgaWYgKHZhbHVlID09IFwiXVwiKSB7XHJcbiAgICAgICAgICAgICAgICAgIGJyZWFrO1xyXG4gICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICAgICAgLy8gSWYgdGhlIGFycmF5IGxpdGVyYWwgY29udGFpbnMgZWxlbWVudHMsIHRoZSBjdXJyZW50IHRva2VuXHJcbiAgICAgICAgICAgICAgICAvLyBzaG91bGQgYmUgYSBjb21tYSBzZXBhcmF0aW5nIHRoZSBwcmV2aW91cyBlbGVtZW50IGZyb20gdGhlXHJcbiAgICAgICAgICAgICAgICAvLyBuZXh0LlxyXG4gICAgICAgICAgICAgICAgaWYgKGhhc01lbWJlcnMpIHtcclxuICAgICAgICAgICAgICAgICAgaWYgKHZhbHVlID09IFwiLFwiKSB7XHJcbiAgICAgICAgICAgICAgICAgICAgdmFsdWUgPSBsZXgoKTtcclxuICAgICAgICAgICAgICAgICAgICBpZiAodmFsdWUgPT0gXCJdXCIpIHtcclxuICAgICAgICAgICAgICAgICAgICAgIC8vIFVuZXhwZWN0ZWQgdHJhaWxpbmcgYCxgIGluIGFycmF5IGxpdGVyYWwuXHJcbiAgICAgICAgICAgICAgICAgICAgICBhYm9ydCgpO1xyXG4gICAgICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgICAgICAgfSBlbHNlIHtcclxuICAgICAgICAgICAgICAgICAgICAvLyBBIGAsYCBtdXN0IHNlcGFyYXRlIGVhY2ggYXJyYXkgZWxlbWVudC5cclxuICAgICAgICAgICAgICAgICAgICBhYm9ydCgpO1xyXG4gICAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgICAgICAvLyBFbGlzaW9ucyBhbmQgbGVhZGluZyBjb21tYXMgYXJlIG5vdCBwZXJtaXR0ZWQuXHJcbiAgICAgICAgICAgICAgICBpZiAodmFsdWUgPT0gXCIsXCIpIHtcclxuICAgICAgICAgICAgICAgICAgYWJvcnQoKTtcclxuICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgICAgIHJlc3VsdHMucHVzaChnZXQodmFsdWUpKTtcclxuICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgICAgcmV0dXJuIHJlc3VsdHM7XHJcbiAgICAgICAgICAgIH0gZWxzZSBpZiAodmFsdWUgPT0gXCJ7XCIpIHtcclxuICAgICAgICAgICAgICAvLyBQYXJzZXMgYSBKU09OIG9iamVjdCwgcmV0dXJuaW5nIGEgbmV3IEphdmFTY3JpcHQgb2JqZWN0LlxyXG4gICAgICAgICAgICAgIHJlc3VsdHMgPSB7fTtcclxuICAgICAgICAgICAgICBmb3IgKDs7IGhhc01lbWJlcnMgfHwgKGhhc01lbWJlcnMgPSB0cnVlKSkge1xyXG4gICAgICAgICAgICAgICAgdmFsdWUgPSBsZXgoKTtcclxuICAgICAgICAgICAgICAgIC8vIEEgY2xvc2luZyBjdXJseSBicmFjZSBtYXJrcyB0aGUgZW5kIG9mIHRoZSBvYmplY3QgbGl0ZXJhbC5cclxuICAgICAgICAgICAgICAgIGlmICh2YWx1ZSA9PSBcIn1cIikge1xyXG4gICAgICAgICAgICAgICAgICBicmVhaztcclxuICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgICAgIC8vIElmIHRoZSBvYmplY3QgbGl0ZXJhbCBjb250YWlucyBtZW1iZXJzLCB0aGUgY3VycmVudCB0b2tlblxyXG4gICAgICAgICAgICAgICAgLy8gc2hvdWxkIGJlIGEgY29tbWEgc2VwYXJhdG9yLlxyXG4gICAgICAgICAgICAgICAgaWYgKGhhc01lbWJlcnMpIHtcclxuICAgICAgICAgICAgICAgICAgaWYgKHZhbHVlID09IFwiLFwiKSB7XHJcbiAgICAgICAgICAgICAgICAgICAgdmFsdWUgPSBsZXgoKTtcclxuICAgICAgICAgICAgICAgICAgICBpZiAodmFsdWUgPT0gXCJ9XCIpIHtcclxuICAgICAgICAgICAgICAgICAgICAgIC8vIFVuZXhwZWN0ZWQgdHJhaWxpbmcgYCxgIGluIG9iamVjdCBsaXRlcmFsLlxyXG4gICAgICAgICAgICAgICAgICAgICAgYWJvcnQoKTtcclxuICAgICAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgICAgICAgIH0gZWxzZSB7XHJcbiAgICAgICAgICAgICAgICAgICAgLy8gQSBgLGAgbXVzdCBzZXBhcmF0ZSBlYWNoIG9iamVjdCBtZW1iZXIuXHJcbiAgICAgICAgICAgICAgICAgICAgYWJvcnQoKTtcclxuICAgICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICAgICAgLy8gTGVhZGluZyBjb21tYXMgYXJlIG5vdCBwZXJtaXR0ZWQsIG9iamVjdCBwcm9wZXJ0eSBuYW1lcyBtdXN0IGJlXHJcbiAgICAgICAgICAgICAgICAvLyBkb3VibGUtcXVvdGVkIHN0cmluZ3MsIGFuZCBhIGA6YCBtdXN0IHNlcGFyYXRlIGVhY2ggcHJvcGVydHlcclxuICAgICAgICAgICAgICAgIC8vIG5hbWUgYW5kIHZhbHVlLlxyXG4gICAgICAgICAgICAgICAgaWYgKHZhbHVlID09IFwiLFwiIHx8IHR5cGVvZiB2YWx1ZSAhPSBcInN0cmluZ1wiIHx8IChjaGFySW5kZXhCdWdneSA/IHZhbHVlLmNoYXJBdCgwKSA6IHZhbHVlWzBdKSAhPSBcIkBcIiB8fCBsZXgoKSAhPSBcIjpcIikge1xyXG4gICAgICAgICAgICAgICAgICBhYm9ydCgpO1xyXG4gICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICAgICAgcmVzdWx0c1t2YWx1ZS5zbGljZSgxKV0gPSBnZXQobGV4KCkpO1xyXG4gICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgICByZXR1cm4gcmVzdWx0cztcclxuICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICAvLyBVbmV4cGVjdGVkIHRva2VuIGVuY291bnRlcmVkLlxyXG4gICAgICAgICAgICBhYm9ydCgpO1xyXG4gICAgICAgICAgfVxyXG4gICAgICAgICAgcmV0dXJuIHZhbHVlO1xyXG4gICAgICAgIH07XHJcblxyXG4gICAgICAgIC8vIEludGVybmFsOiBVcGRhdGVzIGEgdHJhdmVyc2VkIG9iamVjdCBtZW1iZXIuXHJcbiAgICAgICAgdmFyIHVwZGF0ZSA9IGZ1bmN0aW9uIChzb3VyY2UsIHByb3BlcnR5LCBjYWxsYmFjaykge1xyXG4gICAgICAgICAgdmFyIGVsZW1lbnQgPSB3YWxrKHNvdXJjZSwgcHJvcGVydHksIGNhbGxiYWNrKTtcclxuICAgICAgICAgIGlmIChlbGVtZW50ID09PSB1bmRlZikge1xyXG4gICAgICAgICAgICBkZWxldGUgc291cmNlW3Byb3BlcnR5XTtcclxuICAgICAgICAgIH0gZWxzZSB7XHJcbiAgICAgICAgICAgIHNvdXJjZVtwcm9wZXJ0eV0gPSBlbGVtZW50O1xyXG4gICAgICAgICAgfVxyXG4gICAgICAgIH07XHJcblxyXG4gICAgICAgIC8vIEludGVybmFsOiBSZWN1cnNpdmVseSB0cmF2ZXJzZXMgYSBwYXJzZWQgSlNPTiBvYmplY3QsIGludm9raW5nIHRoZVxyXG4gICAgICAgIC8vIGBjYWxsYmFja2AgZnVuY3Rpb24gZm9yIGVhY2ggdmFsdWUuIFRoaXMgaXMgYW4gaW1wbGVtZW50YXRpb24gb2YgdGhlXHJcbiAgICAgICAgLy8gYFdhbGsoaG9sZGVyLCBuYW1lKWAgb3BlcmF0aW9uIGRlZmluZWQgaW4gRVMgNS4xIHNlY3Rpb24gMTUuMTIuMi5cclxuICAgICAgICB2YXIgd2FsayA9IGZ1bmN0aW9uIChzb3VyY2UsIHByb3BlcnR5LCBjYWxsYmFjaykge1xyXG4gICAgICAgICAgdmFyIHZhbHVlID0gc291cmNlW3Byb3BlcnR5XSwgbGVuZ3RoO1xyXG4gICAgICAgICAgaWYgKHR5cGVvZiB2YWx1ZSA9PSBcIm9iamVjdFwiICYmIHZhbHVlKSB7XHJcbiAgICAgICAgICAgIC8vIGBmb3JFYWNoYCBjYW4ndCBiZSB1c2VkIHRvIHRyYXZlcnNlIGFuIGFycmF5IGluIE9wZXJhIDw9IDguNTRcclxuICAgICAgICAgICAgLy8gYmVjYXVzZSBpdHMgYE9iamVjdCNoYXNPd25Qcm9wZXJ0eWAgaW1wbGVtZW50YXRpb24gcmV0dXJucyBgZmFsc2VgXHJcbiAgICAgICAgICAgIC8vIGZvciBhcnJheSBpbmRpY2VzIChlLmcuLCBgIVsxLCAyLCAzXS5oYXNPd25Qcm9wZXJ0eShcIjBcIilgKS5cclxuICAgICAgICAgICAgaWYgKGdldENsYXNzLmNhbGwodmFsdWUpID09IGFycmF5Q2xhc3MpIHtcclxuICAgICAgICAgICAgICBmb3IgKGxlbmd0aCA9IHZhbHVlLmxlbmd0aDsgbGVuZ3RoLS07KSB7XHJcbiAgICAgICAgICAgICAgICB1cGRhdGUodmFsdWUsIGxlbmd0aCwgY2FsbGJhY2spO1xyXG4gICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgfSBlbHNlIHtcclxuICAgICAgICAgICAgICBmb3JFYWNoKHZhbHVlLCBmdW5jdGlvbiAocHJvcGVydHkpIHtcclxuICAgICAgICAgICAgICAgIHVwZGF0ZSh2YWx1ZSwgcHJvcGVydHksIGNhbGxiYWNrKTtcclxuICAgICAgICAgICAgICB9KTtcclxuICAgICAgICAgICAgfVxyXG4gICAgICAgICAgfVxyXG4gICAgICAgICAgcmV0dXJuIGNhbGxiYWNrLmNhbGwoc291cmNlLCBwcm9wZXJ0eSwgdmFsdWUpO1xyXG4gICAgICAgIH07XHJcblxyXG4gICAgICAgIC8vIFB1YmxpYzogYEpTT04ucGFyc2VgLiBTZWUgRVMgNS4xIHNlY3Rpb24gMTUuMTIuMi5cclxuICAgICAgICBleHBvcnRzLnBhcnNlID0gZnVuY3Rpb24gKHNvdXJjZSwgY2FsbGJhY2spIHtcclxuICAgICAgICAgIHZhciByZXN1bHQsIHZhbHVlO1xyXG4gICAgICAgICAgSW5kZXggPSAwO1xyXG4gICAgICAgICAgU291cmNlID0gXCJcIiArIHNvdXJjZTtcclxuICAgICAgICAgIHJlc3VsdCA9IGdldChsZXgoKSk7XHJcbiAgICAgICAgICAvLyBJZiBhIEpTT04gc3RyaW5nIGNvbnRhaW5zIG11bHRpcGxlIHRva2VucywgaXQgaXMgaW52YWxpZC5cclxuICAgICAgICAgIGlmIChsZXgoKSAhPSBcIiRcIikge1xyXG4gICAgICAgICAgICBhYm9ydCgpO1xyXG4gICAgICAgICAgfVxyXG4gICAgICAgICAgLy8gUmVzZXQgdGhlIHBhcnNlciBzdGF0ZS5cclxuICAgICAgICAgIEluZGV4ID0gU291cmNlID0gbnVsbDtcclxuICAgICAgICAgIHJldHVybiBjYWxsYmFjayAmJiBnZXRDbGFzcy5jYWxsKGNhbGxiYWNrKSA9PSBmdW5jdGlvbkNsYXNzID8gd2FsaygodmFsdWUgPSB7fSwgdmFsdWVbXCJcIl0gPSByZXN1bHQsIHZhbHVlKSwgXCJcIiwgY2FsbGJhY2spIDogcmVzdWx0O1xyXG4gICAgICAgIH07XHJcbiAgICAgIH1cclxuICAgIH1cclxuXHJcbiAgICBleHBvcnRzW1wicnVuSW5Db250ZXh0XCJdID0gcnVuSW5Db250ZXh0O1xyXG4gICAgcmV0dXJuIGV4cG9ydHM7XHJcbiAgfVxyXG5cclxuICBpZiAoZnJlZUV4cG9ydHMgJiYgIWlzTG9hZGVyKSB7XHJcbiAgICAvLyBFeHBvcnQgZm9yIENvbW1vbkpTIGVudmlyb25tZW50cy5cclxuICAgIHJ1bkluQ29udGV4dChyb290LCBmcmVlRXhwb3J0cyk7XHJcbiAgfSBlbHNlIHtcclxuICAgIC8vIEV4cG9ydCBmb3Igd2ViIGJyb3dzZXJzIGFuZCBKYXZhU2NyaXB0IGVuZ2luZXMuXHJcbiAgICB2YXIgbmF0aXZlSlNPTiA9IHJvb3QuSlNPTixcclxuICAgICAgICBwcmV2aW91c0pTT04gPSByb290W1wiSlNPTjNcIl0sXHJcbiAgICAgICAgaXNSZXN0b3JlZCA9IGZhbHNlO1xyXG5cclxuICAgIHZhciBKU09OMyA9IHJ1bkluQ29udGV4dChyb290LCAocm9vdFtcIkpTT04zXCJdID0ge1xyXG4gICAgICAvLyBQdWJsaWM6IFJlc3RvcmVzIHRoZSBvcmlnaW5hbCB2YWx1ZSBvZiB0aGUgZ2xvYmFsIGBKU09OYCBvYmplY3QgYW5kXHJcbiAgICAgIC8vIHJldHVybnMgYSByZWZlcmVuY2UgdG8gdGhlIGBKU09OM2Agb2JqZWN0LlxyXG4gICAgICBcIm5vQ29uZmxpY3RcIjogZnVuY3Rpb24gKCkge1xyXG4gICAgICAgIGlmICghaXNSZXN0b3JlZCkge1xyXG4gICAgICAgICAgaXNSZXN0b3JlZCA9IHRydWU7XHJcbiAgICAgICAgICByb290LkpTT04gPSBuYXRpdmVKU09OO1xyXG4gICAgICAgICAgcm9vdFtcIkpTT04zXCJdID0gcHJldmlvdXNKU09OO1xyXG4gICAgICAgICAgbmF0aXZlSlNPTiA9IHByZXZpb3VzSlNPTiA9IG51bGw7XHJcbiAgICAgICAgfVxyXG4gICAgICAgIHJldHVybiBKU09OMztcclxuICAgICAgfVxyXG4gICAgfSkpO1xyXG5cclxuICAgIHJvb3QuSlNPTiA9IHtcclxuICAgICAgXCJwYXJzZVwiOiBKU09OMy5wYXJzZSxcclxuICAgICAgXCJzdHJpbmdpZnlcIjogSlNPTjMuc3RyaW5naWZ5XHJcbiAgICB9O1xyXG4gIH1cclxuXHJcbiAgLy8gRXhwb3J0IGZvciBhc3luY2hyb25vdXMgbW9kdWxlIGxvYWRlcnMuXHJcbiAgaWYgKGlzTG9hZGVyKSB7XHJcbiAgICBkZWZpbmUoZnVuY3Rpb24gKCkge1xyXG4gICAgICByZXR1cm4gSlNPTjM7XHJcbiAgICB9KTtcclxuICB9XHJcbn0pLmNhbGwodGhpcyk7XHJcblxyXG59KS5jYWxsKHRoaXMsdHlwZW9mIGdsb2JhbCAhPT0gXCJ1bmRlZmluZWRcIiA/IGdsb2JhbCA6IHR5cGVvZiBzZWxmICE9PSBcInVuZGVmaW5lZFwiID8gc2VsZiA6IHR5cGVvZiB3aW5kb3cgIT09IFwidW5kZWZpbmVkXCIgPyB3aW5kb3cgOiB7fSlcclxuXHJcbn0se31dLDU5OltmdW5jdGlvbihyZXF1aXJlLG1vZHVsZSxleHBvcnRzKXtcclxuJ3VzZSBzdHJpY3QnO1xyXG5cclxudmFyIGhhcyA9IE9iamVjdC5wcm90b3R5cGUuaGFzT3duUHJvcGVydHk7XHJcblxyXG4vKipcclxuICogU2ltcGxlIHF1ZXJ5IHN0cmluZyBwYXJzZXIuXHJcbiAqXHJcbiAqIEBwYXJhbSB7U3RyaW5nfSBxdWVyeSBUaGUgcXVlcnkgc3RyaW5nIHRoYXQgbmVlZHMgdG8gYmUgcGFyc2VkLlxyXG4gKiBAcmV0dXJucyB7T2JqZWN0fVxyXG4gKiBAYXBpIHB1YmxpY1xyXG4gKi9cclxuZnVuY3Rpb24gcXVlcnlzdHJpbmcocXVlcnkpIHtcclxuICB2YXIgcGFyc2VyID0gLyhbXj0/Jl0rKT0/KFteJl0qKS9nXHJcbiAgICAsIHJlc3VsdCA9IHt9XHJcbiAgICAsIHBhcnQ7XHJcblxyXG4gIC8vXHJcbiAgLy8gTGl0dGxlIG5pZnR5IHBhcnNpbmcgaGFjaywgbGV2ZXJhZ2UgdGhlIGZhY3QgdGhhdCBSZWdFeHAuZXhlYyBpbmNyZW1lbnRzXHJcbiAgLy8gdGhlIGxhc3RJbmRleCBwcm9wZXJ0eSBzbyB3ZSBjYW4gY29udGludWUgZXhlY3V0aW5nIHRoaXMgbG9vcCB1bnRpbCB3ZSd2ZVxyXG4gIC8vIHBhcnNlZCBhbGwgcmVzdWx0cy5cclxuICAvL1xyXG4gIGZvciAoO1xyXG4gICAgcGFydCA9IHBhcnNlci5leGVjKHF1ZXJ5KTtcclxuICAgIHJlc3VsdFtkZWNvZGVVUklDb21wb25lbnQocGFydFsxXSldID0gZGVjb2RlVVJJQ29tcG9uZW50KHBhcnRbMl0pXHJcbiAgKTtcclxuXHJcbiAgcmV0dXJuIHJlc3VsdDtcclxufVxyXG5cclxuLyoqXHJcbiAqIFRyYW5zZm9ybSBhIHF1ZXJ5IHN0cmluZyB0byBhbiBvYmplY3QuXHJcbiAqXHJcbiAqIEBwYXJhbSB7T2JqZWN0fSBvYmogT2JqZWN0IHRoYXQgc2hvdWxkIGJlIHRyYW5zZm9ybWVkLlxyXG4gKiBAcGFyYW0ge1N0cmluZ30gcHJlZml4IE9wdGlvbmFsIHByZWZpeC5cclxuICogQHJldHVybnMge1N0cmluZ31cclxuICogQGFwaSBwdWJsaWNcclxuICovXHJcbmZ1bmN0aW9uIHF1ZXJ5c3RyaW5naWZ5KG9iaiwgcHJlZml4KSB7XHJcbiAgcHJlZml4ID0gcHJlZml4IHx8ICcnO1xyXG5cclxuICB2YXIgcGFpcnMgPSBbXTtcclxuXHJcbiAgLy9cclxuICAvLyBPcHRpb25hbGx5IHByZWZpeCB3aXRoIGEgJz8nIGlmIG5lZWRlZFxyXG4gIC8vXHJcbiAgaWYgKCdzdHJpbmcnICE9PSB0eXBlb2YgcHJlZml4KSBwcmVmaXggPSAnPyc7XHJcblxyXG4gIGZvciAodmFyIGtleSBpbiBvYmopIHtcclxuICAgIGlmIChoYXMuY2FsbChvYmosIGtleSkpIHtcclxuICAgICAgcGFpcnMucHVzaChlbmNvZGVVUklDb21wb25lbnQoa2V5KSArJz0nKyBlbmNvZGVVUklDb21wb25lbnQob2JqW2tleV0pKTtcclxuICAgIH1cclxuICB9XHJcblxyXG4gIHJldHVybiBwYWlycy5sZW5ndGggPyBwcmVmaXggKyBwYWlycy5qb2luKCcmJykgOiAnJztcclxufVxyXG5cclxuLy9cclxuLy8gRXhwb3NlIHRoZSBtb2R1bGUuXHJcbi8vXHJcbmV4cG9ydHMuc3RyaW5naWZ5ID0gcXVlcnlzdHJpbmdpZnk7XHJcbmV4cG9ydHMucGFyc2UgPSBxdWVyeXN0cmluZztcclxuXHJcbn0se31dLDYwOltmdW5jdGlvbihyZXF1aXJlLG1vZHVsZSxleHBvcnRzKXtcclxuJ3VzZSBzdHJpY3QnO1xyXG5cclxuLyoqXHJcbiAqIENoZWNrIGlmIHdlJ3JlIHJlcXVpcmVkIHRvIGFkZCBhIHBvcnQgbnVtYmVyLlxyXG4gKlxyXG4gKiBAc2VlIGh0dHBzOi8vdXJsLnNwZWMud2hhdHdnLm9yZy8jZGVmYXVsdC1wb3J0XHJcbiAqIEBwYXJhbSB7TnVtYmVyfFN0cmluZ30gcG9ydCBQb3J0IG51bWJlciB3ZSBuZWVkIHRvIGNoZWNrXHJcbiAqIEBwYXJhbSB7U3RyaW5nfSBwcm90b2NvbCBQcm90b2NvbCB3ZSBuZWVkIHRvIGNoZWNrIGFnYWluc3QuXHJcbiAqIEByZXR1cm5zIHtCb29sZWFufSBJcyBpdCBhIGRlZmF1bHQgcG9ydCBmb3IgdGhlIGdpdmVuIHByb3RvY29sXHJcbiAqIEBhcGkgcHJpdmF0ZVxyXG4gKi9cclxubW9kdWxlLmV4cG9ydHMgPSBmdW5jdGlvbiByZXF1aXJlZChwb3J0LCBwcm90b2NvbCkge1xyXG4gIHByb3RvY29sID0gcHJvdG9jb2wuc3BsaXQoJzonKVswXTtcclxuICBwb3J0ID0gK3BvcnQ7XHJcblxyXG4gIGlmICghcG9ydCkgcmV0dXJuIGZhbHNlO1xyXG5cclxuICBzd2l0Y2ggKHByb3RvY29sKSB7XHJcbiAgICBjYXNlICdodHRwJzpcclxuICAgIGNhc2UgJ3dzJzpcclxuICAgIHJldHVybiBwb3J0ICE9PSA4MDtcclxuXHJcbiAgICBjYXNlICdodHRwcyc6XHJcbiAgICBjYXNlICd3c3MnOlxyXG4gICAgcmV0dXJuIHBvcnQgIT09IDQ0MztcclxuXHJcbiAgICBjYXNlICdmdHAnOlxyXG4gICAgcmV0dXJuIHBvcnQgIT09IDIxO1xyXG5cclxuICAgIGNhc2UgJ2dvcGhlcic6XHJcbiAgICByZXR1cm4gcG9ydCAhPT0gNzA7XHJcblxyXG4gICAgY2FzZSAnZmlsZSc6XHJcbiAgICByZXR1cm4gZmFsc2U7XHJcbiAgfVxyXG5cclxuICByZXR1cm4gcG9ydCAhPT0gMDtcclxufTtcclxuXHJcbn0se31dLDYxOltmdW5jdGlvbihyZXF1aXJlLG1vZHVsZSxleHBvcnRzKXtcclxuJ3VzZSBzdHJpY3QnO1xyXG5cclxudmFyIHJlcXVpcmVkID0gcmVxdWlyZSgncmVxdWlyZXMtcG9ydCcpXHJcbiAgLCBsb2xjYXRpb24gPSByZXF1aXJlKCcuL2xvbGNhdGlvbicpXHJcbiAgLCBxcyA9IHJlcXVpcmUoJ3F1ZXJ5c3RyaW5naWZ5JylcclxuICAsIHByb3RvY29scmUgPSAvXihbYS16XVthLXowLTkuKy1dKjopPyhcXC9cXC8pPyhbXFxTXFxzXSopL2k7XHJcblxyXG4vKipcclxuICogVGhlc2UgYXJlIHRoZSBwYXJzZSBydWxlcyBmb3IgdGhlIFVSTCBwYXJzZXIsIGl0IGluZm9ybXMgdGhlIHBhcnNlclxyXG4gKiBhYm91dDpcclxuICpcclxuICogMC4gVGhlIGNoYXIgaXQgTmVlZHMgdG8gcGFyc2UsIGlmIGl0J3MgYSBzdHJpbmcgaXQgc2hvdWxkIGJlIGRvbmUgdXNpbmdcclxuICogICAgaW5kZXhPZiwgUmVnRXhwIHVzaW5nIGV4ZWMgYW5kIE5hTiBtZWFucyBzZXQgYXMgY3VycmVudCB2YWx1ZS5cclxuICogMS4gVGhlIHByb3BlcnR5IHdlIHNob3VsZCBzZXQgd2hlbiBwYXJzaW5nIHRoaXMgdmFsdWUuXHJcbiAqIDIuIEluZGljYXRpb24gaWYgaXQncyBiYWNrd2FyZHMgb3IgZm9yd2FyZCBwYXJzaW5nLCB3aGVuIHNldCBhcyBudW1iZXIgaXQnc1xyXG4gKiAgICB0aGUgdmFsdWUgb2YgZXh0cmEgY2hhcnMgdGhhdCBzaG91bGQgYmUgc3BsaXQgb2ZmLlxyXG4gKiAzLiBJbmhlcml0IGZyb20gbG9jYXRpb24gaWYgbm9uIGV4aXN0aW5nIGluIHRoZSBwYXJzZXIuXHJcbiAqIDQuIGB0b0xvd2VyQ2FzZWAgdGhlIHJlc3VsdGluZyB2YWx1ZS5cclxuICovXHJcbnZhciBydWxlcyA9IFtcclxuICBbJyMnLCAnaGFzaCddLCAgICAgICAgICAgICAgICAgICAgICAgIC8vIEV4dHJhY3QgZnJvbSB0aGUgYmFjay5cclxuICBbJz8nLCAncXVlcnknXSwgICAgICAgICAgICAgICAgICAgICAgIC8vIEV4dHJhY3QgZnJvbSB0aGUgYmFjay5cclxuICBbJy8nLCAncGF0aG5hbWUnXSwgICAgICAgICAgICAgICAgICAgIC8vIEV4dHJhY3QgZnJvbSB0aGUgYmFjay5cclxuICBbJ0AnLCAnYXV0aCcsIDFdLCAgICAgICAgICAgICAgICAgICAgIC8vIEV4dHJhY3QgZnJvbSB0aGUgZnJvbnQuXHJcbiAgW05hTiwgJ2hvc3QnLCB1bmRlZmluZWQsIDEsIDFdLCAgICAgICAvLyBTZXQgbGVmdCBvdmVyIHZhbHVlLlxyXG4gIFsvOihcXGQrKSQvLCAncG9ydCcsIHVuZGVmaW5lZCwgMV0sICAgIC8vIFJlZ0V4cCB0aGUgYmFjay5cclxuICBbTmFOLCAnaG9zdG5hbWUnLCB1bmRlZmluZWQsIDEsIDFdICAgIC8vIFNldCBsZWZ0IG92ZXIuXHJcbl07XHJcblxyXG4vKipcclxuICogQHR5cGVkZWYgUHJvdG9jb2xFeHRyYWN0XHJcbiAqIEB0eXBlIE9iamVjdFxyXG4gKiBAcHJvcGVydHkge1N0cmluZ30gcHJvdG9jb2wgUHJvdG9jb2wgbWF0Y2hlZCBpbiB0aGUgVVJMLCBpbiBsb3dlcmNhc2UuXHJcbiAqIEBwcm9wZXJ0eSB7Qm9vbGVhbn0gc2xhc2hlcyBgdHJ1ZWAgaWYgcHJvdG9jb2wgaXMgZm9sbG93ZWQgYnkgXCIvL1wiLCBlbHNlIGBmYWxzZWAuXHJcbiAqIEBwcm9wZXJ0eSB7U3RyaW5nfSByZXN0IFJlc3Qgb2YgdGhlIFVSTCB0aGF0IGlzIG5vdCBwYXJ0IG9mIHRoZSBwcm90b2NvbC5cclxuICovXHJcblxyXG4vKipcclxuICogRXh0cmFjdCBwcm90b2NvbCBpbmZvcm1hdGlvbiBmcm9tIGEgVVJMIHdpdGgvd2l0aG91dCBkb3VibGUgc2xhc2ggKFwiLy9cIikuXHJcbiAqXHJcbiAqIEBwYXJhbSB7U3RyaW5nfSBhZGRyZXNzIFVSTCB3ZSB3YW50IHRvIGV4dHJhY3QgZnJvbS5cclxuICogQHJldHVybiB7UHJvdG9jb2xFeHRyYWN0fSBFeHRyYWN0ZWQgaW5mb3JtYXRpb24uXHJcbiAqIEBhcGkgcHJpdmF0ZVxyXG4gKi9cclxuZnVuY3Rpb24gZXh0cmFjdFByb3RvY29sKGFkZHJlc3MpIHtcclxuICB2YXIgbWF0Y2ggPSBwcm90b2NvbHJlLmV4ZWMoYWRkcmVzcyk7XHJcblxyXG4gIHJldHVybiB7XHJcbiAgICBwcm90b2NvbDogbWF0Y2hbMV0gPyBtYXRjaFsxXS50b0xvd2VyQ2FzZSgpIDogJycsXHJcbiAgICBzbGFzaGVzOiAhIW1hdGNoWzJdLFxyXG4gICAgcmVzdDogbWF0Y2hbM11cclxuICB9O1xyXG59XHJcblxyXG4vKipcclxuICogUmVzb2x2ZSBhIHJlbGF0aXZlIFVSTCBwYXRobmFtZSBhZ2FpbnN0IGEgYmFzZSBVUkwgcGF0aG5hbWUuXHJcbiAqXHJcbiAqIEBwYXJhbSB7U3RyaW5nfSByZWxhdGl2ZSBQYXRobmFtZSBvZiB0aGUgcmVsYXRpdmUgVVJMLlxyXG4gKiBAcGFyYW0ge1N0cmluZ30gYmFzZSBQYXRobmFtZSBvZiB0aGUgYmFzZSBVUkwuXHJcbiAqIEByZXR1cm4ge1N0cmluZ30gUmVzb2x2ZWQgcGF0aG5hbWUuXHJcbiAqIEBhcGkgcHJpdmF0ZVxyXG4gKi9cclxuZnVuY3Rpb24gcmVzb2x2ZShyZWxhdGl2ZSwgYmFzZSkge1xyXG4gIHZhciBwYXRoID0gKGJhc2UgfHwgJy8nKS5zcGxpdCgnLycpLnNsaWNlKDAsIC0xKS5jb25jYXQocmVsYXRpdmUuc3BsaXQoJy8nKSlcclxuICAgICwgaSA9IHBhdGgubGVuZ3RoXHJcbiAgICAsIGxhc3QgPSBwYXRoW2kgLSAxXVxyXG4gICAgLCB1bnNoaWZ0ID0gZmFsc2VcclxuICAgICwgdXAgPSAwO1xyXG5cclxuICB3aGlsZSAoaS0tKSB7XHJcbiAgICBpZiAocGF0aFtpXSA9PT0gJy4nKSB7XHJcbiAgICAgIHBhdGguc3BsaWNlKGksIDEpO1xyXG4gICAgfSBlbHNlIGlmIChwYXRoW2ldID09PSAnLi4nKSB7XHJcbiAgICAgIHBhdGguc3BsaWNlKGksIDEpO1xyXG4gICAgICB1cCsrO1xyXG4gICAgfSBlbHNlIGlmICh1cCkge1xyXG4gICAgICBpZiAoaSA9PT0gMCkgdW5zaGlmdCA9IHRydWU7XHJcbiAgICAgIHBhdGguc3BsaWNlKGksIDEpO1xyXG4gICAgICB1cC0tO1xyXG4gICAgfVxyXG4gIH1cclxuXHJcbiAgaWYgKHVuc2hpZnQpIHBhdGgudW5zaGlmdCgnJyk7XHJcbiAgaWYgKGxhc3QgPT09ICcuJyB8fCBsYXN0ID09PSAnLi4nKSBwYXRoLnB1c2goJycpO1xyXG5cclxuICByZXR1cm4gcGF0aC5qb2luKCcvJyk7XHJcbn1cclxuXHJcbi8qKlxyXG4gKiBUaGUgYWN0dWFsIFVSTCBpbnN0YW5jZS4gSW5zdGVhZCBvZiByZXR1cm5pbmcgYW4gb2JqZWN0IHdlJ3ZlIG9wdGVkLWluIHRvXHJcbiAqIGNyZWF0ZSBhbiBhY3R1YWwgY29uc3RydWN0b3IgYXMgaXQncyBtdWNoIG1vcmUgbWVtb3J5IGVmZmljaWVudCBhbmRcclxuICogZmFzdGVyIGFuZCBpdCBwbGVhc2VzIG15IE9DRC5cclxuICpcclxuICogQGNvbnN0cnVjdG9yXHJcbiAqIEBwYXJhbSB7U3RyaW5nfSBhZGRyZXNzIFVSTCB3ZSB3YW50IHRvIHBhcnNlLlxyXG4gKiBAcGFyYW0ge09iamVjdHxTdHJpbmd9IGxvY2F0aW9uIExvY2F0aW9uIGRlZmF1bHRzIGZvciByZWxhdGl2ZSBwYXRocy5cclxuICogQHBhcmFtIHtCb29sZWFufEZ1bmN0aW9ufSBwYXJzZXIgUGFyc2VyIGZvciB0aGUgcXVlcnkgc3RyaW5nLlxyXG4gKiBAYXBpIHB1YmxpY1xyXG4gKi9cclxuZnVuY3Rpb24gVVJMKGFkZHJlc3MsIGxvY2F0aW9uLCBwYXJzZXIpIHtcclxuICBpZiAoISh0aGlzIGluc3RhbmNlb2YgVVJMKSkge1xyXG4gICAgcmV0dXJuIG5ldyBVUkwoYWRkcmVzcywgbG9jYXRpb24sIHBhcnNlcik7XHJcbiAgfVxyXG5cclxuICB2YXIgcmVsYXRpdmUsIGV4dHJhY3RlZCwgcGFyc2UsIGluc3RydWN0aW9uLCBpbmRleCwga2V5XHJcbiAgICAsIGluc3RydWN0aW9ucyA9IHJ1bGVzLnNsaWNlKClcclxuICAgICwgdHlwZSA9IHR5cGVvZiBsb2NhdGlvblxyXG4gICAgLCB1cmwgPSB0aGlzXHJcbiAgICAsIGkgPSAwO1xyXG5cclxuICAvL1xyXG4gIC8vIFRoZSBmb2xsb3dpbmcgaWYgc3RhdGVtZW50cyBhbGxvd3MgdGhpcyBtb2R1bGUgdHdvIGhhdmUgY29tcGF0aWJpbGl0eSB3aXRoXHJcbiAgLy8gMiBkaWZmZXJlbnQgQVBJOlxyXG4gIC8vXHJcbiAgLy8gMS4gTm9kZS5qcydzIGB1cmwucGFyc2VgIGFwaSB3aGljaCBhY2NlcHRzIGEgVVJMLCBib29sZWFuIGFzIGFyZ3VtZW50c1xyXG4gIC8vICAgIHdoZXJlIHRoZSBib29sZWFuIGluZGljYXRlcyB0aGF0IHRoZSBxdWVyeSBzdHJpbmcgc2hvdWxkIGFsc28gYmUgcGFyc2VkLlxyXG4gIC8vXHJcbiAgLy8gMi4gVGhlIGBVUkxgIGludGVyZmFjZSBvZiB0aGUgYnJvd3NlciB3aGljaCBhY2NlcHRzIGEgVVJMLCBvYmplY3QgYXNcclxuICAvLyAgICBhcmd1bWVudHMuIFRoZSBzdXBwbGllZCBvYmplY3Qgd2lsbCBiZSB1c2VkIGFzIGRlZmF1bHQgdmFsdWVzIC8gZmFsbC1iYWNrXHJcbiAgLy8gICAgZm9yIHJlbGF0aXZlIHBhdGhzLlxyXG4gIC8vXHJcbiAgaWYgKCdvYmplY3QnICE9PSB0eXBlICYmICdzdHJpbmcnICE9PSB0eXBlKSB7XHJcbiAgICBwYXJzZXIgPSBsb2NhdGlvbjtcclxuICAgIGxvY2F0aW9uID0gbnVsbDtcclxuICB9XHJcblxyXG4gIGlmIChwYXJzZXIgJiYgJ2Z1bmN0aW9uJyAhPT0gdHlwZW9mIHBhcnNlcikgcGFyc2VyID0gcXMucGFyc2U7XHJcblxyXG4gIGxvY2F0aW9uID0gbG9sY2F0aW9uKGxvY2F0aW9uKTtcclxuXHJcbiAgLy9cclxuICAvLyBFeHRyYWN0IHByb3RvY29sIGluZm9ybWF0aW9uIGJlZm9yZSBydW5uaW5nIHRoZSBpbnN0cnVjdGlvbnMuXHJcbiAgLy9cclxuICBleHRyYWN0ZWQgPSBleHRyYWN0UHJvdG9jb2woYWRkcmVzcyB8fCAnJyk7XHJcbiAgcmVsYXRpdmUgPSAhZXh0cmFjdGVkLnByb3RvY29sICYmICFleHRyYWN0ZWQuc2xhc2hlcztcclxuICB1cmwuc2xhc2hlcyA9IGV4dHJhY3RlZC5zbGFzaGVzIHx8IHJlbGF0aXZlICYmIGxvY2F0aW9uLnNsYXNoZXM7XHJcbiAgdXJsLnByb3RvY29sID0gZXh0cmFjdGVkLnByb3RvY29sIHx8IGxvY2F0aW9uLnByb3RvY29sIHx8ICcnO1xyXG4gIGFkZHJlc3MgPSBleHRyYWN0ZWQucmVzdDtcclxuXHJcbiAgLy9cclxuICAvLyBXaGVuIHRoZSBhdXRob3JpdHkgY29tcG9uZW50IGlzIGFic2VudCB0aGUgVVJMIHN0YXJ0cyB3aXRoIGEgcGF0aFxyXG4gIC8vIGNvbXBvbmVudC5cclxuICAvL1xyXG4gIGlmICghZXh0cmFjdGVkLnNsYXNoZXMpIGluc3RydWN0aW9uc1syXSA9IFsvKC4qKS8sICdwYXRobmFtZSddO1xyXG5cclxuICBmb3IgKDsgaSA8IGluc3RydWN0aW9ucy5sZW5ndGg7IGkrKykge1xyXG4gICAgaW5zdHJ1Y3Rpb24gPSBpbnN0cnVjdGlvbnNbaV07XHJcbiAgICBwYXJzZSA9IGluc3RydWN0aW9uWzBdO1xyXG4gICAga2V5ID0gaW5zdHJ1Y3Rpb25bMV07XHJcblxyXG4gICAgaWYgKHBhcnNlICE9PSBwYXJzZSkge1xyXG4gICAgICB1cmxba2V5XSA9IGFkZHJlc3M7XHJcbiAgICB9IGVsc2UgaWYgKCdzdHJpbmcnID09PSB0eXBlb2YgcGFyc2UpIHtcclxuICAgICAgaWYgKH4oaW5kZXggPSBhZGRyZXNzLmluZGV4T2YocGFyc2UpKSkge1xyXG4gICAgICAgIGlmICgnbnVtYmVyJyA9PT0gdHlwZW9mIGluc3RydWN0aW9uWzJdKSB7XHJcbiAgICAgICAgICB1cmxba2V5XSA9IGFkZHJlc3Muc2xpY2UoMCwgaW5kZXgpO1xyXG4gICAgICAgICAgYWRkcmVzcyA9IGFkZHJlc3Muc2xpY2UoaW5kZXggKyBpbnN0cnVjdGlvblsyXSk7XHJcbiAgICAgICAgfSBlbHNlIHtcclxuICAgICAgICAgIHVybFtrZXldID0gYWRkcmVzcy5zbGljZShpbmRleCk7XHJcbiAgICAgICAgICBhZGRyZXNzID0gYWRkcmVzcy5zbGljZSgwLCBpbmRleCk7XHJcbiAgICAgICAgfVxyXG4gICAgICB9XHJcbiAgICB9IGVsc2UgaWYgKChpbmRleCA9IHBhcnNlLmV4ZWMoYWRkcmVzcykpKSB7XHJcbiAgICAgIHVybFtrZXldID0gaW5kZXhbMV07XHJcbiAgICAgIGFkZHJlc3MgPSBhZGRyZXNzLnNsaWNlKDAsIGluZGV4LmluZGV4KTtcclxuICAgIH1cclxuXHJcbiAgICB1cmxba2V5XSA9IHVybFtrZXldIHx8IChcclxuICAgICAgcmVsYXRpdmUgJiYgaW5zdHJ1Y3Rpb25bM10gPyBsb2NhdGlvbltrZXldIHx8ICcnIDogJydcclxuICAgICk7XHJcblxyXG4gICAgLy9cclxuICAgIC8vIEhvc3RuYW1lLCBob3N0IGFuZCBwcm90b2NvbCBzaG91bGQgYmUgbG93ZXJjYXNlZCBzbyB0aGV5IGNhbiBiZSB1c2VkIHRvXHJcbiAgICAvLyBjcmVhdGUgYSBwcm9wZXIgYG9yaWdpbmAuXHJcbiAgICAvL1xyXG4gICAgaWYgKGluc3RydWN0aW9uWzRdKSB1cmxba2V5XSA9IHVybFtrZXldLnRvTG93ZXJDYXNlKCk7XHJcbiAgfVxyXG5cclxuICAvL1xyXG4gIC8vIEFsc28gcGFyc2UgdGhlIHN1cHBsaWVkIHF1ZXJ5IHN0cmluZyBpbiB0byBhbiBvYmplY3QuIElmIHdlJ3JlIHN1cHBsaWVkXHJcbiAgLy8gd2l0aCBhIGN1c3RvbSBwYXJzZXIgYXMgZnVuY3Rpb24gdXNlIHRoYXQgaW5zdGVhZCBvZiB0aGUgZGVmYXVsdCBidWlsZC1pblxyXG4gIC8vIHBhcnNlci5cclxuICAvL1xyXG4gIGlmIChwYXJzZXIpIHVybC5xdWVyeSA9IHBhcnNlcih1cmwucXVlcnkpO1xyXG5cclxuICAvL1xyXG4gIC8vIElmIHRoZSBVUkwgaXMgcmVsYXRpdmUsIHJlc29sdmUgdGhlIHBhdGhuYW1lIGFnYWluc3QgdGhlIGJhc2UgVVJMLlxyXG4gIC8vXHJcbiAgaWYgKFxyXG4gICAgICByZWxhdGl2ZVxyXG4gICAgJiYgbG9jYXRpb24uc2xhc2hlc1xyXG4gICAgJiYgdXJsLnBhdGhuYW1lLmNoYXJBdCgwKSAhPT0gJy8nXHJcbiAgICAmJiAodXJsLnBhdGhuYW1lICE9PSAnJyB8fCBsb2NhdGlvbi5wYXRobmFtZSAhPT0gJycpXHJcbiAgKSB7XHJcbiAgICB1cmwucGF0aG5hbWUgPSByZXNvbHZlKHVybC5wYXRobmFtZSwgbG9jYXRpb24ucGF0aG5hbWUpO1xyXG4gIH1cclxuXHJcbiAgLy9cclxuICAvLyBXZSBzaG91bGQgbm90IGFkZCBwb3J0IG51bWJlcnMgaWYgdGhleSBhcmUgYWxyZWFkeSB0aGUgZGVmYXVsdCBwb3J0IG51bWJlclxyXG4gIC8vIGZvciBhIGdpdmVuIHByb3RvY29sLiBBcyB0aGUgaG9zdCBhbHNvIGNvbnRhaW5zIHRoZSBwb3J0IG51bWJlciB3ZSdyZSBnb2luZ1xyXG4gIC8vIG92ZXJyaWRlIGl0IHdpdGggdGhlIGhvc3RuYW1lIHdoaWNoIGNvbnRhaW5zIG5vIHBvcnQgbnVtYmVyLlxyXG4gIC8vXHJcbiAgaWYgKCFyZXF1aXJlZCh1cmwucG9ydCwgdXJsLnByb3RvY29sKSkge1xyXG4gICAgdXJsLmhvc3QgPSB1cmwuaG9zdG5hbWU7XHJcbiAgICB1cmwucG9ydCA9ICcnO1xyXG4gIH1cclxuXHJcbiAgLy9cclxuICAvLyBQYXJzZSBkb3duIHRoZSBgYXV0aGAgZm9yIHRoZSB1c2VybmFtZSBhbmQgcGFzc3dvcmQuXHJcbiAgLy9cclxuICB1cmwudXNlcm5hbWUgPSB1cmwucGFzc3dvcmQgPSAnJztcclxuICBpZiAodXJsLmF1dGgpIHtcclxuICAgIGluc3RydWN0aW9uID0gdXJsLmF1dGguc3BsaXQoJzonKTtcclxuICAgIHVybC51c2VybmFtZSA9IGluc3RydWN0aW9uWzBdIHx8ICcnO1xyXG4gICAgdXJsLnBhc3N3b3JkID0gaW5zdHJ1Y3Rpb25bMV0gfHwgJyc7XHJcbiAgfVxyXG5cclxuICB1cmwub3JpZ2luID0gdXJsLnByb3RvY29sICYmIHVybC5ob3N0ICYmIHVybC5wcm90b2NvbCAhPT0gJ2ZpbGU6J1xyXG4gICAgPyB1cmwucHJvdG9jb2wgKycvLycrIHVybC5ob3N0XHJcbiAgICA6ICdudWxsJztcclxuXHJcbiAgLy9cclxuICAvLyBUaGUgaHJlZiBpcyBqdXN0IHRoZSBjb21waWxlZCByZXN1bHQuXHJcbiAgLy9cclxuICB1cmwuaHJlZiA9IHVybC50b1N0cmluZygpO1xyXG59XHJcblxyXG4vKipcclxuICogVGhpcyBpcyBjb252ZW5pZW5jZSBtZXRob2QgZm9yIGNoYW5naW5nIHByb3BlcnRpZXMgaW4gdGhlIFVSTCBpbnN0YW5jZSB0b1xyXG4gKiBpbnN1cmUgdGhhdCB0aGV5IGFsbCBwcm9wYWdhdGUgY29ycmVjdGx5LlxyXG4gKlxyXG4gKiBAcGFyYW0ge1N0cmluZ30gcGFydCAgICAgICAgICBQcm9wZXJ0eSB3ZSBuZWVkIHRvIGFkanVzdC5cclxuICogQHBhcmFtIHtNaXhlZH0gdmFsdWUgICAgICAgICAgVGhlIG5ld2x5IGFzc2lnbmVkIHZhbHVlLlxyXG4gKiBAcGFyYW0ge0Jvb2xlYW58RnVuY3Rpb259IGZuICBXaGVuIHNldHRpbmcgdGhlIHF1ZXJ5LCBpdCB3aWxsIGJlIHRoZSBmdW5jdGlvblxyXG4gKiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICB1c2VkIHRvIHBhcnNlIHRoZSBxdWVyeS5cclxuICogICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgV2hlbiBzZXR0aW5nIHRoZSBwcm90b2NvbCwgZG91YmxlIHNsYXNoIHdpbGwgYmVcclxuICogICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgcmVtb3ZlZCBmcm9tIHRoZSBmaW5hbCB1cmwgaWYgaXQgaXMgdHJ1ZS5cclxuICogQHJldHVybnMge1VSTH1cclxuICogQGFwaSBwdWJsaWNcclxuICovXHJcbmZ1bmN0aW9uIHNldChwYXJ0LCB2YWx1ZSwgZm4pIHtcclxuICB2YXIgdXJsID0gdGhpcztcclxuXHJcbiAgc3dpdGNoIChwYXJ0KSB7XHJcbiAgICBjYXNlICdxdWVyeSc6XHJcbiAgICAgIGlmICgnc3RyaW5nJyA9PT0gdHlwZW9mIHZhbHVlICYmIHZhbHVlLmxlbmd0aCkge1xyXG4gICAgICAgIHZhbHVlID0gKGZuIHx8IHFzLnBhcnNlKSh2YWx1ZSk7XHJcbiAgICAgIH1cclxuXHJcbiAgICAgIHVybFtwYXJ0XSA9IHZhbHVlO1xyXG4gICAgICBicmVhaztcclxuXHJcbiAgICBjYXNlICdwb3J0JzpcclxuICAgICAgdXJsW3BhcnRdID0gdmFsdWU7XHJcblxyXG4gICAgICBpZiAoIXJlcXVpcmVkKHZhbHVlLCB1cmwucHJvdG9jb2wpKSB7XHJcbiAgICAgICAgdXJsLmhvc3QgPSB1cmwuaG9zdG5hbWU7XHJcbiAgICAgICAgdXJsW3BhcnRdID0gJyc7XHJcbiAgICAgIH0gZWxzZSBpZiAodmFsdWUpIHtcclxuICAgICAgICB1cmwuaG9zdCA9IHVybC5ob3N0bmFtZSArJzonKyB2YWx1ZTtcclxuICAgICAgfVxyXG5cclxuICAgICAgYnJlYWs7XHJcblxyXG4gICAgY2FzZSAnaG9zdG5hbWUnOlxyXG4gICAgICB1cmxbcGFydF0gPSB2YWx1ZTtcclxuXHJcbiAgICAgIGlmICh1cmwucG9ydCkgdmFsdWUgKz0gJzonKyB1cmwucG9ydDtcclxuICAgICAgdXJsLmhvc3QgPSB2YWx1ZTtcclxuICAgICAgYnJlYWs7XHJcblxyXG4gICAgY2FzZSAnaG9zdCc6XHJcbiAgICAgIHVybFtwYXJ0XSA9IHZhbHVlO1xyXG5cclxuICAgICAgaWYgKC86XFxkKyQvLnRlc3QodmFsdWUpKSB7XHJcbiAgICAgICAgdmFsdWUgPSB2YWx1ZS5zcGxpdCgnOicpO1xyXG4gICAgICAgIHVybC5wb3J0ID0gdmFsdWUucG9wKCk7XHJcbiAgICAgICAgdXJsLmhvc3RuYW1lID0gdmFsdWUuam9pbignOicpO1xyXG4gICAgICB9IGVsc2Uge1xyXG4gICAgICAgIHVybC5ob3N0bmFtZSA9IHZhbHVlO1xyXG4gICAgICAgIHVybC5wb3J0ID0gJyc7XHJcbiAgICAgIH1cclxuXHJcbiAgICAgIGJyZWFrO1xyXG5cclxuICAgIGNhc2UgJ3Byb3RvY29sJzpcclxuICAgICAgdXJsLnByb3RvY29sID0gdmFsdWUudG9Mb3dlckNhc2UoKTtcclxuICAgICAgdXJsLnNsYXNoZXMgPSAhZm47XHJcbiAgICAgIGJyZWFrO1xyXG5cclxuICAgIGNhc2UgJ3BhdGhuYW1lJzpcclxuICAgICAgdXJsLnBhdGhuYW1lID0gdmFsdWUubGVuZ3RoICYmIHZhbHVlLmNoYXJBdCgwKSAhPT0gJy8nID8gJy8nICsgdmFsdWUgOiB2YWx1ZTtcclxuXHJcbiAgICAgIGJyZWFrO1xyXG5cclxuICAgIGRlZmF1bHQ6XHJcbiAgICAgIHVybFtwYXJ0XSA9IHZhbHVlO1xyXG4gIH1cclxuXHJcbiAgZm9yICh2YXIgaSA9IDA7IGkgPCBydWxlcy5sZW5ndGg7IGkrKykge1xyXG4gICAgdmFyIGlucyA9IHJ1bGVzW2ldO1xyXG5cclxuICAgIGlmIChpbnNbNF0pIHVybFtpbnNbMV1dID0gdXJsW2luc1sxXV0udG9Mb3dlckNhc2UoKTtcclxuICB9XHJcblxyXG4gIHVybC5vcmlnaW4gPSB1cmwucHJvdG9jb2wgJiYgdXJsLmhvc3QgJiYgdXJsLnByb3RvY29sICE9PSAnZmlsZTonXHJcbiAgICA/IHVybC5wcm90b2NvbCArJy8vJysgdXJsLmhvc3RcclxuICAgIDogJ251bGwnO1xyXG5cclxuICB1cmwuaHJlZiA9IHVybC50b1N0cmluZygpO1xyXG5cclxuICByZXR1cm4gdXJsO1xyXG59O1xyXG5cclxuLyoqXHJcbiAqIFRyYW5zZm9ybSB0aGUgcHJvcGVydGllcyBiYWNrIGluIHRvIGEgdmFsaWQgYW5kIGZ1bGwgVVJMIHN0cmluZy5cclxuICpcclxuICogQHBhcmFtIHtGdW5jdGlvbn0gc3RyaW5naWZ5IE9wdGlvbmFsIHF1ZXJ5IHN0cmluZ2lmeSBmdW5jdGlvbi5cclxuICogQHJldHVybnMge1N0cmluZ31cclxuICogQGFwaSBwdWJsaWNcclxuICovXHJcbmZ1bmN0aW9uIHRvU3RyaW5nKHN0cmluZ2lmeSkge1xyXG4gIGlmICghc3RyaW5naWZ5IHx8ICdmdW5jdGlvbicgIT09IHR5cGVvZiBzdHJpbmdpZnkpIHN0cmluZ2lmeSA9IHFzLnN0cmluZ2lmeTtcclxuXHJcbiAgdmFyIHF1ZXJ5XHJcbiAgICAsIHVybCA9IHRoaXNcclxuICAgICwgcHJvdG9jb2wgPSB1cmwucHJvdG9jb2w7XHJcblxyXG4gIGlmIChwcm90b2NvbCAmJiBwcm90b2NvbC5jaGFyQXQocHJvdG9jb2wubGVuZ3RoIC0gMSkgIT09ICc6JykgcHJvdG9jb2wgKz0gJzonO1xyXG5cclxuICB2YXIgcmVzdWx0ID0gcHJvdG9jb2wgKyAodXJsLnNsYXNoZXMgPyAnLy8nIDogJycpO1xyXG5cclxuICBpZiAodXJsLnVzZXJuYW1lKSB7XHJcbiAgICByZXN1bHQgKz0gdXJsLnVzZXJuYW1lO1xyXG4gICAgaWYgKHVybC5wYXNzd29yZCkgcmVzdWx0ICs9ICc6JysgdXJsLnBhc3N3b3JkO1xyXG4gICAgcmVzdWx0ICs9ICdAJztcclxuICB9XHJcblxyXG4gIHJlc3VsdCArPSB1cmwuaG9zdCArIHVybC5wYXRobmFtZTtcclxuXHJcbiAgcXVlcnkgPSAnb2JqZWN0JyA9PT0gdHlwZW9mIHVybC5xdWVyeSA/IHN0cmluZ2lmeSh1cmwucXVlcnkpIDogdXJsLnF1ZXJ5O1xyXG4gIGlmIChxdWVyeSkgcmVzdWx0ICs9ICc/JyAhPT0gcXVlcnkuY2hhckF0KDApID8gJz8nKyBxdWVyeSA6IHF1ZXJ5O1xyXG5cclxuICBpZiAodXJsLmhhc2gpIHJlc3VsdCArPSB1cmwuaGFzaDtcclxuXHJcbiAgcmV0dXJuIHJlc3VsdDtcclxufVxyXG5cclxuVVJMLnByb3RvdHlwZSA9IHsgc2V0OiBzZXQsIHRvU3RyaW5nOiB0b1N0cmluZyB9O1xyXG5cclxuLy9cclxuLy8gRXhwb3NlIHRoZSBVUkwgcGFyc2VyIGFuZCBzb21lIGFkZGl0aW9uYWwgcHJvcGVydGllcyB0aGF0IG1pZ2h0IGJlIHVzZWZ1bCBmb3JcclxuLy8gb3RoZXJzIG9yIHRlc3RpbmcuXHJcbi8vXHJcblVSTC5leHRyYWN0UHJvdG9jb2wgPSBleHRyYWN0UHJvdG9jb2w7XHJcblVSTC5sb2NhdGlvbiA9IGxvbGNhdGlvbjtcclxuVVJMLnFzID0gcXM7XHJcblxyXG5tb2R1bGUuZXhwb3J0cyA9IFVSTDtcclxuXHJcbn0se1wiLi9sb2xjYXRpb25cIjo2MixcInF1ZXJ5c3RyaW5naWZ5XCI6NTksXCJyZXF1aXJlcy1wb3J0XCI6NjB9XSw2MjpbZnVuY3Rpb24ocmVxdWlyZSxtb2R1bGUsZXhwb3J0cyl7XHJcbihmdW5jdGlvbiAoZ2xvYmFsKXtcclxuJ3VzZSBzdHJpY3QnO1xyXG5cclxudmFyIHNsYXNoZXMgPSAvXltBLVphLXpdW0EtWmEtejAtOSstLl0qOlxcL1xcLy87XHJcblxyXG4vKipcclxuICogVGhlc2UgcHJvcGVydGllcyBzaG91bGQgbm90IGJlIGNvcGllZCBvciBpbmhlcml0ZWQgZnJvbS4gVGhpcyBpcyBvbmx5IG5lZWRlZFxyXG4gKiBmb3IgYWxsIG5vbiBibG9iIFVSTCdzIGFzIGEgYmxvYiBVUkwgZG9lcyBub3QgaW5jbHVkZSBhIGhhc2gsIG9ubHkgdGhlXHJcbiAqIG9yaWdpbi5cclxuICpcclxuICogQHR5cGUge09iamVjdH1cclxuICogQHByaXZhdGVcclxuICovXHJcbnZhciBpZ25vcmUgPSB7IGhhc2g6IDEsIHF1ZXJ5OiAxIH1cclxuICAsIFVSTDtcclxuXHJcbi8qKlxyXG4gKiBUaGUgbG9jYXRpb24gb2JqZWN0IGRpZmZlcnMgd2hlbiB5b3VyIGNvZGUgaXMgbG9hZGVkIHRocm91Z2ggYSBub3JtYWwgcGFnZSxcclxuICogV29ya2VyIG9yIHRocm91Z2ggYSB3b3JrZXIgdXNpbmcgYSBibG9iLiBBbmQgd2l0aCB0aGUgYmxvYmJsZSBiZWdpbnMgdGhlXHJcbiAqIHRyb3VibGUgYXMgdGhlIGxvY2F0aW9uIG9iamVjdCB3aWxsIGNvbnRhaW4gdGhlIFVSTCBvZiB0aGUgYmxvYiwgbm90IHRoZVxyXG4gKiBsb2NhdGlvbiBvZiB0aGUgcGFnZSB3aGVyZSBvdXIgY29kZSBpcyBsb2FkZWQgaW4uIFRoZSBhY3R1YWwgb3JpZ2luIGlzXHJcbiAqIGVuY29kZWQgaW4gdGhlIGBwYXRobmFtZWAgc28gd2UgY2FuIHRoYW5rZnVsbHkgZ2VuZXJhdGUgYSBnb29kIFwiZGVmYXVsdFwiXHJcbiAqIGxvY2F0aW9uIGZyb20gaXQgc28gd2UgY2FuIGdlbmVyYXRlIHByb3BlciByZWxhdGl2ZSBVUkwncyBhZ2Fpbi5cclxuICpcclxuICogQHBhcmFtIHtPYmplY3R8U3RyaW5nfSBsb2MgT3B0aW9uYWwgZGVmYXVsdCBsb2NhdGlvbiBvYmplY3QuXHJcbiAqIEByZXR1cm5zIHtPYmplY3R9IGxvbGNhdGlvbiBvYmplY3QuXHJcbiAqIEBhcGkgcHVibGljXHJcbiAqL1xyXG5tb2R1bGUuZXhwb3J0cyA9IGZ1bmN0aW9uIGxvbGNhdGlvbihsb2MpIHtcclxuICBsb2MgPSBsb2MgfHwgZ2xvYmFsLmxvY2F0aW9uIHx8IHt9O1xyXG4gIFVSTCA9IFVSTCB8fCByZXF1aXJlKCcuLycpO1xyXG5cclxuICB2YXIgZmluYWxkZXN0aW5hdGlvbiA9IHt9XHJcbiAgICAsIHR5cGUgPSB0eXBlb2YgbG9jXHJcbiAgICAsIGtleTtcclxuXHJcbiAgaWYgKCdibG9iOicgPT09IGxvYy5wcm90b2NvbCkge1xyXG4gICAgZmluYWxkZXN0aW5hdGlvbiA9IG5ldyBVUkwodW5lc2NhcGUobG9jLnBhdGhuYW1lKSwge30pO1xyXG4gIH0gZWxzZSBpZiAoJ3N0cmluZycgPT09IHR5cGUpIHtcclxuICAgIGZpbmFsZGVzdGluYXRpb24gPSBuZXcgVVJMKGxvYywge30pO1xyXG4gICAgZm9yIChrZXkgaW4gaWdub3JlKSBkZWxldGUgZmluYWxkZXN0aW5hdGlvbltrZXldO1xyXG4gIH0gZWxzZSBpZiAoJ29iamVjdCcgPT09IHR5cGUpIHtcclxuICAgIGZvciAoa2V5IGluIGxvYykge1xyXG4gICAgICBpZiAoa2V5IGluIGlnbm9yZSkgY29udGludWU7XHJcbiAgICAgIGZpbmFsZGVzdGluYXRpb25ba2V5XSA9IGxvY1trZXldO1xyXG4gICAgfVxyXG5cclxuICAgIGlmIChmaW5hbGRlc3RpbmF0aW9uLnNsYXNoZXMgPT09IHVuZGVmaW5lZCkge1xyXG4gICAgICBmaW5hbGRlc3RpbmF0aW9uLnNsYXNoZXMgPSBzbGFzaGVzLnRlc3QobG9jLmhyZWYpO1xyXG4gICAgfVxyXG4gIH1cclxuXHJcbiAgcmV0dXJuIGZpbmFsZGVzdGluYXRpb247XHJcbn07XHJcblxyXG59KS5jYWxsKHRoaXMsdHlwZW9mIGdsb2JhbCAhPT0gXCJ1bmRlZmluZWRcIiA/IGdsb2JhbCA6IHR5cGVvZiBzZWxmICE9PSBcInVuZGVmaW5lZFwiID8gc2VsZiA6IHR5cGVvZiB3aW5kb3cgIT09IFwidW5kZWZpbmVkXCIgPyB3aW5kb3cgOiB7fSlcclxuXHJcbn0se1wiLi9cIjo2MX1dfSx7fSxbMV0pKDEpXHJcbn0pO1xyXG5cclxuXHJcbi8vIyBzb3VyY2VNYXBwaW5nVVJMPXNvY2tqcy5qcy5tYXBcblxuXG4vLy8vLy8vLy8vLy8vLy8vLy9cbi8vIFdFQlBBQ0sgRk9PVEVSXG4vLyAuL3Jlc291cmNlcy9zb2NranMuanNcbi8vIG1vZHVsZSBpZCA9IDEzXG4vLyBtb2R1bGUgY2h1bmtzID0gMCIsInZhciBnO1xyXG5cclxuLy8gVGhpcyB3b3JrcyBpbiBub24tc3RyaWN0IG1vZGVcclxuZyA9IChmdW5jdGlvbigpIHtcclxuXHRyZXR1cm4gdGhpcztcclxufSkoKTtcclxuXHJcbnRyeSB7XHJcblx0Ly8gVGhpcyB3b3JrcyBpZiBldmFsIGlzIGFsbG93ZWQgKHNlZSBDU1ApXHJcblx0ZyA9IGcgfHwgRnVuY3Rpb24oXCJyZXR1cm4gdGhpc1wiKSgpIHx8ICgxLGV2YWwpKFwidGhpc1wiKTtcclxufSBjYXRjaChlKSB7XHJcblx0Ly8gVGhpcyB3b3JrcyBpZiB0aGUgd2luZG93IHJlZmVyZW5jZSBpcyBhdmFpbGFibGVcclxuXHRpZih0eXBlb2Ygd2luZG93ID09PSBcIm9iamVjdFwiKVxyXG5cdFx0ZyA9IHdpbmRvdztcclxufVxyXG5cclxuLy8gZyBjYW4gc3RpbGwgYmUgdW5kZWZpbmVkLCBidXQgbm90aGluZyB0byBkbyBhYm91dCBpdC4uLlxyXG4vLyBXZSByZXR1cm4gdW5kZWZpbmVkLCBpbnN0ZWFkIG9mIG5vdGhpbmcgaGVyZSwgc28gaXQnc1xyXG4vLyBlYXNpZXIgdG8gaGFuZGxlIHRoaXMgY2FzZS4gaWYoIWdsb2JhbCkgeyAuLi59XHJcblxyXG5tb2R1bGUuZXhwb3J0cyA9IGc7XHJcblxuXG5cbi8vLy8vLy8vLy8vLy8vLy8vL1xuLy8gV0VCUEFDSyBGT09URVJcbi8vICh3ZWJwYWNrKS9idWlsZGluL2dsb2JhbC5qc1xuLy8gbW9kdWxlIGlkID0gMTRcbi8vIG1vZHVsZSBjaHVua3MgPSAwIiwiLy8gR2VuZXJhdGVkIGJ5IENvZmZlZVNjcmlwdCAxLjEyLjZcclxuXHJcbi8qXHJcbiAgIFN0b21wIE92ZXIgV2ViU29ja2V0IGh0dHA6Ly93d3cuam1lc25pbC5uZXQvc3RvbXAtd2Vic29ja2V0L2RvYy8gfCBBcGFjaGUgTGljZW5zZSBWMi4wXHJcbiAgIENvcHlyaWdodCAoQykgMjAxMC0yMDEzIFtKZWZmIE1lc25pbF0oaHR0cDovL2ptZXNuaWwubmV0LylcclxuICAgQ29weXJpZ2h0IChDKSAyMDEyIFtGdXNlU291cmNlLCBJbmMuXShodHRwOi8vZnVzZXNvdXJjZS5jb20pXHJcbiAgIENvcHlyaWdodCAoQykgMjAxNyBbRGVlcGFrIEt1bWFyXShodHRwczovL3d3dy5rcmVhdGlvLmNvbSlcclxuICovXHJcblxyXG4oZnVuY3Rpb24oKSB7XHJcbiAgdmFyIEJ5dGUsIENsaWVudCwgRnJhbWUsIFN0b21wLFxyXG4gICAgaGFzUHJvcCA9IHt9Lmhhc093blByb3BlcnR5LFxyXG4gICAgc2xpY2UgPSBbXS5zbGljZTtcclxuXHJcbiAgQnl0ZSA9IHtcclxuICAgIExGOiAnXFx4MEEnLFxyXG4gICAgTlVMTDogJ1xceDAwJ1xyXG4gIH07XHJcblxyXG4gIEZyYW1lID0gKGZ1bmN0aW9uKCkge1xyXG4gICAgdmFyIHVubWFyc2hhbGxTaW5nbGU7XHJcblxyXG4gICAgZnVuY3Rpb24gRnJhbWUoY29tbWFuZDEsIGhlYWRlcnMxLCBib2R5MSwgZXNjYXBlSGVhZGVyVmFsdWVzMSkge1xyXG4gICAgICB0aGlzLmNvbW1hbmQgPSBjb21tYW5kMTtcclxuICAgICAgdGhpcy5oZWFkZXJzID0gaGVhZGVyczEgIT0gbnVsbCA/IGhlYWRlcnMxIDoge307XHJcbiAgICAgIHRoaXMuYm9keSA9IGJvZHkxICE9IG51bGwgPyBib2R5MSA6ICcnO1xyXG4gICAgICB0aGlzLmVzY2FwZUhlYWRlclZhbHVlcyA9IGVzY2FwZUhlYWRlclZhbHVlczEgIT0gbnVsbCA/IGVzY2FwZUhlYWRlclZhbHVlczEgOiBmYWxzZTtcclxuICAgIH1cclxuXHJcbiAgICBGcmFtZS5wcm90b3R5cGUudG9TdHJpbmcgPSBmdW5jdGlvbigpIHtcclxuICAgICAgdmFyIGxpbmVzLCBuYW1lLCByZWYsIHNraXBDb250ZW50TGVuZ3RoLCB2YWx1ZTtcclxuICAgICAgbGluZXMgPSBbdGhpcy5jb21tYW5kXTtcclxuICAgICAgc2tpcENvbnRlbnRMZW5ndGggPSAodGhpcy5oZWFkZXJzWydjb250ZW50LWxlbmd0aCddID09PSBmYWxzZSkgPyB0cnVlIDogZmFsc2U7XHJcbiAgICAgIGlmIChza2lwQ29udGVudExlbmd0aCkge1xyXG4gICAgICAgIGRlbGV0ZSB0aGlzLmhlYWRlcnNbJ2NvbnRlbnQtbGVuZ3RoJ107XHJcbiAgICAgIH1cclxuICAgICAgcmVmID0gdGhpcy5oZWFkZXJzO1xyXG4gICAgICBmb3IgKG5hbWUgaW4gcmVmKSB7XHJcbiAgICAgICAgaWYgKCFoYXNQcm9wLmNhbGwocmVmLCBuYW1lKSkgY29udGludWU7XHJcbiAgICAgICAgdmFsdWUgPSByZWZbbmFtZV07XHJcbiAgICAgICAgaWYgKHRoaXMuZXNjYXBlSGVhZGVyVmFsdWVzICYmIHRoaXMuY29tbWFuZCAhPT0gJ0NPTk5FQ1QnICYmIHRoaXMuY29tbWFuZCAhPT0gJ0NPTk5FQ1RFRCcpIHtcclxuICAgICAgICAgIGxpbmVzLnB1c2gobmFtZSArIFwiOlwiICsgKEZyYW1lLmZyRXNjYXBlKHZhbHVlKSkpO1xyXG4gICAgICAgIH0gZWxzZSB7XHJcbiAgICAgICAgICBsaW5lcy5wdXNoKG5hbWUgKyBcIjpcIiArIHZhbHVlKTtcclxuICAgICAgICB9XHJcbiAgICAgIH1cclxuICAgICAgaWYgKHRoaXMuYm9keSAmJiAhc2tpcENvbnRlbnRMZW5ndGgpIHtcclxuICAgICAgICBsaW5lcy5wdXNoKFwiY29udGVudC1sZW5ndGg6XCIgKyAoRnJhbWUuc2l6ZU9mVVRGOCh0aGlzLmJvZHkpKSk7XHJcbiAgICAgIH1cclxuICAgICAgbGluZXMucHVzaChCeXRlLkxGICsgdGhpcy5ib2R5KTtcclxuICAgICAgcmV0dXJuIGxpbmVzLmpvaW4oQnl0ZS5MRik7XHJcbiAgICB9O1xyXG5cclxuICAgIEZyYW1lLnNpemVPZlVURjggPSBmdW5jdGlvbihzKSB7XHJcbiAgICAgIGlmIChzKSB7XHJcbiAgICAgICAgcmV0dXJuIGVuY29kZVVSSShzKS5tYXRjaCgvJS4ufC4vZykubGVuZ3RoO1xyXG4gICAgICB9IGVsc2Uge1xyXG4gICAgICAgIHJldHVybiAwO1xyXG4gICAgICB9XHJcbiAgICB9O1xyXG5cclxuICAgIHVubWFyc2hhbGxTaW5nbGUgPSBmdW5jdGlvbihkYXRhLCBlc2NhcGVIZWFkZXJWYWx1ZXMpIHtcclxuICAgICAgdmFyIGJvZHksIGNociwgY29tbWFuZCwgZGl2aWRlciwgaGVhZGVyTGluZXMsIGhlYWRlcnMsIGksIGlkeCwgaiwgaywgbGVuLCBsZW4xLCBsaW5lLCByZWYsIHJlZjEsIHJlZjIsIHN0YXJ0LCB0cmltO1xyXG4gICAgICBpZiAoZXNjYXBlSGVhZGVyVmFsdWVzID09IG51bGwpIHtcclxuICAgICAgICBlc2NhcGVIZWFkZXJWYWx1ZXMgPSBmYWxzZTtcclxuICAgICAgfVxyXG4gICAgICBkaXZpZGVyID0gZGF0YS5zZWFyY2goUmVnRXhwKFwiXCIgKyBCeXRlLkxGICsgQnl0ZS5MRikpO1xyXG4gICAgICBoZWFkZXJMaW5lcyA9IGRhdGEuc3Vic3RyaW5nKDAsIGRpdmlkZXIpLnNwbGl0KEJ5dGUuTEYpO1xyXG4gICAgICBjb21tYW5kID0gaGVhZGVyTGluZXMuc2hpZnQoKTtcclxuICAgICAgaGVhZGVycyA9IHt9O1xyXG4gICAgICB0cmltID0gZnVuY3Rpb24oc3RyKSB7XHJcbiAgICAgICAgcmV0dXJuIHN0ci5yZXBsYWNlKC9eXFxzK3xcXHMrJC9nLCAnJyk7XHJcbiAgICAgIH07XHJcbiAgICAgIHJlZiA9IGhlYWRlckxpbmVzLnJldmVyc2UoKTtcclxuICAgICAgZm9yIChqID0gMCwgbGVuMSA9IHJlZi5sZW5ndGg7IGogPCBsZW4xOyBqKyspIHtcclxuICAgICAgICBsaW5lID0gcmVmW2pdO1xyXG4gICAgICAgIGlkeCA9IGxpbmUuaW5kZXhPZignOicpO1xyXG4gICAgICAgIGlmIChlc2NhcGVIZWFkZXJWYWx1ZXMgJiYgY29tbWFuZCAhPT0gJ0NPTk5FQ1QnICYmIGNvbW1hbmQgIT09ICdDT05ORUNURUQnKSB7XHJcbiAgICAgICAgICBoZWFkZXJzW3RyaW0obGluZS5zdWJzdHJpbmcoMCwgaWR4KSldID0gRnJhbWUuZnJVbkVzY2FwZSh0cmltKGxpbmUuc3Vic3RyaW5nKGlkeCArIDEpKSk7XHJcbiAgICAgICAgfSBlbHNlIHtcclxuICAgICAgICAgIGhlYWRlcnNbdHJpbShsaW5lLnN1YnN0cmluZygwLCBpZHgpKV0gPSB0cmltKGxpbmUuc3Vic3RyaW5nKGlkeCArIDEpKTtcclxuICAgICAgICB9XHJcbiAgICAgIH1cclxuICAgICAgYm9keSA9ICcnO1xyXG4gICAgICBzdGFydCA9IGRpdmlkZXIgKyAyO1xyXG4gICAgICBpZiAoaGVhZGVyc1snY29udGVudC1sZW5ndGgnXSkge1xyXG4gICAgICAgIGxlbiA9IHBhcnNlSW50KGhlYWRlcnNbJ2NvbnRlbnQtbGVuZ3RoJ10pO1xyXG4gICAgICAgIGJvZHkgPSAoJycgKyBkYXRhKS5zdWJzdHJpbmcoc3RhcnQsIHN0YXJ0ICsgbGVuKTtcclxuICAgICAgfSBlbHNlIHtcclxuICAgICAgICBjaHIgPSBudWxsO1xyXG4gICAgICAgIGZvciAoaSA9IGsgPSByZWYxID0gc3RhcnQsIHJlZjIgPSBkYXRhLmxlbmd0aDsgcmVmMSA8PSByZWYyID8gayA8IHJlZjIgOiBrID4gcmVmMjsgaSA9IHJlZjEgPD0gcmVmMiA/ICsrayA6IC0taykge1xyXG4gICAgICAgICAgY2hyID0gZGF0YS5jaGFyQXQoaSk7XHJcbiAgICAgICAgICBpZiAoY2hyID09PSBCeXRlLk5VTEwpIHtcclxuICAgICAgICAgICAgYnJlYWs7XHJcbiAgICAgICAgICB9XHJcbiAgICAgICAgICBib2R5ICs9IGNocjtcclxuICAgICAgICB9XHJcbiAgICAgIH1cclxuICAgICAgcmV0dXJuIG5ldyBGcmFtZShjb21tYW5kLCBoZWFkZXJzLCBib2R5LCBlc2NhcGVIZWFkZXJWYWx1ZXMpO1xyXG4gICAgfTtcclxuXHJcbiAgICBGcmFtZS51bm1hcnNoYWxsID0gZnVuY3Rpb24oZGF0YXMsIGVzY2FwZUhlYWRlclZhbHVlcykge1xyXG4gICAgICB2YXIgZnJhbWUsIGZyYW1lcywgbGFzdF9mcmFtZSwgcjtcclxuICAgICAgaWYgKGVzY2FwZUhlYWRlclZhbHVlcyA9PSBudWxsKSB7XHJcbiAgICAgICAgZXNjYXBlSGVhZGVyVmFsdWVzID0gZmFsc2U7XHJcbiAgICAgIH1cclxuICAgICAgZnJhbWVzID0gZGF0YXMuc3BsaXQoUmVnRXhwKFwiXCIgKyBCeXRlLk5VTEwgKyBCeXRlLkxGICsgXCIqXCIpKTtcclxuICAgICAgciA9IHtcclxuICAgICAgICBmcmFtZXM6IFtdLFxyXG4gICAgICAgIHBhcnRpYWw6ICcnXHJcbiAgICAgIH07XHJcbiAgICAgIHIuZnJhbWVzID0gKGZ1bmN0aW9uKCkge1xyXG4gICAgICAgIHZhciBqLCBsZW4xLCByZWYsIHJlc3VsdHM7XHJcbiAgICAgICAgcmVmID0gZnJhbWVzLnNsaWNlKDAsIC0xKTtcclxuICAgICAgICByZXN1bHRzID0gW107XHJcbiAgICAgICAgZm9yIChqID0gMCwgbGVuMSA9IHJlZi5sZW5ndGg7IGogPCBsZW4xOyBqKyspIHtcclxuICAgICAgICAgIGZyYW1lID0gcmVmW2pdO1xyXG4gICAgICAgICAgcmVzdWx0cy5wdXNoKHVubWFyc2hhbGxTaW5nbGUoZnJhbWUsIGVzY2FwZUhlYWRlclZhbHVlcykpO1xyXG4gICAgICAgIH1cclxuICAgICAgICByZXR1cm4gcmVzdWx0cztcclxuICAgICAgfSkoKTtcclxuICAgICAgbGFzdF9mcmFtZSA9IGZyYW1lcy5zbGljZSgtMSlbMF07XHJcbiAgICAgIGlmIChsYXN0X2ZyYW1lID09PSBCeXRlLkxGIHx8IChsYXN0X2ZyYW1lLnNlYXJjaChSZWdFeHAoXCJcIiArIEJ5dGUuTlVMTCArIEJ5dGUuTEYgKyBcIiokXCIpKSkgIT09IC0xKSB7XHJcbiAgICAgICAgci5mcmFtZXMucHVzaCh1bm1hcnNoYWxsU2luZ2xlKGxhc3RfZnJhbWUsIGVzY2FwZUhlYWRlclZhbHVlcykpO1xyXG4gICAgICB9IGVsc2Uge1xyXG4gICAgICAgIHIucGFydGlhbCA9IGxhc3RfZnJhbWU7XHJcbiAgICAgIH1cclxuICAgICAgcmV0dXJuIHI7XHJcbiAgICB9O1xyXG5cclxuICAgIEZyYW1lLm1hcnNoYWxsID0gZnVuY3Rpb24oY29tbWFuZCwgaGVhZGVycywgYm9keSwgZXNjYXBlSGVhZGVyVmFsdWVzKSB7XHJcbiAgICAgIHZhciBmcmFtZTtcclxuICAgICAgZnJhbWUgPSBuZXcgRnJhbWUoY29tbWFuZCwgaGVhZGVycywgYm9keSwgZXNjYXBlSGVhZGVyVmFsdWVzKTtcclxuICAgICAgcmV0dXJuIGZyYW1lLnRvU3RyaW5nKCkgKyBCeXRlLk5VTEw7XHJcbiAgICB9O1xyXG5cclxuICAgIEZyYW1lLmZyRXNjYXBlID0gZnVuY3Rpb24oc3RyKSB7XHJcbiAgICAgIHJldHVybiAoXCJcIiArIHN0cikucmVwbGFjZSgvXFxcXC9nLCBcIlxcXFxcXFxcXCIpLnJlcGxhY2UoL1xcci9nLCBcIlxcXFxyXCIpLnJlcGxhY2UoL1xcbi9nLCBcIlxcXFxuXCIpLnJlcGxhY2UoLzovZywgXCJcXFxcY1wiKTtcclxuICAgIH07XHJcblxyXG4gICAgRnJhbWUuZnJVbkVzY2FwZSA9IGZ1bmN0aW9uKHN0cikge1xyXG4gICAgICByZXR1cm4gKFwiXCIgKyBzdHIpLnJlcGxhY2UoL1xcXFxyL2csIFwiXFxyXCIpLnJlcGxhY2UoL1xcXFxuL2csIFwiXFxuXCIpLnJlcGxhY2UoL1xcXFxjL2csIFwiOlwiKS5yZXBsYWNlKC9cXFxcXFxcXC9nLCBcIlxcXFxcIik7XHJcbiAgICB9O1xyXG5cclxuICAgIHJldHVybiBGcmFtZTtcclxuXHJcbiAgfSkoKTtcclxuXHJcbiAgQ2xpZW50ID0gKGZ1bmN0aW9uKCkge1xyXG4gICAgdmFyIG5vdztcclxuXHJcbiAgICBmdW5jdGlvbiBDbGllbnQod3NfZm4pIHtcclxuICAgICAgdGhpcy53c19mbiA9IGZ1bmN0aW9uKCkge1xyXG4gICAgICAgIHZhciB3cztcclxuICAgICAgICB3cyA9IHdzX2ZuKCk7XHJcbiAgICAgICAgd3MuYmluYXJ5VHlwZSA9IFwiYXJyYXlidWZmZXJcIjtcclxuICAgICAgICByZXR1cm4gd3M7XHJcbiAgICAgIH07XHJcbiAgICAgIHRoaXMucmVjb25uZWN0X2RlbGF5ID0gMDtcclxuICAgICAgdGhpcy5jb3VudGVyID0gMDtcclxuICAgICAgdGhpcy5jb25uZWN0ZWQgPSBmYWxzZTtcclxuICAgICAgdGhpcy5oZWFydGJlYXQgPSB7XHJcbiAgICAgICAgb3V0Z29pbmc6IDEwMDAwLFxyXG4gICAgICAgIGluY29taW5nOiAxMDAwMFxyXG4gICAgICB9O1xyXG4gICAgICB0aGlzLm1heFdlYlNvY2tldEZyYW1lU2l6ZSA9IDE2ICogMTAyNDtcclxuICAgICAgdGhpcy5zdWJzY3JpcHRpb25zID0ge307XHJcbiAgICAgIHRoaXMucGFydGlhbERhdGEgPSAnJztcclxuICAgIH1cclxuXHJcbiAgICBDbGllbnQucHJvdG90eXBlLmRlYnVnID0gZnVuY3Rpb24obWVzc2FnZSkge1xyXG4gICAgICB2YXIgcmVmO1xyXG4gICAgICByZXR1cm4gdHlwZW9mIHdpbmRvdyAhPT0gXCJ1bmRlZmluZWRcIiAmJiB3aW5kb3cgIT09IG51bGwgPyAocmVmID0gd2luZG93LmNvbnNvbGUpICE9IG51bGwgPyByZWYubG9nKG1lc3NhZ2UpIDogdm9pZCAwIDogdm9pZCAwO1xyXG4gICAgfTtcclxuXHJcbiAgICBub3cgPSBmdW5jdGlvbigpIHtcclxuICAgICAgaWYgKERhdGUubm93KSB7XHJcbiAgICAgICAgcmV0dXJuIERhdGUubm93KCk7XHJcbiAgICAgIH0gZWxzZSB7XHJcbiAgICAgICAgcmV0dXJuIG5ldyBEYXRlKCkudmFsdWVPZjtcclxuICAgICAgfVxyXG4gICAgfTtcclxuXHJcbiAgICBDbGllbnQucHJvdG90eXBlLl90cmFuc21pdCA9IGZ1bmN0aW9uKGNvbW1hbmQsIGhlYWRlcnMsIGJvZHkpIHtcclxuICAgICAgdmFyIG91dDtcclxuICAgICAgb3V0ID0gRnJhbWUubWFyc2hhbGwoY29tbWFuZCwgaGVhZGVycywgYm9keSwgdGhpcy5lc2NhcGVIZWFkZXJWYWx1ZXMpO1xyXG4gICAgICBpZiAodHlwZW9mIHRoaXMuZGVidWcgPT09IFwiZnVuY3Rpb25cIikge1xyXG4gICAgICAgIHRoaXMuZGVidWcoXCI+Pj4gXCIgKyBvdXQpO1xyXG4gICAgICB9XHJcbiAgICAgIHdoaWxlICh0cnVlKSB7XHJcbiAgICAgICAgaWYgKG91dC5sZW5ndGggPiB0aGlzLm1heFdlYlNvY2tldEZyYW1lU2l6ZSkge1xyXG4gICAgICAgICAgdGhpcy53cy5zZW5kKG91dC5zdWJzdHJpbmcoMCwgdGhpcy5tYXhXZWJTb2NrZXRGcmFtZVNpemUpKTtcclxuICAgICAgICAgIG91dCA9IG91dC5zdWJzdHJpbmcodGhpcy5tYXhXZWJTb2NrZXRGcmFtZVNpemUpO1xyXG4gICAgICAgICAgaWYgKHR5cGVvZiB0aGlzLmRlYnVnID09PSBcImZ1bmN0aW9uXCIpIHtcclxuICAgICAgICAgICAgdGhpcy5kZWJ1ZyhcInJlbWFpbmluZyA9IFwiICsgb3V0Lmxlbmd0aCk7XHJcbiAgICAgICAgICB9XHJcbiAgICAgICAgfSBlbHNlIHtcclxuICAgICAgICAgIHJldHVybiB0aGlzLndzLnNlbmQob3V0KTtcclxuICAgICAgICB9XHJcbiAgICAgIH1cclxuICAgIH07XHJcblxyXG4gICAgQ2xpZW50LnByb3RvdHlwZS5fc2V0dXBIZWFydGJlYXQgPSBmdW5jdGlvbihoZWFkZXJzKSB7XHJcbiAgICAgIHZhciByZWYsIHJlZjEsIHNlcnZlckluY29taW5nLCBzZXJ2ZXJPdXRnb2luZywgdHRsLCB2O1xyXG4gICAgICBpZiAoKHJlZiA9IGhlYWRlcnMudmVyc2lvbikgIT09IFN0b21wLlZFUlNJT05TLlYxXzEgJiYgcmVmICE9PSBTdG9tcC5WRVJTSU9OUy5WMV8yKSB7XHJcbiAgICAgICAgcmV0dXJuO1xyXG4gICAgICB9XHJcbiAgICAgIHJlZjEgPSAoZnVuY3Rpb24oKSB7XHJcbiAgICAgICAgdmFyIGosIGxlbjEsIHJlZjEsIHJlc3VsdHM7XHJcbiAgICAgICAgcmVmMSA9IGhlYWRlcnNbJ2hlYXJ0LWJlYXQnXS5zcGxpdChcIixcIik7XHJcbiAgICAgICAgcmVzdWx0cyA9IFtdO1xyXG4gICAgICAgIGZvciAoaiA9IDAsIGxlbjEgPSByZWYxLmxlbmd0aDsgaiA8IGxlbjE7IGorKykge1xyXG4gICAgICAgICAgdiA9IHJlZjFbal07XHJcbiAgICAgICAgICByZXN1bHRzLnB1c2gocGFyc2VJbnQodikpO1xyXG4gICAgICAgIH1cclxuICAgICAgICByZXR1cm4gcmVzdWx0cztcclxuICAgICAgfSkoKSwgc2VydmVyT3V0Z29pbmcgPSByZWYxWzBdLCBzZXJ2ZXJJbmNvbWluZyA9IHJlZjFbMV07XHJcbiAgICAgIGlmICghKHRoaXMuaGVhcnRiZWF0Lm91dGdvaW5nID09PSAwIHx8IHNlcnZlckluY29taW5nID09PSAwKSkge1xyXG4gICAgICAgIHR0bCA9IE1hdGgubWF4KHRoaXMuaGVhcnRiZWF0Lm91dGdvaW5nLCBzZXJ2ZXJJbmNvbWluZyk7XHJcbiAgICAgICAgaWYgKHR5cGVvZiB0aGlzLmRlYnVnID09PSBcImZ1bmN0aW9uXCIpIHtcclxuICAgICAgICAgIHRoaXMuZGVidWcoXCJzZW5kIFBJTkcgZXZlcnkgXCIgKyB0dGwgKyBcIm1zXCIpO1xyXG4gICAgICAgIH1cclxuICAgICAgICB0aGlzLnBpbmdlciA9IFN0b21wLnNldEludGVydmFsKHR0bCwgKGZ1bmN0aW9uKF90aGlzKSB7XHJcbiAgICAgICAgICByZXR1cm4gZnVuY3Rpb24oKSB7XHJcbiAgICAgICAgICAgIF90aGlzLndzLnNlbmQoQnl0ZS5MRik7XHJcbiAgICAgICAgICAgIHJldHVybiB0eXBlb2YgX3RoaXMuZGVidWcgPT09IFwiZnVuY3Rpb25cIiA/IF90aGlzLmRlYnVnKFwiPj4+IFBJTkdcIikgOiB2b2lkIDA7XHJcbiAgICAgICAgICB9O1xyXG4gICAgICAgIH0pKHRoaXMpKTtcclxuICAgICAgfVxyXG4gICAgICBpZiAoISh0aGlzLmhlYXJ0YmVhdC5pbmNvbWluZyA9PT0gMCB8fCBzZXJ2ZXJPdXRnb2luZyA9PT0gMCkpIHtcclxuICAgICAgICB0dGwgPSBNYXRoLm1heCh0aGlzLmhlYXJ0YmVhdC5pbmNvbWluZywgc2VydmVyT3V0Z29pbmcpO1xyXG4gICAgICAgIGlmICh0eXBlb2YgdGhpcy5kZWJ1ZyA9PT0gXCJmdW5jdGlvblwiKSB7XHJcbiAgICAgICAgICB0aGlzLmRlYnVnKFwiY2hlY2sgUE9ORyBldmVyeSBcIiArIHR0bCArIFwibXNcIik7XHJcbiAgICAgICAgfVxyXG4gICAgICAgIHJldHVybiB0aGlzLnBvbmdlciA9IFN0b21wLnNldEludGVydmFsKHR0bCwgKGZ1bmN0aW9uKF90aGlzKSB7XHJcbiAgICAgICAgICByZXR1cm4gZnVuY3Rpb24oKSB7XHJcbiAgICAgICAgICAgIHZhciBkZWx0YTtcclxuICAgICAgICAgICAgZGVsdGEgPSBub3coKSAtIF90aGlzLnNlcnZlckFjdGl2aXR5O1xyXG4gICAgICAgICAgICBpZiAoZGVsdGEgPiB0dGwgKiAyKSB7XHJcbiAgICAgICAgICAgICAgaWYgKHR5cGVvZiBfdGhpcy5kZWJ1ZyA9PT0gXCJmdW5jdGlvblwiKSB7XHJcbiAgICAgICAgICAgICAgICBfdGhpcy5kZWJ1ZyhcImRpZCBub3QgcmVjZWl2ZSBzZXJ2ZXIgYWN0aXZpdHkgZm9yIHRoZSBsYXN0IFwiICsgZGVsdGEgKyBcIm1zXCIpO1xyXG4gICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgICByZXR1cm4gX3RoaXMud3MuY2xvc2UoKTtcclxuICAgICAgICAgICAgfVxyXG4gICAgICAgICAgfTtcclxuICAgICAgICB9KSh0aGlzKSk7XHJcbiAgICAgIH1cclxuICAgIH07XHJcblxyXG4gICAgQ2xpZW50LnByb3RvdHlwZS5fcGFyc2VDb25uZWN0ID0gZnVuY3Rpb24oKSB7XHJcbiAgICAgIHZhciBhcmdzLCBjb25uZWN0Q2FsbGJhY2ssIGVycm9yQ2FsbGJhY2ssIGhlYWRlcnM7XHJcbiAgICAgIGFyZ3MgPSAxIDw9IGFyZ3VtZW50cy5sZW5ndGggPyBzbGljZS5jYWxsKGFyZ3VtZW50cywgMCkgOiBbXTtcclxuICAgICAgaGVhZGVycyA9IHt9O1xyXG4gICAgICBzd2l0Y2ggKGFyZ3MubGVuZ3RoKSB7XHJcbiAgICAgICAgY2FzZSAyOlxyXG4gICAgICAgICAgaGVhZGVycyA9IGFyZ3NbMF0sIGNvbm5lY3RDYWxsYmFjayA9IGFyZ3NbMV07XHJcbiAgICAgICAgICBicmVhaztcclxuICAgICAgICBjYXNlIDM6XHJcbiAgICAgICAgICBpZiAoYXJnc1sxXSBpbnN0YW5jZW9mIEZ1bmN0aW9uKSB7XHJcbiAgICAgICAgICAgIGhlYWRlcnMgPSBhcmdzWzBdLCBjb25uZWN0Q2FsbGJhY2sgPSBhcmdzWzFdLCBlcnJvckNhbGxiYWNrID0gYXJnc1syXTtcclxuICAgICAgICAgIH0gZWxzZSB7XHJcbiAgICAgICAgICAgIGhlYWRlcnMubG9naW4gPSBhcmdzWzBdLCBoZWFkZXJzLnBhc3Njb2RlID0gYXJnc1sxXSwgY29ubmVjdENhbGxiYWNrID0gYXJnc1syXTtcclxuICAgICAgICAgIH1cclxuICAgICAgICAgIGJyZWFrO1xyXG4gICAgICAgIGNhc2UgNDpcclxuICAgICAgICAgIGhlYWRlcnMubG9naW4gPSBhcmdzWzBdLCBoZWFkZXJzLnBhc3Njb2RlID0gYXJnc1sxXSwgY29ubmVjdENhbGxiYWNrID0gYXJnc1syXSwgZXJyb3JDYWxsYmFjayA9IGFyZ3NbM107XHJcbiAgICAgICAgICBicmVhaztcclxuICAgICAgICBkZWZhdWx0OlxyXG4gICAgICAgICAgaGVhZGVycy5sb2dpbiA9IGFyZ3NbMF0sIGhlYWRlcnMucGFzc2NvZGUgPSBhcmdzWzFdLCBjb25uZWN0Q2FsbGJhY2sgPSBhcmdzWzJdLCBlcnJvckNhbGxiYWNrID0gYXJnc1szXSwgaGVhZGVycy5ob3N0ID0gYXJnc1s0XTtcclxuICAgICAgfVxyXG4gICAgICByZXR1cm4gW2hlYWRlcnMsIGNvbm5lY3RDYWxsYmFjaywgZXJyb3JDYWxsYmFja107XHJcbiAgICB9O1xyXG5cclxuICAgIENsaWVudC5wcm90b3R5cGUuY29ubmVjdCA9IGZ1bmN0aW9uKCkge1xyXG4gICAgICB2YXIgYXJncywgb3V0O1xyXG4gICAgICBhcmdzID0gMSA8PSBhcmd1bWVudHMubGVuZ3RoID8gc2xpY2UuY2FsbChhcmd1bWVudHMsIDApIDogW107XHJcbiAgICAgIHRoaXMuZXNjYXBlSGVhZGVyVmFsdWVzID0gZmFsc2U7XHJcbiAgICAgIG91dCA9IHRoaXMuX3BhcnNlQ29ubmVjdC5hcHBseSh0aGlzLCBhcmdzKTtcclxuICAgICAgdGhpcy5oZWFkZXJzID0gb3V0WzBdLCB0aGlzLmNvbm5lY3RDYWxsYmFjayA9IG91dFsxXSwgdGhpcy5lcnJvckNhbGxiYWNrID0gb3V0WzJdO1xyXG4gICAgICByZXR1cm4gdGhpcy5fY29ubmVjdCgpO1xyXG4gICAgfTtcclxuXHJcbiAgICBDbGllbnQucHJvdG90eXBlLl9jb25uZWN0ID0gZnVuY3Rpb24oKSB7XHJcbiAgICAgIHZhciBlcnJvckNhbGxiYWNrLCBoZWFkZXJzO1xyXG4gICAgICBoZWFkZXJzID0gdGhpcy5oZWFkZXJzO1xyXG4gICAgICBlcnJvckNhbGxiYWNrID0gdGhpcy5lcnJvckNhbGxiYWNrO1xyXG4gICAgICBpZiAodHlwZW9mIHRoaXMuZGVidWcgPT09IFwiZnVuY3Rpb25cIikge1xyXG4gICAgICAgIHRoaXMuZGVidWcoXCJPcGVuaW5nIFdlYiBTb2NrZXQuLi5cIik7XHJcbiAgICAgIH1cclxuICAgICAgdGhpcy53cyA9IHRoaXMud3NfZm4oKTtcclxuICAgICAgdGhpcy53cy5vbm1lc3NhZ2UgPSAoZnVuY3Rpb24oX3RoaXMpIHtcclxuICAgICAgICByZXR1cm4gZnVuY3Rpb24oZXZ0KSB7XHJcbiAgICAgICAgICB2YXIgYXJyLCBjLCBjbGllbnQsIGRhdGEsIGZyYW1lLCBqLCBsZW4xLCBtZXNzYWdlSUQsIG9ucmVjZWl2ZSwgcmVmLCByZXN1bHRzLCBzdWJzY3JpcHRpb24sIHVubWFyc2hhbGxlZERhdGE7XHJcbiAgICAgICAgICBkYXRhID0gdHlwZW9mIEFycmF5QnVmZmVyICE9PSAndW5kZWZpbmVkJyAmJiBldnQuZGF0YSBpbnN0YW5jZW9mIEFycmF5QnVmZmVyID8gKGFyciA9IG5ldyBVaW50OEFycmF5KGV2dC5kYXRhKSwgdHlwZW9mIF90aGlzLmRlYnVnID09PSBcImZ1bmN0aW9uXCIgPyBfdGhpcy5kZWJ1ZyhcIi0tLSBnb3QgZGF0YSBsZW5ndGg6IFwiICsgYXJyLmxlbmd0aCkgOiB2b2lkIDAsICgoZnVuY3Rpb24oKSB7XHJcbiAgICAgICAgICAgIHZhciBqLCBsZW4xLCByZXN1bHRzO1xyXG4gICAgICAgICAgICByZXN1bHRzID0gW107XHJcbiAgICAgICAgICAgIGZvciAoaiA9IDAsIGxlbjEgPSBhcnIubGVuZ3RoOyBqIDwgbGVuMTsgaisrKSB7XHJcbiAgICAgICAgICAgICAgYyA9IGFycltqXTtcclxuICAgICAgICAgICAgICByZXN1bHRzLnB1c2goU3RyaW5nLmZyb21DaGFyQ29kZShjKSk7XHJcbiAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgcmV0dXJuIHJlc3VsdHM7XHJcbiAgICAgICAgICB9KSgpKS5qb2luKCcnKSkgOiBldnQuZGF0YTtcclxuICAgICAgICAgIF90aGlzLnNlcnZlckFjdGl2aXR5ID0gbm93KCk7XHJcbiAgICAgICAgICBpZiAoZGF0YSA9PT0gQnl0ZS5MRikge1xyXG4gICAgICAgICAgICBpZiAodHlwZW9mIF90aGlzLmRlYnVnID09PSBcImZ1bmN0aW9uXCIpIHtcclxuICAgICAgICAgICAgICBfdGhpcy5kZWJ1ZyhcIjw8PCBQT05HXCIpO1xyXG4gICAgICAgICAgICB9XHJcbiAgICAgICAgICAgIHJldHVybjtcclxuICAgICAgICAgIH1cclxuICAgICAgICAgIGlmICh0eXBlb2YgX3RoaXMuZGVidWcgPT09IFwiZnVuY3Rpb25cIikge1xyXG4gICAgICAgICAgICBfdGhpcy5kZWJ1ZyhcIjw8PCBcIiArIGRhdGEpO1xyXG4gICAgICAgICAgfVxyXG4gICAgICAgICAgdW5tYXJzaGFsbGVkRGF0YSA9IEZyYW1lLnVubWFyc2hhbGwoX3RoaXMucGFydGlhbERhdGEgKyBkYXRhLCBfdGhpcy5lc2NhcGVIZWFkZXJWYWx1ZXMpO1xyXG4gICAgICAgICAgX3RoaXMucGFydGlhbERhdGEgPSB1bm1hcnNoYWxsZWREYXRhLnBhcnRpYWw7XHJcbiAgICAgICAgICByZWYgPSB1bm1hcnNoYWxsZWREYXRhLmZyYW1lcztcclxuICAgICAgICAgIHJlc3VsdHMgPSBbXTtcclxuICAgICAgICAgIGZvciAoaiA9IDAsIGxlbjEgPSByZWYubGVuZ3RoOyBqIDwgbGVuMTsgaisrKSB7XHJcbiAgICAgICAgICAgIGZyYW1lID0gcmVmW2pdO1xyXG4gICAgICAgICAgICBzd2l0Y2ggKGZyYW1lLmNvbW1hbmQpIHtcclxuICAgICAgICAgICAgICBjYXNlIFwiQ09OTkVDVEVEXCI6XHJcbiAgICAgICAgICAgICAgICBpZiAodHlwZW9mIF90aGlzLmRlYnVnID09PSBcImZ1bmN0aW9uXCIpIHtcclxuICAgICAgICAgICAgICAgICAgX3RoaXMuZGVidWcoXCJjb25uZWN0ZWQgdG8gc2VydmVyIFwiICsgZnJhbWUuaGVhZGVycy5zZXJ2ZXIpO1xyXG4gICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICAgICAgX3RoaXMuY29ubmVjdGVkID0gdHJ1ZTtcclxuICAgICAgICAgICAgICAgIF90aGlzLnZlcnNpb24gPSBmcmFtZS5oZWFkZXJzLnZlcnNpb247XHJcbiAgICAgICAgICAgICAgICBpZiAoX3RoaXMudmVyc2lvbiA9PT0gU3RvbXAuVkVSU0lPTlMuVjFfMikge1xyXG4gICAgICAgICAgICAgICAgICBfdGhpcy5lc2NhcGVIZWFkZXJWYWx1ZXMgPSB0cnVlO1xyXG4gICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICAgICAgX3RoaXMuX3NldHVwSGVhcnRiZWF0KGZyYW1lLmhlYWRlcnMpO1xyXG4gICAgICAgICAgICAgICAgcmVzdWx0cy5wdXNoKHR5cGVvZiBfdGhpcy5jb25uZWN0Q2FsbGJhY2sgPT09IFwiZnVuY3Rpb25cIiA/IF90aGlzLmNvbm5lY3RDYWxsYmFjayhmcmFtZSkgOiB2b2lkIDApO1xyXG4gICAgICAgICAgICAgICAgYnJlYWs7XHJcbiAgICAgICAgICAgICAgY2FzZSBcIk1FU1NBR0VcIjpcclxuICAgICAgICAgICAgICAgIHN1YnNjcmlwdGlvbiA9IGZyYW1lLmhlYWRlcnMuc3Vic2NyaXB0aW9uO1xyXG4gICAgICAgICAgICAgICAgb25yZWNlaXZlID0gX3RoaXMuc3Vic2NyaXB0aW9uc1tzdWJzY3JpcHRpb25dIHx8IF90aGlzLm9ucmVjZWl2ZTtcclxuICAgICAgICAgICAgICAgIGlmIChvbnJlY2VpdmUpIHtcclxuICAgICAgICAgICAgICAgICAgY2xpZW50ID0gX3RoaXM7XHJcbiAgICAgICAgICAgICAgICAgIGlmIChfdGhpcy52ZXJzaW9uID09PSBTdG9tcC5WRVJTSU9OUy5WMV8yKSB7XHJcbiAgICAgICAgICAgICAgICAgICAgbWVzc2FnZUlEID0gZnJhbWUuaGVhZGVyc1tcImFja1wiXTtcclxuICAgICAgICAgICAgICAgICAgfSBlbHNlIHtcclxuICAgICAgICAgICAgICAgICAgICBtZXNzYWdlSUQgPSBmcmFtZS5oZWFkZXJzW1wibWVzc2FnZS1pZFwiXTtcclxuICAgICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICAgICAgICBmcmFtZS5hY2sgPSBmdW5jdGlvbihoZWFkZXJzKSB7XHJcbiAgICAgICAgICAgICAgICAgICAgaWYgKGhlYWRlcnMgPT0gbnVsbCkge1xyXG4gICAgICAgICAgICAgICAgICAgICAgaGVhZGVycyA9IHt9O1xyXG4gICAgICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgICAgICAgICByZXR1cm4gY2xpZW50LmFjayhtZXNzYWdlSUQsIHN1YnNjcmlwdGlvbiwgaGVhZGVycyk7XHJcbiAgICAgICAgICAgICAgICAgIH07XHJcbiAgICAgICAgICAgICAgICAgIGZyYW1lLm5hY2sgPSBmdW5jdGlvbihoZWFkZXJzKSB7XHJcbiAgICAgICAgICAgICAgICAgICAgaWYgKGhlYWRlcnMgPT0gbnVsbCkge1xyXG4gICAgICAgICAgICAgICAgICAgICAgaGVhZGVycyA9IHt9O1xyXG4gICAgICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgICAgICAgICByZXR1cm4gY2xpZW50Lm5hY2sobWVzc2FnZUlELCBzdWJzY3JpcHRpb24sIGhlYWRlcnMpO1xyXG4gICAgICAgICAgICAgICAgICB9O1xyXG4gICAgICAgICAgICAgICAgICByZXN1bHRzLnB1c2gob25yZWNlaXZlKGZyYW1lKSk7XHJcbiAgICAgICAgICAgICAgICB9IGVsc2Uge1xyXG4gICAgICAgICAgICAgICAgICByZXN1bHRzLnB1c2godHlwZW9mIF90aGlzLmRlYnVnID09PSBcImZ1bmN0aW9uXCIgPyBfdGhpcy5kZWJ1ZyhcIlVuaGFuZGxlZCByZWNlaXZlZCBNRVNTQUdFOiBcIiArIGZyYW1lKSA6IHZvaWQgMCk7XHJcbiAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgICAgICBicmVhaztcclxuICAgICAgICAgICAgICBjYXNlIFwiUkVDRUlQVFwiOlxyXG4gICAgICAgICAgICAgICAgaWYgKGZyYW1lLmhlYWRlcnNbXCJyZWNlaXB0LWlkXCJdID09PSBfdGhpcy5jbG9zZVJlY2VpcHQpIHtcclxuICAgICAgICAgICAgICAgICAgX3RoaXMud3Mub25jbG9zZSA9IG51bGw7XHJcbiAgICAgICAgICAgICAgICAgIF90aGlzLndzLmNsb3NlKCk7XHJcbiAgICAgICAgICAgICAgICAgIHJlc3VsdHMucHVzaChfdGhpcy5fY2xlYW5VcCgpKTtcclxuICAgICAgICAgICAgICAgIH0gZWxzZSB7XHJcbiAgICAgICAgICAgICAgICAgIHJlc3VsdHMucHVzaCh0eXBlb2YgX3RoaXMub25yZWNlaXB0ID09PSBcImZ1bmN0aW9uXCIgPyBfdGhpcy5vbnJlY2VpcHQoZnJhbWUpIDogdm9pZCAwKTtcclxuICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgICAgIGJyZWFrO1xyXG4gICAgICAgICAgICAgIGNhc2UgXCJFUlJPUlwiOlxyXG4gICAgICAgICAgICAgICAgcmVzdWx0cy5wdXNoKHR5cGVvZiBlcnJvckNhbGxiYWNrID09PSBcImZ1bmN0aW9uXCIgPyBlcnJvckNhbGxiYWNrKGZyYW1lKSA6IHZvaWQgMCk7XHJcbiAgICAgICAgICAgICAgICBicmVhaztcclxuICAgICAgICAgICAgICBkZWZhdWx0OlxyXG4gICAgICAgICAgICAgICAgcmVzdWx0cy5wdXNoKHR5cGVvZiBfdGhpcy5kZWJ1ZyA9PT0gXCJmdW5jdGlvblwiID8gX3RoaXMuZGVidWcoXCJVbmhhbmRsZWQgZnJhbWU6IFwiICsgZnJhbWUpIDogdm9pZCAwKTtcclxuICAgICAgICAgICAgfVxyXG4gICAgICAgICAgfVxyXG4gICAgICAgICAgcmV0dXJuIHJlc3VsdHM7XHJcbiAgICAgICAgfTtcclxuICAgICAgfSkodGhpcyk7XHJcbiAgICAgIHRoaXMud3Mub25jbG9zZSA9IChmdW5jdGlvbihfdGhpcykge1xyXG4gICAgICAgIHJldHVybiBmdW5jdGlvbigpIHtcclxuICAgICAgICAgIHZhciBtc2c7XHJcbiAgICAgICAgICBtc2cgPSBcIldob29wcyEgTG9zdCBjb25uZWN0aW9uIHRvIFwiICsgX3RoaXMud3MudXJsO1xyXG4gICAgICAgICAgaWYgKHR5cGVvZiBfdGhpcy5kZWJ1ZyA9PT0gXCJmdW5jdGlvblwiKSB7XHJcbiAgICAgICAgICAgIF90aGlzLmRlYnVnKG1zZyk7XHJcbiAgICAgICAgICB9XHJcbiAgICAgICAgICBfdGhpcy5fY2xlYW5VcCgpO1xyXG4gICAgICAgICAgaWYgKHR5cGVvZiBlcnJvckNhbGxiYWNrID09PSBcImZ1bmN0aW9uXCIpIHtcclxuICAgICAgICAgICAgZXJyb3JDYWxsYmFjayhtc2cpO1xyXG4gICAgICAgICAgfVxyXG4gICAgICAgICAgcmV0dXJuIF90aGlzLl9zY2hlZHVsZV9yZWNvbm5lY3QoKTtcclxuICAgICAgICB9O1xyXG4gICAgICB9KSh0aGlzKTtcclxuICAgICAgcmV0dXJuIHRoaXMud3Mub25vcGVuID0gKGZ1bmN0aW9uKF90aGlzKSB7XHJcbiAgICAgICAgcmV0dXJuIGZ1bmN0aW9uKCkge1xyXG4gICAgICAgICAgaWYgKHR5cGVvZiBfdGhpcy5kZWJ1ZyA9PT0gXCJmdW5jdGlvblwiKSB7XHJcbiAgICAgICAgICAgIF90aGlzLmRlYnVnKCdXZWIgU29ja2V0IE9wZW5lZC4uLicpO1xyXG4gICAgICAgICAgfVxyXG4gICAgICAgICAgaGVhZGVyc1tcImFjY2VwdC12ZXJzaW9uXCJdID0gU3RvbXAuVkVSU0lPTlMuc3VwcG9ydGVkVmVyc2lvbnMoKTtcclxuICAgICAgICAgIGhlYWRlcnNbXCJoZWFydC1iZWF0XCJdID0gW190aGlzLmhlYXJ0YmVhdC5vdXRnb2luZywgX3RoaXMuaGVhcnRiZWF0LmluY29taW5nXS5qb2luKCcsJyk7XHJcbiAgICAgICAgICByZXR1cm4gX3RoaXMuX3RyYW5zbWl0KFwiQ09OTkVDVFwiLCBoZWFkZXJzKTtcclxuICAgICAgICB9O1xyXG4gICAgICB9KSh0aGlzKTtcclxuICAgIH07XHJcblxyXG4gICAgQ2xpZW50LnByb3RvdHlwZS5fc2NoZWR1bGVfcmVjb25uZWN0ID0gZnVuY3Rpb24oKSB7XHJcbiAgICAgIGlmICh0aGlzLnJlY29ubmVjdF9kZWxheSA+IDApIHtcclxuICAgICAgICBpZiAodHlwZW9mIHRoaXMuZGVidWcgPT09IFwiZnVuY3Rpb25cIikge1xyXG4gICAgICAgICAgdGhpcy5kZWJ1ZyhcIlNUT01QOiBzY2hlZHVsaW5nIHJlY29ubmVjdGlvbiBpbiBcIiArIHRoaXMucmVjb25uZWN0X2RlbGF5ICsgXCJtc1wiKTtcclxuICAgICAgICB9XHJcbiAgICAgICAgcmV0dXJuIHNldFRpbWVvdXQoKGZ1bmN0aW9uKF90aGlzKSB7XHJcbiAgICAgICAgICByZXR1cm4gZnVuY3Rpb24oKSB7XHJcbiAgICAgICAgICAgIGlmIChfdGhpcy5jb25uZWN0ZWQpIHtcclxuICAgICAgICAgICAgICByZXR1cm4gdHlwZW9mIF90aGlzLmRlYnVnID09PSBcImZ1bmN0aW9uXCIgPyBfdGhpcy5kZWJ1ZygnU1RPTVA6IGFscmVhZHkgY29ubmVjdGVkJykgOiB2b2lkIDA7XHJcbiAgICAgICAgICAgIH0gZWxzZSB7XHJcbiAgICAgICAgICAgICAgaWYgKHR5cGVvZiBfdGhpcy5kZWJ1ZyA9PT0gXCJmdW5jdGlvblwiKSB7XHJcbiAgICAgICAgICAgICAgICBfdGhpcy5kZWJ1ZygnU1RPTVA6IGF0dGVtcHRpbmcgdG8gcmVjb25uZWN0Jyk7XHJcbiAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICAgIHJldHVybiBfdGhpcy5fY29ubmVjdCgpO1xyXG4gICAgICAgICAgICB9XHJcbiAgICAgICAgICB9O1xyXG4gICAgICAgIH0pKHRoaXMpLCB0aGlzLnJlY29ubmVjdF9kZWxheSk7XHJcbiAgICAgIH1cclxuICAgIH07XHJcblxyXG4gICAgQ2xpZW50LnByb3RvdHlwZS5kaXNjb25uZWN0ID0gZnVuY3Rpb24oZGlzY29ubmVjdENhbGxiYWNrLCBoZWFkZXJzKSB7XHJcbiAgICAgIGlmIChoZWFkZXJzID09IG51bGwpIHtcclxuICAgICAgICBoZWFkZXJzID0ge307XHJcbiAgICAgIH1cclxuICAgICAgaWYgKCFoZWFkZXJzLnJlY2VpcHQpIHtcclxuICAgICAgICBoZWFkZXJzLnJlY2VpcHQgPSBcImNsb3NlLVwiICsgdGhpcy5jb3VudGVyKys7XHJcbiAgICAgIH1cclxuICAgICAgdGhpcy5jbG9zZVJlY2VpcHQgPSBoZWFkZXJzLnJlY2VpcHQ7XHJcbiAgICAgIHRoaXMuX3RyYW5zbWl0KFwiRElTQ09OTkVDVFwiLCBoZWFkZXJzKTtcclxuICAgICAgcmV0dXJuIHR5cGVvZiBkaXNjb25uZWN0Q2FsbGJhY2sgPT09IFwiZnVuY3Rpb25cIiA/IGRpc2Nvbm5lY3RDYWxsYmFjaygpIDogdm9pZCAwO1xyXG4gICAgfTtcclxuXHJcbiAgICBDbGllbnQucHJvdG90eXBlLl9jbGVhblVwID0gZnVuY3Rpb24oKSB7XHJcbiAgICAgIHRoaXMuY29ubmVjdGVkID0gZmFsc2U7XHJcbiAgICAgIHRoaXMuc3Vic2NyaXB0aW9ucyA9IHt9O1xyXG4gICAgICB0aGlzLnBhcnRpYWwgPSAnJztcclxuICAgICAgaWYgKHRoaXMucGluZ2VyKSB7XHJcbiAgICAgICAgU3RvbXAuY2xlYXJJbnRlcnZhbCh0aGlzLnBpbmdlcik7XHJcbiAgICAgIH1cclxuICAgICAgaWYgKHRoaXMucG9uZ2VyKSB7XHJcbiAgICAgICAgcmV0dXJuIFN0b21wLmNsZWFySW50ZXJ2YWwodGhpcy5wb25nZXIpO1xyXG4gICAgICB9XHJcbiAgICB9O1xyXG5cclxuICAgIENsaWVudC5wcm90b3R5cGUuc2VuZCA9IGZ1bmN0aW9uKGRlc3RpbmF0aW9uLCBoZWFkZXJzLCBib2R5KSB7XHJcbiAgICAgIGlmIChoZWFkZXJzID09IG51bGwpIHtcclxuICAgICAgICBoZWFkZXJzID0ge307XHJcbiAgICAgIH1cclxuICAgICAgaWYgKGJvZHkgPT0gbnVsbCkge1xyXG4gICAgICAgIGJvZHkgPSAnJztcclxuICAgICAgfVxyXG4gICAgICBoZWFkZXJzLmRlc3RpbmF0aW9uID0gZGVzdGluYXRpb247XHJcbiAgICAgIHJldHVybiB0aGlzLl90cmFuc21pdChcIlNFTkRcIiwgaGVhZGVycywgYm9keSk7XHJcbiAgICB9O1xyXG5cclxuICAgIENsaWVudC5wcm90b3R5cGUuc3Vic2NyaWJlID0gZnVuY3Rpb24oZGVzdGluYXRpb24sIGNhbGxiYWNrLCBoZWFkZXJzKSB7XHJcbiAgICAgIHZhciBjbGllbnQ7XHJcbiAgICAgIGlmIChoZWFkZXJzID09IG51bGwpIHtcclxuICAgICAgICBoZWFkZXJzID0ge307XHJcbiAgICAgIH1cclxuICAgICAgaWYgKCFoZWFkZXJzLmlkKSB7XHJcbiAgICAgICAgaGVhZGVycy5pZCA9IFwic3ViLVwiICsgdGhpcy5jb3VudGVyKys7XHJcbiAgICAgIH1cclxuICAgICAgaGVhZGVycy5kZXN0aW5hdGlvbiA9IGRlc3RpbmF0aW9uO1xyXG4gICAgICB0aGlzLnN1YnNjcmlwdGlvbnNbaGVhZGVycy5pZF0gPSBjYWxsYmFjaztcclxuICAgICAgdGhpcy5fdHJhbnNtaXQoXCJTVUJTQ1JJQkVcIiwgaGVhZGVycyk7XHJcbiAgICAgIGNsaWVudCA9IHRoaXM7XHJcbiAgICAgIHJldHVybiB7XHJcbiAgICAgICAgaWQ6IGhlYWRlcnMuaWQsXHJcbiAgICAgICAgdW5zdWJzY3JpYmU6IGZ1bmN0aW9uKGhkcnMpIHtcclxuICAgICAgICAgIHJldHVybiBjbGllbnQudW5zdWJzY3JpYmUoaGVhZGVycy5pZCwgaGRycyk7XHJcbiAgICAgICAgfVxyXG4gICAgICB9O1xyXG4gICAgfTtcclxuXHJcbiAgICBDbGllbnQucHJvdG90eXBlLnVuc3Vic2NyaWJlID0gZnVuY3Rpb24oaWQsIGhlYWRlcnMpIHtcclxuICAgICAgaWYgKGhlYWRlcnMgPT0gbnVsbCkge1xyXG4gICAgICAgIGhlYWRlcnMgPSB7fTtcclxuICAgICAgfVxyXG4gICAgICBkZWxldGUgdGhpcy5zdWJzY3JpcHRpb25zW2lkXTtcclxuICAgICAgaGVhZGVycy5pZCA9IGlkO1xyXG4gICAgICByZXR1cm4gdGhpcy5fdHJhbnNtaXQoXCJVTlNVQlNDUklCRVwiLCBoZWFkZXJzKTtcclxuICAgIH07XHJcblxyXG4gICAgQ2xpZW50LnByb3RvdHlwZS5iZWdpbiA9IGZ1bmN0aW9uKHRyYW5zYWN0aW9uX2lkKSB7XHJcbiAgICAgIHZhciBjbGllbnQsIHR4aWQ7XHJcbiAgICAgIHR4aWQgPSB0cmFuc2FjdGlvbl9pZCB8fCBcInR4LVwiICsgdGhpcy5jb3VudGVyKys7XHJcbiAgICAgIHRoaXMuX3RyYW5zbWl0KFwiQkVHSU5cIiwge1xyXG4gICAgICAgIHRyYW5zYWN0aW9uOiB0eGlkXHJcbiAgICAgIH0pO1xyXG4gICAgICBjbGllbnQgPSB0aGlzO1xyXG4gICAgICByZXR1cm4ge1xyXG4gICAgICAgIGlkOiB0eGlkLFxyXG4gICAgICAgIGNvbW1pdDogZnVuY3Rpb24oKSB7XHJcbiAgICAgICAgICByZXR1cm4gY2xpZW50LmNvbW1pdCh0eGlkKTtcclxuICAgICAgICB9LFxyXG4gICAgICAgIGFib3J0OiBmdW5jdGlvbigpIHtcclxuICAgICAgICAgIHJldHVybiBjbGllbnQuYWJvcnQodHhpZCk7XHJcbiAgICAgICAgfVxyXG4gICAgICB9O1xyXG4gICAgfTtcclxuXHJcbiAgICBDbGllbnQucHJvdG90eXBlLmNvbW1pdCA9IGZ1bmN0aW9uKHRyYW5zYWN0aW9uX2lkKSB7XHJcbiAgICAgIHJldHVybiB0aGlzLl90cmFuc21pdChcIkNPTU1JVFwiLCB7XHJcbiAgICAgICAgdHJhbnNhY3Rpb246IHRyYW5zYWN0aW9uX2lkXHJcbiAgICAgIH0pO1xyXG4gICAgfTtcclxuXHJcbiAgICBDbGllbnQucHJvdG90eXBlLmFib3J0ID0gZnVuY3Rpb24odHJhbnNhY3Rpb25faWQpIHtcclxuICAgICAgcmV0dXJuIHRoaXMuX3RyYW5zbWl0KFwiQUJPUlRcIiwge1xyXG4gICAgICAgIHRyYW5zYWN0aW9uOiB0cmFuc2FjdGlvbl9pZFxyXG4gICAgICB9KTtcclxuICAgIH07XHJcblxyXG4gICAgQ2xpZW50LnByb3RvdHlwZS5hY2sgPSBmdW5jdGlvbihtZXNzYWdlSUQsIHN1YnNjcmlwdGlvbiwgaGVhZGVycykge1xyXG4gICAgICBpZiAoaGVhZGVycyA9PSBudWxsKSB7XHJcbiAgICAgICAgaGVhZGVycyA9IHt9O1xyXG4gICAgICB9XHJcbiAgICAgIGlmICh0aGlzLnZlcnNpb24gPT09IFN0b21wLlZFUlNJT05TLlYxXzIpIHtcclxuICAgICAgICBoZWFkZXJzW1wiaWRcIl0gPSBtZXNzYWdlSUQ7XHJcbiAgICAgIH0gZWxzZSB7XHJcbiAgICAgICAgaGVhZGVyc1tcIm1lc3NhZ2UtaWRcIl0gPSBtZXNzYWdlSUQ7XHJcbiAgICAgIH1cclxuICAgICAgaGVhZGVycy5zdWJzY3JpcHRpb24gPSBzdWJzY3JpcHRpb247XHJcbiAgICAgIHJldHVybiB0aGlzLl90cmFuc21pdChcIkFDS1wiLCBoZWFkZXJzKTtcclxuICAgIH07XHJcblxyXG4gICAgQ2xpZW50LnByb3RvdHlwZS5uYWNrID0gZnVuY3Rpb24obWVzc2FnZUlELCBzdWJzY3JpcHRpb24sIGhlYWRlcnMpIHtcclxuICAgICAgaWYgKGhlYWRlcnMgPT0gbnVsbCkge1xyXG4gICAgICAgIGhlYWRlcnMgPSB7fTtcclxuICAgICAgfVxyXG4gICAgICBpZiAodGhpcy52ZXJzaW9uID09PSBTdG9tcC5WRVJTSU9OUy5WMV8yKSB7XHJcbiAgICAgICAgaGVhZGVyc1tcImlkXCJdID0gbWVzc2FnZUlEO1xyXG4gICAgICB9IGVsc2Uge1xyXG4gICAgICAgIGhlYWRlcnNbXCJtZXNzYWdlLWlkXCJdID0gbWVzc2FnZUlEO1xyXG4gICAgICB9XHJcbiAgICAgIGhlYWRlcnMuc3Vic2NyaXB0aW9uID0gc3Vic2NyaXB0aW9uO1xyXG4gICAgICByZXR1cm4gdGhpcy5fdHJhbnNtaXQoXCJOQUNLXCIsIGhlYWRlcnMpO1xyXG4gICAgfTtcclxuXHJcbiAgICByZXR1cm4gQ2xpZW50O1xyXG5cclxuICB9KSgpO1xyXG5cclxuICBTdG9tcCA9IHtcclxuICAgIFZFUlNJT05TOiB7XHJcbiAgICAgIFYxXzA6ICcxLjAnLFxyXG4gICAgICBWMV8xOiAnMS4xJyxcclxuICAgICAgVjFfMjogJzEuMicsXHJcbiAgICAgIHN1cHBvcnRlZFZlcnNpb25zOiBmdW5jdGlvbigpIHtcclxuICAgICAgICByZXR1cm4gJzEuMiwxLjEsMS4wJztcclxuICAgICAgfVxyXG4gICAgfSxcclxuICAgIGNsaWVudDogZnVuY3Rpb24odXJsLCBwcm90b2NvbHMpIHtcclxuICAgICAgdmFyIHdzX2ZuO1xyXG4gICAgICBpZiAocHJvdG9jb2xzID09IG51bGwpIHtcclxuICAgICAgICBwcm90b2NvbHMgPSBbJ3YxMC5zdG9tcCcsICd2MTEuc3RvbXAnLCAndjEyLnN0b21wJ107XHJcbiAgICAgIH1cclxuICAgICAgd3NfZm4gPSBmdW5jdGlvbigpIHtcclxuICAgICAgICB2YXIga2xhc3M7XHJcbiAgICAgICAga2xhc3MgPSBTdG9tcC5XZWJTb2NrZXRDbGFzcyB8fCBXZWJTb2NrZXQ7XHJcbiAgICAgICAgcmV0dXJuIG5ldyBrbGFzcyh1cmwsIHByb3RvY29scyk7XHJcbiAgICAgIH07XHJcbiAgICAgIHJldHVybiBuZXcgQ2xpZW50KHdzX2ZuKTtcclxuICAgIH0sXHJcbiAgICBvdmVyOiBmdW5jdGlvbih3cykge1xyXG4gICAgICB2YXIgd3NfZm47XHJcbiAgICAgIHdzX2ZuID0gdHlwZW9mIHdzID09PSBcImZ1bmN0aW9uXCIgPyB3cyA6IGZ1bmN0aW9uKCkge1xyXG4gICAgICAgIHJldHVybiB3cztcclxuICAgICAgfTtcclxuICAgICAgcmV0dXJuIG5ldyBDbGllbnQod3NfZm4pO1xyXG4gICAgfSxcclxuICAgIEZyYW1lOiBGcmFtZVxyXG4gIH07XHJcblxyXG4gIFN0b21wLnNldEludGVydmFsID0gZnVuY3Rpb24oaW50ZXJ2YWwsIGYpIHtcclxuICAgIHJldHVybiBzZXRJbnRlcnZhbChmLCBpbnRlcnZhbCk7XHJcbiAgfTtcclxuXHJcbiAgU3RvbXAuY2xlYXJJbnRlcnZhbCA9IGZ1bmN0aW9uKGlkKSB7XHJcbiAgICByZXR1cm4gY2xlYXJJbnRlcnZhbChpZCk7XHJcbiAgfTtcclxuXHJcbiAgaWYgKHR5cGVvZiBleHBvcnRzICE9PSBcInVuZGVmaW5lZFwiICYmIGV4cG9ydHMgIT09IG51bGwpIHtcclxuICAgIGV4cG9ydHMuU3RvbXAgPSBTdG9tcDtcclxuICB9XHJcblxyXG4gIGlmICh0eXBlb2Ygd2luZG93ICE9PSBcInVuZGVmaW5lZFwiICYmIHdpbmRvdyAhPT0gbnVsbCkge1xyXG4gICAgd2luZG93LlN0b21wID0gU3RvbXA7XHJcbiAgfSBlbHNlIGlmICghZXhwb3J0cykge1xyXG4gICAgc2VsZi5TdG9tcCA9IFN0b21wO1xyXG4gIH1cclxuXHJcbn0pLmNhbGwodGhpcyk7XG5cblxuLy8vLy8vLy8vLy8vLy8vLy8vXG4vLyBXRUJQQUNLIEZPT1RFUlxuLy8gLi9yZXNvdXJjZXMvc3RvbXAuanNcbi8vIG1vZHVsZSBpZCA9IDE1XG4vLyBtb2R1bGUgY2h1bmtzID0gMCIsIlxyXG4vKipcclxuICogU2ltdWxhdGVzIG5ldHdvcmsgY29tbXVuaWNhdGlvbiBpbiBhbiBvZmZsaW5lIHNpdHVhdGlvbiAtIGZvciBkZW1vIGlzc3VlcyBvbmx5XHJcbiAqL1xyXG5cclxuaW1wb3J0IHsgVmVydGV4LCBFZGdlLCB2ZXJ0aWNlc1RvRWRnZXMsIFRyaWFuZ2xlIH0gZnJvbSBcIi4uL2xvZ2ljL2dlb21ldHJpY19vYmplY3RzXCI7XHJcbmltcG9ydCB7IENhbnZhc0hhbmRsZXIgfSBmcm9tIFwiLi4vYm9hcmRfaGFuZGxpbmcvY2FudmFzX2hhbmRsZXJcIjtcclxuaW1wb3J0IHsgYXBwbHlTZXJ2ZXJVcGRhdGUsIGFwcGx5U2VydmVyUmVzZXQsIGFwcGx5U2VydmVyUmVnaXN0ZXIsIHJlc3VtZSwgc2V0QWNjb3VudCB9IGZyb20gXCIuLi9ydW4vbWFpblwiO1xyXG5pbXBvcnQgKiBhcyBTb2NrSlMgZnJvbSAnLi4vLi4vcmVzb3VyY2VzL3NvY2tqcy5qcyc7XHJcbmltcG9ydCAqIGFzIFN0b21wIGZyb20gJy4uLy4uL3Jlc291cmNlcy9zdG9tcC5qcyc7XHJcbmltcG9ydCB7IFVwZGF0ZUhhbmRsZXIsIFJlc2V0SGFuZGxlciwgSW5pdEhhbmRsZXIsIEludmFsaWRDb21tYW5kSGFuZGxlciwgQWNjb3VudEhhbmRsZXIgfSBmcm9tIFwiLi9jb21tYW5kX2hhbmRsZXJzXCI7XHJcbmltcG9ydCB7IFRyaWFuZ2xlQmVhbiwgUmVzZXRTdGF0ZSwgVmVydGV4QmVhbiB9IGZyb20gXCIuL25ldHdvcmtfYmVhbnNcIjtcclxuaW1wb3J0IHsgSW5nYW1lVHJpYW5nbGUgfSBmcm9tIFwiLi4vYm9hcmRfaGFuZGxpbmcvZ2FtZV9vYmplY3RzXCI7XHJcbmltcG9ydCB7IGdldENvc3RzRm9yLCBnZXRWYWx1ZU9mLCBnZXRUdXJuSW5jb21lRm9yLCBnZXREZWZlbnNlVXBncmFkZUNvc3RzIH0gZnJvbSBcIi4uL2xvZ2ljL2FjY291bnRhbnRcIjtcclxuaW1wb3J0IHsgTmV0d29yayB9IGZyb20gXCIuL25ldHdvcmtcIjtcclxuaW1wb3J0IHsgcmVtb3ZlRnJvbUxpc3QgfSBmcm9tIFwiLi4vdXRpbC9zaGFyZWRcIjtcclxuXHJcbi8qKiBIb2xkcyB0aGUgYXZhaWxhYmxlIHNlcnZlciBjb21tYW5kIGhhbmRsZXJzLiAqL1xyXG5jb25zdCBpbml0SGFuZGxlciA9IG5ldyBJbml0SGFuZGxlcigpO1xyXG5jb25zdCB1cGRhdGVIYW5kbGVyID0gbmV3IFVwZGF0ZUhhbmRsZXIoKTtcclxuY29uc3QgcmVzZXRIYW5kbGVyID0gbmV3IFJlc2V0SGFuZGxlcigpO1xyXG5jb25zdCBhY2NvdW50SGFuZGxlciA9IG5ldyBBY2NvdW50SGFuZGxlcigpO1xyXG5cbi8qKlxyXG4gKiBIYW5kbGVzIHJlcXVlc3RzIGZvciB0aGUgc2VydmVyIGFuZCBkZWxpdmVycyBzZXJ2ZXIgbWVzc2FnZXMgdmlhIGNhbGxiYWNrcy5cclxuICovXHJcbmV4cG9ydCBjbGFzcyBEdW1teUNvbm5lY3RvciBpbXBsZW1lbnRzIE5ldHdvcmsge1xyXG4gICAgLyoqVGhlIHtAY29kZSBJbmdhbWVUcmlhbmdsZXN9IG9mIHRoZSBwbGF5ZXIqL1xyXG4gICAgcHJpdmF0ZSB0cmlhbmdsZXM6IEluZ2FtZVRyaWFuZ2xlW10gPSBbXTtcclxuICAgIC8qKlRoZSBhY2NvdW50IGJhbGFuY2Ugb2YgdGhlIHBsYXllciovXHJcbiAgICBwcml2YXRlIG1vbmV5ID0gMDtcclxuICAgIC8qKlxyXG4gICAgICogSW5pdGlhbGlzZXMgdGhpcyBjb25uZWN0b3IgYW5kIGNhbGxzIHRoZSBnaXZlbiBtZXRob2Qgb25jZSB0aGUgY29ubmVjaW9uIGlzIGNvbXBsZXRlZC5cclxuICAgICAqIEBwYXJhbSByZWFkeUhhbmRsZXIgY2FsbGJhY2sgb25jZSB0aGUgY29ubmVjdGlvbiBpcyBlc3RhYmxpc2hlZFxyXG4gICAgICovXHJcbiAgICBwdWJsaWMgaW5pdCggcmVhZHlIYW5kbGVyOiAoIGJvb2xlYW4gKSA9PiB2b2lkICk6IHZvaWQge1xyXG4gICAgICAgIHNldEludGVydmFsKCgpID0+IHtcclxuICAgICAgICAgICAgZm9yICggbGV0IHRyaWFuZ2xlIG9mIHRoaXMudHJpYW5nbGVzICkgdGhpcy5tb25leSArPSBnZXRUdXJuSW5jb21lRm9yKCB0cmlhbmdsZSApO1xyXG4gICAgICAgICAgICBzZXRBY2NvdW50KCB0aGlzLm1vbmV5ICk7XHJcbiAgICAgICAgfSwgMTAwMCApO1xyXG4gICAgICAgIHJlYWR5SGFuZGxlci5jYWxsKCB0aGlzLCB0cnVlICk7XHJcbiAgICB9XHJcblxyXG5cclxuICAgIC8qKlxyXG4gICAgICogUmVnaXN0ZXJzIGEgbmV3IHBsYXllciBmb3IgdGhlIGdhbWUuXHJcbiAgICAgKi9cclxuICAgIHB1YmxpYyByZWdpc3RlclBsYXllcigpIHtcclxuICAgICAgICBsZXQgcGxheWVyID0gXCJbbm90IGNvbm5lY3RlZF1cIjtcclxuICAgICAgICBsZXQgdjEgPSBuZXcgVmVydGV4KCAwLCAwICk7IGxldCB2MiA9IG5ldyBWZXJ0ZXgoIDUwLCAwICk7IGxldCB2MyA9IG5ldyBWZXJ0ZXgoIDAsIDUwICk7XHJcbiAgICAgICAgbGV0IGVkZ2VzID0gdmVydGljZXNUb0VkZ2VzKCB2MSwgdjIsIHYzICk7XHJcbiAgICAgICAgbGV0IHRyaWFuZ2xlID0gbmV3IFRyaWFuZ2xlKCBlZGdlc1swXSApO1xyXG4gICAgICAgIHRoaXMudHJpYW5nbGVzLnB1c2goIEluZ2FtZVRyaWFuZ2xlLm9mKCB0cmlhbmdsZSwgcGxheWVyICkgKTtcclxuICAgICAgICBsZXQgdmIxID0gVmVydGV4QmVhbi5vZiggdjEsIHBsYXllciApOyBsZXQgdmIyID0gVmVydGV4QmVhbi5vZiggdjIsIHBsYXllciApOyBsZXQgdmIzID0gVmVydGV4QmVhbi5vZiggdjMsIHBsYXllciApO1xyXG4gICAgICAgIGxldCB0cmlhbmdsZUJlYW4gPSBuZXcgVHJpYW5nbGVCZWFuKCB2YjEsIHZiMiwgdmIzLCBnZXRDb3N0c0ZvciggdHJpYW5nbGUgKSwgMCwgcGxheWVyICk7XHJcbiAgICAgICAgaW5pdEhhbmRsZXIucHJvY2Vzcygge1xyXG4gICAgICAgICAgICBwbGF5ZXJJZDogcGxheWVyLFxyXG4gICAgICAgICAgICBpbml0aWFsU3RhdGU6IFJlc2V0U3RhdGUuZnJvbUpzb24oIHsgdHJpYW5nbGVzVG9DcmVhdGU6IFt0cmlhbmdsZUJlYW5dIH0gKVxyXG4gICAgICAgIH0gKTtcclxuICAgIH1cclxuXHJcbiAgICAvKipQcm9wb3NlcyBhIHtAY29kZSBUcmlhbmdsZX0gdG8gdGhlIHNlcnZlciB3aGljaCBtaWdodCBhcHBlYXIgaW4gdGhlIG5leHQge0Bjb2RlIFVwZGF0ZUNvbW1hbmR9IGZyb20gdGhlIHNlcnZlci5cclxuICAgICAqQHRvUmVxdWVzdCB0aGUge0Bjb2RlIFRyaWFuZ2xlfSB3aGljaCBzaGFsbCBiZSBwcm9wb3NlZFxyXG4gICAgICovXHJcbiAgICBwdWJsaWMgcHJvcG9zZVRyaWFuZ2xlKCB0b1JlcXVlc3Q6IEluZ2FtZVRyaWFuZ2xlICkge1xyXG4gICAgICAgIGlmICggIXRvUmVxdWVzdCApIHJlc3VtZSgpO1xyXG4gICAgICAgIGxldCB1cGdyYWRlID0gZmFsc2U7XHJcbiAgICAgICAgZm9yICggbGV0IHRyaWFuZ2xlIG9mIHRoaXMudHJpYW5nbGVzICkge1xyXG4gICAgICAgICAgICBpZiAoIHRyaWFuZ2xlLmVxdWFscyggdG9SZXF1ZXN0ICkgKSB7XHJcbiAgICAgICAgICAgICAgICBpZiAoICF0cmlhbmdsZS5iYXNlVmFsdWUgKSB0cmlhbmdsZS5iYXNlVmFsdWUgPSBnZXRWYWx1ZU9mKCB0cmlhbmdsZSApO1xyXG4gICAgICAgICAgICAgICAgdGhpcy5tb25leSAtPSBnZXREZWZlbnNlVXBncmFkZUNvc3RzKCB0cmlhbmdsZSApO1xyXG4gICAgICAgICAgICAgICAgdG9SZXF1ZXN0LmRlZmVuc2UgKz0gMTtcclxuICAgICAgICAgICAgICAgIHVwZ3JhZGUgPSB0cnVlO1xyXG4gICAgICAgICAgICAgICAgYnJlYWs7XHJcbiAgICAgICAgICAgIH1cclxuICAgICAgICB9XHJcbiAgICAgICAgaWYgKCAhdXBncmFkZSApIHtcclxuICAgICAgICAgICAgdG9SZXF1ZXN0LmJhc2VWYWx1ZSA9IGdldFZhbHVlT2YoIHRvUmVxdWVzdCApO1xyXG4gICAgICAgICAgICB0b1JlcXVlc3QuZGVmZW5zZSA9IDA7XHJcbiAgICAgICAgICAgIHRoaXMubW9uZXkgLT0gZ2V0Q29zdHNGb3IoIHRvUmVxdWVzdCApO1xyXG4gICAgICAgICAgICB0aGlzLnRyaWFuZ2xlcy5wdXNoKCB0b1JlcXVlc3QgKTtcclxuICAgICAgICB9XHJcbiAgICAgICAgdXBkYXRlSGFuZGxlci5wcm9jZXNzKCB7XHJcbiAgICAgICAgICAgIHRyaWFuZ2xlc1RvQWRkOiBbdG9SZXF1ZXN0XSxcclxuICAgICAgICAgICAgdHJpYW5nbGVzVG9SZW1vdmU6IFtdXHJcbiAgICAgICAgfSApO1xyXG4gICAgfVxyXG5cclxuICAgIC8qKlxyXG4gICAgICogUmVxdWVzdHMgdGhlIGNvbXBsZXRlIHNlcnZlciBzdGF0ZS4gVXNlZnVsIGlmIHRoZSBjbGllbnQncyBzdGF0ZSBzZWVtcyB0byBiZSBpbmNvbnNpc3RlbnQgc2VydmVyJ3MuXHJcbiAgICAgKi9cclxuICAgIHB1YmxpYyByZXF1ZXN0Q29tcGxldGVTdGF0ZSgpIHtcclxuICAgICAgICByZXNldEhhbmRsZXIucHJvY2Vzcygge1xyXG4gICAgICAgICAgICB0cmlhbmdsZXNUb0NyZWF0ZTogW11cclxuICAgICAgICB9ICk7XHJcbiAgICB9XHJcblxyXG4gICAgcHVibGljIG5ld0dhbWUoKSB7XHJcbiAgICAgICAgcmVzZXRIYW5kbGVyLnByb2Nlc3MoIHtcclxuICAgICAgICAgICAgdHJpYW5nbGVzVG9DcmVhdGU6IFtdXHJcbiAgICAgICAgfSApO1xyXG4gICAgfVxyXG59XHJcblxyXG5cblxuXG4vLyBXRUJQQUNLIEZPT1RFUiAvL1xuLy8gLi90eXBlc2NyaXB0L25ldHdvcmsvb2ZmbGluZS50cyJdLCJzb3VyY2VSb290IjoiIn0=