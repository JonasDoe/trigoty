module.exports = {
  devtool: 'inline-source-map',
  entry: './typescript/run/main.ts',
  output: {
    path: __dirname + '/website/js',
    filename: 'app.js'
  },
  resolve: {
    extensions: ['.ts', '.tsx', '.js']
  },
  module: {
    rules: [
      { test: /\.tsx?$/, loader: 'ts-loader' }
    ]
  }
}