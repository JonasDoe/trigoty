package de.trigoty;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.util.List;

import org.junit.After;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.test.context.junit4.SpringRunner;

import de.trigoty.data.Pair;
import de.trigoty.data.config.DataConfig;
import de.trigoty.service.loader.TriangleLoader;
import de.trigoty.service.web.objects.ObjectConvertionService;
import de.trigoty.service.web.objects.beans.TriangleBean;
import de.trigoty.state.StateHolder;
import de.trigoty.state.objects.IngameTriangle;

@RunWith(SpringRunner.class)
@SpringBootTest
@Configuration
@ComponentScan
public class TrigotyApplicationTests {

	@Autowired
	TriangleLoader triangleLoader;
	@Autowired
	StateHolder state;
	@Autowired
	ObjectConvertionService conversion;

	@After
	public void resetServer() {
		state.clear();
	}

	@Test
	public void testScenarios1() throws Exception {
		testScenario("scenario1.json");
	}

	@Test
	public void testScenarios2() throws Exception {
		testScenario("scenario2.json");
	}

	private void testScenario(final String scenarioName) throws Exception {
		final List<TriangleBean> scenarioTriangles = triangleLoader.loadTriangles(scenarioName);

		for (final TriangleBean toRequest : scenarioTriangles) {
			final String player = toRequest.getOwner();
			if (!state.getState().containsKey(player)) {
				state.registerNewPlayer(player);
				for (int i = 0; i < 1000; i++)
					state.turn();
			}
			final IngameTriangle triangle = conversion.toTriangle(toRequest, DataConfig.ORIENTATION);
			final Pair<List<IngameTriangle>, List<IngameTriangle>> addedTriangles = state.addTriangle(triangle);
			assertTrue(addedTriangles.first.isEmpty());
			assertEquals(1, addedTriangles.second.size());
			assertEquals(toRequest, conversion.toTriangleBean(addedTriangles.second.get(0)));
		}

		// does not work anymore since the request of a existing triangle will be
		// interpreted as upgrade
		// for (final TriangleBean toRequest : scenarioTriangles) {
		// final String player = toRequest.getOwner();
		// assertTrue(state.getState().containsKey(player));
		// final IngameTriangle triangle = conversion.toTriangle(toRequest,
		// DataConfig.ORIENTATION);
		// final Pair<List<IngameTriangle>, List<IngameTriangle>> addedTriangles =
		// state.addTriangle(triangle);
		// assertEquals(null, addedTriangles);
		// }
	}
}
