package de.trigoty.service.stateupdate;

import java.io.Closeable;
import java.io.IOException;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

import javax.annotation.PreDestroy;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.context.event.ApplicationReadyEvent;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationListener;
import org.springframework.stereotype.Component;

import de.trigoty.backend.socket.handler.TurnHandler;
import de.trigoty.state.config.ApplicationConfiguration;

/**
 * Schedules game turns.
 *
 * @author J
 *
 */
// TODO disable on test runs
@Component
public class IncomeUpdater implements Closeable, ApplicationListener<ApplicationReadyEvent> {
	/** Frequency of game turns, in seconds */
	private static final int PERIOD = 1;
	/** Calls the turns */
	private final ScheduledExecutorService turnCaller = Executors.newSingleThreadScheduledExecutor();

	@Autowired
	private ApplicationContext ctx;
	@Autowired
	private ApplicationConfiguration appConfig;

	@Override
	public void onApplicationEvent(final ApplicationReadyEvent event) {
		if (appConfig.debug < 2)
			turnCaller.scheduleAtFixedRate(() -> ctx.getBean(TurnHandler.class).run(), 1, PERIOD, TimeUnit.SECONDS);

	}

	@Override
	@PreDestroy
	public void close() throws IOException {
		turnCaller.shutdownNow();
	}

}
