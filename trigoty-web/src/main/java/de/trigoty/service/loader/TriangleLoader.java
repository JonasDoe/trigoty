package de.trigoty.service.loader;

import java.io.File;
import java.nio.file.Paths;
import java.util.List;

import org.springframework.stereotype.Service;

import com.fasterxml.jackson.databind.ObjectMapper;

import de.trigoty.service.web.objects.beans.TriangleBean;

/**
 * Loads a list of {@code TriangleBeans} from a json file.
 * 
 * @author J
 *
 */
@Service
public class TriangleLoader {

	/**
	 * Loads {@code TriangleBeans} from a json file in the resource. The order of
	 * the {@code Triangles} is likely to represent the order they shall be
	 * constructed in.
	 * 
	 * @param jsonFileName
	 *            name of the json file containing the triangles. Relative to the
	 *            resources path.
	 * @return the loaded {@code TriangleBeans}
	 * @throws Exception
	 *             if loading or parsing fails
	 */
	public List<TriangleBean> loadTriangles(String jsonFileName) throws Exception {
		File scenarioFile = Paths.get(getClass().getClassLoader().getResource(jsonFileName).toURI()).toFile();
		ObjectMapper jsonToObject = new ObjectMapper();
		Triangles triangles = jsonToObject.readValue(scenarioFile, Triangles.class);
		return triangles.getTriangles();
	}
}
