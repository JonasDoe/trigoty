package de.trigoty.service.loader;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;

import de.trigoty.service.web.objects.beans.TriangleBean;

/**
 * Represents a list of {@code TriangleBeans}. Used as (de-)serialization
 * object.
 * 
 * @author J
 *
 */
class Triangles {
	/**
	 * A list of {@code Triangles}. Their is likely to represent the order they
	 * shall be constructed in.
	 */
	private List<TriangleBean> triangles;

	/**
	 * Creates a new {@code Triangles} instance.
	 * 
	 * @param triangles
	 *            hold by this instance. Their is likely to represent the order they
	 *            shall be constructed in.
	 */
	@JsonCreator
	public Triangles(@JsonProperty("triangles") List<TriangleBean> triangles) {
		this.triangles = triangles;
	}

	/**
	 * Returns the {@code Triangles} held by this instance. Their is likely to
	 * represent the order they shall be constructed in.
	 * 
	 * @return the {@code Triangles}
	 */
	public List<TriangleBean> getTriangles() {
		return triangles;
	}

	/**
	 * Sets the {@code Triangles} to be held by this instance. Their is likely to
	 * represent the order they shall be constructed in.
	 * 
	 * @param triangles
	 *            the {@code Triangles}
	 */
	public void setTriangles(List<TriangleBean> triangles) {
		this.triangles = triangles;
	}
}