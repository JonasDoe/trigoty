package de.trigoty.service.web.alias;

import java.util.concurrent.atomic.AtomicInteger;

import org.springframework.stereotype.Component;

/**
 * Greates aliases like "{@code player1}", "{@code player2}", "{@code player3}".
 *
 * @author J
 *
 */
@Component("incremental")
public class IncrementalAliasGenerator extends AbstractAliasManager {
	private final AtomicInteger aliasCount = new AtomicInteger();

	@Override
	public void reset() {
		// TODO locking?
		aliasCount.set(0);
		super.reset();
	}

	@Override
	protected String createAlias(final String name) {
		return "player" + aliasCount.incrementAndGet();
	}
}
