package de.trigoty.service.web.objects.command;

import java.util.Collections;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;

import de.trigoty.data.GeometricObject;
import de.trigoty.service.web.objects.beans.TriangleBean;

/**
 * Holds all information to rebuilt the whole canvas.
 * 
 * @author J
 *
 */
public class ResetCommand implements GeometricObject, ServerCommand {
	public static final Type TYPE = Type.RESET;
	public static final ResetCommand CLEAR = new ResetCommand(Collections.emptyList());

	private List<TriangleBean> trianglesToCreate;

	@JsonCreator
	public ResetCommand(@JsonProperty("trianglesToCreate") List<TriangleBean> trianglesToCreate) {
		this.trianglesToCreate = trianglesToCreate;
	}

	public List<TriangleBean> getTrianglesToCreate() {
		return trianglesToCreate;
	}

	public void setTrianglesToCreate(List<TriangleBean> trianglesToCreate) {
		this.trianglesToCreate = trianglesToCreate;
	}

	@Override
	public Type getType() {
		return TYPE;
	}

}
