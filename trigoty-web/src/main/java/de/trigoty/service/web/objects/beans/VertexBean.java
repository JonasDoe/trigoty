package de.trigoty.service.web.objects.beans;

import java.util.Objects;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * Data class for a point/{@code Vertex} to be (de-)serialized to a JSON object.
 *
 * @author J
 *
 */
public class VertexBean implements Comparable<VertexBean> {
	/** The x coordinate of this point */
	private int x;
	/** The y coordinate of this point */
	private int y;
	/** The player the {@code Triangle} belongs to */
	private String player;

	/**
	 * Creates a new {@code VertexBean}.
	 *
	 * @param x
	 *            the x coordinate of this point
	 * @param y
	 *            the y coordinate of this point
	 * @param player
	 *            the player the {@code Triangle} belongs to
	 */
	@JsonCreator
	public VertexBean(@JsonProperty("x") final int x, @JsonProperty("y") final int y,
			@JsonProperty("player") final String player) {
		this.x = x;
		this.y = y;
		this.player = player;
	}

	/**
	 * Returns the x coordinate of this point.
	 *
	 * @return the x coordinate of this point
	 */
	public int getX() {
		return x;
	}

	/**
	 * Sets the x coordinate of this point.
	 *
	 * @param x
	 *            the x coordinate of this point
	 */
	public void setX(final int x) {
		this.x = x;
	}

	/**
	 * Returns the y coordinates.
	 *
	 * @return the y coordinate
	 */
	public int getY() {
		return y;
	}

	/**
	 * Sets the y coordinate.
	 *
	 * @param y
	 *            the y coordinate
	 */
	public void setY(final int y) {
		this.y = y;
	}

	/**
	 * Returns the player the {@code Triangle} belongs to.
	 *
	 * @return the player the {@code Triangle} belongs to
	 */
	public String getPlayer() {
		return player;
	}

	/**
	 * Sets the player the {@code Triangle} belongs to.
	 *
	 * @param the
	 *            player the {@code Triangle} belongs to
	 */
	public void setPlayer(final String player) {
		this.player = player;
	}

	@Override
	public int compareTo(final VertexBean other) {
		final int xComp = x - other.x;
		return xComp != 0 ? xComp : y - other.y;
	}

	@Override
	public String toString() {
		return "{x:" + x + ",y:" + y + "," + "player:" + player + "}";
	}

	@Override
	public int hashCode() {
		return Objects.hash(x, y, player);
	}

	@Override
	public boolean equals(final Object obj) {
		if (!(obj instanceof VertexBean)) return false;
		if (this == obj) return true;
		final VertexBean other = (VertexBean) obj;
		return Objects.equals(x, other.x) && Objects.equals(y, other.y) && Objects.equals(player, other.player);
	}
}
