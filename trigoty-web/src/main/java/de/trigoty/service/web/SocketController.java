package de.trigoty.service.web;

import java.security.Principal;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationListener;
import org.springframework.messaging.handler.annotation.MessageMapping;
import org.springframework.stereotype.Controller;
import org.springframework.web.socket.messaging.SessionDisconnectEvent;

import de.trigoty.backend.JobProcessor;
import de.trigoty.backend.socket.handler.ClearGameHandler;
import de.trigoty.backend.socket.handler.DisconnectHandler;
import de.trigoty.backend.socket.handler.RegisterPlayerHandler;
import de.trigoty.backend.socket.handler.SendFullStateHandler;
import de.trigoty.backend.socket.handler.TriangleProposalHandler;
import de.trigoty.service.web.objects.beans.TriangleBean;
import de.trigoty.state.config.ApplicationConfiguration;

/**
 * Handles the communication with the front end by receiving requests and
 * sending commands.
 *
 * @author J
 *
 */
@Controller
public class SocketController implements ApplicationListener<SessionDisconnectEvent> {
	/** Request path to register as a new player */
	private static final String REGISTER_PLAYER_PATH = "/register";
	/** Request path to propose {@code Triangle} */
	private static final String PROPOSE_TRIANGLE_PATH = "/propose/triangle";
	/** Request path for the complete game state */
	private static final String REQUEST_COMPLETESTATE_PATH = "/fullstate";
	/** Path to request a game reset */
	private static final String REQUEST_RESET_PATH = "/debug/new_game";

	/** Hold the settings for this application */
	@Autowired
	private ApplicationConfiguration appConfig;
	/** Processes the updates */
	@Autowired
	private JobProcessor updateProcessor;
	@Autowired
	/** Used to handle and propagate disconnects */
	private SessionManager sessionMgr;
	@Autowired
	private ApplicationContext ctx;

	// TODO second JobProcessor who informs the clients simultaniously?

	@Autowired
	public SocketController(final JobProcessor updateProcessor, final ApplicationConfiguration appConfig) {
		// TODO set default path
		updateProcessor.start();
	}

	@MessageMapping(REGISTER_PLAYER_PATH)
	// all available objects:
	// https://docs.spring.io/spring/docs/current/javadoc-api/org/springframework/messaging/handler/annotation/MessageMapping.html
	public void registerPlayer(final Principal principal/* , SimpMessageHeaderAccessor headers */) {
		updateProcessor.enqueueJob(ctx.getBean(RegisterPlayerHandler.class, principal));
	}

	/**
	 * Validates a proposed triangle. If it's valid, an update will be send,
	 * otherwise a reset to the last server state
	 *
	 * @param proposedTriangle
	 *            to be validated
	 * @return either a server update or a reset
	 */
	@MessageMapping(PROPOSE_TRIANGLE_PATH)
	public void processTriangleProposal(final Principal principal, final TriangleBean proposedTriangle) {
		updateProcessor.enqueueJob(ctx.getBean(TriangleProposalHandler.class, principal, proposedTriangle));
	}

	/**
	 * Requests the reset information, i.e. the last valid state of the server.
	 *
	 * @return the reset information
	 */
	@MessageMapping(REQUEST_COMPLETESTATE_PATH)
	public void getFullStateInfos(final Principal principal) {
		updateProcessor.enqueueJob(ctx.getBean(SendFullStateHandler.class, principal));
	}

	/**
	 * Handles disconnection a users.
	 *
	 * @param event
	 *            holds information about the user that has been disconnected.
	 */
	@Override
	public void onApplicationEvent(final SessionDisconnectEvent event) {
		sessionMgr.close(event.getSessionId());
		ctx.getBean(DisconnectHandler.class, event.getUser()).run();
	}

	/**
	 * Requests the reset information, i.e. the last valid state of the server.
	 *
	 * @return the reset information
	 */
	@MessageMapping(REQUEST_RESET_PATH)
	public void resetServer(final Principal principal) {
		if (appConfig.debug == 0) return;
		else {
			updateProcessor.stop(true);
			ctx.getBean(ClearGameHandler.class).run();
			sessionMgr.reset();
			updateProcessor.start();
		}
	}
}
