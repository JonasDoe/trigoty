package de.trigoty.service.web;

import java.time.Instant;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

import org.springframework.messaging.Message;
import org.springframework.messaging.core.MessagePostProcessor;
import org.springframework.messaging.support.MessageBuilder;
import org.springframework.messaging.support.NativeMessageHeaderAccessor;

/**
 * Keeps track of the processing time of a request.
 * 
 * @author J
 *
 */
public class BenchmarkingMessagePostProcessor implements MessagePostProcessor {

	/** Start time to keep track of the processing timg */
	private final Instant start;

	/**
	 * Creates a new {@code BenchmarkingMessagePostProcessor} with the current time
	 * as start time.
	 */
	public BenchmarkingMessagePostProcessor() {
		this(Instant.now());
	}

	/**
	 * Creates a new {@code BenchmarkingMessagePostProcessor} with the current time
	 * as start time.
	 *
	 * @param start
	 *            the start time
	 */
	public BenchmarkingMessagePostProcessor(final Instant start) {
		this.start = start;
	}

	@Override
	public Message<?> postProcessMessage(final Message<?> message) {
		final Long end = System.currentTimeMillis();
		final Map<String, Object> nativeHeader = new HashMap<>();
		nativeHeader.put("processing-time", Collections.singletonList(String.valueOf(end - start.toEpochMilli())));
		final Message<?> build = MessageBuilder.fromMessage(message)
				.setHeader(NativeMessageHeaderAccessor.NATIVE_HEADERS, nativeHeader).build();
		return build;
	}
}
