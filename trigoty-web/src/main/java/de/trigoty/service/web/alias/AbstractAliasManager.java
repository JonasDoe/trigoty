package de.trigoty.service.web.alias;

import java.util.Map;
import java.util.Map.Entry;
import java.util.Optional;
import java.util.concurrent.ConcurrentHashMap;

/**
 * Concurrent implementation of an {@code AliasManager}
 *
 * @author J
 *
 */
public abstract class AbstractAliasManager implements AliasManager {
	/** Holds the aliases for the names */
	private final Map<String, String> nameToAlias = new ConcurrentHashMap<>();

	@Override
	public Optional<String> register(final String name) {
		if (nameToAlias.containsKey(name)) return Optional.empty();
		else {
			final String alias = createAlias(name);
			if (alias == null) return Optional.empty();
			else {
				nameToAlias.put(name, alias);
				return Optional.of(alias);
			}
		}
	}

	@Override
	public boolean unregister(final String name) {
		return nameToAlias.remove(name) != null;
	}

	@Override
	public boolean isRegistered(final String name) {
		return nameToAlias.containsKey(name);
	}

	@Override
	public Optional<String> getAlias(final String name) {
		final String alias = nameToAlias.get(name);
		return alias != null ? Optional.of(alias) : Optional.empty();
	}

	@Override
	public Optional<String> getName(final String alias) {
		// TODO not really that performant, but keeping two maps in sync does not sound
		// like fun either
		for (final Entry<String, String> entry : nameToAlias.entrySet())
			if (entry.getValue().equals(alias)) return Optional.of(entry.getKey());
		return Optional.empty();
	}

	@Override
	public void reset() {
		nameToAlias.clear();
	}

	protected abstract String createAlias(String name);
}
