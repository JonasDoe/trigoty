package de.trigoty.service.web.objects.command;

/**
 * Command and information sent from the server.
 *
 * @author J
 *
 */
public interface ServerCommand {
	/**
	 * Returns the type of the command.
	 *
	 * @return the type of the command
	 */
	Type getType();

	/**
	 * Marks the kind of command for the front end.
	 *
	 * @author J
	 *
	 */
	enum Type {
		/** No incremental update but a re-construction form the scratch */
		RESET,
		/** Incremental update for the present frontend state */
		UPDATE,
		/** Informs the player about his current account balance */
		ACCOUNT_INFO,
		/** Gives the player the initial information */
		INIT
	}
}
