package de.trigoty.service.web.alias;

import java.util.Optional;

/**
 * Generations aliases for a given name.
 *
 * @author J
 *
 */
public interface AliasManager {

	/**
	 * Registers a alias for a given name.
	 *
	 * @param name
	 *            the alias shall be created for. Depending on the implementation,
	 *            this information might not be used.
	 * @return the generated alias, or {@link Optional#empty} if the registration
	 *         fails.
	 */
	Optional<String> register(String name);

	/**
	 * Unregisters the name and its alias.
	 *
	 * @param name
	 *            to be unregistered
	 * @return {@code true} if the unregistration was successful, otherwise
	 *         {@code false}
	 */
	boolean unregister(String name);

	/**
	 * Checks whether a name is already registered with an alias;
	 *
	 * @param name
	 *            to be checked
	 * @return {@code true} if the name is already registered with an alias,
	 *         otherwise {@code false}
	 */
	boolean isRegistered(String name);

	/**
	 * Returns an alias for the given name.
	 *
	 * @param name
	 *            the alias is requested for
	 * @return the alias, if the name is registered, otherwise
	 *         {@link Optional#empty}
	 */
	Optional<String> getAlias(String name);

	/**
	 * Returns the id/name for an alias.
	 * 
	 * @param alias
	 *            the name is requested for
	 * @return the name, if the alias is linked to one, otherwise
	 *         {@link Optional#empty()}}
	 */
	Optional<String> getName(String alias);

	/**
	 * Resets the generators state, if there is any.
	 */
	default void reset() {};

}
