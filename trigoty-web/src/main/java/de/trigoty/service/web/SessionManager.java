package de.trigoty.service.web;

import java.io.IOException;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.web.socket.WebSocketSession;

/**
 * Holds all {@code WebSocketSessions}.
 *
 * @author J
 *
 */
@Service
public class SessionManager {
	/** Logger used in this application */
	private static final Logger LOGGER = LoggerFactory.getLogger(SessionManager.class);
	/** Holds all {@code WebSocketSessions} by id */
	private final Map<String, WebSocketSession> idToSession = new ConcurrentHashMap<>();

	/**
	 * Registers a {@code WebSocketSession} for this {@code SessionManager}.
	 *
	 * @param session
	 *            to be registers
	 */
	public void register(final WebSocketSession session) {
		idToSession.put(session.getId(), session);
	}

	/**
	 * Unregisters {@code WebSocketSession}.
	 *
	 * @param sessionId
	 *            of the {@code WebSocketSession} to be unregistered
	 */
	public void unregister(final String sessionId) {
		idToSession.remove(sessionId);
	}

	/**
	 * Unregisters and closes a {@code WebSocketSession}.
	 * 
	 * @param sessionId
	 *            of the {@code WebSocketSession} to be closed
	 */
	public void close(final String sessionId) {
		final WebSocketSession session = idToSession.remove(sessionId);
		if (session != null) try {
			session.close();
		} catch (final IOException e) {
			LOGGER.debug("Error when closing session {}: {}", sessionId, e);
		}
	}

	/**
	 * Unregisters and closes all {@code WebSocketSessions}.
	 */
	public void reset() {
		idToSession.keySet().forEach(this::unregister);
	}

}
