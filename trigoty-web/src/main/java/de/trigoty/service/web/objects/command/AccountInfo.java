package de.trigoty.service.web.objects.command;

/** Tells the player about the money he owns. */
public class AccountInfo implements ServerCommand {
	/** The type of this command */
	public static final Type TYPE = Type.ACCOUNT_INFO;
	/** The account balance of the player */
	private long balance;

	/**
	 * Creates a new {@code BalanceInfo} item.
	 *
	 * @param balance
	 *            the player's account balance
	 */
	public AccountInfo(final long balance) {
		this.balance = balance;
	}

	/**
	 * Returns the account balance information.
	 *
	 * @return the balance
	 */
	public long getBalance() {
		return balance;
	}

	/**
	 * Sets the account balance information.
	 *
	 * @param balance
	 *            the balance to set
	 */
	public void setBalance(final long balance) {
		this.balance = balance;
	}

	@Override
	public Type getType() {
		return TYPE;
	}

}
