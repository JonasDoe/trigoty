package de.trigoty.service.web.objects.beans;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Objects;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * Data class for a {@code Triangle} to be (de-)serialized to a JSON object.
 * Note that while it contains the points/{@code Vertices} it does not give
 * guarantees for any orientation of the {@code Triangle} or any the order of
 * its {@code Vertices}.
 *
 * @author J
 *
 */
public class TriangleBean implements Comparable<TriangleBean> {
	/** The first point of the {@code Triangle} */
	private VertexBean point1;
	/** The second point of the {@code Triangle} */
	private VertexBean point2;
	/** The third point of the {@code Triangle} */
	private VertexBean point3;
	/** The player the {@code Triangle} belongs to */
	private String owner;
	/** The base value and the cost of the {@code Triangle} */
	private long baseValue;
	/** The difficulty to destroy the {@code Triangle} */
	private long defense;
	/** Used to speed up comparisons */
	private List<VertexBean> vertices;

	/**
	 * Creates a new {@code TriangleBean}
	 *
	 * @param point1
	 *            the first point of the {@code Triangle}
	 * @param point2
	 *            the second point of the {@code Triangle}
	 * @param point3
	 *            the third point of the {@code Triangle}
	 * @param player
	 *            the player the {@code Triangle} belongs to
	 */
	@JsonCreator
	public TriangleBean(@JsonProperty("point1") final VertexBean point1,
			@JsonProperty("point2") final VertexBean point2, @JsonProperty("point3") final VertexBean point3,
			@JsonProperty("baseValue") final long baseValue, @JsonProperty("defense") final long defense,
			@JsonProperty("player") final String player) {
		this.point1 = point1;
		this.point2 = point2;
		this.point3 = point3;
		this.baseValue = baseValue;
		this.defense = defense;
		this.owner = player;
		updateVertices();
	}

	/**
	 * Updates {@link #vertices}.
	 */
	private void updateVertices() {
		final List<VertexBean> vertices = Arrays.asList(point1, point2, point3);
		Collections.sort(vertices);
		this.vertices = vertices;
	}

	/**
	 * Returns the first point of the {@code Triangle}.
	 *
	 * @return the first point of the {@code Triangle}
	 */
	public VertexBean getPoint1() {
		return point1;
	}

	/**
	 * Sets the first point of the {@code Triangle}.
	 *
	 * @param point1
	 *            the first point of the {@code Triangle}
	 */
	public void setPoint1(final VertexBean point1) {
		this.point1 = point1;
		updateVertices();
	}

	/**
	 * Returns the second point of the {@code Triangle}.
	 *
	 * @return the second point of the {@code Triangle}
	 */
	public VertexBean getPoint2() {
		return point2;
	}

	/**
	 * Returns the second point of the {@code Triangle}.
	 *
	 * @param point2
	 *            the second point of the {@code Triangle}
	 */
	public void setPoint2(final VertexBean point2) {
		this.point2 = point2;
		updateVertices();
	}

	/**
	 * Returns the third point of the {@code Triangle}.
	 *
	 * @return the third point of the {@code Triangle}
	 */
	public VertexBean getPoint3() {
		return point3;
	}

	/**
	 * Sets the third point of the {@code Triangle}.
	 *
	 * @param point3
	 *            the third point of the {@code Triangle}
	 */
	public void setPoint3(final VertexBean point3) {
		this.point3 = point3;
		updateVertices();
	}

	/**
	 * Returns the player the {@code Triangle} belongs to.
	 *
	 * @return the player the {@code Triangle} belongs to
	 */
	public String getOwner() {
		return owner;
	}

	/**
	 * Sets the player the {@code Triangle} belongs to.
	 *
	 * @param owner
	 *            the player the {@code Triangle} belongs to
	 */
	public void setOwner(final String owner) {
		this.owner = owner;
	}

	/**
	 * Returns the base value, i.e. the costs of the {@code Triangle}.
	 *
	 * @return the base value
	 */
	public long getBaseValue() {
		return baseValue;
	}

	/**
	 * Sets the base value, i.e. the costs of the {@code Triangle}.
	 *
	 * @param baseValue
	 *            the base value
	 */
	public void setBaseValue(final long baseValue) {
		this.baseValue = baseValue;
	}

	/**
	 * Returns the defense value.
	 * 
	 * @return the defense value
	 */
	public long getDefense() {
		return defense;
	}

	/**
	 * Sets the defense value.
	 * 
	 * @param defense
	 *            the defense value to set
	 */
	public void setDefense(final long defense) {
		this.defense = defense;
	}

	@Override
	public int compareTo(final TriangleBean other) {
		final List<VertexBean> otherVertices = other.vertices;
		final int p1Comp = vertices.get(0).compareTo(otherVertices.get(0));
		if (p1Comp != 0) return p1Comp;
		final int p2Comp = vertices.get(1).compareTo(otherVertices.get(1));
		if (p2Comp != 0) return p2Comp;
		final int p3Comp = vertices.get(2).compareTo(otherVertices.get(2));
		if (p3Comp != 0) return p3Comp;
		return owner.compareTo(other.getOwner());
	}

	@Override
	public String toString() {
		return "{point1:" + point1.toString() + ",point2:" + point2.toString() + ",point3:" + point3.toString()
				+ ",player:" + owner + "}";
	}

	@Override
	public int hashCode() {
		return Objects.hash(point1, point2, point3, owner);
	}

	@Override
	public boolean equals(final Object obj) {
		if (!(obj instanceof TriangleBean)) return false;
		if (this == obj) return true;
		final List<VertexBean> otherVertices = ((TriangleBean) obj).vertices;
		return Objects.equals(vertices.get(0), otherVertices.get(0))
				&& Objects.equals(vertices.get(1), otherVertices.get(1))
				&& Objects.equals(vertices.get(2), otherVertices.get(2))
				&& Objects.equals(owner, ((TriangleBean) obj).owner);
	}
}
