package de.trigoty.service.web.alias;

/**
 * Returns the given name, unmodified.
 * 
 * @author J
 *
 */
public class IdentityAliasManager extends AbstractAliasManager {

	@Override
	protected String createAlias(String name) {
		return name;
	}
}
