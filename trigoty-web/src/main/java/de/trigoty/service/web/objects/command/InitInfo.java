package de.trigoty.service.web.objects.command;

/**
 * Gives a new player all inital information.
 *
 * @author J
 *
 */
public class InitInfo implements ServerCommand {
	public static final Type TYPE = Type.INIT;
	/** The id the player must use and refer to */
	private String playerId;
	/** The state of the game to initialize the new player's board with */
	private ResetCommand initialState;

	public InitInfo(final String playerId, final ResetCommand initialState) {
		this.playerId = playerId;
		this.initialState = initialState;
	}

	public String getPlayerId() {
		return playerId;
	}

	public void setPlayerId(final String playerId) {
		this.playerId = playerId;
	}

	public ResetCommand getInitialState() {
		return initialState;
	}

	public void setInitialState(final ResetCommand initalState) {
		this.initialState = initalState;
	}

	@Override
	public Type getType() {
		return TYPE;
	}

}
