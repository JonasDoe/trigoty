package de.trigoty.service.web.objects.beans;

import java.util.Objects;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * Data class for a line/{@code Edge} to be (de-)serialized to a JSON object.
 *
 * @author J
 *
 */
public class EdgeBean implements Comparable<EdgeBean> {
	/** The starting point of the line */
	private VertexBean start;
	/** The end point of the line */
	private VertexBean end;
	/** The player the {@code Triangle} belongs to */
	private String player;

	/**
	 * Creates a new {@code EdgeBean}.
	 *
	 * @param starting
	 *            the start point of the line
	 * @param end
	 *            the end point of the line
	 * @param player
	 *            the player the {@code Triangle} belongs to
	 */
	@JsonCreator
	public EdgeBean(@JsonProperty("start") final VertexBean start, @JsonProperty("end") final VertexBean end,
			@JsonProperty final String player) {
		this.start = start;
		this.end = end;
		this.player = player;
	}

	/**
	 * Returns the starting point of this line.
	 *
	 * @return the starting point of this line
	 */
	public VertexBean getStart() {
		return start;
	}

	/**
	 * Sets the starting point of this line.
	 *
	 * @param the
	 *            starting point of this line
	 */
	public void setStart(final VertexBean start) {
		this.start = start;
	}

	/**
	 * Returns the end point of this line.
	 *
	 * @return the end point of this line
	 */
	public VertexBean getEnd() {
		return end;
	}

	/**
	 * Sets the end point of this line.
	 *
	 * @param the
	 *            end point of this line
	 */
	public void setEnd(final VertexBean end) {
		this.end = end;
	}

	/**
	 * Returns the player the {@code Triangle} belongs to.
	 *
	 * @return the player the {@code Triangle} belongs to
	 */
	public String getPlayer() {
		return player;
	}

	/**
	 * Sets the player the {@code Triangle} belongs to.
	 *
	 * @param the
	 *            player the {@code Triangle} belongs to
	 */
	public void setPlayer(final String player) {
		this.player = player;
	}

	@Override
	public int compareTo(final EdgeBean other) {
		final int startComp = start.compareTo(other.start);
		return startComp != 0 ? startComp : end.compareTo(other.end);
	}

	@Override
	public String toString() {
		return "{start:" + start.toString() + ",end:" + end.toString() + ",player:" + player + "}";
	}

	@Override
	public int hashCode() {
		return Objects.hash(start, end, player);
	}

	@Override
	public boolean equals(final Object obj) {
		if (!(obj instanceof EdgeBean)) return false;
		if (this == obj) return true;
		final EdgeBean other = (EdgeBean) obj;
		return Objects.equals(start, other.start) && Objects.equals(end, other.end)
				&& Objects.equals(player, other.player);
	}
}
