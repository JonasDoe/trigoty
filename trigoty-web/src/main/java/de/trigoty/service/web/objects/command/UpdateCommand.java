package de.trigoty.service.web.objects.command;

import java.util.Collections;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;

import de.trigoty.service.web.objects.beans.TriangleBean;

/**
 * Represents a list of {@code Triangles} to be removed and to be added issued
 * by the server. First, the {@code Triangles} to be removed are supposed to be
 * processed, in the order of their list. In the second step the
 * {@code Triangles} to be added are supposed to be processed, again in the
 * order they appear in the list.
 *
 * @author J
 *
 */
public class UpdateCommand implements ServerCommand {
	/** The type of this command */
	public static final Type TYPE = Type.UPDATE;
	/** A command without any further instructions */
	public static final UpdateCommand NO_OP = new UpdateCommand(Collections.emptyList(), Collections.emptyList());
	/**
	 * The {@code Triangles} which are supposed to be removed before
	 * {@code Triangles} will be added
	 */
	private List<TriangleBean> trianglesToRemove;
	/**
	 * The {@code Triangles} which are supposed to be added after all listed
	 * {@code Triangles} have been removed
	 */
	private List<TriangleBean> trianglesToAdd;

	/**
	 * Creates a new {@code UpdateCommand}.
	 * 
	 * @param trianglesToRemove
	 *            the {@code Triangles} which are supposed to be removed in the
	 *            given order and before {@code Triangles} will be added
	 * @param trianglesToAdd
	 *            the {@code Triangles} which are supposed to be added in the given
	 *            order after all listed {@code Triangles} have been removed
	 */
	@JsonCreator
	public UpdateCommand(@JsonProperty("trianglesToRemove") final List<TriangleBean> trianglesToRemove,
			@JsonProperty("trianglesToAdd") final List<TriangleBean> trianglesToAdd) {
		setTrianglesToAdd(trianglesToAdd);
		setTrianglesToRemove(trianglesToRemove);
	}

	/**
	 * Creates an {@code UpdateCommand} which only holds {@code Triangles} to be
	 * removed, in the given order.
	 * 
	 * @param trianglesToRemove
	 *            the {@code Triangles} which shall be removed, in the given order
	 * @return the newly created {@code UpdateCommand}
	 */
	public static UpdateCommand forDeletion(final List<TriangleBean> trianglesToRemove) {
		return new UpdateCommand(trianglesToRemove, Collections.emptyList());
	}

	/**
	 * Creates an {@code UpdateCommand} which only holds {@code Triangles} to be
	 * added, in the given order.
	 * 
	 * @param trianglesToRemove
	 *            the {@code Triangles} which shall be added, in the given order
	 * @return the newly created {@code UpdateCommand}
	 */
	public static UpdateCommand forAddition(final List<TriangleBean> trianglesToAdd) {
		return new UpdateCommand(Collections.emptyList(), trianglesToAdd);
	}

	/**
	 * Returns all {@code Triangles} which shall be added, in the given order.
	 * 
	 * @return all {@code Triangles} which shall be added
	 */
	public List<TriangleBean> getTrianglesToAdd() {
		return trianglesToAdd;
	}

	/**
	 * Sets all {@code Triangles} which shall be added, in the given order.
	 * 
	 * @param trianglesToAdd
	 *            all {@code Triangles} which shall be added
	 */
	public void setTrianglesToAdd(final List<TriangleBean> trianglesToAdd) {
		// TODO order matters atm ... settle conflicts between add and remove in here
		this.trianglesToAdd = trianglesToAdd;
	}

	/**
	 * Returns all {@code Triangles} which shall be removed, in the given order.
	 * 
	 * @return all {@code Triangles} which shall be removed
	 */
	public List<TriangleBean> getTrianglesToRemove() {
		return trianglesToRemove;
	}

	/**
	 * Sets all {@code Triangles} which shall be removed, in the given order.
	 * 
	 * @param trianglesToAdd
	 *            all {@code Triangles} which shall be removed
	 */
	public void setTrianglesToRemove(final List<TriangleBean> trianglesToRemove) {
		this.trianglesToRemove = trianglesToRemove;
	}

	@Override
	public Type getType() {
		return TYPE;
	}
}
