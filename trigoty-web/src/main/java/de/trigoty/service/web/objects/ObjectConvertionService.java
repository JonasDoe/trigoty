package de.trigoty.service.web.objects;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.stereotype.Service;

import de.trigoty.data.Edge;
import de.trigoty.data.Triangle;
import de.trigoty.data.Triangle.Orientation;
import de.trigoty.data.Vertex;
import de.trigoty.service.web.objects.beans.EdgeBean;
import de.trigoty.service.web.objects.beans.TriangleBean;
import de.trigoty.service.web.objects.beans.VertexBean;
import de.trigoty.service.web.objects.command.ResetCommand;
import de.trigoty.state.PlayerState;
import de.trigoty.state.StateHolder;
import de.trigoty.state.objects.IngameTriangle;

/**
 * Offers conversion from logic objects to beans and vice versa.
 *
 * @author J
 *
 */
@Service
public class ObjectConvertionService {
	@Autowired
	ApplicationContext ctx;

	/**
	 * Converts a {@code VertexBean} to a {@code Vertex}.
	 *
	 * @param toConvert
	 *            the {@code VertexBean} to be converted
	 * @return the newly created {@code Vertex}
	 */
	public Vertex toVertex(final VertexBean toConvert) {
		return new Vertex(toConvert.getX(), toConvert.getY());
	}

	/**
	 * Converts a {@code Vertex} to a {@code VertexBean}.
	 *
	 * @param toConvert
	 *            {@code Vertex} to be converted
	 * @param player
	 *            the player the {@code VertexBean} shall be assigned to
	 * @return the newly created {@code VertexBean}
	 */
	public VertexBean toVertexBean(final Vertex toConvert, final String player) {
		return new VertexBean(toConvert.getX(), toConvert.getY(), player);
	}

	/**
	 * Converts a {@code EdgeBean} to a {@code Edge}.
	 *
	 * @param toConvert
	 *            the {@code EdgeBean} to be converted
	 * @return the newly created {@code Edge}
	 */
	public Edge toEdge(final EdgeBean toConvert) {
		final Edge prevEdge = new Edge(null, null, toEdge(toConvert), null);
		final Edge nextEdge = new Edge(toVertex(toConvert.getEnd()), null, null, null);
		final Edge edge = new Edge(toVertex(toConvert.getStart()), prevEdge, nextEdge, null);
		nextEdge.setPrevEdge(edge);
		return edge;
	}

	/**
	 * Converts a {@code Edge} to a {@code EdgeBean}.
	 *
	 * @param toConvert
	 *            {@code Edge} to be converted
	 * @param player
	 *            the player the {@code EdgeBean} shall be assigned to
	 * @return the newly created {@code EdgeBean}
	 */
	public EdgeBean toEdgeBean(final Edge toConvert, final String player) {
		return new EdgeBean(toVertexBean(toConvert.getStartVertex(), player),
				toVertexBean(toConvert.getNextEdge().getStartVertex(), player), player);
	}

	/**
	 * Converts a {@code TriangleBean} to a {@code Triangle}.
	 *
	 * @param toConvert
	 *            the {@code TriangleBean} to be converted
	 * @return the newly created {@code Triangle}
	 */

	public IngameTriangle toTriangle(final TriangleBean toConvert, final Orientation targetOrientation) {
		final Triangle geometricTriangle = Triangle.of(toVertex(toConvert.getPoint1()), toVertex(toConvert.getPoint2()),
				toVertex(toConvert.getPoint3()), targetOrientation);
		return ctx.getBean(IngameTriangle.class, geometricTriangle, toConvert.getOwner());
	}

	/**
	 * Converts a {@code Triangle} to a {@code TriangleBean}.
	 *
	 * @param toConvert
	 *            {@code Triangle} to be converted
	 * @param player
	 *            the player the {@code TriangleBean} shall be assigned to
	 * @return the newly created {@code TriangleBean}
	 */
	public TriangleBean toTriangleBean(final IngameTriangle toConvert) {
		final List<Vertex> vertices = new ArrayList<>(toConvert.getTriangle().getVertices());
		final String player = toConvert.getOwner();
		return new TriangleBean(toVertexBean(vertices.get(0), player), toVertexBean(vertices.get(1), player),
				toVertexBean(vertices.get(2), player), toConvert.getBaseValue(), toConvert.getDefense(),
				toConvert.getOwner());
	}

	/**
	 * Creates a {@code ResetCommand} restoring a given state.
	 *
	 * @param targetState
	 *            the state the reset shall restore
	 * @return the {@code ResetCommand} restoring to the given state
	 */
	public ResetCommand stateToReset(final StateHolder targetState) {
		// streaming not worth it for couple of triangles
		final List<TriangleBean> existingTriangles = new LinkedList<>();
		for (final PlayerState state : targetState.getState().values()) {
			for (final IngameTriangle playerTriangle : state.getTriangles()) {
				existingTriangles.add(this.toTriangleBean(playerTriangle));
			}
		}
		return new ResetCommand(existingTriangles);
	}
}
