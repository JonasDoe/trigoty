package de.trigoty;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.AutoConfigurationPackage;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
@AutoConfigurationPackage
public class TrigotyApplication {

	public static void main(String[] args) {
		SpringApplication.run(TrigotyApplication.class, args);
	}
}
