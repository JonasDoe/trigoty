package de.trigoty.config;

import java.security.Principal;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Map;
import java.util.UUID;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.server.ServerHttpRequest;
import org.springframework.messaging.simp.config.MessageBrokerRegistry;
import org.springframework.security.authentication.AnonymousAuthenticationToken;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.User;
import org.springframework.web.socket.WebSocketHandler;
import org.springframework.web.socket.WebSocketSession;
import org.springframework.web.socket.config.annotation.AbstractWebSocketMessageBrokerConfigurer;
import org.springframework.web.socket.config.annotation.EnableWebSocketMessageBroker;
import org.springframework.web.socket.config.annotation.StompEndpointRegistry;
import org.springframework.web.socket.config.annotation.WebSocketTransportRegistration;
import org.springframework.web.socket.handler.WebSocketHandlerDecorator;
import org.springframework.web.socket.handler.WebSocketHandlerDecoratorFactory;
import org.springframework.web.socket.server.support.DefaultHandshakeHandler;

import de.trigoty.service.web.SessionManager;

/**
 * Sets up the STOMP over Websocket connection.
 * 
 * @author J
 *
 */
@Configuration
@EnableWebSocketMessageBroker
public class WebSocketConfig extends AbstractWebSocketMessageBrokerConfigurer {
	/** Logger used in this application */
	private static final Logger LOGGER = LoggerFactory.getLogger(WebSocketConfig.class);
	/** Base path for requests */
	private static final String WEBSOCKET_PATH = "/trigoty-websocket";
	@Autowired
	private SessionManager sessionMgr;

	@Override
	public void configureMessageBroker(final MessageBrokerRegistry config) {
		config.enableSimpleBroker("/update", "/queue");
		config.setUserDestinationPrefix("/user");
	}

	@Override
	public void registerStompEndpoints(final StompEndpointRegistry registry) {
		registry.addEndpoint(WEBSOCKET_PATH).setHandshakeHandler(new DefaultHandshakeHandler() {
			@Override
			protected Principal determineUser(final ServerHttpRequest request, final WebSocketHandler wsHandler,
					final Map<String, Object> attributes) {
				String websocketKey;
				try {
					websocketKey = request.getHeaders().get("sec-websocket-key").get(0);
				} catch (final Exception e) {
					LOGGER.debug("No websocket key available, using UUID instead.");
					websocketKey = UUID.randomUUID().toString();
				}
				Principal principal = request.getPrincipal();
				if (principal == null) {
					final Collection<SimpleGrantedAuthority> authorities = new ArrayList<>();
					final SimpleGrantedAuthority authority = new SimpleGrantedAuthority("PARTICIPATING");
					authorities.add(authority);
					principal = new AnonymousAuthenticationToken("WebsocketConfiguration",
							new User(websocketKey, "", Collections.singletonList(authority)), authorities);
				}
				return principal;
			}
		}).withSockJS();
	}

	@Override
	public void configureWebSocketTransport(final WebSocketTransportRegistration registration) {
		registration.addDecoratorFactory(new WebSocketHandlerDecoratorFactory() {
			@Override
			public WebSocketHandler decorate(final WebSocketHandler handler) {
				return new WebSocketHandlerDecorator(handler) {
					@Override
					public void afterConnectionEstablished(final WebSocketSession session) throws Exception {
						sessionMgr.register(session);
						super.afterConnectionEstablished(session);
					}
				};
			}
		});
		super.configureWebSocketTransport(registration);
	}
}