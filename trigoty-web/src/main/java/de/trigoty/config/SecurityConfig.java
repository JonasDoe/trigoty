package de.trigoty.config;

import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;

/**
 * Configures the security issues for the communication between server and
 * clients.
 * 
 * @author J
 *
 */
@EnableWebSecurity
public class SecurityConfig extends WebSecurityConfigurerAdapter {

	@Override
	protected void configure(HttpSecurity http) throws Exception {
		// ALTERNATIVE: remove spring-security-config dependency
		http.authorizeRequests().antMatchers("/trigoty-websocket/**", "/canvas.html").anonymous().anyRequest()
				.authenticated();
	}
}