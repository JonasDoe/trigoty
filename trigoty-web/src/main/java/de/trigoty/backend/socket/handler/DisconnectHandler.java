package de.trigoty.backend.socket.handler;

import java.security.Principal;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import de.trigoty.service.web.objects.beans.TriangleBean;
import de.trigoty.service.web.objects.command.UpdateCommand;
import de.trigoty.state.objects.IngameTriangle;

/**
 * Handles the disconnection of a player and propagates the disconnection.
 *
 * @author J
 *
 */
public class DisconnectHandler extends AbstractSocketHandler {

	/**
	 * Creates a new {@code DisconnectHandler}.
	 *
	 * @param disconnected
	 *            the disconnecting user
	 */
	DisconnectHandler(final Principal disconnected) {
		super("Disconnect handler", disconnected);
	}

	@Override
	protected void execute() {
		final String name = requestor.getName();
		final Optional<String> aliasOpt = aliasMgr.getAlias(name);
		if (!aliasOpt.isPresent()) return;
		aliasMgr.unregister(name);
		final List<IngameTriangle> removed = state.removePlayer(aliasOpt.get());
		final List<TriangleBean> removedBeans = removed.stream().map(conversion::toTriangleBean)
				.collect(Collectors.toList());
		broadcast(UpdateCommand.forDeletion(removedBeans));
	}
}
