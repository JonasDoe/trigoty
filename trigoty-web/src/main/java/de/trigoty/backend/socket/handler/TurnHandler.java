package de.trigoty.backend.socket.handler;

import java.util.Map;
import java.util.Map.Entry;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import de.trigoty.service.web.objects.command.AccountInfo;

public class TurnHandler extends AbstractSocketHandler {
	/** Logger used in this handler */
	private static final Logger LOGGER = LoggerFactory.getLogger(TurnHandler.class);

	TurnHandler() {
		super("Turn handler", null);
	}

	@Override
	protected void execute() {
		try {
			final Map<String, Long> balancePerPlayer = state.turn();
			for (final Entry<String, Long> balanceForPlayer : balancePerPlayer.entrySet())
				if (balanceForPlayer.getKey() != null && balanceForPlayer.getValue() != null)
					whisper(new AccountInfo(balanceForPlayer.getValue()), balanceForPlayer.getKey());
		} catch (final RuntimeException e) {
			LOGGER.info("Error when handling a turn: {}", e.getMessage());
		}
	}

}
