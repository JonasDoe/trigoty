package de.trigoty.backend.socket.handler;

import java.security.Principal;

/**
 * Sends the full game state to a requesting user.
 *
 * @author J
 *
 */
public class SendFullStateHandler extends AbstractSocketHandler {

	/**
	 * Creates a new {@code SendFullStateHandler}.
	 *
	 * @param requestor
	 *            requesting the full game state
	 */
	SendFullStateHandler(final Principal requestor) {
		super("Send full state handler", requestor);
	}

	@Override
	protected void execute() {
		whisper(conversion.stateToReset(state));
	}
}
