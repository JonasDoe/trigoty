package de.trigoty.backend.socket.handler;

import static java.util.Objects.requireNonNull;

import java.security.Principal;
import java.util.HashMap;
import java.util.Map;
import java.util.Optional;
import java.util.function.Predicate;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.messaging.core.MessagePostProcessor;
import org.springframework.messaging.simp.SimpMessageSendingOperations;
import org.springframework.messaging.support.NativeMessageHeaderAccessor;

import de.trigoty.backend.Job;
import de.trigoty.service.web.BenchmarkingMessagePostProcessor;
import de.trigoty.service.web.alias.AliasManager;
import de.trigoty.service.web.objects.ObjectConvertionService;
import de.trigoty.state.StateHolder;

/**
 * Holds some default features and methods for different jobs triggered by
 * player requests. These jobs are not guaranteed to be re-usable. Note that per
 * default jobs are required to be initialized via
 * {@link #init(SimpMessageSendingOperations, StateHolder, AliasManager, ObjectConvertionService)}
 * before they can run.
 *
 * @author J
 *
 */
// TODO enable autowire constructor
abstract class AbstractSocketHandler extends Job {
	/**
	 * Used to add some detail about the processing performance of this job to the
	 * response header
	 */
	protected final MessagePostProcessor postProcessor = new BenchmarkingMessagePostProcessor();
	/** Default public channel */
	protected static final String BROADCAST_DESTINATION = "/update";
	/** Default private channel */
	protected static final String WHISPER_DESTINATION = "/queue/reply";
	/** Used to send messages to private or public channels */
	@Autowired
	private SimpMessageSendingOperations messagingTemplate;
	/** Holds and manages the state of a game */
	@Autowired
	protected StateHolder state;
	/**
	 * Manages the {@code Principals'} names to aliases which can be propagated to
	 * the players
	 */
	@Autowired
	protected AliasManager aliasMgr;
	/** The user triggering this job by a request */
	protected final Principal requestor;
	/**
	 * Used to convert beans to the corresponding geometric objects and vice versa
	 */
	@Autowired
	protected ObjectConvertionService conversion;

	/**
	 * Creates a new {@code AbstractSocketHandler}.
	 *
	 * @param name
	 *            of the handler
	 * @param requestor
	 *            triggering this job. Depending on the job this can be
	 *            {@code null}.
	 */
	public AbstractSocketHandler(final String name, final Principal requestor) {
		super(name);
		this.requestor = requestor;
	}

	/**
	 * Initializes this job with the required components.
	 *
	 * @param messagingTemplate
	 *            used to send messages to private or public channels. Must not be
	 *            {@code null}.
	 * @param state
	 *            holds and manages the state of a game. Must not be {@code null}.
	 * @param aliasMgr
	 *            manages the {@code Principals'} names to aliases which can be
	 *            propagated to the players. Must not be {@code null}.
	 * @param conversion
	 *            used to convert beans to the corresponding geometric objects and
	 *            vice versa. Must not be {@code null}.
	 * @throws NullPointerException
	 *             if any argument is {@code null}
	 */
	public void init(final SimpMessageSendingOperations messagingTemplate, final StateHolder state,
			final AliasManager aliasMgr, final ObjectConvertionService conversion) {
		this.messagingTemplate = requireNonNull(messagingTemplate);
		this.state = requireNonNull(state);
		this.aliasMgr = requireNonNull(aliasMgr);
		this.conversion = requireNonNull(conversion);
	}

	@Override
	public void run() {
		if (messagingTemplate == null) throw new IllegalStateException(toString() + " has not been inialized yet.");
		execute();
	}

	/**
	 * Returns the {@code MessagePostProcessor} used for any messages sent.
	 *
	 * @return the {@code MessagePostProcessor}. Might be {@code null}.
	 */
	protected MessagePostProcessor getPostProcessor() {
		return postProcessor;
	}

	/**
	 * Returns additional websocket headers.
	 *
	 * @return additional websocket headers. Might be {@code null}.
	 */
	protected Map<String, Object> getHeaders() {
		final Map<String, Object> stompHeaders = getStompHeaders();
		if (stompHeaders != null && !stompHeaders.isEmpty()) {
			final Map<String, Object> toReturn = new HashMap<>();
			toReturn.put(NativeMessageHeaderAccessor.NATIVE_HEADERS, stompHeaders);
			return toReturn;
		} else return null;
	}

	/**
	 * Returns additional STOMP headers.
	 *
	 * @return additional STOMP headers. Might be {@code null.}
	 */
	protected Map<String, Object> getStompHeaders() {
		return null;
	}

	/*
	 * CONVENIENCE METHODS
	 */

	/**
	 * Sends the given data to all players matching the given {@code Predicate}.
	 * Convenience method which can be used optionally.
	 *
	 * @param payload
	 *            the data to be sent
	 * @param includedAliases
	 *            specifies the players who shall receive a message
	 */
	protected void whisper(final Object payload, final Predicate<String> includedAliases) {
		final String destination = getWhisperDestination();
		state.getState().keySet().forEach(alias -> {
			if (includedAliases.test(alias)) {
				final Optional<String> name = aliasMgr.getName(alias);
				if (name.isPresent()) messagingTemplate.convertAndSendToUser(name.get(), destination, payload,
						getHeaders(), getPostProcessor());
			}
		});
	}

	/**
	 * Sends the given data the requestor. Convenience method which can be used
	 * optionally.
	 *
	 * @param payload
	 *            the data to be sent
	 */
	protected void whisper(final Object payload) {
		messagingTemplate.convertAndSendToUser(requestor.getName(), getWhisperDestination(), payload, getHeaders(),
				getPostProcessor());
	}

	/**
	 * Sends the given data to all players specified by their aliases. Convenience
	 * method which can be used optionally.
	 *
	 * @param payload
	 *            the data to be sent
	 * @param aliases
	 *            the players who shall receive a message
	 */
	protected void whisper(final Object payload, final String... aliases) {
		for (final String alias : aliases) {
			final Optional<String> name = aliasMgr.getName(alias);
			if (name.isPresent()) messagingTemplate.convertAndSendToUser(name.get(), getWhisperDestination(), payload,
					getHeaders(), getPostProcessor());
		}
	}

	/**
	 * Sends the given data to all players. Convenience method which can be used
	 * optionally.
	 *
	 * @param payload
	 *            the data to be sent
	 */
	protected void broadcast(final Object payload) {
		final String destination = getDestination();
		messagingTemplate.convertAndSend(destination, payload, getHeaders(), getPostProcessor());
	}

	/**
	 * Returns the channel for whispers.
	 *
	 * @return the whisper channel
	 */
	protected String getWhisperDestination() {
		return WHISPER_DESTINATION;
	}

	/**
	 * Returns the channel for broadcasts.
	 *
	 * @return the broadcast channel
	 */
	protected String getDestination() {
		return BROADCAST_DESTINATION;
	}

	/**
	 * Executes the job, e.g. generates the data and sends it.
	 */
	protected abstract void execute();
}
