package de.trigoty.backend.socket.handler;

import java.security.Principal;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Scope;

import de.trigoty.service.web.objects.beans.TriangleBean;

@Configuration
public class JobFactory {

	@Bean
	@Scope("prototype")
	public RegisterPlayerHandler getRegisterPlayerHandler(final Principal user) {
		return new RegisterPlayerHandler(user);
	}

	@Bean
	@Scope("prototype")
	public TriangleProposalHandler getTriangleProposalHandler(final Principal user, final TriangleBean proposed) {
		return new TriangleProposalHandler(user, proposed);
	}

	@Bean
	@Scope("prototype")
	public SendFullStateHandler getSendFullStateHandler(final Principal user) {
		return new SendFullStateHandler(user);
	}

	@Bean
	@Scope("prototype")
	public DisconnectHandler getDisconnectHandler(final Principal user) {
		return new DisconnectHandler(user);
	}

	@Bean
	public ClearGameHandler getClearGameHandler() {
		return new ClearGameHandler();
	}

	@Bean
	public TurnHandler getTurnHandler() {
		return new TurnHandler();
	}
}
