package de.trigoty.backend.socket.handler;

import java.security.Principal;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import de.trigoty.data.Pair;
import de.trigoty.data.config.DataConfig;
import de.trigoty.service.web.objects.beans.TriangleBean;
import de.trigoty.service.web.objects.command.UpdateCommand;
import de.trigoty.state.objects.IngameTriangle;

/**
 * Registers a proposed {@code Triangle} to the game state and propagates it to
 * all users. If the registration fails the requesting user will reveive a reset
 * to the server's game state.
 *
 * @author J
 *
 */
public class TriangleProposalHandler extends AbstractSocketHandler {
	/** Logger used in this handler */
	private static final Logger LOGGER = LoggerFactory.getLogger(TriangleProposalHandler.class);
	/** The {@code Triangle} requested by a user */
	private final TriangleBean proposed;

	/**
	 * Creates a new {@code TriangleProposalHandler}.
	 *
	 * @param requestor
	 *            proposing a {@code Triangle}
	 * @param proposed
	 *            the proposed {@code Triangle}
	 */
	TriangleProposalHandler(final Principal requestor, final TriangleBean proposed) {
		super("Triangle proposal handler", requestor);
		this.proposed = proposed;
	}

	@Override
	protected void execute() {
		try {
			final String requestorName = requestor.getName();
			final Optional<String> aliasOpt = aliasMgr.getAlias(requestorName);
			final String alias = aliasOpt.orElse(null);
			if (alias == null || !alias.equals(proposed.getOwner())) {
				LOGGER.warn("Illegal request: Triangle {} was proposed by {}.", proposed, requestorName);
				return;
			}
			final IngameTriangle requestedTriangle = conversion.toTriangle(proposed, DataConfig.ORIENTATION);
			final Pair<List<IngameTriangle>, List<IngameTriangle>> result = state.addTriangle(requestedTriangle);
			if (result == null) {
				LOGGER.warn("Invalid triangle request: {}", proposed);
				whisper(conversion.stateToReset(state));
				return;
			}
			final List<TriangleBean> removed = toBeans(result.first);
			final List<TriangleBean> added = toBeans(result.second);
			broadcast(new UpdateCommand(removed, added));
		} catch (final Exception e) {
			LOGGER.warn("Invalid triangle request for {} : {}", proposed, e);
			whisper(conversion.stateToReset(state));
		}
	}

	/**
	 * Converts the given {@code Triangles} to {@code TriangleBeans} and returns
	 * them. Since this method does utilize stream, it's more efficient especially
	 * for a little number of elements.
	 *
	 * @param toConvert
	 *            the {@code Triangles} which shall be converted
	 * @return the {@code TriangleBeans} created from the given {@code Triangles}
	 */
	private List<TriangleBean> toBeans(final List<IngameTriangle> toConvert) {
		final List<TriangleBean> toReturn = new ArrayList<>(toConvert.size());
		for (final IngameTriangle t : toConvert)
			toReturn.add(conversion.toTriangleBean(t));
		return toReturn;
	}
}
