package de.trigoty.backend.socket.handler;

import de.trigoty.service.web.objects.command.ResetCommand;

/**
 * Resets the game by deregistering all players and clearing the state.
 *
 * @author J
 *
 */
public class ClearGameHandler extends AbstractSocketHandler {

	/**
	 * Creates a new {@code ClearGameHandler}.
	 */
	ClearGameHandler() {
		super("Clear game handler", null);
	}

	@Override
	protected void execute() {
		broadcast(ResetCommand.CLEAR);
		aliasMgr.reset();
		state.clear();
	}
}