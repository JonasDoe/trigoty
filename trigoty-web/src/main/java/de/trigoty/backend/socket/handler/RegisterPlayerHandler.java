package de.trigoty.backend.socket.handler;

import java.security.Principal;
import java.util.Collections;
import java.util.Optional;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

import de.trigoty.service.web.objects.beans.TriangleBean;
import de.trigoty.service.web.objects.command.InitInfo;
import de.trigoty.service.web.objects.command.UpdateCommand;
import de.trigoty.state.config.ApplicationConfiguration;
import de.trigoty.state.objects.IngameTriangle;

/**
 * Registers a new player to the game's state and tells him the initial
 * information.
 *
 * @author J
 *
 */
public class RegisterPlayerHandler extends AbstractSocketHandler {
	/** Logger used in this handler */
	private static final Logger LOGGER = LoggerFactory.getLogger(RegisterPlayerHandler.class);
	/**
	 * Only used for debugging issues atm, but maybe we can later load the start
	 * scenarios this way
	 */
	// @Autowired
	// private TriangleLoader initialStateLoader;
	/**
	 * Holds the settings for this application.
	 */
	@Autowired
	private ApplicationConfiguration appConfig;

	RegisterPlayerHandler(final Principal requestor) {
		super("Register player handler", requestor);
	}

	@Override
	protected void execute() {
		try {
			final String name = requestor.getName();
			final Optional<String> aliasOpt = aliasMgr.register(name);
			if (!aliasOpt.isPresent()) {
				LOGGER.debug("Second registration of user {} denied.", name);
				return;
			}

			final String alias = aliasOpt.get();
			final IngameTriangle initialTriangle = state.registerNewPlayer(alias);
			if (initialTriangle == null) return;

			// stuff for debugging: load scenario
			if (appConfig.debug > 1) for (int i = 0; i < 1000; i++)
				state.turn();
			// final List<TriangleBean> loaded =
			// initialStateLoader.loadTriangles("scenario4.json");
			// loaded.forEach(l -> {
			// if (l.getOwner().equals("player1")) l.setOwner(alias);
			// l.getPoint1().setPlayer(alias);
			// l.getPoint2().setPlayer(alias);
			// l.getPoint3().setPlayer(alias);
			// });
			// toCreate.addAll(loaded);
			// toCreate.stream().map(t -> conversion.toTriangle(t,
			// DataConfig.ORIENTATION)).forEach(state::addTriangle);

			// tell new player about the state of the game
			whisper(new InitInfo(alias, conversion.stateToReset(state)));
			// update all other players with the newly created triangle
			final TriangleBean initBean = conversion.toTriangleBean(initialTriangle);
			whisper(UpdateCommand.forAddition(Collections.singletonList(initBean)), a -> !alias.equals(a));
		} catch (final Exception e) {
			LOGGER.warn("Initial request failed: {}", e);
		}
	}
}
