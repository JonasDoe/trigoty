package de.trigoty.backend;

import java.io.Closeable;
import java.io.IOException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;
import java.util.function.Supplier;

import javax.annotation.PreDestroy;

import org.springframework.stereotype.Component;

/**
 * Processes enqueued jobs. Jobs can be added to a {@code Queue}. Can be stopped
 * and resume.
 *
 * @author J
 *
 */
// TODO: here or in the jobs: on error, send empty command to front end so it
// resumes
@Component
public class JobProcessor implements Closeable {
	/** Processes the jobs */
	private ExecutorService jobExecutor;
	/** Supplies jobExecutors. Used on every time after this handler was stopped. */
	private final Supplier<ExecutorService> jobExecutorFactory;

	/**
	 * Creates a new {@code SingleThreadJobHandler} which processes one job at a
	 * time.
	 */
	public JobProcessor() {
		this(() -> Executors.newSingleThreadExecutor());
	}

	/**
	 * Creates a new {@code SingleThreadJobHandler}.
	 *
	 * @param jobExecutorFactory
	 *            provides the executor to process the jobs.
	 */
	public JobProcessor(final Supplier<ExecutorService> jobExecutorFactory) {
		this.jobExecutorFactory = jobExecutorFactory;
	}

	/**
	 * Starts or resumes the job handler.
	 */
	public void start() {
		if (jobExecutor == null || jobExecutor.isShutdown()) jobExecutor = jobExecutorFactory.get();
	}

	/**
	 * Stops the job handler but allows the current job to finish.
	 */
	public void stop() {
		stop(false);
	}

	/**
	 * Stops the job handler and cancels all remaining jobs.
	 *
	 * @param now
	 *            {@code true} if the running job shall be cancelled, otherwise
	 *            {@code false}
	 */
	public void stop(final boolean now) {
		final ExecutorService old = jobExecutor; // TODO AtomicReference#getAndSet worth it?
		jobExecutor = null;
		if (now) old.shutdownNow();
		else old.shutdown();
		try {
			old.awaitTermination(10, TimeUnit.MILLISECONDS);
		} catch (final InterruptedException e2) {/* noop */ }
	}

	/**
	 * Resets the job handlers state and starts anew. All current jobs will be
	 * cancelled.
	 */
	public void restart() {
		stop(true);
		start();
	}

	/**
	 * Adds a job to be processed.
	 *
	 * @param job
	 *            to be queued and processed
	 * @throws IllegalStateException
	 *             if the {@code JobProcessor} is not running
	 */
	public void enqueueJob(final Job toEnqueue) {
		if (jobExecutor == null) throw new IllegalStateException("Job processor is not running.");
		else jobExecutor.execute(toEnqueue);
	}

	@PreDestroy
	@Override
	public void close() throws IOException {
		stop(true);
	}
}
