package de.trigoty.backend;

import java.util.Objects;

/**
 * Job to be run by the backend.
 *
 * @author J
 *
 */
public abstract class Job implements Runnable {
	/** The name of the runnable */
	private final String name;

	/**
	 * Creates a new {@code NamedRunnable}.
	 *
	 * @param name
	 *            the name of the {@code Runnable}.
	 */
	public Job(final String name) {
		this.name = name;
	}

	/**
	 * Returns a new {@code NamedRunnable}.
	 *
	 * @param name
	 *            the name of the {@code Runnable}
	 * @return the newly created {@code NamedRunnable}. Must not be {@code null}
	 */
	public static Job named(final String name, final Runnable r) {
		Objects.requireNonNull(r);
		return new Job(name) {

			@Override
			public void run() {
				r.run();
			}

		};
	}

	/**
	 * Returns the name of the {@code Runnable}.
	 *
	 * @return the name of the {@code Runnable}
	 */
	public String getName() {
		return name;
	}

	@Override
	public String toString() {
		return "Runnable " + name != null ? name : "<unnamed>";
	}

}
