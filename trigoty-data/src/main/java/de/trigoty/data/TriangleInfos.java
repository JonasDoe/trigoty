package de.trigoty.data;

import java.util.Arrays;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * 
 * @author J
 * @deprecated use {@code TriangleBean} instead
 */
@Deprecated
public class TriangleInfos {
	public final int playerNo;
	public final Vertex edge1Start;
	public final Vertex edge2Start;
	public final Vertex edge3Start;

	@JsonCreator
	public TriangleInfos(@JsonProperty("p1") Vertex edge1Start, @JsonProperty("p2") Vertex edge2Start,
			@JsonProperty("p3") Vertex edge3Start, @JsonProperty("playerNo") int playerNo) {
		this.edge1Start = edge1Start;
		this.edge2Start = edge2Start;
		this.edge3Start = edge3Start;
		this.playerNo = playerNo;
	}

	public List<Vertex> getVertices() {
		return Arrays.asList(edge1Start, edge2Start, edge3Start);
	}

}
