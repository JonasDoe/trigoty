package de.trigoty.data;

import java.util.Objects;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * This class describes a two-dimensional point which represents the start
 * coordinates of an edge.
 *
 * @author Jonas
 *
 */
public class Vertex implements GeometricObject, Comparable<Vertex> {
	private final int x, y;

	@JsonCreator
	public Vertex(@JsonProperty("x") final int x, @JsonProperty("y") final int y) {
		super();
		this.x = x;
		this.y = y;
	}

	public int getX() {
		return x;
	}

	public int getY() {
		return y;
	}

	public static Vertex of(final int x, final int y) {
		return new Vertex(x, y);
	}

	public Vertex subtractVertex(final Vertex vertex) {
		return new Vertex(this.x - vertex.x, this.y - vertex.y);
	}

	public int skalarProduct(final Vertex vertex) {
		return (this.x * vertex.x) + (this.y * vertex.y);
	}

	public Vertex inverseVertex() {
		return new Vertex(-x, -y);
	}

	public int sqrDistanceToPoint(final Vertex vertex) {
		return (int) (Math.pow(this.x - vertex.x, 2) + Math.pow(this.y - vertex.y, 2));
	}

	@Override
	public int compareTo(final Vertex other) {
		final int xComp = x - other.x;
		if (xComp != 0) return xComp;
		else return y - other.y;
	}

	@Override
	public int hashCode() {
		return Objects.hash(x, y);
	}

	@Override
	public boolean equals(final Object obj) {
		if (this == obj) return true;
		if (obj == null) return false;
		if (getClass() != obj.getClass()) return false;
		final Vertex other = (Vertex) obj;
		return Objects.equals(x, other.x) && Objects.equals(y, other.y);
	}

	@Override
	public String toString() {
		return "Coord [x=" + x + ", y=" + y + "]";
	}
}
