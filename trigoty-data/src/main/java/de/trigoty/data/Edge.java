package de.trigoty.data;

import static java.lang.Math.min;
import static java.lang.Math.round;
import static java.lang.Math.toIntExact;

import java.util.Objects;

import de.trigoty.data.Triangle.Orientation;

/**
 * This class describes a geometrical edge, i.e. a line segment specified by a
 * starting point, its previous and its next edge and a twin pointing in the
 * opposite direction. Additionally, there is a reference to the triangle this
 * edge is a side of.
 *
 * @author J
 *
 */
public class Edge implements GeometricObject, Comparable<Edge> {
	/** The {@code Vertex} this {@code Edge} is starting from */
	private Vertex startVertex;
	/**
	 * The preceding {@code Edge} ending at this {@code Edge's} {@code startVertex}
	 */
	private Edge prevEdge;
	/** The {@code Edge} starting at the {@code Vertex} this {@code Edge} ends at */
	private Edge nextEdge;
	/** The neighbour {@code Edge} starting pointing in the opposite direction */
	private Edge twinEdge;
	/**
	 * The {@code Triangle} which is constituted by this {@code Edge} together with
	 * its {@code prevEdge} and its {@code nextEdge}
	 */
	private Triangle relatedTriangle;

	/**
	 * Creates a new {@code Edge}. All parameters can be {@code null}.
	 *
	 * @param startVertex
	 *            the {@code Vertex} this {@code Edge} is starting from
	 * @param prevEdge
	 *            the preceding {@code Edge} ending at this {@code Edge's}
	 *            {@code startVertex}
	 * @param nextEdge
	 *            the {@code Edge} starting at the {@code Vertex} this {@code Edge}
	 *            ends at
	 * @param twinEdge
	 *            the neighbour {@code Edge} starting pointing in the opposite
	 *            direction
	 */
	public Edge(final Vertex startVertex, final Edge prevEdge, final Edge nextEdge, final Edge twinEdge) {
		this.startVertex = startVertex;
		this.prevEdge = prevEdge;
		this.nextEdge = nextEdge;
		this.twinEdge = twinEdge;
	}

	/**
	 * Sets the {@code Triangle} on the "left" side of this {@code Edge}, i.e. the
	 * {@code Triangle} partially surrounded by this {@code Edge}.
	 *
	 * @param relatedTriangle
	 *            to be set for this {@code Edge}
	 */
	public void setRelatedTriangle(final Triangle relatedTriangle) {
		this.relatedTriangle = relatedTriangle;
	}

	/**
	 * Creates and sets the twin edge to this {@code Edge}, if it does not exist
	 * already.
	 *
	 * @return twin edge of this {@code Edge}
	 */
	public Edge createTwinIfAbsent() {
		if (this.twinEdge == null) this.twinEdge = createTwinModel(this);
		return this.twinEdge;
	}

	/**
	 * Checks whether an {@code Edge} fulfills the requirements of an twin edge,
	 * i.e. the start vertex and the end vertex flipped.
	 *
	 * @param toCheck
	 *            {@code} to be checked for twin properties
	 * @return {@code true}, if the {@code edge} is a fitting twin edge
	 */
	public boolean isProperTwinFor(final Edge toCheck) {
		if (toCheck == null) return false;
		else return this.startVertex.equals(toCheck.nextEdge.startVertex)
				&& this.nextEdge.startVertex.equals(toCheck.startVertex);
	}

	/**
	 * Removes the all geometric references to this {@code Edge}
	 */
	public void drop() {
		if (this.twinEdge != null) this.twinEdge.twinEdge = null;
		if (this.nextEdge != null) this.nextEdge.prevEdge = null;
		if (this.prevEdge != null) this.prevEdge.nextEdge = null;
	}

	/**
	 * Returns the {@code Vertex} both {@code Edges} share, if any.
	 *
	 * @param toCheck
	 *            the {@code Edge} to be checked for a common {@code Vertex}
	 * @return the shared {@code Vertex}, if any, otherwise {@code null}
	 */
	public Vertex getTouch(final Edge toCheck) {
		if (toCheck == null) return null;
		else if (startVertex.equals(toCheck.startVertex)) return startVertex;
		else if (startVertex.equals(toCheck.nextEdge.startVertex)) return startVertex;
		else if (nextEdge.startVertex.equals(toCheck.startVertex)) return nextEdge.startVertex;
		else if (nextEdge.startVertex.equals(toCheck.nextEdge.startVertex)) return nextEdge.startVertex;
		else return null;
	}

	/**
	 * Checks whether an {@code Edge} is intersecting with this one. Note that
	 * neigther colinearity nor a shared {@code Vertex} count as intersection atm.
	 *
	 * @param toCheck
	 *            the edge to check for intersection
	 * @return {@code true}, if the {@code Edges} are intersecting
	 */
	public boolean isIntersecting(final Edge toCheck) {
		if (getTouch(toCheck) != null) return false;

		// idea stolen from
		// http://www.geeksforgeeks.org/check-if-two-given-line-segments-intersect
		// Find the four orientations needed for general and special cases
		final Orientation o1 = Triangle.getOrientation(startVertex, nextEdge.startVertex, toCheck.startVertex);
		final Orientation o2 = Triangle.getOrientation(startVertex, nextEdge.startVertex, toCheck.nextEdge.startVertex);
		final Orientation o3 = Triangle.getOrientation(toCheck.startVertex, toCheck.nextEdge.startVertex, startVertex);
		final Orientation o4 = Triangle.getOrientation(toCheck.startVertex, toCheck.nextEdge.startVertex,
				nextEdge.startVertex);

		// General case
		if (o1 != o2 && o3 != o4) return true;
		else return false;
		// Special case (colinearity) skipped
	};

	/**
	 * Checks whether an {@code Edge} is intersecting with this one, considering a
	 * set proximity. Due to the proximity feature this method is somewhat slower
	 * than {@link #isIntersecting(Edge)} which is used currently. Note that
	 * neigther colinearity nor a shared {@code Vertex} count as intersection atm.
	 *
	 * @param toCheck
	 *            the edge to check for intersection
	 * @param proximity
	 *            determines the tolerance and specifies the exclusive max value of
	 *            the squared distance between the two {@code Edge}s. A value of
	 *            {@code null} or {@code 0} will force a real intersection check.
	 * @return {@code true}, if the {@code Edges} are intersecting
	 */
	public boolean isIntersecting(final Edge toCheck, final Integer proximity) {
		if (proximity == null || proximity == 0) return isIntersecting(toCheck);
		else return this.sqrDistanceToEdge(toCheck, proximity) < proximity;
	}

	/**
	 * Calculates the minimal squared distance between a point and this {@code Edge}
	 *
	 * @param toCheck
	 *            represents the point to check
	 * @return the minimal distance
	 */
	public double sqrDistanceToPoint(final Vertex toCheck) {
		// stolen from http://stackoverflow.com/a/6853926/5767484
		final int y1 = this.startVertex.getY();
		final int x1 = this.startVertex.getX();
		final int x2 = this.nextEdge.startVertex.getX();
		final int y2 = this.nextEdge.startVertex.getY();
		final int a = toCheck.getX() - x1;
		final int b = toCheck.getY() - y1;
		final int c = x2 - x1;
		final int d = y2 - y1;

		final int dot = a * c + b * d;
		final int len_sq = c * c + d * d;
		double param = -1;
		if (len_sq != 0) // in case of 0 length line
			param = (double) dot / len_sq;

		double xx;
		double yy;

		if (param < 0) {
			xx = x1;
			yy = y1;
		} else if (param > 1) {
			xx = x2;
			yy = y2;
		} else {
			xx = x1 + param * c;
			yy = y1 + param * d;
		}

		final double dx = toCheck.getX() - xx;
		final double dy = toCheck.getY() - yy;
		return dx * dx + dy * dy;
	}

	/**
	 * Calculates the minimal distance between two {@code Edge}s.
	 *
	 * @param toCheck
	 *            the {@code Edge} to be checked for the distance
	 * @param proximity
	 *            determines the exclusive max square distance which counts as
	 *            intersection
	 * @return the minimal squared distance between two edges
	 */
	public double sqrDistanceToEdge(final Edge toCheck, final int proximity) {
		// TODO remove this check because can't happen in current
		// implementation?
		if (this.isIntersecting(toCheck, proximity)) return 0;
		final double d1 = this.sqrDistanceToPoint(toCheck.startVertex);
		final double d2 = this.sqrDistanceToPoint(toCheck.nextEdge.startVertex);
		final double d3 = toCheck.sqrDistanceToPoint(this.startVertex);
		final double d4 = toCheck.sqrDistanceToPoint(this.nextEdge.startVertex);
		// TODO: if (min > proximity) return min - proximity; here and in js
		// adopt doc then
		return min(d1, min(d2, min(d3, d4)));
	}

	/**
	 * Returns the squared length of this {@code Edge}.
	 *
	 * @return the squared length of this {@code Edge}
	 */
	public double getSqrLength() {
		return startVertex.sqrDistanceToPoint(nextEdge.startVertex);
	}

	/**
	 * Calculates the point of intersection between two {@code Edges}. Slower than
	 * {@link #isIntersecting(Edge)} and should be only used if the exact point is
	 * needed.
	 *
	 * @param toCheck
	 *            the {@code Edge} to check for intersection
	 * @return {@code Vertex representing the point of intersection}, or null
	 */
	@Deprecated
	public Vertex calculateIntersectionPoint(final Edge toCheck) {
		// stolen from http://stackoverflow.com/a/1968345/5767484
		final int p0_x = startVertex.getX();
		final int p0_y = startVertex.getY();
		final int p1_x = nextEdge.startVertex.getX();
		final int p1_y = nextEdge.startVertex.getY();
		final int p2_x = toCheck.startVertex.getX();
		final int p2_y = toCheck.startVertex.getY();
		final int p3_x = toCheck.nextEdge.startVertex.getX();
		final int p3_y = toCheck.nextEdge.startVertex.getY();

		int s1_x, s1_y, s2_x, s2_y;
		s1_x = p1_x - p0_x;
		s1_y = p1_y - p0_y;
		s2_x = p3_x - p2_x;
		s2_y = p3_y - p2_y;
		double s, t;
		// TODO: divisor = 0
		s = (-s1_y * (p0_x - p2_x) + s1_x * (p0_y - p2_y)) / (double) (-s2_x * s1_y + s1_x * s2_y);
		t = (s2_x * (p0_y - p2_y) - s2_y * (p0_x - p2_x)) / (double) (-s2_x * s1_y + s1_x * s2_y);

		if (s >= 0 && s <= 1 && t >= 0 && t <= 1) {
			// Collision detected
			final double i_x = p0_x + (t * s1_x);
			final double i_y = p0_y + (t * s1_y);

			return new Vertex(toIntExact(round(i_x)), toIntExact(round(i_y)));
		}
		return null;
	};

	/**
	 * Returns the {@code Vertex} this {@code Edge} is starting at.
	 *
	 * @return the {@code Vertex} this {@code Edge} is starting at. Might be
	 *         {@code null}.
	 */
	public Vertex getStartVertex() {
		return startVertex;
	}

	/**
	 * Sets {@code Vertex} this {@code Edge} is starting at.
	 *
	 * @param startVertex
	 *            the {@code Vertex} this {@code Edge} is starting at
	 */
	public void setStartVertex(final Vertex startVertex) {
		this.startVertex = startVertex;
	}

	/**
	 * Returns the {@code Edge} which is preceding this one.
	 *
	 * @return the {@code Edge} prececeding this one. Might be {@code null}.
	 */
	public Edge getPrevEdge() {
		return prevEdge;
	}

	/**
	 * Sets the {@code Edge} which is preceding this one.
	 *
	 * @param prevEdge
	 *            the {@code Edge} prececeding this one
	 */
	public void setPrevEdge(final Edge prevEdge) {
		this.prevEdge = prevEdge;
	}

	/**
	 * Returns the {@code Edge} which is succeeding this one.
	 *
	 * @return the {@code Edge} succeeding this one. Might be {@code null}.
	 */
	public Edge getNextEdge() {
		return nextEdge;
	}

	/**
	 * Sets the {@code Edge} which is succeeding this one.
	 *
	 * @param the
	 *            {@code Edge} succeeding this one
	 */
	public void setNextEdge(final Edge nextEdge) {
		this.nextEdge = nextEdge;
	}

	/**
	 * Returns the {@code Edge} registered pointing in the opposite direction to
	 * this one.
	 *
	 * @return the {@code Edge} pointing in the opposite direction to this one.
	 *         Might be {@code null}.
	 */
	public Edge getTwinEdge() {
		return twinEdge;
	}

	/**
	 * Tells whether there's a twin {@code Edge} registered for this one.
	 *
	 * @return {@code true} if there is a twin {@code Edge} registered, otherwise
	 *         {@code false}
	 */
	public boolean hasTwin() {
		return twinEdge != null;
	}

	/**
	 * Links this {@code} with the given twin. In result both {@code Edges} will be
	 * linked to each other.
	 *
	 * @param twinEdge
	 *            to be registered as a twin {@code Edge}
	 * @throws IllegalArgumentException
	 *             if the given {@code Edge} can not be used as twin
	 */
	public void linkAsTwins(final Edge twinEdge) {
		if (twinEdge == null || twinEdge.getTwinEdge() != null)
			throw new IllegalArgumentException("Twin edge must neither be null nor already have a twin edge.");
		if (!isProperTwinFor(twinEdge)) throw new IllegalArgumentException("No proper twin edge given.");
		this.twinEdge = twinEdge;
		twinEdge.twinEdge = this;
	}

	/**
	 * Returns the {@code Triangle} which is constituted by this {@code Edge},
	 * together with its preceding and succeeding {@code Edges}.
	 *
	 * @return the {@code Trianlge} constituted by this {@code Edge}
	 */
	public Triangle getRelatedTriangle() {
		return relatedTriangle;
	}

	/**
	 * Creates a twin for a given {@code Edge}. The given {@code Edge} won't be
	 * linked to the created twin model.
	 *
	 * @param toConvert
	 *            {@code Edge} on whose basis a twin {@code Edge} will be created
	 * @return a twin {@code Edge} model of the given {@code Edge}
	 */
	public static Edge createTwinModel(final Edge toConvert) {
		Edge twinNextEdge = toConvert.prevEdge.twinEdge;
		if (twinNextEdge == null) twinNextEdge = new Edge(toConvert.startVertex, null, null, null);
		return new Edge(toConvert.nextEdge.startVertex, toConvert.nextEdge.twinEdge, twinNextEdge, toConvert);
	}

	@Override
	public int compareTo(final Edge other) {
		final int v1Comp = startVertex.compareTo(other.startVertex);
		if (v1Comp != 0) return v1Comp;
		else return nextEdge.startVertex.compareTo(other.nextEdge.startVertex);
	}

	@Override
	public boolean equals(final Object edge) {
		if (this == edge) return true;
		if (edge == null) return false;
		if (getClass() != edge.getClass()) return false;
		final Edge other = (Edge) edge;
		return Objects.equals(this.startVertex, other.startVertex)
				&& Objects.equals(this.nextEdge.startVertex, other.nextEdge.startVertex);
	};

	@Override
	public int hashCode() {
		return Objects.hash(startVertex, nextEdge.startVertex);
	}

	@Override
	public String toString() {
		final String prev = this.prevEdge == null ? "null" : this.prevEdge.startVertex.toString();
		final String next = this.nextEdge == null ? "null" : this.nextEdge.startVertex.toString();
		// String twin = this.twinEdge == null ? "null"
		// : this.twinEdge.prevEdge.startVertex + " <-- " + this.startVertex + "
		// --> "
		// + this.twinEdge.nextEdge.startVertex;
		// if (debug) return prev + " <-- " + this.startVertex + " --> " + next
		// + "\nTwin: " + twin;
		return prev + " <-- " + this.startVertex + " --> " + next;
	}
}
