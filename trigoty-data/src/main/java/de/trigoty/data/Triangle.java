package de.trigoty.data;

import static java.lang.Math.max;
import static java.lang.Math.min;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Objects;

/**
 * This class describes a geometrical triangle, specified by one of it's
 * surrounding edges and an owner
 *
 * @author J
 *
 */
public class Triangle implements GeometricObject, Comparable<Triangle> {

	/**
	 * Represents the orientation of three vertices that constitute a
	 * {@code Triangle} in 2D, based on a point of origin in the bottom left
	 *
	 * @author J
	 *
	 */
	public enum Orientation {
		CLOCKWISE, COUNTER_CLOCKWISE, COLINEAR;

		/**
		 * Returns the inverse {@code Orientation}.
		 *
		 * @return the inverse {@code Orientation}
		 */
		public Orientation invert() {
			if (this == Orientation.COLINEAR) return this;
			else return this == Orientation.COUNTER_CLOCKWISE ? Orientation.CLOCKWISE : COUNTER_CLOCKWISE;
		}
	}

	/**
	 * {@code Edge} linked to the {@code Triange} and forming it together with its
	 * {@code prevEdge} and it {@code nextEdge}
	 */
	private final Edge relatedEdge;
	/**
	 * {@code Vertices} constituting this {@code Triangle}. Stored to speed up the
	 * {@link #equals(Object)} comparison
	 */
	private List<Vertex> vertices;
	/**
	 * {@code Edges} constituting this {@code Triangle}. Stored to speed up the
	 * processing its {@code Edges}
	 */
	private List<Edge> edges;

	/*
	 * {@code minX}, {@code maxX}, {@code minY} and {@code maxY} describe the
	 * rectangle the triangle lies in. Used for as fast first check whether
	 * collision is possible
	 */
	/** The lower x value of the rectangle containing this {@code Triangle} */
	private int minX;
	/** The upper x value of the rectangle containing this {@code Triangle} */
	private int maxX;
	/** The lower y value of the rectangle containing this {@code Triangle} */
	private int minY;
	/** The upper y value of the rectangle containing this {@code Triangle} */
	private int maxY;

	/**
	 * Creates a new {@code Triangle} based on three points. Not that this
	 * {@code Triangle} bears no relation to existing {@code Edges}. So twin edges,
	 * if any, must be set manually afterwards. (To clarify this point this
	 * constructor can only be called from
	 * {@link #of(Vertex, Vertex, Vertex, Orientation, int)}, i.e in a static way.
	 *
	 * @param vertex1
	 *            the first of the three points of the {@code Triangle}, must not be
	 *            {@code null}
	 * @param vertex2
	 *            the first of the three points of the {@code Triangle}, must not be
	 *            {@code null}
	 * @param vertex3
	 *            the first of the three points of the {@code Triangle}, must not be
	 *            {@code null}
	 * @param targetOrientation
	 *            the orientation the {@code Triangle} to be created. Determines the
	 *            order in which the given {@code Vertices} are processed. Must not
	 *            be {@code null}.
	 * @param owner
	 *            player the {@code Triangle} belongs to, can be {@code null}
	 * @throws IllegalArgumentException
	 *             if the given {@code Vertices} are colinar and therefore don't
	 *             form a proper {@code Triangle}
	 * @throws NullPointerException
	 *             if any argument except the owner is {@code null}
	 * @throws IllegalArgumentException
	 *             if given {@code Vertices} are collinear
	 */
	private Triangle(final Vertex vertex1, final Vertex vertex2, final Vertex vertex3,
			final Orientation targetOrientation) {
		final Orientation vertexOrientation = getOrientation(vertex1, vertex2, vertex3);
		if (vertexOrientation == Orientation.COLINEAR)
			throw new IllegalArgumentException("Vertices must not be collinear.");
		final Vertex v1 = vertex1;
		Vertex v2, v3;
		if (vertexOrientation == Objects.requireNonNull(targetOrientation)) {
			v2 = vertex2;
			v3 = vertex3;
		} else {
			v2 = vertex3;
			v3 = vertex2;
		}
		final Edge v1v2 = new Edge(v1, null, null, null);
		final Edge v2v3 = new Edge(v2, v1v2, null, null);
		final Edge v3v1 = new Edge(v3, v2v3, v1v2, null);
		v1v2.setNextEdge(v2v3);
		v1v2.setPrevEdge(v3v1);
		v2v3.setNextEdge(v3v1);
		this.relatedEdge = v1v2;
		init();
	}

	/**
	 * Creates a new {@code Triangle} based on three points. Not that this
	 * {@code Triangle} bears no relation to existing {@code Edges}. So twin edges,
	 * if any, must be set manually afterwards.
	 *
	 * @param vertex1
	 *            the first of the three points of the {@code Triangle}, must not be
	 *            {@code null}
	 * @param vertex2
	 *            the first of the three points of the {@code Triangle}, must not be
	 *            {@code null}
	 * @param vertex3
	 *            the first of the three points of the {@code Triangle}, must not be
	 *            {@code null}
	 * @param targetOrientation
	 *            the orientation the {@code Triangle} to be created. Determines the
	 *            order in which the given {@code Vertices} are processed. Must not
	 *            be {@code null}.
	 * @throws IllegalArgumentException
	 *             if the given {@code Vertices} are colinar and therefore don't
	 *             form a proper {@code Triangle}
	 * @throws NullPointerException
	 *             if any argument except the owner is {@code null}
	 * @throws IllegalArgumentException
	 *             if given {@code Vertices} are collinear
	 */
	public static Triangle of(final Vertex vertex1, final Vertex vertex2, final Vertex vertex3,
			final Orientation targetOrientation) {
		return new Triangle(vertex1, vertex2, vertex3, targetOrientation);
	}

	/**
	 * Creates a new {@code Triangle}.
	 *
	 * @param relatedEdge
	 *            one of the three {@code Edges} constituting the {@code Triangle}.
	 *            Must not be {@code null}.
	 * @param targetOrientation
	 *            the orientation the {@code Triangle} to be created. Determines the
	 *            order in which the given {@code Vertices} are processed. Must not
	 *            be {@code null}.
	 * @throws NullPointerException
	 *             if the given {@code Edge}, its predecessor or its successor is
	 *             {@code null}, or if the given target {@code Orientation} is
	 *             {@code null}
	 * @throws IllegalArgumentException
	 *             if given {@code Edges} are collinear
	 */
	public Triangle(final Edge relatedEdge, final Orientation targetOrientation) {
		this.relatedEdge = relatedEdge;
		this.relatedEdge.setRelatedTriangle(this);
		final Vertex vertex1 = relatedEdge.getStartVertex();
		final Vertex vertex2 = relatedEdge.getNextEdge().getStartVertex();
		final Vertex vertex3 = relatedEdge.getPrevEdge().getStartVertex();
		final Orientation vertexOrientation = getOrientation(vertex1, vertex2, vertex3);
		if (vertexOrientation == Orientation.COLINEAR)
			throw new IllegalArgumentException("Vertices must not be collinear.");
		if (vertexOrientation != Objects.requireNonNull(targetOrientation)) {
			final Edge temp = relatedEdge.getNextEdge();
			relatedEdge.setNextEdge(relatedEdge.getPrevEdge());
			relatedEdge.setPrevEdge(temp);
		}
		init();
	}

	private final void init() {
		// focus on performance

		// not colinar -> definitivly 3 different vertices
		final List<Vertex> vertices = new ArrayList<>(3);
		final List<Edge> edges = new ArrayList<>(3);
		edges.add(relatedEdge);
		edges.add(relatedEdge.getNextEdge());
		edges.add(relatedEdge.getPrevEdge());
		for (final Edge edge : edges) {
			edge.setRelatedTriangle(this);
			vertices.add(edge.getStartVertex());
		}
		Collections.sort(edges);
		this.edges = Collections.unmodifiableList(edges);
		Collections.sort(vertices);
		this.vertices = Collections.unmodifiableList(vertices);
		setContainingRectangle();
	}

	/**
	 * Sets the values of the rectangle containing this {@code Triangle}. This way
	 * some collision tests can be sped up.
	 */
	private void setContainingRectangle() {
		minX = min(relatedEdge.getStartVertex().getX(), min(relatedEdge.getNextEdge().getStartVertex().getX(),
				relatedEdge.getPrevEdge().getStartVertex().getX()));
		maxX = max(relatedEdge.getStartVertex().getX(), max(relatedEdge.getNextEdge().getStartVertex().getX(),
				relatedEdge.getPrevEdge().getStartVertex().getX()));
		minY = min(relatedEdge.getStartVertex().getY(), min(relatedEdge.getNextEdge().getStartVertex().getY(),
				relatedEdge.getPrevEdge().getStartVertex().getY()));
		maxY = max(relatedEdge.getStartVertex().getY(), max(relatedEdge.getNextEdge().getStartVertex().getY(),
				relatedEdge.getPrevEdge().getStartVertex().getY()));
	}

	/**
	 * Gathers all {@code Edge}s describing this {@code Triangle}.
	 *
	 * @return The three {@code Edges} forming this {@code Triangle}
	 */
	public List<Edge> getEdges() {
		return edges;
	}

	/**
	 * Gathers all {@code Vertices} describing this {@code Triangle}.
	 *
	 * @return The three {@code Vertices} of this {@code Triangle}
	 */
	public List<Vertex> getVertices() {
		return vertices;
	}

	/**
	 * Removes the all geometric references to this {@code Triangle} by dropping all
	 * related {@code Edges}.
	 */
	public void drop() {
		for (final Edge edge : getEdges())
			edge.drop();
	}

	/**
	 * Checks whether a {@code Vertex} is contained in this {@code Triangle}.
	 *
	 * @param toCheck
	 *            the {@code Vertex} to be checked
	 * @return {@code true}, if the {@code Vertex} lies on this {@code Triangle}
	 */
	public boolean contains(final Vertex toCheck) {
		if (toCheck == null) return false;
		if (this.maxX < toCheck.getX() || this.minX > toCheck.getX() || this.maxY < toCheck.getY()
				|| this.minY > toCheck.getY())
			return false;

		// Stolen from http://blackpawn.com/texts/pointinpoly

		// Compute vectors
		final Vertex v0 = relatedEdge.getPrevEdge().getStartVertex().subtractVertex(relatedEdge.getStartVertex());
		final Vertex v1 = relatedEdge.getNextEdge().getStartVertex().subtractVertex(relatedEdge.getStartVertex());
		final Vertex v2 = toCheck.subtractVertex(relatedEdge.getStartVertex());

		// Compute dot products
		final long dot00 = v0.skalarProduct(v0);
		final long dot01 = v0.skalarProduct(v1);
		final long dot02 = v0.skalarProduct(v2);
		final long dot11 = v1.skalarProduct(v1);
		final long dot12 = v1.skalarProduct(v2);

		// Compute barycentric vertexinates
		final double invDenom = 1D / (dot00 * dot11 - dot01 * dot01);
		final double u = (dot11 * dot02 - dot01 * dot12) * invDenom;
		final double v = (dot00 * dot12 - dot01 * dot02) * invDenom;

		// Check if point is in triangle
		return (u >= 0) && (v >= 0) && (u + v < 1);
	}

	/**
	 * Checks whether a {@code Triangle} intersects with this one. A common
	 * {@code Vertex} does not count as intersection.
	 *
	 * @param toCheck
	 *            the {@code Triangle} to be checked for intersection
	 * @return {@code true}, if the {@code Triangle} intersects with on this one
	 */
	public boolean intersectsWithTriangle(final Triangle toCheck) {
		// Fast checks
		if (toCheck == null) return false;
		if (this.maxX < toCheck.minX || this.minX > toCheck.maxX || this.maxY < toCheck.minY
				|| this.minY > toCheck.maxY)
			return false;
		if (this.isNeighbour(toCheck)) return false;

		// If any edge intersects, the triangles intersect
		for (final Edge triangleEdge : getEdges()) {
			for (final Edge toCheckEdge : toCheck.getEdges()) {
				if (triangleEdge.isIntersecting(toCheckEdge)) return true;
			}
		}

		// Check all vertices of the new triangle: if there a vertex
		// contained in this triangle (excluded the edge points theirself)
		// there's an intersection
		for (final Vertex vertex : toCheck.getVertices()) {
			if (!(vertex.equals(relatedEdge.getStartVertex())
					|| vertex.equals(relatedEdge.getNextEdge().getStartVertex())
					|| vertex.equals(relatedEdge.getPrevEdge().getStartVertex())) && this.contains(vertex))
				return true;
		}

		// If all three vertices are equal (which wasn't checked due to the exlusion
		// above) then there's an intersection
		if (this.equals(toCheck)) return true;

		return false;
	}

	/**
	 * Checks whether a {@code Triangle} is a neighbour to this one and therefore
	 * shares a full-edge (i.e. an {@code Edge} of the {@code Triangle} is a twin
	 * {@code Edge} of an {@code Edge} from this {@code Triangle}.
	 *
	 * @param toCheck
	 *            the {@code Triangle} to be checked for neighbourhood
	 * @return {@code true} if the {@code Triangle}s are neighbours
	 */
	public boolean isNeighbour(final Triangle toCheck) {
		if (toCheck == null) return false;
		for (final Edge edge : toCheck.getEdges()) {
			for (final Edge ownEdge : getEdges()) {
				if (ownEdge.getTwinEdge() == null) {
					final Edge createTwinModel = Edge.createTwinModel(ownEdge);
					if (createTwinModel.equals(edge)) return true;
				}
			}
		}
		return false;
	}

	public boolean isInArea(final int maxX, final int maxY) {
		return this.maxX <= maxX && this.maxY <= maxY;
	}

	/**
	 * Determines the orientation of the {@code Triangle}.
	 *
	 * @return the {@code Orientation} of the {@code Triangle}
	 */
	public Orientation getOrientation() {
		return getOrientation(relatedEdge.getStartVertex(), relatedEdge.getNextEdge().getStartVertex(),
				relatedEdge.getPrevEdge().getStartVertex());
	}

	/**
	 * Determines the orientation of three specified vertices expected to constitute
	 * a {@code Triangle}. Idea stolen from
	 * {@code http://www.geeksforgeeks.org/orientation-3-ordered-points}.
	 *
	 * @param vertex1
	 *            of the expected triangle
	 * @param vertex2
	 *            of the expected triangle
	 * @param vertex3
	 *            of the expected triangle
	 * @return the {@code Orientation} of the constituted {@code Triangle}, or
	 *         {@code COLINEAR} if they don't constitute one
	 */
	public static Orientation getOrientation(final Vertex vertex1, final Vertex vertex2, final Vertex vertex3) {
		final int val = (vertex2.getY() - vertex1.getY()) * (vertex3.getX() - vertex2.getX())
				- (vertex2.getX() - vertex1.getX()) * (vertex3.getY() - vertex2.getY());
		if (val == 0) return Orientation.COLINEAR;
		else return (val > 0) ? Orientation.CLOCKWISE : Orientation.COUNTER_CLOCKWISE;
	}

	/**
	 * Returns the area enclosed by this {@code Triangle}.
	 *
	 * @return the area of this {@code Triangle}
	 */
	public double getArea() {
		final Vertex v1 = vertices.get(0);
		final Vertex v2 = vertices.get(1);
		final Vertex v3 = vertices.get(2);
		final int part1 = v1.getX() * (v2.getY() - v3.getY());
		final int part2 = v2.getX() * (v3.getY() - v1.getY());
		final int part3 = v3.getX() * (v1.getY() - v2.getY());
		return Math.abs(part1 + part2 + part3) >> 1;
	}

	@Override
	public int compareTo(final Triangle other) {
		final List<Vertex> otherVertices = other.vertices;
		final int v1Comp = vertices.get(0).compareTo(otherVertices.get(0));
		if (v1Comp != 0) return v1Comp;
		final int v2Comp = vertices.get(1).compareTo(otherVertices.get(1));
		if (v2Comp != 0) return v1Comp;
		return vertices.get(2).compareTo(otherVertices.get(2));
	}

	@Override
	public int hashCode() {
		return Objects.hash(relatedEdge);
	}

	@Override
	public boolean equals(final Object obj) {
		if (this == obj) return true;
		if (obj == null) return false;
		if (getClass() != obj.getClass()) return false;
		final List<Vertex> otherVertices = ((Triangle) obj).vertices;
		return Objects.equals(vertices.get(0), otherVertices.get(0))
				&& Objects.equals(vertices.get(1), otherVertices.get(1))
				&& Objects.equals(vertices.get(2), otherVertices.get(2));
	}

	@Override
	public String toString() {
		return relatedEdge.getStartVertex() + ":" + relatedEdge.getNextEdge().getStartVertex() + ":"
				+ relatedEdge.getPrevEdge().getStartVertex();
	}
}
