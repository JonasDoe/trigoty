package de.trigoty.data.config;

import de.trigoty.data.Triangle.Orientation;

/**
 * Holds general settings regarding the data project.
 *
 * @author J
 *
 */
public class DataConfig {
	/**
	 * Represents the orientation of all triangles created here. The point of origin
	 * is located in the bottom left. Please note that the point of origin on the
	 * client side is located in the upper left.
	 */
	public static final Orientation ORIENTATION = Orientation.CLOCKWISE;
}
