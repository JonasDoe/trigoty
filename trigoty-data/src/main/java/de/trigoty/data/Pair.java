package de.trigoty.data;

import java.util.Objects;

/**
 * This class wraps two entries
 * 
 * @author Jonas
 *
 * @param <T1>
 *            Type of the first entry
 * @param <T2>
 *            Type of the second entry
 */
public class Pair<T1, T2> {
	/** The first entry of the pair */
	public final T1 first;
	/** The second entry of the pair */
	public final T2 second;

	/**
	 * Creates a pair of {@code nullable} entries
	 * 
	 * @param first
	 *            entry of the pair
	 * @param second
	 *            entry of the pair
	 */
	public Pair(T1 first, T2 second) {
		this(first, second, true);
	}

	/**
	 * Creates a pair of entries
	 * 
	 * @param first
	 *            entry of the pair
	 * @param second
	 *            entry of the pair
	 * @param nullable
	 *            {@code true}, if entrys are {@code nullable}
	 */
	public Pair(T1 first, T2 second, boolean nullable) {
		this(first, second, nullable, nullable);
	}

	/**
	 * Creates a pair of entries
	 * 
	 * @param first
	 *            entry of the pair
	 * @param second
	 *            entry of the pair
	 * @param firstNullable
	 *            {@code true}, if first entry is {@code nullable}
	 * @param second
	 *            Nullable {@code true}, if second entry is {@code nullable}
	 */
	public Pair(T1 first, T2 second, boolean firstNullable,
			boolean secondNullable) {
		this.first = firstNullable ? first : Objects.requireNonNull(first);
		this.second = secondNullable ? second : Objects.requireNonNull(second);
	}
}
