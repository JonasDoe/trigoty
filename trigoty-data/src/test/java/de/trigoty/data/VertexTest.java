package de.trigoty.data;

import static org.junit.Assert.*;

import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.fasterxml.jackson.databind.ObjectMapper;

public class VertexTest {
	private static final Logger log = LoggerFactory.getLogger(VertexTest.class);

	private static final ObjectMapper OM = new ObjectMapper();

	/**
	 * Note: every class that is supposed to be JSON-serializable should have a test, reassuring this; high priority
	 */
	@Test
	public void testSerialization() throws Exception {
		Vertex original = new Vertex(15, 38);
		log.info("original: {}", original);
		String json = OM.writeValueAsString(original);
		log.info("json: {}", json);
		Vertex duplicate = OM.readValue(json, Vertex.class);

		assertNotSame(original, duplicate);
		assertTrue(original.equals(duplicate));
		log.info("deserialized: {}", duplicate);
	}

	/**
	 * Note: Complex computation should have test coverage too, for every method at least one call; high priority
	 */
	@Test
	public void testComputation() throws Exception {
		assertEquals(25, Vertex.of(3, 4).sqrDistanceToPoint(Vertex.of(0, 0)));
		assertEquals(Vertex.of(-1, -2), Vertex.of(1, 2).inverseVertex());
		assertEquals(Vertex.of(3, 4), Vertex.of(10, 70).subtractVertex(Vertex.of(7, 66)));
		assertEquals(82, Vertex.of(4, 6).skalarProduct(Vertex.of(7, 9)));
	}

	/**
	 * Note: equals/hashcode aren't that much testworthy, because they're usually generated; very low priority
	 */
	@Test
	public void testEquals() throws Exception {
		Vertex v1 = new Vertex(3, 4);
		Vertex v2 = new Vertex(7, 9);

		assertTrue(v1.equals(v1));
		assertFalse(v1.equals(null));
		assertFalse(v1.equals(new Object()));
		assertFalse(v1.equals(v2));
		assertFalse(v2.equals(v1));
		assertEquals(v1.hashCode(), new Vertex(3, 4).hashCode());
	}
}
