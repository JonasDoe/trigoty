package de.trigoty.state.config;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

/**
 * Holds configurations regarding the state.
 *
 * @author J
 *
 */
@Component
@ConfigurationProperties
// @AutoConfigurationPackage
public class StateSettings {

	/**
	 * Describes the degree of magnetism when line is drawn towards a point.
	 * Represents by the squared distance.
	 */
	public final int triangleValidationEpsilon;
	/** The default with of two of the edges of {@code Triangle} */
	public final int defaultTriangleWidth;
	/** Determines the minimum size of a {@code Triangle} */
	public final int minSquaredRange;

	private StateSettings(@Value("${geometry.epsilon}") final int triangleValidationEpsilon,
			@Value("${geometry.default_triangle_width}") final int defaultTriangleWidth,
			@Value("${geometry.min_range_multiplier}") final int minRangeMultiplier) {
		this.triangleValidationEpsilon = triangleValidationEpsilon;
		this.defaultTriangleWidth = defaultTriangleWidth;
		minSquaredRange = triangleValidationEpsilon * minRangeMultiplier;
	}
}
