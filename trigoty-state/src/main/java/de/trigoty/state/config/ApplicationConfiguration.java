package de.trigoty.state.config;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

/**
 * Holds application wide settings and beans.
 *
 * @author J
 *
 */
@Component
@ConfigurationProperties
public class ApplicationConfiguration {

	/**
	 * Tells in which debug mode the application is running. {@code 0} disables any
	 * debug mode.
	 */
	public final int debug;

	private ApplicationConfiguration(@Value("${controller.debug}") final int debug) {
		this.debug = debug;
	}
}
