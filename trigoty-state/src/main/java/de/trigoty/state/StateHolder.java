package de.trigoty.state;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Optional;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.atomic.AtomicReference;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.stereotype.Component;

import de.trigoty.data.Edge;
import de.trigoty.data.Pair;
import de.trigoty.data.Triangle;
import de.trigoty.data.Vertex;
import de.trigoty.data.config.DataConfig;
import de.trigoty.state.config.StateSettings;
import de.trigoty.state.objects.IngameTriangle;
import de.trigoty.state.validation.GeometricValidation;

/**
 * Holds the states of the players and allows them to be modified.
 *
 * @author J
 *
 */
// TODO cleanup for inactive players
@Component
public class StateHolder {

	/** Hold the settings of this state */
	@Autowired
	private StateSettings config;
	@Autowired
	private ApplicationContext ctx;
	@Autowired
	private Accountant accountant;

	// TODO config
	// TODO greater field might require BigInteger calculations, especially on
	// collision checks!
	public static final int CANVAS_WIDTH = 800;
	public static final int CANVAS_HEIGTH = 600;
	/** Maximum number of players - will be increased later */
	public static final int MAX_PLAYERS = 4;
	/** Holds the state for each player */
	private final Map<String, PlayerState> statesByPlayers = new ConcurrentHashMap<>(MAX_PLAYERS);

	// TODO clean up, remove rogue states

	/**
	 * Registers a new player.
	 *
	 * @param name
	 *            of the player to registered
	 * @return the inital triangle for the registered player, or {@code null} if the
	 *         registration has failed
	 */
	public IngameTriangle registerNewPlayer(final String name) {
		// int newPlayerNo =
		// playersByNo.keySet().stream().mapToInt(Integer::intValue).max().orElse(1);
		// return newPlayerNo <= PLAYERS ? Optional.of(newPlayerNo) : Optional.empty();
		if (statesByPlayers.containsKey(name)) return null;
		if (statesByPlayers.size() > MAX_PLAYERS) return null;
		// somewhat pointlessly fancy, but just a workaround until player number is
		// flexible
		final int position = 1 + statesByPlayers.values().stream().map(s -> s.getPosition()).filter(Objects::nonNull)
				.sorted().reduce(0, (result, element) -> {
					return result.equals(element - 1) ? element : result;
				});
		final IngameTriangle initialTriangle = createInitTriangle(name, position);
		setPlayerState(name, new PlayerState(initialTriangle, position));
		return initialTriangle;
	}

	/**
	 * Sets a player's state.
	 *
	 * @param player
	 *            whose state shall be set
	 * @param state
	 *            to be set for the given player
	 */
	public void setPlayerState(final String player, final PlayerState state) {
		statesByPlayers.put(player, state);
	}

	/**
	 * Attempt to add the {@code Triangle} to the player's list and register it
	 * geometrically.
	 *
	 * @param toCheck
	 *            Information about the {@code Triangle} to add.
	 * @return a {@code Pair} the {@code Triangles} which have been removed as first
	 *         and the {@code Triangles} that have been added as second element, or
	 *         {@code null} if the validation failed
	 */
	// TODO create verification method, discuss return value
	// TODO does not allow intrusion into foreign triangles atm due to anyMatch
	// check
	// TODO return singleton list of added triangle?
	// Streams are avoided in many parts to increase performance
	public Pair<List<IngameTriangle>, List<IngameTriangle>> addTriangle(final IngameTriangle toCheck) {
		if (!toCheck.getTriangle().isInArea(CANVAS_WIDTH, CANVAS_WIDTH)) return null;
		final PlayerState playerState = statesByPlayers.get(toCheck.getOwner());

		// if the given triangle already exists, handle it as an update/upgrade
		final Optional<IngameTriangle> existing = playerState.getTriangles().stream().filter(toCheck::equals).findAny();
		if (existing.isPresent()) {
			final IngameTriangle triangle = existing.get();
			if (triangle.getDefense() >= IngameTriangle.MAX_DEFENSE) return null;
			final boolean success = updateExistingTriangle(triangle, toCheck);
			return success ? new Pair<>(Collections.emptyList(), Collections.singletonList(triangle)) : null;
		}

		// create reversed model of the requested triangle. This way, the matching with
		// existing edges will be easier
		final List<Vertex> requestedVertices = new ArrayList<>(toCheck.getTriangle().getVertices());
		final Triangle reversedTriangle = Triangle.of(requestedVertices.get(0), requestedVertices.get(1),
				requestedVertices.get(2), DataConfig.ORIENTATION.invert());
		final Collection<Edge> reversedEdges = reversedTriangle.getEdges();

		final List<Edge> usedExistingEdges = playerState.getTriangles().stream()
				.flatMap(triangle -> triangle.getTriangle().getEdges().stream())
				.filter(edge -> reversedEdges.contains(edge)).collect(Collectors.toList());
		if (usedExistingEdges.isEmpty() || usedExistingEdges.size() > 3) return null;

		// make sure not of the existing edges already has a twin and therefore can't be
		// a related to a new triangle
		// could be skipped here since addTriangle() will check this too ... but after
		// time is wasted with collision checks
		for (final Edge usedExistingEdge : usedExistingEdges) {
			if (usedExistingEdge.hasTwin()) return null;
		}

		// ensure a minimum distance between the base edge and the third vertex
		if (usedExistingEdges.size() == 1) {
			final Edge forMinSizeCheck = usedExistingEdges.get(0);
			final Vertex notRelevant1 = forMinSizeCheck.getStartVertex();
			final Vertex notRelevant2 = forMinSizeCheck.getNextEdge().getStartVertex();
			for (final Vertex v : toCheck.getTriangle().getVertices()) {
				// find the one vertex which is not connected with the edge
				if (!(v.equals(notRelevant1) || v.equals(notRelevant2))) {
					if (forMinSizeCheck.sqrDistanceToPoint(v) <= config.minSquaredRange) return null;
					else break;
				}
			}
		}

		final AtomicReference<IngameTriangle> invalidIntersection = new AtomicReference<>();
		final List<IngameTriangle> intersections = GeometricValidation.getIntersections(
				statesByPlayers.values().stream().map(PlayerState::getTriangles).flatMap(Collection::stream), toCheck)
				.peek(t -> {
					if (toCheck.getOwner().equals(t.getOwner())) invalidIntersection.set(t);
					// go through all triangles first to find illegal intersection -> collect
					// required for now
				}).collect(Collectors.toList()); // TODO don't go through all but revert changes once there is an
													// invalid one?
		if (invalidIntersection.get() != null) return null;

		final long costs = accountant.getCostsFor(toCheck);
		if (playerState.getMoney() < costs) return null;
		else playerState.addMoney(-costs);

		// TODO later: triangles are fortifiable
		// check the costs for destroying other triangles
		long destructionCosts = 0;
		for (final IngameTriangle triangle : intersections) {
			destructionCosts += accountant.getDestructionCostsFor(triangle);
		}
		if (playerState.getMoney() < destructionCosts) {
			System.out.println();
			return null;
		} else playerState.addMoney(-destructionCosts);

		intersections.forEach(t -> statesByPlayers.get(t.getOwner()).removeTriangle(t));

		final IngameTriangle addedTriangle = playerState.addTriangle(toCheck);
		final Triangle addedGeometricTriangle = addedTriangle.getTriangle();
		addedGeometricTriangle.getEdges().forEach(e -> e.setRelatedTriangle(addedGeometricTriangle));
		return new Pair<>(intersections, Collections.singletonList(addedTriangle));
	}

	/**
	 * Updates {@code IngameTriangle's} values with the ones from another
	 * {@code IngameTriangle}. Note that the inner geometric {@code Triangle} won't
	 * be changed.
	 *
	 * @param toUpdate
	 *            will be updated
	 * @param update
	 *            contains the new values used for the update
	 * @return {@code true} if the update was successful, otherwise {@code false}
	 */
	private boolean updateExistingTriangle(final IngameTriangle toUpdate, final IngameTriangle update) {
		final long defenseUpgradeCosts = accountant.getDefenseUpgradeCosts(toUpdate);
		final PlayerState playerState = statesByPlayers.get(toUpdate.getOwner());
		if (defenseUpgradeCosts <= playerState.getMoney()) {
			playerState.addMoney(-defenseUpgradeCosts);
			toUpdate.increadeDefense();
			return true;
		}
		return false;
	}

	/**
	 * Removes a given {@code Triangle} from the player's list and unregisters it
	 * geometrically.
	 *
	 * @param toRemove
	 *            {@code Triangle} which shall be removed
	 * @return {@code true} if the {@code Triangle} was removed successfully,
	 *         otherwise {@code false}
	 */
	public boolean removeTriangle(final IngameTriangle toRemove) {
		final PlayerState state = statesByPlayers.get(toRemove.getOwner());
		if (state == null) return false;
		else return state.removeTriangle(toRemove);
	}

	/**
	 * Returns an unmodifiable view of the current state.
	 *
	 * @return the current state as a map from player name to its state
	 */
	public Map<String, PlayerState> getState() {
		return Collections.unmodifiableMap(statesByPlayers);
	}

	/**
	 * Creates a initial {@code Triangle} for a given player and returns it.
	 *
	 * @param player
	 *            a {@code Triangle} shall be created for
	 * @return the newly created initial {@code Triangle}, or {@code null} if there
	 *         are no more player slots available
	 */
	// TODO more or less a workaround atm
	private IngameTriangle createInitTriangle(final String player, final int position) {
		Triangle triangle;
		switch (position) {
		case 1:
			triangle = Triangle.of(new Vertex(0, 0), new Vertex(0, config.defaultTriangleWidth),
					new Vertex(config.defaultTriangleWidth, 0), DataConfig.ORIENTATION);
			break;
		case 2:
			triangle = Triangle.of(new Vertex(StateHolder.CANVAS_WIDTH, StateHolder.CANVAS_HEIGTH),
					new Vertex(StateHolder.CANVAS_WIDTH, StateHolder.CANVAS_HEIGTH - config.defaultTriangleWidth),
					new Vertex(StateHolder.CANVAS_WIDTH - config.defaultTriangleWidth, StateHolder.CANVAS_HEIGTH),
					DataConfig.ORIENTATION);
			break;
		case 3:
			triangle = Triangle.of(new Vertex(StateHolder.CANVAS_WIDTH, 0),
					new Vertex(StateHolder.CANVAS_WIDTH, config.defaultTriangleWidth),
					new Vertex(StateHolder.CANVAS_WIDTH - config.defaultTriangleWidth, 0), DataConfig.ORIENTATION);
			break;
		case 4:
			triangle = Triangle.of(new Vertex(0, StateHolder.CANVAS_HEIGTH),
					new Vertex(0, StateHolder.CANVAS_HEIGTH - config.defaultTriangleWidth),
					new Vertex(config.defaultTriangleWidth, StateHolder.CANVAS_HEIGTH), DataConfig.ORIENTATION);
			break;
		default:
			return null;
		}
		triangle.getEdges().forEach(e -> e.setRelatedTriangle(triangle));
		return ctx.getBean(IngameTriangle.class, triangle, player);
	}

	/**
	 * Clears this state. All state information will be lost.
	 */
	public void clear() {
		statesByPlayers.clear();
	}

	/**
	 * Removes a given player from the game.
	 *
	 * @param name
	 *            of the player to be removed.
	 * @return the player's {@code Triangeles} which have been removed.
	 */
	public List<IngameTriangle> removePlayer(final String name) {
		final PlayerState playerState = statesByPlayers.get(name);
		if (playerState != null) {
			statesByPlayers.remove(name);
		}
		return playerState.getTriangles();
	}

	/**
	 * Increments the players' resources based on their income.
	 *
	 * @return the account balance per player
	 */
	public synchronized Map<String, Long> turn() {
		final Map<String, Long> moneyPerPlayer = new HashMap<>(statesByPlayers.size());
		for (final PlayerState playerState : statesByPlayers.values()) {
			long value = 0;
			for (final IngameTriangle ingameTriangle : playerState.getTriangles()) {
				value += accountant.getTurnIncomeFor(ingameTriangle);
			}
			playerState.addMoney(value);
			moneyPerPlayer.put(playerState.getPlayerName(), playerState.getMoney());
		}
		return moneyPerPlayer;
	}
}
