package de.trigoty.state.validation;

import java.util.Collections;
import java.util.List;

import de.trigoty.data.Edge;
import de.trigoty.data.Triangle;

/**
 * Holds information about the current calculated triangle, so it can be draw
 * for preview or registered if really created. This class is bascially
 * mirroring a JavaScript class.
 * 
 * @deprecated Since {@code Triangles} won't be drawn in the back end there's no
 *             reason for this preview class
 */
// TODO mention related twin edges?
@Deprecated
public class PreviewTriangleInfos {
	private Triangle triangle;
	private List<Edge> outlineAdd;
	private List<Edge> outlineRemove;

	public Triangle getTriangle() {
		return triangle;
	}

	public List<Edge> getOutlineAdd() {
		return outlineAdd;
	}

	public List<Edge> getOutlineRemove() {
		return outlineRemove;
	}

	public PreviewTriangleInfos(Triangle triangle, List<Edge> outlineAdd, List<Edge> outlineRemove) {
		this.triangle = triangle;
		this.outlineAdd = outlineAdd;
		this.outlineRemove = outlineRemove;
	}

	/** Erases the triangle and therefore registered twin edges */
	public void drop() {
		if (triangle != null)
			triangle.drop();
		this.outlineAdd = Collections.emptyList();
		this.outlineRemove = Collections.emptyList();
	}

	/**
	 * Makes the object forget about it's triangle and it's other information. This
	 * way, the former this.triangle can't be erased anymore by {@link #drop()}.
	 */
	public void clear() {
		this.triangle = null;
		this.outlineAdd = Collections.emptyList();
		this.outlineRemove = Collections.emptyList();
	}
}