package de.trigoty.state.validation;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Objects;
import java.util.Optional;
import java.util.function.Function;
import java.util.stream.Stream;

import de.trigoty.data.Edge;
import de.trigoty.data.Pair;
import de.trigoty.data.Triangle;
import de.trigoty.data.Vertex;
import de.trigoty.data.config.DataConfig;
import de.trigoty.state.objects.IngameTriangle;

/**
 * Collection of methods used to validate whether a triangle is legit or not. At
 * the moment most methods in this class aren't used because it's basically a
 * transcription of the relating JavaScript. Some cleanup, further documentation
 * and maybe a state might be required.
 *
 * @author Jonas
 *
 */
public final class GeometricValidation {
	/**
	 * Attempts to add the {@code Triangle} to the existing polygone. Thereby, the
	 * triangle will be validated.
	 *
	 * @param toAdd
	 *            {@code Triangle} to be checked
	 * @param triangles
	 *            {@code Triangle}s that form the polygon
	 * @return the triangle to be checked, registered to the polygone
	 * @throws IllegalArgumentException
	 *             if the {@code Triangle} is invalid
	 */
	public static IngameTriangle addTriangleToPolygon(final IngameTriangle toAdd,
			final List<IngameTriangle> triangles) {
		// TODO faster, but less secure, if only outlines will be checked ->
		// boolean or another function with outline?
		boolean outlineTouched = false;
		for (final Edge edgeToCheck : toAdd.getTriangle().getEdges())
			for (final IngameTriangle currentTriangle : triangles) {
				for (final Edge currentEdge : currentTriangle.getTriangle().getEdges()) {
					if (currentEdge.isProperTwinFor(edgeToCheck)) {
						if (currentEdge.getTwinEdge() != null) {
							toAdd.drop();
							throw new IllegalArgumentException("Triangle not on the outside.");
						}
						currentEdge.linkAsTwins(edgeToCheck);
						outlineTouched = true;
					}
				}
			}
		if (!outlineTouched) {
			toAdd.drop();
			throw new IllegalArgumentException("Triangle does not touch the polygone.");
			// } else if (intersectsWithOtherTriangle(triangles, toAdd) != null)
			// {
			// toAdd.drop();
			// throw new IllegalArgumentException("Triangle intersects with
			// another triangle.");
		} else return toAdd;
	}

	/**
	 * Checks all triangles for an intersection with the given triangle. If there is
	 * any, they will be returned.
	 *
	 * @param triangles
	 *            the existing {@code Triangles}
	 * @param toCheck
	 *            to be tested for intersections with the existing {@code Triangles}
	 * @return the {@code Triangles} from the given ones {@code toCheck} intersects
	 *         with
	 */
	public static Stream<IngameTriangle> getIntersections(final Stream<IngameTriangle> triangles,
			final IngameTriangle toCheck) {
		return triangles.filter(t -> t.getTriangle().intersectsWithTriangle(toCheck.getTriangle()));
	}

	/**
	 * Starting from a given {@code Edge} a twin will be searched
	 *
	 * @param edge
	 *            start for the search of a twin
	 * @param nextEdgeSelectionFunction
	 *            Part of the function which determines get the next edge to be
	 *            checked for the twin property, most likely
	 *            {@code Edge::getNextEdge()} or {@code Edge::getPrevEdge()}
	 * @return {@code Edge} that would work as a legit twin of the specified
	 *         {@code Edge}
	 */
	public static Edge findExistingTwin(final Edge edge, final Function<Edge, Edge> nextEdgeSelectionFunction) {
		Edge currentEdge = nextEdgeSelectionFunction.apply(edge);
		if (currentEdge == null) return null;
		while (!edge.isProperTwinFor(currentEdge)) {
			if (currentEdge.getTwinEdge() == null) return null;
			currentEdge = nextEdgeSelectionFunction.apply(currentEdge.getTwinEdge());
		}
		return currentEdge;
	}

	/*
	 * UNUSED FEATURES - TO BE REMOVED?
	 */

	/**
	 * Find the {@code Triangle} which contains a given {@code Vertex}
	 *
	 * @param vertex
	 *            in a {@code Triangle} to be determined
	 * @param possibleTriangles
	 *            all {@code Triangle}s to be checked whether they contain the given
	 *            {@code Vertex}
	 * @return {@code Optional} containing the first matching
	 *         {@code Triangle, if any}
	 */
	public static Optional<Triangle> getContainingTriangle(final Vertex vertex,
			final List<Triangle> possibleTriangles) {
		return possibleTriangles.stream().filter(triangle -> triangle.contains(vertex)).findAny();
	}

	/**
	 * Returns the closest one or to two edges that are intersected.
	 */
	public static Pair<Edge, Edge> getFirstTwoIntersectingEdges(final int player, final List<Edge> outline,
			final Edge drawnEdge) {
		final Vertex mouseStart = drawnEdge.getStartVertex();
		final List<Pair<Edge, Double>> allIntersectingEdges = new ArrayList<>();
		for (final Edge line : outline) {
			final double distance = drawnEdge.sqrDistanceToPoint(mouseStart);
			if (drawnEdge.isIntersecting(line)) allIntersectingEdges.add(new Pair<Edge, Double>(line, distance, false));
		}
		allIntersectingEdges.sort((i1, i2) -> (int) (i1.second - i2.second));

		Edge firstResult = null;
		Edge secondResult = null;
		if (allIntersectingEdges.size() >= 1) {
			firstResult = allIntersectingEdges.get(0).first;
			if (allIntersectingEdges.size() >= 2) {
				secondResult = allIntersectingEdges.get(1).first;
			}
		}
		return new Pair<Edge, Edge>(firstResult, secondResult);
	}

	/**
	 * Finds all outlines that are close to specified {@code Vertex}
	 *
	 * @param player
	 * @param outline
	 * @param epsilon
	 *            determines the proximity as a measure of the squared distance
	 * @param mouseEnd
	 *            the point to be used to find close {@code Edge}s
	 * @return List of {@code Edges} that are close to the given {@code Vertex}.
	 */
	public static List<Edge> findNearEdges(final int player, final List<Edge> outline, final int epsilon,
			final Vertex mouseEnd) {
		final List<Edge> inProximity = new ArrayList<>();
		for (final Edge line : outline) {
			final double proximity = line.sqrDistanceToPoint(mouseEnd);
			if (proximity < epsilon) inProximity.add(line);
		}
		return inProximity;
	}

	/**
	 * Checks whether two edges share a vertex. If that's the case, the edges will
	 * be returned in the order there're linked togehter. Otherwise, null will be
	 * returned.
	 */
	public static Pair<Edge, Edge> checkConnection(final Pair<Edge, Edge> toCheck) {
		final Edge edge1 = toCheck.first;
		final Edge edge2 = toCheck.second;
		if (edge1.getStartVertex().equals(edge2.getNextEdge().getStartVertex()))
			return new Pair<Edge, Edge>(edge1, edge2);
		else if (edge1.getNextEdge().getStartVertex().equals(edge2.getStartVertex()))
			return new Pair<Edge, Edge>(edge2, edge1);
		else return null;
	}

	/**
	 * Checks all outline triangle for an intersection with the given triangle. If
	 * there is one, it will be returned.
	 */
	public static Triangle intersectsWithOtherTriangleOnOutline(final List<Edge> outline, final Triangle triangle) {
		for (final Edge line : outline) {
			final Triangle existingTriangle = line.getRelatedTriangle();
			if (triangle.intersectsWithTriangle(existingTriangle)) return existingTriangle;
		}
		return null;
	}

	/**
	 * Not used atm. Starting from a outline edge, this method collects all edges
	 * from the outline. Won't detect outlines that are not connected, e.g. from
	 * holes.
	 *
	 * @param relatedEdge
	 *            from the outline
	 * @returns all edges establishing the outline
	 */
	public List<Edge> calculateOutline(final Edge startEdge) {
		final List<Edge> outline = new ArrayList<>();
		outline.add(startEdge);
		Edge currentEdge = startEdge.getNextEdge().getTwinEdge() == null ? startEdge.getNextEdge()
				: startEdge.getNextEdge().getTwinEdge().getNextEdge();
		while (!currentEdge.equals(startEdge)) {
			outline.add(currentEdge);
			currentEdge = currentEdge.getNextEdge().getTwinEdge() == null ? currentEdge.getNextEdge()
					: currentEdge.getNextEdge().getTwinEdge().getNextEdge();
		}
		return outline;
	}

	/**
	 * By some given parameters, this function tries to find a legit
	 * {@code triangle}.
	 *
	 * @param player
	 * @param outline
	 * @param epsilon
	 * @param mouseStart
	 * @param mouseEnd
	 * @return Proposition of a legit {@code Triangle} together with its
	 *         implications for the outline, or null
	 */
	@SuppressWarnings("deprecation")
	public static PreviewTriangleInfos createPreviewTriangle(final int player, final List<Edge> outline,
			final int epsilon, final Vertex mouseStart, final Vertex mouseEnd) {
		PreviewTriangleInfos previewTriangle = null;
		/** first edge of the the newly created triangle, if any */
		Edge firstEdge;
		/** second edge of the the newly created triangle, if any */
		Edge secondEdge;
		/** third edge of the the newly created triangle, if any */
		Edge thirdEdge;
		List<Edge> outlineAdd = new ArrayList<>();
		final List<Edge> outlineRemove = new ArrayList<>();
		final Edge drawnEdge = new Edge(mouseStart, null, new Edge(mouseEnd, null, null, null), null);
		Pair<Edge, Edge> involvedEdges = getFirstTwoIntersectingEdges(player, outline, drawnEdge);
		if (involvedEdges.first == null) return null; // no outline edges involved
		final Edge startEdge = involvedEdges.first;
		if (startEdge.sqrDistanceToPoint(mouseEnd) < epsilon) return null;

		// If there's no other intersected edge to connect with, search in
		// proxmity
		// TODO apply proximity logic before?
		if (involvedEdges.second == null && epsilon > 0) {
			final List<Edge> inProximity = findNearEdges(player, outline, epsilon, mouseEnd);
			final Optional<Pair<Edge, Edge>> connected = inProximity.stream()
					.map(edge -> checkConnection(new Pair<Edge, Edge>(startEdge, edge))).filter(Objects::nonNull)
					.findAny();
			if (connected.isPresent()) involvedEdges = connected.get();
			// if there is another edge met with does not share a vertex:
			// check if it's vertix is near. if that's the case, connect to
			// this vertex, else don't allow the drawn triangle
			if (inProximity.size() > 0 && involvedEdges.second == null) {
				previewTriangle = tryToConnectWithVertex(involvedEdges.first, inProximity, mouseEnd, epsilon);
				if (previewTriangle == null) return null;
			}
		}
		if (previewTriangle == null) { // if there's no triangle created yet ...
			// if two outlines involved -> if they are connected, create a
			// triangle from there
			if (involvedEdges.second == null) {
				previewTriangle = tryToConnectIntersections(involvedEdges);
			} else {
				firstEdge = startEdge.createTwinIfAbsent();
				if (firstEdge.getTwinEdge() == null || firstEdge.getTwinEdge()
						.getTwinEdge() == null) { throw new RuntimeException("twinError: " + firstEdge.toString()); }
				outlineRemove.add(startEdge);
				secondEdge = new Edge(startEdge.getStartVertex(), firstEdge, null, null);
				thirdEdge = new Edge(mouseEnd, secondEdge, firstEdge, null);
				firstEdge.setPrevEdge(thirdEdge);
				firstEdge.setNextEdge(secondEdge);
				secondEdge.setNextEdge(thirdEdge);
				outlineAdd = Arrays.asList(secondEdge, thirdEdge);
				previewTriangle = new PreviewTriangleInfos(new Triangle(firstEdge, DataConfig.ORIENTATION), outlineAdd,
						outlineRemove);
			}
		}

		// Check whether the triangle intersects with another one. If
		// that's the case deregister all set twins and cancel
		// Create new, registered triangle
		if (previewTriangle == null) return null;
		final Triangle existingTriangle = intersectsWithOtherTriangleOnOutline(outline, previewTriangle.getTriangle());
		if (existingTriangle != null) {
			previewTriangle.drop();
			return null;
		}
		return previewTriangle;
	}

	/**
	 * Attempts to use a {@code Vertex} of an loose {@code Edge} to draw a
	 * {@code Triangle}. "Loose" means here: this {@code Edge} is not connected with
	 * the first {@code Edge} of the {@code Triangle} (i.e. the first outline
	 * {@code Edge} of crossed by the drawn line.
	 *
	 * @param startEdge
	 *            the first outline {@code Edge} of crossed by the drawn line
	 * @param inProximity
	 *            all {@code Edges} near the mouse end
	 * @param mouseEnd
	 *            end of the drawn line
	 * @param epsilon
	 *            the proximity criteria, here:
	 *            {@code any Vertex distance to the mouse end < epsilon * 2} counts
	 *            as "close"
	 * @return Proposition of a legit {@code Triangle} together with its
	 *         implications for the outline, or null
	 */
	@SuppressWarnings("deprecation")
	public static PreviewTriangleInfos tryToConnectWithVertex(final Edge startEdge, final List<Edge> inProximity,
			final Vertex mouseEnd, final int epsilon) {
		Pair<Vertex, Double> closestCommonVertex = null;
		for (final Edge currentEdge : inProximity) {
			double distance = currentEdge.getStartVertex().sqrDistanceToPoint(mouseEnd);
			if (distance < epsilon * 2 && (closestCommonVertex == null || closestCommonVertex.second < distance))
				closestCommonVertex = new Pair<Vertex, Double>(currentEdge.getStartVertex(), distance, false);
			else {
				distance = currentEdge.getNextEdge().getStartVertex().sqrDistanceToPoint(mouseEnd);
				if (distance < epsilon * 2 && (closestCommonVertex == null || closestCommonVertex.second < distance))
					closestCommonVertex = new Pair<Vertex, Double>(currentEdge.getNextEdge().getStartVertex(), distance,
							false);
			}
		}
		if (closestCommonVertex != null) {
			/** first edge of the the newly created triangle, if any */
			final Edge firstEdge = startEdge.createTwinIfAbsent();
			/** second edge of the the newly created triangle, if any */
			final Edge secondEdge = new Edge(startEdge.getStartVertex(), firstEdge, null, null);
			/** second edge of the the newly created triangle, if any */
			final Edge thirdEdge = new Edge(closestCommonVertex.first, secondEdge, firstEdge, null);
			firstEdge.setPrevEdge(thirdEdge);
			firstEdge.setNextEdge(secondEdge);
			secondEdge.setNextEdge(thirdEdge);
			final List<Edge> outlineAdd = Arrays.asList(secondEdge, thirdEdge);
			final List<Edge> outlineRemove = Arrays.asList(startEdge);
			return new PreviewTriangleInfos(new Triangle(firstEdge, DataConfig.ORIENTATION), outlineAdd, outlineRemove);
		} else return null;
	}

	/**
	 * Attempts to use two connected {@code Edge}s to construct a {@code Triangle}.
	 *
	 * @param involvedEdges
	 * @return Proposition of a legit {@code Triangle} together with its
	 *         implications for the outline, or null
	 */
	@SuppressWarnings("deprecation")
	public static PreviewTriangleInfos tryToConnectIntersections(final Pair<Edge, Edge> involvedEdges) {
		if (involvedEdges.first == null || involvedEdges.second == null)
			throw new IllegalArgumentException("There must exist two involved edges.");
		/** first edge of the the newly created triangle, if any */
		Edge firstEdge;
		/** second edge of the the newly created triangle, if any */
		Edge secondEdge;
		/** second edge of the the newly created triangle, if any */
		Edge thirdEdge;
		final Pair<Edge, Edge> connected = checkConnection(involvedEdges);
		if (connected == null || connected.first == null) return null;
		// if the are connected, close the gap between them
		final Edge startEdge = involvedEdges.first;
		final Edge closestEdge = involvedEdges.second;
		if (startEdge.equals(connected.first)) {
			firstEdge = startEdge.createTwinIfAbsent();
			secondEdge = closestEdge.createTwinIfAbsent();
			thirdEdge = new Edge(closestEdge.getStartVertex(), secondEdge, firstEdge, null);
		} else if (closestEdge.equals(connected.first)) {
			firstEdge = closestEdge.createTwinIfAbsent();
			secondEdge = startEdge.createTwinIfAbsent();
			thirdEdge = new Edge(startEdge.getStartVertex(), secondEdge, firstEdge, null);
		} else return null;
		firstEdge.setPrevEdge(thirdEdge);
		firstEdge.setNextEdge(secondEdge);
		secondEdge.setPrevEdge(firstEdge);
		secondEdge.setNextEdge(thirdEdge);
		final List<Edge> outlineRemove = new ArrayList<>(Arrays.asList(startEdge, closestEdge));
		List<Edge> outlineAdd = Arrays.asList(thirdEdge);
		// check if whole as been closed, i.e. there is a (yet
		// unlinked) twin edge for the thirdEdge
		// TODO: Alternative: just walk the outline and check for
		// matching twin edge?
		final Edge possibleTwin = findExistingTwin(thirdEdge, Edge::getNextEdge);
		if (thirdEdge.getTwinEdge() != null && possibleTwin.equals(findExistingTwin(thirdEdge, Edge::getPrevEdge))) {
			thirdEdge.linkAsTwins(possibleTwin);
		}
		// hole is closed now
		if (thirdEdge.getTwinEdge() != null) {
			outlineRemove.add(thirdEdge.getTwinEdge());
			outlineAdd = Collections.emptyList();
		}
		return new PreviewTriangleInfos(new Triangle(firstEdge, DataConfig.ORIENTATION), outlineAdd, outlineRemove);
	}
}
