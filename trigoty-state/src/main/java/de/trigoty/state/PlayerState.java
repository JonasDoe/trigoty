package de.trigoty.state;

import java.util.Collections;
import java.util.LinkedList;
import java.util.List;
import java.util.ListIterator;

import de.trigoty.data.Triangle;
import de.trigoty.state.objects.IngameTriangle;
import de.trigoty.state.validation.GeometricValidation;

/**
 * Represents the state of one player.
 *
 * @author J
 *
 */
public class PlayerState {
	/** The {@code Triangles} which belong to the player */
	private final List<IngameTriangle> triangles = new LinkedList<>();
	/** The name/id of the player */
	private final String player;
	/** The starting position. Might become obsolete later on. */
	private final int position;
	/** The primary resource */
	private long money = 1;

	/**
	 * Creates a new {@code PlayerState}.
	 *
	 * @param initialTriangle
	 *            the {@code IngameTriangle} to start with. In opposite to
	 *            {@code IngameTriangle} added by {@link #addTriangle(Triangle)}
	 *            this one won't be validated.
	 */
	public PlayerState(final IngameTriangle initialTriangle, final int position) {
		this.position = position;
		this.player = initialTriangle.getOwner();
		triangles.add(initialTriangle);
	}

	/**
	 * Returns a unmodifiable view of all {@code Triangles} which belong to the
	 * player.
	 *
	 * @return all of the player's {@code Triangles}
	 */
	public List<IngameTriangle> getTriangles() {
		return Collections.unmodifiableList(triangles);
	}

	/**
	 * Attempt to add the {@code Triangle} to the player's list and register it
	 * geometrically.
	 *
	 * @param toAdd
	 *            the {@code Triangle} if its valid
	 * @return the {@code Triangle} that has been added
	 * @throws IllegalStateException
	 *             if the validation of the {@code Triangle} failed, e.g. it is not
	 *             part of the polygone
	 */
	public IngameTriangle addTriangle(final IngameTriangle toAdd) {
		final IngameTriangle triangle = GeometricValidation.addTriangleToPolygon(toAdd, triangles);
		triangles.add(triangle);
		return toAdd;
	}

	/**
	 * Removes a given {@code Triangle} from the player's list and unregisters it
	 * geometrically.
	 *
	 * @param toRemove
	 *            {@code Triangle} which shall be removed
	 * @return {@code true} if the {@code Triangle} was removed successfully,
	 *         otherwise {@code false}
	 */
	public boolean removeTriangle(final IngameTriangle toRemove) {
		final ListIterator<IngameTriangle> listIterator = triangles.listIterator();
		while (listIterator.hasNext()) {
			final IngameTriangle next = listIterator.next();
			if (next.equals(toRemove)) {
				next.drop();
				listIterator.remove();
				return true;
			}
		}
		return false;
	}

	/**
	 * Returns the name/id of the player.
	 *
	 * @return the player this state is belongs to
	 */
	public String getPlayerName() {
		return player;
	}

	/**
	 * Returns the starting position of the player.
	 *
	 * @return the player's starting position
	 */
	public int getPosition() {
		return position;
	}

	public long getMoney() {
		return money;
	}

	public void addMoney(final long toAdd) {
		this.money += toAdd;
	}

	@Override
	public String toString() {
		return player + ":" + triangles.toString();
	}
}
