package de.trigoty.state;

import org.springframework.stereotype.Service;

import de.trigoty.data.Triangle;
import de.trigoty.state.objects.IngameTriangle;

/**
 * Holds all the logic relating players' resources and costs of actions.
 * Balancing adjustments should be done here (in its frontend counterpart).
 *
 * @author J
 *
 */
@Service
public class Accountant {
	// TODO Most methods exist for Triangles as well as IngameTriangles because it's
	// not clear in which situations they'll be used

	/** Used to scale down all values */
	private static final double AREA_FACTOR = .001;

	/**
	 * Returns the value of an {@code IngameTriangle}.
	 *
	 * @param triangle
	 *            to be evaluated.
	 * @return the {@code IngameTriangle's} value
	 */
	public long getValueOf(final IngameTriangle triangle) {
		return getValueOf(triangle.getTriangle());
	}

	/**
	 * Returns the value of an {@code Triangle}.
	 *
	 * @param triangle
	 *            to be evaluated.
	 * @return the {@code Triangle} value
	 */
	public long getValueOf(final Triangle triangle) {
		return (long) (Math.max(1, triangle.getArea() * AREA_FACTOR));
	}

	/**
	 * Returns the income a given {@code IngameTriangle} supplies.
	 *
	 * @param triangle
	 *            whose supplied income is requested
	 * @return the supplied income
	 */
	public long getTurnIncomeFor(final IngameTriangle triangle) {
		// TODO BigDecimals?
		// will become more complex with more factors later on, with threshold or
		// logarithm
		return Math.max(1, triangle.getBaseValue() >> 2); // atm: 25% of base value
	}

	/**
	 * Returns the creation costs of the given {@code IngameTriangle}.
	 *
	 * @param triangle
	 *            whose creation costs shall be determined
	 * @return the creation costs of the given {@code IngameTriangle}
	 */
	public long getCostsFor(final IngameTriangle triangle) {
		return getCostsFor(triangle.getTriangle());
	}

	/**
	 * Returns the creation costs of the given {@code Triangle}.
	 *
	 * @param triangle
	 *            whose creation costs shall be determined
	 * @return the creation costs of the given {@code Triangle}
	 */
	public long getCostsFor(final Triangle triangle) {
		return getValueOf(triangle);
	}

	/**
	 * Returns the costs to destroy an {@code IngameTriangle}.
	 *
	 * @param triangle
	 *            whose destruction costs shall be determined
	 * @return the costs to destruct the given {@code IngameTriangle}
	 */
	public long getDestructionCostsFor(final IngameTriangle triangle) {
		return getValueOf(triangle) + getEffectiveDefense(triangle);
	}

	// TODO rework defense so it makes worth the investment

	/**
	 * Returns the costs to upgrade the defense of a given {@code IngameTriangle} by
	 * {@code 1}.
	 *
	 * @param toUpgrade
	 *            whose upgrade costs shall be determined
	 * @return the costs to upgrade the defense of the given {@code IngameTriangle}
	 *         by {@code 1}
	 */
	public long getDefenseUpgradeCosts(final IngameTriangle toUpgrade) {
		return toUpgrade.getBaseValue() * (toUpgrade.getDefense() + 1) * 4;
	}

	/**
	 * Returns the effective defense value based on a given {@code IngameTriangle's}
	 * defense.
	 *
	 * @param triangle
	 *            howse effective defense value shall be determined
	 * @return the effective defense value based on the given
	 *         {@code IngameTriangle's} defense
	 */
	public long getEffectiveDefense(final IngameTriangle triangle) {
		return triangle.getDefense() * 10;
	}
}
