package de.trigoty.state.objects;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Scope;

import de.trigoty.data.Triangle;

@Configuration
public class IngameTriangleFactory {

	@Bean
	@Scope("prototype")
	public IngameTriangle getNewIngameTriangle(final Triangle simpleTriangle, final String owner) {
		final IngameTriangle toReturn = new IngameTriangle(simpleTriangle, owner);
		// toReturn.init();
		return toReturn;
	}
}
