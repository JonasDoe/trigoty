package de.trigoty.state.objects;

import java.util.Objects;

import org.springframework.beans.factory.annotation.Autowired;

import de.trigoty.data.Triangle;
import de.trigoty.state.Accountant;

/**
 * Adds game-related properties to geometrical {@code Triangles}.
 *
 * @author J
 *
 */
public class IngameTriangle {
	/** Player who owns this {@code Triangle} */
	private String owner;
	/** The base value to calculate effect impacts with */
	private Long baseValue = null;
	/** The difficulty to destroy the {@code Triangle} */
	private long defense = 0;
	/** The maximum defense for a {@code IngameTriangle} */
	public static final int MAX_DEFENSE = 10;
	@Autowired
	private Accountant accountant;
	/** The inner, geometrical {@code Triangle} held by this class */
	private final Triangle simpleTriangle;

	public long getBaseValue() {
		if (baseValue == null) baseValue = accountant.getValueOf(this);
		return baseValue;
	}

	public void setBaseValue(final long baseValue) {
		this.baseValue = baseValue;
	}

	/**
	 * Returns the defense value.
	 *
	 * @return the defense value
	 */
	public long getDefense() {
		return defense;
	}

	/**
	 * Sets the defense value.
	 *
	 * @param defense
	 *            the defense to set
	 */
	public void setDefense(final long defense) {
		if (defense > MAX_DEFENSE)
			throw new IllegalArgumentException("Defense must not excede the " + MAX_DEFENSE + ".");
		this.defense = defense;
	}

	/**
	 * Increases the defense value of this {@code IngameTriangle} by {@code 1}.
	 */
	public void increadeDefense() {
		if (defense >= MAX_DEFENSE)
			throw new IllegalArgumentException("Defense must not excede the " + MAX_DEFENSE + ".");
		this.defense++;
	}

	/**
	 * Creates an new {@code IngameTriangle}. {@link #init()} should be called
	 * afterwards. Package private construtor to ensure the {@code IngameTriangle}
	 * will only be created by the {@code IngameTriangleFactory} and thus
	 * initialized properly.
	 *
	 * @param simpleTriangle
	 *            the inner, geometrical {@code Triangle} held by this class
	 * @param owner
	 *            player who owns this {@code Triangle}
	 */
	IngameTriangle(final Triangle simpleTriangle, final String owner) {
		this.simpleTriangle = simpleTriangle;
		this.owner = owner;
	}

	public String getOwner() {
		return owner;
	}

	public void setOwner(final String owner) {
		this.owner = owner;
	}

	public void setTriangle(Triangle toSet) {
		toSet = Objects.requireNonNull(simpleTriangle);
	}

	public Triangle getTriangle() {
		return simpleTriangle;
	}

	public void drop() {
		simpleTriangle.drop();
	}

	@Override
	public String toString() {
		return simpleTriangle.toString() + (owner != null ? ", owner: " + owner : "");
	}

	@Override
	public int hashCode() {
		return Objects.hash(simpleTriangle);
	}

	@Override
	public boolean equals(final Object obj) {
		if (obj == null) return false;
		if (obj == this) return true;
		if (this.getClass().equals(obj.getClass())) {
			final IngameTriangle other = (IngameTriangle) obj;
			return simpleTriangle.equals(other.simpleTriangle) && Objects.equals(owner, other.owner)
					&& getBaseValue() == other.getBaseValue();
		}
		return super.equals(obj);
	}
}
